//
//  PayselfTypes.h
//  RushOrder
//
//  Created by Conan on 3/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#ifndef RushOrder_PayselfTypes_h
#define RushOrder_PayselfTypes_h

#define APPSTORE    1

#define MERCHANT_ITUNES_URL @"itms-apps://itunes.apple.com/us/app/payself-register/id640081209?mt=8"
#define CUSTOMER_ITUNES_URL @"itms-apps://itunes.apple.com/us/app/payself/id640029109?ls=1&mt=8"
#define BRANCH_LINK @"https://au3j3.app.goo.gl/Eskj"
#define BRANCH_LINK_SHT @"https://au3j3.app.goo.gl/Eskj"

// One or nothing Flags - Don't choose more than one option
//#define USE_INTERNAL_SERVER   1
//#define DEBUG_STAGING_SERVER  1
//#define DEBUG_PRODUCT_SERVER  1
//#define RELEASE_DEV_SERVER 1
//////////////////////////////////////////////////////////

// One or nothing Flags - Don't choose more than one option
//#define INTERNAL_SERVER           @"10.0.1.4:3000" // Odradeck
//#define INTERNAL_SERVER             @"192.168.0.24:3000" // Daniel
#if TARGET_IPHONE_SIMULATOR
    #define INTERNAL_SERVER             @"127.0.0.1:3000" // My Dev Server (Local - simulator)
#else
//    #define INTERNAL_SERVER             @"192.168.52.2:3000" // My Dev Server (Office - Device target)
//    #define INTERNAL_SERVER             @"100.114.28.43:3000" //Google campus
    #define INTERNAL_SERVER             @"192.168.1.102:3000" // My Dev Server (Home - Device target)
//    #define INTERNAL_SERVER             @"192.168.0.3:3000" // My Dev Server (Home - Device target)
#endif
//////////////////////////////////////////////////////////

#define DEV_SERVER                  @"dev.rushorderapp.com:3000"
#define STAGING_SERVER              @"dev.rushorderapp.com"
#define PRODUCT_SERVER              @"rushorderapp.com"

#if DEBUG
    #if USE_INTERNAL_SERVER
        #define SERVER_URL_ADDRESS     INTERNAL_SERVER
    #elif DEBUG_STAGING_SERVER
        #define SSL_ENABLED
        #define SERVER_URL_ADDRESS     STAGING_SERVER
    #elif DEBUG_PRODUCT_SERVER
        #define SSL_ENABLED
        #define SERVER_URL_ADDRESS     PRODUCT_SERVER
    #else
        #define SERVER_URL_ADDRESS     DEV_SERVER
    #endif
#else
    #if RELEASE_DEV_SERVER
        #define SERVER_URL_ADDRESS     DEV_SERVER
    #else
        #define SSL_ENABLED
        #if APPSTORE
            #define SERVER_URL_ADDRESS     PRODUCT_SERVER
        #else
            #define SERVER_URL_ADDRESS     STAGING_SERVER
        #endif
    #endif
#endif

#define PCSerial    int64_t
#define PCCurrency  NSInteger

#define TEMP_MERCHANT_ID    2

#define DEFAULT_BOTTOM_MARGIN 20.0f

#define OVER_IOS7 (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)

#define OVER_IOS8 (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_1)


#define USE_ORANGE_CELL 0 // For using orange
#define USE_INLINE_ORANGE 1 // For using orange in PaymentSummaryView

typedef enum issuer_{
    IssuerUnknown = 0,
    IssuerAmericanExpress,
    IssuerBankcard,
    IssuerChinaUnionPay,
    IssuerDinersClubCarteBlanche,
    IssuerDinersClubEnRoute,
    IssuerDinersClubInternational,
    IssuerDinersClubUnitedStatesNCanada,
    IssuerDiscover,
    IssuerInstaPayment,
    IssuerJCB,
    IssuerLaser,
    IssuerMaestro,
    IssuerMasterCard,
    IssuerSolo,
    IssuerSwitch,
    IssuerVisa,
    IssuerVisaElectron,
    IssuerTest
} Issuer;

typedef enum userRole_{
    UserRoleUnknown = 0,
    UserRoleOwner,
    UserRoleStaff
} UserRole;

typedef enum notificationScope_{
    NotificationScopeNone = 0,
NotificationScopeMyOrders,
    NotificationScopeMyTables,
    NotificationScopeAll
} NotificationScope;

typedef enum printerMakerCode_{
    PrinterMakerUnknown = 0,
    PrinterMakerEpson = 101,
    PrinterMakerStar,
    PrinterMakerBixolon
} PrinterMakerCode;

typedef enum cartStatus_{
    CartStatusUnknown = 0,
    CartStatusOrdering = 1,
    CartStatusSubmitted,
    CartStatusFixed
} CartStatus;

// Refer order type at Order.h
typedef enum cartType_{
    CartTypeUnknown = 0,
    CartTypeCart = 2, // order & pay , table base
    CartTypePickup = 3, // order & pay , ticket base
//    CartTypeOrderOnly = 4, // order only - Don't use
    CartTypeTakeout = 5, // order & pay , takeout
    CartTypePickupServiced = 6,// order & pay , takeout, table flag
    CartTypeDelivery = 7
} CartType;


typedef enum orderStatus_{
    OrderStatusUnknown      = 0,        //In Pickup
    OrderStatusDraft        = 1 << 0,   //Ordering... (but failed to pay)
    OrderStatusPaid         = 1 << 1,   //Paid
    OrderStatusConfirmed    = 1 << 2,   //Confirmed
    OrderStatusReady        = 1 << 3,   //Ready
    OrderStatusCompleted    = 1 << 4,
    OrderStatusFixed        = 1 << 5,
    OrderStatusCanceled     = 1 << 6,
    OrderStatusDeclined     = 1 << 7,
    OrderStatusAccepted     = 1 << 8,   // Internal Confirm - for RO Delivery Service
    OrderStatusPrepared     = 1 << 9,   // Internal Ready - for RO Delivery Service
    OrderStatusRefused      = 1 << 10,   // Internal Decline - for RO Delivery Service
} OrderStatus;

// Refer order type at Cart.h
typedef enum orderType_{
    OrderTypeUnknown = 0,
    OrderTypeBill = 1, //pay only
    OrderTypeCart, // order & pay , table base
    OrderTypePickup, // order & pay , ticket base
    OrderTypeOrderOnly, // order only
    OrderTypeTakeout, // order & pay , takeout
    OrderTypeDelivery // order & pay , delivery
} OrderType;

typedef enum customRequestType_{
    CustomRequestTypeUnknown = 0,
    CustomRequestTypeDelivery = 1,      //For entier delivery order
    CustomRequestTypeTakeout,           //For entier takeout order
    CustomRequestTypeDineIn,            //For entier dinein order
    CustomRequestTypeSpicific           //For specific line item
} CustomRequestType;


typedef enum {
    AutoPushTypeNone = 0,
    AutoPushTypeInvite,
    AutoPushTypeMycredit,
} AutoPushType;
#endif
