//
//  RushOrder.h
//  RushOrder
//
//  Created by Conan on 2/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#ifndef RushOrder_RushOrder_h
#define RushOrder_RushOrder_h

#define RUSHORDER_CUSTOMER        1

#if DEBUG
    #define RUSHORDER_PAGE_COUNT    10
#else
    #define RUSHORDER_PAGE_COUNT    20
#endif

#define USING_GOOGLE_MAP        1


#define AMILE 1609.344f

#if DEBUG
    #define ARE_YOU_HERE_DISTANCE 10000000000.0f //meters
#else
    #define ARE_YOU_HERE_DISTANCE 300.0f //meters
#endif

#define DEFAULT_TIP_RATE 0 //%
#define DEFAULT_TIP_DELIVERY_RATE 0 //%
#define DEFAULT_TIP_TAKEOUT_RATE 0 //%

//#define SLIDE_LEFT_MENU 1

#import "PayselfTypes.h"

#ifdef DEBUG
//    #define ALL_PASS            1
//    #define FLURRY_ENABLED      1
//    #define FBAPPEVENT_ENABLED  1
//    #define FIREBASE_ENABLED    1
#else
    #define FLURRY_ENABLED      1
    #define FBAPPEVENT_ENABLED  1
    #define FIREBASE_ENABLED    1
#endif

#define DB_NAME @"Payself.sqlite"

#define ATTR_ACCOUNT @"PayselfPhoneNumberAuthentication"
//#define ATTR_ACCOUNT @"RushOrderCustomerAccount"

#import "PCViewController.h"
#import "PCTableViewController.h"
#import "PCTextField.h"
#import "PCNumericTextField.h"
#import "PCCurrencyTextField.h"
#import "NPKeyboardAwareScrollView.h"
#import "NPToggleButton.h"
#import "NPStretchableButton.h"
#import "NPValidation.h"
#import "M13ProgressHUD.h"
#import "WLHorizontalSegmentedControl.h"
#import "CircleImageView.h"
#import "LogoCircleImageView.h"
#import "TouchableLabel.h"
#import "DoubleImageButton.h"
#import "NPImageButton.h"

#ifdef FLURRY_ENABLED
    #import "Flurry.h"
#endif

#endif
