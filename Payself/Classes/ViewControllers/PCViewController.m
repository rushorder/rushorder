//
//  PCViewController.m
//  RushOrder
//
//  Created by Conan on 2/24/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "IIViewDeckController.h"

@interface PCViewController ()

@end

@implementation PCViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
#if RUSHORDER_CUSTOMER
//    if([APP.viewDeckController isSideClosed:IIViewDeckLeftSide] && [APP.viewDeckController isSideClosed:IIViewDeckRightSide]){
//        self.view.userInteractionEnabled = YES;
//    }
#endif
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
#ifdef FLURRY_ENABLED
    [Flurry endTimedEvent:NSStringFromClass([self class])
           withParameters:nil];
#endif
    
}

- (void)viewDeckController:(IIViewDeckController*)viewDeckController willOpenViewSide:(IIViewDeckSide)viewDeckSide animated:(BOOL)animated
{
//    PCLog(@"willOpenViewSide ME!!! %@", self);
    self.view.userInteractionEnabled = NO;
}

- (void)viewDeckController:(IIViewDeckController*)viewDeckController didCloseViewSide:(IIViewDeckSide)viewDeckSide animated:(BOOL)animated
{
//    PCLog(@"didCloseViewSide ME!!! %@", self);
    self.view.userInteractionEnabled = YES;
}

- (BOOL)isVisible {
    return (self.isViewLoaded && self.view.window && self.parentViewController != nil);
}

@end
