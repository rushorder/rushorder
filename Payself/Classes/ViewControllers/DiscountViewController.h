//
//  DiscountViewController.h
//  RushOrder
//
//  Created by Conan on 5/23/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "PCViewController.h"
#import "Payment.h"
#import "DiscountTableViewCell.h"

@interface DiscountViewController : PCViewController
<
UITableViewDelegate,
UITableViewDataSource,
DiscountTableViewCellDelegate
>


@property (nonatomic, strong) Payment *payment;
@end

@protocol DiscountViewControllerDelegate <NSObject>

@end