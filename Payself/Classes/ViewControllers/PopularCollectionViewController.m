//
//  PopularCollectionViewController.m
//  RushOrder
//
//  Created by Conan Kim on 7/4/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

#import "PopularCollectionViewController.h"
#import "OrderManager.h"
#import "MenuManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ReachabilityService.h"
#import "PCCredentialService.h"
#import "MenuPageViewController.h"
#import "RemoteDataManager.h"

@interface PopularCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *menuItemImageView;

@end

@implementation PopularCollectionViewCell



@end

@interface PopularCollectionViewController ()
@property (nonatomic, strong) NSMutableArray* popularMenus;
@property (nonatomic) NSUInteger currentPage;
@property (nonatomic) BOOL inProcessing;
@property (nonatomic) BOOL moredata;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) IBOutlet UIView *noItemView;
@property (nonatomic, getter = isOnceFetched) BOOL onceFetched;
@property (nonatomic) NSInteger seed;

@end

@implementation PopularCollectionViewController

static NSString * const reuseIdentifier = @"PopularCollectionViewCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    PCLog(@"PopularCollectionViewController Loaded");
    self.collectionView.alwaysBounceVertical = YES;
    
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                        init];
    
    [refreshControl addTarget:self
                       action:@selector(refreshControlChanged:)
             forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    
if([self.collectionView respondsToSelector:@selector(refreshControl)]){
    self.collectionView.refreshControl = self.refreshControl;
} else {
    [self.collectionView addSubview:self.refreshControl];
    self.collectionView.alwaysBounceVertical = YES;
}
    
    
    self.currentPage = 0;
    [self requestPopularMenus];

    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
//    [self.collectionView registerClass:[PopularCollectionViewCell class]
//            forCellWithReuseIdentifier:reuseIdentifier];
//    
    // Do any additional setup after loading the view.
}


- (void)scrollToTop
{
    [self.collectionView setContentOffset:CGPointZero
                                 animated:YES];
}

- (void)refreshControlChanged:(UIRefreshControl *)refreshControl
{
    self.currentPage = 0;
    [self requestPopularMenus];
}

- (BOOL)requestPopularMenus
{
    if(REACH.status == NotReachable){
        PCWarning(@"!!!!======== Network not reachable ========= !!!!!");
        return NO;
    }
    
    if(self.inProcessing){
        PCWarning(@"Requesting orders is on the progress");
        return NO;
    }
    
    self.inProcessing = YES;
    
    if(self.currentPage == 0){
        self.seed = arc4random();
    }
    
    RequestResult result = RRFail;
    result = [MENU requestPopularMenuInLocation:APP.location
                                           page:(self.currentPage + 1)
                                           seed:self.seed
                                completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              if(self.currentPage == 0){
                                  self.popularMenus = nil;
                              }
                              
                              [self.popularMenus addObjectsFromArray:(NSArray *)response];
                              
                              self.currentPage++;
                              
                              if([(NSArray *)response count] < 48){
                                  self.moredata = NO;
                              } else {
                                  self.moredata = YES;
                              }
                              
                              [self.collectionView reloadData];
                              
                          }
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting orders", nil)];
                              }
                          }
                              break;
                      }
                      if(!self.isOnceFetched){
                          self.onceFetched = YES;
                          [self.collectionView reloadData];
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  
                  self.inProcessing = NO;
                  [SVProgressHUD dismiss];
                  [self.refreshControl endRefreshing];
                  
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            return YES;
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            self.inProcessing = NO;
            [self.refreshControl endRefreshing];
            return NO;
            break;
        default:
            self.inProcessing = NO;
            [self.refreshControl endRefreshing];
            return NO;
            break;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.collectionView reloadData];
    PCLog(@"PopularCollectionViewController Appeared");
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(ORDER.shouldMoveToOrders){
        [APP.tabBarController setSelectedIndex:3];
        UINavigationController *tab1NavigationController = (UINavigationController *)APP.tabBarController.viewControllers[3];
        [tab1NavigationController popToRootViewControllerAnimated:YES];
        ORDER.shouldMoveToOrders = NO;
        
        PCLog(@"Order Type just finished %d", ORDER.order.orderType);
        if(ORDER.order.orderType == OrderTypeDelivery && ORDER.isSetTempDelivery){
            PCLog(@"Temp Delivery Addr %@", ORDER.tempDeliveryAddress.receiverName);
            PCLog(@"Temp Delivery Addr %@", ORDER.lastUsedDeliveryAddress.name);
            
            DeliveryAddress *similarAddress = nil;
            DeliveryAddress *sameAddress = nil;
            
            for(DeliveryAddress *anAddress in REMOTE.deliveryAddresses){
                DeliveryAddressSimilarity rtn = [anAddress sameWithAddresses:ORDER.tempDeliveryAddress];
                switch(rtn){
                    case DeliveryAddressSimilaritySimilar:
                        if(similarAddress == nil || !similarAddress.isDefaultAddress){
                            similarAddress = anAddress;
                        }
                        break;
                    case DeliveryAddressSimilaritySame:
                        sameAddress = anAddress;
                        break;
                    default:
                        break;
                }
                
                if(sameAddress != nil){
                    break;
                }
            }
            
            if(sameAddress != nil){
                ORDER.lastUsedDeliveryAddress = sameAddress;
                if(!sameAddress.isDefaultAddress){
                    if(ORDER.tempDeliveryAddress.needToBeUpdatedToDefault){
                        // Update to default
                        [DeliveryAddress updateDeliveryAddress:sameAddress withAddress:nil];
                    } else {
                        // Do nothing
                    }
                } else {
                    // Do nothing!
                }
            } else if(similarAddress != nil){
                ORDER.lastUsedDeliveryAddress = similarAddress;
                // Ask to update one or create one?
                UIAlertView  *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Update saved delivery address?", nil)
                                                                     message:[NSString stringWithFormat:NSLocalizedString(@"%@\nTO\n%@", nil), ORDER.lastUsedDeliveryAddress.fullAddress, ORDER.tempDeliveryAddress.fullAddress]
                                                                    delegate:self
                                                           cancelButtonTitle:NSLocalizedString(@"No", nil)
                                                           otherButtonTitles:NSLocalizedString(@"Yes", nil), NSLocalizedString(@"No, save as new", nil), nil];
                alertView.tag = 212;
                [alertView show];
                
            } else {
                if(ORDER.tempDeliveryAddress.needToBeUpdatedToDefault){
                    [DeliveryAddress newDeliveryAddressWithAddress:ORDER.tempDeliveryAddress];
                } else {
                    [UIAlertView askWithTitle:NSLocalizedString(@"Save this delivery address?", nil)
                                      message:nil //ORDER.tempDeliveryAddress.fullAddress
                                     delegate:self
                                          tag:211];
                }
            }
        }
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    NSInteger numberOfMenus = [self.popularMenus count];
    if(numberOfMenus > 0){
        [self.noItemView removeFromSuperview];
    } else {
        if(self.isOnceFetched){
            self.noItemView.frame = self.collectionView.bounds;
            [self.collectionView addSubview:self.noItemView];
        }
    }
    return numberOfMenus;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PopularCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                                                forIndexPath:indexPath];
    
    MenuItem *menu = [self.popularMenus objectAtIndex:indexPath.row];
    
    // Configure the cell
    cell.menuItemImageView.image = nil;
    
    if(menu.imageURL != nil){
        [cell.menuItemImageView sd_setImageWithURL:menu.imageURL];
    }
    
    if (self.moredata && indexPath.row > [self.popularMenus count] - 4){
        [self requestPopularMenus];
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat sideLength = (collectionView.bounds.size.width - 3) / 3;
    
    return CGSizeMake(sideLength, sideLength);
}

-(NSMutableArray *)popularMenus
{
    if(_popularMenus == nil){
        _popularMenus = [NSMutableArray array];
    }
    
    return _popularMenus;
}


#pragma mark <UICollectionViewDelegate>


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MenuItem *menu = nil;
    menu = [self.popularMenus objectAtIndex:indexPath.row];
    
    MenuListType neoMenuListType = MenuListTypeNone;
    CartType cartType = CartTypeCart;
    
    if(menu.forDelivery){
        neoMenuListType = MenuListTypeDelivery;
        cartType = CartTypeDelivery;
    } else if(menu.forTakeout){
        neoMenuListType = MenuListTypeTakeout;
        cartType = CartTypeTakeout;
    } else if(menu.forDineIn){
        neoMenuListType = MenuListTypeDinein;
        if(menu.merchant.servicedBy == ServicedByStaff){
            cartType = CartTypePickupServiced;
        } else {
            cartType = CartTypePickup;
        }
    }
    
    BOOL isMenuReset = [MENUPAN resetGraphWithMerchant:menu.merchant
                                              withType:neoMenuListType];
    
    if(isMenuReset){
        [MENUPAN requestMenusWithSuccess:^(id response) {
            [self pushMenuPageViewController:menu cartType:cartType referredMenus:@[@{@"name":menu.name, @"id":[NSNumber numberWithSerial:menu.menuItemNo]}]];
        } andFail:^(NSUInteger errorCode) {
            PCError(@"Can't continue to get menu for this order - requestMenusWithSuccess in near you");
            [UIAlertView alertWithTitle:@"Oops! Unexpected Error"
                                message:@"Sorry, something seems a little buggy! Please try exiting the app and restarting. Please don't hesitate to reach out to us if the problem persists."];
        }];
        
    } else {
        [self pushMenuPageViewController:menu cartType:cartType referredMenus:@[@{@"name":menu.name, @"id":[NSNumber numberWithSerial:menu.menuItemNo]}]];
    }

}

- (void)pushMenuPageViewController:(MenuItem *)menu cartType:(CartType)cartType referredMenus:(NSArray *)referredMenus
{
    MenuPageViewController *viewController = [MenuPageViewController viewControllerFromNib];
    viewController.initialCartType = cartType;
    viewController.referredMenus = referredMenus;
    [ORDER resetGraphWithMerchant:menu.merchant];
    viewController.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:viewController animated:YES];
}

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
