//
//  CloudPhotoCollectionViewController.m
//  RushOrder
//
//  Created by Conan Kim on 2/6/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

#import "CloudPhotoCollectionViewController.h"
#import "PCGeneralService.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface CloudPhotoCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *photoView;
@property (weak, nonatomic) IBOutlet UIImageView *checkMarkImageView;
@end

@implementation CloudPhotoCollectionViewCell

@end

@interface CloudPhotoCollectionViewController ()
@property NSArray* cloudPhotos;
@property NSDictionary *selectedPhoto;
@property NSInteger selectedRow;
@end

@implementation CloudPhotoCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
//    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
    self.selectedRow = NSNotFound;
    [self requestCloudPhotos];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)requestCloudPhotos
{
    RequestResult result = RRFail;
    result = [GENERAL requestCloudPhotosInCriteria:CloudPhotoCriteriaRapidOrder
                               withCompletionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              PCLog(@"Response : %@", response);
                              
                              self.cloudPhotos = (NSArray *)response;
                              [self.collectionView reloadData];
                              
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting my points", nil)];
                                  
                              }
                              break;
                          }
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
//                  [self.refreshControl endRefreshing];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
//            [self.refreshControl endRefreshing];
            break;
        default:
            break;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.cloudPhotos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *aPhotoDict = [self.cloudPhotos objectAtIndex:indexPath.row];
    CloudPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    [cell.photoView sd_setImageWithURL:[NSURL URLWithString:[[aPhotoDict objectForKey:@"image"] objectForKey:@"url"]]];
    
    cell.checkMarkImageView.hidden = (indexPath.row != self.selectedRow);
    
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat sideLength = (collectionView.bounds.size.width - 2.0f) / 3;
    return CGSizeMake(sideLength, sideLength);
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger prevSelectedRow = self.selectedRow;
    self.selectedRow = indexPath.row;
    
    if(prevSelectedRow != NSNotFound){
        [collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:prevSelectedRow
                                                                   inSection:0]]];
    }
    
    self.selectedPhoto = [self.cloudPhotos objectAtIndex:self.selectedRow];
    
    [collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.selectedRow
                                                                 inSection:0]]];
}

- (IBAction)applyButtonTouched:(id)sender
{
    if([self.delegate respondsToSelector:@selector(cloudPhotoCollectionViewController:didSelectPhoto:)]){
        [self.delegate cloudPhotoCollectionViewController:self
                                          didSelectPhoto:self.selectedPhoto];
    }
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
