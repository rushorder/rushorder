//
//  MenuPhotoViewController.h
//  RushOrder
//
//  Created by Conan on 12/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "MenuCategory.h"

@protocol MenuPhotoViewControllerDelegate;

@interface MenuPhotoViewController : UIViewController
<TouchableLabelAction>

@property (weak, nonatomic) id<MenuPhotoViewControllerDelegate> delegate;

@property (nonatomic) NSIndexPath *indexPath;
@property (nonatomic) id item;
@property (strong, nonatomic) MenuCategory *category;

@property (nonatomic) BOOL shouldMenuDetailPop;
@end

@protocol MenuPhotoViewControllerDelegate <NSObject>
@optional
- (void)menuPhotoViewController:(MenuPhotoViewController *)viewController
didSelectAddItemButtonAtIndexPath:(NSIndexPath *)indexPath;

- (void)menuPhotoViewController:(MenuPhotoViewController *)viewController
didTouchDescLabelAtIndexPath:(NSIndexPath *)indexPath;

- (void)menuPhotoViewController:(MenuPhotoViewController *)viewController
 didTouchCloseButtonAtIndexPath:(NSIndexPath *)indexPath;
@end
