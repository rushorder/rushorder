//
//  NearYouTableViewController.m
//  RushOrder
//
//  Created by Conan Kim on 7/4/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

#import "NearYouTableViewController.h"
#import "TransactionManager.h"
#import "PCCredentialService.h"
#import "ReachabilityService.h"
#import "PCMenuService.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MenuPageViewController.h"
#import "OrderManager.h"
#import "MenuManager.h"
#import "MerchantViewController.h"
#import "RemoteDataManager.h"
#import "PCAppDelegate.h"

@implementation NearYouTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.pic1 drawBorderWithColor:[UIColor lightGrayColor]];
    [self.pic2 drawBorderWithColor:[UIColor lightGrayColor]];
    [self.pic3 drawBorderWithColor:[UIColor lightGrayColor]];
    [self.pic4 drawBorderWithColor:[UIColor lightGrayColor]];
    [self.pic5 drawBorderWithColor:[UIColor lightGrayColor]];
    
    self.pic1.target = self;
    self.pic2.target = self;
    self.pic3.target = self;
    self.pic4.target = self;
    self.pic5.target = self;
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    UIColor *backgroundColor = self.profileImageView.backgroundColor;
    [super setHighlighted:highlighted animated:animated];
    self.profileImageView.backgroundColor = backgroundColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    UIColor *backgroundColor = self.profileImageView.backgroundColor;
    [super setSelected:selected animated:animated];
    self.profileImageView.backgroundColor = backgroundColor;
}

- (void)touchableImageViewActionTriggerred:(TouchableImageView *)sender
{
    UITableView *tableView = self.tableView;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    if([tableView.delegate respondsToSelector:@selector(tableView:cell:didTouchImageButtonAtIndexPath:withMenuIndex:)]){
        [((id <NearYouTableViewCellDelegate>)tableView.delegate) tableView:tableView
                                                                      cell:self
                                            didTouchImageButtonAtIndexPath:indexPath
                                                             withMenuIndex:(PCSerial)sender.tag];
    }
}

@end


@interface NearYouTableViewController ()

@property (nonatomic, strong) NSMutableArray* nearYouOrders;
@property (nonatomic) NSUInteger currentPage;
@property (nonatomic) BOOL inProcessing;
@property (nonatomic) BOOL moredata;
@property (strong, nonatomic) IBOutlet UIView *noItemView;
@property (nonatomic, getter = isOnceFetched) BOOL onceFetched;
@property (nonatomic) NSInteger seed;
@property (nonatomic) CartType selectedCartType;

@end

@implementation NearYouTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    PCLog(@"NearYouTableViewController Loaded");
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 160.0; // set to whatever your "average" cell
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                        init];
    
    [refreshControl addTarget:self
                       action:@selector(refreshControlChanged:)
             forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    
    self.currentPage = 0;
    [self requestNearYou];
}

- (void)refreshControlChanged:(UIRefreshControl *)refreshControl
{
    self.currentPage = 0;
    [self requestNearYou];
}

- (BOOL)requestNearYou
{
    if(REACH.status == NotReachable){
        PCWarning(@"!!!!======== Network not reachable ========= !!!!!");
        return NO;
    }
    
    if(self.inProcessing){
        PCWarning(@"Requesting orders is on the progress");
        return NO;
    }
    
    self.inProcessing = YES;
    
    if(self.currentPage == 0){
        self.seed = arc4random();
    }
    
    RequestResult result = RRFail;
    result = [MENU requestNearMeAuthToken:CRED.customerInfo.authToken
                                 location:APP.location
                                     page:(self.currentPage + 1)
                                     seed:self.seed
                          completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              if(self.currentPage == 0){
                                  self.nearYouOrders = nil;
                              }
                              
                              [self.nearYouOrders addObjectsFromArray:(NSArray *)response];
                              
                              self.currentPage++;
                              
                              if([(NSArray *)response count] < RUSHORDER_PAGE_COUNT){
                                  self.moredata = NO;
                              } else {
                                  self.moredata = YES;
                              }
                              
                              [self.tableView reloadData];
                              
                          }
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting orders", nil)];
                              }
                          }
                              break;
                      }
                      if(!self.isOnceFetched){
                          self.onceFetched = YES;
                          [self.tableView reloadData];
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  
                  self.inProcessing = NO;
                  [SVProgressHUD dismiss];
                  [self.refreshControl endRefreshing];
                  
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            return YES;
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            self.inProcessing = NO;
            [self.refreshControl endRefreshing];
            return NO;
            break;
        default:
            self.inProcessing = NO;
            [self.refreshControl endRefreshing];
            return NO;
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    PCLog(@"NearYouTableViewController Appeared");
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(ORDER.shouldMoveToOrders){
        [APP.tabBarController setSelectedIndex:3];
        UINavigationController *tab1NavigationController = (UINavigationController *)APP.tabBarController.viewControllers[3];
        [tab1NavigationController popToRootViewControllerAnimated:YES];
        ORDER.shouldMoveToOrders = NO;
        
        PCLog(@"Order Type just finished %d", ORDER.order.orderType);
        if(ORDER.order.orderType == OrderTypeDelivery && ORDER.isSetTempDelivery){
            PCLog(@"Temp Delivery Addr %@", ORDER.tempDeliveryAddress.receiverName);
            PCLog(@"Temp Delivery Addr %@", ORDER.lastUsedDeliveryAddress.name);
            
            DeliveryAddress *similarAddress = nil;
            DeliveryAddress *sameAddress = nil;
            
            for(DeliveryAddress *anAddress in REMOTE.deliveryAddresses){
                DeliveryAddressSimilarity rtn = [anAddress sameWithAddresses:ORDER.tempDeliveryAddress];
                switch(rtn){
                    case DeliveryAddressSimilaritySimilar:
                        if(similarAddress == nil || !similarAddress.isDefaultAddress){
                            similarAddress = anAddress;
                        }
                        break;
                    case DeliveryAddressSimilaritySame:
                        sameAddress = anAddress;
                        break;
                    default:
                        break;
                }
                
                if(sameAddress != nil){
                    break;
                }
            }
            
            if(sameAddress != nil){
                ORDER.lastUsedDeliveryAddress = sameAddress;
                if(!sameAddress.isDefaultAddress){
                    if(ORDER.tempDeliveryAddress.needToBeUpdatedToDefault){
                        // Update to default
                        [DeliveryAddress updateDeliveryAddress:sameAddress withAddress:nil];
                    } else {
                        // Do nothing
                    }
                } else {
                    // Do nothing!
                }
            } else if(similarAddress != nil){
                ORDER.lastUsedDeliveryAddress = similarAddress;
                // Ask to update one or create one?
                UIAlertView  *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Update saved delivery address?", nil)
                                                                     message:[NSString stringWithFormat:NSLocalizedString(@"%@\nTO\n%@", nil), ORDER.lastUsedDeliveryAddress.fullAddress, ORDER.tempDeliveryAddress.fullAddress]
                                                                    delegate:self
                                                           cancelButtonTitle:NSLocalizedString(@"No", nil)
                                                           otherButtonTitles:NSLocalizedString(@"Yes", nil), NSLocalizedString(@"No, save as new", nil), nil];
                alertView.tag = 212;
                [alertView show];
                
            } else {
                if(ORDER.tempDeliveryAddress.needToBeUpdatedToDefault){
                    [DeliveryAddress newDeliveryAddressWithAddress:ORDER.tempDeliveryAddress];
                } else {
                    [UIAlertView askWithTitle:NSLocalizedString(@"Save this delivery address?", nil)
                                      message:nil //ORDER.tempDeliveryAddress.fullAddress
                                     delegate:self
                                          tag:211];
                }
            }
        }
        
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger numberOfOrders = [self.nearYouOrders count];
    if(numberOfOrders > 0){
        [self.noItemView removeFromSuperview];
    } else {
        if(self.isOnceFetched){
            self.noItemView.frame = self.tableView.bounds;
            [self.tableView addSubview:self.noItemView];
        }
    }
    return numberOfOrders;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Order *order = nil;
    order = [self.nearYouOrders objectAtIndex:indexPath.row];
    
    NearYouTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"nearYouCell" forIndexPath:indexPath];
    cell.delegate = self;
    [cell.profileImageView sd_setImageWithURL:order.customerPhotoURL
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                        
                                    }];
    NSMutableAttributedString *desc = [[NSMutableAttributedString alloc] init];
    
    if(order.receiverName != nil){
        [desc appendAttributedString:[[NSAttributedString alloc] initWithString:order.receiverName]];
    } else {
        [desc appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"Someone", nil)]];
    }
    [desc appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@" ordered ", nil)]];
    
    NSInteger i = 0;
    cell.pic1.image = nil; BOOL pic1Set = NO; cell.pic1.hidden = YES; cell.pic1.tag = 0;
    cell.pic2.image = nil; BOOL pic2Set = NO; cell.pic2.hidden = YES; cell.pic2.tag = 0;
    cell.pic3.image = nil; BOOL pic3Set = NO; cell.pic3.hidden = YES; cell.pic3.tag = 0;
    cell.pic4.image = nil; BOOL pic4Set = NO; cell.pic4.hidden = YES; cell.pic4.tag = 0;
    cell.pic5.image = nil; BOOL pic5Set = NO; cell.pic5.hidden = YES; cell.pic5.tag = 0;
    
    for(LineItem *lineItem in order.lineItems){
        if(lineItem.menuName != nil){
            [desc appendAttributedString:[[NSAttributedString alloc] initWithString:lineItem.menuName
                                                                         attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:12.0f],
                                                                                      NSLinkAttributeName: [NSURL URLWithString:[NSString stringWithFormat:@"rushorderapp://menu/%lld?row=%ld&section=%ld", lineItem.menuItemNo, (long)indexPath.row, (long)indexPath.section]]}]];
        } else {
            [desc appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"Unknown", nil)
                                                                         attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:12.0f]}]];
        }
        
        if(i < [order.lineItems count] - 1 && [order.lineItems count] > 2){
            [desc appendAttributedString:[[NSAttributedString alloc] initWithString:@","]];
        }
        
        if([order.lineItems count] <= 10){
            if(i == [order.lineItems count] - 2){
                [desc appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@" and ", nil)]];
            } else if(i < [order.lineItems count] - 2 && [order.lineItems count] > 1){
                [desc appendAttributedString:[[NSAttributedString alloc] initWithString:@" "]];
            }
        }
        
        if([order.lineItems count] > 10 && i == 9){
            [desc appendAttributedString:[[NSAttributedString alloc]
                                          initWithString:[NSString stringWithFormat:NSLocalizedString(@" and %d more items", nil), ([order.lineItems count] - 10)]]];
            break;
        }
        
        if(lineItem.thumbnailURL != nil){
            if(!pic1Set){
                [cell.pic1 sd_setImageWithURL:lineItem.thumbnailURL
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                        cell.pic1.hidden = NO;
                                    }];
                pic1Set = YES;
                cell.pic1.tag = i;
            } else if(!pic2Set){
                [cell.pic2 sd_setImageWithURL:lineItem.thumbnailURL
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                        cell.pic2.hidden = NO;
                                    }];
                pic2Set = YES;
                cell.pic2.tag = i;
            } else if(!pic3Set){
                [cell.pic3 sd_setImageWithURL:lineItem.thumbnailURL
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                        cell.pic3.hidden = NO;
                                    }];
                pic3Set = YES;
                cell.pic3.tag = i;
            } else if(!pic4Set){
                [cell.pic4 sd_setImageWithURL:lineItem.thumbnailURL
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                        cell.pic4.hidden = NO;
                                    }];
                pic4Set = YES;
                cell.pic4.tag = i;
            } else if(!pic5Set){
                [cell.pic5 sd_setImageWithURL:lineItem.thumbnailURL
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                        cell.pic5.hidden = NO;
                                    }];
                pic5Set = YES;
                cell.pic5.tag = i;
            }
        }
                i++;
    }
    if(order.merchantName != nil){
        [desc appendAttributedString:[[NSAttributedString alloc] initWithString:NSLocalizedString(@" from ", nil)]];
        [desc appendAttributedString:[[NSAttributedString alloc] initWithString:order.merchantName
                                                                     attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:12.0f],
                                                                                  NSForegroundColorAttributeName: [UIColor c11Color],
                                                                                  NSLinkAttributeName: [NSURL URLWithString:[NSString stringWithFormat:@"rushorderapp://merchant/%lld?row=%ld&section=%ld", order.merchantNo, (long)indexPath.row, (long)indexPath.section]]}]];
    }
    cell.descTextView.attributedText = desc;
    
    
    if(!pic1Set && !pic2Set && !pic3Set && !pic4Set && !pic5Set){
        cell.picContainerHeightConstraint.constant = 0.0f;
        cell.picContainerTopConstraint.constant = 0.0f;
    } else {
        cell.picContainerHeightConstraint.constant = 44.0f;
        cell.picContainerTopConstraint.constant = 10.0f;
    }
    
    if (self.moredata && indexPath.row > [self.nearYouOrders count] - 2){
        [self requestNearYou];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView
             cell:(NearYouTableViewCell *)cell
didTouchImageButtonAtIndexPath:(NSIndexPath *)indexPath
    withMenuIndex:(NSInteger)menuIndex
{
    PCLog(@"Hi %ld", menuIndex);
    
    Order *order = [self.nearYouOrders objectAtIndex:indexPath.row];
    LineItem *lineItem = [order.lineItems objectAtIndex:menuIndex];
    PCSerial menuId = lineItem.menuItemNo;
    [self pushMenuPageWithOrder:order andMenuId:menuId];
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    Order *order = nil;
//    order = [self.nearYouOrders objectAtIndex:indexPath.row];
//    
//    MenuListType neoMenuListType = MenuListTypeNone;
//    
//    switch (order.orderType) {
//        case OrderTypeTakeout:
//            neoMenuListType = MenuListTypeTakeout;
//            break;
//        case OrderTypeDelivery:
//            neoMenuListType = MenuListTypeDelivery;
//            break;
//        default:
//            neoMenuListType = MenuListTypeDinein;
//            break;
//    }
//    
//    BOOL isMenuReset = [MENUPAN resetGraphWithMerchant:order.merchant
//                                              withType:neoMenuListType];
//    
//    CartType cartType = CartTypeUnknown;
//    
//    switch(order.orderType){
//        case OrderTypePickup:
//            if([order.tableNumber length] > 0){
//                cartType = CartTypePickupServiced;
//            } else {
//                cartType = CartTypePickup;
//            }
//            break;
//        case OrderTypeDelivery:
//            cartType = CartTypeDelivery;
//            break;
//        case OrderTypeTakeout:
//            cartType = CartTypeTakeout;
//            break;
//        default:
//            cartType = CartTypeCart;
//            break;
//    }
//    
//    NSMutableArray *lineItems = [NSMutableArray array];
//    
//    for(LineItem *lineItem in order.lineItems){
//        [lineItems addObject:@[@{@"name":lineItem.menuName, @"id":[NSNumber numberWithSerial:lineItem.menuItemNo]}]];
//    }
//    
//    if(isMenuReset){
//        [MENUPAN requestMenusWithSuccess:^(id response) {
//            [self pushMenuPageViewController:order cartType:cartType referredMenus:lineItems];
//        } andFail:^(NSUInteger errorCode) {
//            PCError(@"Can't continue to get menu for this order - requestMenusWithSuccess in near you");
//            [UIAlertView alertWithTitle:@"Oops! Unexpected Error"
//                                message:@"Sorry, something seems a little buggy! Please try exiting the app and restarting. Please don't hesitate to reach out to us if the problem persists."];
//        }];
//        
//    } else {
//        [self pushMenuPageViewController:order cartType:cartType referredMenus:lineItems];
//    }
//}

- (void)pushMenuPageViewController:(Merchant *)merchant
                          cartType:(CartType)cartType
                     referredMenus:(NSArray *)referredMenus
{
    MenuPageViewController *viewController = [MenuPageViewController viewControllerFromNib];
    viewController.initialCartType = cartType;
    viewController.referredMenus = referredMenus;
    [ORDER resetGraphWithMerchant:merchant];
    viewController.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (NSMutableArray *)nearYouOrders
{
    if(_nearYouOrders == nil){
        _nearYouOrders = [NSMutableArray array];
    }
    
    return _nearYouOrders;
}

- (void)scrollToTop
{
    if([self.nearYouOrders count] > 0){
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                              atScrollPosition:UITableViewScrollPositionTop
                                      animated:YES];
    }
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange
{
    return [self textView:textView shouldInteractWithURL:URL inRange:characterRange interaction:0];
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange interaction:(UITextItemInteraction)interaction
{
    PCLog(@"URL %@", URL);
    NSArray *paths = [URL pathComponents];
    PCLog(@"paths %@",paths);
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *param in [URL.query componentsSeparatedByString:@"&"]) {
        NSArray *elts = [param componentsSeparatedByString:@"="];
        if([elts count] < 2) continue;
        [params setObject:[elts lastObject] forKey:[elts firstObject]];
    }
    
    NSInteger row = [params integerForKey:@"row"];
    
    if([URL.host isEqualToString:@"menu"]){
        
        PCSerial menuId = [paths[1] integerValue];
        
        Order *order = nil;
        order = [self.nearYouOrders objectAtIndex:row];
        
        [self pushMenuPageWithOrder:order andMenuId:menuId];
        
    } else if([URL.host isEqualToString:@"merchant"]){
        [self showMerchantInfoAtRow:row];
    }
    return NO;
}

- (void)pushMenuPageWithOrder:(Order *)order andMenuId:(PCSerial)menuId
{
    MenuListType neoMenuListType = MenuListTypeNone;
    
    switch (order.orderType) {
        case OrderTypeTakeout:
            neoMenuListType = MenuListTypeTakeout;
            break;
        case OrderTypeDelivery:
            neoMenuListType = MenuListTypeDelivery;
            break;
        default:
            neoMenuListType = MenuListTypeDinein;
            break;
    }
    
    BOOL isMenuReset = [MENUPAN resetGraphWithMerchant:order.merchant
                                              withType:neoMenuListType];
    
    CartType cartType = CartTypeUnknown;
    
    switch(order.orderType){
        case OrderTypePickup:
            if([order.tableNumber length] > 0){
                cartType = CartTypePickupServiced;
            } else {
                cartType = CartTypePickup;
            }
            break;
        case OrderTypeDelivery:
            cartType = CartTypeDelivery;
            break;
        case OrderTypeTakeout:
            cartType = CartTypeTakeout;
            break;
        default:
            cartType = CartTypeCart;
            break;
    }
    
    NSMutableArray *lineItems = [NSMutableArray array];
    
    [lineItems addObject:@{@"name":@"", @"id":[NSNumber numberWithSerial:menuId]}];
    
    for(LineItem *lineItem in order.lineItems){
        if(lineItem.menuItemNo == menuId){
            if([[lineItems objectAtIndex:0] serialForKey:@"id"] == menuId){
                [lineItems replaceObjectAtIndex:0 withObject:@{@"name":lineItem.menuName, @"id":[NSNumber numberWithSerial:lineItem.menuItemNo]}];
            }
        } else {
            [lineItems addObject:@{@"name":lineItem.menuName, @"id":[NSNumber numberWithSerial:lineItem.menuItemNo]}];
        }
    }
    
    if(isMenuReset){
        [MENUPAN requestMenusWithSuccess:^(id response) {
            [self pushMenuPageViewController:order.merchant
                                    cartType:cartType
                               referredMenus:lineItems];
        } andFail:^(NSUInteger errorCode) {
            PCError(@"Can't continue to get menu for this order - requestMenusWithSuccess in near you");
            [UIAlertView alertWithTitle:@"Oops! Unexpected Error"
                                message:@"Sorry, something seems a little buggy! Please try exiting the app and restarting. Please don't hesitate to reach out to us if the problem persists."];
        }];
        
    } else {
        [self pushMenuPageViewController:order.merchant
                                cartType:cartType
                           referredMenus:lineItems];
    }
}

- (void)showMerchantInfoAtRow:(NSInteger)row
{
    Order *order = [self.nearYouOrders objectAtIndex:row];
    Merchant *merchant = order.merchant;
    if(merchant != nil){
        
        [ORDER resetGraphWithMerchant:merchant];
        [MENUPAN resetGraphWithMerchant:merchant];
        
        MerchantViewController *merchantViewController = [UIStoryboard viewController:@"MerchantViewController"
                                from:@"Restaurants"];
        
        merchantViewController.canDirectOrder = YES;
        merchantViewController.delegate = self;
        [merchantViewController.view updateConstraints];
        merchantViewController.topSpaceConstraint.constant = -64.0f;
        [merchantViewController.view setNeedsLayout];
        [merchantViewController.view layoutSubviews];
    
        
        [self presentViewControllerInNavigation:merchantViewController
                                       animated:YES
                                     completion:^{
                                        
                                     }];
        
        
#if FLURRY_ENABLED
        [Flurry logEvent:@"See Restaurant Detail(Near You)" withParameters:@{@"RestaurantName":merchant.name,
                                                                             @"RestaurantId":[NSNumber numberWithSerial:merchant.merchantNo]}];
#endif
    } else {
        PCWarning(@"Merchant should be set");
    }
}


- (void)merchantViewController:(MerchantViewController *)merchantViewController
          didTouchActionButton:(id)sender
                      merchant:(Merchant *)merchant
                  withCartType:(CartType)cartType
{
    self.selectedCartType = cartType;
    
    [merchantViewController dismissViewControllerAnimated:YES
                                               completion:^{
                                                   [self pushMenuView:merchant];
                                               }];
}

- (void)pushMenuView:(Merchant *)merchant
{
    MenuListType neoMenuListType = MenuListTypeNone;
    
    switch (self.selectedCartType) {
        case CartTypeTakeout:
            neoMenuListType = MenuListTypeTakeout;
            break;
        case CartTypeDelivery:
            neoMenuListType = MenuListTypeDelivery;
            break;
        default:
            neoMenuListType = MenuListTypeDinein;
            break;
    }
    
    BOOL isMenuReset = [MENUPAN resetGraphWithMerchant:merchant
                                              withType:neoMenuListType];
    
    
    if(isMenuReset){
        [MENUPAN requestMenusWithSuccess:^(id response) {
            [self pushMenuPageViewController:merchant
                                    cartType:self.selectedCartType
                               referredMenus:nil];
        } andFail:^(NSUInteger errorCode) {
            PCError(@"Can't continue to get menu for this order - requestMenusWithSuccess in near you");
            [UIAlertView alertWithTitle:@"Oops! Unexpected Error"
                                message:@"Sorry, something seems a little buggy! Please try exiting the app and restarting. Please don't hesitate to reach out to us if the problem persists."];
        }];
        
    } else {
        [self pushMenuPageViewController:merchant
                                cartType:self.selectedCartType
                           referredMenus:nil];
    }
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
