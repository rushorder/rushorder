//
//  MenuDetailViewController.h
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "MenuItem.h"
#import "LineItem.h"
#import "Cart.h"


@protocol MenuDetailViewControllerDelegate;

@interface MenuDetailViewController : PCViewController
<
UITableViewDelegate,
UITableViewDataSource,
UITextViewDelegate
>

@property (weak, nonatomic) id<MenuDetailViewControllerDelegate> delegate;
@property (strong, nonatomic) MenuItem *item;
@property (strong, nonatomic) LineItem *lineItem;
@property (strong, nonatomic) Cart *cart;
@property (strong, nonatomic) NSIndexPath *indexPath;

@property (nonatomic, readonly) BOOL isModifyingMode;
@property (nonatomic, readonly) BOOL canEdit;

@end

@protocol MenuDetailViewControllerDelegate <NSObject>
@optional
- (void)menuDetailViewController:(MenuDetailViewController *)viewController
                  didNewMenuItem:(MenuItem *)menuItem
                        quantity:(NSInteger)quantity
             specialInstructions:(NSString *)specialInstructions;
- (void)menuDetailViewController:(MenuDetailViewController *)viewController didUpdateLineItem:(LineItem *)lineItem;
- (void)menuDetailViewControllerDidCancelTouched:(MenuDetailViewController *)viewController;
- (void)menuDetailViewControllerDidTouchedPhoto:(MenuDetailViewController *)viewController
                                    atIndexPath:(NSIndexPath *)indexPath;
@end
