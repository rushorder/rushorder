//
//  RestaurantViewController.m
//  RushOrder
//
//  Created by Conan on 11/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "RestaurantViewController.h"
#import "PCPaymentService.h"
#import "TableSectionItem.h"
#import "TransactionManager.h"
#import "PCCredentialService.h"
#import "Merchant.h"
#import "OrderManager.h"
#import "MenuManager.h"
#import "MenuPageViewController.h"
#import "TablesetViewController.h"
#import "TogoAuthViewController.h"
#import "SettingViewController.h"
#import "BillViewController.h"
#import "RemoteDataManager.h"
#import "SignInCustomerViewController.h"
#import "Cuisine.h"
#import "LocationViewController.h"
#import <AddressBookUI/AddressBookUI.h>
#import "AddressAnnotation.h"
#import "StaticBadgeButton.h"
#import "CustomIOSAlertView.h"
#import "ServiceFeeGuideView.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "SearchResultTableViewController.h"
#import <Smooch/Smooch.h>
#import "RushOrder-Swift.h"
#import "SearchBarContainerView.h"

enum sectionTag_{
    SectionTagSuggestion = 1,
    SectionTagActiveOrder,
    SectionTagFavorite,
    SectionTagVisited,
    SectionTagNearBy,
    SectionTagUnavailable
};

@implementation RestaurantSuggestionCell


- (IBAction)removeButtonTouched:(id)sender
{
    NSIndexPath *selfIndexPath = [self.tableView indexPathForCell:self];

    if([self.delegate respondsToSelector:@selector(restaurantSuggestionCell:didTouchLiveChatButtonAtIndexPath:)]){
        [self.delegate restaurantSuggestionCell:self
           didTouchLiveChatButtonAtIndexPath:selfIndexPath];
    }
}


@end


@interface RestaurantViewController ()

@property (strong, nonatomic) NSMutableArray *sectionList;
@property (strong, nonatomic) NSArray *searchResultList;
@property (strong, nonatomic) id orderOrCart;
@property (strong, nonatomic) Merchant *selectedMerchant;

@property (nonatomic, getter = isDirty) BOOL dirty;
@property (nonatomic, getter = isProgressing) BOOL progressing;

#if OPTION1
    @property (strong, nonatomic) RestaurantCell1 *offScreenRestaurantCell;
#elif OPTION2
    @property (strong, nonatomic) RestaurantCell2 *offScreenRestaurantCell;
#endif

@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (nonatomic) CartType selectedCartType;
@property (strong, nonatomic) IBOutlet UIView *unavailable_header_view;

@property (strong, nonatomic) NSArray *selectedCuisines;
@property (copy, nonatomic) NSString *selectedCuisineNos;

@property (strong, nonatomic) IBOutlet UIView *moreLoadingView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (strong, nonatomic) IBOutlet UILabel *loadingLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomLoadingIndicatorHeightConstraint;

@property (nonatomic) NSUInteger currentPage;
@property (nonatomic, getter = isMoredata) BOOL moredata;

@property (copy, nonatomic) NSString *searchedKeyword;

//////////////////////////////////////////////////////////////////////////// RushOrder2
@property (strong, nonatomic) IBOutlet UIView *filterPanel;
@property (strong, nonatomic) UIToolbar *filterToolBar;
@property (strong, nonatomic) IBOutlet UIPickerView *cuisinePickerView;
@property (weak, nonatomic) IBOutlet UIView *segmentedControlPlaceHolderView;
@property (weak, nonatomic) IBOutlet UIView *deliveryPanel;
@property (weak, nonatomic) IBOutlet NPStretchableButton *applyButton;
@property (weak, nonatomic) IBOutlet UIImageView *shadowView;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *mapBarButtonItem;
@property (strong, nonatomic) UIBarButtonItem *resetBarButtonItem;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deliveryHeightConstraint;
@property (strong, nonatomic) IBOutlet UIToolbar *cuisineToolBar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *filterBarButtonItem;
@property (weak, nonatomic) IBOutlet UIView *priceSegmentControlPlaceholder;
@property (weak, nonatomic) IBOutlet UIView *locationSegmentControlPlaceholder;

@property (strong, nonatomic) WLHorizontalSegmentedControl *segmentControl;
@property (strong, nonatomic) WLHorizontalSegmentedControl *priceSegmentControl;
@property (strong, nonatomic) WLHorizontalSegmentedControl *locationSegmentControl;
@property (nonatomic) BOOL differentLocationSelected;
@property (weak, nonatomic) IBOutlet UIButton *openButton;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UIButton *dealsOnlyButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *cuisineButton;
@property (weak, nonatomic) IBOutlet UIButton *deliveryFeeButton;

@property (strong, nonatomic) CLLocation *filterLocation;
@property (copy, nonatomic) NSString *filterAddress;
@property (copy, nonatomic) NSString *filterAddressName;
@property (strong, nonatomic) CLLocation *currentLocation;

@property (nonatomic) BOOL didReceivedUserLocation;

@property (strong, nonatomic) NSMutableArray *restaurants;

@property (strong, nonatomic) CLGeocoder *geoCoder;
@property (weak, nonatomic) IBOutlet UIButton *locationButton;

@property (strong, nonatomic) UIButton *filterDimButton;

@property (nonatomic, getter = isPresenting) BOOL presenting;

@property (strong, nonatomic) IBOutlet UIView *noResultView;
@property (copy, nonatomic) NSString *lastRequestedAreaName;

@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) SearchResultTableViewController *resultsTableController;
@property (strong, nonatomic) IBOutlet UIButton *orangeCoinButton;
@property (nonatomic) BOOL orangeButtonHidden;

@property (nonatomic) AutoPushType autoPushType;
@property (strong, nonatomic) SearchBarContainerView *searchbarContainerView;
@end

@implementation RestaurantViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self){
        [self commonInit];
    }
    return self;
}



- (void)commonInit
{
    self.dirty = YES;
    self.currentPage = 0;
    
    self.geoCoder = [[CLGeocoder alloc] init];
    
    [self addObserver:self
           forKeyPath:@"progressing"
              options:NSKeyValueObservingOptionNew
              context:NULL];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidEnterBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillEnterForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(locationUpdated:)
                                                 name:locationUpdatedNotificationKey
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(merchantUpdated:)
                                                 name:MerchantInformationUpdatedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signedIn:)
                                                 name:SignedInNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signedOut:)
                                                 name:SignedOutNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(autoNotified:)
                                                 name:@"NeedToPreparePushingAutoViewController"
                                               object:nil];
    
    // TODO: Sign Out / In Notification and set includeMyMerchant properly
    
}

- (void)autoNotified:(NSNotification *)noti
{
    if(self.tabBarController.selectedIndex == 0){
        NSDictionary *userInfo = noti.userInfo;
        if([[userInfo objectForKey:@"type"] isEqualToString:@"invite"]){
            self.autoPushType = AutoPushTypeInvite;
        } else if([[userInfo objectForKey:@"type"] isEqualToString:@"mycredit"]){
            self.autoPushType = AutoPushTypeMycredit;
        } else {
            self.autoPushType = AutoPushTypeNone;
        }
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqual:@"progressing"]){
        self.tableView.tableFooterView = self.moreLoadingView;
        if([[change objectForKey:NSKeyValueChangeNewKey] boolValue]){
            self.loadingIndicator.hidden = NO;
            [self.loadingIndicator startAnimating];
            self.bottomLoadingIndicatorHeightConstraint.constant = 20.0f;
            self.loadingLabel.text = NSLocalizedString(@"Loading...", nil);
        } else {
            [self.loadingIndicator stopAnimating];
            self.loadingIndicator.hidden = YES;

            if(self.isMoredata){
                self.bottomLoadingIndicatorHeightConstraint.constant = 0.0f;
                self.loadingLabel.text = NSLocalizedString(@"Pull Up To Load More", nil);
            } else {
                if([self.sectionList count] > 0){
                    TableSection *tableSection = [self.sectionList objectAtIndex:0];
                    
                    if([tableSection.menus count] > 0){
//                        self.tableView.tableFooterView = nil;
                        [self.noResultView removeFromSuperview];
                    } else {
//                        self.tableView.tableFooterView = self.noResultView;
                        [self addNoResultView];
                    }
                } else {
//                    self.tableView.tableFooterView = self.noResultView;
                    [self addNoResultView];
                }
                self.bottomLoadingIndicatorHeightConstraint.constant = 0.0f;
                self.loadingLabel.text = NSLocalizedString(@"End of the List", nil);
            }

        }
    }
}

- (void)addNoResultView
{
    self.tableView.tableFooterView = nil;
    
    [self.tableView insertSubview:self.noResultView atIndex:0];
    [self.noResultView drawBorder];
    [self.tableView addConstraint:
     [NSLayoutConstraint constraintWithItem:self.noResultView
                                  attribute:NSLayoutAttributeCenterX
                                  relatedBy:NSLayoutRelationEqual
                                     toItem:self.tableView
                                  attribute:NSLayoutAttributeCenterX
                                 multiplier:1
                                   constant:0]];
    [self.tableView addConstraint:
     [NSLayoutConstraint constraintWithItem:self.noResultView
                                  attribute:NSLayoutAttributeCenterY
                                  relatedBy:NSLayoutRelationEqual
                                     toItem:self.tableView
                                  attribute:NSLayoutAttributeCenterY
                                 multiplier:1
                                   constant:0]];
}

- (void)signedIn:(NSNotification *)aNoti
{
    [self configureSection];
    [self.tableView reloadData];
//    if(self.tableView.contentOffset.y > 0.0){
//        [self goToTop];
//    }
}

- (void)signedOut:(NSNotification *)aNoti
{
    [self.tableView reloadData];
//    if(self.tableView.contentOffset.y > 0.0){
//        [self goToTop];
//    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)favoritedRestaurantsChanged:(NSNotification *)notification
{
    [self.tableView reloadData];
//    if(self.tableView.contentOffset.y > 0.0){
//        [self goToTop];
//    }
}

- (void)merchantUpdated:(NSNotification *)notification
{
    BOOL invalidMerchant = [notification.userInfo boolForKey:@"InvalidMerchant"];
    Merchant *updatedMerchant = (Merchant *)notification.object;
    
    if(updatedMerchant.isDemoMode) return;
    
    NSInteger i = 0;
    
    if(invalidMerchant){
        
        for(i = 0 ; i < [self.merchantList count] ; i++){
            Merchant *merchant = [self.merchantList objectAtIndex:i];
            if([merchant isKindOfClass:[Merchant class]] && updatedMerchant.merchantNo == merchant.merchantNo){
                break;
            }
        }
        
        if(i < [self.merchantList count]){
            [self.merchantList removeObjectAtIndex:i];
        }
        
        for(i = 0 ; i < [self.unavailableList count] ; i++){
            Merchant *merchant = [self.unavailableList objectAtIndex:i];
            if([merchant isKindOfClass:[Merchant class]] && updatedMerchant.merchantNo == merchant.merchantNo){
                break;
            }
        }
        
        if(i < [self.unavailableList count]){
            [self.unavailableList removeObjectAtIndex:i];
        }
        
    } else {
        for(i = 0 ; i < [self.merchantList count] ; i++){
            PCWarning(@"  TODO: This is very Heavy job! Fix it - later");
            Merchant *merchant = [self.merchantList objectAtIndex:i];
            if([merchant isKindOfClass:[Merchant class]] && updatedMerchant.merchantNo == merchant.merchantNo){
                PCWarning(@"  TODO: But Bingo");
                break;
            }
        }
        
        if(i < [self.merchantList count]){
            [self.merchantList replaceObjectAtIndex:i withObject:updatedMerchant];
        }
        
        for(i = 0 ; i < [self.unavailableList count] ; i++){
            PCWarning(@"  TODO: This is very Heavy job! Fix it - later");
            Merchant *merchant = [self.unavailableList objectAtIndex:i];
            if([merchant isKindOfClass:[Merchant class]] && updatedMerchant.merchantNo == merchant.merchantNo){
                PCWarning(@"  TODO: But Bingo");
                break;
            }
        }
        
        if(i < [self.unavailableList count]){
            [self.unavailableList replaceObjectAtIndex:i withObject:updatedMerchant];
        }
        
    }
    
    [self.tableView reloadData];
//    if(self.tableView.contentOffset.y > 0.0){
//        [self goToTop];
//    }
}

- (void)applicationDidEnterBackground:(NSNotification *)noti
{
    [self viewWillDisappear:NO];
//    self.dirty = YES;
    self.lastRequestedAreaName = nil;
//    self.searchDisplayController.active = NO;
}

- (void)applicationWillEnterForeground:(NSNotification *)noti
{
    [self viewWillAppear:NO];
    [APP startLocationUpdates];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.presenting = YES;
    
    if(!ORDER.shouldMoveToOrders){
        [self.tableView reloadData];
        
//        if(self.tableView.contentOffset.y > 0.0){
//            [self goToTop];
//        }
    }
}

// Default priority
// When create new of default address - not make it as default

- (void)viewDidAppear:(BOOL)animated
{
#define COIN_WIDTH 48.0f
#define COIN_HEIGHT 53.0f
#define COIN_SHOW_THREASHOLD 160.0f
    [super viewDidAppear:animated];
    
    if([NSUserDefaults standardUserDefaults].isHideCoin){
        self.orangeCoinButton.transform = CGAffineTransformIdentity;
        [self.orangeCoinButton removeFromSuperview];
    } else {
        if(self.orangeCoinButton.superview == nil){
            CGRect coinFrame = self.orangeCoinButton.frame;
            coinFrame.origin.x = self.view.superview.width - COIN_WIDTH - 10.0f;
            coinFrame.origin.y = self.navigationController.navigationBar.size.height + self.navigationController.navigationBar.origin.y + 10.0f;
            coinFrame.size = CGSizeMake(COIN_WIDTH, COIN_HEIGHT);
            self.orangeCoinButton.frame = coinFrame;
            
            [self.view.superview addSubview:self.orangeCoinButton];
            if(self.tableView.contentOffset.y > COIN_SHOW_THREASHOLD){
                self.orangeCoinButton.transform = CGAffineTransformMakeScale(0.0f, 0.0f);
                self.orangeButtonHidden = YES;
            } else {
                self.orangeCoinButton.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
                self.orangeButtonHidden = NO;
            }
        } else {
            [self.view.superview bringSubviewToFront:self.orangeCoinButton];
        }
    }
    
    if(ORDER.shouldMoveToOrders){
        [self.tabBarController setSelectedIndex:3];
        UINavigationController *tab1NavigationController = (UINavigationController *)self.tabBarController.viewControllers[3];
        [tab1NavigationController popToRootViewControllerAnimated:YES];
        ORDER.shouldMoveToOrders = NO;
        
        PCLog(@"Order Type just finished %d", ORDER.order.orderType);
        if(ORDER.order.orderType == OrderTypeDelivery && ORDER.isSetTempDelivery){
            PCLog(@"Temp Delivery Addr %@", ORDER.tempDeliveryAddress.receiverName);
            PCLog(@"Temp Delivery Addr %@", ORDER.lastUsedDeliveryAddress.name);
            
            DeliveryAddress *similarAddress = nil;
            DeliveryAddress *sameAddress = nil;
            
            for(DeliveryAddress *anAddress in REMOTE.deliveryAddresses){
                DeliveryAddressSimilarity rtn = [anAddress sameWithAddresses:ORDER.tempDeliveryAddress];
                switch(rtn){
                    case DeliveryAddressSimilaritySimilar:
                        if(similarAddress == nil || !similarAddress.isDefaultAddress){
                            similarAddress = anAddress;
                        }
                        break;
                    case DeliveryAddressSimilaritySame:
                        sameAddress = anAddress;
                        break;
                    default:
                        break;
                }
                
                if(sameAddress != nil){
                    break;
                }
            }
            
            if(sameAddress != nil){
                ORDER.lastUsedDeliveryAddress = sameAddress;
                if(!sameAddress.isDefaultAddress){
                    if(ORDER.tempDeliveryAddress.needToBeUpdatedToDefault){
                        // Update to default
                        [DeliveryAddress updateDeliveryAddress:sameAddress withAddress:nil];
                    } else {
                        // Do nothing
                    }
                } else {
                    // Do nothing!
                }
            } else if(similarAddress != nil){
                ORDER.lastUsedDeliveryAddress = similarAddress;
                // Ask to update one or create one?
                UIAlertView  *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Update saved delivery address?", nil)
                                                              message:[NSString stringWithFormat:NSLocalizedString(@"%@\nTO\n%@", nil), ORDER.lastUsedDeliveryAddress.fullAddress, ORDER.tempDeliveryAddress.fullAddress]
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"No", nil)
                                                    otherButtonTitles:NSLocalizedString(@"Yes", nil), NSLocalizedString(@"No, save as new", nil), nil];
                alertView.tag = 212;
                [alertView show];
                
            } else {
                if(ORDER.tempDeliveryAddress.needToBeUpdatedToDefault){
                    [DeliveryAddress newDeliveryAddressWithAddress:ORDER.tempDeliveryAddress];
                } else {
                    [UIAlertView askWithTitle:NSLocalizedString(@"Save this delivery address?", nil)
                                      message:nil //ORDER.tempDeliveryAddress.fullAddress
                                     delegate:self
                                          tag:211];
                }
            }
        }
    } else if(self.autoPushType != AutoPushTypeNone){
        switch(self.autoPushType){
            case AutoPushTypeInvite:
                
                if(CRED.isSignedIn){
                    UIViewController *viewController = [UIStoryboard viewController:@"shareEarnSignedIn" from:@"Settings"];
                    [self.navigationController pushViewController:viewController
                                                         animated:YES];
                } else {
                    UIViewController *viewController = [UIStoryboard viewController:@"shareEarn" from:@"Settings"];
                    [self.navigationController pushViewController:viewController
                                                         animated:YES];
                }
                
                self.autoPushType = AutoPushTypeNone;
                break;
            case AutoPushTypeMycredit:
                
                if(CRED.isSignedIn){
                    UIViewController *viewController = [UIStoryboard viewController:@"creditRewardSignedIn" from:@"Settings"];
                    [self.navigationController pushViewController:viewController
                                                         animated:YES];
                } else {
                    UIViewController *viewController = [UIStoryboard viewController:@"creditReward" from:@"Settings"];
                    [self.navigationController pushViewController:viewController
                                                         animated:YES];
                }
                
                self.autoPushType = AutoPushTypeNone;
                break;
            default:
                break;
        }
    
    } else {
        [TRANS requestMyOrders]; //For refreshing local cart when existing from the ordering status
    }
}


- (void)setBadgeOn:(BOOL)badgeOn
{
    StaticBadgeButton *badgeButton = (StaticBadgeButton *)self.filterBarButtonItem.button;
    badgeButton.badgeOn =  badgeOn;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.presenting = NO;
//    self.orangeCoinButton.transform = CGAffineTransformIdentity;
//    [self.orangeCoinButton removeFromSuperview];
}

- (void)locationUpdated:(NSNotification *)noti
{
    if(noti.userInfo == nil){
        
    } else {
        CLLocation *location = [noti.userInfo objectForKey:@"location"];
        self.currentLocation = location;
        
        if(self.filterLocation == nil){
            self.filterLocation = location;
            [self showAddressNameBasedOnFilterLocation];
        }
    }
    
    if(self.isDirty){
        [self startFiltering];
    }
}

- (void)showAddressNameBasedOnFilterLocation
{
    if(self.filterLocation == nil){
        self.filterAddress = NSLocalizedString(@"Unknown", nil);;
        self.filterAddressName = NSLocalizedString(@"Unknown", nil);;
        self.locationButton.buttonTitle = self.filterAddressName;
        return;
    }
    
    [self.geoCoder reverseGeocodeLocation:self.filterLocation
                        completionHandler:^(NSArray *placemarks, NSError *error) {
                            if(error != nil){
                                PCError(@"Geo Coder Error : %@", error);
                                self.filterAddress = NSLocalizedString(@"Unknown", nil);;
                                self.filterAddressName = NSLocalizedString(@"Unknown", nil);;
                                self.locationButton.buttonTitle = self.filterAddressName;
                                return;
                            }
                            
                            if ((placemarks.count > 0)) {
                                CLPlacemark *placemark = [placemarks objectAtIndex:0];
                                self.filterAddress = placemark.address;
                                self.filterAddressName = [placemark.addressDictionary objectForKey:@"Name"];
                                PCLog(@"Geo Address %@", self.filterAddress);
                                
                                self.locationButton.buttonTitle = self.filterAddressName;
                            }
                            else {
                                // Handle the nil case if necessary.
                            }
                        }];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.navigationController.navigationBar.prefersLargeTitles = NO;
//    self.navigationController.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeNever;
    
    self.noResultView.translatesAutoresizingMaskIntoConstraints = NO;
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                        init];

    [refreshControl addTarget:self
                       action:@selector(refreshControlChanged:)
             forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
 
    
    self.tableView.tableFooterView = self.moreLoadingView;
    
    [self.navigationItem setRightBarButtonItem:self.filterBarButtonItem
                                       animated:NO];
    self.filterBarButtonItem.button.buttonTitle = NSLocalizedString(@"Filter", nil);

    
    self.segmentControl = [[WLHorizontalSegmentedControl alloc] initWithItems:@[@"Dine-in", @"Take-out", @"Delivery"]];
    self.segmentControl.frame = self.segmentedControlPlaceHolderView.bounds;
    self.segmentControl.allowsMultiSelection = YES;
    self.segmentControl.tintColor = [UIColor c11Color];
    self.segmentControl.selectedSegmentIndice = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 0)];
    [self.segmentControl addTarget:self
                            action:@selector(segmentedChange:)
                  forControlEvents:UIControlEventValueChanged];
    [self.segmentedControlPlaceHolderView addSubview:self.segmentControl];
    self.segmentControl.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.segmentedControlPlaceHolderView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[segmentControl]|"
                                                                                                 options:0
                                                                                                 metrics:nil
                                                                                                   views:@{@"segmentControl":self.segmentControl}]];
    [self.segmentedControlPlaceHolderView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[segmentControl]|"
                                                                                                 options:0
                                                                                                 metrics:nil
                                                                                                   views:@{@"segmentControl":self.segmentControl}]];
    
    
    self.priceSegmentControl = [[WLHorizontalSegmentedControl alloc] initWithItems:@[@"$", @"$$", @"$$$", @"$$$$"]];
    self.priceSegmentControl.frame = self.priceSegmentControlPlaceholder.bounds;
    self.priceSegmentControl.allowsMultiSelection = YES;
    self.priceSegmentControl.tintColor = [UIColor c11Color];
    self.priceSegmentControl.selectedSegmentIndice = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 0)];
    self.priceSegmentControl.translatesAutoresizingMaskIntoConstraints = NO;
    [self.priceSegmentControl addTarget:self
                                 action:@selector(priceSegmentedChange:)
                       forControlEvents:UIControlEventValueChanged];
    [self.priceSegmentControlPlaceholder addSubview:self.priceSegmentControl];
    [self.priceSegmentControlPlaceholder addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[priceSegmentControl]|"
                                                                                                 options:0
                                                                                                 metrics:nil
                                                                                                   views:@{@"priceSegmentControl":self.priceSegmentControl}]];
    [self.priceSegmentControlPlaceholder addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[priceSegmentControl]|"
                                                                                                 options:0
                                                                                                 metrics:nil
                                                                                                   views:@{@"priceSegmentControl":self.priceSegmentControl}]];
    
    
    self.locationSegmentControl = [[WLHorizontalSegmentedControl alloc] initWithItems:@[@"Current Location", @"Different Location"]];
    self.locationSegmentControl.frame = self.locationSegmentControlPlaceholder.bounds;
    self.locationSegmentControl.allowsMultiSelection = NO;
    self.locationSegmentControl.tintColor = [UIColor c11Color];
    self.locationSegmentControl.selectedSegmentIndex = 0;
    //    self.locationSegmentControl.
    [self.locationSegmentControl addTarget:self
                                    action:@selector(locationSegmentedChange:)
                          forControlEvents:UIControlEventValueChanged];
    
    self.locationSegmentControl.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.locationSegmentControlPlaceholder addSubview:self.locationSegmentControl];
    
    [self.locationSegmentControlPlaceholder addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[locationSegmentControl]|"
                                                                                                options:0
                                                                                                metrics:nil
                                                                                                  views:@{@"locationSegmentControl":self.locationSegmentControl}]];
    [self.locationSegmentControlPlaceholder addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[locationSegmentControl]|"
                                                                                                options:0
                                                                                                metrics:nil
                                                                                                  views:@{@"locationSegmentControl":self.locationSegmentControl}]];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
//    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_navigation_rushorder"]];

    [self.filterPanel removeFromSuperview];
    self.tableView.tableFooterView = nil;
    self.tableView.tableHeaderView = nil;
    
//    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(34.031383, -118.439284);
//    MKCoordinateSpan span = {0.01, 0.01};    MKCoordinateRegion region = {coord, span};
//    
//    [self.mapView setRegion:region];

//    self.resultsTableController.tableView.rowHeight = 164.0f;
//    self.searchDisplayController.searchResultsTitle = @"Search Result";

    //////////////////////////////////////////////////////////////////////
    _resultsTableController = [[SearchResultTableViewController alloc] init];
    _searchController = [[UISearchController alloc] initWithSearchResultsController:self.resultsTableController];
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.delegate = self;
    self.searchController.obscuresBackgroundDuringPresentation = NO;
    self.searchController.searchBar.placeholder = @"Restaurant, Food, Area";
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTintColor:[UIColor defaultTintColor]];
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.searchController.dimsBackgroundDuringPresentation = YES;
    self.searchController.searchBar.barTintColor = [UIColor defaultTintColor];
    self.searchController.searchBar.backgroundImage = [UIImage alloc]; //Don't remove - this code is preventing the black background symptom.
    
    if (@available(iOS 11.0, *)) {
//        [self.searchController.searchBar.heightAnchor constraintEqualToConstant:44.].active = YES;
        self.searchbarContainerView = [[SearchBarContainerView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 44.)];
        [self.searchbarContainerView addSubview:self.searchController.searchBar];
        self.searchbarContainerView.searchBar = self.searchController.searchBar;
        self.navigationItem.titleView = self.searchbarContainerView;
    } else {
        self.navigationItem.titleView = self.searchController.searchBar;
    }
    
    
    

    self.definesPresentationContext = YES;
    
    self.resultsTableController.tableView.delegate = self;
    self.resultsTableController.tableView.dataSource = self;
    self.searchController.delegate = self;
    
    //////////////////////////////////////////////////////////////////////
    
    self.deliveryPanel.alpha = 0.0f;
    self.deliveryPanel.hidden = YES;
    
    [self relayoutFilterPanelWithDeliveryShow:NO];
    
    [self showAddressNameBasedOnFilterLocation];
}

- (void)willPresentSearchController:(UISearchController *)searchController
{
//    [self.navigationItem setLeftBarButtonItem:nil
//                                     animated:YES];
//    [self.navigationItem setRightBarButtonItem:nil
//                                      animated:YES];
//    [self.resultsTableController.tableView setContentInset:UIEdgeInsetsZero];
    
    [UIView transitionWithView:self.navigationController.navigationBar
                      duration:0.3f
                       options:0
                    animations:^{
//                           self.navigationItem.leftBarButtonItem = nil;
                           [self.navigationItem setLeftBarButtonItem:nil
                                                            animated:YES];
                           [self.navigationItem setRightBarButtonItem:nil
                                                            animated:YES];
                           if(@available(iOS 11.0, *)){
                               CGRect frame = self.searchbarContainerView.frame;
                               frame.size.width = self.navigationController.navigationBar.width;
                               self.searchbarContainerView.frame = frame;
                           }
                       } completion:NULL];
    
//    searchController.searchBar.text = self.searchedKeyword;
}
- (void)didPresentSearchController:(UISearchController *)searchController
{
//    [self.resultsTableController.tableView setContentInset:UIEdgeInsetsZero];
    searchController.searchBar.text = self.searchedKeyword;
}
- (void)willDismissSearchController:(UISearchController *)searchController
{
    [UIView transitionWithView:self.navigationController.navigationBar
                      duration:0.3f
                       options:UIViewAnimationOptionLayoutSubviews animations:^{
                           [self.navigationItem setLeftBarButtonItem:self.mapBarButtonItem
                                                            animated:YES];
                           [self.navigationItem setRightBarButtonItem:self.filterBarButtonItem
                                                            animated:YES];
                       } completion:NULL];
}
- (void)didDismissSearchController:(UISearchController *)searchController
{
//    [self.resultsTableController.tableView setContentInset:UIEdgeInsetsZero];
}

// Called when the search bar's text or scope has changed or when the search bar becomes first responder.
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
//    PCLog(@"Hello");
}


- (void)segmentedChange:(WLHorizontalSegmentedControl *)sender
{
    if([sender.selectedSegmentIndice containsIndex:2]
       && [sender.selectedSegmentIndice count] == 1){
        NSLog(@"Delivery Only");
        if(self.deliveryPanel.hidden){
            //Show
            self.deliveryPanel.hidden = NO;
            self.deliveryPanel.alpha = 0.0f;
            [UIView animateWithDuration:0.3f
                                  delay:0.0f
                 usingSpringWithDamping:0.9
                  initialSpringVelocity:0.1
                                options:0
                             animations:^{
                                 self.deliveryPanel.alpha = 1.0f;
                                 
                                 [self relayoutFilterPanelWithDeliveryShow:YES];
                                 
                             } completion:^(BOOL finished) {
                                 
                             }];
        }
    } else {
        if(!self.deliveryPanel.hidden){
            [UIView animateWithDuration:0.3f
                                  delay:0.0f
                 usingSpringWithDamping:0.9
                  initialSpringVelocity:0.1
                                options:0
                             animations:^{
                                 self.deliveryPanel.alpha = 0.0f;
                                 
                                 [self relayoutFilterPanelWithDeliveryShow:NO];
                                 
                             } completion:^(BOOL finished) {
                                 self.deliveryPanel.hidden = YES;
                             }];
        }
    }
}

- (void)relayoutFilterPanelWithDeliveryShow:(BOOL)isDeliveryShow
{
    if(isDeliveryShow){
        
        self.deliveryHeightConstraint.constant = 93.0f;
        
    } else {
        self.deliveryHeightConstraint.constant = 0.0f;
    }
    
    CGRect frame = self.filterPanel.frame;
    frame.size.height = [self.filterPanel systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    self.filterPanel.frame = frame;
    
    CGRect toolBarFrame = self.filterToolBar.frame;
    toolBarFrame.size.width = frame.size.width;
    toolBarFrame.size.height = frame.size.height;
    self.filterToolBar.frame = toolBarFrame;
    
    [self.filterToolBar updateConstraints];
    [self.filterToolBar layoutIfNeeded];
}

- (void)priceSegmentedChange:(WLHorizontalSegmentedControl *)sender
{
    
}

- (void)locationSegmentedChange:(WLHorizontalSegmentedControl *)sender
{
    switch(sender.selectedSegmentIndex){
        case 0: //Current Location
            self.differentLocationSelected = NO;
            self.filterLocation = APP.location;
            [self showAddressNameBasedOnFilterLocation];
            break;
        case 1: //Different Location
            if(!self.differentLocationSelected){
                self.differentLocationSelected = YES;
                [self performSegueWithIdentifier:@"LocationSegue"
                                          sender:sender];
            }
            break;
    }
}


- (void)resetTouched:(id)sender
{
    self.dealsOnlyButton.selected = NO;
    self.openButton.selected = NO;
    self.favoriteButton.selected = NO;
    self.deliveryFeeButton.selected = NO;
    
    // Set Cuisine
    for(Cuisine *cuisine in APP.cuisines){
        if(cuisine.no == 0)
            cuisine.selected = YES;
        else
            cuisine.selected = NO;
    }
    self.selectedCuisineNos = nil;
    self.cuisineButton.buttonTitle = NSLocalizedString(@"All Cuisines", nil);
    
    self.priceSegmentControl.selectedSegmentIndice = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 0)];
    self.segmentControl.selectedSegmentIndice = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 0)];
    self.locationSegmentControl.selectedSegmentIndice = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 0)];
    
    [self segmentedChange:self.segmentControl];
}

- (void)refreshControlChanged:(UIRefreshControl *)refreshControl
{
    self.currentPage = 0;
    [self startFiltering];
}

- (void)goToTop
{
    [self.tableView setContentOffset:CGPointMake(0.0f, 0.0f)
                            animated:YES];
}

- (void)goToTopOfSearchController
{
//    [self.resultsTableController.tableView setContentOffset:CGPointMake(0.0f, 0.0f)
//                                                   animated:YES];
}


- (IBAction)filterButtonTouched:(id)sender
{
    if(self.filterToolBar.superview != nil){
        [self closeFilterButton:sender];
        return;
    }
    
    CGRect visibleFrame = self.filterToolBar.frame;
    
    visibleFrame.origin.y = -visibleFrame.size.height;
    self.filterToolBar.frame = visibleFrame;
    [self.view.superview addSubview:self.filterToolBar];
    self.filterDimButton.alpha = 0.0f;
    [self.view.superview insertSubview:self.filterDimButton
                          belowSubview:self.filterToolBar];
    
    [UIView animateWithDuration:0.3f
                          delay:0.0f
         usingSpringWithDamping:0.9
          initialSpringVelocity:0.1
                        options:0
                     animations:^{
                         
                         self.favoriteButton.hidden = !CRED.isSignedIn;
                         self.favoriteButton.selected = NO;
                         
                         self.filterDimButton.alpha = 0.5f;
                         
                         CGRect visibleFrame = self.filterToolBar.frame;
                         visibleFrame.origin.y = CGRectGetMaxY(self.navigationController.navigationBar.frame);
                         self.filterToolBar.frame = visibleFrame;
                         
                         self.searchController.searchBar.alpha = 0.0f;
                         
                     } completion:^(BOOL finished) {

                     }];
    
    [self.navigationItem setLeftBarButtonItem:self.resetBarButtonItem
                                     animated:YES];
    [self.navigationItem setRightBarButtonItem:self.filterBarButtonItem
                                       animated:YES];
    self.filterBarButtonItem.button.buttonTitle = NSLocalizedString(@"Close", nil);

}

- (UIToolbar *)filterToolBar
{
    if(_filterToolBar == nil){
        _filterToolBar = [[UIToolbar alloc] initWithFrame:self.filterPanel.bounds];
        [_filterToolBar layoutIfNeeded];
        _filterToolBar.barStyle = UIBarStyleDefault;
        _filterToolBar.clipsToBounds = NO;
        [_filterToolBar addSubview:self.filterPanel];
        
        
        NSDictionary *dict = @{@"filterPanel":self.filterPanel};
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[filterPanel]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:dict];
        [_filterToolBar addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[filterPanel]|"
                                                              options:0
                                                              metrics:nil
                                                                views:dict];
        [_filterToolBar addConstraints:constraints];
        
        [_filterToolBar setContentHuggingPriority:UILayoutPriorityDefaultHigh
                                          forAxis:UILayoutConstraintAxisVertical];
        [_filterToolBar setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh
                                                        forAxis:UILayoutConstraintAxisVertical];
        
    }
    return _filterToolBar;
}

- (IBAction)closeFilterButton:(id)sender
{
    if(_filterToolBar != nil){

        [UIView animateWithDuration:0.3f
                              delay:0.0f
             usingSpringWithDamping:0.9
              initialSpringVelocity:0.1
                            options:0
                         animations:^{
                             
                             CGRect visibleFrame = self.filterToolBar.frame;
                             visibleFrame.origin.y = -visibleFrame.size.height;
                             self.filterToolBar.frame = visibleFrame;
                             
                             self.filterDimButton.alpha = 0.0f;
                             self.searchController.searchBar.alpha = 1.0f;
                             
                         } completion:^(BOOL finished) {
                             
                             [self.filterToolBar removeFromSuperview];
                             [self.filterDimButton removeFromSuperview];
                         
                         }];
        
        [self.navigationItem setLeftBarButtonItem:self.mapBarButtonItem
                                         animated:YES];
        [self.navigationItem setRightBarButtonItem:self.filterBarButtonItem
                                           animated:YES];
        self.filterBarButtonItem.button.buttonTitle = NSLocalizedString(@"Filter", nil);
    }
}

- (UIBarButtonItem *)resetBarButtonItem
{
    if(_resetBarButtonItem == nil){
        _resetBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Reset"
                                                               style:UIBarButtonItemStylePlain
                                                              target:self
                                                              action:@selector(resetTouched:)];
    }
    return _resetBarButtonItem;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == self.resultsTableController.tableView){
        return 1;
    } else {
        return [self.sectionList count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == self.resultsTableController.tableView){
        return [self.searchResultList count];
    } else {
        TableSection *tableSection = [self.sectionList objectAtIndex:section];
        return [tableSection.menus count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Merchant *merchant = nil;
    
    if(tableView == self.resultsTableController.tableView){
        merchant = [self.searchResultList objectAtIndex:indexPath.row];
    } else {
        
        TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
        merchant = [tableSection.menus objectAtIndex:indexPath.row];
        
        if(![merchant isKindOfClass:[Merchant class]]){
            merchant = nil;
        }
    
    }

    if(merchant != nil && merchant.cellHeight == FLT_MAX){
        self.offScreenRestaurantCell.bounds = CGRectMake(0.0f, 0.0f,
                                                         CGRectGetWidth(tableView.bounds),
                                                         CGRectGetHeight(self.offScreenRestaurantCell.bounds));
        [self.offScreenRestaurantCell updateConstraints];
        
        self.offScreenRestaurantCell.merchant = merchant;
        [self.offScreenRestaurantCell drawDataForHeight:YES];
        
        self.offScreenRestaurantCell.nameLabel.preferredMaxLayoutWidth = self.offScreenRestaurantCell.nameLabel.width;
        
        [self.offScreenRestaurantCell setNeedsUpdateConstraints];
        [self.offScreenRestaurantCell updateConstraintsIfNeeded];
        [self.offScreenRestaurantCell setNeedsLayout];
        [self.offScreenRestaurantCell layoutIfNeeded];
        
        merchant.cellHeight = [self.offScreenRestaurantCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height + 1.0f;
        
    } else {
        
    }
    
    if(merchant != nil){
        return merchant.cellHeight;
    } else {
        return 100.0f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.resultsTableController.tableView){
        Merchant *merchant = [self.searchResultList objectAtIndex:indexPath.row];
        
        return [self tableView:tableView
         cellForRowAtIndexPath:indexPath
                      merchant:merchant];
    } else {
    
        TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
        
        id obj = [tableSection.menus objectAtIndex:indexPath.row];
        
        if([obj isKindOfClass:[NSNumber class]]){
            RestaurantSuggestionCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"restaurantSuggestionCell"];
            cell.delegate = self;
            return cell;
        } else {
            return [self tableView:tableView
             cellForRowAtIndexPath:indexPath
                          merchant:obj];
        }
    
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
{
    if(tableView == self.resultsTableController.tableView){
        return nil;
    } else {
        TableSection *tableSection = [self.sectionList objectAtIndex:section];
        if(tableSection.tag == SectionTagUnavailable){
            return self.unavailable_header_view;
        } else {
            return nil;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == self.resultsTableController.tableView){
        return 0.f;
    } else {
        TableSection *tableSection = [self.sectionList objectAtIndex:section];
        if(tableSection.tag == SectionTagUnavailable){
            return 75.f;
        } else {
            return 0.f;
        }
    }
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    TableSection *tableSection = [self.sectionList objectAtIndex:section];
//    if(tableSection.tag == SectionTagUnavailable){
//        return tableSection.title;
//    } else {
//        return nil;
//    }
//}

- (void)restaurantSuggestionCell:(RestaurantSuggestionCell *)cell
didTouchLiveChatButtonAtIndexPath:(NSIndexPath *)indexPath
{
    if(CRED.isSignedIn){
        [SKTUser currentUser].firstName = CRED.customerInfo.firstName;
        [SKTUser currentUser].lastName = CRED.customerInfo.lastName;
        [SKTUser currentUser].email = CRED.customerInfo.email;
        [[SKTUser currentUser] addProperties:@{ @"Customer" : [NSString stringWithFormat:@"https://www.rushorderapp.com/customers/customers/%lld", CRED.customerInfo.customerNo]
                                                , @"phoneNumber" : CRED.customerInfo.phoneNumber ? CRED.customerInfo.phoneNumber : @"Unknown"
                                                , @"birthday" : CRED.customerInfo.birthday ? CRED.customerInfo.birthday : @"Unknown"
                                                , @"UUID": [NSString stringWithFormat:@"https://www.rushorderapp.com/douglas/guests/%@", CRED.udid]
                                                }];
        
    } else {
        
        [SKTUser currentUser].firstName = [NSUserDefaults standardUserDefaults].custName ? [NSUserDefaults standardUserDefaults].custName : @"?";
        [SKTUser currentUser].lastName = @"Guest";
        [SKTUser currentUser].email = [NSUserDefaults standardUserDefaults].emailAddress;
        [[SKTUser currentUser] addProperties:@{ @"phoneNumber" : [NSUserDefaults standardUserDefaults].custPhone ? [NSUserDefaults standardUserDefaults].custPhone : @"Unknown"
                                                , @"UUID": [NSString stringWithFormat:@"https://www.rushorderapp.com/douglas/guests/%@", CRED.udid]
                                                }];
    }
    
#if FLURRY_ENABLED
    [Flurry logEvent:@"Tapped Rest. Suggestion" withParameters:@{@"Type":CRED.isSignedIn ? @"Customer" : @"Guest"}];
#endif
    
    [Smooch show];
}

#define SECTION_FOOTER_HEIGHT 0.0f

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
                      merchant:(Merchant *)merchant
{
#if OPTION1
    NSString *cellIdentifier = @"RestaurantCell1";
    RestaurantCell1 *cell = nil;
#elif OPTION2
    NSString *cellIdentifier = @"RestaurantCell2";
    RestaurantCell2 *cell = nil;
#endif
    
    if(tableView == self.tableView){
        cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                               forIndexPath:indexPath];
    } else {
        cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    }
    
    if([self.segmentControl.selectedSegmentIndice containsIndex:2]
       && [self.segmentControl.selectedSegmentIndice count] == 1){
        cell.showDeliveryDistance = YES;
    } else {
        cell.showDeliveryDistance = NO;
    }
    
    if(![merchant isKindOfClass:[Merchant class]]){
        PCLog(@"Not Merchant");
    }
    
    cell.merchant = merchant;
    cell.delegate = self;
    [cell drawData];
    
    if(tableView == self.resultsTableController.tableView){
        if(indexPath.row == (self.searchResultList.count - 1)){
            cell.bottomLineView.backgroundColor = [UIColor colorWithWhite:0.9f alpha:1.0f];
        } else {
            cell.bottomLineView.backgroundColor = [UIColor colorWithWhite:0.55f alpha:1.0f];
        }
    } else {
        TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
//        PCLog(@"Row : %d / last index : %d", indexPath.row, (tableSection.menus.count - 1));
        if(indexPath.row == (tableSection.menus.count)){
            cell.bottomLineView.backgroundColor = [UIColor colorWithWhite:0.9f alpha:1.0f];
        } else {
            cell.bottomLineView.backgroundColor = [UIColor colorWithWhite:0.55f alpha:1.0f];
        }
    }
    
    
    [cell.contentView setNeedsUpdateConstraints];
    [cell.contentView updateConstraintsIfNeeded];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Merchant *merchant = nil;
    
    if(tableView == self.resultsTableController.tableView){
        merchant = [self.searchResultList objectAtIndex:indexPath.row];
        [self.resultsTableController.tableView deselectRowAtIndexPath:indexPath
                                                                           animated:YES];
    } else {
        TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
        id obj = [tableSection.menus objectAtIndex:indexPath.row];
        
        if([obj isKindOfClass:[Merchant class]]){
            merchant = (Merchant *)obj;
        }
    }
    
    if(merchant != nil){
        [self showMerchantInfo:merchant];
    }
}

- (void)showMerchantInfo:(Merchant *)merchant
{
    if(merchant != nil){
        
        [ORDER resetGraphWithMerchant:merchant];
        [MENUPAN resetGraphWithMerchant:merchant];
        
        [self performSegueWithIdentifier:@"restaurantInfoModalSegue"
                                  sender:merchant];
        
#if FLURRY_ENABLED
        [Flurry logEvent:@"See Restaurant Detail" withParameters:@{@"RestaurantName":merchant.name,
                                                                   @"RestaurantId":[NSNumber numberWithSerial:merchant.merchantNo]}];
#endif
    } else {
        PCWarning(@"Merchant should be set");
    }
}

- (NSMutableArray *)sectionList
{
    if(_sectionList == nil){
        _sectionList = [NSMutableArray array];
    }
    return _sectionList;
}

- (void)configureSection
{
    self.sectionList = nil;
    
    TableSection *section = nil;
    
    if([self.merchantList count] > 0){
        section = [[TableSection alloc] init];
        section.title = NSLocalizedString(@"Nearby", nil);
        section.menus = self.merchantList;
        section.tag = SectionTagNearBy;
        
        [self.sectionList addObject:section];
    }
    
    if([self.unavailableList count] > 0){
        section = [[TableSection alloc] init];
        section.title = NSLocalizedString(@"Unavailable", nil);
        section.menus = self.unavailableList;
        section.tag = SectionTagUnavailable;
        
        [self.sectionList addObject:section];
    }
}

- (IBAction)orangeCoinButtonTouched:(id)sender
{
     if(CRED.isSignedIn){
         UIViewController *viewController = [UIStoryboard viewController:@"shareEarnSignedIn" from:@"Settings"];
         [self.navigationController pushViewController:viewController
                                              animated:YES];
     } else {
         UIViewController *viewController = [UIStoryboard viewController:@"shareEarn" from:@"Settings"];
         [self.navigationController pushViewController:viewController
                                              animated:YES];
     }
     
//    SettingViewController *viewController = (SettingViewController *)[self.tabBarController.viewControllers[4] rootViewController];
//    viewController.showShareAndEarn = YES;
//    [viewController.navigationController popToRootViewControllerAnimated:NO];
//    [self.tabBarController setSelectedIndex:4];
}
    
- (void)requestMerchantList:(NSString *)keyword
                   location:(CLLocation *)myLocation
{
    if(self.isProgressing) return;
    
    self.progressing = YES;
    
    RequestResult result = RRFail;
    
//    PCLog(@"My updated location %@", myLocation);
    
    result = [PAY requestMerchants:keyword
                   currentLocation:myLocation
                              page:(self.currentPage + 1)
                   completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  BOOL goToTopAfterEndRefreshing = NO;
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      
                      self.dirty = NO;

                      NSMutableArray *nearByList = [response objectForKey:@"nearby"];
                      NSMutableArray *unavailableList = [response objectForKey:@"unavailable"];
                      NSNumber *returnCount = [response objectForKey:@"resultCount"];
                      
                      
                      if([keyword length] > 0){
                          
                          self.searchResultList = nearByList;
                          [self.resultsTableController.tableView reloadData];
                          
                          if(self.resultsTableController.tableView.contentOffset.y < 0.0f){
                              [self goToTopOfSearchController];
                          }
                          
                      } else {
                          if(self.currentPage == 0){
                              self.merchantList = nearByList;
                              self.unavailableList = unavailableList;
                          } else {
                              [self.merchantList addObjectsFromArray:nearByList];
                              [self.unavailableList addObjectsFromArray:unavailableList];
                          }
                          
                          if([returnCount integerValue] < RUSHORDER_PAGE_COUNT){
                              self.moredata = NO;
                          } else {
                              self.moredata = YES;
                              if(self.currentPage == 3){
                                  if([self.unavailableList count] > 0){
                                      [self.unavailableList addObject:[NSNumber numberWithBool:YES]];
                                  } else if([self.merchantList count] > 0){
                                      [self.merchantList addObject:[NSNumber numberWithBool:YES]];
                                  }
#if FLURRY_ENABLED
                                  [Flurry logEvent:@"Showed Rest. Suggestion" withParameters:@{@"Type":@"After page Number 4 - Searching"}];
#endif
                              }
                          }
                          self.currentPage++;
                          
                          [self configureSection];
                          
                          if([self.merchantList count] == 0 && [self.unavailableList count] == 0){
//                              self.tableView.tableFooterView = self.noResultView;
//                              [self addNoResultView];
                              // TODO: Added No Result View here for search result
                          }
                          
                          [self.tableView reloadData];
                          if(self.tableView.contentOffset.y < 0.0f){
                              goToTopAfterEndRefreshing = YES;
                          }
                      }
                      
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  self.progressing = NO;
                  
                  [CATransaction begin];
                  [CATransaction setCompletionBlock:^{
                      // reload tableView after refresh control finish refresh animation
                      if(goToTopAfterEndRefreshing){
                          [self goToTopOfSearchController];
                      }
                  }];
                  
                  [self.refreshControl endRefreshing];
                  [CATransaction commit];
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            self.progressing = NO;
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            [self.refreshControl endRefreshing];
            break;
        default:
            self.progressing = NO;
            [SVProgressHUD showErrorWithStatus:NETWORK_ERROR_MESSAGE];
            [self.refreshControl endRefreshing];
            break;
    }
}

- (void)scrollToTop
{
    [self.tableView setContentOffset:CGPointZero
                            animated:YES];
}

- (void)requestMerchantListWithFilter:(NSDictionary *)filter
                             location:(CLLocation *)myLocation
{
    if(self.isProgressing) return;
    
    self.progressing = YES;
    
    
    RequestResult result = RRFail;
    
#if FLURRY_ENABLED
    if(self.currentPage == 0){
        NSMutableDictionary *filteredFilter = [NSMutableDictionary dictionaryWithDictionary:filter];
        if([filteredFilter objectForKey:@"auth_token"] != nil){
            [filteredFilter removeObjectForKey:@"auth_token"];
        }
        if([[filteredFilter allKeys] count] > 0){
            [Flurry logEvent:@"Filtering Restaurants" withParameters:filteredFilter];
        } else {
            [Flurry logEvent:@"Listing Restaurants"];
        }
    }
#endif
    
    if(self.currentPage == 0){
        NSMutableDictionary *filteredFilter = [NSMutableDictionary dictionaryWithDictionary:filter];
        if([filteredFilter objectForKey:@"auth_token"] != nil){
            [filteredFilter removeObjectForKey:@"auth_token"];
        }
        if([[filteredFilter allKeys] count] > 0){
#if FBAPPEVENT_ENABLED
            [FBSDKAppEvents logEvent:FBSDKAppEventNameSearched
                          parameters:@{ FBSDKAppEventParameterNameSearchString  : filteredFilter.JSONString }];
#endif
        }
    }
    
//    [FBSDKAppEvents log:(self.payment.payAmount / 100.0f)
//                    currency:@"USD"
//                  parameters:@{ FBSDKAppEventParameterNameCurrency    : @"USD",
//                                FBSDKAppEventParameterNameContentType : @"food",
//                                FBSDKAppEventParameterNameDescription : [NSUserDefaults standardUserDefaults].deviceToken}];
    
    
    result = [PAY requestMerchantsWithFilter:filter
                             currentLocation:myLocation
                                        page:(self.currentPage + 1)
                             completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  BOOL goToTopAfterEndRefreshing = NO;
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      
                      self.dirty = NO;
                      
                      NSMutableArray *nearByList = [response objectForKey:@"nearby"];
                      NSMutableArray *unavailableList = [response objectForKey:@"unavailable"];
                      NSNumber *returnCount = [response objectForKey:@"resultCount"];
                      NSString *detectedAreaName = [response objectForKey:@"detected_area_name"];

                      NSNumber *isOutOfServiceArea = [response objectForKey:@"out_of_service_area"];
#if DEBUG
                      isOutOfServiceArea = [NSNumber numberWithBool:NO];
#endif
                      if([isOutOfServiceArea boolValue] && APP.location != nil){
                          if(![detectedAreaName isEqualToString:self.lastRequestedAreaName]){
                              if(self.presentedViewController == nil) {
                                  OOAViewController *viewController = [OOAViewController viewControllerFromNib];
                                  viewController.delegate = self;
                                  [self presentViewControllerInNavigation:viewController
                                                                 animated:YES
                                                               completion:^{
                                                                   //Nothing
                                                               }];
                              } else {
                                  
                                  if(![self.presentedViewController isKindOfClass:[UINavigationController class]] ||
                                     ![[((UINavigationController *)self.presentedViewController) rootViewController] isKindOfClass:[OOAViewController class]]){
                                        [self dismissViewControllerAnimated:YES
                                                                 completion:^{
                                                                     
                                                                     
                                                                     
                                                                     OOAViewController *viewController = [OOAViewController viewControllerFromNib];
                                                                     viewController.delegate = self;
                                                                     [self presentViewControllerInNavigation:viewController
                                                                                                    animated:YES
                                                                                                  completion:^{
                                                                                                      //Nothing
                                                                                                  }];
                                                                     
                                                                 }];
                                  } else {

                                  }
                              }
                          }
                      } else {
                          if([self.presentedViewController isKindOfClass:[UINavigationController class]] &&
                             [[((UINavigationController *)self.presentedViewController) rootViewController] isKindOfClass:[OOAViewController class]]){
                              [self dismissViewControllerAnimated:YES
                                                       completion:^{

                                                       }];
                          }
                      }

                      
                      self.lastRequestedAreaName = detectedAreaName;
                      
                      if(self.currentPage == 0){
                          self.merchantList = nearByList;
                          self.unavailableList = unavailableList;
                          
#if FLURRY_ENABLED
                          if([[filter allKeys] count] > 0 && returnCount != nil){
                              NSMutableDictionary *logParam = [NSMutableDictionary dictionaryWithDictionary:filter];
                              [logParam setObject:returnCount forKey:@"ResultCount"];
                              [Flurry logEvent:@"Filtering Restaurants Result" withParameters:logParam];
                          }
#endif
                          
                      } else {
                          [self.merchantList addObjectsFromArray:nearByList];
                          [self.unavailableList addObjectsFromArray:unavailableList];
                      }
                      
                      if([returnCount integerValue] < RUSHORDER_PAGE_COUNT){
                          self.moredata = NO;
                      } else {
                          self.moredata = YES;
                          if(self.currentPage == 3){
                              if([self.unavailableList count] > 0){
                                  [self.unavailableList addObject:[NSNumber numberWithBool:YES]];
                              } else if([self.merchantList count] > 0){
                                  [self.merchantList addObject:[NSNumber numberWithBool:YES]];
                              }
                              
#if FLURRY_ENABLED
                              [Flurry logEvent:@"Showed Rest. Suggestion" withParameters:@{@"Type":@"After page Number 4 - Listing"}];
#endif
                          }
                      }
                      self.currentPage++;
                      
                      [self configureSection];
                      
                      if([self.merchantList count] == 0 && [self.unavailableList count] == 0){
//                          self.tableView.tableFooterView = self.noResultView;
                          [self addNoResultView];
                      }
                      
                      [self.tableView reloadData];
                      
                      if(self.tableView.contentOffset.y > 0.0 && self.currentPage == 1){
                          goToTopAfterEndRefreshing = YES;
                      }
                          
                      
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  self.progressing = NO;
                  
                  [CATransaction begin];
                  [CATransaction setCompletionBlock:^{
                      // reload tableView after refresh control finish refresh animation
                      if(goToTopAfterEndRefreshing){
                          [self goToTop];
                      }
                  }];
                  
                  [self.refreshControl endRefreshing];
                  [CATransaction commit];
                  
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            self.progressing = NO;
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            [self.refreshControl endRefreshing];
            break;
        default:
            self.progressing = NO;
            [SVProgressHUD showErrorWithStatus:NETWORK_ERROR_MESSAGE];
            [self.refreshControl endRefreshing];
            break;
    }
}


#pragma mark - UISearchBarDelegate
//- (void) searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
//{
//    
////    controller.searchBar.text = self.searchedKeyword;
////    self.searchResultList = nil;
////    [self.resultsTableController.tableView reloadData];
//}

//- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller
//{
//    controller.searchBar.text = self.searchedKeyword;
//}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if([searchBar.text length] < 2){
//        self.searchResultList = nil;
//        [self.searchDisplayController setActive:YES animated:YES];
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"At Least 2 characters", nil)];
        return;
    }
    self.searchedKeyword = searchBar.text;
    
    [self requestMerchantList:searchBar.text
                     location:APP.location];
}

//- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
//{
//    PCLog(@"Hi");
//    return YES;
//}// return NO to not become first responder
//- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
//{
//    PCLog(@"Hi2");
//}
//- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
//{
//    PCLog(@"Hi3");
//    return YES;
//}
//- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
//{
//    PCLog(@"Hi4");
//}


- (IBAction)applyFilterButtonTouched:(id)sender
{
    if(_filterToolBar != nil){
        
        self.lastRequestedAreaName = nil;
        
        [UIView animateWithDuration:0.3f
                              delay:0.0f
             usingSpringWithDamping:0.9
              initialSpringVelocity:0.1
                            options:0
                         animations:^{
                             
                             CGRect visibleFrame = self.filterToolBar.frame;
                             visibleFrame.origin.y = -visibleFrame.size.height;
                             self.filterToolBar.frame = visibleFrame;
                             
                             self.filterDimButton.alpha = 0.0f;
                             self.searchController.searchBar.alpha = 1.0f;
                             
                         } completion:^(BOOL finished) {
                             
                             [self.filterDimButton removeFromSuperview];
                             [self.filterToolBar removeFromSuperview];
                             
//                             self.navigationItem.leftBarButtonItem = self.mapBarButtonItem;
                             
                             [self.navigationItem setLeftBarButtonItem:self.mapBarButtonItem
                                                              animated:YES];
                             
                             self.currentPage = 0;
                             [self startFiltering];
                         }];
        
        [self.navigationItem setRightBarButtonItems:@[self.filterBarButtonItem]
                                           animated:YES];
        self.filterBarButtonItem.button.buttonTitle = NSLocalizedString(@"Filter", nil);
    }
}

- (void)startFiltering
{
    BOOL locationForFilter = YES;
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    
    BOOL onlyDeliverable = YES;
    NSMutableString *orderTypeString = [NSMutableString string];
    if(![self.segmentControl.selectedSegmentIndice containsIndexesInRange:NSMakeRange(0, 3)]
       && [self.segmentControl.selectedSegmentIndice count] > 0){
        if([self.segmentControl.selectedSegmentIndice containsIndex:0]){ //Dine-in
            locationForFilter = NO;
            onlyDeliverable = NO;
            [orderTypeString appendString:@"dinein"];
        }
        if([self.segmentControl.selectedSegmentIndice containsIndex:1]){ //Take-out
            locationForFilter = NO;
            onlyDeliverable = NO;
            if([orderTypeString length] > 0) [orderTypeString appendString:@","];
            [orderTypeString appendString:@"takeout"];
        }
        
        if([self.segmentControl.selectedSegmentIndice containsIndex:2]){ //Delivery
            if([orderTypeString length] > 0) [orderTypeString appendString:@","];
            [orderTypeString appendString:@"delivery"];
        } else {
            locationForFilter = NO;
        }
        
        if([orderTypeString length] > 0){
            [dict setObject:orderTypeString forKey:@"order_type"];
        }
    } else {
        locationForFilter = NO;
    }
    
    if(onlyDeliverable && [self.segmentControl.selectedSegmentIndice containsIndex:2]){
        [dict setObject:@"true" forKey:@"deliverable"];
    }
    
    if(self.openButton.isSelected){
        [dict setObject:@"true" forKey:@"open_now"];
    }
    
    if(self.dealsOnlyButton.isSelected){
        [dict setObject:@"true" forKey:@"deals_only"];
    }
    
    if(self.favoriteButton.isSelected){
        [dict setObject:@"true" forKey:@"favorites"];
    }
    
    NSMutableString *priceString = [NSMutableString string];
    if(![self.priceSegmentControl.selectedSegmentIndice containsIndexesInRange:NSMakeRange(0, 4)]
       && [self.priceSegmentControl.selectedSegmentIndice count] > 0){
        
        [self.priceSegmentControl.selectedSegmentIndice enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
            if([priceString length] > 0) [priceString appendString:@","];
            [priceString appendString:[[NSNumber numberWithUnsignedInteger:idx+1] stringValue]];
            [dict setObject:priceString forKey:@"price"];
        }];
        
        
    }
    
    if(self.deliveryFeeButton.isSelected){
        [dict setObject:@"true" forKey:@"free_delivery"];
    }
    
    
    if([self.selectedCuisineNos length] > 0){
        [dict setObject:self.selectedCuisineNos forKey:@"category_ids"];
    }
    
    if([[dict allKeys] count] > 0){
        [self setBadgeOn:YES];
    } else {
        [self setBadgeOn:NO];
    }
    
    if(CRED.isSignedIn){
        [dict setObject:CRED.customerInfo.authToken forKey:@"auth_token"];
    }
    
    PCLog(@"Filter : %@", dict);

       
    [self requestMerchantListWithFilter:dict
                               location:locationForFilter ? self.filterLocation : APP.location];
}

- (void)locationViewController:(LocationViewController *)viewController
            didApplyCoordinate:(CLLocationCoordinate2D)coordinate
                          name:(NSString *)name
            placemarkOrAddress:(id)obj
{
    self.filterLocation = [[CLLocation alloc] initWithLatitude:coordinate.latitude
                                                     longitude:coordinate.longitude];
    self.filterAddressName = name;
    
    
    if([obj isKindOfClass:[Addresses class]]){
        Addresses *address = (Addresses *)obj;
        self.filterAddress = address.address;
        
    } else if([obj isKindOfClass:[CLPlacemark class]]){
        CLPlacemark *place = (CLPlacemark *)obj;
        
        self.filterAddress = place.address;
    }
    
    self.locationButton.buttonTitle = name;
    
    self.differentLocationSelected = YES;
    self.locationSegmentControl.selectedSegmentIndex = 1;
}

- (void)didCancelLocationViewController:(LocationViewController *)viewController
{
    if(viewController.shouldBeBackToCurrent){
        self.locationSegmentControl.selectedSegmentIndex = 0;
    }
}

- (IBAction)toggleButtonTouched:(UIButton *)sender
{
    sender.selected = !sender.isSelected;
}

- (IBAction)serviceFeeInfoButtonTouched:(id)sender
{
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    
    ServiceFeeGuideView *view = [ServiceFeeGuideView view];
    view.guideLabel.text = [NSString stringWithFormat:NSLocalizedString(@"To offer delivery from places that don't normally deliver, we sometimes partner with 3rd parties who charge an added Service Fee. We're working hard to get these fees down, so just hang on tight!", nil)];
    [alertView setContainerView:view];
    alertView.buttonTitles = [NSArray arrayWithObject:NSLocalizedString(@"Got it!", nil)];
    [alertView show];
    
    [view startAnimation];
}

#if OPTION1
- (void)tableView:(UITableView *)tableView cell:(RestaurantCell1 *)cell didTouchActionButtonAtIndexPath:(NSIndexPath *)indexPath withCartType:(CartType)cartType
#elif OPTION2
- (void)tableView:(UITableView *)tableView cell:(RestaurantCell2 *)cell didTouchActionButtonAtIndexPath:(NSIndexPath *)indexPath withCartType:(CartType)cartType
#endif
{
    self.selectedCartType = cartType;
    
    Merchant *merchant = nil;
    
    if(tableView == self.resultsTableController.tableView){
        
        merchant = [self.searchResultList objectAtIndex:indexPath.row];
        
        if(![self decideNextViewAndPushWithMerchant:merchant]){
            [tableView deselectRowAtIndexPath:indexPath
                                          animated:YES];
        }
    } else {
        
        TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
        
        merchant = [tableSection.menus objectAtIndex:indexPath.row];

        if(![self decideNextViewAndPushWithMerchant:merchant]){
            [tableView deselectRowAtIndexPath:indexPath
                                          animated:NO];
        }
    }
    
#if FLURRY_ENABLED
    [Flurry logEvent:@"Try To Enter Menu" withParameters:@{@"RestaurantName":merchant.name,
                                                           @"RestaurantId":[NSNumber numberWithSerial:merchant.merchantNo],
                                                           @"hasServiceFee":[NSNumber numberWithBool:merchant.roServiceRate ? YES : NO],
                                                           @"OrderType":[Cart typeStringForCartType:cartType]}];
#endif
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
}

- (BOOL)decideNextViewAndPushWithMerchant:(Merchant *)merchant
{
    return [self decideNextViewAndPushWithMerchant:merchant withForce:NO];
}

- (BOOL)decideNextViewAndPushWithMerchant:(Merchant *)merchant withForce:(BOOL)withForce
{
    id orderOrCart = nil;
    
    if(withForce){
        if(self.selectedCartType != CartTypeCart){
            orderOrCart = [TRANS activeLocalCartAtMerchantNo:merchant.merchantNo];
        }
    } else {
        orderOrCart = [TRANS activeOrderAtMerchantNo:merchant.merchantNo];
    }
    
    if(orderOrCart == nil){
        if(merchant != nil){
            if(merchant.isTableBase){
                //Table serving
                if(merchant.isMenuOrder){
                    [self pushMenuViewController:merchant];
                } else {
                    if(merchant.isAblePayment){
                        [self pushTablesetViewController:merchant];
                    } else {
                        PCError(@"Merchant has at least menuorder or direct pay");
                        [UIAlertView alertWithTitle:NSLocalizedString(@"This restaurant has problems", nil)
                                            message:NSLocalizedString(@"There must be somethig wrong for this restaurant. Select other restaurants.", nil)];
                        return NO;
                    }
                }
            } else {
                //Pick-up
                [self pushMenuViewController:merchant];
            }
//            [self.searchDisplayController setActive:NO animated:YES];
            
            return YES;
        }
    } else {
        self.orderOrCart = orderOrCart;
        self.selectedMerchant = merchant;
        
        NSString *title = nil;
        NSString *message = nil;
        NSString *cancelButtonTitle = NSLocalizedString(@"Start New Order", nil);
        
        if([orderOrCart isKindOfClass:[Order class]]){
            Order *castedOrder = (Order *)orderOrCart;
            
            title =[NSString stringWithFormat:NSLocalizedString(@"You have an active %@ order at %@", nil),
                    castedOrder.typeString.uppercaseString, castedOrder.merchant.name];
            
            if(castedOrder.tableNumber == nil){
                message = NSLocalizedString(@"Would you like to view this current order?", nil);
            } else {
                message = [NSString stringWithFormat:NSLocalizedString(@"Do you want to continue your order at table No. %@?", nil),
                           castedOrder.tableNumber];
            }
        } else if([orderOrCart isKindOfClass:[Cart class]]){
            Cart *castedCart = (Cart *)orderOrCart;
            
            title =[NSString stringWithFormat:NSLocalizedString(@"You have an active %@ order at %@", nil),
                    castedCart.typeString.uppercaseString, castedCart.merchant.name];
            
            if(castedCart.isLocal && self.selectedCartType != CartTypeCart){
                message = nil;
                cancelButtonTitle = nil;
            } else {
                if(castedCart.tableLabel == nil){
                    message = NSLocalizedString(@"Do you want to continue to order?", nil);
                } else {
                    message = [NSString stringWithFormat:NSLocalizedString(@"Do you want to continue your order at table No. %@?", nil),
                               castedCart.tableLabel];
                }
            }
        }
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:cancelButtonTitle
                                                  otherButtonTitles:NSLocalizedString(@"Continue", nil), nil];
        alertView.tag = cancelButtonTitle == nil ? 302 : 301;
        
        [alertView show];
    }
    
    return NO;
}

- (void)pushMenuViewController:(Merchant *)merchant orderOrCart:(id)orderOrCart
{
    [self pushMenuViewController:merchant
                   withTableInfo:nil
                     orderOrCart:orderOrCart
                withMenuListType:MenuListTypeAll];
}

- (void)pushMenuViewController:(Merchant *)merchant
{
    [self pushMenuViewController:merchant withTableInfo:nil withMenuListType:MenuListTypeAll];
}

- (void)pushMenuViewController:(Merchant *)merchant withMenuListType:(MenuListType)menuListType
{
    [self pushMenuViewController:merchant withTableInfo:nil withMenuListType:menuListType];
}

- (void)pushMenuViewController:(Merchant *)merchant withTableInfo:(TableInfo *)tableInfo
{
    [self pushMenuViewController:merchant withTableInfo:tableInfo withMenuListType:MenuListTypeAll];
}

- (void)pushMenuViewController:(Merchant *)merchant
                 withTableInfo:(TableInfo *)tableInfo
              withMenuListType:(MenuListType)menuListType
{
    [self pushMenuViewController:merchant
                   withTableInfo:tableInfo
                     orderOrCart:nil
                withMenuListType:menuListType];
}

- (void)pushMenuViewController:(Merchant *)merchant
                 withTableInfo:(TableInfo *)tableInfo
                   orderOrCart:(id)orderOrCart
              withMenuListType:(MenuListType)menuListType
{
    BOOL pushMenuView = YES;
    
    MenuListType neoMenuListType = MenuListTypeNone;
    
    switch (self.selectedCartType) {
        case CartTypeTakeout:
            neoMenuListType = MenuListTypeTakeout;
            break;
        case CartTypeDelivery:
            neoMenuListType = MenuListTypeDelivery;
            break;
        default:
            neoMenuListType = MenuListTypeDinein;
            break;
    }
    
    if([MENUPAN resetGraphWithMerchant:merchant
                              withType:neoMenuListType]){
        if(pushMenuView){
            
            [MENUPAN requestMenusWithSuccess:^(id response) {
                
                MenuPageViewController *viewController = [MenuPageViewController viewControllerFromNib];
                viewController.enteredFromList = YES;
                viewController.initialCartType = self.selectedCartType;
                [ORDER resetGraphWithMerchant:merchant];
                if([orderOrCart isKindOfClass:[Order class]]){
                    ORDER.order = orderOrCart;
                    [ORDER.order isAllItemsInHour];
                } else if ([orderOrCart isKindOfClass:[Cart class]]){
                    ORDER.cart = orderOrCart;
                    [ORDER.cart isAllItemsInHour];
                }
                viewController.tableInfo = tableInfo;
                viewController.hidesBottomBarWhenPushed = YES;
                ORDER.tempDeliveryAddress = nil;
                [self.navigationController pushViewController:viewController animated:YES];
                
            } andFail:^(NSUInteger errorCode) {
                PCError(@"Can't continue to push MenuViewController for requestMenusWithSuccess");
                [UIAlertView alertWithTitle:@"Oops! Unexpected Error"
                                    message:@"Sorry, something seems a little buggy! Please try exiting the app and restarting. Please don't hesitate to reach out to us if the problem persists."];
            }];
        }
    } else {
        if(pushMenuView){
            MenuPageViewController *viewController = [MenuPageViewController viewControllerFromNib];
            viewController.enteredFromList = YES;
            viewController.initialCartType = self.selectedCartType;
            [ORDER resetGraphWithMerchant:merchant];
            if([orderOrCart isKindOfClass:[Order class]]){
                ORDER.order = orderOrCart;
                [ORDER.order isAllItemsInHour];
            } else if ([orderOrCart isKindOfClass:[Cart class]]){
                ORDER.cart = orderOrCart;
                [ORDER.cart isAllItemsInHour];
            }
            viewController.tableInfo = tableInfo;
            viewController.hidesBottomBarWhenPushed = YES;
            ORDER.tempDeliveryAddress = nil;
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
}

- (void)pushTablesetViewController:(Merchant *)merchant
{
    TablesetViewController *viewController = [TablesetViewController viewControllerFromNib];
    [ORDER resetGraphWithMerchant:merchant];
    [MENUPAN resetGraphWithMerchant:merchant];
    
    ORDER.tempDeliveryAddress = nil;
    [self.navigationController pushViewController:viewController animated:YES];
}


- (void)pushActiveOrder:(id)orderOrCart
{
    Merchant *merchant = nil;
    
    if([orderOrCart isKindOfClass:[Order class]]){
        Order *order = (Order *)orderOrCart;
        
        merchant = order.merchant;
        
        TableInfo *tableInfo = nil;
        
        if(order.status == OrderStatusFixed && (order.orderType == OrderTypeCart || order.orderType == OrderTypeOrderOnly)){
            [UIAlertView alertWithTitle:NSLocalizedString(@"Completed", nil)
                                message:NSLocalizedString(@"Your order is complete.", nil)];
            [self.tableView deselectRowAtIndexPath:self.selectedIndexPath
                                          animated:YES];
        } else if(order.status == OrderStatusCanceled){
            [UIAlertView alertWithTitle:NSLocalizedString(@"Removed Order", nil)
                                message:NSLocalizedString(@"Your order was removed by restaurant. Please ask restaurant staff for assistance.", nil)];
            [self.tableView deselectRowAtIndexPath:self.selectedIndexPath
                                          animated:YES];
            
        } else {
//            [self.searchDisplayController setActive:NO animated:YES];
            
            switch(order.orderType){
                case OrderTypeBill:
                {
                    BillViewController *viewController = [BillViewController viewControllerFromNib];
                    [ORDER resetGraphWithMerchant:order.merchant];
                    ORDER.order = order;
                    [MENUPAN resetGraphWithMerchant:order.merchant];
                    
                    ORDER.tempDeliveryAddress = nil;
                    [self.navigationController pushViewController:viewController
                                                         animated:YES];
                }
                    break;
                case OrderTypeCart:
                case OrderTypeOrderOnly:
                    tableInfo = [[TableInfo alloc] initWithTableLabel:order.tableNumber
                                                        merchantNo:merchant.merchantNo];
                    self.selectedCartType = CartTypeCart;
                    [self pushMenuViewController:merchant
                                   withTableInfo:tableInfo
                                withMenuListType:MenuListTypeDinein];
                    break;
                case OrderTypePickup:
                case OrderTypeTakeout:
                case OrderTypeDelivery:
                    [self pushTogoAuthViewController:merchant
                                           withOrder:order];
                    break;
                default:
                    // ??
                    break;
            }
        }
        
    } else if([orderOrCart isKindOfClass:[Cart class]]){
        Cart *cart = (Cart *)orderOrCart;
        
        merchant = cart.merchant;
        
        if(merchant == nil){
            PCError(@"Merchant removed in local cart");
            [cart delete];
            [self.tableView reloadData];
//            if(self.tableView.contentOffset.y > 0.0){
//                [self goToTop];
//            }
        } else {
//            [self.searchDisplayController setActive:NO animated:YES];
            
            if(cart.isLocal){
                
                for(Merchant *aMerchant in self.merchantList){
                    PCWarning(@" TODO: This code should be fixed too - so heavy!");
                    if([aMerchant isKindOfClass:[Merchant class]] && aMerchant.merchantNo == merchant.merchantNo){
                        [merchant refreshWithMerchant:aMerchant];
                        break;
                    }
                }
                
                for(Merchant *aMerchant in self.unavailableList){
                    PCWarning(@" TODO: This code should be fixed too - so heavy!");
                    if([aMerchant isKindOfClass:[Merchant class]] && aMerchant.merchantNo == merchant.merchantNo){
                        [merchant refreshWithMerchant:aMerchant];
                        break;
                    }
                }
                
                self.selectedCartType = cart.cartType;
                [self pushMenuViewController:merchant orderOrCart:cart];
                
            } else {
                TableInfo *tableInfo = [[TableInfo alloc] initWithTableLabel:cart.tableLabel
                                                               merchantNo:merchant.merchantNo];
                self.selectedCartType = cart.cartType;
                [self pushMenuViewController:merchant
                               withTableInfo:tableInfo];
            }
        }
    }
}


#pragma mark - MerchantViewControllerDelegate
- (void)merchantViewController:(MerchantViewController *)merchantViewController
          didTouchActionButton:(id)sender
                      merchant:(Merchant *)merchant
                  withCartType:(CartType)cartType
{
    self.selectedCartType = cartType;
    
    [merchantViewController dismissViewControllerAnimated:YES
                             completion:^{
                                 [self decideNextViewAndPushWithMerchant:merchant];
                             }];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 211: // New
            if(buttonIndex == AlertButtonIndexYES){
                [DeliveryAddress newDeliveryAddressWithAddress:ORDER.tempDeliveryAddress];
            }
            break;
        case 212: // Update to Default
            if(buttonIndex == AlertButtonIndexYES){
                [DeliveryAddress updateDeliveryAddress:ORDER.lastUsedDeliveryAddress withAddress:ORDER.tempDeliveryAddress];
            } else if(buttonIndex == 2) {
                ORDER.tempDeliveryAddress.needToBeUpdatedToDefault = NO;
                [DeliveryAddress newDeliveryAddressWithAddress:ORDER.tempDeliveryAddress];
            }
            break;
        case 213: // Update
            if(buttonIndex == AlertButtonIndexYES){
                [DeliveryAddress updateDeliveryAddress:ORDER.lastUsedDeliveryAddress withAddress:ORDER.tempDeliveryAddress];
            }
            break;
        case 301:
            if(buttonIndex == AlertButtonIndexYES){
                [self pushActiveOrder:self.orderOrCart];
            } else {
                [self decideNextViewAndPushWithMerchant:self.selectedMerchant withForce:YES];
            }
            break;
        case 302:
            [self pushActiveOrder:self.orderOrCart];
            break;
    }
}
//
//- (void)newDeliveryAddressWithAddress:(Addresses *)address
//{
//    RequestResult result = RRFail;
//    
//    DeliveryAddress *deliveryAddress = [[DeliveryAddress alloc] init];
//    
//    [deliveryAddress updateWithAddresses:address];
//    
//    result = [PAY requestAddDeliveryAddress:deliveryAddress
//                            completionBlock:^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode) {
//                                if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
//                                    switch(response.errorCode){
//                                        case ResponseSuccess:{
//                                            [REMOTE handleDeliveryAddressesResponse:(NSMutableArray *)response
//                                                                   withNotification:YES];
//                                        }
//                                            break;
//                                        default:
//                                        {
//                                            NSString *message = [response objectForKey:@"message"];
//                                            NSString *title = [response objectForKey:@"title"];
//                                            if(title == nil){
//                                                [UIAlertView alert:message];
//                                            } else {
//                                                [UIAlertView alertWithTitle:title
//                                                                    message:message];
//                                            }
//                                        }
//                                            break;
//                                    }
//                                } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
//                                    [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
//                                    [CRED resetUserAccount];
//                                } else {
//                                    if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
//                                        [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
//                                                                                            object:nil];
//                                    } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
//                                        [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
//                                                            ,statusCode]];
//                                    }
//                                }
//                                [SVProgressHUD dismiss];
//                            }];
//    
//    switch(result){
//        case RRSuccess:
//            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
//            break;
//        case RRParameterError:
//            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
//            [SVProgressHUD dismiss];
//            break;
//        default:
//            [SVProgressHUD dismiss];
//            break;
//    }
//}
//
//- (void)updateDeliveryAddress:(DeliveryAddress *)deliveryAddress withAddress:(Addresses *)address
//{
//    RequestResult result = RRFail;
//    
//    if(address != nil){
//        [deliveryAddress updateWithAddresses:address];
//    } else {
//        deliveryAddress.defaultAddress = YES;
//    }
//    
//    result = [PAY requestUpdateDeliveryAddress:deliveryAddress
//                               completionBlock:^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode) {
//                                   if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
//                                       switch(response.errorCode){
//                                           case ResponseSuccess:
//                                               
////                                               [self.navigationController popViewControllerAnimated:YES];
//                                               
//                                               [REMOTE handleDeliveryAddressesResponse:(NSMutableArray *)response
//                                                                      withNotification:YES];
//                                               
//                                               break;
//                                           default:
//                                           {
//                                               NSString *message = [response objectForKey:@"message"];
//                                               NSString *title = [response objectForKey:@"title"];
//                                               if(title == nil){
//                                                   [UIAlertView alert:message];
//                                               } else {
//                                                   [UIAlertView alertWithTitle:title
//                                                                       message:message];
//                                               }
//                                           }
//                                               break;
//                                       }
//                                   } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
//                                       [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
//                                       [CRED resetUserAccount];
//                                   } else {
//                                       if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
//                                           [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
//                                                                                               object:nil];
//                                       } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
//                                           [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
//                                                               ,statusCode]];
//                                       }
//                                   }
//                                   [SVProgressHUD dismiss];
//                               }];
//    
//    switch(result){
//        case RRSuccess:
//            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
//            break;
//        case RRParameterError:
//            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
//            [SVProgressHUD dismiss];
//            break;
//        default:
//            [SVProgressHUD dismiss];
//            break;
//    }
//}
//
//
- (void)pushTogoAuthViewController:(Merchant *)merchant withOrder:(Order *)order
{
    TogoAuthViewController *viewController = [UIStoryboard viewController:@"TogoAuthViewController"
                                                                     from:@"Orders"];
    [ORDER resetGraphWithMerchant:merchant];
    [MENUPAN resetGraphWithMerchant:merchant];
    ORDER.order = order;
    viewController.payment = nil;
    viewController.order = order;
    ORDER.tempDeliveryAddress = nil;
    [self.navigationController pushViewController:viewController animated:YES];
}

#if OPTION1
- (RestaurantCell1 *)offScreenRestaurantCell
#elif OPTION2
- (RestaurantCell2 *)offScreenRestaurantCell
#endif
{
    if(_offScreenRestaurantCell == nil){
#if OPTION1
        _offScreenRestaurantCell = [self.tableView dequeueReusableCellWithIdentifier:@"RestaurantCell1"];
#elif OPTION2
        _offScreenRestaurantCell = [self.tableView dequeueReusableCellWithIdentifier:@"RestaurantCell2"];
#endif
    }
//    [_offScreenRestaurantCell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"[contentView(==%f)]",self.tableView.width]
//                                                                                                 options:0
//                                                                                                 metrics:nil
//                                                                                                   views:@{@"contentView":_offScreenRestaurantCell.contentView}]];
//    CGRect frame = _offScreenRestaurantCell.frame;
//    frame.size.width = self.tableView.width;
//    _offScreenRestaurantCell.frame = frame;
    
    [_offScreenRestaurantCell.contentView updateConstraints];
    return _offScreenRestaurantCell;
}


- (void)restaurantMapViewController:(RestaurantMapViewController *)viewController
                didSelectRestaurant:(Merchant *)merchant
                       withCartType:(CartType)cartType
{
    self.selectedCartType = cartType;
    if(![self decideNextViewAndPushWithMerchant:merchant]){
        
    }
}
              


              
#define LOAD_MORE_THRESHOLD 80.0f
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate
{
    if(scrollView == self.tableView){
//        PCLog(@"ScrollView %@", scrollView);
        
        if(scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.bounds.size.height) + LOAD_MORE_THRESHOLD){
            if(self.isMoredata){
                [self startFiltering];
            }
        }
    }
}
              
 
              
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    PCLog(@"scrollView %@", scrollView);
    if(self.isMoredata && !self.isProgressing){
        if(scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.bounds.size.height) + LOAD_MORE_THRESHOLD){
            
            self.loadingLabel.text = NSLocalizedString(@"Release Down To Load More", nil);
        } else {
            self.loadingLabel.text = NSLocalizedString(@"Pull Up To Load More", nil);
        }
    }
    
    if(![NSUserDefaults standardUserDefaults].isHideCoin){
        if(scrollView.contentOffset.y > COIN_SHOW_THREASHOLD){ // Hide
            if(!self.orangeButtonHidden){
                self.orangeButtonHidden = YES;
                [UIView animateWithDuration:0.3f
                                      delay:0.3f
                     usingSpringWithDamping:0.6f
                      initialSpringVelocity:0.4f
                                    options:UIViewAnimationOptionBeginFromCurrentState
                                 animations:^{
                                     PCLog(@"Animation hide");
                                     self.orangeCoinButton.transform = CGAffineTransformMakeScale(0.01f,0.01f);
                                 } completion:^(BOOL finished) {
                                     if(finished && self.orangeButtonHidden){
                                         self.orangeCoinButton.transform = CGAffineTransformMakeScale(0.0f,0.0f);
                                     }
                                 }];
            }
        } else { //Show
            if(self.orangeButtonHidden){
                self.orangeButtonHidden = NO;
                [UIView animateWithDuration:0.3f
                                      delay:0.3f
                     usingSpringWithDamping:0.6f
                      initialSpringVelocity:0.3f
                                    options:UIViewAnimationOptionBeginFromCurrentState
                                 animations:^{
                                     PCLog(@"Animation show");
                                     self.orangeCoinButton.transform = CGAffineTransformMakeScale(1.0,1.0);
                                 } completion:^(BOOL finished) {
                                     
                                 }];
            }
        }
    }
}

- (void)cuisineTableViewController:(CuisineTableViewController *)viewController
                 didSelectCuisines:(NSArray *)array
{
    PCLog(@"Selected Cuisine Array %@", array);
    
    self.selectedCuisines = array;
    
    if([array count] > 0){
        Cuisine *cuisine = [array objectAtIndex:0];
        if(cuisine.no == 0){
            self.cuisineButton.buttonTitle = NSLocalizedString(@"All Cuisines", nil);
            self.selectedCuisineNos = @"";
        } else {
            NSMutableString *cuisineNames = [NSMutableString string];
            NSMutableString *cuisineNos = [NSMutableString string];
            for(Cuisine *aCuisine in array){
                if([cuisineNames length] > 0) [cuisineNames appendString:@", "];
                [cuisineNames appendString:aCuisine.name];
                
                if([cuisineNos length] > 0) [cuisineNos appendString:@","];
                [cuisineNos appendString:[NSString stringWithFormat:@"%lld", aCuisine.no]];
            }
            
            self.cuisineButton.buttonTitle = cuisineNames;
            self.selectedCuisineNos = cuisineNos;
        }
    } else {
        // No selected Cuisine
        [UIAlertView alertWithTitle:NSLocalizedString(@"You Should Select At Least 1 Cuisine", nil)
                            message:nil];
        self.cuisineButton.buttonTitle = NSLocalizedString(@"No Cuisines", nil);
        self.selectedCuisineNos = @"";
    }
}

//viewController.delegate = self;
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    MerchantViewController *viewController = [segue isDestClass:[MerchantViewController class]];
    viewController.canDirectOrder = YES;
    if(viewController){
        viewController.delegate = self;
        [viewController.view updateConstraints];
        viewController.topSpaceConstraint.constant = -64.0f;
        [viewController.view setNeedsLayout];
        [viewController.view layoutSubviews];
        return;
    }
    
    RestaurantMapViewController *viewController2 = [segue isDestClass:[RestaurantMapViewController class]];
    if(viewController2){
        viewController2.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        viewController2.merchantList = self.merchantList;
        viewController2.unavailableList = self.unavailableList;
        viewController2.delegate = self;
        return;
    }
    
    CuisineTableViewController *viewController3 = [segue isDestClass:[CuisineTableViewController class]];
    if(viewController3){
        viewController3.delegate = self;
        return;
    }
    
    LocationViewController *viewController4 = [segue isDestClass:[LocationViewController class]];
    if(viewController4){
        AddressAnnotation *annotation = [[AddressAnnotation alloc] init];
        annotation.coordinate = self.filterLocation.coordinate;
        annotation.title = self.filterAddressName;
        annotation.subtitle = self.filterAddress;
        viewController4.selectedAddress = annotation;
        viewController4.delegate = self;
        
        if([sender isKindOfClass:[WLHorizontalSegmentedControl class]]){
            viewController4.shouldBeBackToCurrent = YES;
        }
        return;
    }
}

- (UIButton *)filterDimButton
{
    if(!_filterDimButton){
        _filterDimButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _filterDimButton.frame = self.view.frame;
        _filterDimButton.backgroundColor = [UIColor blackColor];
        [_filterDimButton addTarget:self
                             action:@selector(closeFilterButton:)
                   forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _filterDimButton;
}

- (void)ooaViewController:(OOAViewController *)viewController didTouchChangeLocationButton:(id)sender
{
    [self filterButtonTouched:sender];
    self.segmentControl.selectedSegmentIndice = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(2, 1)];
    
    self.locationSegmentControl.selectedSegmentIndex = 1;
    self.differentLocationSelected = NO;
    [self locationSegmentedChange:self.locationSegmentControl];
}


@end
