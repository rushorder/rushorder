//
//  OOAViewController.m
//  RushOrder
//
//  Created by Conan Kim on 5/1/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import "OOAViewController.h"
#import "RequestAreaViewController.h"

@interface OOAViewController ()

@end

@implementation OOAViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_navigation_rushorder"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)requestButtonTouched:(id)sender
{
    RequestAreaViewController *viewController = [RequestAreaViewController viewControllerFromNib];
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (IBAction)changeLocationButtonTouched:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 
                                 [self.delegate ooaViewController:self
                                     didTouchChangeLocationButton:sender];
                                 
                             }];
}

- (IBAction)gotItButtonTouched:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 
                             }];
}
@end
