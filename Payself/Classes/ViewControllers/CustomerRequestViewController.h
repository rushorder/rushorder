//
//  CustomerRequestViewController.h
//  RushOrder
//
//  Created by Conan on 1/21/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import "PCViewController.h"
#import "FavoriteOrder.h"

@interface CustomerRequestViewController : PCViewController

@property (strong, nonatomic) FavoriteOrder *favoriteOrder;

@end
