//
//  FaqViewController.m
//  RushOrder
//
//  Created by Conan Kim on 3/22/16.
//  Copyright © 2016 Paycorn. All rights reserved.
//

#import "FaqViewController.h"

@interface FaqViewController()
@property (weak, nonatomic) IBOutlet UIWebView *faqWebView;
@property (weak, nonatomic) IBOutlet UITextView *faqTextView;

@end

@implementation FaqViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.faqTextView.textContainerInset = UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = [self.faq objectForKey:@"title"];
//    [self.faqWebView loadHTMLString:[self.faq objectForKey:@"answer"] baseURL:nil];
    
    UIFont *font = [UIFont fontWithName:@"Helvetica Neue" size:16.0];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font
                                                                forKey:NSFontAttributeName];
    NSMutableAttributedString *attrAnswer = [[NSMutableAttributedString alloc] initWithData:[[self.faq objectForKey:@"answer"] dataUsingEncoding:NSUTF8StringEncoding]
                                                                      options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                           documentAttributes:nil
                                                                        error:nil];
    [attrAnswer addAttributes:attrsDictionary
                        range:NSMakeRange(0, attrAnswer.length)];
    
    self.faqTextView.attributedText = attrAnswer;
}
@end
