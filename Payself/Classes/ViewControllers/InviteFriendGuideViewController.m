//
//  InviteFriendGuideViewController.m
//  RushOrder
//
//  Created by Conan Kim on 1/21/16.
//  Copyright © 2016 Paycorn. All rights reserved.
//

#import "InviteFriendGuideViewController.h"
#import <TwitterKit/TwitterKit.h>
#import "PCCredentialService.h"
#import "SignInCustomerViewController.h"
#import "SignUpCustomerViewController.h"

@interface InviteFriendGuideViewController ()
@property (weak, nonatomic) IBOutlet UILabel *referralCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *myOrangesEarnedLabel;
@property (weak, nonatomic) IBOutlet UIButton *coinHideButton;
@property (nonatomic) BOOL outdated;
@end

@implementation InviteFriendGuideViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signedIn:)
                                                 name:SignedInNotification
                                               object:nil];
}

- (void)signedIn:(NSNotification *)aNoti
{
    self.outdated = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemBack
                                                                        target:self
                                                                        action:@selector(backButtonTouched:)];
    
    [self requestMyReferralCode];
}

- (void)backButtonTouched:(id)sender
{
//    if(self.isBackToRestaurantPage){
//        [self.tabBarController setSelectedIndex:0];
//    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.coinHideButton.selected = [NSUserDefaults standardUserDefaults].isHideCoin;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(self.outdated){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NeedToPreparePushingAutoViewController"
                                                            object:self
                                                          userInfo:@{@"type":@"invite"}];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (IBAction)hideCoinButtonTouched:(UIButton *)sender
{
    sender.selected = !sender.selected;
    [NSUserDefaults standardUserDefaults].hideCoin = sender.selected;
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)requestMyReferralCode
{
    if(CRED.customerInfo != nil){
        RequestResult result = RRFail;
        result = [CRED requestMyReferralCodeWithCompletionBlock:
                  ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                      if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                          switch(response.errorCode){
                              case ResponseSuccess:

                                  [self updateViews];
                                  break;
                              case ResponseErrorGeneral:{
                                  NSString *message = [response objectForKey:@"message"];
                                  if([message isKindOfClass:[NSString class]]){
                                      [UIAlertView alert:message];
                                  }
                                  break;
                              }
                              default:{
                                  NSString *message = [response objectForKey:@"message"];
                                  if([message isKindOfClass:[NSString class]]){
                                      [UIAlertView alert:message];
                                  } else {
                                      [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting my referral code", nil)];
                                      
                                  }
                                  break;
                              }
                          }
                      } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                          [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                          [CRED resetUserAccount];
                          //Signed out in requestMyCreditWithCompletionBlock
                      } else {
                          if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                              [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                                  object:nil];
                          } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                              [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                                  ,statusCode]];
                          }
                      }
                      [SVProgressHUD dismiss];
                  }];
        switch(result){
            case RRSuccess:
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                break;
            case RRParameterError:
                [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                break;
            default:
                break;
        }
    } else {
        [self updateViews];
    }
}

- (void)updateViews
{
    self.referralCodeLabel.text = CRED.customerInfo.referralCode;
    self.myOrangesEarnedLabel.text = CRED.customerInfo.totalEarnedCreditString;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)mailButtonTouched:(id)sender
{       
    NSString *subject = [@"Something I wanted to share" urlEncodedString];
     NSString *body = [[NSString stringWithFormat:@"Try the RushOrder App to order from the best restaurants in town! Use my referral code, %@, for a $5 credit! %@", CRED.customerInfo.referralCode, BRANCH_LINK] urlEncodedString];
//    NSString *body = [[NSString stringWithFormat:@"Hey,\nCheck out RushOrder App! Order Takeout and Delivery from some of the best spots in town! Use my referral code:%@ for $5 off! Download here:<a href=\"%@\">%@</a>\n\nThanks!\n%@\n\n", CRED.customerInfo.referralCode, BRANCH_LINK, BRANCH_LINK, CRED.customerInfo.name] urlEncodedString];
    
    NSString *email = [NSString stringWithFormat:@"mailto:?subject=%@&body=%@", subject, body];
    
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:email]]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
    } else {
        [UIAlertView alert:@"This device is not supporting mail"];
    }
}

- (IBAction)smsButtonTouched:(id)sender
{
#if FLURRY_ENABLED
    [Flurry logEvent:@"Tried to Share Referral Code via SMS" withParameters:@{@"Customer Name": CRED.customerInfo.name,
                                                                      @"Referral Code":CRED.customerInfo.referralCode,
                                                                      @"Customer ID":CRED.customerInfo.customerNoNumber}];
#endif
    if([MFMessageComposeViewController canSendText]){
    //    NSString *subject = [@"Refer by Email" urlEncodedString];
        NSString *body = [NSString stringWithFormat:@"Try the RushOrder App to order from the best restaurants in town! Use my referral code, %@, for a $5 credit! %@", CRED.customerInfo.referralCode, BRANCH_LINK];
    //
    //    NSString *email = [NSString stringWithFormat:@"sms:?subject=%@&body=%@", subject, body];
    //    
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
        
        
        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
        messageController.messageComposeDelegate = self;
        messageController.body = body;
        messageController.recipients = nil;//[NSArray arrayWithObject:@"01028537679"];
        [self presentViewController:messageController
                           animated:YES
                         completion:^{

                         }];
    } else {
        [UIAlertView alert:@"This device is not supporting SMS text"];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result {
    if(MessageComposeResultSent == result){
        
#if FLURRY_ENABLED
        [Flurry logEvent:@"Shared Referral Code via SMS" withParameters:@{@"Customer Name": CRED.customerInfo.name,
                                                                          @"Referral Code":CRED.customerInfo.referralCode,
                                                                          @"Customer ID":CRED.customerInfo.customerNoNumber}];
#endif
    }
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 
                             }];
}

- (IBAction)twitterButtonTouched:(id)sender
{
#if FLURRY_ENABLED
    [Flurry logEvent:@"Tried to Share Referral Code via TWT" withParameters:@{@"Customer Name": CRED.customerInfo.name,
                                                                     @"Referral Code":CRED.customerInfo.referralCode,
                                                                     @"Customer ID":CRED.customerInfo.customerNoNumber}];
#endif
    
    TWTRComposer *composer = [[TWTRComposer alloc] init];
    
    NSString *body = [NSString stringWithFormat:@"Try the RushOrder App to order from the best restaurants in town! Use my referral code, %@, for a $5 credit!", CRED.customerInfo.referralCode];
    
    [composer setText:body];
    [composer setURL:[NSURL URLWithString:BRANCH_LINK]];
//    [composer setImage:[UIImage imageNamed:@"fabric"]];
    
    // Called from a UIViewController
    [composer showFromViewController:self completion:^(TWTRComposerResult result) {
        if (result == TWTRComposerResultCancelled) {

        }
        else {
#if FLURRY_ENABLED
            [Flurry logEvent:@"Shared Referral Code via TWT" withParameters:@{@"Customer Name": CRED.customerInfo.name,
                                                                              @"Referral Code":CRED.customerInfo.referralCode,
                                                                              @"Customer ID":CRED.customerInfo.customerNoNumber}];
#endif
        }
    }];
    
}

- (IBAction)facebooButtonTouched:(id)sender
{
    
#if FLURRY_ENABLED
    [Flurry logEvent:@"Try to Share Referral Code via FB" withParameters:@{@"Customer Name": CRED.customerInfo.name,
                                                       @"Referral Code":CRED.customerInfo.referralCode,
                                                       @"Customer ID":CRED.customerInfo.customerNoNumber}];
#endif
    
//    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
//    content.contentTitle = @"My title";
//    content.contentDescription = @"My Description";
//    content.contentURL = [NSURL URLWithString:@"https://rushorderapp.com"];
//    [FBSDKShareDialog showFromViewController:self
//                                 withContent:content
//                                    delegate:self];
    
    // http://www.instacart.com/store?code=CKIMB9F02&utm_campaign=off5&utm_medium=facebook&utm_source=web
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://rushorderapp.com/referral_promo?referral_code=%@", CRED.customerInfo.referralCode]];
//    content.contentTitle = @"Content Title";
//    content.contentDescription = @"Content Description";
    
    [FBSDKShareDialog showFromViewController:self
                                 withContent:content
                                    delegate:self];
    
//    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
//    content.contentURL = [NSURL URLWithString:@"https://developers.facebook.com"];
//    [FBSDKShareDialog showFromViewController:self
//                                 withContent:content
//                                    delegate:self];
    
//    FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] init];
//    content.appLinkURL = [NSURL URLWithString:BRANCH_LINK];
//    //optionally set previewImageURL
//    content.appInvitePreviewImageURL = [NSURL URLWithString:@"https://www.rushorderapp.com/assets/website_main_background_580.jpg"];
//    
//    // present the dialog. Assumes self implements protocol `FBSDKAppInviteDialogDelegate`
//    [FBSDKAppInviteDialog showFromViewController:self
//                                     withContent:content
//                                        delegate:self];
}

//- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didCompleteWithResults:(NSDictionary *)results
//{
//    PCLog(@"didCompleteWithResults");
//}
//
//- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error
//{
//    PCLog(@"didFailWithError");
//}

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    PCLog(@"results %@",results);
#if FLURRY_ENABLED
    [Flurry logEvent:@"Shared Referral Code via FB" withParameters:@{@"Customer Name": CRED.customerInfo.name,
                                                                           @"Referral Code":CRED.customerInfo.referralCode,
                                                                           @"Customer ID":CRED.customerInfo.customerNoNumber}];
#endif

}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    PCLog(@"results %@",error);
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    PCLog(@"results %@",sharer);
}

- (IBAction)singUpButtonTouched:(id)sender
{
    SignUpCustomerViewController *viewController = [SignUpCustomerViewController viewControllerFromNib];
    [self.tabBarController presentViewControllerInNavigation:viewController
                                                    animated:YES
                                                  completion:^{
                                                      
                                                  }];
}

- (IBAction)signInButtonTouched:(id)sender
{
    [self showSignIn:NO];
}

- (void)showSignIn:(BOOL)showGuide
{
    SignInCustomerViewController *viewController = [SignInCustomerViewController viewControllerFromNib];
    viewController.showGuide = showGuide;
    [self.tabBarController presentViewControllerInNavigation:viewController
                                                    animated:YES
                                                  completion:^{
                                                  }];
}


@end
