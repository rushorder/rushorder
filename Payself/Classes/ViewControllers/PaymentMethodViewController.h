//
//  PaymentMethodViewController.h
//  RushOrder
//
//  Created by Conan on 2/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"
#import "Payment.h"
#import "OrangeCell.h"
#import "CreditAuthCell.h"
#import "TakeoutViewController.h"
#import "DineinViewController.h"
#import "DeliveryViewController.h"
#import "VerificationViewController.h"
#import "PaymentSummaryView.h"
#import "Addresses.h"
#import "SignInCustomerViewController.h"

@interface PaymentMethodViewController : PCViewController
<
UITableViewDelegate,
UITableViewDataSource,
UIAlertViewDelegate,
#if USE_ORANGE_CELL
OrangeCellDelegate,
#endif
CreditAuthCellDelegate,
TakeoutViewControllerDelegate,
DineinViewControllerDelegate,
DeliveryViewControllerDelegate,
VerificationViewControllerDelegate,
PaymentSummaryViewDelegate,
SignInCustomerViewControllerDelegate
>
@property (strong, nonatomic) Payment *payment;
@property (strong, nonatomic) NSMutableArray *paymentMethodList;
@property (strong, nonatomic) Addresses *address;
@property (nonatomic, copy) NSString *receiverName;
@property (nonatomic, copy) NSString *phoneNumber;
@property (copy, nonatomic) NSString *requestMessage;
@property (nonatomic) NSInteger afterMinute;

@property (nonatomic) BOOL disposeOrderWhenBack;

@property (copy, nonatomic) NSString *promotionCodeForCheckAfterSigningIn;

@property (nonatomic) BOOL initialCreditSelection;

- (void)setInitialOptionValueWithOrder:(Order *)order;
@end
