//
//  BillViewController.h
//  RushOrder
//
//  Created by Conan on 2/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"
#import "BillOrderCell.h"

@interface BillViewController : PCViewController
<
UITextFieldDelegate,
PCCurrencyTextFieldDelegate,
UIAlertViewDelegate,
UITableViewDelegate,
UITableViewDataSource,
BillOrderCellDelegate
>


- (void)fillOrder;
@end