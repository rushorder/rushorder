//
//  RecentListViewController.h
//  RushOrder
//
//  Created by Conan Kim on 6/19/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomRequest.h"

@protocol RecentListViewControllerDelegate;

@interface RecentListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) id <RecentListViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *panelView;
@property (nonatomic) NSUInteger selectedRecentIndex;
@end

@protocol RecentListViewControllerDelegate <NSObject>
- (void)recentListViewController:(RecentListViewController *)viewController
                didSelectRequest:(CustomRequest *)selectedRequest;
@end
