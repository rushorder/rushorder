//
//  DineinViewController.h
//  RushOrder
//
//  Created by Conan on 11/13/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "RecentListViewController.h"
#import "Payment.h"

@protocol DineinViewControllerDelegate;

@interface DineinViewController : PCViewController
<
UIPickerViewDataSource,
UIPickerViewDelegate,
UITextFieldDelegate,
UITextViewDelegate,
RecentListViewControllerDelegate
>

@property (nonatomic, weak) id<DineinViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextView *additionalRequest;
@property (weak, nonatomic) IBOutlet NPToggleButton *asapButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *pickupTimeButton;
@property (weak, nonatomic) IBOutlet PCTextField *orderNameField;

@property (copy, nonatomic) NSString *initialName;
@property (copy, nonatomic) NSString *initialRequestMessage;

@property (strong, nonatomic) Payment *payment;
@property (nonatomic) BOOL disposeOrderWhenBack;

@property (nonatomic) NSInteger afterMinute;
@end


@protocol DineinViewControllerDelegate <NSObject>
@optional
- (void)dineinViewController:(DineinViewController *)viewController
     didTouchPlaceOrderButton:(id)sender;

- (void)dineinViewController:(DineinViewController *)viewController
    didTouchSignInButton:(id)sender;
@end
