//
//  TogoAuthViewController.m
//  RushOrder
//
//  Created by Conan on 7/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "TogoAuthViewController.h"
#import "OrderManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ReceiptViewController.h"
#import "TransactionManager.h"
#import "PaymentMethodViewController.h"
#import "NewCardViewController.h"
#import "PCCredentialService.h"
#import "PCPaymentService.h"
#import "RestaurantSummaryView.h"
#import "TicketOrderCell.h"
#import "RemoteDataManager.h"
#import <AddressBook/AddressBook.h>
#import "SubtotalTableViewCell.h"
#import "CustomerRequestTableViewCell.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import "CustomerRequestViewController.h"
#import "SelectCardViewController.h"
#import "MenuManager.h"
#import "MenuPageViewController.h"
#import <Smooch/Smooch.h>

enum{
    SectionLineItem = 1,
    SectionSubtotal,
    SectionSpecialInstruction,
    SectionPayment,
    SectionDelivery,
};

NSString * const FavoriteOrderInsertedNotification = @"FavoriteOrderInsertedNotification";
NSString * const FavoriteOrderUpdatedNotification = @"FavoriteOrderUpdatedNotification";
NSString * const NeedToShowAppRatingNotification = @"NeedToShowAppRatingNotification";


@interface TogoAuthViewController ()

@property (strong, nonatomic) NSMutableArray *mobileCardList;

@property (weak, nonatomic) IBOutlet RestaurantSummaryView *restaurantSummaryView;

@property (weak, nonatomic) IBOutlet UILabel *orderNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *ticketNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *informationLabel;
@property (weak, nonatomic) IBOutlet NPStretchableButton *clearButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *rejectReasonLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rejectLabelVSConstraint;

@property (weak, nonatomic) IBOutlet UIImageView *sorryIcon;
@property (weak, nonatomic) IBOutlet UILabel *sorryLabel;


@property (weak, nonatomic) IBOutlet NPToggleButton *progressButton0;
@property (weak, nonatomic) IBOutlet NPToggleButton *progressButton1;
@property (weak, nonatomic) IBOutlet NPToggleButton *progressButton2;
@property (weak, nonatomic) IBOutlet NPToggleButton *progressButton3;
@property (weak, nonatomic) IBOutlet UILabel *subTicketNoTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTicketNoLabel;

@property (weak, nonatomic) IBOutlet UILabel *expectedTimeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *expectedTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *ticketView;

@property (strong, nonatomic) TicketOrderCell *offScreenTicketOrderCell;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *activeDetailTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressButtonTopConstraint;

@property (strong, nonatomic) CLGeocoder *geoCoder;

@property (weak, nonatomic) IBOutlet UILabel *subTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryFeeLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UIImageView *testIcon;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *receiptBarButtonItem;
@property (weak, nonatomic) IBOutlet UIView *activeDetailPanel;
@property (weak, nonatomic) IBOutlet UIView *rapidROPanel;
@property (strong, nonatomic) IBOutlet UIView *orderHeaderView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *clearButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cancelButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cancelTopHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *button2WidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *button2EqualConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *button3EqualConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *button3WidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *button2LeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *button3LeadingConstraint;

@property (strong, nonatomic) NSMutableArray *data;

@property (weak, nonatomic) IBOutlet UILabel *restaurantNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *restaurantAddrLabel;
@property (weak, nonatomic) IBOutlet CircleImageView *restaurantLogoImageView;

@property (weak, nonatomic) IBOutlet UITextField *favoriteOrderNameTextField;
@property (weak, nonatomic) IBOutlet UIImageView *favoriteOrderImageView;

@property (strong, nonatomic) UIImage *imageToBeApplied;
@property (strong, nonatomic) IBOutlet UIView *bottomFunctionView;
@property (weak, nonatomic) IBOutlet NPStretchableButton *callHelplineButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *liveChatButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *callHelplineHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *liveChatHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *helpGuideLabel;

@property (strong, nonatomic) NSMutableDictionary *offscreenCells;


@end

@implementation TogoAuthViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self){
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(transactionListChanged:)
                                                 name:TransactionListChangedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cardListChanged:)
                                                 name:CardListChangeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signedOut:)
                                                 name:SignedOutNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signedIn:)
                                                 name:SignedInNotification
                                               object:nil];
    
    self.offscreenCells = [[NSMutableDictionary alloc] init];
}

- (void)signedOut:(NSNotification *)notification
{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)signedIn:(NSNotification *)notification
{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)cardListChanged:(NSNotification *)notification
{
    if(CRED.isSignedIn){
        self.mobileCardList = REMOTE.cards;
    } else {
        self.mobileCardList = [MobileCard listAll];
    }
    
    if(self.favoriteOrder != nil){
        [self fillFavoriteOrder];
    } else {
        [self fillOrder];
    }
}

- (void)configureDataForFavoriteOrder
{
    self.data = [NSMutableArray array];
    
    [self.data addObject:[NSNumber numberWithInteger:SectionLineItem]];
    [self.data addObject:[NSNumber numberWithInteger:SectionSubtotal]];
    
    if([self.favoriteOrder.customerRequest length] > 0)
        [self.data addObject:[NSNumber numberWithInteger:SectionSpecialInstruction]];
    
    if(self.favoriteOrder.card != nil)
        [self.data addObject:[NSNumber numberWithInteger:SectionPayment]];
    
    if(self.favoriteOrder.orderType == OrderTypeDelivery)
        [self.data addObject:[NSNumber numberWithInteger:SectionDelivery]];
}

- (void)configureData
{
    self.data = [NSMutableArray array];
    
    [self.data addObject:[NSNumber numberWithInteger:SectionLineItem]];
    [self.data addObject:[NSNumber numberWithInteger:SectionSubtotal]];
    
    if([self.order.customerRequest length] > 0)
        [self.data addObject:[NSNumber numberWithInteger:SectionSpecialInstruction]];
    
    if([self.order.paymentList count] > 0)
        [self.data addObject:[NSNumber numberWithInteger:SectionPayment]];
    
    if(self.order.isDelivery)
        [self.data addObject:[NSNumber numberWithInteger:SectionDelivery]];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
 
    if(self.favoriteOrder == nil){
        [TRANS requestMyOrder:self.order];
    }
    
    if(!self.isRapidRO){
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(orderConfirmed:)
                                                     name:OrderConfirmedNotificationKey
                                                   object:nil];
    } else {
        self.callHelplineHeightConstraint.constant = 0.0f;
        self.liveChatHeightConstraint.constant = 0.0f;
        self.callHelplineButton.hidden = YES;
        self.liveChatButton.hidden = YES;
        self.helpGuideLabel.hidden = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if(!self.isRapidRO){
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:OrderConfirmedNotificationKey
                                                      object:nil];
    }
}


- (void)orderConfirmed:(NSNotification *)noti
{
    [TRANS requestMyOrder:self.order];
}

- (void)transactionListChanged:(NSNotification *)noti
{
    [self.refreshControl endRefreshing];
    
    for(id trans in TRANS.transactions){
        if([trans isKindOfClass:[Order class]]){
            Order *anOrder = (Order *)trans;
            
            if(anOrder.orderNo == self.order.orderNo){
                self.order = anOrder;
                break;
            }
        }
    }
    
    if(self.favoriteOrder != nil){
        [self fillFavoriteOrder];
    } else {
        [self fillOrder];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(self.isRapidRO){
        
        
        self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemSave
                                                                            target:self
                                                                            action:@selector(saveButtonTouched:)];
        
        [self.orderHeaderView removeFromSuperview];
        self.tableView.tableHeaderView = self.rapidROPanel;
        self.tableView.tableFooterView = nil;
        
        if(self.favoriteOrder != nil){
            self.title = NSLocalizedString(@"Edit RapidOrder", nil);
            self.restaurantNameLabel.text = self.favoriteOrder.merchant.name;
            self.restaurantAddrLabel.text = self.favoriteOrder.merchant.address;
            self.orderTypeLabel.text = self.favoriteOrder.typeString;
            self.orderTypeImageView.image = self.favoriteOrder.orderTypeIcon;
            
            NSURL *imgURL = self.favoriteOrder.merchant.logoURL;
            if(imgURL == nil) imgURL = self.favoriteOrder.merchant.imageURL;
            
            [self.restaurantLogoImageView sd_setImageWithURL:imgURL
                                         placeholderImage:[UIImage imageNamed:@"placeholder_logo"]
                                                  options:0
                                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                                    
                                                }];
        } else {
            self.title = NSLocalizedString(@"Add to RapidOrder", nil);
            self.restaurantNameLabel.text = self.order.merchant.name;
            self.restaurantAddrLabel.text = self.order.merchant.address;
            self.orderTypeLabel.text = self.order.typeString;
            self.orderTypeImageView.image = self.order.orderTypeIcon;
            
            NSURL *imgURL = self.order.merchant.logoURL;
            if(imgURL == nil) imgURL = self.order.merchant.imageURL;
            
            [self.restaurantLogoImageView sd_setImageWithURL:imgURL
                                         placeholderImage:[UIImage imageNamed:@"placeholder_logo"]
                                                  options:0
                                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                                    
                                                }];
        }
        
        
        
    } else {
        self.title = NSLocalizedString(@"Order", nil);
        
        [self.rapidROPanel removeFromSuperview];
        self.tableView.tableHeaderView = self.orderHeaderView;
        
        if(self.order.isCustomerClose){
            [self.activeDetailPanel removeFromSuperview];
            
            [self.ticketView addConstraints:
             [NSLayoutConstraint constraintsWithVisualFormat:@"V:[orderNoLabel]-8-|"
                                                     options:0
                                                     metrics:nil
                                                       views:@{@"orderNoLabel":self.orderNoLabel}]];
        }
        
        UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                            init];
        refreshControl.tintColor = [UIColor c11Color];
        [refreshControl addTarget:self
                           action:@selector(refreshControlChanged:)
                 forControlEvents:UIControlEventValueChanged];
        self.refreshControl = refreshControl;
        
        [self.progressButton0 applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateSelected];
        [self.progressButton1 applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateSelected];
        [self.progressButton2 applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateSelected];
        [self.progressButton3 applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateSelected];
        
        self.restaurantSummaryView.merchant = self.order.merchant;
        //    [self.restaurantSummaryView drawMerchant];
        [self.restaurantSummaryView drawMerchant:nil
                                     forceHeight:NO
                                      isDelivery:NO
                                       showPromo:NO
                                   showRoService:NO];
    }
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemBack
                                                                        target:self
                                                                        action:@selector(backButtonTouched:)];
    if(self.favoriteOrder != nil){
        [self fillFavoriteOrder];
    } else {
        [self fillOrder];
    }
}

// Save Rapid Order
- (void)saveButtonTouched:(id)sender
{
    if(![VALID validate:self.favoriteOrderNameTextField
                  title:NSLocalizedString(@"Oops!", nil)
                message:NSLocalizedString(@"Please add a title to your RapidOrder.", nil)]){
        return;
    }
    
    [self.view endEditing:YES];
    
    if(self.favoriteOrder == nil){
        if(self.order.orderType == OrderTypeDelivery){
            [self checkDeliveryAvailableAndOrder:YES];
        } else {
            [self requestNewFavoriteOrder];
        }
    } else {
        [self requestUpdateFavoriteOrder:FOupdateTypeMain];
    }
}

- (void)requestUpdateFavoriteOrder:(FOupdateType)type
{
    [self requestUpdateFavoriteOrder:type tempFavoriteOrder:nil];
}

- (void)requestUpdateFavoriteOrder:(FOupdateType)type tempFavoriteOrder:(FavoriteOrder *)tempFavoriteOrder
{
    FavoriteOrder *temp = nil;
    if(tempFavoriteOrder == nil){
        if(type == FOupdateTypeMain){
            temp = [self.favoriteOrder copy];
            temp.title = self.favoriteOrderNameTextField.text;
        }
    } else {
        temp = tempFavoriteOrder;
    }
    
    RequestResult result = RRFail;
    result = [PAY requestUpdateRapidReOrder:temp
                                       type:type
                            completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              [self.favoriteOrder updateWithDictionary:response];
                              
                              if(type == FOupdateTypeMain){
                                  self.favoriteOrder.image = nil;
                                  // Same name of profile picture
                                  [[SDWebImageManager sharedManager].imageCache removeImageForKey:[self.favoriteOrder.imageURL absoluteString]];
                                  [self.navigationController popViewControllerAnimated:YES];
                                  
                                  [[NSNotificationCenter defaultCenter] postNotificationName:FavoriteOrderUpdatedNotification
                                                                                      object:self
                                                                                    userInfo:@{@"FavoriteOrder":self.favoriteOrder,
                                                                                               @"IndexPath":self.indexPath}];
                              } else {
                                  [self.tableView reloadData];
                              }
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while creating RapidOrder", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)requestNewFavoriteOrder
{
    [self requestNewFavoriteOrder:nil];
}

- (void)requestNewFavoriteOrder:(CLLocation *)deliveryLocation
{
    RequestResult result = RRFail;
    result = [PAY requestCreateRapid:self.order.orderNo
                               title:self.favoriteOrderNameTextField.text
                               image:self.imageToBeApplied
                            location:deliveryLocation
                     completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              self.favoriteOrder = [[FavoriteOrder alloc] initWithDictionary:response];
                              
                              self.favoriteOrder.image = nil;
                              // Same name of profile picture
                              [[SDWebImageManager sharedManager].imageCache removeImageForKey:[self.favoriteOrder.imageURL absoluteString]];
                              
                              [[NSNotificationCenter defaultCenter] postNotificationName:FavoriteOrderInsertedNotification
                                                                                  object:self
                                                                                userInfo:@{@"FavoriteOrder":self.favoriteOrder}];
                              
                              [self dismissViewControllerAnimated:YES
                                                       completion:^{
                                                           //
                                                       }];
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while creating RapidOrder", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)refreshControlChanged:(UIRefreshControl *)refreshControl
{
    TRANS.dirty = YES;
    if(![TRANS requestMyOrder:self.order]){
        [self.refreshControl endRefreshing];
    }
}


- (void)backButtonTouched:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)doneButtonTouched:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)receiptButtonTouched:(id)sender
{
    [self pushReceiptViewController];
}

- (void)removeButtonTouched:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Your order will be removed. Do you want to proceed?", nil)
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"OK", nil)
                                                    otherButtonTitles:nil];
    actionSheet.tag = 103;
    [actionSheet showInView:self.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)fillFavoriteOrder
{
    [self configureDataForFavoriteOrder];
    UITableView *tableView = (UITableView *)self.view;
    
    UIView *tempView = tableView.tableHeaderView;
    tableView.tableHeaderView = nil;
    
    
    self.restaurantNameLabel.preferredMaxLayoutWidth = self.restaurantNameLabel.width;
    self.restaurantAddrLabel.preferredMaxLayoutWidth = self.restaurantAddrLabel.width;
    
    [tempView setNeedsUpdateConstraints];
    [tempView setNeedsDisplay];
    
    tempView.height = [tempView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    
    tableView.tableHeaderView = tempView;
    
    self.favoriteOrderNameTextField.text = self.favoriteOrder.title;
    [self.favoriteOrderImageView sd_setImageWithURL:self.favoriteOrder.imageURL
                                   placeholderImage:[UIImage imageNamed:@"placeholder_menu"]];
    [tableView reloadData];
}


- (void)fillOrder
{
    [self configureData];
    
    UITableView *tableView = (UITableView *)self.view;
    UIView *tempView = tableView.tableHeaderView;
    tableView.tableHeaderView = nil;
    
    if(tempView == self.rapidROPanel){
        
        self.restaurantNameLabel.preferredMaxLayoutWidth = self.restaurantNameLabel.width;
        self.restaurantAddrLabel.preferredMaxLayoutWidth = self.restaurantAddrLabel.width;
        
        [tempView setNeedsUpdateConstraints];
        [tempView setNeedsDisplay];
        
        tempView.height = [tempView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
        
    } else {
    
        if([self.order.tableNumber length] > 0){
            self.ticketNoLabel.text = NSLocalizedString(@"Table No.", nil);
            self.orderNoLabel.text = self.order.tableNumber;
            self.subTicketNoLabel.hidden = NO;
            self.subTicketNoTitleLabel.hidden = NO;
            self.subTicketNoLabel.text = [NSString stringWithLongLong:self.order.orderNo];
            self.progressButtonTopConstraint.constant = 38.0f;
        } else {
            self.subTicketNoLabel.hidden = YES;
            self.subTicketNoTitleLabel.hidden = YES;
            self.orderNoLabel.text = [NSString stringWithLongLong:self.order.orderNo];
            self.progressButtonTopConstraint.constant = 8.0f;
        }
        
        self.clearButton.hidden = YES;
        self.cancelButton.hidden = YES;
        self.rejectReasonLabel.text = nil;
        self.rejectReasonLabel.textColor = [UIColor c11Color];
        self.rejectLabelVSConstraint.constant = 6.0f;
        self.sorryIcon.hidden = YES;
        self.sorryLabel.hidden = YES;
        
        self.receiptBarButtonItem.enabled = !self.order.isTestMode;
        
        self.testIcon.hidden = !self.order.isTestMode;
        
        self.informationLabel.text = [self.order statusGuideStringWithMerchant:self.order.merchant fully:YES];
        
        if(self.order.orderType == OrderTypeDelivery || self.order.orderType == OrderTypeTakeout){
            if((self.order.status & (OrderStatusConfirmed | OrderStatusReady | OrderStatusFixed | OrderStatusCompleted)) == self.order.status){
                [self showExpectedTime:self.order.expectedDate];
            } else {
                [self showExpectedTime:nil];
            }
        } else {
            [self showExpectedTime:nil];
        }
        
        switch(self.order.status){
            case OrderStatusDraft:
                self.clearButton.hidden = NO;
                self.cancelButton.hidden = NO;

                if(CRED.isSignedIn && !REMOTE.isCardFetched){
                    self.clearButton.buttonTitle = NSLocalizedString(@"Fetching Cards...", nil);
                    self.clearButton.enabled = NO;
                } else {
                    self.clearButton.enabled = YES;
                    if([self.mobileCardList count] > 0){
                        self.clearButton.buttonTitle = NSLocalizedString(@"Pay With Card", nil);
                    } else {
                        if(self.order.merchant.isTestMode){
                            self.clearButton.buttonTitle = NSLocalizedString(@"Pay With Test Card", nil);
                        } else {
                            self.clearButton.buttonTitle = NSLocalizedString(@"Pay With New Card", nil);
                        }
                    }
                }
                
                if(CRED.isSignedIn){
                    self.mobileCardList = REMOTE.cards;
                } else {
                    self.mobileCardList = [MobileCard listAll];
                }
                
                break;
            case OrderStatusPaid:
            case OrderStatusConfirmed:
            case OrderStatusAccepted:
                if(self.order.orderType != OrderTypeCart){
                    if(self.order.didTimedOut){
                        if(!self.order.isCustomerClose){
                            self.clearButton.hidden = NO;
                            self.clearButton.buttonTitle = NSLocalizedString(@"Complete Order", nil);
                        }
                    }
                }
                break;
            case OrderStatusReady:
            case OrderStatusPrepared:
            case OrderStatusCompleted:
                // "Complete Order" button - wording is confusing / not clear / https://app.asana.com/0/11543106091665/49997038701433
//                if(!self.order.isCustomerClose){
//                    self.clearButton.hidden = NO;
//                    self.clearButton.buttonTitle = NSLocalizedString(@"Complete Order", nil);
//                }
                if(self.order.orderType != OrderTypeCart){
                    if(self.order.didTimedOut){
                        if(!self.order.isCustomerClose){
                            self.clearButton.hidden = NO;
                            self.clearButton.buttonTitle = NSLocalizedString(@"Complete Order", nil);
                        }
                    }
                }
                break;
            case OrderStatusFixed:
                if(!self.order.isCustomerClose){
                    self.clearButton.hidden = NO;
                    self.clearButton.buttonTitle = NSLocalizedString(@"Complete Order", nil);
                }
                break;
            case OrderStatusDeclined:
            case OrderStatusRefused:
                if(!self.order.isCustomerClose){
                    if(self.order.needToAdjustment){
                        self.clearButton.hidden = NO;
                        self.clearButton.buttonTitle = NSLocalizedString(@"Update Existing Order", nil);
                        
                        self.cancelButton.hidden = NO;
                        self.cancelButton.buttonTitle = NSLocalizedString(@"Cancel Order", nil);
                        
                    } else {
                        self.cancelButton.hidden = NO;
                        self.cancelButton.buttonTitle = NSLocalizedString(@"Ok. Got it!", nil);
                    }
                }
                self.rejectReasonLabel.text = self.order.rejectReason;
                self.rejectReasonLabel.textColor = [UIColor c14Color];
                self.rejectLabelVSConstraint.constant = 5.0f;
                self.sorryIcon.hidden = NO;
                self.sorryLabel.hidden = NO;
                break;
            case OrderStatusCanceled:
                break;
            default:
                break;
        }

        
        switch(self.order.orderType){
            case OrderTypeBill:
                self.progressButton0.buttonTitle = NSLocalizedString(@"Requested", nil);
                self.progressButton1.buttonTitle = NSLocalizedString(@"Issued", nil);
                break;
            case OrderTypeTakeout:
                self.progressButton2.buttonTitle = NSLocalizedString(@"Ready", nil);
                self.progressButton3.buttonTitle = NSLocalizedString(@"Picked Up", nil);
                break;
            case OrderTypeDelivery:
                self.progressButton2.buttonTitle = NSLocalizedString(@"Picked Up", nil);
                self.progressButton3.buttonTitle = NSLocalizedString(@"Delivered", nil);
                break;
            case OrderTypePickup:
                
                if([self.order.tableNumber length] > 0){
                    self.progressButton2.buttonTitle = NSLocalizedString(@"Serving", nil);
                    self.progressButton3.buttonTitle = NSLocalizedString(@"Completed", nil);
                } else {
                    self.progressButton2.buttonTitle = NSLocalizedString(@"Ready", nil);
                    self.progressButton3.buttonTitle = NSLocalizedString(@"Picked Up", nil);
                }
                
                break;
            case OrderTypeOrderOnly:
                self.progressButton2.buttonTitle = NSLocalizedString(@"Ready", nil);
                self.progressButton3.buttonTitle = NSLocalizedString(@"Completed", nil);
                break;
            default:
                self.progressButton2.buttonTitle = NSLocalizedString(@"Payable", nil);
                self.progressButton3.buttonTitle = NSLocalizedString(@"Completed", nil);
                break;
        }
        
        
        if(self.order.isTwoStepProcess){
            self.button2EqualConstraint.priority = 500;
            self.button2WidthConstraint.priority = 999;
            self.button3EqualConstraint.priority = 500;
            self.button3WidthConstraint.priority = 999;
            self.button2LeadingConstraint.constant = 0.0f;
            self.button3LeadingConstraint.constant = 0.0f;
        } else {
            self.button2EqualConstraint.priority = 999;
            self.button2WidthConstraint.priority = 500;
            self.button3EqualConstraint.priority = 999;
            self.button3WidthConstraint.priority = 500;
            self.button2LeadingConstraint.constant = -6.0f;
            self.button3LeadingConstraint.constant = -6.0f;
        }
        
        [self.progressButton2 setBackgroundImage:[UIImage imageNamed:@"progressbar_02_red"]
                                        forState:UIControlStateSelected];
        [self.progressButton3 setBackgroundImage:[UIImage imageNamed:@"progressbar_03_red"]
                                        forState:UIControlStateSelected];
        
        switch(self.order.status){
            case OrderStatusDraft:
                [self paintStepLabelWithStep:-1];
                break;
            case OrderStatusPaid:
                [self paintStepLabelWithStep:0];
                break;
            case OrderStatusConfirmed:
            case OrderStatusAccepted:
            case OrderStatusCompleted: // This is not completed status like on the server. - server completed is 'Fixed'
                [self paintStepLabelWithStep:1];
                break;
            case OrderStatusDeclined:
            case OrderStatusRefused:
                if(self.order.isTwoStepProcess){
                    [self paintStepLabelWithStep:3];
                } else {
                    [self paintStepLabelWithStep:1];
                }
                break;
            case OrderStatusReady:
            case OrderStatusPrepared:
                [self paintStepLabelWithStep:2];
                break;
            case OrderStatusFixed:
            case OrderStatusCanceled:
                [self paintStepLabelWithStep:3];
                break;
            default:
                [self paintStepLabelWithStep:-1];
                break;
        }
        
        if(self.order.isTwoStepProcess){
            if(self.order.status == OrderStatusDeclined){
                self.progressButton3.buttonTitle = NSLocalizedString(@"Declined", nil);
                [self.progressButton3 setBackgroundImage:[UIImage imageNamed:@"progressbar_03_declined"]
                                                forState:UIControlStateSelected];
            } else if(self.order.status == OrderStatusCanceled){
                self.progressButton3.buttonTitle = NSLocalizedString(@"Removed", nil);
                [self.progressButton3 setBackgroundImage:[UIImage imageNamed:@"progressbar_03_declined"]
                                                forState:UIControlStateSelected];
            } else {
                self.progressButton3.buttonTitle = NSLocalizedString(@"Confirmed", nil);
            }
        } else {
            if(self.order.status == OrderStatusDeclined){
                self.progressButton1.buttonTitle = NSLocalizedString(@"Declined", nil);
                [self.progressButton1 setBackgroundImage:[UIImage imageNamed:@"progressbar_02_declined"]
                                                forState:UIControlStateSelected];
            } else if(self.order.status == OrderStatusCanceled){
                self.progressButton2.buttonTitle = NSLocalizedString(@"Removed", nil);
                [self.progressButton2 setBackgroundImage:[UIImage imageNamed:@"progressbar_02_declined"]
                                                forState:UIControlStateSelected];
            }
        }
        
        [self.progressButton1 applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateSelected];
        [self.progressButton2 applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateSelected];
        [self.progressButton3 applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateSelected];
        
        [tempView updateConstraints];
        [tempView layoutIfNeeded];
        
        tempView.height = [tempView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    }
    
    tableView.tableHeaderView = tempView;
    
    tableView.tableFooterView = nil;
    if(self.clearButton.hidden){
        self.clearButtonHeightConstraint.constant = 0.0f;
        self.cancelTopHeightConstraint.constant = 0.0f;
    } else {
        self.clearButtonHeightConstraint.constant = 44.0f;
    }
    
    if(self.cancelButton.hidden){
        self.cancelButtonHeightConstraint.constant = 0.0f;
        self.cancelTopHeightConstraint.constant = 0.0f;
    } else {
        self.cancelButtonHeightConstraint.constant = 44.0f;
        self.cancelTopHeightConstraint.constant = 8.0f;
    }
    
//    if(!self.clearButton.hidden || !self.cancelButton.hidden){
        self.bottomFunctionView.height = [self.bottomFunctionView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
        tableView.tableFooterView = self.bottomFunctionView;
//    }
    
    [tableView reloadData];
}

- (void)showExpectedTime:(NSDate *)expectedTime
{
    if(expectedTime == nil){
        self.expectedTimeTitleLabel.text = nil;
        self.expectedTimeLabel.text = nil;
        self.activeDetailTopConstraint.constant = 0.0f;
    } else {
        if(self.order.orderType == OrderTypeTakeout){
            self.expectedTimeTitleLabel.text = NSLocalizedString(@"Estimated Pickup Time", nil);
        } else if(self.order.orderType == OrderTypeDelivery){
            self.expectedTimeTitleLabel.text = NSLocalizedString(@"Estimated Delivery Time", nil);
        }
//        [self.expectedTimeTitleLabel updateConstraints];
         
        self.expectedTimeLabel.text = [expectedTime hourMinuteString];
        self.activeDetailTopConstraint.constant = 6.0f;
    }
}

- (IBAction)clearThisOrderButtonTouched:(id)sender
{
    switch(self.order.status){
        case OrderStatusDraft:
            
            if(![ORDER isNearMerchant]){
                [UIAlertView alertWithTitle:NSLocalizedString(@"Too Far to Check out", nil)
                                    message:NSLocalizedString(@"You can check out at restaurant which is within 1 mile.", nil)];
                return;
            }
            
            if(self.order.orderType == OrderTypePickup || self.order.orderType == OrderTypeTakeout || self.order.orderType == OrderTypeDelivery){
                if(!self.order.merchant.isOpenHour){
                    [UIAlertView alertWithTitle:NSLocalizedString(@"Sorry, restaurant is currently closed!", nil)
                                        message:NSLocalizedString(@"Please feel free to browse the menu, but ordering isn't available until the restaurant opens.", nil)
                              cancleButtonTitle:NSLocalizedString(@"Got it!", nil)];
                    return;
                }
                
                if(self.order.orderType == OrderTypeDelivery){
                    if(!self.order.merchant.isDeliveryHour){
                        
                        NSString *hourString = [self.order.merchant nextDeliveryAvailableTime];
                        [UIAlertView alertWithTitle:NSLocalizedString(@"Delivery is Currently Unavailable", nil)
                                            message:[NSString stringWithFormat:NSLocalizedString(@"Next Available Time:\n%@", nil), [hourString length] > 0 ? hourString : NSLocalizedString(@"N/A", nil)]];
                        return;
                    }
                }
                
            }
            
            if(self.order.orderType == OrderTypeDelivery){
                [self checkDeliveryAvailableAndOrder];
            } else {
                [self proceedPayment];
            }
            
            break;
        case OrderStatusConfirmed:
        case OrderStatusAccepted:
        case OrderStatusPaid:
        case OrderStatusReady:
        case OrderStatusCompleted:
        case OrderStatusFixed:
        {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Would you like to complete this order?\nYou can still view all completed orders below in Past Orders.", nil)
                                                                     delegate:self
                                                            cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                       destructiveButtonTitle:NSLocalizedString(@"OK", nil)
                                                            otherButtonTitles:nil];
            actionSheet.tag = 104;
            [actionSheet showInView:self.view];
        }
            break;
        case OrderStatusDeclined:
        case OrderStatusRefused:
            // Adjusting - Menu View Controller
        {
            
            MenuListType neoMenuListType = MenuListTypeNone;
            CartType cartType = CartTypeUnknown;
            switch (self.order.orderType) {
                case OrderTypeTakeout:
                    neoMenuListType = MenuListTypeTakeout;
                    cartType = CartTypeTakeout;
                    break;
                case OrderTypeDelivery:
                    neoMenuListType = MenuListTypeDelivery;
                    cartType = CartTypeDelivery;
                    break;
                default:
                    neoMenuListType = MenuListTypeDinein;
                    if(self.order.merchant.isServicedByStaff){
                        cartType = CartTypePickupServiced;
                    } else {
                        cartType = CartTypePickup;
                    }
                    break;
            }
            
            if([MENUPAN resetGraphWithMerchant:self.order.merchant
                                      withType:neoMenuListType]){
                [MENUPAN requestMenusWithSuccess:^(id response) {
                    MenuPageViewController *viewController = [MenuPageViewController viewControllerFromNib];
                    viewController.initialCartType = cartType;
                    [ORDER resetGraphWithMerchant:self.order.merchant];
                    viewController.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:viewController animated:YES];
                    ORDER.order = self.order;
                    [ORDER.order isAllItemsInHour];
                } andFail:^(NSUInteger errorCode) {
                    PCError(@"Can't continue to push MenuViewController for requestMenusWithSuccess");
                    [UIAlertView alertWithTitle:@"Oops! Unexpected Error"
                                        message:@"Sorry, something seems a little buggy! Please try exiting the app and restarting. Please don't hesitate to reach out to us if the problem persists."];
                }];
            } else {
                
                MenuPageViewController *viewController = [MenuPageViewController viewControllerFromNib];
                viewController.initialCartType = cartType;
                [ORDER resetGraphWithMerchant:self.order.merchant];
                viewController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:viewController animated:YES];
                ORDER.order = self.order;
            }

        }
            break;
        default:
            self.informationLabel.text = NSLocalizedString(@"Error To Process.", nil);
            break;
    }
}

- (IBAction)cancelOrderButtonTouched:(id)sender
{
    switch(self.order.status){
        case OrderStatusDeclined:
        {
//            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Are you sure?", nil)
//                                                                     delegate:self
//                                                            cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
//                                                       destructiveButtonTitle:NSLocalizedString(@"Yes", nil)
//                                                            otherButtonTitles:nil];
//            actionSheet.tag = 901;
//            [actionSheet showInView:self.view];
            
            TRANS.dirty = YES;
//                [self requestRemoveOrder];
            [self requestCustomerCloseOrder];
        }
            break;
        default:
        {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Are you sure?", nil)
                                                                     delegate:self
                                                            cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                       destructiveButtonTitle:NSLocalizedString(@"Yes", nil)
                                                            otherButtonTitles:nil];
            actionSheet.tag = 902;
            [actionSheet showInView:self.view];
        }
            break;
    }
}

- (void)requestRemoveOrder
{
    RequestResult result = RRFail;
    result = [PAY requestRemoveOrder:self.order
                     completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              [self backButtonTouched:nil];
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while removing orders", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)requestCustomerCloseOrder
{
    RequestResult result = RRFail;
    result = [PAY requestCustomerCloseOrder:self.order.orderNo
                            completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              // TODO: Handle active order removing and adding to history like
                              // requestCustomerCloseOrder:(Order *)order at PaymentHistoryViewController
                              
                              if([response boolForKey:@"app_rating"]){
                                  [[NSNotificationCenter defaultCenter] postNotificationName:NeedToShowAppRatingNotification
                                                                                      object:self];
                              }
                              
                              [self backButtonTouched:nil];
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while canceling order", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)proceedPayment
{
    [self proceedPaymentWithDeliveryFee:0];
}

- (void)proceedPaymentWithDeliveryFee:(PCCurrency)deliveryFee
{
    Payment *payment = [[Payment alloc] init];
    if(self.order.orderType == OrderTypeDelivery){
        if(deliveryFee > 0){
            payment.deliveryFeeAmount = deliveryFee;
        }
        [payment applyRoServiceFee:self.order.merchant];
    }
    payment.merchantNo = self.order.merchantNo;
    payment.taxAmount = self.order.taxes;
    payment.subTotal = self.order.subTotal;
    payment.subTotalTaxable = self.order.subTotalTaxable;
    payment.orderAmount = payment.subTotal + payment.taxAmount;
    payment.payAmount = payment.orderAmount;
    payment.currency = FORMATTER.currencyFormatter.currencyCode;
    
    payment.payDate = [NSDate date];
    
    UIViewController *viewController = nil;
    
#if FLURRY_ENABLED
    [Flurry logEvent:@"Try to Pay(F)" withParameters:@{@"RestaurantName":ORDER.merchant.name,
                                                       @"RestaurantId":[NSNumber numberWithSerial:ORDER.merchant.merchantNo],
                                                       @"subTotal":[NSNumber numberWithCurrency:ORDER.order.subTotal],
                                                       @"subTotalTaxable":[NSNumber numberWithCurrency:ORDER.order.subTotalTaxable]}];
#endif
    
    if([self.mobileCardList count] > 0 || CRED.customerInfo.credit > 0){
        
        viewController = [PaymentMethodViewController viewControllerFromNib];
        ((PaymentMethodViewController *)viewController).payment = payment;
        ((PaymentMethodViewController *)viewController).paymentMethodList = self.mobileCardList;
    } else {
        viewController = [NewCardViewController viewControllerFromNib];
        ((NewCardViewController *)viewController).unintendedPresented = YES;
        ((NewCardViewController *)viewController).payment = payment;
    }
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)checkDeliveryAvailableAndOrder
{
    [self checkDeliveryAvailableAndOrder:NO];
}

- (void)checkDeliveryAvailableAndOrder:(BOOL)isRapid
{
    
#if DO_NOT_VALIDATE_DELIVERY
    PCCurrency feeAmount = 880;
    
    if(feeAmount != NSNotFound){
        if(self.payment.total >= self.order.merchant.deliveryBigOrderAmount && self.order.merchant.deliveryBigOrderAmount > 0){
            [self proceedPaymentWithDeliveryFee:self.order.merchant.deliveryFeeAmountForBigOrder];
        } else {
            [self proceedPaymentWithDeliveryFee:feeAmount];
        }
    }
#else
    
    NSDictionary *locationDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                        self.order.city, kABPersonAddressCityKey,
                                        @"United States", kABPersonAddressCountryKey,
                                        @"us", kABPersonAddressCountryCodeKey,
                                        self.order.state, kABPersonAddressStateKey,
                                        self.order.address1, kABPersonAddressStreetKey,
                                        self.order.zip, kABPersonAddressZIPKey,
                                        nil];
    
//    if(self.favoriteOrder != nil){
//        locationDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
//                              self.favoriteOrder.city, kABPersonAddressCityKey,
//                              @"United States", kABPersonAddressCountryKey,
//                              @"us", kABPersonAddressCountryCodeKey,
//                              self.favoriteOrder.state, kABPersonAddressStateKey,
//                              self.favoriteOrder.address1, kABPersonAddressStreetKey,
//                              self.favoriteOrder.zip, kABPersonAddressZIPKey,
//                              nil];
//    } else {
//        locationDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
//                              self.order.city, kABPersonAddressCityKey,
//                              @"United States", kABPersonAddressCountryKey,
//                              @"us", kABPersonAddressCountryCodeKey,
//                              self.order.state, kABPersonAddressStateKey,
//                              self.order.address1, kABPersonAddressStreetKey,
//                              self.order.zip, kABPersonAddressZIPKey,
//                              nil];
//    }
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    [self.geoCoder geocodeAddressDictionary:locationDictionary
                          completionHandler:^(NSArray *placemarks, NSError *error){
                              
                              [SVProgressHUD dismiss];
                              
                              if (error){
                                  PCError(@"Geocode failed with error: %@", error);
                                  [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot Locate Address", nil)
                                                      message:NSLocalizedString(@"Your address might be not valid or some other problems occured", nil)];
                                  return;
                              }
                              
//                              PCLog(@"Received placemarks: %@", placemarks);
                              
                              if([placemarks count] > 0){
                                  
                                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                                  
                                  PCCurrency feeAmount = [self.order.merchant deliveryFeeAt:placemark.location andSubtotal:NSNotFound];
                                  
                                  if(feeAmount != NSNotFound){
                                      if(isRapid){
                                          [self requestNewFavoriteOrder:placemark.location];
                                      } else {
                                          if(self.payment.total >= self.order.merchant.deliveryBigOrderAmount && self.order.merchant.deliveryBigOrderAmount > 0){
                                              [self proceedPaymentWithDeliveryFee:self.order.merchant.deliveryFeeAmountForBigOrder];
                                          } else {
                                              [self proceedPaymentWithDeliveryFee:feeAmount];
                                          }
                                      }
                                  } else {
                                      
                                      PCLog(@"Out of range for delivery");
                                      switch(self.order.merchant.deliveryFeePlan){
                                          case DeliveryFeePlanFlat:
                                          case DeliveryFeePlanRadius:
                                              [UIAlertView alertWithTitle:NSLocalizedString(@"Delivery Distance Limit", nil)
                                                                  message:NSLocalizedString(@"Sorry, we do not deliver to this location.", nil)];
                                              
                                              break;
                                          case DeliveryFeePlanZone:
                                              [UIAlertView alertWithTitle:NSLocalizedString(@"Outside of Delivery Area", nil)
                                                                  message:NSLocalizedString(@"Sorry, we do not deliver to this location.", nil)];
                                              break;
                                          default:
                                              [UIAlertView alertWithTitle:NSLocalizedString(@"Delivery Area Error", nil)
                                                                  message:NSLocalizedString(@"Sorry, we cannot decide this location is in the delivery area.", nil)];
                                              break;
                                      }
                                  }
                              }
                          }];
#endif
}

- (void)pushReceiptViewController
{
    ReceiptViewController *viewController = [ReceiptViewController viewControllerFromNib];
    viewController.payment = self.payment;
    viewController.mobileCard = self.mobileCard;
    viewController.backToRoot = NO;
    if(self.receipt == nil){
//        viewController.receipt = [REMOTE receiptWithOrderNo:self.order.orderNo];
        //
        
    } else {
        viewController.receipt = self.receipt;
    }
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)viewDidUnload {
    [self setOrderNoLabel:nil];
    [self setInformationLabel:nil];
    [self setClearButton:nil];
    
    [super viewDidUnload];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(actionSheet.tag){
        case 103:
        {
            if(buttonIndex == actionSheet.destructiveButtonIndex){
                Cart *cart = ORDER.cart;
                ORDER.cart = nil;
                TRANS.localDirty = YES;
                [cart delete];
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }
            break;
        case 104:
        {
            if(buttonIndex == actionSheet.destructiveButtonIndex){
                // Hidden Temporarily, Prevent to close by customer side
                [self requestCustomerCloseOrder];
                TRANS.dirty = YES;
            }
        }
            break;
        case 105:
        {
            if(actionSheet.cancelButtonIndex != buttonIndex){
                PCLog(@"actionSheet button index %d", buttonIndex);
                if(actionSheet.destructiveButtonIndex == buttonIndex){
                    if([self startCameraFromViewController:self
                                             usingDelegate:self]){
                    }
                } else if (buttonIndex == 1) { //Choose from album
                    if([self startMediaBrowserFromViewController:self
                                                   usingDelegate:self]){
                    }
                } else { //Index 2 Choose From Available Icons
                 
                    
                    [self performSegueWithIdentifier:@"photoSelectionSegue"
                                              sender:self];
                }
            }
        }
            break;
        case 901:
            if(buttonIndex == actionSheet.destructiveButtonIndex){
                TRANS.dirty = YES;
//                [self requestRemoveOrder];
                [self requestCustomerCloseOrder];
            }
            break;
        case 902:
            if(buttonIndex == actionSheet.destructiveButtonIndex){
                TRANS.dirty = YES;
                [self requestRemoveOrder];
//                [self requestCustomerCloseOrder];
            }
            break;
        default:
            break;
    }
}

- (void)paintStepLabelWithStep:(NSInteger)step
{
    self.progressButton0.selected = step >= 0;
    self.progressButton1.selected = step >= 1;
    self.progressButton2.selected = step >= 2;
    self.progressButton3.selected = step >= 3;
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.data count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger sectionType = [((NSNumber *)[self.data objectAtIndex:section]) integerValue];
    
    if(self.favoriteOrder != nil){
        switch(sectionType){
            case SectionLineItem:
                return [self.favoriteOrder.lineItems count];
                break;
            case SectionSubtotal:
                return 1;
                break;
            case SectionSpecialInstruction:
                return 1;
                break;
            case SectionPayment:
                return 1;
                break;
            case SectionDelivery:
                return 1;
                break;
        }
    } else {
        switch(sectionType){
            case SectionLineItem:
                return [self.order.lineItems count];
                break;
            case SectionSubtotal:
                return 1;
                break;
            case SectionSpecialInstruction:
                return 1;
                break;
            case SectionPayment:
                return [self.order.paymentList count];
                break;
            case SectionDelivery:
                return 1;
                break;
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger sectionType = [((NSNumber *)[self.data objectAtIndex:indexPath.section]) integerValue];
    
    switch(sectionType){
        case SectionLineItem:
        {
            static NSString * cellIdentifier = @"TicketOrderCell";
            
            TicketOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                                                    forIndexPath:indexPath];
            
            LineItem *lineItem = nil;
            
            if(self.favoriteOrder != nil){
                lineItem = [self.favoriteOrder.lineItems objectAtIndex:indexPath.row];
            } else {
                lineItem = [self.order.lineItems objectAtIndex:indexPath.row];
            }
            
            cell.lineItem = lineItem;
            
            [cell fillContentsWithOrderComment:nil];
            
            cell.menuNameLabel.preferredMaxLayoutWidth = cell.menuNameLabel.width;
            cell.qtyLabel.preferredMaxLayoutWidth = cell.qtyLabel.width;
            
            return cell;
        }
            break;
        case SectionSubtotal:
        {
            SubtotalTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SubtotalTableViewCell"
                                                                          forIndexPath:indexPath];
            if(self.favoriteOrder != nil){
                cell.favoriteOrder = self.favoriteOrder;
            } else {
                cell.order = self.order;
            }
            
            [cell drawData];
            
            return cell;
        }
            break;
        case SectionSpecialInstruction:
        {
            CustomerRequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomerRequestTableViewCell"
                                                                                 forIndexPath:indexPath];
            
            if(self.favoriteOrder != nil){
                cell.customerRequestLabel.text = self.favoriteOrder.customerRequest;
                cell.editButton.hidden = NO;
            } else {
                cell.customerRequestLabel.text = self.order.customerRequest;
                cell.editButton.hidden = YES;
            }
            cell.customerRequestLabel.preferredMaxLayoutWidth = cell.customerRequestLabel.width;
            
            [cell.contentView setNeedsUpdateConstraints];
            [cell.contentView updateConstraintsIfNeeded];
            
            return cell;
        }
            break;
        case SectionPayment:
        {
            PaymentInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PaymentInfoTableViewCell"
                                                                             forIndexPath:indexPath];
            
            if(self.favoriteOrder != nil){
                cell.cardInfoLabel.text = [NSString stringWithFormat:@"Card Type %@\nAcc No. %@",
                                           [NSString issuerName:self.favoriteOrder.card.cardIssuer],
                                           [self.favoriteOrder.card formattedCardNumber]];
                cell.refundImageView.hidden = YES;
                cell.editButton.hidden = NO;
                
            } else {
                Payment *payment = [self.order.paymentList objectAtIndex:indexPath.row];
                
                cell.cardInfoLabel.text = [NSString stringWithFormat:@"Card Type %@\nAcc No. %@",
                                           [NSString issuerName:payment.issuer],
                                           [payment formattedCardNumberWithSecureString:@"*"]];
                cell.refundImageView.hidden = (payment.status != PaymentStatusRefunded);
                cell.editButton.hidden = YES;
            }
            
            cell.receiptButton.hidden = !cell.editButton.hidden || self.isRapidRO;
            return cell;
        }
            break;
        case SectionDelivery:
        {
            DeliveryInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DeliveryInfoTableViewCell"
                                                                              forIndexPath:indexPath];
            
            if(self.favoriteOrder != nil){
                cell.deliveryAddressLabel.text = self.favoriteOrder.address;
                cell.editButton.hidden = NO;
            } else {
                cell.deliveryAddressLabel.text = self.order.address;
                cell.editButton.hidden = YES;
            }
            
            return cell;
        }
            break;
    }

    
    return nil; //Crash if reach here
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger sectionType = [((NSNumber *)[self.data objectAtIndex:indexPath.section]) integerValue];
    
    switch(sectionType){
        case SectionLineItem:
        {
            LineItem *lineItem = nil;
            
            if(self.favoriteOrder != nil){
                lineItem = [self.favoriteOrder.lineItems objectAtIndex:indexPath.row];
            } else {
                lineItem = [self.order.lineItems objectAtIndex:indexPath.row];
            }
            
            self.offScreenTicketOrderCell.lineItem = lineItem;
            [self.offScreenTicketOrderCell fillContentsWithOrderComment:nil];
            
            self.offScreenTicketOrderCell.menuNameLabel.preferredMaxLayoutWidth = self.offScreenTicketOrderCell.menuNameLabel.width;
            self.offScreenTicketOrderCell.qtyLabel.preferredMaxLayoutWidth = self.offScreenTicketOrderCell.qtyLabel.width;
            [self.offScreenTicketOrderCell.contentView updateConstraints];
            [self.offScreenTicketOrderCell.contentView setNeedsLayout];
            [self.offScreenTicketOrderCell.contentView layoutIfNeeded];
            return [self.offScreenTicketOrderCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height + 1.0f;
        }
            break;
        case SectionSubtotal:
        {
            NSString *reuseIdentifier = @"SubtotalTableViewCell";
            SubtotalTableViewCell *cell = [self.offscreenCells objectForKey:reuseIdentifier];
            if (!cell) {
                SubtotalTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
                [self.offscreenCells setObject:cell forKey:reuseIdentifier];
            }
            
            if(self.favoriteOrder != nil){
                cell.favoriteOrder = self.favoriteOrder;
            } else {
                cell.order = self.order;
            }
            
            [cell drawData];
            
            [cell setNeedsUpdateConstraints];
            [cell updateConstraintsIfNeeded];
            cell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(tableView.bounds));
            [cell setNeedsLayout];
            [cell layoutIfNeeded];
            
            return [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height + 1.0f;
        }
            break;
        case SectionSpecialInstruction:
        {
            CustomerRequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomerRequestTableViewCell"];

            if(self.favoriteOrder != nil){
                cell.customerRequestLabel.text = self.favoriteOrder.customerRequest;
            } else {
                cell.customerRequestLabel.text = self.order.customerRequest;
            }
            [cell.customerRequestLabel updateConstraints];
            
            cell.customerRequestLabel.preferredMaxLayoutWidth = cell.customerRequestLabel.width;
            
            [cell setNeedsUpdateConstraints];
            [cell updateConstraintsIfNeeded];
            [cell setNeedsLayout];
            [cell layoutIfNeeded];
            
            CGSize size = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
            
            return size.height + 1.0f;
        }
            break;
        case SectionPayment:
        {
            PaymentInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PaymentInfoTableViewCell"];
            
            if(self.favoriteOrder != nil){
                cell.cardInfoLabel.text = [NSString stringWithFormat:@"Card Type %@\nAcc No. %@",
                                           [NSString issuerName:self.favoriteOrder.card.cardIssuer],
                                           [self.favoriteOrder.card formattedCardNumber]];
            } else {
                Payment *payment = [self.order.paymentList objectAtIndex:indexPath.row];
                
                cell.cardInfoLabel.text = [NSString stringWithFormat:@"Card Type %@\nAcc No. %@",
                                           [NSString issuerName:payment.issuer],
                                           [payment formattedCardNumberWithSecureString:@"*"]];
            }
            
            [cell setNeedsUpdateConstraints];
            [cell updateConstraintsIfNeeded];
            [cell setNeedsLayout];
            [cell layoutIfNeeded];
            CGSize size = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
            return size.height + 1.0f;
        }
            break;
        case SectionDelivery:
        {
            DeliveryInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DeliveryInfoTableViewCell"];
            cell.delegate = self;
            
            if(self.favoriteOrder != nil){
                cell.deliveryAddressLabel.text = self.favoriteOrder.address;
            } else {
                cell.deliveryAddressLabel.text = self.order.address;
            }
            
            [cell setNeedsUpdateConstraints];
            [cell updateConstraintsIfNeeded];
            [cell setNeedsLayout];
            [cell layoutIfNeeded];
            return [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height + 1;
        }
            break;
    }
    
    return 0;
}

- (IBAction)cameraButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:@"Take a Picture"
                                                    otherButtonTitles:@"Choose From Album",
                                  @"Choose From Available Icons",
                                  nil];
    actionSheet.tag = 105;
    [actionSheet showInView:self.view];
}

- (void)cell:(DeliveryInfoTableViewCell *)cell didTouchEditButton:(NSIndexPath *)indexPath
{
    DeliveryViewController *viewController = [DeliveryViewController viewControllerFromNib];
    viewController.favoriteOrder = self.favoriteOrder;
    viewController.delegate = self;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^{
                                     
                                 }];
}

- (void)deliveryViewController:(DeliveryViewController *)viewController
      didTouchPlaceOrderButton:(id)sender
             addressDictionary:(NSDictionary *)dictionary
{
    FavoriteOrder *temp = [self.favoriteOrder copy];
    
    temp.pickupAfter = viewController.afterMinute;
    
    if(viewController.asapButton.selected){
        temp.pickupAfter = 0;
    }
    
    temp.address1 = viewController.address1TextField.text;
    temp.address2 = viewController.address2TextField.text;
    temp.zip = viewController.zipCodeTextField.text;
    temp.phoneNumber = viewController.phoneNumberTextField.text;
    temp.receiverName = viewController.nameTextField.text;
    temp.city = viewController.cityTextField.text;
    temp.state = viewController.stateTextField.text;
    
    temp.image = nil; // Make it not be updated Image
    
    temp.location = [dictionary objectForKey:@"deliveryLocation"];

    [self dismissViewControllerAnimated:YES
                                       completion:^{
                                           [self requestUpdateFavoriteOrder:FOupdateTypeAddress
                                                          tempFavoriteOrder:temp];
                                       }];
}



- (void)tableView:(UITableView *)tableView didTouchReceiptButtonAtIndexPath:(NSIndexPath *)indexPath
{
    Payment *payment = [self.order.paymentList objectAtIndex:indexPath.row];
 
    ReceiptViewController *viewController = [ReceiptViewController viewControllerFromNib];
    viewController.payment = payment;
    viewController.mobileCard = self.mobileCard;
    viewController.backToRoot = NO;
    if(self.receipt == nil){
        //        viewController.receipt = [REMOTE receiptWithOrderNo:self.order.orderNo];
        //
        
    } else {
        viewController.receipt = self.receipt;
    }
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)tableView:(UITableView *)tableView didTouchEditButtonAtIndexPath:(NSIndexPath *)indexPath
{
    //Card Selection View
    SelectCardViewController *viewController = [SelectCardViewController viewControllerFromNib];
    viewController.favoriteOrder = self.favoriteOrder;
    viewController.selectedCard = self.favoriteOrder.card;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^{
                                     
                                 }];
}

- (TicketOrderCell *)offScreenTicketOrderCell
{
    if(_offScreenTicketOrderCell == nil){
        _offScreenTicketOrderCell = [self.tableView dequeueReusableCellWithIdentifier:@"TicketOrderCell"];
    }
    return _offScreenTicketOrderCell;
}

- (CLGeocoder *)geoCoder
{
    if(_geoCoder == nil){
        _geoCoder = [[CLGeocoder alloc] init];
    }
    return _geoCoder;
}

- (BOOL)startMediaBrowserFromViewController: (UIViewController*) controller
                              usingDelegate: (id <UIImagePickerControllerDelegate,
                                              UINavigationControllerDelegate>) delegate {
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypePhotoLibrary] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.navigationBar.tintColor = [UIColor c12Color];
    mediaUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    // Displays saved pictures and movies, if both are available, from the
    // Camera Roll album.
    mediaUI.mediaTypes =
    [UIImagePickerController availableMediaTypesForSourceType:
     UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    mediaUI.allowsEditing = YES;
    
    mediaUI.delegate = delegate;
    
    [controller presentViewController:mediaUI
                             animated:YES
                           completion:^{
                               
                           }];
    return YES;
}

- (BOOL) startCameraFromViewController: (UIViewController*) controller
                         usingDelegate: (id <UIImagePickerControllerDelegate,
                                         UINavigationControllerDelegate>) delegate {
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeCamera] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    // Displays saved pictures and movies, if both are available, from the
    // Camera Roll album.
    mediaUI.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    mediaUI.allowsEditing = YES;
    
    mediaUI.delegate = delegate;
    
    [controller presentViewController:mediaUI
                             animated:YES
                           completion:^{
                               
                           }];
    return YES;
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    UIImage *originalImage, *editedImage;
    
    // Handle a still image picked from a photo album
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0)
        == kCFCompareEqualTo) {
        
        editedImage = (UIImage *) [info objectForKey:
                                   UIImagePickerControllerEditedImage];
        
        originalImage = (UIImage *) [info objectForKey:
                                     UIImagePickerControllerOriginalImage];
        
        if (editedImage) {
            self.imageToBeApplied = [editedImage thumbnailImage:(106 * 3)
                                              transparentBorder:0
                                                   cornerRadius:0
                                           interpolationQuality:kCGInterpolationHigh];
        } else {
            self.imageToBeApplied = [originalImage thumbnailImage:(106 * 3)
                                              transparentBorder:0
                                                   cornerRadius:0
                                           interpolationQuality:kCGInterpolationHigh];
        }
        
        self.favoriteOrder.image = self.imageToBeApplied;
    }
    
    self.favoriteOrderImageView.image = self.imageToBeApplied;
    self.favoriteOrderImageView.hidden = NO;
    self.favoriteOrderImageView.alpha = 0.0;
    [picker dismissViewControllerAnimated:YES
                               completion:^{
                                   [UIView animateWithDuration:0.4f
                                                    animations:^{
                                                        self.favoriteOrderImageView.alpha = 1.0;
                                                    }];
                               }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES
                               completion:^{
                                   
                               }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.destinationViewController isKindOfClass:[UINavigationController class]]){
        
        UIViewController *rootViewController = ((UINavigationController *)segue.destinationViewController).rootViewController;
        if([rootViewController isKindOfClass:[CustomerRequestViewController class]]){
            CustomerRequestViewController *viewController = (CustomerRequestViewController *)rootViewController;
            viewController.favoriteOrder = self.favoriteOrder;
        } else if([rootViewController isKindOfClass:[CloudPhotoCollectionViewController class]]){
            CloudPhotoCollectionViewController *viewController = (CloudPhotoCollectionViewController *)rootViewController;
            viewController.delegate = self;
        }
        
    } else {
        
    }
}
- (IBAction)callHelpLineButtonTouched:(id)sender {
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://424-488-3170"]]){
        if(![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://424-488-3170"]]){
            // User's cancel
        }
    } else {
        [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to Call",nil)
                            message:NSLocalizedString(@"Try to call manually", nil)];
    }
}

- (IBAction)liveChatButtonTouched:(id)sender {
    if(CRED.isSignedIn){
        [SKTUser currentUser].firstName = CRED.customerInfo.firstName;
        [SKTUser currentUser].lastName = CRED.customerInfo.lastName;
        [SKTUser currentUser].email = CRED.customerInfo.email;
        [[SKTUser currentUser] addProperties:@{ @"Customer" : [NSString stringWithFormat:@"https://www.rushorderapp.com/customers/customers/%lld", CRED.customerInfo.customerNo]
                                                , @"phoneNumber" : CRED.customerInfo.phoneNumber ? CRED.customerInfo.phoneNumber : @"Unknown"
                                                , @"birthday" : CRED.customerInfo.birthday ? CRED.customerInfo.birthday : @"Unknown"
                                                , @"UUID": [NSString stringWithFormat:@"https://www.rushorderapp.com/douglas/guests/%@", CRED.udid]
                                                }];
    } else {
        [SKTUser currentUser].firstName = [NSUserDefaults standardUserDefaults].custName ? [NSUserDefaults standardUserDefaults].custName : @"?";
        [SKTUser currentUser].lastName = @"Guest";
        [SKTUser currentUser].email = [NSUserDefaults standardUserDefaults].emailAddress;
        [[SKTUser currentUser] addProperties:@{ @"phoneNumber" : [NSUserDefaults standardUserDefaults].custPhone ? [NSUserDefaults standardUserDefaults].custPhone : @"Unknown"
                                                , @"UUID": [NSString stringWithFormat:@"https://www.rushorderapp.com/douglas/guests/%@", CRED.udid]
                                                }];
    }



    Order *order = self.order;
    
    [[SKTUser currentUser] addProperties:@{ @"Merchant" : order.merchantName
                                            ,@"Order" : [NSString stringWithFormat:@"https://www.rushorderapp.com/douglas/orders/%lld", order.orderNo]
                                            }];
    
    
    
    [Smooch show];
}

- (void)cloudPhotoCollectionViewController:(CloudPhotoCollectionViewController *)viewController
                            didSelectPhoto:(NSDictionary *) dictionary
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 PCLog(@"Dictionary %@", dictionary);
                                 
                                 self.favoriteOrderImageView.hidden = NO;
                                 [self.favoriteOrderImageView sd_setImageWithURL:[NSURL URLWithString:[[dictionary objectForKey:@"image"] objectForKey:@"url"]]
                                                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                           self.imageToBeApplied = image;
                                                                           self.favoriteOrder.image = image;
                                                                       }];
                                 // sd_setImageWithURL:[NSURL URLWithString:[[aPhotoDict objectForKey:@"image"] objectForKey:@"url"]]];
                                 //                                 [self.favoriteOrder.image sd_se
                                 
                                 
//                                 [[self.favoriteOrderImageView sd_setImageWithURL:[NSURL URLWithString:[[dictionary objectForKey:@"image"] objectForKey:@"url"]]]
//                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                                 self.favoriteOrder.image = image;
//                             }];
//                                  self.favoriteOrderImageView.hidden = NO;
//                                  self.favoriteOrderImageView.alpha = 0.0;
                             }];
}



@end
