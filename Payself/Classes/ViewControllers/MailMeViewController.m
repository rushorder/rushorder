//
//  MailMeViewController.m
//  RushOrder
//
//  Created by Conan on 11/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MailMeViewController.h"
#import "PCPaymentService.h"
#import "PCCredentialService.h"

@interface MailMeViewController ()

@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UILabel *autoGuideLabel;
@property (weak, nonatomic) IBOutlet UILabel *setGuideLabel;
@property (strong, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@end

@implementation MailMeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"E-mail Receipt", nil);
    
    self.autoGuideLabel.text = NSLocalizedString(@"Turn on auto receipt e-mailing to have all future receipts automatically forwarded to the e-mail address above.", nil);
    self.setGuideLabel.text = NSLocalizedString(@"You can change the email address by going to Settings at any time.", nil);
    
    if(self.presentingViewController != nil){
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemClose
                                                                              target:self
                                                                              action:@selector(cancelButtonTouched:)];
    } else {
        self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemSave
                                                                              target:self
                                                                              action:@selector(saveButtonTouched:)];
    }
    
    self.doneButton.hidden = (self.presentingViewController == nil);
    
    self.mailTextField.text = [NSUserDefaults standardUserDefaults].emailAddress;
    
    if([self.mailTextField.text length] == 0){
        if(CRED.isSignedIn){
            self.mailTextField.placeholder = CRED.customerInfo.email;
            self.mailTextField.text = CRED.customerInfo.email;
        }
    }
    
    [self.scrollView setContentSizeWithBottomView:self.doneButton];
}

- (void)cancelButtonTouched:(id)sender
{
    if(self.presentingViewController != nil){
        [self dismissViewControllerAnimated:YES
                                 completion:^{}];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)saveButtonTouched:(id)sender
{
    if(![VALID validate:self.mailTextField
                  title:NSLocalizedString(@"Please provide your E-mail Address.", nil)
                message:nil]){
        return;
    }
    
    
    if([self.mailTextField.text length] > 0) {
        if(![VALID validateEmail:self.mailTextField
                           title:NSLocalizedString(@"Invalid Email",nil)
                         message:NSLocalizedString(@"Please enter a valid email address", nil)]){
            return;
        }
    }
    
    
    
    if([self.delegate respondsToSelector:@selector(mailMeViewController:didTouchDoneButton:)]){
        [self.delegate mailMeViewController:self
                         didTouchDoneButton:sender];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sendButton:(id)sender
{
    if([self.delegate respondsToSelector:@selector(mailMeViewController:didTouchDoneButton:)]){
        [self.delegate mailMeViewController:self
                         didTouchDoneButton:sender];
    }
}


@end
