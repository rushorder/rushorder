//
//  TablesetViewController.m
//  RushOrder
//
//  Created by Conan on 2/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "TablesetViewController.h"
#import "BillViewController.h"
#import "PCPaymentService.h"
#import "Order.h"
#import "MenuPageViewController.h"
#import "OrderManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "RestaurantSummaryView.h"
#import "TransactionManager.h"

typedef enum tableNumberStatus_{
    TableNumberStatusInitial = 0,
    TableNumberStatusFocused ,
    TableNumberStatusInvalidTable ,
    TableNumberStatusNoOrder ,
    TableNumberStatusActiveOrder,
    TableNumberStatusCompletedOrder,
    TableNumberStatusWaitBill
} TableNumberStatus;

@interface TablesetViewController ()
{
    CGFloat topOfTableNoTextField;
    BOOL manuallyCheckTable;
}


@property (weak, nonatomic) IBOutlet RestaurantSummaryView *restaurantSummaryView;
@property (weak, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *buttonsContainer;
@property (weak, nonatomic) IBOutlet NPStretchableButton *billRequestButton;
@property (weak, nonatomic) IBOutlet PCTextField *tableNumberTextField;
@property (weak, nonatomic) IBOutlet UILabel *guideLabel;
@property (strong, nonatomic) IBOutlet UIPickerView *tableNumberPickerView;
@property (strong, nonatomic) IBOutlet UIToolbar *tableNumberToolBar;


@property (weak, nonatomic) IBOutlet UIBarButtonItem *toggleBarButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneBarButtonItem;

@property (weak, nonatomic) IBOutlet UILabel *moreGuideLabel;
@property (nonatomic, getter = isDirty) BOOL dirty;

@end

@implementation TablesetViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if(self){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.dirty = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(merchantUpdated:)
                                                 name:MerchantInformationUpdatedNotification
                                               object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)merchantUpdated:(NSNotification *)notification
{
    BOOL invalidMerchant = [notification.userInfo boolForKey:@"InvalidMerchant"];
    if(invalidMerchant){
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else {
        // TODO: if the restaurant enable menu orfder -> change view to menu order
        [self.restaurantSummaryView drawMerchant];
    }
}

- (void)tableListChanged:(NSNotification *)notification
{
    [self.tableNumberPickerView reloadAllComponents];
    
    if([self.tableNumberTextField.text length] == 0){
        if([ORDER.tableList count] > 0){
            TableInfo *firstTableInfo = [ORDER.tableList objectAtIndex:0];
            self.tableNumberTextField.text = firstTableInfo.tableNumber;
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Request Bill", nil);
    
//    [self.guideLabel drawBorder];
    
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Search", nil)
//                                                                             target:self
//                                                                             action:@selector(backButtonTouched:)];
    
//    [self drawMerchant];
    
    self.restaurantSummaryView.merchant = ORDER.merchant;
    [self.restaurantSummaryView drawMerchant];
    
    self.tableNumberTextField.inputView = self.tableNumberPickerView;
    self.tableNumberTextField.inputAccessoryView = self.tableNumberToolBar;
    
    topOfTableNoTextField = self.tableNumberTextField.origin.y;
        
//    [self requestTableLables];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [ORDER requestMerchantInfo];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(self.isDirty){
        [self taransitViewsByStatus:TableNumberStatusInitial];
    }
    [self.scrollView setContentSizeWithBottomView:self.buttonsContainer];
    self.scrollView.adjustingOffsetY = -60.0f;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orderConfirmed:)
                                                 name:OrderConfirmedNotificationKey
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:OrderConfirmedNotificationKey
                                                  object:nil];
}

- (void)orderConfirmed:(NSNotification *)notification
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)backButtonTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)requestTheBill
{    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *deviceToken = userDefaults.deviceToken;
    
    RequestResult result = RRFail;
    result = [PAY requestTheBill:ORDER.merchant.merchantNo
                     tableNumber:self.tableNumberTextField.text
                     deviceToken:deviceToken
                 completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){

                      switch(response.errorCode){
                          case ResponseSuccess:
                              [self taransitViewsByStatus:TableNumberStatusWaitBill];
                              break;                          
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:                              
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while requesting the bull", nil)];
                              }
                          }
                              break;
                      }
                      
                      
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (IBAction)requestBillButtonTouched:(id)sender
{
    if(![ORDER isNearMerchant]){
        [UIAlertView alertWithTitle:NSLocalizedString(@"It is too far to reqeust bill", nil)
                            message:NSLocalizedString(@"You can request bill at restaurant which is within 1 mile.", nil)];
        return;
    }
    
    if(!ORDER.merchant.isOpenHour){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Sorry, restaurant is currently closed!", nil)
                            message:NSLocalizedString(@"Please feel free to browse the menu, but ordering isn't available until the restaurant opens.", nil)
                  cancleButtonTitle:NSLocalizedString(@"Got it!", nil)];
        return;
    }
    
    if(![VALID validate:self.tableNumberTextField
                message:NSLocalizedString(@"Table number should be entered.", nil)]){
        return;
    }
    
    [self.view endEditing:YES];
    
    if(manuallyCheckTable){
        [self requestTheBill];
    } else {
        [self checkThisTableButtonTouched:sender];
    }
}


- (void)pushBillViewController
{
    BillViewController *viewController = [BillViewController viewControllerFromNib];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    ORDER.order = nil;
    [self taransitViewsByStatus:TableNumberStatusFocused];
    
//    if([ORDER.tableList count] == 0){
//        [self requestTableLables];
//    }
}

- (IBAction)checkThisTableButtonTouched:(id)sender
{
    if([self.tableNumberTextField.text length] == 0){
        [self taransitViewsByStatus:TableNumberStatusInitial];
        [self.tableNumberTextField resignFirstResponder];
    }
    
    NSString *inputTableNumber = self.tableNumberTextField.text;
    
    RequestResult result = RRFail;
    result = [PAY requestCheckTable:ORDER.merchant.merchantNo
                        tableNumber:inputTableNumber
                    completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      [self.tableNumberTextField resignFirstResponder];
                      
                      if(response){ //Success
                          BOOL validTable = YES;
                          
                          switch(response.errorCode){
                              case ResponseErrorEmpty:
                              case ResponseSuccess:
                                  ORDER.order = [[Order alloc] initWithDictionary:response.data
                                                                         merchant:ORDER.merchant];
                                  //Fixed and Canceled order removed in service layer
                                  break;
                              case ResponseErrorInvalid:
                                  //                                  ORDER.merchant.numberOfTables = NSNotFound;
                                  validTable = NO;
                                  break;
                              case ResponseErrorGeneral:{
                                  NSString *message = [response objectForKey:@"message"];
                                  if([message isKindOfClass:[NSString class]]){
                                      [UIAlertView alert:message];
                                  }
                                  break;
                              }
                              default:
                                  validTable = NO;
                              {
                                  NSString *message = [response objectForKey:@"message"];
                                  if([message isKindOfClass:[NSString class]]){
                                      [UIAlertView alert:message];
                                  } else {
                                      [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while checking table status", nil)];
                                  }
                              }
                                  break;
                          }
                          
                          if(validTable){
                              if(ORDER.order.orderNo == 0){
                                  ORDER.order.tableNumber = inputTableNumber;
                                  [self taransitViewsByStatus:TableNumberStatusNoOrder];
                                  
                                  if(sender == self.billRequestButton){
                                      [self requestTheBill];
                                  } else {
                                      PCWarning(@"sender is not self.billRequestButton");
                                  }
                              } else {
                                  // Order exists
                                  if(ORDER.order.status == OrderStatusCompleted){
                                      [self taransitViewsByStatus:TableNumberStatusCompletedOrder];
                                      // Block requesting if not yet reset table
                                      //                                      if(sender == self.billRequestButton){
                                      //                                          [self requestTheBill];
                                      //                                      }
                                  } else {
                                      [self taransitViewsByStatus:TableNumberStatusActiveOrder];
                                      [self pushBillViewController];
                                  }
                              }
                          } else {
                              [self taransitViewsByStatus:TableNumberStatusInvalidTable];
                          }
                      } else {
                          [UIAlertView alert:NSLocalizedString(@"Error has occurred while checking table status", nil)];
                          //                          [self taransitViewsByStatus:TableNumberStatusInvalidTable];
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
            break;
        default:
            break;
    }    
}

- (void)taransitViewsByStatus:(TableNumberStatus)tableStatus
{
    self.dirty = NO;
    
    if(!manuallyCheckTable){
        
        self.billRequestButton.hidden = NO;

    } else {
        
        self.billRequestButton.hidden = (tableStatus == TableNumberStatusInitial ||
                                         tableStatus == TableNumberStatusFocused ||
                                         tableStatus == TableNumberStatusInvalidTable ||
                                         tableStatus == TableNumberStatusActiveOrder ||
                                         tableStatus == TableNumberStatusCompletedOrder);
    }

    
    if(tableStatus == TableNumberStatusWaitBill){
        self.billRequestButton.buttonTitle = NSLocalizedString(@"Request Bill Again.", nil);
    } else {
        self.billRequestButton.buttonTitle = NSLocalizedString(@"Request Bill", nil);
    }
    
    self.guideLabel.textColor = [UIColor darkGrayColor];
    NSString *msg = nil;
    
    if(tableStatus == TableNumberStatusWaitBill){
        msg = NSLocalizedString(@"Bill request delivered.\nWe will notify you once bill is issued.", nil);
        self.guideLabel.text = msg;
    } else if(tableStatus == TableNumberStatusInvalidTable){
        msg = NSLocalizedString(@"Table number is invalid.", nil);
        self.guideLabel.textColor = [UIColor redColor];
        if(ORDER.merchant.numberOfTables == NSNotFound){
            self.guideLabel.text = msg;
        } else {
            self.guideLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Table number is invalid.\nThe last table number is %d", nil), ORDER.merchant.numberOfTables];
        }
    } else if(tableStatus == TableNumberStatusCompletedOrder){
        self.guideLabel.textColor = [UIColor redColor];
        msg = NSLocalizedString(@"Prior Table Orders Not Settled.\nPlease wait for the restaurant to clear prior table orders. If table is not cleared for an extended period of time, please ask restaurant staff for assistance", nil);
            self.guideLabel.text = msg;
    } else {
        self.guideLabel.text = NSLocalizedString(@"Your Table Number", nil);
    }
    
#define BUTTON_SPAN 6.0f
    
    [UIView animateWithDuration:0.2f
                     animations:
     ^(){
    
     }];    
    
}

- (void)viewDidUnload
{
    [self setTableNumberPickerView:nil];
    [self setTableNumberToolBar:nil];
    [self setToggleBarButtonItem:nil];
    [self setDoneBarButtonItem:nil];
    [self setGuideLabel:nil];
    [self setMoreGuideLabel:nil];
    [super viewDidUnload];
}

#pragma mark - UIPickerViewDataSource
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [ORDER.tableList count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if([ORDER.tableList count] > row){
        TableInfo *table = [ORDER.tableList objectAtIndex:row];
        return table.tableNumber;
    }
    
    return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if([ORDER.tableList count] > row){
        TableInfo *table = [ORDER.tableList objectAtIndex:row];
        self.tableNumberTextField.text = table.tableNumber;
    }
}

//- (void)requestTableLables
//{
//    RequestResult result = RRFail;
//    result = [PAY requestTableLabels:ORDER.merchant.merchantNo
//                          completionBlock:
//              ^(BOOL isSuccess, NSMutableArray *tableList, NSInteger statusCode){
//                  
//                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
//                      ORDER.tableList = tableList;
//                      [self.tableNumberPickerView reloadAllComponents];
//                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
//                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
//                  } else {
//                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
//                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
//                                                                              object:nil];
//                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
//                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
//                                              ,statusCode]];
//                      }
//                  }
//                  [SVProgressHUD dismiss];
//              }];
//    switch(result){
//        case RRSuccess:
//            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
//            break;
//        case RRParameterError:
//            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
//            break;
//        default:
//            break;
//    }
//}

- (IBAction)toggleButtonTouched:(id)sender
{
    [self.tableNumberTextField resignFirstResponder];
    
    if(self.tableNumberTextField.inputView != nil){
        self.tableNumberTextField.inputView = nil;
        self.toggleBarButtonItem.title = NSLocalizedString(@"Picker", nil);
    } else {
        self.tableNumberTextField.inputView = self.tableNumberPickerView;
        self.toggleBarButtonItem.title = NSLocalizedString(@"Keyboard", nil);
    }
    
    [self.tableNumberTextField becomeFirstResponder];
}


- (IBAction)doneButtonTouched:(id)sender
{
    if(self.tableNumberTextField.inputView == nil){
    } else {
        NSInteger selRow = [self.tableNumberPickerView selectedRowInComponent:0];
        
        if([ORDER.tableList count] > selRow){
            TableInfo *table = [ORDER.tableList objectAtIndex:selRow];
            self.tableNumberTextField.text = table.tableNumber;
        }
    }
    [self.tableNumberTextField resignFirstResponder];
}

- (IBAction)selectMenuButtonTouched:(id)sender
{
    MenuPageViewController *viewController = [MenuPageViewController viewControllerFromNib];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];

}
@end
