//
//  PaymentHistoryViewController.m
//  RushOrder
//
//  Created by Conan on 2/22/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PaymentHistoryViewController.h"
#import "Receipt.h"
#import "Payment.h"
#import "PaymentCell.h"
#import "ReceiptViewController.h"
#import "RemoteDataManager.h"
#import "PCCredentialService.h"
#import "PCPaymentService.h"
#import "TransactionManager.h"
#import "BillViewController.h"
#import "OrderManager.h"
#import "MenuManager.h"
#import "MenuPageViewController.h"
#import "TogoAuthViewController.h"
#import "NoOrdersForFavoriteOrder.h"
#import "ProfileViewController.h"
#import "AccountSwitchingManager.h"
#import "RatingView.h"

@interface PaymentHistoryViewController ()

@property (strong, nonatomic) NSMutableArray *activeOrders;
//@property (strong, nonatomic) NSMutableArray *activityHistory;
@property (strong, nonatomic) UILabel *noItemLabel;
@property (strong, nonatomic) UIImageView *noItemImageView;
@property (strong, nonatomic) UILabel *noItemDescLabel;
@property (strong, nonatomic) UIView *footerView;
@property (strong, nonatomic) ProcessingCell *offScreenProcessingCell;
@property (strong, nonatomic) PaymentCell *offScreenPaymentCell;
@property (nonatomic) CartType selectedCartType;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (strong, nonatomic) IBOutlet UIView *completedHeaderView;

@property (nonatomic) NSInteger selectedTransactionIndex;

@property (nonatomic, getter = isDirty) BOOL dirty;

@property (strong, nonatomic) IBOutlet UIView *moreLoadingView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (strong, nonatomic) IBOutlet UILabel *loadingLabel;

@property (nonatomic) CGFloat selectedRating;
@property (nonatomic) BOOL shouldShowAppRating;
@end

@implementation PaymentHistoryViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.dirty = YES;
    
    [REMOTE addObserver:self
           forKeyPath:@"ordersProgressing"
              options:NSKeyValueObservingOptionNew
              context:NULL];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqual:@"ordersProgressing"]){
        
        NSInteger count = [REMOTE.orders count] + (self.isRapidRO ? [TRANS.activeOrders count] : [TRANS.transactions count]);
        
        // Show more loading indicator only when activityHistory is not zero. Because transactions has no paging.
        if(count == 0){
            if(self.isRapidRO){
                NoOrdersForFavoriteOrder *view = [NoOrdersForFavoriteOrder view];
                [view.startButton addTarget:self
                                     action:@selector(startButtonTouched:)
                           forControlEvents:UIControlEventTouchUpInside];
                self.tableView.tableFooterView = view;
            } else {
                self.tableView.tableFooterView = self.footerView;
            }
        } else {
            if([REMOTE.orders count] > 0){
                if(REMOTE.isMoredata){
                    self.tableView.tableFooterView = self.moreLoadingView;
                } else {
                    self.tableView.tableFooterView = nil;
                }
            } else {
                self.tableView.tableFooterView = nil;
            }
        }
        
        if([[change objectForKey:NSKeyValueChangeNewKey] boolValue]){
            self.loadingIndicator.hidden = NO;
            [self.loadingIndicator startAnimating];
            self.loadingLabel.text = NSLocalizedString(@"Loading...", nil);
        } else {
            [self.loadingIndicator stopAnimating];
            self.loadingIndicator.hidden = YES;
            if(REMOTE.isMoredata){
                self.loadingLabel.text = NSLocalizedString(@"Pull Up To Load More", nil);
            } else {
                self.loadingLabel.text = NSLocalizedString(@"End of the List", nil);
            }
        }
    }
}

- (void)startButtonTouched:(id)sender
{
    if(self.isRapidRO){
        [self dismissViewControllerAnimated:YES
                                 completion:^{
                                     [APP.tabBarController setSelectedIndex:0];
                                     UINavigationController *tab1NavigationController = (UINavigationController *)APP.tabBarController.viewControllers[0];
                                     [tab1NavigationController popToRootViewControllerAnimated:YES];
                                 }];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate
{
    if(scrollView == self.tableView){
        //        PCLog(@"ScrollView %@", scrollView);
        
        if(scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.bounds.size.height) + 50.0f){
            if(REMOTE.isMoredata){
                [REMOTE requestOrdersList];
            }
        }
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [REMOTE removeObserver:self
                forKeyPath:@"ordersProgressing"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(self.isRapidRO){
        self.title = NSLocalizedString(@"Select Order", nil);
        
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemCancel
                                                                            target:self
                                                                            action:@selector(close:)];
        
    } else {
        self.title = NSLocalizedString(@"Orders", nil);
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(signedOut:)
                                                     name:SignedOutNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(signedIn:)
                                                     name:SignedInNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(needToShowAppRatingNotified:)
                                                     name:NeedToShowAppRatingNotification
                                                   object:nil];
        
    }
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                        init];
    refreshControl.tintColor = [UIColor c11Color];
    [refreshControl addTarget:self
                       action:@selector(refreshControlChanged:)
             forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
}

- (void)needToShowAppRatingNotified:(NSNotification *)aNoti
{
    self.shouldShowAppRating = YES;
}

- (void)close:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 //
                             }];
}

- (void)transactionListChanged:(NSNotification *)noti
{
    [self.refreshControl endRefreshing];

//    TableSection *section = nil;
//
//    if([self.sectionList count] > 0)
//        section = [self.sectionList objectAtIndex:0];
//
//    [self configureSection];

    if(noti.userInfo == nil){
        [self.tableView reloadData];
    } else {

        NSDictionary *userInfo = noti.userInfo;


        NSArray *inserted = [userInfo objectForKey:@"insertedKey"];
        NSArray *removed = [userInfo objectForKey:@"removedKey"];
        NSArray *updated = [userInfo objectForKey:@"updatedKey"];

        [self.tableView beginUpdates];

        NSIndexPath *focusIndexPath = nil;

        UITableViewScrollPosition position = UITableViewScrollPositionTop;

        if([inserted count] > 0){
            [self.tableView insertRowsAtIndexPaths:inserted
                                  withRowAnimation:UITableViewRowAnimationLeft];
            focusIndexPath = [inserted objectAtIndex:0];
        }

        if([removed count] > 0){
            [self.tableView deleteRowsAtIndexPaths:removed
                                  withRowAnimation:UITableViewRowAnimationBottom];
        }

        if([updated count] > 0){
            [self.tableView reloadRowsAtIndexPaths:updated
                                  withRowAnimation:UITableViewRowAnimationFade];
            if(focusIndexPath == nil)
                focusIndexPath = [updated objectAtIndex:0];
        }

        [self.tableView endUpdates];

        [self performSelector:@selector(tableAnimationDone:)
                   withObject:self
                   afterDelay:0.5f];
        
        if(focusIndexPath == nil){

        } else {
            [self.tableView scrollToRowAtIndexPath:focusIndexPath
                                  atScrollPosition:position
                                          animated:YES];
        }
    }
    
    if([REMOTE.orders count] > 0){
        if(REMOTE.isMoredata){
            self.tableView.tableFooterView = self.moreLoadingView;
        } else {
            self.tableView.tableFooterView = nil;
        }
    } else {
        NSInteger count = [REMOTE.orders count] + [TRANS.transactions count];
        if(count == 0){
            if(self.isRapidRO){
                NoOrdersForFavoriteOrder *view = [NoOrdersForFavoriteOrder view];
                [view.startButton addTarget:self
                                     action:@selector(startButtonTouched:)
                           forControlEvents:UIControlEventTouchUpInside];
                self.tableView.tableFooterView = view;
            } else {
                self.tableView.tableFooterView = self.footerView;
            }
        } else {
            self.tableView.tableFooterView = nil;
        }
    }
}

- (void)tableAnimationDone:(id)sender
{
    if(self.isVisible && CRED.isUnattendedSignUp){
        //Show edit profile
        [UIAlertView askWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Now, you have account <%@> set", nil), CRED.customerInfo.email]
                          message:NSLocalizedString(@"Would you like to edit your profile now?", nil)
                         delegate:self
                              tag:301];
        CRED.unattendedSignUp = NO;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 301:
            if(buttonIndex == AlertButtonIndexYES){
                //Show Profile Edit Window
                ProfileViewController *viewController = [ProfileViewController viewControllerFromNib];
                [self.tabBarController presentViewControllerInNavigation:viewController
                                                                animated:YES
                                                              completion:^{
                                                                  
                                                              }];
            }
            break;
        case 302: // ExternalRating
            if(buttonIndex == AlertButtonIndexYES){
                [self sendAppRating:5 rate:self.selectedRating comment:nil];
            } else {
                [self sendAppRating:4 rate:self.selectedRating comment:nil];
            }
            break;
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ordersListChanged:)
                                                 name:OrdersListChangeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orderConfirmed:)
                                                 name:OrderConfirmedNotificationKey
                                               object:nil];
    
    if(!self.isRapidRO){
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(transactionListChanged:)
                                                     name:TransactionListChangedNotification
                                                   object:nil];
    }
    
    if(self.isDirty){
//        self.activityHistory = REMOTE.orders;
        self.dirty = NO;
    }
    
    [self.tableView reloadData];
    
    if([REMOTE.orders count] > 0){
        if(REMOTE.isMoredata){
            self.tableView.tableFooterView = self.moreLoadingView;
        } else {
            self.tableView.tableFooterView = nil;
        }
    } else {
        NSInteger count = [REMOTE.orders count] + (self.isRapidRO ? [TRANS.activeOrders count] : [TRANS.transactions count]);
        if(count == 0){
            if(self.isRapidRO && REMOTE.isOrdersFetched){
                NoOrdersForFavoriteOrder *view = [NoOrdersForFavoriteOrder view];
                [view.startButton addTarget:self
                                     action:@selector(startButtonTouched:)
                           forControlEvents:UIControlEventTouchUpInside];
                self.tableView.tableFooterView = view;
            } else {
                self.tableView.tableFooterView = self.footerView;
            }
        } else {
            self.tableView.tableFooterView = nil;
        }
    }
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:OrdersListChangeNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:OrderConfirmedNotificationKey
                                                  object:nil];
    
    if(!self.isRapidRO){
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:TransactionListChangedNotification
                                                      object:nil];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [TRANS requestMyOrders];
    
    if(self.shouldShowAppRating){
        [self startGettingRate];
        self.shouldShowAppRating = NO;
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
#ifdef FLURRY_ENABLED
    [Flurry endTimedEvent:NSStringFromClass([self class])
           withParameters:nil];
#endif
}

- (void)orderConfirmed:(NSNotification *)notification
{
    [TRANS requestMyOrders];
}

- (void)ordersListChanged:(NSNotification *)notification
{
    [self.refreshControl endRefreshing];
    
//    self.activityHistory = REMOTE.orders;

    [self.tableView reloadData];
    
    if([REMOTE.orders count] > 0){
        if(REMOTE.isMoredata){
            self.tableView.tableFooterView = self.moreLoadingView;
        } else {
            self.tableView.tableFooterView = nil;
        }
    } else {
        NSInteger count = [REMOTE.orders count] + (self.isRapidRO ? [TRANS.activeOrders count] : [TRANS.transactions count]);
        if(count == 0){
            if(self.isRapidRO){
                NoOrdersForFavoriteOrder *view = [NoOrdersForFavoriteOrder view];
                [view.startButton addTarget:self
                                     action:@selector(startButtonTouched:)
                           forControlEvents:UIControlEventTouchUpInside];
                self.tableView.tableFooterView = view;
            } else {
                self.tableView.tableFooterView = self.footerView;
            }
        } else {
            self.tableView.tableFooterView = nil;
        }
    }
}

- (void)signedOut:(NSNotification *)notification
{
    self.dirty = YES;
    [self ordersListChanged:notification];
}

- (void)signedIn:(NSNotification *)notification
{
    self.dirty = YES;
    [self ordersListChanged:notification];
}

- (void)refreshControlChanged:(UIRefreshControl *)refreshControl
{
    REMOTE.ordersDirty = YES;
    REMOTE.currentPage = 0;
    TRANS.dirty = YES;
    if(![TRANS requestMyOrders]){
        [self.refreshControl endRefreshing];
    }
    if(![REMOTE requestOrdersList]){
        [self.refreshControl endRefreshing];
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch(section){
        case 0:
            return (self.isRapidRO ? [TRANS.activeOrders count] : [TRANS.transactions count]);
            break;
        case 1:
            return [REMOTE.orders count];
            break;
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id orderOrCart = nil;
        
    switch(indexPath.section){
        case 0:
        {
            if(self.isRapidRO){
                orderOrCart = [TRANS.activeOrders objectAtIndex:indexPath.row];
            } else {
                orderOrCart = [TRANS.transactions objectAtIndex:indexPath.row];
            }
        }
            break;
        case 1:
        {
            orderOrCart = [REMOTE.orders objectAtIndex:indexPath.row];
        }
            break;
    }
    
    CGFloat extMargin = 0.0f;
    
    if([tableView respondsToSelector:@selector(layoutMargins)]){
        extMargin = tableView.layoutMargins.left + tableView.layoutMargins.right - 16.0f;
    }
    
    if(indexPath.section == 0 && !self.isRapidRO){
        self.offScreenProcessingCell.bounds = CGRectMake(0.0f, 0.0f,
                                                         CGRectGetWidth(tableView.bounds) - extMargin,
                                                         CGRectGetHeight(self.offScreenProcessingCell.bounds));
        [self.offScreenProcessingCell updateConstraints];
        
        self.offScreenProcessingCell.orderOrCart = orderOrCart;
        
        [self.offScreenProcessingCell fillContentsForHeight:YES];
        
        self.offScreenProcessingCell.merchantNameLabel.preferredMaxLayoutWidth = self.offScreenProcessingCell.merchantNameLabel.width;
        self.offScreenProcessingCell.addressLabel.preferredMaxLayoutWidth = self.offScreenProcessingCell.addressLabel.width;
        self.offScreenProcessingCell.itemNameLabel.preferredMaxLayoutWidth = self.offScreenProcessingCell.itemNameLabel.width;
        self.offScreenProcessingCell.guideLabel.preferredMaxLayoutWidth = self.offScreenProcessingCell.guideLabel.width;
        
        [self.offScreenProcessingCell setNeedsUpdateConstraints];
        [self.offScreenProcessingCell updateConstraintsIfNeeded];
        [self.offScreenProcessingCell setNeedsLayout];
        [self.offScreenProcessingCell layoutIfNeeded];
        
        CGSize size = [self.offScreenProcessingCell.contentView
                       systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        
        return size.height + 1.0;
    } else {
        self.offScreenPaymentCell.bounds = CGRectMake(0.0f, 0.0f,
                                                  CGRectGetWidth(self.tableView.bounds) - extMargin,
                                                  CGRectGetHeight(_offScreenPaymentCell.bounds));
        [self.offScreenPaymentCell updateConstraints];
        [self.offScreenPaymentCell setNeedsUpdateConstraints];
        [self.offScreenPaymentCell updateConstraintsIfNeeded];
        [self.offScreenPaymentCell setNeedsLayout];
        [self.offScreenPaymentCell layoutIfNeeded];
        
        self.offScreenPaymentCell.order = orderOrCart;
        [self.offScreenPaymentCell fillContentsForHeight:YES];

        
        self.offScreenPaymentCell.restaurantLabel.preferredMaxLayoutWidth = self.offScreenPaymentCell.restaurantLabel.width;
        self.offScreenPaymentCell.addressLabel.preferredMaxLayoutWidth = self.offScreenPaymentCell.addressLabel.width;
        self.offScreenPaymentCell.lineItemNamesLabel.preferredMaxLayoutWidth = self.offScreenPaymentCell.lineItemNamesLabel.width;
        
        
        [self.offScreenPaymentCell setNeedsUpdateConstraints];
        [self.offScreenPaymentCell updateConstraintsIfNeeded];
        [self.offScreenPaymentCell setNeedsLayout];
        [self.offScreenPaymentCell layoutIfNeeded];
        
        CGSize size = [self.offScreenPaymentCell.contentView
                       systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        
        return size.height + 1.0;
    }

    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    id orderOrCart = nil;
    
        switch(indexPath.section){
            case 0:
            {
                if(self.isRapidRO){
                    orderOrCart = [TRANS.activeOrders objectAtIndex:indexPath.row];
                } else {
                    orderOrCart = [TRANS.transactions objectAtIndex:indexPath.row];
                }
            }
                break;
            case 1:
            {
                orderOrCart = [REMOTE.orders objectAtIndex:indexPath.row];
            }
                break;
            default:
                return nil;
                break;
        }
    
    if(indexPath.section == 0 && !self.isRapidRO){
        static NSString *CellIdentifier = @"ProcessingCell";
        ProcessingCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier
                                                               forIndexPath:indexPath];
        
        cell.orderOrCart = orderOrCart;
        [cell fillContents];
        
        cell.delegate = self;
        
        [cell setNeedsUpdateConstraints];
        [cell updateConstraintsIfNeeded];
        [cell setNeedsLayout];
        [cell layoutIfNeeded];
        
        // This code should be located after setNeedsUpdateConstraints,updateConstraintsIfNeeded,setNeedsLayout,layoutIfNeeded
        // Because width of label is adjusted after those code.
        cell.guideLabel.preferredMaxLayoutWidth = cell.guideLabel.width;
        cell.merchantNameLabel.preferredMaxLayoutWidth = cell.merchantNameLabel.width;
        cell.addressLabel.preferredMaxLayoutWidth = cell.addressLabel.width;
        cell.itemNameLabel.preferredMaxLayoutWidth = cell.itemNameLabel.width;
        
        return cell;
    } else {
        static NSString *CellIdentifier = @"PaymentCell";
        PaymentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier
                                                            forIndexPath:indexPath];
        
        cell.order = orderOrCart;
        
        cell.restaurantLabel.userInteractionEnabled = !self.isRapidRO;
        cell.addressLabel.userInteractionEnabled = !self.isRapidRO;
        [cell fillContents];
        
        [cell setNeedsUpdateConstraints];
        [cell updateConstraintsIfNeeded];
        [cell setNeedsLayout];
        [cell layoutIfNeeded];
        
        cell.restaurantLabel.preferredMaxLayoutWidth = cell.restaurantLabel.width;
        cell.addressLabel.preferredMaxLayoutWidth = cell.addressLabel.width;
        cell.lineItemNamesLabel.preferredMaxLayoutWidth = cell.lineItemNamesLabel.width;
        
        return cell;
    }
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0 && !self.isRapidRO){
        [self pushActiveOrderAtIndex:indexPath.row];
    } else {
        Order *order = nil;
        switch(indexPath.section){
            case 0:
                if(self.isRapidRO){
                    order = [TRANS.activeOrders objectAtIndex:indexPath.row];
                } else {
                    order = [TRANS.transactions objectAtIndex:indexPath.row];
                }
                break;
            case 1:
                order = [REMOTE.orders objectAtIndex:indexPath.row];
                break;
        }
        [self pushActiveOrder:order];
    }
    
    
    [self.tableView deselectRowAtIndexPath:indexPath
                                  animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(self.isRapidRO){
        return nil;
    } else {
        switch(section){
            case 0:
            {
                return nil;
            }
                break;
            case 1:
            {
                if([REMOTE.orders count] > 0){
                    return self.completedHeaderView;
                } else {
                    return nil;
                }
            }
                break;
            default:
                return nil;
                break;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(self.isRapidRO){
        return 0.0f;
    } else {
        switch(section){
            case 0:
            {
                return 0.0f;
            }
                break;
            case 1:
            {
                if([REMOTE.orders count] > 0){
                    return 36.0f;
                } else {
                    return 0.0f;
                }
            }
                break;
            default:
                return 0.0f;
                break;
        }
    }
}

- (void)pushActiveOrderAtIndex:(NSInteger)index
{
    id orderOrCart = nil;
    
    if(self.isRapidRO){
        orderOrCart = [TRANS.activeOrders objectAtIndex:index];
    } else {
        orderOrCart = [TRANS.transactions objectAtIndex:index];
    }
    
    [self pushActiveOrder:orderOrCart];
}

- (void)pushActiveOrder:(id)orderOrCart
{
    Merchant *merchant = nil;
    
    if([orderOrCart isKindOfClass:[Order class]]){
        Order *order = (Order *)orderOrCart;
        
        merchant = order.merchant;
        
        TableInfo *tableInfo = nil;
        
        if(order.status == OrderStatusFixed && (order.orderType == OrderTypeCart || order.orderType == OrderTypeOrderOnly)){
            [UIAlertView alertWithTitle:NSLocalizedString(@"Completed", nil)
                                message:NSLocalizedString(@"Your order is complete.", nil)];
            [self.tableView deselectRowAtIndexPath:self.selectedIndexPath
                                          animated:YES];
        } else if(order.status == OrderStatusCanceled){
            [UIAlertView alertWithTitle:NSLocalizedString(@"Removed Order", nil)
                                message:NSLocalizedString(@"Your order was removed by restaurant. Please ask restaurant staff for assistance.", nil)];
            [self.tableView deselectRowAtIndexPath:self.selectedIndexPath
                                          animated:YES];
        } else {
            switch(order.orderType){
                case OrderTypeBill:
                {
                    BillViewController *viewController = [BillViewController viewControllerFromNib];
                    [ORDER resetGraphWithMerchant:order.merchant];
                    ORDER.order = order;
                    [MENUPAN resetGraphWithMerchant:order.merchant];
                    
                    [self.navigationController pushViewController:viewController
                                                         animated:YES];
                }
                    break;
                case OrderTypeCart:
                case OrderTypeOrderOnly:
                    tableInfo = [[TableInfo alloc] initWithTableLabel:order.tableNumber
                                                           merchantNo:merchant.merchantNo];
                    self.selectedCartType = CartTypeCart;
                    [self pushMenuViewController:merchant
                                   withTableInfo:tableInfo
                                withMenuListType:MenuListTypeDinein];
                    break;
                case OrderTypePickup:
                case OrderTypeTakeout:
                case OrderTypeDelivery:                    
                    [self pushTogoAuthViewController:merchant
                                           withOrder:order];
                    break;
                default:
                    // ??
                    break;
            }
        }
    } else if([orderOrCart isKindOfClass:[Cart class]]){
        Cart *cart = (Cart *)orderOrCart;
        
        merchant = cart.merchant;
        
        if(merchant == nil){
            PCError(@"Merchant removed in local cart");
            [cart delete];
            self.localDirty = YES;
            [self.tableView reloadData];
        } else {
            if(cart.isLocal){
                
                self.selectedCartType = cart.cartType;
                [self pushMenuViewController:merchant orderOrCart:cart];
                
            } else {
                TableInfo *tableInfo = [[TableInfo alloc] initWithTableLabel:cart.tableLabel
                                                                  merchantNo:merchant.merchantNo];
                self.selectedCartType = cart.cartType;
                [self pushMenuViewController:merchant
                               withTableInfo:tableInfo];
            }
        }
    }
    
    [self.searchDisplayController setActive:NO animated:YES];
}

- (void)pushMenuViewController:(Merchant *)merchant
                   orderOrCart:(id)orderOrCart
{
    [self pushMenuViewController:merchant
                   withTableInfo:nil
                     orderOrCart:orderOrCart
                withMenuListType:MenuListTypeAll];
}

- (void)pushMenuViewController:(Merchant *)merchant
{
    [self pushMenuViewController:merchant
                   withTableInfo:nil
                withMenuListType:MenuListTypeAll];
}

- (void)pushMenuViewController:(Merchant *)merchant
              withMenuListType:(MenuListType)menuListType
{
    [self pushMenuViewController:merchant
                   withTableInfo:nil
                withMenuListType:menuListType];
}

- (void)pushMenuViewController:(Merchant *)merchant
                 withTableInfo:(TableInfo *)tableInfo
{
    [self pushMenuViewController:merchant
                   withTableInfo:tableInfo
                withMenuListType:MenuListTypeAll];
}

- (void)pushMenuViewController:(Merchant *)merchant
                 withTableInfo:(TableInfo *)tableInfo
              withMenuListType:(MenuListType)menuListType
{
    [self pushMenuViewController:merchant
                   withTableInfo:tableInfo
                     orderOrCart:nil
                withMenuListType:menuListType];
}

- (void)pushMenuViewController:(Merchant *)merchant
                 withTableInfo:(TableInfo *)tableInfo
                   orderOrCart:(id)orderOrCart
              withMenuListType:(MenuListType)menuListType
{
    BOOL pushMenuView = YES;
    
    MenuListType neoMenuListType = MenuListTypeNone;
    
    switch (self.selectedCartType) {
        case CartTypeTakeout:
            neoMenuListType = MenuListTypeTakeout;
            break;
        case CartTypeDelivery:
            neoMenuListType = MenuListTypeDelivery;
            break;
        default:
            neoMenuListType = MenuListTypeDinein;
            break;
    }
    
    if([MENUPAN resetGraphWithMerchant:merchant
                              withType:neoMenuListType]){
        
        if(pushMenuView){
            [MENUPAN requestMenusWithSuccess:^(id response) {
                
                MenuPageViewController *viewController = [MenuPageViewController viewControllerFromNib];
                viewController.initialCartType = self.selectedCartType;
                [ORDER resetGraphWithMerchant:merchant];
                if(orderOrCart != nil){
                    if([orderOrCart isKindOfClass:[Order class]]){
                        ORDER.order = orderOrCart;
                        [ORDER.order isAllItemsInHour];
                    } else if ([orderOrCart isKindOfClass:[Cart class]]){
                        ORDER.cart = orderOrCart;
                        [ORDER.cart isAllItemsInHour];
                    }
                }
                
                viewController.tableInfo = tableInfo;
                viewController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:viewController animated:YES];
                
            } andFail:^(NSUInteger errorCode) {
                PCError(@"Can't continue to push MenuViewController for requestMenusWithSuccess");
                [UIAlertView alertWithTitle:@"Oops! Unexpected Error"
                                    message:@"Sorry, something seems a little buggy! Please try exiting the app and restarting. Please don't hesitate to reach out to us if the problem persists."];
            }];
        }
    } else {
        if(pushMenuView){
            MenuPageViewController *viewController = [MenuPageViewController viewControllerFromNib];
            viewController.initialCartType = self.selectedCartType;
            [ORDER resetGraphWithMerchant:merchant];
            if(orderOrCart != nil){
                if([orderOrCart isKindOfClass:[Order class]]){
                    ORDER.order = orderOrCart;
                    [ORDER.order isAllItemsInHour];
                } else if ([orderOrCart isKindOfClass:[Cart class]]){
                    ORDER.cart = orderOrCart;
                    [ORDER.cart isAllItemsInHour];
                }
            }
            viewController.tableInfo = tableInfo;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
}

- (void)pushTogoAuthViewController:(Merchant *)merchant withOrder:(Order *)order
{
    [self performSegueWithIdentifier:@"TicketPushSegue"
                              sender:[NSArray arrayWithObjects:order,merchant, nil]];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if(indexPath.section == 1){
        return UITableViewCellEditingStyleDelete;
    } else {
        return UITableViewCellEditingStyleNone;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 1){
        return NSLocalizedString(@"Remove", nil);
    } else {
        return nil;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(UITableViewCellEditingStyleDelete){
        Order *order = [REMOTE.orders objectAtIndex:indexPath.row];
        if(order != nil){
//            if(CRED.isSignedIn){
                self.selectedIndexPath = indexPath;
                [self requestRemoveOrder:order];
//            } else {
//                if([order delete]){
//                    [REMOTE.orders removeObjectAtIndex:indexPath.row];
//                    [self.tableView deleteRowsAtIndexPaths:@[indexPath]
//                                          withRowAnimation:UITableViewRowAnimationLeft];
//                    
//                    NSInteger count = [REMOTE.orders count] + [TRANS.transactions count];
//                    if(count == 0){
//                        self.tableView.tableFooterView = self.footerView;
//                    }
//                }
//            }
        }
    }
}

- (void)requestRemoveOrder:(Order *)order
{
    RequestResult result = RRFail;
    result = [PAY requestRemoveOrder:order
                     completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              switch(self.selectedIndexPath.section){
                                  case 0:
                                      [TRANS.transactions removeObjectAtIndex:self.selectedIndexPath.row];
                                      break;
                                  case 1:
                                      [REMOTE.orders removeObjectAtIndex:self.selectedIndexPath.row];
                                      break;
                              }
                              [self.tableView deleteRowsAtIndexPaths:@[self.selectedIndexPath]
                                                    withRowAnimation:UITableViewRowAnimationLeft];
                              
                              if([REMOTE.orders count] > 0){
                                  if(REMOTE.isMoredata){
                                      self.tableView.tableFooterView = self.moreLoadingView;
                                  } else {
                                      self.tableView.tableFooterView = nil;
                                  };
                              } else {
                                  NSInteger count = [REMOTE.orders count] + (self.isRapidRO ? [TRANS.activeOrders count] : [TRANS.transactions count]);
                                  if(count == 0){
                                      if(self.isRapidRO){
                                          NoOrdersForFavoriteOrder *view = [NoOrdersForFavoriteOrder view];
                                          [view.startButton addTarget:self
                                                               action:@selector(startButtonTouched:)
                                                     forControlEvents:UIControlEventTouchUpInside];
                                          self.tableView.tableFooterView = view;
                                      } else {
                                          self.tableView.tableFooterView = self.footerView;
                                      }
                                  } else {
                                      self.tableView.tableFooterView = nil;
                                  }
                              }
                          }
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while removing orders", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}


- (UIView *)footerView
{
    if(_footerView == nil){
        _footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 280.0f, 300.0f)];
        _footerView.backgroundColor = [UIColor clearColor];
        [_footerView addSubview:self.noItemLabel];
        [_footerView addSubview:self.noItemImageView];
        
        [_footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[noItemImageView]-20-[noItemLabel]-(>=0)-|"
                                                                            options:NSLayoutFormatAlignAllCenterX
                                                                            metrics:nil
                                                                              views:@{@"noItemLabel":self.noItemLabel,
                                                                                      @"noItemImageView":self.noItemImageView}]];
        
        [_footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[noItemImageView(==133.0)]"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:@{@"noItemImageView":self.noItemImageView}]];
        
        [_footerView addConstraint:[NSLayoutConstraint constraintWithItem:self.noItemImageView
                                                                attribute:NSLayoutAttributeCenterX
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:_footerView
                                                                attribute:NSLayoutAttributeCenterX
                                                               multiplier:1.0f
                                                                 constant:0]];
    }
    
    return _footerView;
}

- (void)scrollToTop
{
    [self.tableView setContentOffset:CGPointZero
                            animated:YES];
}

- (UIImageView *)noItemImageView
{
    if(_noItemImageView == nil){
        _noItemImageView = [[UIImageView alloc] initWithFrame:CGRectMake(77.0f,
                                                                         40.0f,
                                                                         133.0f,
                                                                         133.0f)];
        _noItemImageView.translatesAutoresizingMaskIntoConstraints = NO;
        
        _noItemImageView.image = [UIImage imageNamed:@"icon_no_activity"];
    }
    return _noItemImageView;
}

- (UILabel *)noItemLabel
{
    if(_noItemLabel == nil){
        _noItemLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 224.0f, 280.0f, 40.0f)];
        _noItemLabel.font = [UIFont systemFontOfSize:18.0f];
        _noItemLabel.textAlignment = NSTextAlignmentCenter;
        _noItemLabel.numberOfLines = 0;
        _noItemLabel.textColor = [UIColor colorWithR:163.0f G:163.0f B:163.0f];
        _noItemLabel.shadowColor = [UIColor whiteColor];
        _noItemLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
        _noItemLabel.backgroundColor = [UIColor clearColor];
        _noItemLabel.text = NSLocalizedString(@"No Orders", nil);
        
        _noItemLabel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    return _noItemLabel;
}

- (UILabel *)noItemDescLabel
{
    if(_noItemDescLabel == nil){
        _noItemDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 270.0f, 280.0f, 100.0f)];
        _noItemDescLabel.font = [UIFont systemFontOfSize:13.0f];
        _noItemDescLabel.textAlignment = NSTextAlignmentCenter;
        _noItemDescLabel.numberOfLines = 0;
        _noItemDescLabel.textColor = [UIColor colorWithR:163.0f G:163.0f B:163.0f];
        _noItemDescLabel.shadowColor = [UIColor whiteColor];
        _noItemDescLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
        _noItemDescLabel.backgroundColor = [UIColor clearColor];
        _noItemDescLabel.text = NSLocalizedString(@"Each of your transactions will be listed here.", nil);
        [_noItemDescLabel sizeToHeightFit];
    }
    
    return _noItemDescLabel;
}

- (ProcessingCell *)offScreenProcessingCell
{
    if(_offScreenProcessingCell == nil){
        _offScreenProcessingCell = [self.tableView dequeueReusableCellWithIdentifier:@"ProcessingCell" ];
    }
    return _offScreenProcessingCell;
}

- (PaymentCell *)offScreenPaymentCell
{
    if(_offScreenPaymentCell == nil){
        _offScreenPaymentCell = [self.tableView dequeueReusableCellWithIdentifier:@"PaymentCell" ];
    }
    return _offScreenPaymentCell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.destinationViewController isKindOfClass:[TogoAuthViewController class]]){
        TogoAuthViewController *viewController = (TogoAuthViewController *)segue.destinationViewController;
        
        NSArray *param = (NSArray *)sender;
        
        Order *order = [param objectAtIndex:0];
        Merchant *merchant = nil;
        if([param count] > 1){
            merchant = [param objectAtIndex:1];
        }
        
        [ORDER resetGraphWithMerchant:merchant];
        [MENUPAN resetGraphWithMerchant:merchant];
        ORDER.order = order;
        viewController.payment = nil;
        viewController.order = order;
        viewController.rapidRO = self.isRapidRO;
    }
}

- (void)tableView:(UITableView *)tableView cell:(ProcessingCell *)cell didTouchProcessButtonAtIndexPath:(NSIndexPath *)indexPath
{
    id orderOrCart = nil;
    
    if(self.isRapidRO){
        orderOrCart = [TRANS.activeOrders objectAtIndex:indexPath.row];
    } else {
        orderOrCart = [TRANS.transactions objectAtIndex:indexPath.row];
    }
    
    if([orderOrCart isKindOfClass:[Order class]]){
        Order *order = (Order *)orderOrCart;
        
        if(order.status == OrderStatusDeclined){
            // Give a chance to adjust and re place order
            switch(order.orderType){
                case OrderTypePickup:
                    self.selectedCartType = CartTypePickup;
                    break;
                case OrderTypeTakeout:
                    self.selectedCartType = CartTypeTakeout;
                    break;
                case OrderTypeDelivery:
                    self.selectedCartType = CartTypeDelivery;
                    break;
                default:
                    // Nothing
                    break;
            }
            [self pushMenuViewController:order.merchant orderOrCart:order];

            
        } else {
            [self pushActiveOrderAtIndex:indexPath.row];
        }
    } else {
        [self pushActiveOrderAtIndex:indexPath.row];
    }
}

- (void)tableView:(UITableView *)tableView cell:(ProcessingCell *)cell didTouchClearButtonAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedTransactionIndex = indexPath.row;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Would you like to complete this order?\nYou can still view all completed orders below in Past Orders.", nil)
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"OK", nil)
                                                    otherButtonTitles:nil];
    actionSheet.tag = 101;
    [actionSheet showInView:self.view];
}

- (void)tableView:(UITableView *)tableView cell:(ProcessingCell *)cell didTouchCancelButtonAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedTransactionIndex = indexPath.row;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Proceed in celeling order?\nCanceled order can be viewed in Past Orders.", nil)
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"OK", nil)
                                                    otherButtonTitles:nil];
    actionSheet.tag = 101;
    [actionSheet showInView:self.view];
}

- (void)tableView:(UITableView *)tableView cell:(ProcessingCell *)cell didTouchGotItButtonAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedTransactionIndex = indexPath.row;
    self.selectedIndexPath = indexPath;

    id orderOrCart = nil;
    if(self.isRapidRO){
        orderOrCart = [TRANS.activeOrders objectAtIndex:self.selectedTransactionIndex];
    } else {
        orderOrCart = [TRANS.transactions objectAtIndex:self.selectedTransactionIndex];
    }
    
    if([orderOrCart isKindOfClass:[Order class]]){
        Order *castedOrder = (Order *)orderOrCart;
        //                    [self requestRemoveOrder:castedOrder];
        [self requestCustomerCloseOrder:castedOrder];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(actionSheet.destructiveButtonIndex == buttonIndex){
        switch(actionSheet.tag){
            case 101:
            {
                id orderOrCart = nil;
                if(self.isRapidRO){
                    orderOrCart = [TRANS.activeOrders objectAtIndex:self.selectedTransactionIndex];
                } else {
                    orderOrCart = [TRANS.transactions objectAtIndex:self.selectedTransactionIndex];
                }
                if([orderOrCart isKindOfClass:[Order class]]){
                    Order *castedOrder = (Order *)orderOrCart;
                    [self requestCustomerCloseOrder:castedOrder];
                }
            }
                break;
            case 102:
            {
                id orderOrCart = nil;
                if(self.isRapidRO){
                    orderOrCart = [TRANS.activeOrders objectAtIndex:self.selectedTransactionIndex];
                } else {
                    orderOrCart = [TRANS.transactions objectAtIndex:self.selectedTransactionIndex];
                }
                
                if([orderOrCart isKindOfClass:[Order class]]){
                    Order *castedOrder = (Order *)orderOrCart;
//                    [self requestRemoveOrder:castedOrder];
                    [self requestCustomerCloseOrder:castedOrder];
                }
            }
                break;
        }
    }
}

- (void)startGettingRate
{
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    alertView.backgroundColor = [UIColor whiteColor];
    RatingView *ratingView = [RatingView view];
//    ratingView.starRatingView.fullStarImage = [UIImage imageNamed:@"star_selected"];
//    ratingView.starRatingView.emptyStarImage = [UIImage imageNamed:@"star_empty"];
//    ratingView.starRatingView.halfFullStarImage = [UIImage imageNamed:@"star_selected"];
//    ratingView.starRatingView.rating = 3;
//    ratingView.starRatingView.editable = YES;
//    ratingView.starRatingView.maxRating = 3;
//    ratingView.starRatingView.delegate = self;
    [alertView setContainerView:ratingView];
    
    alertView.delegate = self;
    alertView.tag = 100;
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedString(@"Later", nil),
                                NSLocalizedString(@"Send", nil), nil]];
    [alertView show];
}

- (void)showExternalRate
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Like Using RushOrder?", nil)
                                                        message:NSLocalizedString(@"Would you take a moment to rate us in the App Store?", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Later", nil)
                                              otherButtonTitles:NSLocalizedString(@"Okay", nil), nil];
    alertView.tag = 302;
    [alertView show];
}

//- (void)ratingView:(BTRatingView *)ratingView ratingDidChange:(float)rating
//{
//    if(rating > 0){
//        ((RatingView *)ratingView.superview).ratingLabel.text = NSLocalizedString(@"How do you like RushOrder?", nil);
//        ((RatingView *)ratingView.superview).ratingLabel.textColor = [UIColor darkTextColor];
//    } else {
//         ((RatingView *)ratingView.superview).ratingLabel.text = NSLocalizedString(@"Please pick from one of the three faces to submit.", nil);
//        ((RatingView *)ratingView.superview).ratingLabel.textColor = [UIColor redColor];
//        
//    }
//}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Button at position %ld is clicked on alertView %ld.", (long)buttonIndex, (long)[alertView tag]);
    switch(alertView.tag){
        case 100: //Rating
            
                if(buttonIndex == 1){ //Send
                    
                    RatingView *view = (RatingView *)alertView.containerView;
                    self.selectedRating = view.selectedRating;
                    
                    if(self.selectedRating < 1){
                        ((RatingView *)alertView.containerView).ratingLabel.text = NSLocalizedString(@"Please pick from one of the three faces to submit.", nil);
                        ((RatingView *)alertView.containerView).ratingLabel.textColor = [UIColor redColor];
                    } else if (self.selectedRating <= 2){
                        [alertView close];
                        // Show feedback
                        CustomIOSAlertView *feedbackAlertView = [[CustomIOSAlertView alloc] init];
                        feedbackAlertView.backgroundColor = [UIColor whiteColor];
                        FeedbackView *feedbackView = [FeedbackView viewWithNibName:@"RatingView"
                                                                           atIndex:1];
                        [feedbackAlertView setContainerView:feedbackView];
                        
                        feedbackAlertView.delegate = self;
                        [feedbackAlertView setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedString(@"Later", nil),
                                                            NSLocalizedString(@"Send", nil), nil]];
                        feedbackAlertView.tag = 101;
                        [feedbackAlertView show];
                        
                        [((FeedbackView *)feedbackAlertView.containerView).feedbackTextView becomeFirstResponder];


                    } else { // More than 2
                        // Show external App Rating Alert
                        [alertView close];
                        [self showExternalRate];
                    }
                } else { //Later
                    [self sendAppRating:1 rate:0 comment:nil];
                    [alertView close];
                }
            break;
        case 101: //Feedback
            if(buttonIndex == 1){ // Send
                NSString *commentText = ((FeedbackView *)alertView.containerView).feedbackTextView.text;
                
                if([commentText length] > 0){
                    [self sendAppRating:3 rate:self.selectedRating comment:commentText];
                    [alertView close];
                } else {
                    ((FeedbackView *)alertView.containerView).feedbackLabel.text = NSLocalizedString(@"Let us know how we can improve!", nil);
                    ((FeedbackView *)alertView.containerView).feedbackLabel.textColor = [UIColor redColor];
                    [((FeedbackView *)alertView.containerView).feedbackTextView becomeFirstResponder];
                }
            } else { // Later
                [alertView close];
                [self sendAppRating:2 rate:self.selectedRating comment:nil];
            }
            
            break;
    }
}

- (void)sendAppRating:(NSInteger)step rate:(NSInteger)rate comment:(NSString *)comment
{
    RequestResult result = RRFail;
    result = [PAY requestAppRating:step
                              rate:rate
                           comment:comment
                   completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              PCLog(@"Result %@", response);
                              switch([response integerForKey:@"step"]){
                                  case 3:{
                                      UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Thank you!",nil)
                                                                                          message:NSLocalizedString(@"You can provide additional feedback at any time by emailing us at feedback@rushorderapp.com",nil)
                                                                                         delegate:self
                                                                                cancelButtonTitle:@"Okay"
                                                                                otherButtonTitles:nil];
                                      alertView.tag = 303;
                                      [alertView show];
                                  }
                                      break;
                                  case 5:
                                      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:CUSTOMER_ITUNES_URL]];
                                      break;
                              }
                          }
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while completing order", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}



- (void)requestCustomerCloseOrder:(Order *)order
{
    RequestResult result = RRFail;
    result = [PAY requestCustomerCloseOrder:order.orderNo
                            completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              order.customerClose = YES;
                              
                              NSUInteger oldIndex = [TRANS.transactions indexOfObject:order];
                              
                              [TRANS.transactions removeObjectAtIndex:oldIndex];
                              
                              if([TRANS.transactions count] > 0){
                                  ((UITabBarItem *)APP.tabBarController.tabBar.items[3]).badgeValue = [NSString stringWithFormat:@"%lu",(unsigned long)[TRANS.transactions count]];
                              } else {
                                  ((UITabBarItem *)APP.tabBarController.tabBar.items[3]).badgeValue = nil;
                              }
                              
                              NSUInteger newIndex = [REMOTE.orders indexOfObject:order
                                                                   inSortedRange:NSMakeRange(0, [REMOTE.orders count])
                                                                         options:NSBinarySearchingInsertionIndex
                                                                 usingComparator:^NSComparisonResult(Order *obj1, Order *obj2) {
                                                                     return [obj1 compareTimeWith:obj2];
                                                                 }];
                              
                              if(newIndex != NSNotFound){
                                  [REMOTE.orders insertObject:order
                                                      atIndex:newIndex];
                              }
                              
                              [self.tableView beginUpdates];
                              [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:oldIndex
                                                                                          inSection:0]]
                                                    withRowAnimation:UITableViewRowAnimationBottom];
                              
                              [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:newIndex
                                                                                       inSection:1]]
                                                 withRowAnimation:UITableViewRowAnimationTop];
                              [self.tableView endUpdates];
                              
                              if([REMOTE.orders count] > 0){
                                  if(REMOTE.isMoredata){
                                      self.tableView.tableFooterView = self.moreLoadingView;
                                  } else {
                                      self.tableView.tableFooterView = nil;
                                  };
                              } else {
                                  NSInteger count = [REMOTE.orders count] + [TRANS.transactions count];
                                  if(count == 0){
                                      if(self.isRapidRO){
                                          NoOrdersForFavoriteOrder *view = [NoOrdersForFavoriteOrder view];
                                          [view.startButton addTarget:self
                                                               action:@selector(startButtonTouched:)
                                                     forControlEvents:UIControlEventTouchUpInside];
                                          self.tableView.tableFooterView = view;
                                      } else {
                                          self.tableView.tableFooterView = self.footerView;
                                      }
                                  } else {
                                      self.tableView.tableFooterView = nil;
                                  }
                              }
                              
#if DEBUG
                              [self startGettingRate];
#else
                              if([response boolForKey:@"app_rating"]){
                                  [self startGettingRate];
                              }
#endif
                              
                          }
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while completing order", nil)];
                              }
                              
                              REMOTE.ordersDirty = YES;
                              REMOTE.currentPage = 0;
                              TRANS.dirty = YES;
                              if(![TRANS requestMyOrders]){
                                  [self.refreshControl endRefreshing];
                              }
                              if(![REMOTE requestOrdersList]){
                                  [self.refreshControl endRefreshing];
                              }

                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}
- (IBAction)rateButtonTouched:(id)sender {
    PCLog(@"Touched");
    [self startGettingRate];
}
@end
