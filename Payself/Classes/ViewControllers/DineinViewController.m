//
//  DineinViewController.m
//  RushOrder
//
//  Created by Conan on 11/13/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "DineinViewController.h"
#import "PhoneNumberFormatter.h"
#import "OrderManager.h"
#import "PCCredentialService.h"
#import "AccountSwitchingManager.h"
#import "RemoteDataManager.h"
#import "PaymentMethodViewController.h"
#import "NewCardViewController.h"
#import "MenuManager.h"

@interface DineinViewController ()
{
    int _textFieldSemaphore;
    __strong PhoneNumberFormatter *_phoneNumberFormatter;
}

@property (strong, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPickerView *timePickerView;
@property (strong, nonatomic) IBOutlet UIToolbar *pickerToolBar;
@property (weak, nonatomic) IBOutlet UITextField *pickupTimeTextField;
@property (weak, nonatomic) IBOutlet NPStretchableButton *pickupTimeSelectButton;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel;
@property (strong, nonatomic) NSMutableArray *timeArray;
@property (strong, nonatomic) IBOutlet UIToolbar *tableNumberToolBar;
@property (strong, nonatomic) IBOutlet UIPickerView *tableNumberPickerView;
@property (weak, nonatomic) IBOutlet UILabel *nameTitleLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *toggleBarButtonItem;

@property (weak, nonatomic) IBOutlet UILabel *placeHolderLabel;

@property (weak, nonatomic) IBOutlet UIButton *orderButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressTopConstraint;
@property (weak, nonatomic) IBOutlet UIView *emailAdditionView;
@property (weak, nonatomic) IBOutlet PCTextField *emailTextField;
@property (weak, nonatomic) IBOutlet PCTextField *passwordTextField;

@property (weak, nonatomic) IBOutlet NPStretchableButton *recentRequestButton;
@property (strong, nonatomic) RecentListViewController *recentRequestViewController;
@property (strong, nonatomic) NSMutableArray *suggestingCustomRequests;
@property (strong, nonatomic) IBOutlet UIButton *suggestButton;

@property (strong, nonatomic) NSMutableArray *mobileCardList;
@property (nonatomic, getter = isCardListWaiting) BOOL cardListWaiting;
@property (nonatomic, getter = isNeedCustomerInfoOverwriting) BOOL needCustomerInfoOverwriting;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deliveryLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *switchButtonSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *takeoutTrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameSpaceContraint;
@property (weak, nonatomic) IBOutlet NPStretchableButton *takeoutButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *deliveryButton;
@property (nonatomic, getter = isNeedToSaveTempInfo) BOOL needToSaveTempInfo;

@end

@implementation DineinViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _textFieldSemaphore = 0;
        _phoneNumberFormatter = [[PhoneNumberFormatter alloc] init];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(signedIn:)
                                                     name:SignedInNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(signedOut:)
                                                     name:SignedOutNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(cardListChanged:)
                                                     name:CardListChangeNotification
                                                   object:nil];
        self.needToSaveTempInfo = YES;
    }
    return self;
}

- (void)signedIn:(NSNotification *)aNoti
{
    self.mobileCardList = nil;
    self.needCustomerInfoOverwriting = YES;
    // This code makes current name and number nil - so commented out
//    ORDER.tempDeliveryAddress.receiverName = nil;
//    ORDER.tempDeliveryAddress.phoneNumber = nil;
}

- (void)signedOut:(NSNotification *)aNoti
{
    self.mobileCardList = nil;
}

- (void)cardListChanged:(NSNotification *)notification
{
    if(CRED.isSignedIn){
        self.mobileCardList = REMOTE.cards;
        if(self.isCardListWaiting){
            self.cardListWaiting = NO;
            [self stepForPayment];
        }
    } else {
        self.mobileCardList = [MobileCard listAll];
    }
}

- (void)stepForPayment
{
    if(CRED.isSignedIn){
        self.mobileCardList = REMOTE.cards;
        if(!REMOTE.isCardFetched){
            self.cardListWaiting = YES;
            return;
        }
    }
    
    UIViewController *viewController = nil;
    
//    ORDER.tempDeliveryAddress = [[Addresses alloc] init];
    if(self.isNeedToSaveTempInfo){
        ORDER.tempDeliveryAddress.receiverName = self.orderNameField.text;
        if(ORDER.tempDeliveryAddressFilled < 1) ORDER.tempDeliveryAddressFilled = 1;
    }
    
    ORDER.tempAfterMinute = self.afterMinute;
    ORDER.tempCustomerRequest = self.additionalRequest.text;
    
#if FLURRY_ENABLED
    [Flurry logEvent:@"Try to Pay(I)" withParameters:@{@"RestaurantName":ORDER.merchant.name,
                                                       @"RestaurantId":[NSNumber numberWithSerial:ORDER.merchant.merchantNo],
                                                       @"orderTotal":[NSNumber numberWithCurrency:ORDER.order.subTotal]}];
#endif
    
    if([self.mobileCardList count] > 0 || CRED.customerInfo.credit > 0){
        
        viewController = [PaymentMethodViewController viewControllerFromNib];
        ((PaymentMethodViewController *)viewController).payment = self.payment;
        ((PaymentMethodViewController *)viewController).paymentMethodList = self.mobileCardList;
        ((PaymentMethodViewController *)viewController).address = ORDER.tempDeliveryAddress;
        ((PaymentMethodViewController *)viewController).requestMessage = self.additionalRequest.text;
        ((PaymentMethodViewController *)viewController).afterMinute = self.afterMinute;
        
    } else {
        viewController = [NewCardViewController viewControllerFromNib];
        ((NewCardViewController *)viewController).unintendedPresented = YES;
        ((NewCardViewController *)viewController).payment = self.payment;
        ((NewCardViewController *)viewController).address = ORDER.tempDeliveryAddress;
        ((NewCardViewController *)viewController).requestMessage = self.additionalRequest.text;
        ((NewCardViewController *)viewController).afterMinute = self.afterMinute;
    }
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.recentRequestButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
    self.additionalRequest.inputAccessoryView = self.suggestButton;
    self.additionalRequest.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.title = NSLocalizedString(@"Dine-in Option", nil);

    
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemCancel
//                                                                        target:self
//                                                                        action:@selector(cancelButtonTouched:)];
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemBack
                                                                        target:self
                                                                        action:@selector(backButtonTouched:)];
    
    [self.scrollView setContentSizeWithBottomView:self.orderButton];
    
    self.pickupTimeTextField.inputView = self.timePickerView;
    self.pickupTimeTextField.inputAccessoryView = self.pickerToolBar;
    
    if([ORDER.merchant.takeoutRemark length] > 0){
        self.remarkLabel.text = ORDER.merchant.takeoutRemark;
    } else {
        self.remarkLabel.text = NSLocalizedString(@"Thank you for your order", nil);
    }
    
    
    
    if(ORDER.merchant.servicedBy == ServicedByStaff && [ORDER.merchant.tableList count] > 0){
        
        self.nameTitleLabel.text = NSLocalizedString(@"Table Number", nil);
        self.orderNameField.placeholder = @"0";
        
        self.orderNameField.inputAccessoryView = self.tableNumberToolBar;
        self.orderNameField.keyboardType = UIKeyboardTypeNumberPad;
        
    } else {
        
        self.nameTitleLabel.text = NSLocalizedString(@"Name", nil);
        self.orderNameField.placeholder = @"John Doe";
        
        if(CRED.isSignedIn){
            if([self.orderNameField.text length] == 0){
                self.orderNameField.text = CRED.customerInfo.name;
            }
        }
        
        if([self.orderNameField.text length] == 0){
            self.orderNameField.text = [NSUserDefaults standardUserDefaults].custName;
        }
        
        if([self.orderNameField.text length] > 0){
            self.orderNameField.placeholder = self.orderNameField.text;
        }
    }
    
    if([self.initialName length] > 0){
        if(self.orderNameField.inputAccessoryView == nil){
            self.orderNameField.text = self.initialName;
        }
    }
    
    if([self.initialRequestMessage length] > 0){
        self.additionalRequest.text = self.initialRequestMessage;
        self.placeHolderLabel.hidden = ([self.additionalRequest.text length] > 0);
    }

}

- (void)backButtonTouched:(id)sender
{
    if(ORDER.order.status == OrderStatusDraft
       && ORDER.cart.cartType != CartTypeCart){
        if(self.disposeOrderWhenBack){
            ORDER.order = nil;
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
//    ORDER.tempDeliveryAddress = [[Addresses alloc] init];
    
    if(self.isNeedToSaveTempInfo){
        ORDER.tempDeliveryAddress.receiverName = self.orderNameField.text;
        if(ORDER.tempDeliveryAddressFilled < 1) ORDER.tempDeliveryAddressFilled = 1;
    }
    ORDER.tempCustomerRequest = self.additionalRequest.text;
}

- (void)drawTopPartAccordingToOrderType
{
    
    if(ORDER.merchant.isAbleTakeout && ORDER.merchant.isAbleDelivery){
        self.deliveryButton.hidden = NO;
        self.takeoutButton.hidden = NO;
        self.switchButtonSpaceConstraint.priority = 999;
        self.deliveryLeadingConstraint.priority = 500;
        self.takeoutTrailingConstraint.priority = 500;
        self.nameTopConstraint.priority = 500;
        self.nameSpaceContraint.priority = 999;
    } else if(ORDER.merchant.isAbleTakeout){
        self.deliveryButton.hidden = YES;
        self.takeoutButton.hidden = NO;
        self.switchButtonSpaceConstraint.priority = 500;
        self.deliveryLeadingConstraint.priority = 999;
        self.takeoutTrailingConstraint.priority = 999;
        self.nameTopConstraint.priority = 500;
        self.nameSpaceContraint.priority = 999;
    } else if(ORDER.merchant.isAbleDelivery){
        self.deliveryButton.hidden = NO;
        self.takeoutButton.hidden = YES;
        self.switchButtonSpaceConstraint.priority = 500;
        self.deliveryLeadingConstraint.priority = 999;
        self.takeoutTrailingConstraint.priority = 999;
        self.nameTopConstraint.priority = 500;
        self.nameSpaceContraint.priority = 999;
    } else {
        self.deliveryButton.hidden = YES;
        self.takeoutButton.hidden = YES;
        self.switchButtonSpaceConstraint.priority = 999;
        self.deliveryLeadingConstraint.priority = 500;
        self.takeoutTrailingConstraint.priority = 500;
        self.nameTopConstraint.priority = 999;
        self.nameSpaceContraint.priority = 500;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self drawTopPartAccordingToOrderType];
    
    if(ORDER.isSetTempDelivery){
        
        if(ORDER.tempDeliveryAddressFilled >= 1){
            if(self.orderNameField.inputAccessoryView == nil && ORDER.tempDeliveryAddress.receiverName.length != 0){
                self.orderNameField.text = ORDER.tempDeliveryAddress.receiverName;
            }
        }
        self.additionalRequest.text = ORDER.tempCustomerRequest;
        [self textViewDidChange:self.additionalRequest];
    }
    
    if(CRED.isSignedIn){
        self.emailAdditionView.hidden = YES;
        self.addressTopConstraint.constant = 19.0f;
        self.orderNameField.nextField = self.additionalRequest;
        
//        if([self.orderNameField.text length] == 0){ // commented reason : Ability to Sign-in During Checkout Process (https://app.asana.com/0/inbox/23035815421587/222338978099920/222338978099925)
        if(self.orderNameField.inputAccessoryView == nil){
            if(self.isNeedCustomerInfoOverwriting){
                self.orderNameField.text = CRED.customerInfo.name;
            } else {
                if([self.orderNameField.text length] == 0){
                    self.orderNameField.text = CRED.customerInfo.name;
                }
            }
        }
        
        
        self.needCustomerInfoOverwriting = NO;
        
    } else {
        self.emailAdditionView.hidden = NO;
        self.addressTopConstraint.constant = 150.0f;
        self.orderNameField.nextField = self.emailTextField;
        if([self.emailTextField.text length] == 0){
            self.emailTextField.text = [NSUserDefaults standardUserDefaults].emailAddress;
        }
    }
}

- (IBAction)toggleButtonTouched:(id)sender
{
    [self.orderNameField resignFirstResponder];
    
    if(self.orderNameField.inputView != nil){
        self.orderNameField.inputView = nil;
        self.toggleBarButtonItem.title = NSLocalizedString(@"Picker", nil);
    } else {
        self.orderNameField.inputView = self.tableNumberPickerView;
        self.toggleBarButtonItem.title = NSLocalizedString(@"Keyboard", nil);
    }
    
    [self.orderNameField becomeFirstResponder];
}

- (void)cancelButtonTouched:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 
                             }];
}

- (IBAction)doneButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    if(self.orderNameField.inputAccessoryView != nil){
        NSString *inputTableNumber = self.orderNameField.text;
        
        if([inputTableNumber length] > 0){
            
            TableInfo *selectedTableInfo = nil;
            for(TableInfo *tableInfo in ORDER.tableList){
                
                if([tableInfo.tableNumber isCaseInsensitiveEqual:inputTableNumber]){
                    selectedTableInfo = tableInfo;
                    break;
                }
            }
            
            if(selectedTableInfo == nil){
                if(!ORDER.merchant.isTableBase && ORDER.merchant.servicedBy == ServicedByStaff){
                    [UIAlertView alertWithTitle:NSLocalizedString(@"Invalid Table Number", nil)
                                        message:NSLocalizedString(@"You have entered a table number outside the range established by the restaurant.  Please check and re-enter.", nil)];
                } else {
                    [UIAlertView alertWithTitle:NSLocalizedString(@"Invalid Table Number", nil)
                                        message:NSLocalizedString(@"You have entered a table number outside the range established by the restaurant.  Please check and re-enter.", nil)];
                }
                [self.orderNameField becomeFirstResponder];
                return;
            }
            
            ORDER.cart.cartType = CartTypePickupServiced;
            ORDER.cart.flagNumber = selectedTableInfo.tableNumber;
            
        } else {
            [UIAlertView alertWithTitle:NSLocalizedString(@"Select Table Number", nil)
                                message:NSLocalizedString(@"If you don't know your table number, please ask your server", nil)];
            
            [self.orderNameField becomeFirstResponder];
            return;
        }
    } else {
        if(![VALID validate:self.orderNameField
                      title:NSLocalizedString(@"Please Enter Name", nil)
                    message:NSLocalizedString(@"Name is Required to Complete This Order", nil)]){
            return;
        }
        
        [NSUserDefaults standardUserDefaults].custName = self.orderNameField.text;
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    
    if(CRED.isSignedIn){
        [self stepForPayment];
    } else {
        
        if(![VALID validate:self.emailTextField
                      title:NSLocalizedString(@"Email Is Required", nil)
                    message:nil]){
            return;
        }
        
        if(![VALID validateEmail:self.emailTextField
                           title:NSLocalizedString(@"Invalid Email", nil)
                         message:NSLocalizedString(@"Please enter a valid email address", nil)]){
            return;
        }
        
        [NSUserDefaults standardUserDefaults].emailAddress = self.emailTextField.text;
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if([self.passwordTextField.text length] > 0){
            // Sign Up Call if Guest
            // After Signing up - proceed order again.
            [NSUserDefaults standardUserDefaults].lastTriedUserId = self.emailTextField.text;
            
            NSArray *nameArray = [self.orderNameField.text componentsSeparatedByString:@" "];
            
            RequestResult result = RRFail;
            result = [CRED requestSignUp:self.emailTextField.text
                                password:self.passwordTextField.text
                               firstName:[nameArray firstName]
                                lastName:[nameArray lastName]
                             deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                         completionBlock:
                      ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                          
                          if(isSuccess && 200 <= statusCode && statusCode < 300){
                              switch(response.errorCode){
                                  case ResponseSuccess:{
                                      
                                      CRED.unattendedSignUp = YES;
                                      
                                      NSArray *customers = [CustomerInfo listAll];
                                      CustomerInfo *customerInfo = nil;
                                      if([customers count] == 1){
                                          customerInfo = [customers objectAtIndex:0];
                                      } else{
                                          PCWarning(@"Customers is not just only 1- sign up");
                                          // Clear
                                          for(CustomerInfo *customer in customers){
                                              if([customer delete]){
                                                  // Success
                                              } else {
                                                  PCError(@"Error to remove customerInfo - sign up");
                                              }
                                          }
                                      }
                                      
                                      if(customerInfo == nil){
                                          customerInfo = [[CustomerInfo alloc] init];
                                      }
                                      
                                      NSDictionary *customerDict = [response objectForKey:@"customer"];
                                      [customerInfo updateWithDictionary:customerDict];
                                      customerInfo.authToken = [response objectForKey:@"auth_token"];
                                      
                                      if([customerInfo save]){
                                          CRED.customerInfo = customerInfo;
                                          
                                          [SWITCH removeLocalDataIncludeCard:NO];
                                          
                                          [[NSNotificationCenter defaultCenter] postNotificationName:SignedInNotification
                                                                                              object:self
                                                                                            userInfo:nil];
                                          
                                          [self stepForPayment];
                                      } else {
                                          [UIAlertView alertWithTitle:NSLocalizedString(@"Can Not Save Account Information", nil)
                                                              message:NSLocalizedString(@"Try to sign in again. If the problem contiue, Please contact us.\nhelp@rushorderapp.com", nil)];
                                      }
                                  }
                                      break;
                                  default:{
                                      NSString *message = [response objectForKey:@"message"];
                                      NSString *title = [response objectForKey:@"title"];
                                      if([message isKindOfClass:[NSString class]]){
                                          if(title != nil){
                                              [UIAlertView alertWithTitle:title
                                                                  message:message];
                                          } else {
                                              [UIAlertView alert:message];
                                          }
                                      } else {
                                          [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while signing up", nil)];
                                      }
                                      
                                      CRED.customerInfo = nil;
                                  }
                                      break;
                              }
                          } else {
                              if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                                  [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                                      object:nil];
                              } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                                  [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                                      ,statusCode]];
                              }
                          }
                          [SVProgressHUD dismiss];
                      }];
            switch(result){
                case RRSuccess:
                    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                    break;
                case RRParameterError:
                    [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                    break;
                default:
                    break;
            }
            
        } else {
            [self stepForPayment];
        }
    }
}

//- (void)proceedOrder
//{
//    if([self.delegate respondsToSelector:@selector(dineinViewController:didTouchPlaceOrderButton:)]){
//        [self.delegate dineinViewController:self
//                   didTouchPlaceOrderButton:nil
//         ];
//    }
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isKindOfClass:[PCTextField class]]){
        [textField resignFirstResponder];
        [[(PCTextField *)textField nextField] becomeFirstResponder];
    }
    return YES;
}



#pragma mark - UIPickerViewDataSource
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger count = [ORDER.tableList count];
    
    return MAX(count, 1);
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if([ORDER.tableList count] > row){
        TableInfo *table = [ORDER.tableList objectAtIndex:row];
        return table.tableNumber;
    }
    
    return @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if([ORDER.tableList count] > row){
        TableInfo *table = [ORDER.tableList objectAtIndex:row];
        
        self.orderNameField.text = table.tableNumber;
    }
}

- (IBAction)tableNumberDoneButtonTouched:(id)sender
{
    ORDER.cart.flagNumber = nil;
    
    if(self.orderNameField.inputView == nil){
        // When the user input table number manually. Not by Picker view
        NSString *inputTableNumber = self.orderNameField.text;
        
        if([inputTableNumber length] > 0){
            
            TableInfo *selectedTableInfo = nil;
            for(TableInfo *tableInfo in ORDER.tableList){
                
                if([tableInfo.tableNumber isCaseInsensitiveEqual:inputTableNumber]){
                    selectedTableInfo = tableInfo;
                    break;
                }
            }
            
            if(selectedTableInfo == nil){
                if(!ORDER.merchant.isTableBase && ORDER.merchant.servicedBy == ServicedByStaff){
                    [UIAlertView alertWithTitle:NSLocalizedString(@"Invalid Table Number", nil)
                                        message:NSLocalizedString(@"You have entered a table number outside the range established by the restaurant.  Please check and re-enter.", nil)];
                } else {
                    [UIAlertView alertWithTitle:NSLocalizedString(@"Invalid Table Number", nil)
                                        message:NSLocalizedString(@"You have entered a table number outside the range established by the restaurant.  Please check and re-enter.", nil)];
                }
                [self.orderNameField becomeFirstResponder];
            } else {
            
                ORDER.cart.cartType = CartTypePickupServiced;
                ORDER.cart.flagNumber = selectedTableInfo.tableNumber;
                
                [self.additionalRequest becomeFirstResponder];
            }
            
        } else {
            [self.additionalRequest becomeFirstResponder];
        }
    } else {
        // By picker view
        NSInteger selRow = [self.tableNumberPickerView selectedRowInComponent:0];
        
        if([ORDER.tableList count] > selRow){
            TableInfo *table = [ORDER.tableList objectAtIndex:selRow];
            
            ORDER.cart.cartType = CartTypePickupServiced;
            ORDER.cart.flagNumber = table.tableNumber;
            
            self.orderNameField.text = table.tableNumber;
        }
        [self.additionalRequest becomeFirstResponder];
    }
}

- (IBAction)recentRequestButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    self.recentRequestViewController = [RecentListViewController viewControllerFromNib];
    self.recentRequestViewController.delegate = self;
    
    UIWindow *window = self.view.window;
    self.recentRequestViewController.view.frame = window.frame;
    
    [self.recentRequestViewController.tableView reloadData];
    self.recentRequestViewController.panelView.frame = CGRectMake(((self.recentRequestViewController.view.width - self.recentRequestViewController.panelView.width) / 2),
                                                                  700.0f,
                                                                  self.recentRequestViewController.panelView.width,
                                                                  self.recentRequestViewController.panelView.height);
    self.recentRequestViewController.view.alpha = 0.0f;
    [self.view.window addSubview:self.recentRequestViewController.view];
    
    [UIView animateWithDuration:0.3f
                     animations:^{
                         self.recentRequestViewController.view.alpha = 1.0f;
                         CGRect frame = self.recentRequestViewController.panelView.frame;
                         frame.origin.y = (self.recentRequestViewController.view.height - frame.size.height) / 2;
                         self.recentRequestViewController.panelView.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                     }];
    
}

- (void)recentListViewController:(RecentListViewController *)viewController
                didSelectRequest:(CustomRequest *)selectedRequest
{
    self.additionalRequest.text = selectedRequest.request;
    [self textViewDidChange:self.additionalRequest];
//    [self.additionalRequest becomeFirstResponder];
}


//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
//{
//    if(textField.inputAccessoryView != nil){
//        if(self.tableInfo == nil){
//            return NO;
//        } else {
//            return YES;
//        }
//    }
//}

- (NSMutableArray *)mobileCardList
{
    if(_mobileCardList == nil){
        if(CRED.isSignedIn){
            _mobileCardList = REMOTE.cards; // TODO:If card does not exist? -> Blocking?
        } else {
            _mobileCardList = [MobileCard listAll];
        }
    }
    return _mobileCardList;
}


#pragma mark - TextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    if([textView.text length] > 0){
        self.placeHolderLabel.hidden = YES;
        self.suggestingCustomRequests = [CustomRequest suggestingListWithKeyword:textView.text
                                                                            type:CustomRequestTypeDineIn
                                                                      merchantNo:ORDER.merchant.merchantNo];
        
        if([self.suggestingCustomRequests count] > 0){
            CustomRequest *request = [self.suggestingCustomRequests objectAtIndex:0];
            self.suggestButton.buttonTitle = [request.request stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
        } else {
            self.suggestButton.buttonTitle = nil;
        }
    } else {
        self.placeHolderLabel.hidden = NO;
        self.suggestButton.buttonTitle = nil;
    }
}

- (IBAction)suggestButtonTouched:(id)sender
{
    if(self.suggestButton.buttonTitle.length > 0){
        CustomRequest *request = [self.suggestingCustomRequests objectAtIndex:0];
        self.additionalRequest.text = request.request;
        self.placeHolderLabel.hidden = YES;
        [self.additionalRequest resignFirstResponder];
    } else {
        // Do nothing
    }
}

- (IBAction)switchToTakeoutButtonTouched:(id)sender
{
    if([MENUPAN resetGraphWithMerchant:ORDER.merchant
                              withType:MenuListTypeTakeout]){
        ORDER.cart.cartType = CartTypeTakeout;
        ORDER.order.orderType = OrderTypeTakeout;
        
        [MENUPAN requestMenus:YES
                      success:^(id response) {
                          self.needToSaveTempInfo = NO;
                          [ORDER.cart isAllItemsInHour:YES];
                          [[NSNotificationCenter defaultCenter] postNotificationName:OrderTypeSwitchedNotification
                                                                              object:self];
                          [self backButtonTouched:nil];
                      } andFail:^(NSUInteger errorCode) {
                          [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to renew Menus",nil)
                                              message:NSLocalizedString(@"Sorry, please try again and if it continues, please contact us.",nil)];
                          [self.navigationController popToRootViewControllerAnimated:YES];
                      }];
        
    } else {
    }
}

- (IBAction)switchToDeliveryButtonTouched:(id)sender
{
    if([MENUPAN resetGraphWithMerchant:ORDER.merchant
                              withType:MenuListTypeDelivery]){
        ORDER.cart.cartType = CartTypeDelivery;
        ORDER.order.orderType = OrderTypeDelivery;
        
        [MENUPAN requestMenus:YES
                      success:^(id response) {
                          self.needToSaveTempInfo = NO;
                          [ORDER.cart isAllItemsInHour:YES];
                          [[NSNotificationCenter defaultCenter] postNotificationName:OrderTypeSwitchedNotification
                                                                              object:self];
                          [self backButtonTouched:nil];
                      } andFail:^(NSUInteger errorCode) {
                          [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to renew Menus",nil)
                                              message:NSLocalizedString(@"Sorry, please try again and if it continues, please contact us.",nil)];
                          [self.navigationController popToRootViewControllerAnimated:YES];
                      }];
    } else {
        
    }
}

- (IBAction)signInButtonTouched:(id)sender
{
    [self showSignIn];
}


- (void)showSignIn
{
    return [self showSignIn:NO];
}

- (void)showSignIn:(BOOL)showGuide
{
    SignInCustomerViewController *viewController = [SignInCustomerViewController viewControllerFromNib];
    viewController.showGuide = showGuide;
    [self.tabBarController presentViewControllerInNavigation:viewController
                                                    animated:YES
                                                  completion:^{
                                                      
                                                  }];
}
@end
