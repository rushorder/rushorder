//
//  SettingViewController.m
//  RushOrder
//
//  Created by Conan on 2/22/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "SettingViewController.h"
#import "SelectCardViewController.h"
#import "PaymentHistoryViewController.h"
#import "AboutViewController.h"
#import "EULAViewController.h"
#import "PassCodeSettingViewController.h"
#import "PCCredentialService.h"
#import "CreditPointViewController.h"
#import "CustomCell.h"
#import "SignInCustomerViewController.h"
#import "SignUpCustomerViewController.h"
#import "ChangePasswordViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ProfileViewController.h"
#import "ContactUsViewController.h"
#import "InviteFriendViewController.h"
#import "InviteFriendGuideViewController.h"

#define SETTING_SECTION_HEIGHT 28.0f
#define SETTING_SECTION_FOOTER_HEIGHT 65.0f

enum {
    TableItemMyCards = 1,
    TableItemMyCredit,
    TableItemMyHistory,
    TableItemReceiptMailing,
    TableItemInviteFriend,
    TableItemChangePassword,
    TableItemDeliveryAddress,
    TableItemSignInOut,
    TableItemAbout,
    TableItemContactUs,
    TableItemFaq,
    TableItemEULA,
    TableItemPrivacy,
    TableItemSign,
    TableItemTest,
    TableItemPassCode,
    TableItemHideDemo,
    TableItemHideTest,
    TableItemAutoTip,
    TableItemAutoTakeoutTip,
    TableItemAutoDeliveryTip,
    TableItemBecomDriver
};


@interface SettingViewController ()

@property (nonatomic, strong) NSMutableArray *items;
//@property (nonatomic, strong) UISwitch *testDriveSwitch;
@property (nonatomic, strong) UISwitch *demoOnSwitch;
@property (nonatomic, strong) UISwitch *testOnSwitch;
@property (nonatomic, strong) UISwitch *autoTipSwitch;
@property (nonatomic, strong) UISwitch *autoTakeoutTipSwitch;
@property (nonatomic, strong) UISwitch *autoDeliveryTipSwitch;
@property (weak, nonatomic) IBOutlet CircleImageView *profilePhotoImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet NPStretchableButton *signInButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *signUpButton;
@property (strong, nonatomic) UILabel *badgeLabel;

@property (nonatomic) AutoPushType autoPushType;
@end

@implementation SettingViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.tableView.separatorInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    
    [self.signInButton applyResizableImageFromCenterForState:UIControlStateNormal];
    [self.signUpButton applyResizableImageFromCenterForState:UIControlStateNormal];
}

- (void)commonInit
{
//    [self configureMenus];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signedIn:)
                                                 name:SignedInNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signedOut:)
                                                 name:SignedOutNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(autoNotified:)
                                                 name:@"NeedToPreparePushingAutoViewController"
                                               object:nil];
}

- (void)autoNotified:(NSNotification *)noti
{
    if(self.tabBarController.selectedIndex == 4){
        NSDictionary *userInfo = noti.userInfo;
        if([[userInfo objectForKey:@"type"] isEqualToString:@"invite"]){
            self.autoPushType = AutoPushTypeInvite;
        } else if([[userInfo objectForKey:@"type"] isEqualToString:@"mycredit"]){
            self.autoPushType = AutoPushTypeMycredit;
        } else {
            self.autoPushType = AutoPushTypeNone;
        }
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)signedIn:(NSNotification *)noti
{
    [self configureMenus];
    [self.tableView reloadData];
}

- (void)signedOut:(NSNotification *)noti
{
    [self viewWillAppear:NO];
    [self.tableView reloadData];
}

- (void)configureMenus
{
    self.items = [NSMutableArray array];
    
    NSMutableArray *subItems = [NSMutableArray array];
    
    NSDictionary *item = nil;
    NSDictionary *subItemDict = nil;
    
    item = [NSDictionary dictionaryWithObjectsAndKeys:
            NSLocalizedString(@"Share & Earn", nil), @"title",
            [NSNumber numberWithInteger:TableItemInviteFriend], @"tableItem",
            nil];
    [subItems addObject:item];
    
    subItemDict = [NSDictionary dictionaryWithObjectsAndKeys:
                   subItems, @"list",
                   nil];
    [self.items addObject:subItemDict];
    
    
    subItems = [NSMutableArray array];
    
#ifndef SLIDE_LEFT_MENU
    item = [NSDictionary dictionaryWithObjectsAndKeys:
            NSLocalizedString(@"Manage Cards", nil), @"title",
            [NSNumber numberWithInteger:TableItemMyCards], @"tableItem",
            nil];
    [subItems addObject:item];
    
    item = [NSDictionary dictionaryWithObjectsAndKeys:
            NSLocalizedString(@"Delivery Addresses", nil), @"title",
            [NSNumber numberWithInteger:TableItemDeliveryAddress], @"tableItem",
            nil];
    [subItems addObject:item];
    
    item = [NSDictionary dictionaryWithObjectsAndKeys:
            NSLocalizedString(@"Credits & Rewards", nil), @"title",
            [NSNumber numberWithInteger:TableItemMyCredit], @"tableItem",
            nil];
    [subItems addObject:item];
    
//    item = [NSDictionary dictionaryWithObjectsAndKeys:
//            NSLocalizedString(@"Recent Activity", nil), @"title",
//            [NSNumber numberWithInteger:TableItemMyHistory], @"tableItem",
//            nil];
//    [subItems addObject:item];
    
#endif
    
    item = [NSDictionary dictionaryWithObjectsAndKeys:
            NSLocalizedString(@"Receipt Mailing", nil), @"title",
            [NSNumber numberWithInteger:TableItemReceiptMailing], @"tableItem",
            nil];
    [subItems addObject:item];
    
    if(CRED.isSignedIn){
        item = [NSDictionary dictionaryWithObjectsAndKeys:
                NSLocalizedString(@"Change Password", nil), @"title",
                [NSNumber numberWithInteger:TableItemChangePassword], @"tableItem",
                nil];
        [subItems addObject:item];
    }
//    
//    item = [NSDictionary dictionaryWithObjectsAndKeys:
//            CRED.isSignedIn ?  NSLocalizedString(@"Sign Out", nil) : NSLocalizedString(@"Sign In", nil), @"title",
//            [NSNumber numberWithInteger:TableItemSignInOut], @"tableItem",
//            nil];
//    [subItems addObject:item];
    
    subItemDict = [NSDictionary dictionaryWithObjectsAndKeys:
                   NSLocalizedString(@"Account", nil), @"title",
                   subItems, @"list",
                   nil];
    [self.items addObject:subItemDict];
    
    //======================================================================
    
    subItems = [NSMutableArray array];
    
    item = [NSDictionary dictionaryWithObjectsAndKeys:
            NSLocalizedString(@"Passcode Lock", nil), @"title",
            [NSNumber numberWithInteger:TableItemPassCode], @"tableItem",
            nil];
    [subItems addObject:item];
    
    
//    item = [NSDictionary dictionaryWithObjectsAndKeys:
//            NSLocalizedString(@"Show Demo Restaurant", nil), @"title",
//            [NSNumber numberWithInteger:TableItemHideDemo], @"tableItem",
//            nil];
//    [subItems addObject:item];
    
//    item = [NSDictionary dictionaryWithObjectsAndKeys:
//            NSLocalizedString(@"Show Test Restaurant", nil), @"title",
//            [NSNumber numberWithInteger:TableItemHideTest], @"tableItem",
//            nil];
//    [subItems addObject:item];
   
    subItemDict = [NSDictionary dictionaryWithObjectsAndKeys:
                   NSLocalizedString(@"Preferences", nil), @"title",
                   subItems, @"list",
                   nil];
    
    [self.items addObject:subItemDict];
    
    //======================================================================
    
    /*
     
    // below commented by https://app.asana.com/0/11543106091665/108690098988023
     
    subItems = [NSMutableArray array];
    
    NSNumber *tipAmountSaved = [[NSUserDefaults standardUserDefaults] objectForKey:tipRateLastUsed];
    
    NSInteger TipPercent = round([NSUserDefaults standardUserDefaults].tipRate);
    
    item = [NSDictionary dictionaryWithObjectsAndKeys:
            NSLocalizedString(@"Dine-in Tip Amount", nil), @"title",
            ((tipAmountSaved == nil) ? @"—" : [NSString stringWithFormat:@"%ld%%",(long)TipPercent]), @"subTitle",
            [NSNumber numberWithInteger:TableItemAutoTip], @"tableItem",
            nil];
    [subItems addObject:item];
    
    NSNumber *takeoutTipAmountSaved = [[NSUserDefaults standardUserDefaults] objectForKey:tipRateLastUsedTakeout];
    
    NSInteger TipTakeoutPercent = round([NSUserDefaults standardUserDefaults].tipTakeoutRate);
    
    item = [NSDictionary dictionaryWithObjectsAndKeys:
            NSLocalizedString(@"Take-out Tip Amount", nil), @"title",
            ((takeoutTipAmountSaved == nil) ? @"—" : [NSString stringWithFormat:@"%ld%%",(long)TipTakeoutPercent]), @"subTitle",
            [NSNumber numberWithInteger:TableItemAutoTakeoutTip], @"tableItem",
            nil];
    [subItems addObject:item];
    
    NSNumber *deliveryTipAmountSaved = [[NSUserDefaults standardUserDefaults] objectForKey:tipRateLastUsedDelivery];
    
    NSInteger TipDeliveryPercent = round([NSUserDefaults standardUserDefaults].tipDeliveryRate);
    
    item = [NSDictionary dictionaryWithObjectsAndKeys:
            NSLocalizedString(@"Delivery Tip Amount", nil), @"title",
            ((deliveryTipAmountSaved == nil) ? @"—" : [NSString stringWithFormat:@"%ld%%",(long)TipDeliveryPercent]), @"subTitle",
            [NSNumber numberWithInteger:TableItemAutoDeliveryTip], @"tableItem",
            nil];
    [subItems addObject:item];
    
    subItemDict = [NSDictionary dictionaryWithObjectsAndKeys:
                   NSLocalizedString(@"Applying Auto Tip Amount", nil), @"title",
                   subItems, @"list",
                   NSLocalizedString(@"The Auto Tip Amount will be changed to the amount of your last payment automatically for each type of order", nil), @"footer",
                   nil];
    
    [self.items addObject:subItemDict];
    
     */
    //======================================================================
    
    subItems = [NSMutableArray array];
    
    //CFBundleShortVersionString
    NSString *appShortVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
    item = [NSDictionary dictionaryWithObjectsAndKeys:
            NSLocalizedString(@"About", nil), @"title",
            [NSString stringWithFormat:@"v%@", appShortVersion], @"subTitle",
            [NSNumber numberWithInteger:TableItemAbout], @"tableItem",
            nil];
    [subItems addObject:item];
    
    item = [NSDictionary dictionaryWithObjectsAndKeys:
            NSLocalizedString(@"Contact Us", nil), @"title",
            [NSNumber numberWithInteger:TableItemContactUs], @"tableItem",
            nil];
    [subItems addObject:item];
    
    item = [NSDictionary dictionaryWithObjectsAndKeys:
            NSLocalizedString(@"FAQ", nil), @"title",
            [NSNumber numberWithInteger:TableItemFaq], @"tableItem",
            nil];
    [subItems addObject:item];
    
    
    item = [NSDictionary dictionaryWithObjectsAndKeys:
            NSLocalizedString(@"User Agreement", nil), @"title",
            [NSNumber numberWithInteger:TableItemEULA], @"tableItem",
            nil];
    [subItems addObject:item];
    
    item = [NSDictionary dictionaryWithObjectsAndKeys:
            NSLocalizedString(@"Privacy Policy", nil), @"title",
            [NSNumber numberWithInteger:TableItemPrivacy], @"tableItem",
            nil];
    [subItems addObject:item];
    
    item = [NSDictionary dictionaryWithObjectsAndKeys:
            NSLocalizedString(@"Electronic Signature Consent", nil), @"title",
            [NSNumber numberWithInteger:TableItemSign], @"tableItem",
            nil];
    [subItems addObject:item];
    
    subItemDict = [NSDictionary dictionaryWithObjectsAndKeys:
                   NSLocalizedString(@"Information", nil), @"title",
                   subItems, @"list",
                   nil];
    
    [self.items addObject:subItemDict];
    
    
    //======================================================================
    
    subItems = [NSMutableArray array];
    
    item = [NSDictionary dictionaryWithObjectsAndKeys:
            NSLocalizedString(@"Become a RushOrder Driver!", nil), @"title",
            [NSNumber numberWithInteger:TableItemBecomDriver], @"tableItem",
            nil];
    [subItems addObject:item];
    
    subItemDict = [NSDictionary dictionaryWithObjectsAndKeys:
                   subItems, @"list",
                   nil];
    
    [self.items addObject:subItemDict];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Settings", nil);
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    self.tableView.sectionHeaderHeight = 35.0f;
    
//    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f,
//                                                                              0.0f,
//                                                                              320.0f,
//                                                                              10.0f)];
    
    self.tableView.backgroundColor = [UIColor c1Color];
    
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemLeftMenu
//                                                                        target:self
//                                                                        action:@selector(leftMenuButtonTouched:)];
}

- (void)leftMenuButtonTouched:(id)sender
{
//    [APP.viewDeckController toggleLeftViewAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.items count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary *subItems = [self.items objectAtIndex:section];
    NSArray *subItemList = [subItems objectForKey:@"list"];
    return [subItemList count];
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    NSDictionary *subItems = [self.items objectAtIndex:section];
//    return [subItems objectForKey:@"title"];
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSDictionary *subItems = [self.items objectAtIndex:section];
    NSString *title = [subItems objectForKey:@"title"];
    if(title != nil){
        return SETTING_SECTION_HEIGHT;
    } else {
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSDictionary *subItems = [self.items objectAtIndex:section];
    NSString *titleString = [subItems objectForKey:@"title"];
    
    if(titleString == nil) return nil;
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0f,
                                                                 0.0f,
                                                                 320.0f,
                                                                 SETTING_SECTION_HEIGHT)];
    
    UILabel *sectionHeaderTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0f,
                                                                                 4.0f,
                                                                                 300.0f,
                                                                                 20.0f)];
//    [sectionHeaderTitleLabel drawBorder];
    sectionHeaderTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    sectionHeaderTitleLabel.backgroundColor = [UIColor clearColor];
    sectionHeaderTitleLabel.textColor = [UIColor c11Color];
    sectionHeaderTitleLabel.font = [UIFont systemFontOfSize:14.0f];
    
    [titleView addSubview:sectionHeaderTitleLabel];
    
    [titleView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-8-[sectionHeaderTitleLabel]-8-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(sectionHeaderTitleLabel)]];
    
    [titleView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-8-[sectionHeaderTitleLabel]"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(sectionHeaderTitleLabel)]];
    
    sectionHeaderTitleLabel.text = titleString;
    
    return titleView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *subItems = [self.items objectAtIndex:indexPath.section];
    NSArray *subItemList = [subItems objectForKey:@"list"];
    NSDictionary *item = [subItemList objectAtIndex:indexPath.row];
    
    NSInteger tableItem = [item integerForKey:@"tableItem"];
    
    if(tableItem == TableItemInviteFriend){
        return 82.0f;
    } else {
        return 44.0f;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    NSDictionary *subItems = [self.items objectAtIndex:section];
    NSString *footerString = [subItems objectForKey:@"footer"];
    
    if(footerString != nil){
        return SETTING_SECTION_FOOTER_HEIGHT;
    } else {
        return 0.0f;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    NSDictionary *subItems = [self.items objectAtIndex:section];
    NSString *footerString = [subItems objectForKey:@"footer"];
    
    if(footerString == nil) return nil;
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f,
                                                                 0.0f,
                                                                 320.0f,
                                                                 SETTING_SECTION_FOOTER_HEIGHT)];
    
    UILabel *sectionFooterTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0f,
                                                                                 10.0f,
                                                                                 300.0f,
                                                                                 SETTING_SECTION_FOOTER_HEIGHT)];
    sectionFooterTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    sectionFooterTitleLabel.backgroundColor = [UIColor clearColor];
    sectionFooterTitleLabel.textColor = [UIColor colorWithR:41.0f G:41.0f B:41.0f];
    sectionFooterTitleLabel.font = [UIFont systemFontOfSize:12.0f];
    sectionFooterTitleLabel.numberOfLines = 0;
    
    [footerView addSubview:sectionFooterTitleLabel];
    
    [footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-[sectionFooterTitleLabel]-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(sectionFooterTitleLabel)]];
    
    [footerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[sectionFooterTitleLabel]"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(sectionFooterTitleLabel)]];
    
    sectionFooterTitleLabel.text = footerString;
    [sectionFooterTitleLabel sizeToFit];
    
    return footerView;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"CustomCell";
    
    NSDictionary *subItems = [self.items objectAtIndex:indexPath.section];
    NSArray *subItemList = [subItems objectForKey:@"list"];
    NSDictionary *item = [subItemList objectAtIndex:indexPath.row];
    
    NSInteger tableItem = [item integerForKey:@"tableItem"];
    
    if(tableItem == TableItemInviteFriend){
        CellIdentifier = @"ShareCell";
    }
    
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if(cell == nil){
        cell = [CustomCell cell];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    if(tableItem != TableItemInviteFriend){
        cell.titleLabel.textColor = [UIColor c6Color];
        [cell setBadgeValue:0];
    }
    if(tableItem == TableItemHideDemo){
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryView = self.demoOnSwitch;
        self.demoOnSwitch.on = [NSUserDefaults standardUserDefaults].isDemoOn;
    } else if(tableItem == TableItemFaq){
        cell.accessoryType = UITableViewCellAccessoryNone;
    } else if(tableItem == TableItemHideTest){
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryView = self.testOnSwitch;
        self.testOnSwitch.on = [NSUserDefaults standardUserDefaults].isTestOn;
    } else if(tableItem == TableItemAutoTip){
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryView = self.autoTipSwitch;
        self.autoTipSwitch.on = [NSUserDefaults standardUserDefaults].adoptLastTipRate;
    } else if(tableItem == TableItemAutoTakeoutTip){
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryView = self.autoTakeoutTipSwitch;
        self.autoTakeoutTipSwitch.on = [NSUserDefaults standardUserDefaults].adoptLastTakeoutTipRate;
    } else if(tableItem == TableItemAutoDeliveryTip){
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryView = self.autoDeliveryTipSwitch;
        self.autoDeliveryTipSwitch.on = [NSUserDefaults standardUserDefaults].adoptLastDeliveryTipRate;
    } else if(tableItem == TableItemSignInOut){
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    } else if (tableItem == TableItemMyCredit){
        [cell setBadgeValue:CRED.customerInfo.referralBadgeCount];
    } else if (tableItem == TableItemBecomDriver){
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_truck_animation"]];
        cell.titleLabel.textColor = [UIColor c11Color];
    } else {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryView = nil;
    }
    
    if(tableItem != TableItemInviteFriend){
    
        cell.titleLabel.text = [item objectForKey:@"title"];
        
        if(tableItem == TableItemPassCode){

            if(CRED.isPinSet){
                cell.subTitleLabel.text = @"On";
            } else {
                cell.subTitleLabel.text = @"Off";
            }
            
        } else {
            cell.subTitleLabel.text = [item objectForKey:@"subTitle"];
        }
    }
    
    return cell;
}

- (void)demoOnSwitchValueChanged:(UISwitch *)sender
{
    [NSUserDefaults standardUserDefaults].demoOn = sender.isOn;
}

- (void)testOnSwitchValueChanged:(UISwitch *)sender
{
    [NSUserDefaults standardUserDefaults].testOn = sender.isOn;
}

- (void)autoTipSwitchValueChanged:(UISwitch *)sender
{
    [NSUserDefaults standardUserDefaults].adoptLastTipRate = sender.isOn;
}

- (void)autoDeliveryTipSwitchValueChanged:(UISwitch *)sender
{
    [NSUserDefaults standardUserDefaults].adoptLastDeliveryTipRate = sender.isOn;
}

- (void)autoTakeoutTipSwitchValueChanged:(UISwitch *)sender
{
    [NSUserDefaults standardUserDefaults].adoptLastTakeoutTipRate = sender.isOn;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self configureMenus];
    
    //Fill Profile
    if(CRED.customerInfo == nil){
        // Guest
        self.profilePhotoImageView.image = [UIImage imageNamed:@"placeholder_profile_photo"];
        self.nameLabel.text = NSLocalizedString(@"Guest", nil);
        self.nameLabel.textColor = [UIColor c6Color];
        [self.nameLabel updateConstraints];
        
        self.signInButton.buttonTitle = NSLocalizedString(@"Sign In", nil);
        self.signInButton.tag = 1;
        self.signUpButton.buttonTitle = NSLocalizedString(@"Sign Up", nil);
        self.signUpButton.tag = 1;
        
    } else {
        // Signed In
        
        self.nameLabel.text = CRED.customerInfo.name;
        self.nameLabel.textColor = [UIColor c9Color];
        [self.nameLabel updateConstraints];
        
        self.signInButton.buttonTitle = NSLocalizedString(@"Edit Profile", nil);
        self.signInButton.tag = 2;
        self.signUpButton.buttonTitle = NSLocalizedString(@"Sign Out", nil);
        self.signUpButton.tag = 2;
        
        if(CRED.customerInfo.photo != nil){
            self.profilePhotoImageView.image = CRED.customerInfo.photo;
        } else {
            [self.profilePhotoImageView sd_setImageWithURL:CRED.customerInfo.photoURL
                                       placeholderImage:[UIImage imageNamed:@"placeholder_profile_photo"]
                                                options:0
                                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                     CRED.customerInfo.photo = image;
                                                 }];
        }
    }
}

- (IBAction)signInButtonTouched:(UIButton *)sender
{
    switch(sender.tag){
        case 1:
        {
            SignInCustomerViewController *viewController = [SignInCustomerViewController viewControllerFromNib];
            [self.tabBarController presentViewControllerInNavigation:viewController
                                                           animated:YES
                                                         completion:^{
                                                             
                                                         }];
        }
            break;
        case 2: //Edit Profile
        {
            ProfileViewController *viewController = [ProfileViewController viewControllerFromNib];
            [self.tabBarController presentViewControllerInNavigation:viewController
                                                           animated:YES
                                                         completion:^{
                                                             
                                                         }];
        }
            break;
    }
    
}

- (IBAction)signUpButtonTouched:(UIButton *)sender
{
    switch(sender.tag){
        case 1:
        {
            SignUpCustomerViewController *viewController = [SignUpCustomerViewController viewControllerFromNib];
            [self.tabBarController presentViewControllerInNavigation:viewController
                                                           animated:YES
                                                         completion:^{

                                                         }];
        }
            break;
        case 2: //Sign Out
        {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Do you really want to sign out?", nil)
                                                                     delegate:self
                                                            cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                       destructiveButtonTitle:NSLocalizedString(@"YES", nil)
                                                            otherButtonTitles:nil];
            [actionSheet showInView:self.view];
        }
            break;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
    
    if(self.showShareAndEarn){
        if(CRED.isSignedIn){
            [self performSegueWithIdentifier:@"inviteFriends"
                                      sender:nil];
        } else {
            [self performSegueWithIdentifier:@"inviteFriendsGuest"
                                      sender:nil];
        }
        
    } else {
        switch(self.autoPushType){
            case AutoPushTypeInvite:
                
                if(CRED.isSignedIn){
                    [self performSegueWithIdentifier:@"inviteFriends"
                                              sender:nil];
                } else {
                    [self performSegueWithIdentifier:@"inviteFriendsGuest"
                                              sender:nil];
                }
                
                self.autoPushType = AutoPushTypeNone;
                break;
            case AutoPushTypeMycredit:
                
                if(CRED.isSignedIn){
                    [self performSegueWithIdentifier:@"myOrages"
                                              sender:nil];
                } else {
                    [self performSegueWithIdentifier:@"guestOrange"
                                              sender:nil];
                }
                
                self.autoPushType = AutoPushTypeNone;
                break;
            default:
                break;
        }
    }
    
//    APP.viewDeckController.panningMode = IIViewDeckFullViewPanning;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
//    APP.viewDeckController.panningMode = IIViewDeckNoPanning;
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
#ifdef FLURRY_ENABLED
    [Flurry endTimedEvent:NSStringFromClass([self class])
           withParameters:nil];
#endif
    
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *subItems = [self.items objectAtIndex:indexPath.section];
    NSArray *subItemList = [subItems objectForKey:@"list"];
    NSDictionary *item = [subItemList objectAtIndex:indexPath.row];
    
    NSNumber *tableItem = [item objectForKey:@"tableItem"];
    
    /*
     TableItemMyCards = 1,
     TableItemMyHistory,
     TableItemAbout
     */
    
#ifdef SSL_ENABLED
    NSString *urlProtocol = @"https://";
#else
    NSString *urlProtocol = @"http://";
#endif
    NSString *serverAPIURL = [urlProtocol stringByAppendingString:SERVER_URL_ADDRESS];
    
    UIViewController *viewController = nil;
    switch([tableItem integerValue]){
        case TableItemMyCards:
            viewController = [SelectCardViewController viewControllerFromNib];
            ((SelectCardViewController *)viewController).manageMode = YES;
            break;
        case TableItemMyCredit:
//            viewController = [CreditPointViewController viewControllerFromNib];
//            viewController.title = [item objectForKey:@"title"];
//            
            if(CRED.isSignedIn){
                [self performSegueWithIdentifier:@"myOrages"
                                          sender:nil];
            } else {
                [self performSegueWithIdentifier:@"guestOrange"
                                          sender:nil];
            }
            break;
        case TableItemMyHistory:
            viewController = [[PaymentHistoryViewController alloc] initWithStyle:UITableViewStylePlain];
            break;
        case TableItemReceiptMailing:
            viewController = [MailMeViewController viewControllerFromNib];
            ((MailMeViewController *)viewController).delegate = self;
            break;
        case TableItemInviteFriend:
            if(CRED.isSignedIn){
                [self performSegueWithIdentifier:@"inviteFriends"
                                          sender:nil];
            } else {
                [self performSegueWithIdentifier:@"inviteFriendsGuest"
                                          sender:nil];
            }
            break;
        case TableItemChangePassword:
        {
            ChangePasswordViewController *viewController = [ChangePasswordViewController viewControllerFromNib];
            [self.navigationController pushViewController:viewController
                                                 animated:YES];
        }
            break;
        case TableItemSignInOut:
            // This section is replaced with top sign in/out button
            break;
        case TableItemAbout:
            viewController = [AboutViewController viewControllerFromNib];
            ((AboutViewController *)viewController).versionString = [item objectForKey:@"subTitle"];
            break;
        case TableItemFaq:
//            [self performSegueWithIdentifier:@"faqSegue"
//                                      sender:nil];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[serverAPIURL stringByAppendingString:@"/customer/faq#faq"]]];
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://localhost:3000/customer/faq#faq"]];
            [self.tableView deselectRowAtIndexPath:indexPath
                                          animated:YES];
            break;
        case TableItemEULA:
            viewController = [EULAViewController viewControllerFromNib];
            viewController.title = [item objectForKey:@"title"];
            ((EULAViewController *) viewController).urlString = [serverAPIURL stringByAppendingString:@"/legal/ua/"];
            break;
        case TableItemPrivacy:
            viewController = [EULAViewController viewControllerFromNib];
            viewController.title = [item objectForKey:@"title"];
            ((EULAViewController *) viewController).urlString = [serverAPIURL stringByAppendingString:@"/legal/privacy"];
            break;
        case TableItemSign:
            viewController = [EULAViewController viewControllerFromNib];
            viewController.title = [item objectForKey:@"title"];
            ((EULAViewController *) viewController).urlString = [serverAPIURL stringByAppendingString:@"/legal/sign"];
            break;
        case TableItemPassCode:
            viewController = [PassCodeSettingViewController viewControllerFromNib];
            break;
        case TableItemContactUs:
            viewController = [ContactUsViewController viewControllerFromNib];
            break;
        case TableItemDeliveryAddress:
            [self performSegueWithIdentifier:@"DeliveryAddressSegue"
                                      sender:nil];
            break;
        case TableItemBecomDriver:
        {
            NSString *urlString = [NSString stringWithFormat:@"%@%@/become_drivers#ft", urlProtocol, SERVER_URL_ADDRESS];
            PCLog(@"String %@", urlString);
            NSURL *url = [NSURL URLWithString:urlString];
            PCLog(@"url %@", url);
            [[UIApplication sharedApplication] openURL:url];
        }
            break;
    }
    
    if(viewController != nil){
        [self.navigationController pushViewController:viewController
                                             animated:YES];
    }    
}


- (void)mailMeViewController:(MailMeViewController *)viewController didTouchDoneButton:(id)sender
{
    [NSUserDefaults standardUserDefaults].emailAddress = viewController.mailTextField.text;
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        default:
            break;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == actionSheet.destructiveButtonIndex){
        [self requestSignOut];
    }
}

- (void)scrollToTop
{
    [self.tableView setContentOffset:CGPointZero
                            animated:YES];
}

- (void)requestSignOut
{
    RequestResult result = RRFail;
    result = [CRED requestSignOutWithCompletionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while Signing out", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (UISwitch *)demoOnSwitch
{
    if(_demoOnSwitch == nil){
        _demoOnSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
        [_demoOnSwitch addTarget:self
                             action:@selector(demoOnSwitchValueChanged:)
                   forControlEvents:UIControlEventValueChanged];
//        _demoOnSwitch.onTintColor = [UIColor colorWithR:205 G:34 B:59];
    }
    return _demoOnSwitch;
}

- (UISwitch *)testOnSwitch
{
    if(_testOnSwitch == nil){
        _testOnSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
        [_testOnSwitch addTarget:self
                          action:@selector(testOnSwitchValueChanged:)
                forControlEvents:UIControlEventValueChanged];
//        _testOnSwitch.onTintColor = [UIColor colorWithR:205 G:34 B:59];
    }
    return _testOnSwitch;
}

- (UISwitch *)autoTipSwitch
{
    if(_autoTipSwitch == nil){
        _autoTipSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
        [_autoTipSwitch addTarget:self
                          action:@selector(autoTipSwitchValueChanged:)
                forControlEvents:UIControlEventValueChanged];
//        _autoTipSwitch.onTintColor = [UIColor colorWithR:205 G:34 B:59];
    }
    return _autoTipSwitch;
}

- (UISwitch *)autoTakeoutTipSwitch
{
    if(_autoTakeoutTipSwitch == nil){
        _autoTakeoutTipSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
        [_autoTakeoutTipSwitch addTarget:self
                           action:@selector(autoTakeoutTipSwitchValueChanged:)
                 forControlEvents:UIControlEventValueChanged];
        //        _autoTakeoutTipSwitch.onTintColor = [UIColor colorWithR:205 G:34 B:59];
    }
    return _autoTakeoutTipSwitch;
}

- (UISwitch *)autoDeliveryTipSwitch
{
    if(_autoDeliveryTipSwitch == nil){
        _autoDeliveryTipSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
        [_autoDeliveryTipSwitch addTarget:self
                           action:@selector(autoDeliveryTipSwitchValueChanged:)
                 forControlEvents:UIControlEventValueChanged];
        //        _autoDeliveryTipSwitch.onTintColor = [UIColor colorWithR:205 G:34 B:59];
    }
    return _autoDeliveryTipSwitch;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if(self.showShareAndEarn){
        self.showShareAndEarn = NO;
        id viewController = [segue isDestClass:[InviteFriendViewController class]];
        if(viewController){
            ((InviteFriendViewController *)viewController).backToRestaurantPage = YES;
            return;
        }
        
        viewController = [segue isDestClass:[InviteFriendGuideViewController class]];
        if(viewController){
            ((InviteFriendGuideViewController *)viewController).backToRestaurantPage = YES;
            return;
        }
    }
}

@end
