//
//  ReceiptViewController.h
//  RushOrder
//
//  Created by Conan on 2/22/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Receipt.h"
#import "Order.h"
#import "Payment.h"
#import "MobileCard.h"
#import <MessageUI/MessageUI.h>
#import "MailMeViewController.h"
#import "VerificationViewController.h"

@interface ReceiptViewController : PCViewController
<
UIAlertViewDelegate,
MFMessageComposeViewControllerDelegate,
UITableViewDelegate,
UITableViewDataSource,
UIActionSheetDelegate,
MailMeViewControllerDelegate,
VerificationViewControllerDelegate,
LogoCircleImageViewAction,
TouchableLabelAction
>

@property (strong, nonatomic) Receipt *receipt;
@property (strong, nonatomic) Order *order;
@property (strong, nonatomic) Payment *payment;
@property (strong, nonatomic) MobileCard *mobileCard;

@property (nonatomic, getter = isBackToRoot)BOOL backToRoot;
@end
