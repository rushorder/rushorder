//
//  DeliveryAddressViewController.m
//  RushOrder
//
//  Created by Conan Kim on 2/20/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//


#import "DeliveryAddressViewController.h"
#import "PCPaymentService.h"
#import "PCCredentialService.h"
#import "RemoteDataManager.h"
#import "Addresses.h"
#import "GMPrediction.h"
#import "PhoneNumberFormatter.h"

@interface DeliveryAddressViewController ()
{
    int _textFieldSemaphore;
    __strong PhoneNumberFormatter *_phoneNumberFormatter;
}

@property (weak, nonatomic) IBOutlet PCTextField *nameTextField;
@property (weak, nonatomic) IBOutlet PCTextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet PCTextField *streetAddressTextField;
@property (weak, nonatomic) IBOutlet PCTextField *street2AddressTextField;
@property (weak, nonatomic) IBOutlet PCTextField *cityTextField;
@property (weak, nonatomic) IBOutlet PCTextField *stateTextField;
@property (weak, nonatomic) IBOutlet PCTextField *zipTextField;
@property (weak, nonatomic) IBOutlet UITextView *deliveryInstructionTextView;
@property (weak, nonatomic) IBOutlet NPToggleButton *defaultButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *locationButton;

@property (strong, nonatomic) CLGeocoder *geoCoder;

@property (strong, nonatomic) IBOutlet UIPickerView *statePickerView;
@property (strong, nonatomic) IBOutlet UIToolbar *pickerToolBar;

@property (strong, nonatomic) NSArray *stateList;
@property (nonatomic) CLLocationCoordinate2D geocodedCoords;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *saveButtonTopConstraint2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *saveButtonTopConstraint1;

@end

@implementation DeliveryAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _phoneNumberFormatter = [[PhoneNumberFormatter alloc] init];
    
    if(self.deliveryAddress != nil){ // Show, Edit
        [self drawDeliveryAddress];
        self.title = self.deliveryAddress.shortAddress;
    } else { // New
        self.title = NSLocalizedString(@"New Delivery Address",nil);
    }
    
    self.stateTextField.inputView = self.statePickerView;
    self.stateTextField.inputAccessoryView = self.pickerToolBar;
    
    [self.locationButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
    
    self.stateList = @[@"Alabama",
                       @"Alaska",
                       @"Arizona",
                       @"Arkansas",
                       @"California",
                       @"Colorado",
                       @"Connecticut",
                       @"Delaware",
                       @"Florida",
                       @"Georgia",
                       @"Hawaii",
                       @"Idaho",
                       @"Illinois",
                       @"Indiana",
                       @"Iowa",
                       @"Kansas",
                       @"Kentucky",
                       @"Louisiana",
                       @"Maine",
                       @"Maryland",
                       @"Massachusetts",
                       @"Michigan",
                       @"Minnesota",
                       @"Mississippi",
                       @"Missouri",
                       @"Montana",
                       @"Nebraska",
                       @"Nevada",
                       @"New Hampshire",
                       @"New Jersey",
                       @"New Mexico",
                       @"New York",
                       @"North Carolina",
                       @"North Dakota",
                       @"Ohio",
                       @"Oklahoma",
                       @"Oregon",
                       @"Pennsylvania",
                       @"Rhode Island",
                       @"South Carolina",
                       @"South Dakota",
                       @"Tennessee",
                       @"Texas",
                       @"Utah",
                       @"Vermont",
                       @"Virginia",
                       @"Washington",
                       @"West Virginia",
                       @"Wisconsin",
                       @"Wyoming"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)drawDeliveryAddress
{
    self.nameTextField.text = self.deliveryAddress.name;
    self.phoneNumberTextField.text = self.deliveryAddress.phoneNumber;
    self.streetAddressTextField.text = self.deliveryAddress.street;
    self.street2AddressTextField.text = self.deliveryAddress.street2;
    self.cityTextField.text = self.deliveryAddress.city;
    self.stateTextField.text = self.deliveryAddress.state;
    self.zipTextField.text = self.deliveryAddress.zip;
    self.deliveryInstructionTextView.text = self.deliveryAddress.deliveryInstruction;
    
    self.defaultButton.hidden = self.deliveryAddress.isDefaultAddress;
    self.defaultButton.selected = self.deliveryAddress.isDefaultAddress;
    if(self.defaultButton.hidden){
        self.saveButtonTopConstraint2.priority = 999;
        self.saveButtonTopConstraint1.priority = 500;
    } else {
        self.saveButtonTopConstraint1.priority = 999;
        self.saveButtonTopConstraint2.priority = 500;
    }
}

- (IBAction)saveButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    if(![VALID validate:self.nameTextField
                  title:NSLocalizedString(@"Please Enter Name", nil)
                message:NSLocalizedString(@"Name is Required to Make a Delivery Address", nil)]){
        return;
    }
    
    if(![VALID validate:self.phoneNumberTextField
                  title:NSLocalizedString(@"Please Enter Phone Number", nil)
                message:NSLocalizedString(@"A Phone Number is Required to Make a Delivery Address", nil)]){
        return;
    }
    
    NSString * number = self.phoneNumberTextField.text;
    
    NSString *phoneRegex2 = @"^(\\([0-9]{3})\\) [0-9]{3}-[0-9]{4}$";
    NSPredicate *phoneTest2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex2];
    BOOL isValid =[phoneTest2 evaluateWithObject:number];
    
    if(![VALID validate:self.phoneNumberTextField
              condition:isValid
                  title:NSLocalizedString(@"Please Enter a Valid Phone Number", nil)
                message:NSLocalizedString(@"A valid phone number is required to Make a Delivery Address - (XXX) XXX-XXXX", nil)]){
        return;
    }
    
    if(![VALID validate:self.streetAddressTextField
                  title:NSLocalizedString(@"Please Enter Street Address", nil)
                message:NSLocalizedString(@"Address1 is Required to Make a Delivery Address", nil)]){
        return;
    }
    
    if(![VALID validate:self.zipTextField
                  title:NSLocalizedString(@"Please Enter Zip code", nil)
                message:NSLocalizedString(@"Zip code is Required to Make a Delivery Address", nil)]){
        return;
    }
    
    if(![VALID validate:self.zipTextField
              condition:self.zipTextField.text.length == 5
                  title:NSLocalizedString(@"Please Enter a Valid Zip Code", nil)
                message:nil]){
        return;
    }
    
    if(![VALID validate:self.cityTextField
                  title:NSLocalizedString(@"Please Enter City", nil)
                message:NSLocalizedString(@"City is Required to Make a Delivery Address", nil)]){
        return;
    }
    
    if(![VALID validate:self.stateTextField
                  title:NSLocalizedString(@"Please Select State", nil)
                message:NSLocalizedString(@"State is Required to Make a Delivery Address", nil)]){
        return;
    }

    
    
    RequestResult result = RRFail;
    
    if(self.deliveryAddress == nil){
        
        DeliveryAddress *deliveryAddress = [[DeliveryAddress alloc] init];
        
        [self updateDeliveryAddress:deliveryAddress];
        
        result = [PAY requestAddDeliveryAddress:deliveryAddress
                       completionBlock:^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode) {
                           if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                               switch(response.errorCode){
                                   case ResponseSuccess:{
                                       
                                       [self.navigationController popViewControllerAnimated:YES];
                                       
                                       [REMOTE handleDeliveryAddressesResponse:(NSMutableArray *)response
                                                              withNotification:YES];
                                       
                                   }
                                       break;
                                   default:
                                   {
                                       NSString *message = [response objectForKey:@"message"];
                                       NSString *title = [response objectForKey:@"title"];
                                       if(title == nil){
                                           [UIAlertView alert:message];
                                       } else {
                                           [UIAlertView alertWithTitle:title
                                                               message:message];
                                       }
                                   }
                                       break;
                               }
                           } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                               [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                               [CRED resetUserAccount];
                           } else {
                               if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                                   [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                                       object:nil];
                               } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                                   [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                                       ,statusCode]];
                               }
                           }
                           [SVProgressHUD dismiss];
                       }];
    } else {
        
        [self updateDeliveryAddress:self.deliveryAddress];
        result = [PAY requestUpdateDeliveryAddress:self.deliveryAddress
                                completionBlock:^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode) {
                                    if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                                        switch(response.errorCode){
                                            case ResponseSuccess:
                                                
                                                [self.navigationController popViewControllerAnimated:YES];
                                                
                                                [REMOTE handleDeliveryAddressesResponse:(NSMutableArray *)response
                                                                       withNotification:YES];
                                                
                                                break;
                                            default:
                                            {
                                                NSString *message = [response objectForKey:@"message"];
                                                NSString *title = [response objectForKey:@"title"];
                                                if(title == nil){
                                                    [UIAlertView alert:message];
                                                } else {
                                                    [UIAlertView alertWithTitle:title
                                                                        message:message];
                                                }
                                            }
                                                break;
                                        }
                                    } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                                        [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                                        [CRED resetUserAccount];
                                    } else {
                                        if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                                            [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                                                object:nil];
                                        } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                                            [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                                                ,statusCode]];
                                        }
                                    }
                                    [SVProgressHUD dismiss];
                                }];

    }
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            [SVProgressHUD dismiss];
            break;
        default:
            [SVProgressHUD dismiss];
            break;
    }
}

#pragma mark - UIPickerViewDatasource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.stateList count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self.stateList objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    return [self selectStateAtIndex:row];
}

- (void)selectStateAtIndex:(NSInteger)index
{
    self.stateTextField.text = [self.stateList objectAtIndex:index];
}

- (IBAction)inputDoneButtonTouched:(id)sender
{
    NSInteger selectedRow = [self.statePickerView selectedRowInComponent:0];
    [self selectStateAtIndex:selectedRow];
    [self.stateTextField resignFirstResponder];
}

- (void)updateDeliveryAddress:(DeliveryAddress *)deliveryAddress
{
    deliveryAddress.name = self.nameTextField.text;
    deliveryAddress.phoneNumber = self.phoneNumberTextField.text;
    deliveryAddress.street = self.streetAddressTextField.text;
    deliveryAddress.street2 = self.street2AddressTextField.text;
    deliveryAddress.city = self.cityTextField.text;
    deliveryAddress.state = self.stateTextField.text;
    deliveryAddress.zip = self.zipTextField.text;
    deliveryAddress.deliveryInstruction = self.deliveryInstructionTextView.text;
    deliveryAddress.defaultAddress = self.defaultButton.selected;
}

- (IBAction)defaultButtonTouched:(NPToggleButton *)sender {
//    if(self.deliveryAddress != nil){ // Show, Edit
//        if(sender.selected){
//            [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot make this address as not default option", nil)
//                                message:NSLocalizedString(@"You can change default address only by making other address as default option", nil)];
//        } else {
//            sender.selected = YES;
//        }
//    } else { // New
        sender.selected = !sender.selected;
//    }
}

- (IBAction)locationButtonTouched:(id)sender
{
    LocationViewController *viewController = [UIStoryboard viewController:@"LocationViewController"
                                                                     from:@"Restaurants"];
    AddressAnnotation *annotation = [[AddressAnnotation alloc] init];
    if(self.geocodedCoords.latitude == 0.0 && self.geocodedCoords.longitude == 0.0){
        annotation.coordinate = APP.location.coordinate;
//        annotation.title = self.streetAddressTextField.text;
//        annotation.subtitle = self.street2AddressTextField.text;
    } else {
        annotation.coordinate = self.geocodedCoords;
        annotation.title = self.self.streetAddressTextField.text;
        annotation.subtitle = self.self.streetAddressTextField.text;
    }
    viewController.selectedAddress = annotation;
    viewController.delegate = self;
    
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^{
                                     
                                 }];
}

- (void)locationViewController:(LocationViewController *)viewController
            didApplyCoordinate:(CLLocationCoordinate2D)location
                          name:(NSString *)name
            placemarkOrAddress:(id)obj
{
    if([obj isKindOfClass:[Addresses class]]){
        Addresses *address = (Addresses *)obj;
        [self fillAddresses:address];
        
    } else if([obj isKindOfClass:[CLPlacemark class]]){
        CLPlacemark *place = (CLPlacemark *)obj;
        
        [self fillAddressesWithPlacemark:place];
        if([self.streetAddressTextField.text length] == 0){
            [self.streetAddressTextField becomeFirstResponder];
        } else if([self.street2AddressTextField.text length] == 0){
            [self.street2AddressTextField becomeFirstResponder];
        }
    } else if([obj isKindOfClass:[GMPrediction class]]){
        GMPrediction *prediction = (GMPrediction *)obj;
        [self fillAddressesWithPrediction:prediction];
        
        if([self.streetAddressTextField.text length] == 0){
            [self.streetAddressTextField becomeFirstResponder];
        } else if([self.street2AddressTextField.text length] == 0){
            [self.street2AddressTextField becomeFirstResponder];
        }
    }
}

- (void)didCancelLocationViewController:(LocationViewController *)viewController
{
    
}

- (void)fillAddresses:(Addresses *)addresses
{
    self.streetAddressTextField.text = addresses.address1;
    self.street2AddressTextField.text = addresses.address2;
    self.zipTextField.text = addresses.zip;
    self.cityTextField.text = addresses.city;
    self.stateTextField.text = addresses.state;
    self.geocodedCoords = addresses.coordinate;
}

- (void)fillAddressesWithPlacemark:(CLPlacemark *)placemark
{
    self.streetAddressTextField.text = [placemark.addressDictionary objectForKey:@"Street"];
    self.street2AddressTextField.text = @"";
    self.zipTextField.text = [placemark.addressDictionary objectForKey:@"ZIP"];
    self.cityTextField.text = [placemark.addressDictionary objectForKey:@"City"];
    self.stateTextField.text = [placemark.addressDictionary objectForKey:@"State"];
    self.geocodedCoords = placemark.location.coordinate;
}

- (void)fillAddressesWithPrediction:(GMPrediction *)prediction
{
    self.streetAddressTextField.text = prediction.street;
    self.street2AddressTextField.text = @"";
    self.zipTextField.text = prediction.zip;
    self.cityTextField.text = prediction.city;
    self.stateTextField.text = prediction.state;
    self.geocodedCoords = prediction.coordinate;
}


- (CLGeocoder *)geoCoder
{
    if(_geoCoder == nil){
        _geoCoder = [[CLGeocoder alloc] init];
    }
    return _geoCoder;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isKindOfClass:[PCTextField class]]){
        [textField resignFirstResponder];
        [[(PCTextField *)textField nextField] becomeFirstResponder];
    }
    return YES;
}

- (IBAction)nextButtonTouched:(id)sender
{
    [self inputDoneButtonTouched:sender];
    [self.zipTextField becomeFirstResponder];
}

- (IBAction)phoneNumberValueChanged:(UITextField *)sender
{
    if(_textFieldSemaphore) return;
    
    _textFieldSemaphore = 1;
    
    NSString *locale = [[NSLocale currentLocale] localeIdentifier];
    sender.text = [_phoneNumberFormatter format:sender.text
                                     withLocale:locale];
    _textFieldSemaphore = 0;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
