//
//  NoticeViewController.m
//  RushOrder
//
//  Created by Conan on 6/13/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "NoticeViewController.h"

@interface NoticeViewController ()

@end

@implementation NoticeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Notice", nil);
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemClose
                                                                         target:self
                                                                         action:@selector(closeTouched:)];
    
}

- (void)closeTouched:(id)sender
{
    [NSUserDefaults standardUserDefaults].lastAccessedNoticeId = self.noticeNo;
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 
                             }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.noticeWebView loadHTMLString:self.content
                               baseURL:nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
