//
//  PaymentMethodViewController.m
//  RushOrder
//
//  Created by Conan on 2/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PaymentMethodViewController.h"
#import "NewCardViewController.h"
#import "MobileCardCell.h"
#import "MobileCard.h"
#import "Merchant.h"
#import "PCPaymentService.h"
#import "ReceiptViewController.h"
#import "PCCredentialService.h"
#import "NewCardCell.h"
#import "OrderManager.h"
#import "PCMenuService.h"
#import "TransactionManager.h"
#import "TogoAuthViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NPBatchTableView.h"
#import "RestaurantViewController.h"
#import "Addresses.h"
#import "PCCredentialService.h"
#import "RemoteDataManager.h"
#import "SignInCustomerViewController.h"
#import "DiscountViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "AccountSwitchingManager.h"
#import "CustomRequest.h"
#import "MenuManager.h"
#import "OrderViewController.h"
#import <Google/Analytics.h>
@import Firebase;

enum _alertViewTag{
    AlertViewTagStripeError = 1,
    AlertViewTagContinuePayment,
    AlertViewTagContinueOrderPayment,
    AlertViewTagInvalidOrderError,
    AlertViewTagPromoOrderAmount,
    AlertViewTagTipInput
};

@interface PaymentMethodViewController ()

@property (weak, nonatomic) IBOutlet NPBatchTableView *tableView;
@property (weak, nonatomic) IBOutlet NPStretchableButton *neoCardButton;
@property (strong, nonatomic) PaymentSummaryView *paymentSummaryView;

@property (nonatomic, strong) NSMutableArray *sectionArray;
@property (nonatomic) CGFloat initialHeaderHeight;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
#if USE_ORANGE_CELL
@property (strong, nonatomic) NSNumber *useCreditNumber;
#endif
@property (nonatomic) BOOL shouldBackToRoot;
@property (strong, nonatomic) MobileCard *selectedCard;
@property (nonatomic, getter = isUseCredit) BOOL useCredit;

@property (copy, nonatomic) NSString *lineItemJsonList;




@end

@implementation PaymentMethodViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.useCredit = YES;
#if USE_ORANGE_CELL
        self.useCreditNumber = [NSNumber numberWithInt:0];
#endif
        
        [self requestMyCredit];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(signedIn:)
                                                     name:SignedInNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(signedOut:)
                                                     name:SignedOutNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(cardListChanged:)
                                                     name:CardListChangeNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)signedIn:(NSNotification *)aNoti
{
    if(CRED.isUnattendedSignUp){
        // Don't make nil!
    } else {
        self.selectedCard = nil;
//        ORDER.tempDeliveryAddress.receiverName = nil;
//        ORDER.tempDeliveryAddress.phoneNumber = nil;
    }
    
    if(self.promotionCodeForCheckAfterSigningIn != nil){
        [self validatePromotionWithCode:self.promotionCodeForCheckAfterSigningIn];
    }
    
    self.tableView.tableHeaderView = nil;
    [self.paymentSummaryView drawPayment];
    self.tableView.tableHeaderView = self.paymentSummaryView;
    
}

- (void)signedOut:(NSNotification *)aNoti
{
    self.tableView.tableHeaderView = nil;
    [self.paymentSummaryView drawPayment];
    self.tableView.tableHeaderView = self.paymentSummaryView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Payment", nil);
    
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemBack
                                                                        target:self
                                                                        action:@selector(backButtonTouched:)];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    

    [self configureLineItems];
    
    self.paymentSummaryView = [PaymentSummaryView view];
    self.paymentSummaryView.order = ORDER.order;
    self.paymentSummaryView.payment = self.payment;
    self.paymentSummaryView.merchant = ORDER.merchant;
    self.paymentSummaryView.delegate = self;
    self.tableView.tableHeaderView = self.paymentSummaryView;
}

- (void)setInitialOptionValueWithOrder:(Order *)order
{
    
    self.address = [[Addresses alloc] init];
    
    self.address.state = order.state;
    self.address.city = order.city;
    self.address.address1 = order.address1;
    self.address.address2 = order.address2;
    self.address.zip = order.zip;
    self.address.phoneNumber = order.phoneNumber;
    self.address.receiverName = order.receiverName;
    self.address.coordinate = order.takenCoordinate;
    
    self.receiverName = order.receiverName;
    self.phoneNumber = order.phoneNumber;
    self.requestMessage = order.customerRequest;
    self.afterMinute = order.pickupAfter;

}

- (void)configureLineItems
{
    if([ORDER.order.lineItems count] > 0){
        NSMutableArray *arrayForJSON = [NSMutableArray array];
        
        for(LineItem * lineItem in ORDER.order.lineItems){
            if(!lineItem.isSplitExcept && !lineItem.isPaid){
                [arrayForJSON addObject:[NSNumber numberWithSerial:lineItem.lineItemNo]];
            }
        }
        
        if([arrayForJSON count] > 0){
            self.lineItemJsonList = [arrayForJSON JSONString];
        }
    }
}

- (void)backButtonTouched:(id)sender
{
    if(self.shouldBackToRoot){
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else {
        if(ORDER.order.status == OrderStatusDraft
           && ORDER.cart.cartType != CartTypeCart){
            if(self.disposeOrderWhenBack){
                ORDER.order = nil;
            }
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if(!CRED.isSignedIn){
        self.promotionCodeForCheckAfterSigningIn = nil;
    } else {
        if(self.promotionCodeForCheckAfterSigningIn != nil){
            [self validatePromotionWithCode:self.promotionCodeForCheckAfterSigningIn];
        }
    }
    
    self.tableView.tableHeaderView = nil;
    [self.paymentSummaryView drawPayment];
    self.tableView.tableHeaderView = self.paymentSummaryView;

    [self.tableView reloadData];
    
    [self selectUseOranges:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(CRED.isUnattendedSignUp){
        // Don't refresh temporarily
    } else {
        if(CRED.isSignedIn){
            self.paymentMethodList = REMOTE.cards;
        } else {
            self.paymentMethodList = [MobileCard listAll];
        }
        
        [self.tableView reloadData];
    }
}

- (void)cardListChanged:(NSNotification *)notification
{
    if(CRED.isUnattendedSignUp){
        // Don't refresh temporarily
    } else {
        self.selectedCard = nil;
        if(CRED.isSignedIn){
            self.paymentMethodList = REMOTE.cards;
        } else {
            self.paymentMethodList = [MobileCard listAll];
        }
        
        [self.tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sectionArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id sectionObj = [self.sectionArray objectAtIndex:section];
    if([sectionObj isNSArray]){
        return [(NSArray *)sectionObj count];
    } else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id sectionObj = [self.sectionArray objectAtIndex:indexPath.section];
    
    id obj = nil;
    if([sectionObj isNSArray]){
        obj = [self.paymentMethodList objectAtIndex:indexPath.row];
    } else {
        obj = sectionObj;
    }
    
    
    if([obj isKindOfClass:[MobileCard class]]){
        return [self tableView:tableView cardCellForRowAtIndexPath:indexPath];
    } else if([obj isKindOfClass:[NSString class]]){
        return [self tableView:tableView cardCellForRowAtIndexPath:indexPath];
    } else if([obj isKindOfClass:[NSNumber class]]){
#if USE_ORANGE_CELL
            return [self tableView:tableView orangeCellForRowAtIndexPath:indexPath];
#else
        return [self tableView:tableView cardCellForRowAtIndexPath:indexPath]; // This is fallback cell, Don't use card cell in real situations.
#endif

    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id sectionObj = [self.sectionArray objectAtIndex:indexPath.section];
    
    id obj = nil;
    if([sectionObj isNSArray]){
        obj = [self.paymentMethodList objectAtIndex:indexPath.row];
    } else {
        obj = sectionObj;
    }
    
    if([obj isKindOfClass:[MobileCard class]] && [obj isKindOfClass:[NSString class]]){ //Mobile card cell
        return 56.0f;
    } else if([obj isKindOfClass:[NSNumber class]]){ //Point cell
        return 52.0f;
    } else { // Default
        return 56.0f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cardCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MobileCardCell";
    
    MobileCardCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [MobileCardCell cell];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;

    }
    
    id obj = [self.paymentMethodList objectAtIndex:indexPath.row];
    
    MobileCard *mobileCard = nil;
    NSString *castedString = nil;
    
    if([obj isKindOfClass:[MobileCard class]]){
        mobileCard = (MobileCard *)obj;
    } else {
        castedString = (NSString *)obj;
    }
    
    if(mobileCard != nil){
        cell.neoCardLabel.hidden = YES;
        cell.titleLabel.text = mobileCard.formattedCardNumber;
        cell.subTitleLabel.text = [NSString stringWithFormat:@"%@ (%02ld/%02ld)",
                                   mobileCard.bankName,
                                   (long)mobileCard.cardExpireMonth,
                                   (long)mobileCard.cardExpireYear];
        
        cell.thumbnailImageView.image = mobileCard.cardLogo;
    } else {
        cell.neoCardLabel.hidden = NO;
        cell.titleLabel.text = nil;
        cell.subTitleLabel.text = nil;
        cell.thumbnailImageView.image = [UIImage imageNamed:@"icon_card"];
    }
    
    NPToggleButton *toggleButton = (NPToggleButton *)cell.checkMarkView;
    
    cell.amountLabel.text = [NSString stringWithFormat:@"(%@)",self.payment.netAmountString];
    if(self.selectedIndexPath.row == indexPath.row){
        toggleButton.selected = YES;
        cell.amountLabel.hidden = NO;
    } else {
        toggleButton.selected = NO;
        cell.amountLabel.hidden = YES;
    }
    
    
    
    [self.tableView bringSubviewToFront:cell];
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView stringCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NewCardCell";
    
    NewCardCell *cell = (NewCardCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[NewCardCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        cell.textLabel.textColor = [UIColor colorWithR:71.0f G:71.0f B:71.0f];
        
        NPToggleButton *toggleButton = [[NPToggleButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 38.0f, 38.0f)];
        [toggleButton setImage:nil
                      forState:UIControlStateNormal];
        [toggleButton setImage:[UIImage imageNamed:@"icon_check_off"]
                      forState:UIControlStateSelected];
        toggleButton.userInteractionEnabled = NO;
        
        cell.accessoryView = toggleButton;
    }
    
    NSString *string = [self.paymentMethodList objectAtIndex:indexPath.row];;
    cell.textLabel.text = string;
    
    NPToggleButton *toggleButton = (NPToggleButton *)cell.accessoryView;
    toggleButton.selected = (self.selectedIndexPath.row == indexPath.row);
    
    return cell;
}

#if USE_ORANGE_CELL
- (UITableViewCell *)tableView:(UITableView *)tableView orangeCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"OrangeCell";
    
    OrangeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [OrangeCell cell];
    }
    
    NSNumber *credit = [self.sectionArray objectAtIndex:indexPath.row];;
    cell.creditBalaceLabel.text = [credit pointString];
    cell.useCreditLabel.text = [self.useCreditNumber pointString];
    cell.useCreditAmountLabel.text = [NSString stringWithFormat:@"(%@)",[self.useCreditNumber currencyString]];
    
//    NSInteger maxUsableCredit = MIN(self.payment.payAmountWithoutCents, CRED.customerInfo.credit);
    
//    cell.useCreditSlider.value = (float)[self.useCreditNumber longLongValue] / (float)maxUsableCredit;
    
    return cell;
}
#endif

- (UITableViewCell *)tableView:(UITableView *)tableView creditAuthCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CreditAuthCell";
    
    CreditAuthCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [CreditAuthCell cell];
        cell.delegate = self;
        
        //        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        //        cell.accessoryType = UITableViewCellAccessoryNone;
        //        cell.textLabel.textAlignment = NSTextAlignmentRight;
        
        //        NPToggleButton *toggleButton = [[NPToggleButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 38.0f, 38.0f)];
        //        [toggleButton setImage:[UIImage imageNamed:@"UIRemoveControlMultiNotCheckedImage"]
        //                      forState:UIControlStateNormal];
        //        [toggleButton setImage:[UIImage imageNamed:@"UIRemoveControlMultiCheckedImage"]
        //                      forState:UIControlStateSelected];
        //        toggleButton.manualSelection = YES;
        //        [toggleButton addTarget:self
        //                         action:@selector(useCreditButtonTouched:)
        //               forControlEvents:UIControlEventTouchUpInside];
        //
        //        cell.accessoryView = toggleButton;
    }
    
    NSNumber *credit = [self.sectionArray objectAtIndex:indexPath.row];;
    cell.creditBalaceLabel.text = [credit pointString];
    
    return cell;
}

- (void)creditAuthCell:(CreditAuthCell *)cell didTouchedVerifyButton:(id)sender
{
    VerificationViewController *viewController = [VerificationViewController viewControllerFromNib];
    viewController.delegate = self;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^(){
                                 }];
}

- (void)viewController:(VerificationViewController *)viewController
   successRegistration:(BOOL)success
{
    if(success){
        [self.tableView reloadData];
    }
}

- (void)useCreditButtonTouched:(UIButton *)sender
{
    self.useCredit = !self.useCredit;
    [self.tableView reloadData];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id sectionObj = [self.sectionArray objectAtIndex:indexPath.section];
    
    id obj = nil;
    if([sectionObj isNSArray]){
        obj = [self.paymentMethodList objectAtIndex:indexPath.row];
    } else {
        obj = sectionObj;
    }
    
    if([obj isKindOfClass:[MobileCard class]]){
        return UITableViewCellEditingStyleDelete;
    } else if([obj isKindOfClass:[NSString class]]){
        return UITableViewCellEditingStyleNone;
    } else {
        return UITableViewCellEditingStyleNone;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NSLocalizedString(@"Remove", nil);
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle == UITableViewCellEditingStyleDelete){
        if(CRED.isSignedIn){
            MobileCard *mobileCard = [self.paymentMethodList objectAtIndex:indexPath.row];
            [self requestRemoveCard:mobileCard];
        } else {
            MobileCard *mobileCard = [self.paymentMethodList objectAtIndex:indexPath.row];
            if([mobileCard delete]){
                [self.paymentMethodList removeObjectAtIndex:indexPath.row];
                [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                      withRowAnimation:UITableViewRowAnimationLeft];
            }
        }
    }
}

- (void)requestRemoveCard:(MobileCard *)card
{
    RequestResult result = RRFail;
    result = [PAY requestRemoveCard:card
                    completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              [REMOTE handleCardsResponse:(NSMutableArray *)response
                                         withNotification:NO];
                              
                              self.paymentMethodList = REMOTE.cards;
                              
                              
                              [self.tableView reloadData];
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while removing card", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id sectionObj = [self.sectionArray objectAtIndex:indexPath.section];
    
    id obj = nil;
    if([sectionObj isNSArray]){
        obj = [self.paymentMethodList objectAtIndex:indexPath.row];
    } else {
        obj = sectionObj;
    }
    
    if([obj isKindOfClass:[MobileCard class]]){
        [self tableView:tableView cardDidSelectRowAtIndexPath:indexPath];
    } else if([obj isKindOfClass:[NSString class]]){
        [self tableView:tableView stringDidSelectRowAtIndexPath:indexPath];
    } else {
        [self tableView:tableView creditDidSelectRowAtIndexPath:indexPath];
    }
}

- (void)tableView:(UITableView *)tableView cardDidSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MobileCard *mobileCard = [self.paymentMethodList objectAtIndex:indexPath.row];
    
    self.selectedCard = mobileCard;
    self.selectedIndexPath = indexPath;
    
    self.neoCardButton.buttonTitle = NSLocalizedString(@"Place Order", nil);
    
    [self.tableView reloadData];
}


- (void)tableView:(UITableView *)tableView stringDidSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    self.selectedCard = nil;
    
    if(ORDER.merchant.isTestMode){
        self.neoCardButton.buttonTitle = NSLocalizedString(@"Enter Test Card Information", nil);
    } else {
        self.neoCardButton.buttonTitle = NSLocalizedString(@"Place Order With New Card", nil);
    }
    
    [self.tableView reloadData];
}


- (void)tableView:(UITableView *)tableView creditDidSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)requestNewOrder:(MobileCard *)card
{
    RequestResult result = RRFail;
    result = [PAY requestNewOrder:ORDER.order
                  completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              [ORDER.order updateWithDictionary:response.data
                                                       merchant:ORDER.merchant
                                                        menuPan:MENUPAN.wholeSerialMenuList];
                              
                              [self configureLineItems];
                              
                              TRANS.dirty = YES;
                              
                              NSString *confirmTitle = NSLocalizedString(@"Confirm To Proceed", nil);
                              if(ORDER.merchant.isTestMode){
                                  confirmTitle = NSLocalizedString(@"[TEST MODE]\nConfirm To Proceed", nil);
                              }
                              
                              if(ORDER.order.tableNumber != nil){
                                  [UIAlertView askWithTitle:confirmTitle
                                                    message:[NSString stringWithFormat:NSLocalizedString(@"Table : %@\nAmount : %@\n%@", nil),
                                                             ORDER.order.tableNumber,
                                                             [[NSNumber numberWithCurrency:self.payment.netAmount] currencyString],
                                                             card.formattedCardNumber]
                                                   delegate:self
                                                        tag:AlertViewTagContinuePayment];
                              } else {
                                  [UIAlertView askWithTitle:confirmTitle
                                                    message:[NSString stringWithFormat:NSLocalizedString(@"Amount : %@\n%@", nil),
                                                             [[NSNumber numberWithCurrency:self.payment.netAmount] currencyString],
                                                             card.formattedCardNumber]
                                                   delegate:self
                                                        tag:AlertViewTagContinuePayment];
                              }
                              
                              
                              self.selectedCard = card;
                              
                              break;
                          }
                          case ResponseErrorUnavailableItems:{
                              [MENUPAN requestMenus:YES
                                            success:^(id response) {
                                                [ORDER.cart isAllItemsInHour:YES];
                                                [self.navigationController popToViewControllerClass:[OrderViewController class] animated:YES];
                                            } andFail:^(NSUInteger errorCode) {
                                                [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to renew Menus",nil)
                                                                    message:NSLocalizedString(@"Sorry, please try again and if it continues, please contact us.",nil)];
                                            }];
                              
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Review Cart Updates", nil)
                                                  message:NSLocalizedString(@"There are item updates in your cart that need your attention before you can check out. Please tap each highlighted item to confirm changes and updates.", nil)];
                              [SVProgressHUD dismiss];
                              
                              
                              break;
                          }
                          case ResponseErrorExists:
                              // TODO: check the money amount. if same go on, or the status is draft update or back and show order
                              [UIAlertView alert:NSLocalizedString(@"The order already exists", nil)];
                              
                              [self backButtonTouched:nil];
                              
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while creating new order", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)requestRenewOrder:(Order *)order
                 withCard:(MobileCard *)card
{
    RequestResult result = RRFail;
    result = [MENU requestRenewOrder:ORDER.order
                         deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                           authToken:CRED.customerInfo.authToken
                     completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              [ORDER.order updateWithDictionary:response.data
                                                       merchant:ORDER.merchant
                                                        menuPan:MENUPAN.wholeSerialMenuList];
                              TRANS.dirty = YES;
                              
                              // There must not be cart here. But for safty, remove cart
                              [ORDER.cart delete];
                              ORDER.cart = nil;
                              
                              self.shouldBackToRoot = YES;
                              
                              [self requestNewPayment:card];
                              
                              break;
                          }
                          case ResponseErrorUnavailableItems:{
                              [MENUPAN requestMenus:YES
                                            success:^(id response) {
                                                [ORDER.cart isAllItemsInHour:YES];
                                                [self.navigationController popToViewControllerClass:[OrderViewController class] animated:YES];
                                            } andFail:^(NSUInteger errorCode) {
                                                [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to renew Menus",nil)
                                                                    message:NSLocalizedString(@"Sorry, please try again and if it continues, please contact us.",nil)];
                                            }];
                              
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Review Cart Updates", nil)
                                                  message:NSLocalizedString(@"There are item updates in your cart that need your attention before you can check out. Please tap each highlighted item to confirm changes and updates.", nil)];
                              [SVProgressHUD dismiss];
                              
                              
                              break;
                          }
                          case ResponseErrorSaveFail:{
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to place the order", nil)
                                                  message:NSLocalizedString(@"Please try later or contact us", nil)];
                          }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while making ordering", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  if(response.errorCode != ResponseSuccess){
                      [SVProgressHUD dismiss];
                  }
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)takeoutViewController:(TakeoutViewController *)viewController
    didTouchPlaceOrderButton:(id)sender
{
    self.afterMinute = viewController.afterMinute;
    
    if(viewController.asapButton.selected){
        self.afterMinute = 0;
    }
    
    self.receiverName = viewController.orderNameField.text;
    self.phoneNumber = viewController.phoneNumberTextField.text;
    self.requestMessage = viewController.additionalRequest.text;
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [UIAlertView askWithTitle:NSLocalizedString(@"Do you want to proceed with payment below?", nil)
                                                   message:[NSString stringWithFormat:NSLocalizedString(@"Amount : %@\n%@", nil),
                                                            [[NSNumber numberWithCurrency:self.payment.netAmount] currencyString],
                                                            self.selectedCard.formattedCardNumber]
                                                  delegate:self
                                                       tag:(ORDER.order.orderNo > 0 && !ORDER.order.isEditable) ? AlertViewTagContinuePayment : AlertViewTagContinueOrderPayment];
                             }];
}

- (void)takeoutViewController:(TakeoutViewController *)viewController
         didTouchSignInButton:(id)sender
{
    self.afterMinute = viewController.afterMinute;
    
    if(viewController.asapButton.selected){
        self.afterMinute = 0;
    }
    
    self.receiverName = viewController.orderNameField.text;
    self.phoneNumber = viewController.phoneNumberTextField.text;
    self.requestMessage = viewController.additionalRequest.text;
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [self showSignIn:NO withDelegate:self];
                             }];
}

- (void)dineinViewController:(DineinViewController *)viewController
    didTouchPlaceOrderButton:(id)sender
{
    self.requestMessage = viewController.additionalRequest.text;
    self.receiverName = viewController.orderNameField.text;
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [UIAlertView askWithTitle:NSLocalizedString(@"Do you want to proceed with payment below?", nil)
                                                   message:[NSString stringWithFormat:NSLocalizedString(@"Amount : %@\n%@", nil),
                                                            [[NSNumber numberWithCurrency:self.payment.netAmount] currencyString],
                                                            self.selectedCard.formattedCardNumber]
                                                  delegate:self
                                                       tag:(ORDER.order.orderNo > 0 && !ORDER.order.isEditable) ? AlertViewTagContinuePayment : AlertViewTagContinueOrderPayment];
                             }];
}
- (void)dineinViewController:(DineinViewController *)viewController
         didTouchSignInButton:(id)sender
{
    self.requestMessage = viewController.additionalRequest.text;
    self.receiverName = viewController.orderNameField.text;
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [self showSignIn:NO withDelegate:self];
                             }];
}



- (void)deliveryViewController:(DeliveryViewController *)viewController
      didTouchPlaceOrderButton:(id)sender
             addressDictionary:(NSDictionary *)dictionary
{
    self.afterMinute = viewController.afterMinute;
    
    if(viewController.asapButton.selected){
        self.afterMinute = 0;
    }
    
    self.requestMessage = viewController.additionalRequest.text;
    
    self.address = [[Addresses alloc] init];
                    
    self.address.state = viewController.stateTextField.text;
    self.address.city = viewController.cityTextField.text;
    self.address.address1 = viewController.address1TextField.text;
    self.address.address2 = viewController.address2TextField.text;
    self.address.zip = viewController.zipCodeTextField.text;
    self.address.phoneNumber = viewController.phoneNumberTextField.text;
    self.address.receiverName = viewController.nameTextField.text;
    self.address.coordinate = viewController.geocodedCoords;
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [self handleOrder];
                             }];
}

- (void)handleOrder
{
    if(ORDER.order.orderNo > 0 && !ORDER.order.isEditable){ //Order exists
        [self requestNewPayment:self.selectedCard];
    } else {
        [UIAlertView askWithTitle:NSLocalizedString(@"Do you want to proceed with payment below?", nil)
                          message:[NSString stringWithFormat:NSLocalizedString(@"Amount : %@\n%@", nil),
                                   [[NSNumber numberWithCurrency:self.payment.netAmount] currencyString],
                                   self.selectedCard.formattedCardNumber]
                         delegate:self
                              tag:AlertViewTagContinueOrderPayment];
        
    }
}

#if USE_ORANGE_CELL
- (void)tableView:(UITableView *)tableView toggleButtonTouched:(NPToggleButton *)sender forRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrangeCell *cell = (OrangeCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if(sender.selected){
        // Use self.payment.payAmountWithoutCents when wanting to remove cent units.
        NSInteger maxUsableCredit = MIN(self.payment.payAmount, CRED.customerInfo.credit);
        self.useCreditNumber = [NSNumber numberWithFloat:maxUsableCredit];
    } else {
        self.useCreditNumber = [NSNumber numberWithFloat:0];
    }
    
    self.payment.creditAmount = [self.useCreditNumber longLongValue];

    cell.useCreditLabel.text = self.payment.creditAmountString;
    cell.useCreditAmountLabel.text = [NSString stringWithFormat:@"(%@)",self.payment.creditConvertedAmountString];
    
    self.paymentSummaryView.pointAmountLabel.text = self.payment.creditConvertedAmountString;
    self.paymentSummaryView.cardAmountLabel.text = self.payment.netAmountString;
    self.paymentSummaryView.pointConvertedLabel.text = self.payment.creditConvertedAmountStringInParenthesis;
    
    MobileCardCell *cardCell = (MobileCardCell *)[tableView cellForRowAtIndexPath:self.selectedIndexPath];
    
    if([cardCell isKindOfClass:[MobileCardCell class]]){
        cardCell.amountLabel.text = [NSString stringWithFormat:@"(%@)",self.payment.netAmountString];
    } else {
        NSIndexPath *testIndexPath = [NSIndexPath indexPathForRow:self.selectedIndexPath.row
                                                        inSection:self.selectedIndexPath.row + 1];
        
        cardCell = (MobileCardCell *)[tableView cellForRowAtIndexPath:testIndexPath];
        
        if([cardCell isKindOfClass:[MobileCardCell class]]){
            
            self.selectedIndexPath = testIndexPath;
            cardCell.amountLabel.text = [NSString stringWithFormat:@"(%@)",self.payment.netAmountString];
        } else {
            
        }
    }
}
#endif

- (void)paymentSummaryViewOrangeSwitchChanged:(UISwitch *)sender
{
    MobileCardCell *cardCell = (MobileCardCell *)[self.tableView cellForRowAtIndexPath:self.selectedIndexPath];
    
    if([cardCell isKindOfClass:[MobileCardCell class]]){
        cardCell.amountLabel.text = [NSString stringWithFormat:@"(%@)",self.payment.netAmountString];
    } else {
        NSIndexPath *testIndexPath = [NSIndexPath indexPathForRow:self.selectedIndexPath.row
                                                        inSection:self.selectedIndexPath.row + 1];
        
        cardCell = (MobileCardCell *)[self.tableView cellForRowAtIndexPath:testIndexPath];
        
        if([cardCell isKindOfClass:[MobileCardCell class]]){
            self.selectedIndexPath = testIndexPath;
            cardCell.amountLabel.text = [NSString stringWithFormat:@"(%@)",self.payment.netAmountString];
        } else {
            
        }
    }
}

- (void)requestNewPayment:(MobileCard *)card
{
    if(!ORDER.merchant.isOpenHour){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Sorry, restaurant is currently closed!", nil)
                            message:NSLocalizedString(@"Please feel free to browse the menu, but ordering isn't available until the restaurant opens.", nil)
                  cancleButtonTitle:NSLocalizedString(@"Got it!", nil)];
        [SVProgressHUD dismiss];
        return;
    }
    
    if(ORDER.order.orderType == OrderTypeDelivery){
        if(!ORDER.merchant.isDeliveryHour){
            NSString *hourString = [ORDER.merchant nextDeliveryAvailableTime];
            [UIAlertView alertWithTitle:NSLocalizedString(@"Delivery is Currently Unavailable", nil)
                                message:[NSString stringWithFormat:NSLocalizedString(@"Next Available Time:\n%@", nil), [hourString length] > 0 ? hourString : NSLocalizedString(@"N/A", nil)]];
            
            [SVProgressHUD dismiss];
            return;
        }
    }
    
    if(![ORDER.order isAllItemsInHour]){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Review Cart Updates", nil)
                                     message:NSLocalizedString(@"There are item updates in your cart that need your attention before you can check out. Please tap each highlighted item to confirm changes and updates.", nil)];
        [SVProgressHUD dismiss];
        return;
    }
    
    [self.payment updateCardInfo:card];
    self.payment.orderNo = ORDER.order.orderNo;
    
    if(ORDER.merchant.isAllowTip){
        float tipRate = MIN(20.0f, roundf(((float)self.payment.tipAmount / (float)self.payment.subTotal) * 100));
        switch(ORDER.order.orderType){
            case OrderTypeDelivery:
                [NSUserDefaults standardUserDefaults].tipDeliveryRate = tipRate;
                break;
            case OrderTypeTakeout:
                [NSUserDefaults standardUserDefaults].tipTakeoutRate = tipRate;
                break;
            case OrderTypeBill:
            case OrderTypeCart:
            case OrderTypePickup:
            default:
                [NSUserDefaults standardUserDefaults].tipRate = tipRate;
                break;
        }
    }
    
    RequestResult result = RRFail;
    
#if FLURRY_ENABLED
    [Flurry logEvent:@"Send Payment(O)" withParameters:@{@"RestaurantName":ORDER.merchant.name,
                                                      @"RestaurantId":[NSNumber numberWithSerial:ORDER.merchant.merchantNo],
                                                      @"orderType":[Order typeStringForOrderType:ORDER.order.orderType],
                                                      @"orderTotal":[NSNumber numberWithCurrency:ORDER.order.subTotal]}];
#endif
    
#if FBAPPEVENT_ENABLED
    [FBSDKAppEvents logEvent:FBSDKAppEventNameInitiatedCheckout
               valueToSum:(self.payment.payAmount / 100.0f)
               parameters:@{ FBSDKAppEventParameterNameCurrency    : @"USD",
                             FBSDKAppEventParameterNameContentType : @"food",
                             @"UUID" : (CRED.udid ? CRED.udid : @"Unknown")}];
#endif
    
    result = [PAY requestNewPayment:ORDER.order.orderNo
                            payment:self.payment
                   lineItemJsonList:self.lineItemJsonList
                        deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                   appliedDiscounts:[self.payment appliedDiscountsDictsWithSmallCutAmount:self.paymentSummaryView.smallCutPromotion.appliedDiscountAmount]
                     smallCutAmount:self.paymentSummaryView.smallCutPromotion.appliedDiscountAmount
                 paymentMethodNonce:nil
                    completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              
                              CRED.dirtyCredit = YES;
                              
                              if(CRED.isUnattendedSignUp){
                                  [SWITCH removeLocalData];
                              }
                              
#if FBAPPEVENT_ENABLED
                              [FBSDKAppEvents logPurchase:(self.payment.payAmount / 100.0f)
                                              currency:@"USD"
                                            parameters:@{ FBSDKAppEventParameterNameCurrency    : @"USD",
                                                          FBSDKAppEventParameterNameContentType : @"food",
                                                          @"UUID" : (CRED.udid ? CRED.udid : @"Unknown")}];
#endif
                            
                              
#if FIREBASE_ENABLED
                              id tracker = [[GAI sharedInstance] defaultTracker];
                              
                              
                              
                              [tracker send:[[GAIDictionaryBuilder createTransactionWithId:(CRED.udid ? CRED.udid : @"Unknown")             // (NSString) Transaction ID
                                                                               affiliation:@"Food"         // (NSString) Affiliation
                                                                                   revenue:@(self.payment.payAmount / 100.0f)                  // (NSNumber) Order revenue (including tax and shipping)
                                                                                       tax:@(ORDER.cart.taxAmount)                  // (NSNumber) Tax
                                                                                  shipping:@(self.payment.deliveryFeeAmount)                  // (NSNumber) Shipping
                                                                              currencyCode:@"USD"] build]];        // (NSString) Currency code
                              
                              
                              //    [tracker send:[[GAIDictionaryBuilder createItemWithTransactionId:@"0_123456"         // (NSString) Transaction ID
                              //                                                                name:@"Space Expansion"  // (NSString) Product Name
                              //                                                                 sku:@"L_789"            // (NSString) Product SKU
                              //                                                            category:@"Game expansions"  // (NSString) Product category
                              //                                                               price:@1.9F               // (NSNumber) Product price
                              //                                                            quantity:@1                  // (NSInteger) Product quantity
                              //                                                        currencyCode:@"USD"] build]];    // (NSString) Currency code
#endif
                              
                              
#if FIREBASE_ENABLED
                              [FIRAnalytics logEventWithName:kFIREventEcommercePurchase
                                                  parameters:@{
                                                               kFIRParameterCurrency:@"USD",
                                                               kFIRParameterValue:@(self.payment.payAmount / 100.0f)
                                                               }];
#endif
                              if(ORDER.order.isTicketBase){
                                  ORDER.order.status = OrderStatusPaid;
                              }
                              
                              if(CRED.isSignedIn){
                                  REMOTE.ordersDirty = YES;
                              } else {
                                  [ORDER.merchant save];
                                  [ORDER.order save];
                              }
                              
                              if(ORDER.order.orderType == OrderTypeDelivery){
                                  Addresses *address = [[Addresses alloc] initWithOrder:ORDER.order];
                                  address.coordinate = self.address.coordinate;
                                  [address save];
                              }
                              
                              TRANS.dirty = YES;
                              
                              self.payment.order = ORDER.order;
                              
                              Receipt *receipt = [[Receipt alloc] initWithDictionary:response.data];
                              NSDictionary *paymentDict = [response.data objectForKey:@"payment"];
                              self.payment = receipt.payment;
                              if(!CRED.isSignedIn){
                                  [receipt save];
                                  [self.payment save];
                              }
                              
                              if([paymentDict objectForKey:@"card_id"] != nil){
                                  [card updateWithPaymentDictionary:paymentDict];
                              }
                              
                              [self moveOrdersTap];
                          }
                              break;
                          case ResponseErrorOutOfMenuHours:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              NSString *title = [response objectForKey:@"title"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alertWithTitle:title
                                                      message:message];
                              }
                              break;
                          }
                          case ResponseErrorItemsAlreadyPaid:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Some Items are Already Paid", nil)
                                                  message:NSLocalizedString(@"Some items you try to pay have been paid by others. Please select items you want to pay again", nil)];
                              break;
                          case ResponseErrorCardError:
                          case ResponseErrorPaymentError:{
                              NSString *param = [[response objectForKey:@"message"] objectForKey:@"param"];
                              if([param isEqualToString:@"amount"]){
                                  [UIAlertView alertWithTitle:NSLocalizedString(@"Minimum Order Value", nil)
                                                      message:NSLocalizedString(@"Minimum payment amount must be at least 50c", nil)
                                                     delegate:self
                                                          tag:AlertViewTagStripeError];
                              } else {
//                                  NSString *message = [[response objectForKey:@"message"] objectForKey:@"message"];
//                                  [UIAlertView alertWithTitle:UNDEFINED_ALERT_TITLE
//                                                      message:message
//                                                     delegate:self
//                                                          tag:AlertViewTagStripeError];
                                  
                                  [UIAlertView alertWithTitle:NSLocalizedString(@"Your card information is not correct.", nil)
                                                      message:nil
                                                     delegate:self
                                                          tag:AlertViewTagStripeError];
                              }
                          }
                              break;
                          case ResponseErrorFingerprintError:{
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Your Card is Not Valid", nil)
                                                  message:NSLocalizedString(@"Check your card information.", nil)
                                                 delegate:self
                                                      tag:AlertViewTagStripeError];
                          }
                              break;
                          case ResponseErrorInvalidOrder:{
                              [UIAlertView alertWithTitle:NSLocalizedString(@"This Order is Not Valid", nil)
                                                  message:NSLocalizedString(@"This Order may be removed or completed with some reasons. Please ask the restaurant what to do.", nil)
                                                 delegate:self
                                                      tag:AlertViewTagInvalidOrderError];
                              
                              TRANS.dirty = YES;
                              
                              break;
                          }
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while making payment", nil)];
                              }
                          }
                              break;
                      }
                      [self.tableView reloadData];
                      
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)requestPickupOrder:(MobileCard *)card
           requestMesssage:(NSString *)requestMessage
              receiverName:(NSString *)receiverName
{
    if([requestMessage length] > 0){
        [CustomRequest updateCustomRequest:requestMessage
                                      type:CustomRequestTypeDineIn
                                merchantNo:ORDER.merchant.merchantNo];
    }
    
    RequestResult result = RRFail;
    result = [MENU requestPickupOrder:ORDER.cart.lineItems
                           merchantNo:ORDER.merchant.merchantNo
                           flagNumber:ORDER.cart.flagNumber
                         receiverName:ORDER.cart.cartType == CartTypePickupServiced ? nil : receiverName
                      customerRequest:requestMessage
                          deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                            authToken:CRED.customerInfo.authToken
                       subTotalAmount:ORDER.cart.amount
                subTotalAmountTaxable:ORDER.cart.subtotalAmountTaxable
                            taxAmount:ORDER.cart.taxAmount
                      completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              [ORDER.order updateWithDictionary:response.data
                                                       merchant:ORDER.merchant
                                                        menuPan:MENUPAN.wholeSerialMenuList];
                              
                              [self configureLineItems];
                              TRANS.dirty = YES;
                              
                              [ORDER.cart delete];
                              ORDER.cart = nil;
                              self.shouldBackToRoot = YES;
                              
                              [self requestNewPayment:card];
                              
                              break;
                          }
                          case ResponseErrorUnavailableItems:{
                              [MENUPAN requestMenus:YES
                                            success:^(id response) {
                                                [ORDER.cart isAllItemsInHour:YES];
                                                [self.navigationController popToViewControllerClass:[OrderViewController class] animated:YES];
                                            } andFail:^(NSUInteger errorCode) {
                                                [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to renew Menus",nil)
                                                                    message:NSLocalizedString(@"Sorry, please try again and if it continues, please contact us.",nil)];
                                            }];
                              
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Review Cart Updates", nil)
                                                  message:NSLocalizedString(@"There are item updates in your cart that need your attention before you can check out. Please tap each highlighted item to confirm changes and updates.", nil)];
                              [SVProgressHUD dismiss];
                              
                              
                              break;
                          }
                          case ResponseErrorNotSupportOrderType:{
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Does Not Support Dine-in Order", nil)
                                                  message:NSLocalizedString(@"This restaurant does not take dine-in order no more. Try to order later or order at other restaurant.", nil)];
                          }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while making ordering", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  if(response.errorCode != ResponseSuccess){
                      [SVProgressHUD dismiss];
                  }
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)requestTakeoutOrder:(MobileCard *)card
                 afterTime:(NSInteger)afterTime
           requestMesssage:(NSString *)requestMessage
              receiverName:(NSString *)receiverName
              receiverPhone:(NSString *)receiverPhone
{
    if([requestMessage length] > 0){
        [CustomRequest updateCustomRequest:requestMessage
                                      type:CustomRequestTypeTakeout
                                merchantNo:ORDER.merchant.merchantNo];
    }
    
    RequestResult result = RRFail;
    result = [MENU requestTakeoutOrder:ORDER.cart.lineItems
                            merchantNo:ORDER.merchant.merchantNo
                           pickupAfter:afterTime
                       customerRequest:requestMessage
                          receiverName:receiverName
                         receiverPhone:receiverPhone
                           deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                             authToken:CRED.customerInfo.authToken
                        subTotalAmount:ORDER.cart.amount
                 subTotalAmountTaxable:ORDER.cart.subtotalAmountTaxable
                             taxAmount:ORDER.cart.taxAmount
                       completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              [ORDER.order updateWithDictionary:response.data
                                                       merchant:ORDER.merchant
                                                        menuPan:MENUPAN.wholeSerialMenuList];
                              
                              [self configureLineItems];
                              TRANS.dirty = YES;
                              
                              [ORDER.cart delete];
                              ORDER.cart = nil;
                              self.shouldBackToRoot = YES;

                              [self requestNewPayment:card];
                              
                              break;
                          }
                          case ResponseErrorUnavailableItems:{
                              [MENUPAN requestMenus:YES
                                            success:^(id response) {
                                                [ORDER.cart isAllItemsInHour:YES];
                                                [self.navigationController popToViewControllerClass:[OrderViewController class] animated:YES];
                                            } andFail:^(NSUInteger errorCode) {
                                                [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to renew Menus",nil)
                                                                    message:NSLocalizedString(@"Sorry, please try again and if it continues, please contact us.",nil)];
                                            }];
                              
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Review Cart Updates", nil)
                                                  message:NSLocalizedString(@"There are item updates in your cart that need your attention before you can check out. Please tap each highlighted item to confirm changes and updates.", nil)];
                              [SVProgressHUD dismiss];
                              
                              
                              break;
                          }
                          case ResponseErrorNotSupportOrderType:{
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Does Not Support Take-out Order", nil)
                                                  message:NSLocalizedString(@"This restaurant does not take take-out order no more. Try to order later or order at other restaurants.", nil)];
                          }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while making ordering", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  if(response.errorCode != ResponseSuccess){
                      [SVProgressHUD dismiss];
                  }
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)requestDeliveryOrder:(MobileCard *)card
                   afterTime:(NSInteger)afterTime
             requestMesssage:(NSString *)requestMessage
                addressState:(NSString *)state
                 addressCity:(NSString *)city
              addressStreet1:(NSString *)street1
              addressStreet2:(NSString *)street2
                  addressZip:(NSString *)zip
                 phoneNumber:(NSString *)phoneNumber
                receiverName:(NSString *)receiverName
           deliveryFeeAmount:(PCCurrency)deliveryFeeAmount
          roServiceFeeAmount:(PCCurrency)roServiceFeeAmount
{
    if([requestMessage length] > 0){
        [CustomRequest updateCustomRequest:requestMessage
                                      type:CustomRequestTypeDelivery
                                merchantNo:ORDER.merchant.merchantNo];
    }
    
    RequestResult result = RRFail;
    result = [MENU requestDeliveryOrder:ORDER.cart.lineItems
                             merchantNo:ORDER.merchant.merchantNo
                            pickupAfter:afterTime
                        customerRequest:requestMessage
                            deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                              authToken:CRED.customerInfo.authToken
                         subTotalAmount:ORDER.cart.amount
                  subTotalAmountTaxable:ORDER.cart.subtotalAmountTaxable
                              taxAmount:ORDER.cart.taxAmount
                           addressState:state
                            addressCity:city
                         addressStreet1:street1
                         addressStreet2:street2
                             addressZip:zip
                            phoneNumber:phoneNumber
                           receiverName:receiverName
                      deliveryFeeAmount:deliveryFeeAmount
                     roServiceFeeAmount:roServiceFeeAmount
                        completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              [ORDER.order updateWithDictionary:response.data
                                                       merchant:ORDER.merchant
                                                        menuPan:MENUPAN.wholeSerialMenuList];
                              [self configureLineItems];
                              TRANS.dirty = YES;
                              
                              [ORDER.cart delete];
                              ORDER.cart = nil;
                              self.shouldBackToRoot = YES;
                             
                              [self requestNewPayment:card];
                              
                              break;
                          }
                          case ResponseErrorUnavailableItems:{
                              [MENUPAN requestMenus:YES
                                            success:^(id response) {
                                                [ORDER.cart isAllItemsInHour:YES];
                                                [self.navigationController popToViewControllerClass:[OrderViewController class] animated:YES];
                                            } andFail:^(NSUInteger errorCode) {
                                                [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to renew Menus",nil)
                                                                    message:NSLocalizedString(@"Sorry, please try again and if it continues, please contact us.",nil)];
                                            }];
                              
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Review Cart Updates", nil)
                                                  message:NSLocalizedString(@"There are item updates in your cart that need your attention before you can check out. Please tap each highlighted item to confirm changes and updates.", nil)];
                              [SVProgressHUD dismiss];
                              
                              
                              break;
                          }
                          case ResponseErrorNotSupportOrderType:{
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Does not Support Delivery Order", nil)
                                                  message:NSLocalizedString(@"This restaurant does not take delivery order no more. Try to order later or order at other restaurants.", nil)];
                          }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while making ordering", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  if(response.errorCode != ResponseSuccess){
                      [SVProgressHUD dismiss];
                  }
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)moveOrdersTap
{
    if(self.tabBarController.selectedIndex == 0 || self.tabBarController.selectedIndex == 1 || self.tabBarController.selectedIndex == 2){
        ORDER.shouldMoveToOrders = YES;
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (IBAction)payNowButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    if(self.selectedCard == nil){
        [self newCardButtonTouched:nil];
    } else {
        
        if(ORDER.merchant.isAllowTip
           && [self.paymentSummaryView.tipTextField.text length] == 0
           && !self.paymentSummaryView.tipTextField.isHidden){
            
            // !!!:Please do not remove [self.paymentSummaryView.tipTextField becomeFirstResponder]; code
            // This code is workaround for UIAlertView and responder chain bug
            // If we remove this line of code, the becomeFirstResponder will not work properly
            // when using custom input view.
            // I think it's because the UIAlertView memorize the status of input trait and get back it
            // when it's being dismissed.
            [self.paymentSummaryView.tipTextField becomeFirstResponder];
            
//            if(self.paymentSummaryView.isAutoTipSetOnce){
//                [UIAlertView alertWithTitle:NSLocalizedString(@"Please Enter Tip", nil)
//                                    message:NSLocalizedString(@"How much would you like to tip? Please enter it in the blank space next to \"Tip\"", nil)
//                                   delegate:self
//                                        tag:AlertViewTagTipInput];
//            } else {
//                [UIAlertView alertWithTitle:NSLocalizedString(@"Please Enter Tip", nil)
//                                    message:NSLocalizedString(@"How much would you like to tip? Please enter it in the blank space next to \"Tip\"", nil)
//                                   delegate:self
//                                        tag:AlertViewTagTipInput];
//            }
            return;
        }
        
        if(ORDER.order.orderNo > 0 && !ORDER.order.isEditable){ //Order exists
            
            if(ORDER.merchant.isDemoMode || ORDER.merchant.isTestMode){
                
                NSString *restaurantType = @"This is DEMO restaurant.";
                
                if(ORDER.merchant.isTestMode){
                    restaurantType = @"This restaurant is in TEST mode.";
                }
                
                if(ORDER.order.tableNumber != nil){
                    [UIAlertView askWithTitle:[NSString stringWithFormat:NSLocalizedString(@"%@ You won't be charged for payments made in this store.", nil), restaurantType]
                                      message:[NSString stringWithFormat:NSLocalizedString(@"Do you want to proceed?\nTable : %@\nAmount : %@\n%@", nil),
                                               ORDER.order.tableNumber,
                                               [[NSNumber numberWithCurrency:self.payment.netAmount] currencyString],
                                               self.selectedCard.formattedCardNumber]
                                     delegate:self
                                          tag:AlertViewTagContinuePayment];
                    
                } else {
                    [UIAlertView askWithTitle:[NSString stringWithFormat:NSLocalizedString(@"%@ You won't be charged for payments made in this store.", nil), restaurantType]
                                      message:[NSString stringWithFormat:NSLocalizedString(@"Do you want to proceed?\nAmount : %@\n%@", nil),
                                               [[NSNumber numberWithCurrency:self.payment.netAmount] currencyString],
                                               self.selectedCard.formattedCardNumber]
                                     delegate:self
                                          tag:AlertViewTagContinuePayment];
                }
            } else {
                if(ORDER.order.tableNumber != nil){
                    [UIAlertView askWithTitle:NSLocalizedString(@"Do you want to proceed with payment below?", nil)
                                      message:[NSString stringWithFormat:NSLocalizedString(@"Table : %@\nAmount : %@\n%@", nil),
                                               ORDER.order.tableNumber,
                                               [[NSNumber numberWithCurrency:self.payment.netAmount] currencyString],
                                               self.selectedCard.formattedCardNumber]
                                     delegate:self
                                          tag:AlertViewTagContinuePayment];
                } else {
                    [UIAlertView askWithTitle:NSLocalizedString(@"Do you want to proceed with payment below?", nil)
                                      message:[NSString stringWithFormat:NSLocalizedString(@"Amount : %@\n%@", nil),
                                               [[NSNumber numberWithCurrency:self.payment.netAmount] currencyString],
                                               self.selectedCard.formattedCardNumber]
                                     delegate:self
                                          tag:AlertViewTagContinuePayment];
                }
            }
        } else {
            if((ORDER.order.isEditable && ORDER.order.orderType == OrderTypeTakeout) ||
               (!ORDER.order.isEditable && ORDER.cart.cartType == CartTypeTakeout)){
                
                [self handleOrder];
            } else if((ORDER.order.isEditable && ORDER.order.orderType == OrderTypeDelivery) ||
                      (!ORDER.order.isEditable && ORDER.cart.cartType == CartTypeDelivery)){
                
                [self handleOrder];
                
            } else if((ORDER.order.isEditable && ORDER.order.orderType == OrderTypePickup) ||
                      (!ORDER.order.isEditable && (ORDER.cart.cartType == CartTypePickup || ORDER.cart.cartType == CartTypePickupServiced))){
                
                [self handleOrder];
                
            } else if(!ORDER.order.isEditable && ORDER.cart.cartType == CartTypeCart){
                [self requestNewOrder:self.selectedCard];
            } else {
                [UIAlertView alertWithTitle:NSLocalizedString(@"Broken Order", nil)
                                    message:NSLocalizedString(@"There might be an unordinary function. Pleaes try to order again", nil)];
                PCError(@"(SelectCard)CartType is not Defined %d", ORDER.cart.cartType);
            }
        }
    }
}

- (IBAction)newCardButtonTouched:(id)sender
{
    NewCardViewController *viewController = [NewCardViewController viewControllerFromNib];
    viewController.initialCreditSelection = self.paymentSummaryView.orangeUseSwitch.on;
    viewController.payment = self.payment;
    viewController.address = self.address;
    
    viewController.afterMinute = self.afterMinute;
    viewController.requestMessage = self.requestMessage;
    viewController.phoneNumber = self.phoneNumber;

//    viewController.promotionValidStatus = self.paymentSummaryView.promotionValidStatus;
    
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case AlertViewTagTipInput:
            [self.paymentSummaryView.tipTextField becomeFirstResponder];
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case AlertViewTagStripeError:
            break;
        case AlertViewTagInvalidOrderError:
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        case AlertViewTagContinuePayment:
            if(buttonIndex == AlertButtonIndexYES){
                [self requestNewPayment:self.selectedCard];
            }
            break;
        case AlertViewTagContinueOrderPayment:
            if(buttonIndex == AlertButtonIndexYES){
                if(ORDER.order.isEditable){
                    
                    ORDER.order.deliveryFee = self.payment.deliveryFeeAmount;
                    ORDER.order.roServiceFee = self.payment.roServiceFeeAmount;
                    ORDER.order.pickupAfter = self.afterMinute;
                    ORDER.order.customerRequest = self.requestMessage;
                    ORDER.order.state = self.address.state;
                    ORDER.order.city = self.address.city;
                    ORDER.order.address1 = self.address.address1;
                    ORDER.order.address2 = self.address.address2;
                    ORDER.order.zip = self.address.zip;
                    ORDER.order.phoneNumber = self.address.phoneNumber;
                    
                    if(ORDER.cart.cartType == CartTypePickupServiced){
                        ORDER.order.tableNumber = ORDER.cart.flagNumber;
                        ORDER.order.receiverName = nil;
                    } else {
                        ORDER.order.receiverName = self.address.receiverName;
                        ORDER.order.tableNumber = nil;
                    }
                    
                    [self requestRenewOrder:ORDER.order
                                   withCard:self.selectedCard];
                } else {
                    
                    if(!ORDER.merchant.isOpenHour){
                        [UIAlertView alertWithTitle:NSLocalizedString(@"Sorry, restaurant is currently closed!", nil)
                                            message:NSLocalizedString(@"Please feel free to browse the menu, but ordering isn't available until the restaurant opens.", nil)
                                  cancleButtonTitle:NSLocalizedString(@"Got it!", nil)];
                        return;
                    }
                    
                    if(ORDER.order.orderType == OrderTypeDelivery){
                        if(!ORDER.merchant.isDeliveryHour){
                            NSString *hourString = [ORDER.merchant nextDeliveryAvailableTime];
                            [UIAlertView alertWithTitle:NSLocalizedString(@"Delivery is Currently Unavailable", nil)
                                                message:[NSString stringWithFormat:NSLocalizedString(@"Next Available Time:\n%@", nil), [hourString length] > 0 ? hourString : NSLocalizedString(@"N/A", nil)]];
                            
                            return;
                        }
                    }
                    
                    if(![ORDER.cart isAllItemsInHour]){
                        [UIAlertView alertWithTitle:NSLocalizedString(@"Review Cart Updates", nil)
                                            message:NSLocalizedString(@"There are item updates in your cart that need your attention before you can check out. Please tap each highlighted item to confirm changes and updates.", nil)];
                        [SVProgressHUD dismiss];
                        
                        [self.navigationController popToViewControllerClass:[OrderViewController class] animated:YES];
                        return;
                    }
                    
                    switch(ORDER.cart.cartType){
                        case CartTypeTakeout:
                            [self requestTakeoutOrder:self.selectedCard
                                            afterTime:self.afterMinute
                                      requestMesssage:self.requestMessage
                                         receiverName:self.address.receiverName
                                        receiverPhone:self.address.phoneNumber];
                            break;
                        case CartTypeDelivery:
                            [self requestDeliveryOrder:self.selectedCard
                                             afterTime:self.afterMinute
                                       requestMesssage:self.requestMessage
                                          addressState:self.address.state
                                           addressCity:self.address.city
                                        addressStreet1:self.address.address1
                                        addressStreet2:self.address.address2
                                            addressZip:self.address.zip
                                           phoneNumber:self.address.phoneNumber
                                          receiverName:self.address.receiverName
                                     deliveryFeeAmount:self.payment.deliveryFeeAmount
                                    roServiceFeeAmount:self.payment.roServiceFeeAmount];
                            break;
                        case CartTypePickup:
                        case CartTypePickupServiced:
                            [self requestPickupOrder:self.selectedCard
                                     requestMesssage:self.requestMessage
                                        receiverName:self.address.receiverName];
                            break;
                        case CartTypeCart: //Should not be this case
                        default:
                            [UIAlertView alertWithTitle:NSLocalizedString(@"Broken Order", nil)
                                                message:NSLocalizedString(@"There might be an unordinary function. Pleaes try to order again", nil)];
                            PCError(@"(SelectCard)CartType is not Defined %d", ORDER.cart.cartType);
                            break;
                    }
                }
            }
            break;
        case AlertViewTagPromoOrderAmount:
            if(buttonIndex == AlertButtonIndexYES){
                [self backButtonTouched:nil];
            }
            break;
    }
    
    [self.tableView reloadData];
}

- (void)requestMyCredit
{
    if(CRED.customerInfo != nil){
        RequestResult result = RRFail;
        result = [CRED requestMyCreditWithCompletionBlock:
                  ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                      if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                          switch(response.errorCode){
                              case ResponseSuccess:
                              {
                                  CRED.customerInfo.credit = [response integerForKey:@"credit_balance"];
#if USE_ORANGE_CELL
                                  [self drawCredits];
#endif
                                  [self.paymentSummaryView drawPayment];
                                  self.tableView.tableHeaderView = self.paymentSummaryView;
                                  if(CRED.customerInfo.credit > 0){
                                      [self selectUseOrangesWithDefaultValue:self.initialCreditSelection];
                                  }
                                  
                              }
                                  break;
                              case ResponseErrorGeneral:{
                                  NSString *message = [response objectForKey:@"message"];
                                  if([message isKindOfClass:[NSString class]]){
                                      [UIAlertView alert:message];
                                  }
                                  break;
                              }
                              default:{
                                  NSString *message = [response objectForKey:@"message"];
                                  if([message isKindOfClass:[NSString class]]){
                                      [UIAlertView alert:message];
                                  } else {
                                      [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting my points", nil)];
                                  }
                              }
                                  break;
                          }
                      } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                          [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                          //Signed out in requestMyCreditWithCompletionBlock
                      } else {
                          if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                              [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                          } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                              [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                                  ,statusCode]];
                          }
                      }
                      [SVProgressHUD dismiss];
                  }];
        switch(result){
            case RRSuccess:
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                break;
            case RRParameterError:
                [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                break;
            default:
                break;
        }
    }
}

- (void)drawCredits
{
    if([self.sectionArray count] > 0){
        id obj = [self.sectionArray objectAtIndex:0];
        if([obj isKindOfClass:[NSNumber class]]){
            if(CRED.customerInfo.credit > 0){
                [self.sectionArray replaceObjectAtIndex:0 withObject:[NSNumber numberWithInteger:CRED.customerInfo.credit]];
            } else {
                [self.sectionArray removeObjectAtIndex:0];
            }
        } else {
            if(CRED.customerInfo.credit > 0){
                [self.sectionArray insertObject:[NSNumber numberWithInteger:CRED.customerInfo.credit] atIndex:0];
            }
        }
    } else {
        if(CRED.customerInfo.credit > 0){
            [self.sectionArray addObject:[NSNumber numberWithInteger:CRED.customerInfo.credit]];
        }
    }
    
    [self.paymentSummaryView drawPayment];
    self.tableView.tableHeaderView = self.paymentSummaryView;
    [self.tableView reloadData];
}

- (void)setPaymentMethodList:(NSMutableArray *)methodList
{
    self.sectionArray = [NSMutableArray array];
    
#if USE_ORANGE_CELL
    if(CRED.customerInfo.credit > 0){
        [self.sectionArray addObject:[NSNumber numberWithInteger:CRED.customerInfo.credit]];
    }
#endif
    
    _paymentMethodList = [NSMutableArray arrayWithArray:methodList];
    NSInteger newSelectedIndexPathRow = 0;
    
    if([_paymentMethodList count] > 0){
        
        if(self.selectedCard == nil){
            self.selectedCard = [_paymentMethodList objectAtIndex:0];
        } else {
            BOOL found = NO;
            for(id obj in _paymentMethodList){
                if([obj isKindOfClass:[MobileCard class]]){
                    MobileCard *mobileCard = (MobileCard *)obj;
                    if([mobileCard.cardFingerPrint isEqual:self.selectedCard.cardFingerPrint]){
                        self.selectedCard = mobileCard;
                        found = YES;
                        break;
                    }
                    newSelectedIndexPathRow++;
                }
            }
            if(!found){
                newSelectedIndexPathRow = 0;
                self.selectedCard = [_paymentMethodList objectAtIndex:0];
            }
        }
        
        self.neoCardButton.buttonTitle = NSLocalizedString(@"Place Order", nil);
    } else {
        self.neoCardButton.buttonTitle = NSLocalizedString(@"Enter Card Information", nil);
    }

    if(ORDER.merchant.isTestMode){
        [_paymentMethodList addObject:NSLocalizedString(@"Test Card", nil)];
    } else {
        [_paymentMethodList addObject:NSLocalizedString(@"New Card", nil)];
    }
    
    [self.sectionArray addObject:_paymentMethodList];
    self.selectedIndexPath = [NSIndexPath indexPathForRow:newSelectedIndexPathRow
                                                inSection:([self.sectionArray count] - 1)];
}

- (void)paymentSummaryView:(PaymentSummaryView *)summaryView
 didTouchApplyCouponButton:(id)sender
{
    if(CRED.isSignedIn){
        
        NSString *enteredCouponCode = self.paymentSummaryView.couponCodeTextField.text;
        
        if([enteredCouponCode hasPrefix:@"RF"]){
            // This is Referral Code
            
        } else {
            for(Promotion *promotion in self.payment.appliedPromotions){
                if(promotion.isRequiredCode){
                    if([promotion.couponCode compare:enteredCouponCode
                                             options:NSCaseInsensitiveSearch] == NSOrderedSame){
                        [UIAlertView alertWithTitle:NSLocalizedString(@"Unable to Apply Code", nil)
                                            message:NSLocalizedString(@"Promo code has already been applied to this order.", nil)];
                        return;
                    }
                }
            }
        }
        
        [self validatePromotionWithCode:enteredCouponCode];
    } else {
        self.promotionCodeForCheckAfterSigningIn = self.paymentSummaryView.couponCodeTextField.text;
        [self showSignIn:YES];
    }
}

- (void)validatePromotionWithCode:(NSString *)code
{
    RequestResult result = RRFail;
    self.promotionCodeForCheckAfterSigningIn = nil;
    result = [PAY requestValidatePromoCode:code
                           completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              if([response objectForKey:@"rewarded_credit"] != nil){
                                  // This is Referall code response
                                  
                                  CRED.customerInfo.credit = [response integerForKey:@"credit_balance"];
                                  self.paymentSummaryView.couponCodeTextField.text = nil;
                                  
#if USE_ORANGE_CELL
                                  [self drawCredits];
#endif
                                  [self.paymentSummaryView drawPayment];
                                  self.tableView.tableHeaderView = self.paymentSummaryView;
                                  
                                  if(CRED.customerInfo.credit > 0){
                                      [self selectUseOranges];
                                  }
                                  
                                  // Check First time use Promotion and remove it
//                                  Promotion *firstTimeUsePromotion = nil;
//                                  for(Promotion *promotion in self.payment.appliedPromotions){
//                                      if(promotion.isFirstTimeUse){
//                                          firstTimeUsePromotion = promotion;
//                                          break;
//                                      }
//                                  }
//                                  
//                                  if(firstTimeUsePromotion != nil){
//                                      if ([self.payment removePromotion:firstTimeUsePromotion] == PromotionErrorSuccess){
//                                          [UIAlertView alertWithTitle:NSLocalizedString(@"First Time Use Coupon cannot be applied with Referral Program", nil)
//                                                              message:NSLocalizedString(@"So, we gave you Orange Point by Referral Program but removed First Time Use Coupon from this order.", nil)];
//                                          self.tableView.tableHeaderView = nil;
//                                          [self.paymentSummaryView drawPayment];
//                                          self.tableView.tableHeaderView = self.paymentSummaryView;
//                                          
//                                          [self selectUseOranges:NO];
//                                      }
//                                  }
                              } else {
                                  Promotion *promotion = [[Promotion alloc] initWithDictionary:response];
                                  
                                  if(promotion.merchantNo == 0 || promotion.merchantNo == ORDER.merchant.merchantNo){
                                      PromotionErrorCode returnCode = [self.payment applyPromotion:promotion];

                                      switch(returnCode){
                                          case PromotionErrorSuccess:
                                              
                                              self.paymentSummaryView.couponCodeTextField.text = nil;
                                              
                                              break;
                                          case PromotionErrorNotCombinable:
                                          case PromotionErrorNotCombinableExists:
                                          {
                                              
                                              [UIAlertView alertWithTitle:NSLocalizedString(@"Could Not Apply Promo Code", nil)
                                                                  message:NSLocalizedString(@"These codes cannot be combined.", nil)];
                                          }
                                              break;
                                          case PromotionErrorLackOrderAmount:
                                          {
                                              NSString *title = [NSString stringWithFormat:NSLocalizedString(@"You Can Get %@ Off If You Order %@ or More", nil),
                                                                 promotion.discountString, promotion.minOrderAmountString];
                                              NSString *message = [NSString stringWithFormat:NSLocalizedString(@"Your current order is only %@, would you like to go back and order more?", nil),
                                                                   self.payment.subTotalString];
                                              
                                              UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                                                              message:message
                                                                                             delegate:self
                                                                                    cancelButtonTitle:NSLocalizedString(@"No, Thanks", nil)
                                                                                    otherButtonTitles:NSLocalizedString(@"Yes", nil), nil];
                                              alert.tag = AlertViewTagPromoOrderAmount;
                                              [alert show];
                                              
                                          }
                                              break;
                                          case PromotionErrorFullUse:
                                              self.paymentSummaryView.couponCodeTextField.text = nil;
                                              break;
                                          default:

                                              break;
                                      }
                                      
                                      self.tableView.tableHeaderView = nil;
                                      [self.paymentSummaryView drawPayment];
                                      self.tableView.tableHeaderView = self.paymentSummaryView;
                                      
                                      [self selectUseOranges:NO];
                                      
                                  } else {
                                      [UIAlertView alertWithTitle:NSLocalizedString(@"Could Not Apply Promo Code", nil)
                                                          message:NSLocalizedString(@"Promo code is incorrect. Please try again", nil)];
                                  }
                              } //if([response objectForKey:@"rewarded_credit"] != nil){
                          }
                              break;
                          case ResponseErrorAlreadyReferring:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Could Not Apply Promo Code", nil)
                                                  message:NSLocalizedString(@"Sorry! You have already applied a referral code to your account.", nil)];
                              
                              break;
                          case ResponseErrorReferreringNotFirst:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Could Not Apply Promo Code", nil)
                                                  message:NSLocalizedString(@"Sorry! This code can only be used on your first order.", nil)];
                              
                              break;
                          case ResponseErrorAlreadyReferringUUID:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Could Not Apply Promo Code", nil)
                                                  message:NSLocalizedString(@"Sorry! You have already applied a referral code on this device.", nil)];
                              
                              break;
                          case ResponseErrorReferreringNotFirstUUID:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Could Not Apply Promo Code", nil)
                                                  message:NSLocalizedString(@"Sorry! This code can only be used on your first order.", nil)];
                              
                              break;
                          case ResponseErrorPromoCodeIncorrect:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Could Not Apply Promo Code", nil)
                                                  message:message];
                          }
                              break;
                          case ResponseErrorPromoInExists:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Could Not Apply Promo Code", nil)
                                                  message:message];
                          }
                              break;
                          case ResponseErrorPromoUsedAlready:
                          case ResponseErrorPromotionNotAssigned:
                          case ResponseErrorPromotionFirstUse:
                          {
                              NSString *title = [response objectForKey:@"title"];
                              NSString *message = [response objectForKey:@"message"];
                              [UIAlertView alertWithTitle:title
                                                  message:message];
                          }
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while validationg coupon code", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)selectUseOranges
{
    [self selectUseOranges:YES];
}

- (void)selectUseOranges:(BOOL)forcibly
{
    [self selectUseOranges:forcibly defaultValue:NO];
}

- (void)selectUseOrangesWithDefaultValue:(BOOL)defaultValue
{
    [self selectUseOranges:YES defaultValue:defaultValue];
}

- (void)selectUseOranges:(BOOL)forcibly defaultValue:(BOOL)defaultValue
{
#if USE_ORANGE_CELL
    NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
    OrangeCell *cell = (OrangeCell *)[self.tableView cellForRowAtIndexPath:path];
    if([cell isKindOfClass:[OrangeCell class]]){
        if(forcibly){
            cell.useButton.selected = YES;
        }
        
        [self tableView:self.tableView
    toggleButtonTouched:cell.useButton
      forRowAtIndexPath:path];
    }
#endif
    
#if USE_INLINE_ORANGE
    if(forcibly){
        self.paymentSummaryView.orangeUseSwitch.on = YES;
    }
    [self paymentSummaryViewOrangeSwitchChanged:self.paymentSummaryView.orangeUseSwitch];
#endif
}

- (void)paymentSummaryView:(PaymentSummaryView *)summaryView
      didTouchSignInButton:(id)sender
{
    [self showSignIn];
}



- (void)paymentSummaryView:(PaymentSummaryView *)summaryView
    didTouchDiscountAmount:(Promotion *)promotion
{
    DiscountViewController *viewController = [DiscountViewController viewControllerFromNib];
    viewController.payment = self.payment;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^{
                                     
                                 }];
}

- (void)paymentSummaryView:(PaymentSummaryView *)summaryView
        didTouchFeesAmount:(Promotion *)promotion
{
    DiscountViewController *viewController = [DiscountViewController viewControllerFromNib];
    viewController.payment = self.payment;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^{
                                     
                                 }];
}

- (void)paymentSummaryViewWillViewHeightChagned:(PaymentSummaryView *)summaryView
{
    self.tableView.tableHeaderView = nil;
}

- (void)paymentSummaryViewDidViewHeightChagned:(PaymentSummaryView *)summaryView
{
    self.paymentSummaryView.height = [self.paymentSummaryView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    self.tableView.tableHeaderView = self.paymentSummaryView;
}

- (void)paymentSummaryViewTipAmountChanged:(PaymentSummaryView *)summaryView
{
    [self selectUseOranges:NO];
}

- (void)showSignIn
{
    return [self showSignIn:NO withDelegate:nil];
}

- (void)showSignIn:(BOOL)showGuide
{
    return [self showSignIn:showGuide
               withDelegate:nil];
}

- (void)showSignIn:(BOOL)showGuide withDelegate:(id<SignInCustomerViewControllerDelegate>)delegate
{
    SignInCustomerViewController *viewController = [SignInCustomerViewController viewControllerFromNib];
    viewController.showGuide = showGuide;
    viewController.delegate = delegate;
    [self.tabBarController presentViewControllerInNavigation:viewController
                                                     animated:YES
                                                   completion:^{
                                                       
                                                   }];
}

- (void)signInCustomerViewControllerNeedDissmissingViewController:(SignInCustomerViewController *)viewController
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [self payNowButtonTouched:viewController];
                             }];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}
@end
