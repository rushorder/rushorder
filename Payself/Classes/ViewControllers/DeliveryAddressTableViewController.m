//
//  DeliveryAddressTableViewController.m
//  RushOrder
//
//  Created by Conan Kim on 2/20/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

#import "DeliveryAddressTableViewController.h"
#import "DeliveryAddressViewController.h"
#import "PCPaymentService.h"


#pragma mark - DeliveryAddressCell


@implementation DeliveryAddressCell

- (IBAction)removeButtonTouched:(id)sender
{
    NSIndexPath *selfIndexPath = [self.tableView indexPathForCell:self];
    
    if([self.delegate respondsToSelector:@selector(deliveryAddressCell:didTouchRemoveButtonAtIndexPath:)]){
        [self.delegate deliveryAddressCell:self
             didTouchRemoveButtonAtIndexPath:selfIndexPath];
    }
}

@end


#pragma mark - Delivery Address TableViewController
@interface DeliveryAddressTableViewController ()

@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@end

@implementation DeliveryAddressTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deliveryAddressesChanged:)
                                                 name:DeliveryAddressChangeNotification
                                               object:nil];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                        init];
    refreshControl.tintColor = [UIColor c11Color];
    [refreshControl addTarget:self
                       action:@selector(refreshControlChanged:)
             forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
}

- (void)refreshControlChanged:(UIRefreshControl *)refreshControl
{
    REMOTE.deliveryAddressDirty = YES;
    if(![REMOTE requestDeliveryAddresses]){
        [self.refreshControl endRefreshing];
    }
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:DeliveryAddressChangeNotification
                                                  object:nil];
}


- (void)deliveryAddressesChanged:(NSNotification *)notification
{
    
//    if([self.mobileCardList count] == 0){
//        self.tableView.tableHeaderView = self.noItemView;
//    } else {
//        self.tableView.tableHeaderView = self.infoHeaderView;
//    }
    
    if([notification.object isMemberOfClass:[DeliveryAddress class]]) {
//        DeliveryAddress *refreshDeliveryAddress = (DeliveryAddress *)notification.object;
        
        [self.tableView reloadData];
    } else {
        [self.tableView reloadData];
    }
    
    [self.refreshControl endRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return REMOTE.deliveryAddresses.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DeliveryAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DeliveryAddressCell"
                                                            forIndexPath:indexPath];
    cell.delegate = self;
    DeliveryAddress *anAddress = [REMOTE.deliveryAddresses objectAtIndex:indexPath.row];
    cell.nameTextField.text = anAddress.name;
    cell.addressTextField.text = anAddress.shortAddress;
    cell.defalultButton.hidden = !anAddress.isDefaultAddress;
    return cell;
}

- (void)deliveryAddressCell:(DeliveryAddressCell *)cell didTouchRemoveButtonAtIndexPath:(NSIndexPath *)indexPath
{
    DeliveryAddress *anAddress = [REMOTE.deliveryAddresses objectAtIndex:indexPath.row];
    self.selectedIndexPath = indexPath;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Are you sure to remove\n\"%@\"",nil), anAddress.shortAddress]
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"NO", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"YES", nil)
                                                    otherButtonTitles:nil];
    actionSheet.tag = 1;
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(actionSheet.tag == 1){
        if(actionSheet.destructiveButtonIndex == buttonIndex){
            [self requestRemoveDeliveryAddress];
        }
    }
}

- (void)requestRemoveDeliveryAddress
{
    DeliveryAddress *anAddress = [REMOTE.deliveryAddresses objectAtIndex:self.selectedIndexPath.row];
    
    RequestResult result = RRFail;
    result = [PAY requestDeleteDeliveryAddress:anAddress
                            completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              [REMOTE handleDeliveryAddressesResponse:(NSMutableArray *)response
                                                     withNotification:NO];
                              NSInteger defaultRow = NSNotFound;
                              for(DeliveryAddress *anDeliveryAddress in REMOTE.deliveryAddresses){
                                  if(defaultRow == NSNotFound) defaultRow = 0;
                                  if(anDeliveryAddress.isDefaultAddress){
                                      break;
                                  }
                                  defaultRow++;
                              }
                              
                              [self.tableView beginUpdates];
                              if(defaultRow == self.selectedIndexPath.row){
                                  [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:defaultRow+1
                                                                                              inSection:0]]
                                                        withRowAnimation:UITableViewRowAnimationFade];
                              } else {
                                  if(defaultRow != NSNotFound){
                                      [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:defaultRow
                                                                                                  inSection:0]]
                                                            withRowAnimation:UITableViewRowAnimationFade];
                                  }
                              }

                              [self.tableView deleteRowsAtIndexPaths:@[self.selectedIndexPath]
                                                    withRowAnimation:UITableViewRowAnimationLeft];
                              [self.tableView endUpdates];
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              NSString *title = [response objectForKey:@"title"];
                              if(title == nil){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alertWithTitle:title
                                                      message:message];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}


- (IBAction)addNewAddressButtonTouched:(id)sender
{
    [self performSegueWithIdentifier:@"DetailDeliveryAddressSegue"
                              sender:sender];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    DeliveryAddressViewController *vc = [segue destinationViewController];
    if([sender isKindOfClass:[UITableViewCell class]]){
        UITableViewCell *selectedCell = (UITableViewCell *)sender;
        if(REMOTE.deliveryAddresses.count > selectedCell.indexPath.row){
            DeliveryAddress *anAddress = [REMOTE.deliveryAddresses objectAtIndex:selectedCell.indexPath.row];
            vc.deliveryAddress = anAddress;
        } else {
            [self.tableView reloadData];
        }
    } else {
        [vc setDeliveryAddress:nil];
    }
}

@end
