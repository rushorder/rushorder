//
//  DeliveryFeeViewController.swift
//  RushOrder
//
//  Created by Conan Kim on 9/28/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

import UIKit

class DeliveryFeeTableViewCell: UITableViewCell {
    @IBOutlet weak var feeLabel: UILabel!
    @IBOutlet weak var subtotalLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

class DeliveryFeeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    let deliveryFeeDiscount: NSArray
    let baseDeliveryFee: Int64
    var deliveryFeeDiscountDescription: NSMutableArray?
    
    init(nibName: String?, bundle: Bundle?, deliveryFeeDiscount: NSArray, baseDeliveryFee: Int64){
        self.deliveryFeeDiscount = deliveryFeeDiscount
        self.baseDeliveryFee = baseDeliveryFee
        super.init(nibName: nibName, bundle: bundle)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        tableView.register(DeliveryFeeTableViewCell.self, forCellReuseIdentifier: "cell")
        fillDescriptionsTo(deliveryFeeDiscount: self.deliveryFeeDiscount)
        tableView.register(UINib(nibName:"DeliveryFeeCell", bundle: nil), forCellReuseIdentifier: "cell")
        // Do any additional setup after loading the view.
        
        self.tableViewHeightConstraint.constant = 32 * CGFloat(self.deliveryFeeDiscountDescription!.count)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.deliveryFeeDiscountDescription!.count
    }
    
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DeliveryFeeTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "cell") as? DeliveryFeeTableViewCell
        let item = self.deliveryFeeDiscountDescription?[indexPath.row] as! NSDictionary
        cell.feeLabel?.text = item["fee"] as? String
        cell.subtotalLabel?.text = item["order_sum"] as? String
        
//        print("Hell \(item)")
        // Configure the cell...
        
        return cell
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("Contetnt Height : \(self.tableView.contentSize.height)")
        super.viewDidAppear(animated)
    }
    
    func fillDescriptionsTo(deliveryFeeDiscount: NSArray) {
        deliveryFeeDiscountDescription = []
        var decorateText = " And Over"
        var i = 0
        for item in deliveryFeeDiscount.reversed() {
            
            let aFeeDiscount = item as! NSDictionary
            let discountAmount = Int64(aFeeDiscount["discount_amount"] as! NSNumber)
            let atLeastAmount = aFeeDiscount["at_least_amount"] as! NSNumber
            let discountFee = NSNumber(value: (baseDeliveryFee - discountAmount))
   
            let aDesc = ["fee":discountFee.intValue > 0 ? discountFee.currencyString() : "Free",
                         "order_sum":atLeastAmount.currencyString() + decorateText]
            deliveryFeeDiscountDescription?.add(aDesc)
        
            let underAmount = NSNumber(value: atLeastAmount.int64Value - 1)
            if i == (deliveryFeeDiscount.count - 1) {
                decorateText = "\(underAmount.currencyString() ?? "") And Under"
            } else {
                decorateText = " - \(underAmount.currencyString() ?? "")"
            }
            i += 1
        }
        
        let aDesc = ["fee":NSNumber(value: baseDeliveryFee).currencyString(),
                     "order_sum":decorateText]
        deliveryFeeDiscountDescription?.add(aDesc)
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
