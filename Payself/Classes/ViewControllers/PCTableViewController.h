//
//  PCTableViewController.h
//  RushOrder
//
//  Created by Conan on 6/14/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCTableViewController : UITableViewController

@property (nonatomic) BOOL exceptDefalutBackground;
@property (readonly) BOOL isVisible;
@end