//
//  PassCodeSettingViewController.m
//  RushOrder
//
//  Created by Conan on 3/28/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PassCodeSettingViewController.h"
#import "TableSectionItem.h"
#import "PCNavigationController.h"
#import "PCCredentialService.h"
#import "CustomCell.h"

@interface PassCodeSettingViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *sectionList;

@end

@implementation PassCodeSettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self != nil){
        [self commonInit];
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

- (void)commonInit
{
    self.exceptDefalutBackground = YES;
    
    self.sectionList = [NSMutableArray array];
    
    ///////////////////////// Sections /////////////////////////////////////////
    NSMutableArray *sectionItems = [NSMutableArray array];
    
    //Menu
    TableItem *tableItem = [[TableItem alloc] init];
    tableItem.tag = 1;
    tableItem.action = @selector(turnPasscode:);
    [sectionItems addObject:tableItem];
    //////
    
    //Menu
    tableItem = [[TableItem alloc] init];
    tableItem.title = NSLocalizedString(@"Change Passcode", nil);
    tableItem.action = @selector(changePasscode:);
    tableItem.tag = 2;
    [sectionItems addObject:tableItem];
//    //////
    
    //Section
    TableSection *tableSection = [[TableSection alloc] init];
    tableSection.title = nil;
    tableSection.menus = sectionItems;
    [self.sectionList addObject:tableSection];
    //////////
    
    ///////////////////////// Sections /////////////////////////////////////////
    //    sectionItems = [NSMutableArray array];
    //
    //    //Menu
    //    tableItem = [[TableItem alloc] init];
    //    tableItem.title = NSLocalizedString(@"Report", nil);
    //    tableItem.action = @selector(goAggregation:);
    //    [sectionItems addObject:tableItem];
    //    //////
    //
    //    //Section
    //    tableSection = [[TableSection alloc] init];
    //    tableSection.title = NSLocalizedString(@"First", nil);
    //    tableSection.menus = sectionItems;
    //    [self.sectionList addObject:tableSection];
    /////////
    
    
    ////////////////////////////////////////////////////////////////////////////
}

- (void)turnPasscode:(id)sender
{
    if(CRED.isPinSet){
        PassCodeViewController *viewController = [PassCodeViewController viewControllerFromNib];
        viewController.passMode = YES;
        viewController.delegate = self;
        viewController.passCodeMode = PassCodeModeOff;
        PCNavigationController *navigationController = [[PCNavigationController alloc] initWithRootViewController:viewController];
        [self presentViewController:navigationController
                           animated:YES
                         completion:^(){
                         }];        
    } else {
        PassCodeViewController *viewController = [PassCodeViewController viewControllerFromNib];
        PCNavigationController *navigationController = [[PCNavigationController alloc] initWithRootViewController:viewController];
        [self presentViewController:navigationController
                           animated:YES
                         completion:^(){
                         }];
    }
}

- (void)changePasscode:(id)sender
{
    if(CRED.isPinSet){
        
        PassCodeViewController *viewController = [PassCodeViewController viewControllerFromNib];
        viewController.passMode = YES;
        viewController.delegate = self;
        viewController.passCodeMode = PassCodeModeChange;
        PCNavigationController *navigationController = [[PCNavigationController alloc] initWithRootViewController:viewController];
        [self presentViewController:navigationController
                           animated:YES
                         completion:^(){
                         }];
    } else {
        [self.tableView reloadData];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Passcode Lock", nil);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sectionList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    TableSection *tableSection = [self.sectionList objectAtIndex:section];
    return [tableSection.menus count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CustomCell";
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil){
        cell = [CustomCell cell];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    
    // Configure the cell...
    
    TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
    TableItem *tableItem = [tableSection.menus objectAtIndex:indexPath.row];
    
    if(tableItem.tag == 1){
        if(CRED.isPinSet){
            tableItem.title = NSLocalizedString(@"Turn Passcode OFF", nil);
            tableItem.subTitle = NSLocalizedString(@"On", nil);
        } else {
            tableItem.title = NSLocalizedString(@"Turn Passcode ON", nil);
            tableItem.subTitle = NSLocalizedString(@"Off", nil);
        }
    } else if(tableItem.tag == 2){
        if(!CRED.isPinSet){
            cell.titleLabel.textColor = [UIColor lightGrayColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        } else {
            cell.titleLabel.textColor = [UIColor darkGrayColor];
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
    }
    
    cell.titleLabel.text = tableItem.title;
    cell.subTitleLabel.text = tableItem.subTitle;
    
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    TableSection *tableSection = [self.sectionList objectAtIndex:section];
    
    return tableSection.title;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
    TableItem *tableItem = [tableSection.menus objectAtIndex:indexPath.row];
    
    [self performSelector:tableItem.action
               withObject:indexPath];
}

- (void)didPassCodeAuthorized:(PassCodeViewController *)passCodeViewController
{
    if(passCodeViewController.passCodeMode == PassCodeModeChange){
        PassCodeViewController *viewController = [PassCodeViewController viewControllerFromNib];
        PCNavigationController *navigationController = [[PCNavigationController alloc] initWithRootViewController:viewController];
        [self presentViewController:navigationController
                           animated:YES
                         completion:^(){
                         }];
    } else if (passCodeViewController.passCodeMode == PassCodeModeOff) {
        [CRED.appPINItem resetKeychainItem];
        [NSUserDefaults standardUserDefaults].pinSet = NO;
        [self.tableView reloadData];
    }
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [super viewDidUnload];
}
@end
