//
//  MenuPhotoViewController.m
//  RushOrder
//
//  Created by Conan on 12/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MenuPhotoViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MenuSubCategory.h"

@interface MenuPhotoViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *menuImageView;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *sectionLabel;
@property (weak, nonatomic) IBOutlet UILabel *subSctionLabel;
@property (weak, nonatomic) IBOutlet UIView *descriptionContainer;
@property (weak, nonatomic) IBOutlet UILabel *menuNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *menuPriceLabel;
@property (weak, nonatomic) IBOutlet TouchableLabel *menuDescLabel;
@end

@implementation MenuPhotoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if([self.item isKindOfClass:[MenuItem class]]){
        
        self.view.backgroundColor = [UIColor blackColor];
        
        MenuItem *menuItem = (MenuItem *)self.item;
        
        self.imageView.hidden = NO;
        self.descriptionContainer.hidden = NO;
        self.sectionLabel.hidden = YES;
        self.subSctionLabel.hidden = YES;
        self.menuImageView.hidden = YES;
        self.lineView.hidden = YES;
        
        self.menuDescLabel.target = self;
        
        [self.imageView sd_setImageWithURL:menuItem.imageURL
                       placeholderImage:[UIImage imageNamed:@"menu_placeholder"]
                                options:0
                              completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                  
                              }];
        
        
        /*
         [[SDImageCache sharedImageCache] queryDiskCacheForKey:menuItem.thumbnailURL.absoluteString
         done:^(UIImage *image, SDImageCacheType cacheType) {
         if(image == nil){
         self.imageView.image = [UIImage imageNamed:@"menu_placeholder"];
         } else {
         self.imageView.image = image;
         }
         }];
         
         [self.imageView setImageWithURL:menuItem.imageURL
         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
         
         }];
         */
        
        self.menuNameLabel.text = menuItem.name;
        self.menuPriceLabel.text = menuItem.defaultMenuPrice.priceString;
        self.menuDescLabel.text = menuItem.desc;
        
    } else if([self.item isKindOfClass:[MenuSubCategory class]]){
        
        self.view.backgroundColor = [UIColor colorWithR:231.0f G:226.0f B:220.0f];
        
        MenuSubCategory *subCategory = (MenuSubCategory *)self.item;
        
        self.imageView.hidden = YES;
        self.descriptionContainer.hidden = YES;
        self.sectionLabel.hidden = NO;
        self.subSctionLabel.hidden = NO;
        self.menuImageView.hidden = NO;
        self.lineView.hidden = NO;
        
        
        self.sectionLabel.text = self.category.name;
        self.subSctionLabel.text = subCategory.name;
        
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeButtonTouched:(id)sender
{
    if([self.delegate respondsToSelector:@selector(menuPhotoViewController:didTouchCloseButtonAtIndexPath:)]){
        [self.delegate menuPhotoViewController:self
                didTouchCloseButtonAtIndexPath:self.shouldMenuDetailPop ? self.indexPath : nil];
    }
}

- (IBAction)addItemButtonTouched:(id)sender
{
    if([self.delegate respondsToSelector:@selector(menuPhotoViewController:didSelectAddItemButtonAtIndexPath:)]){
        
        [self dismissViewControllerAnimated:YES
                                 completion:^{
                                     [self.delegate menuPhotoViewController:self
                                          didSelectAddItemButtonAtIndexPath:self.indexPath];
                                 }];
    }
}

- (void)touchableLabelActionTriggerred:(id)sender
{
    if([self.delegate respondsToSelector:@selector(menuPhotoViewController:didTouchDescLabelAtIndexPath:)]){
        [self.delegate menuPhotoViewController:self
                  didTouchDescLabelAtIndexPath:self.indexPath];
    }
}

@end
