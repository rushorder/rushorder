//
//  NearYouTableViewController.h
//  RushOrder
//
//  Created by Conan Kim on 7/4/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MerchantViewController.h"
#import "TouchableImageView.h"

@protocol NearYouTableViewCellDelegate;

@interface NearYouTableViewCell : UITableViewCell<TouchableImageViewAction>
@property (weak, nonatomic) IBOutlet CircleImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UITextView *descTextView;
@property (nonatomic, weak) id<NearYouTableViewCellDelegate>delegate;


@property (weak, nonatomic) IBOutlet TouchableImageView *pic1;
@property (weak, nonatomic) IBOutlet TouchableImageView *pic2;
@property (weak, nonatomic) IBOutlet TouchableImageView *pic3;
@property (weak, nonatomic) IBOutlet TouchableImageView *pic4;
@property (weak, nonatomic) IBOutlet TouchableImageView *pic5;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *picContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *picContainerTopConstraint;

@end

@protocol NearYouTableViewCellDelegate <UITableViewDelegate>
- (void)tableView:(UITableView *)tableView
             cell:(NearYouTableViewCell *)cell
didTouchImageButtonAtIndexPath:(NSIndexPath *)indexPath
       withMenuIndex:(NSInteger)menuIndex;
@end

@interface NearYouTableViewController : PCTableViewController
<UITextViewDelegate,
MerchantViewControllerDelegate,
NearYouTableViewCellDelegate>

@end
