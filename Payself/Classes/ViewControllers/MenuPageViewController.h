//
//  MenuPageViewController.h
//  RushOrder
//
//  Created by Conan on 5/14/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "MenuItemCell.h"
#import "MenuDetailViewController.h"
#import "OrderViewController.h"
#import "Merchant.h"
#import "MenuPhotoViewController.h"
#import "MenuCellController.h"

@interface MenuPageViewController : PCViewController
<
UIPickerViewDataSource,
UIPickerViewDelegate,
MenuItemCellDelegate,
MenuDetailViewControllerDelegate,
OrderViewControllerDelegate,
UITextFieldDelegate,
UIAlertViewDelegate,
UIActionSheetDelegate,
UIPageViewControllerDelegate,
UIPageViewControllerDataSource,
MenuCellControllerDelegate,
UIScrollViewDelegate,
CAAnimationDelegate
>

@property (strong, nonatomic) TableInfo *tableInfo;
@property (nonatomic) CartType initialCartType;
@property (nonatomic, getter = isEnteredFromList) BOOL enteredFromList;
@property (nonatomic, getter = isForceOpenOrderViewController) BOOL forceOpenOrderViewController;
@property (strong, nonatomic) NSArray *referredMenus;
- (void)cartUnsolicitChanged:(NSNotification *)notification;

@end
