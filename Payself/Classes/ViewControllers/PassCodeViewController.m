//
//  PassCodeViewController.m
//  RushOrder
//
//  Created by Conan on 3/28/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PassCodeViewController.h"
#import "PCCredentialService.h"

@interface PassCodeViewController ()

@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (strong, nonatomic) NSArray *textFields;
@property (weak, nonatomic) IBOutlet UITextField *pinTextField;
@property (weak, nonatomic) IBOutlet UITextField *pinConfirmTextField;
@property (weak, nonatomic) IBOutlet PCTextField *pin1TextField;
@property (weak, nonatomic) IBOutlet PCTextField *pin2TextField;
@property (weak, nonatomic) IBOutlet PCTextField *pin3TextField;
@property (weak, nonatomic) IBOutlet PCTextField *pin4TextField;
@end

@implementation PassCodeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.exceptDefalutBackground = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(!self.isModal){
        self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Cancel", nil)
                                                                                  target:self
                                                                                  action:@selector(close:)];
    }
    
    self.textFields = [NSArray arrayWithObjects:
                       self.pin1TextField,
                       self.pin2TextField,
                       self.pin3TextField,
                       self.pin4TextField,
                       nil];
    
    if(self.isPassMode){
        self.title = NSLocalizedString(@"Passcode", nil);
        self.infoLabel.text = NSLocalizedString(@"Verify your passcode", nil);
    } else {
        if(CRED.isPinSet){
            self.title = NSLocalizedString(@"Change Passcode", nil);
            self.infoLabel.text = NSLocalizedString(@"Change your passcode for RushOrder", nil);
        } else {
            self.title = NSLocalizedString(@"Set Passcode Lock", nil);
            self.infoLabel.text = NSLocalizedString(@"Set your passcode for RushOrder", nil);
        }
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.pinTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)close:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^(){
                             }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{    
//    NSMutableString *mutableString = [NSMutableString stringWithString:textField.text];
//    [mutableString appendString:string];
    
    if(range.location == 3 && (range.length == 0)){
        [self performSelector:@selector(inputDone:)
                   withObject:textField
                   afterDelay:0.1];
        return YES;
    } else if(range.location > 3){
        return NO;
    } else {
        return YES;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField.tag <= 100){
        
        UIView *firstResponder = self.view.findFirstResponder;
        if(firstResponder != nil){
            [firstResponder resignFirstResponder];
            [firstResponder becomeFirstResponder];
        } else {
            [self.pinTextField becomeFirstResponder];
        }
        return NO;
    }
    return YES;
}

- (void)inputDone:(UITextField *)textField
{
    self.infoLabel.textColor = [UIColor blackColor];
    self.infoLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:0.6f];
    self.infoLabel.shadowOffset = CGSizeMake(0, 1.0f);
    
    if(textField == self.pinTextField){
        if(self.isPassMode){
            
            NSString *password = [CRED.appPINItem objectForKey:(__bridge id)(kSecValueData)];
            
            if([password isEqualToString:self.pinTextField.text]){
                
                [self dismissViewControllerAnimated:YES
                                         completion:^(){
                                             if([self.delegate respondsToSelector:@selector(didPassCodeAuthorized:)]){
                                                 [self.delegate didPassCodeAuthorized:self];
                                             }
                                         }];
            } else {
                self.pinTextField.text = nil;
                [self pinTextFieldEditingChanged:self.pinTextField];
                self.infoLabel.text = NSLocalizedString(@"Passcode is wrong. Try again please.", nil);
                self.infoLabel.textColor = [UIColor redColor];
                self.infoLabel.shadowColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
                self.infoLabel.shadowOffset = CGSizeMake(0, -1.0f);
            }
            
        } else {
            // Need to confirm
            self.pinConfirmTextField.text = nil;
            [self.pinConfirmTextField becomeFirstResponder];
            [self pinTextFieldEditingChanged:self.pinConfirmTextField];
            
            self.infoLabel.text = NSLocalizedString(@"Confirm your passcode", nil);
        }
        
    } else if (textField == self.pinConfirmTextField){
        
        if([self.pinTextField.text isEqual:self.pinConfirmTextField.text]){
            
            self.infoLabel.text = NSLocalizedString(@"Setting your passcode...", nil);
            
            [CRED.appPINItem setObject:@"PayselfPrivatePIN"
                                forKey:(__bridge id)(kSecAttrAccount)];
            [CRED.appPINItem setObject:self.pinTextField.text
                                forKey:(__bridge id)(kSecValueData)];
            [NSUserDefaults standardUserDefaults].pinSet = YES;
            
            // Success to set
            [self dismissViewControllerAnimated:YES
                                     completion:^(){
                                     }];
        } else {
            
            self.infoLabel.text = NSLocalizedString(@"Passcode did not match. Plaease try again.", nil);
            // Mismatch fail
            self.pinTextField.text = nil;
            self.pinConfirmTextField.text = nil;
            [self.pinTextField becomeFirstResponder];
            [self pinTextFieldEditingChanged:self.pinTextField];
        }
    }
}


- (IBAction)pinTextFieldEditingChanged:(UITextField *)sender
{
    NSInteger passLength = [sender.text length];
    
    for(UITextField *aTextField in self.textFields){
        aTextField.text = (aTextField.tag < passLength) ? @"*" : nil;
    }
}

- (IBAction)pinCodeDigitTextFieldTouched:(id)sender
{
    PCLog(@"Touched");
}

- (void)viewDidUnload {
    [self setPinTextField:nil];
    [self setPin1TextField:nil];
    [self setPin2TextField:nil];
    [self setPin3TextField:nil];
    [self setPin4TextField:nil];
    [self setPinConfirmTextField:nil];
    [self setInfoLabel:nil];
    [super viewDidUnload];
}
@end
