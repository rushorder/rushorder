//
//  RecentListViewController.m
//  RushOrder
//
//  Created by Conan Kim on 6/19/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import "RecentListViewController.h"
#import "RecentAddressCell.h"

@interface RecentListViewController ()
@property (strong, nonatomic) UIView *nomatchesView;
@property (strong, nonatomic) NSMutableArray *customRequests;
@end

@implementation RecentListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.selectedRecentIndex = NSNotFound;
    self.customRequests = [CustomRequest listWithCondition:@"order by lastUsedDate desc"];
    
}

- (UIView *)nomatchesView
{
    if(_nomatchesView == nil){
        _nomatchesView = [[UIView alloc] initWithFrame:self.panelView.frame];
        _nomatchesView.backgroundColor = [UIColor clearColor];
        _nomatchesView.translatesAutoresizingMaskIntoConstraints = NO;
        
        UILabel *matchesLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0,self.panelView.width,200)];
        matchesLabel.font = [UIFont systemFontOfSize:18];
        matchesLabel.numberOfLines = 1;
        matchesLabel.lineBreakMode = NSLineBreakByWordWrapping;
        matchesLabel.shadowColor = [UIColor lightTextColor];
        matchesLabel.textColor = [UIColor grayColor];
        matchesLabel.shadowOffset = CGSizeMake(0, 1);
        matchesLabel.backgroundColor = [UIColor clearColor];
        matchesLabel.textAlignment =  NSTextAlignmentCenter;
        matchesLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        //Here is the text for when there are no results
        matchesLabel.text = @"No Recent Customer Requests";
        
        
        _nomatchesView.hidden = YES;
        [_nomatchesView addSubview:matchesLabel];
        
        [_nomatchesView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[matchesLabel]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:@{@"matchesLabel":matchesLabel}]];
        
        [_nomatchesView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[matchesLabel]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:@{@"matchesLabel":matchesLabel}]];
        
        [self.panelView addSubview:_nomatchesView];
        [self.panelView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-[nomatchesView]-|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:@{@"nomatchesView":_nomatchesView}]];
        
        [self.panelView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-80-[nomatchesView]-50-|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:@{@"nomatchesView":_nomatchesView}]];
        [self.panelView setNeedsUpdateConstraints];
    }
    return _nomatchesView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([self.customRequests count] == 0 ){
        self.nomatchesView.hidden = NO;
    } else {
        self.nomatchesView.hidden = YES;
    }
    
    return [self.customRequests count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"RecentAddressCell";
    RecentAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil){
        cell = [RecentAddressCell cell];
    }
    
    CustomRequest *request = [self.customRequests objectAtIndex:indexPath.row];
    cell.addressLabel.text = request.request;
    
    if(self.selectedRecentIndex != NSNotFound){
        if(self.selectedRecentIndex == indexPath.row){
            cell.checkImageView.hidden = NO;
        } else {
            cell.checkImageView.hidden = YES;
        }
    } else {
        cell.checkImageView.hidden = YES;
    }
    
    return cell;
}

- (IBAction)recentAddressCloseButtonTouched:(id)sender
{
    [UIView animateWithDuration:0.3f
                     animations:^{
                         self.view.alpha = 0.0f;
                         CGRect frame = self.panelView.frame;
                         frame.origin.y = 700.0f;
                         self.panelView.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         if(finished){
                             [self.view removeFromSuperview];
                             self.selectedRecentIndex = NSNotFound;
                         }
                     }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedRecentIndex = indexPath.row;
    
    CustomRequest *request = [self.customRequests objectAtIndex:self.selectedRecentIndex];
    
    [self.delegate recentListViewController:self
                           didSelectRequest:request];
    
    [self.tableView reloadData];
    [self performSelector:@selector(closeAfterDelay)
               withObject:nil
               afterDelay:0.2f];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle == UITableViewCellEditingStyleDelete){
        self.selectedRecentIndex = NSNotFound;
        Addresses *address = [self.customRequests objectAtIndex:indexPath.row];
        if([address delete]){
            [self.customRequests removeObjectAtIndex:indexPath.row];
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                  withRowAnimation:UITableViewRowAnimationLeft];
        }
    }
}


- (void)closeAfterDelay
{
    [self recentAddressCloseButtonTouched:nil];
}

@end
