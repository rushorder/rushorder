//
//  MenuCellController.m
//  RushOrder
//
//  Created by Conan on 6/15/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "MenuCellController.h"
#import "PCPaymentService.h"
#import "IIViewDeckController.h"
#import "TablesetViewController.h"
#import "BillViewController.h"
#import "MenuCategory.h"
#import "MenuSubCategory.h"
#import "MenuOptionGroup.h"
#import "MenuOption.h"
#import "MenuItem.h"
#import "OrderManager.h"
#import "MenuManager.h"
#import "PCMenuService.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FormatterManager.h"
#import "PCCredentialService.h"
#import "PaymentMethodViewController.h"
#import "NewCardViewController.h"
#import "RestaurantSummaryView.h"
#import "MenuPhotoPageViewController.h"
#import "RemoteDataManager.h"
#import "DeliveryViewController.h"
#import "MenuPan.h"

#import "TransactionManager.h"

#define MENU_REL_XIB_NAME   @"MenuItemCell"
#define SEL_BACK_IMAGEVIEW_MARGIN 5.0f

#define IMG_BACK_ORIGIN_Y 7.0f

typedef enum menuItemType_{
    MenuItemTypeSubCategory = 1,
    MenuItemTypeItem
} MenuItemType;

@interface MenuCellController ()

@property (weak, nonatomic) IBOutlet UIScrollView *shortcutScrollView;
@property (strong, nonatomic) OrderViewController *orderViewController;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) NSInteger selectedQuantity;
@property (copy, nonatomic) NSString *specialInstructions;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (strong, nonatomic) NSMutableArray *mobileCardList;
@property (strong, nonatomic) IBOutlet RestaurantSummaryView *restaurantSummaryView;

@property (nonatomic) BOOL didAppeared;
@property (nonatomic) BOOL checkedNoMenuItem;
@property (strong, nonatomic) IBOutlet UIView *noItemView;
@property (weak, nonatomic) IBOutlet UILabel *noMenuInfoLabel;
@property (weak, nonatomic) IBOutlet NPStretchableButton *gotoRequestBillButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shortcutHeightConstraint;
@property (strong, nonatomic) NSLayoutConstraint *selectMarkLeadingConstraint;
//@property (weak, nonatomic) IBOutlet UIImageView *shortcutBackImageView;

@property (strong, nonatomic) MenuItemCell *offScreenMenuItemCell;
@property (strong, nonatomic) MenuSubCategoryCell *offScreenMenuSubCategoryCell;
@property (nonatomic) NSInteger selectedSectionIndex;

@property (strong, nonatomic) IBOutlet MenuPhotoPageViewController *photoPageViewController;
@property (strong, nonatomic) IBOutlet UIImageView *selectedSectionBackground;
@property (strong, nonatomic) IBOutlet UIView *tableFooterView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectedSectionBgWidthConstraint;
@property (weak, nonatomic) IBOutlet UIView *pageDotContainer;
@property (nonatomic) NSUInteger selectedPageIndex;
@property (nonatomic) NSUInteger totalPageCount;
@property (nonatomic) CGFloat widthPerPage;
@property (nonatomic, getter = isCardListWaiting) BOOL cardListWaiting;

@property (nonatomic) CGFloat shortcutContentWidth;

@property (nonatomic) BOOL holdSpyScrolling;

@property (nonatomic, getter = isViewDidLayoutOnce) BOOL viewDidLayoutOnce;
@property (nonatomic, getter = isNeedToAddTableFooter) BOOL needToAddTableFooter;

@property (nonatomic, getter = isNeedToBeReloaded) BOOL needToBeReloaded;

@end

@implementation MenuCellController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(menuListChanged:)
                                                     name:MenuListChangedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(merchantUpdated:)
                                                     name:MerchantInformationUpdatedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(willEnterForeground:)
                                                     name:UIApplicationWillEnterForegroundNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(orderTypeSwitched:)
                                                     name:OrderTypeSwitchedNotification
                                                   object:nil];
        
        self.selectedSectionIndex = -1;
    }
    return self;
}

- (void)orderTypeSwitched:(NSNotification *)notification
{
    self.initialCartType = ORDER.cart.cartType;
}

- (void)merchantUpdated:(NSNotification *)notification
{
    BOOL invalidMerchant = [notification.userInfo boolForKey:@"InvalidMerchant"];
    if(invalidMerchant){
        
//        if([APP.viewDeckController isSideOpen:IIViewDeckRightSide]){
//            [APP.viewDeckController closeRightViewAnimated:YES
//                                                completion:^(IIViewDeckController *controller, BOOL success) {
//                                                    if(success){
//                                                        TRANS.dirty = YES;
//                                                        [self.navigationController popToRootViewControllerAnimated:YES];
//                                                    }
//                                                }];
//        }
        
        // TODO: Can I do this?
        
        
    } else {
        self.tableView.tableHeaderView = nil;
        [self.restaurantSummaryView drawMerchant:ORDER.merchant
                                     forceHeight:YES
                                      isDelivery:(self.initialCartType == CartTypeDelivery)
                                       showPromo:YES
                                   showRoService:(self.initialCartType == CartTypeDelivery)
                                        cartType:self.initialCartType];
        switch(self.initialCartType){
            case CartTypePickupServiced:
            case CartTypePickup:
                if(ORDER.merchant.isTableBase){
                    self.initialCartType = CartTypeCart;
                }
            case CartTypeCart:
                if(!ORDER.merchant.isTableBase){
                    if(ORDER.merchant.servicedBy == ServicedByStaff){
                        self.initialCartType = CartTypePickupServiced;
                    } else {
                        self.initialCartType = CartTypePickup;
                    }
                }
                break;
            default:
                
                break;
        }
        self.tableView.tableHeaderView = self.restaurantSummaryView;
    }
}

- (void)willEnterForeground:(NSNotification *)aNoti
{
    [self.tableView reloadData];
}


- (void)menuListChanged:(NSNotification *)notification
{
    self.gotoRequestBillButton.hidden = YES;
    
    if([[self menuList] count] > 0){
        [self reloadData];
        if(self.tableView.tableFooterView == self.noItemView){
            self.tableView.tableFooterView = nil;
        }
    } else if(ORDER.merchant.isAblePayment){
        self.tableView.tableFooterView = self.noItemView;
        self.noMenuInfoLabel.text = NSLocalizedString(@"This restaurant may be configuring its menu so you cannot order right now.", nil);

        
        
    } else {
        self.tableView.tableFooterView = self.noItemView;
        self.noMenuInfoLabel.text = NSLocalizedString(@"This restaurant may be configuring its menu so you cannot order right now.", nil);
        
        [self reloadData];
    }
}

- (void)reloadData
{
    if(self.isViewLoaded){
        self.needToBeReloaded = NO;
        [self.tableView reloadData];
        [self drawShortcut];
        //Can be estimated
        if(self.isViewDidLayoutOnce){
            [self addFooterIfNeeded];
        } else {
            self.needToAddTableFooter = YES;
        }
    } else {
        self.needToBeReloaded = YES;
    }
}

- (void)drawShortcut
{
    if([[self menuList] count] < 2){
        self.shortcutHeightConstraint.constant = 0.0f;
        self.shortcutScrollView.hidden = YES;
//        self.shortcutBackImageView.hidden = YES;
        return;
    } else {
        self.shortcutHeightConstraint.constant = 49.0f;
        self.shortcutScrollView.hidden = NO;
//        self.shortcutBackImageView.hidden = NO;
    }
    
    CGFloat xOrigin = 0.0f;
    
    [self.shortcutScrollView removeAllSubviews];
    [self.pageDotContainer removeAllSubviews];
    self.pageDotContainer.translatesAutoresizingMaskIntoConstraints = NO;
    NSInteger i = 0;
    
    UIView *backgroundView = [[UIView alloc] init];
    backgroundView.backgroundColor = [UIColor clearColor];
    backgroundView.tag = -1;
    
    for(MenuCategory *menuCategory in [self menuList]){
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(xOrigin,
                                                                      0.0f,
                                                                      100.0f,
                                                                      49.0f)];

        button.buttonTitle = menuCategory.name;
        if([menuCategory.name length] == 0 && i == 0){
            button.buttonTitle = NSLocalizedString(@"Top", nil);
        }
        button.titleLabel.font = [UIFont systemFontOfSize:14.0f];

        [button setTitleColor:[UIColor c3Color] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor c11Color] forState:UIControlStateHighlighted];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        
        button.titleLabel.textAlignment = NSTextAlignmentCenter;
        button.tag = i;
        button.autoresizingMask = UIViewAutoresizingFlexibleHeight;

        [button addTarget:self
                   action:@selector(shortcutButtonTouched:)
         forControlEvents:UIControlEventTouchUpInside];
        button.titleEdgeInsets = UIEdgeInsetsMake(-10.0f,
                                                  0.0f,
                                                  0.0f,
                                                  0.0f);
        CGSize size = [button intrinsicContentSize];
        size.width += 40.0f;
        size.height = self.shortcutScrollView.height;
        button.size = size;
        xOrigin += button.width;
        
        UIImageView *buttonBackImageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"button_menu_category_off"] resizableImageFromCenter]];
        buttonBackImageView.tag = -1;
        [backgroundView addSubview:buttonBackImageView];
        CGRect backFrame = buttonBackImageView.frame;
        backFrame.origin.x = button.x + SEL_BACK_IMAGEVIEW_MARGIN;
        backFrame.origin.y = IMG_BACK_ORIGIN_Y;
        backFrame.size.width = button.width - (SEL_BACK_IMAGEVIEW_MARGIN * 2);
        buttonBackImageView.frame = backFrame;
        
        if(i == 0 && self.selectedSectionBackground != nil){
            [self.shortcutScrollView addSubview:self.selectedSectionBackground];
            NSArray *selectionButtonConstraints = [NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"|-%f-[selectedSectionBackground]", button.x + SEL_BACK_IMAGEVIEW_MARGIN]
                                                                                          options:0
                                                                                          metrics:nil
                                                                                            views:@{@"selectedSectionBackground":self.selectedSectionBackground}];
            if([selectionButtonConstraints count] > 0)
                self.selectMarkLeadingConstraint = [selectionButtonConstraints objectAtIndex:0];
            
            [self.shortcutScrollView addConstraints:selectionButtonConstraints];
            
            [self.shortcutScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[selectedSectionBackground]", IMG_BACK_ORIGIN_Y]
                                                                                            options:0
                                                                                            metrics:nil
                                                                                              views:@{@"selectedSectionBackground":self.selectedSectionBackground}]];
            
            self.selectedSectionBgWidthConstraint.constant = button.width - (SEL_BACK_IMAGEVIEW_MARGIN * 2);
            button.selected = YES;
            self.selectedSectionIndex = 0;
            
        }
        
        [self.shortcutScrollView addSubview:button];
        i++;
    }
    [self.shortcutScrollView insertSubview:backgroundView
                                   atIndex:0];
    
    if(self.shortcutScrollView.width == 0){
        PCWarning(@"Shortcut ScrollView width is 0");
        self.totalPageCount = 1;
    } else {
        self.totalPageCount = (NSUInteger)ceil(xOrigin / self.shortcutScrollView.width);
    }
    self.widthPerPage = xOrigin / (CGFloat)self.totalPageCount;
    
#define DOT_SPACE 12.0f
    
    UIButton *prevButton = nil;
    
    NSString *formatString = nil;
    
    for(i = 0; i < self.totalPageCount ; i++){
        
        NSInteger centerIndex = self.totalPageCount / 2;
        
        
        CGFloat viewWidth = self.pageDotContainer.width;
        
        CGFloat originX = (viewWidth - (self.totalPageCount * DOT_SPACE)) / 2;
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(originX + (i * DOT_SPACE),
                                                                      0.0f,
                                                                      DOT_SPACE,
                                                                      5.0f)];
        button.translatesAutoresizingMaskIntoConstraints = NO;
        button.tag = (i+1);
        [button addTarget:self
                   action:@selector(pageControlButtonTouched:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setImage:[UIImage imageNamed:@"point_slide_off"]
                forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"point_slide_on"]
                forState:UIControlStateSelected];

        [button addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"[button(==%f)]",DOT_SPACE]
                                                                                      options:0
                                                                                      metrics:nil
                                                                                        views:@{@"button":button}]];
        [button addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[button(==%f)]",5.0f]
                                                                                      options:0
                                                                                      metrics:nil
                                                                                        views:@{@"button":button}]];
        
        [self.pageDotContainer addSubview:button];

        
        if(centerIndex % 2 == 0){ // even
            if(i == centerIndex) {
                
                [self.pageDotContainer addConstraint:
                 [NSLayoutConstraint constraintWithItem:button
                                              attribute:NSLayoutAttributeCenterX
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:self.pageDotContainer
                                              attribute:NSLayoutAttributeCenterX
                                             multiplier:1
                                               constant:0]];
                [self.pageDotContainer addConstraint:
                 [NSLayoutConstraint constraintWithItem:button
                                              attribute:NSLayoutAttributeCenterY
                                              relatedBy:NSLayoutRelationEqual
                                                 toItem:self.pageDotContainer
                                              attribute:NSLayoutAttributeCenterY
                                             multiplier:1
                                               constant:0]];
                
                //Center - (DOT_SPACE's Center / 2)
//                formatString = @"[button(=12.0)]";
                
                
            } else if(i == centerIndex) {
                //Center + (DOT_SPACE's Center / 2)
            } else if(i < centerIndex) {
                //trailing
            } else {
                //leading
            }
        } else { // odd
            if(i == centerIndex) {

                NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:button
                                                                              attribute:NSLayoutAttributeCenterX
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:self.pageDotContainer
                                                                              attribute:NSLayoutAttributeCenterX
                                                                             multiplier:1
                                                                               constant:0];
                [self.pageDotContainer addConstraint:constraint];
                
                constraint = [NSLayoutConstraint constraintWithItem:button
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.pageDotContainer
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1
                                                           constant:0];
                [self.pageDotContainer addConstraint:constraint];
                
                
            } else if(i < centerIndex) {
                //trailing
                if(prevButton != nil){
                    formatString = @"[prevButton]-0-[button]";
                }
                
                
            } else { //if(i > centerIndex)
                //leading
                if(prevButton != nil){
                    formatString = @"[prevButton]-0-[button]";
                }
            }
        }
        
        if(prevButton != nil){
            [self.pageDotContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[prevButton]-0-[button]"
                                                                                          options:0
                                                                                          metrics:nil
                                                                                            views:@{
                                                                                                    @"button":button,
                                                                                                    @"prevButton":prevButton
                                                                                                    }]];
            formatString = nil;
        }
        
        if(prevButton != nil){
            [self.pageDotContainer addConstraint:[NSLayoutConstraint constraintWithItem:button
                                                                              attribute:NSLayoutAttributeCenterY
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:prevButton
                                                                              attribute:NSLayoutAttributeCenterY
                                                                             multiplier:1
                                                                               constant:0]];
        }
        
        if(i == 0){
            button.selected = YES;
            self.selectedPageIndex = 1;
        }
        
        prevButton = button;
    }
    
    [self.pageDotContainer updateConstraints];
    [self.pageDotContainer setNeedsDisplay];
    [self.pageDotContainer layoutIfNeeded];
    [self.pageDotContainer layoutSubviews];
    
    
    
    
//    PCLog(@"self.shortcutScrollView.height %f", self.shortcutScrollView.height);
    self.shortcutContentWidth = xOrigin;
    [self.shortcutScrollView setContentSize:CGSizeMake(xOrigin, (self.shortcutScrollView.height - 5.0f))];
    CGRect backgroundFrame = backgroundView.frame;
    backgroundFrame.size = self.shortcutScrollView.size;
    backgroundFrame.origin.x = 0;
    backgroundFrame.origin.y = 0;
    backgroundView.frame = backgroundFrame;
}

- (void)viewDidLayoutSubviews
{
    self.shortcutScrollView.contentSize = CGSizeMake(self.shortcutContentWidth, self.shortcutScrollView.bounds.size.height  - 5.0f);
    if(self.isNeedToAddTableFooter){
        [self addFooterIfNeeded];
        self.needToAddTableFooter = NO;
    }
    
    self.viewDidLayoutOnce = YES;
    
    [self.view layoutIfNeeded];
}

- (void)shortcutButtonTouched:(UIButton *)sender
{
    NSInteger selectedSection = sender.tag;

    CGRect sectionRect = [self.tableView rectForSection:selectedSection];
    sectionRect.size.height = self.tableView.frame.size.height;
    
    self.holdSpyScrolling = YES;
    [self.tableView scrollRectToVisible:sectionRect animated:YES];
    [self performSelector:@selector(autoScrollCompletion)
               withObject:nil
               afterDelay:0.33f];
    // !!!: Never change delay time to shorter amount than 0.30f
    // because scrollRectToVisible animation delayed over 0.30f
    // So for preventing scrollViewDid called automatically
}

- (void)autoScrollCompletion
{
    self.holdSpyScrolling = NO;
    [self scrollViewDidScroll:self.tableView];
}

- (void)pageControlButtonTouched:(UIButton *)sender
{
    [self.shortcutScrollView setContentOffset:CGPointMake((sender.tag - 1) * self.widthPerPage, 0.0f)
                                     animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // !!!: Don't remove this becuase of duplicated in reloadData. -> reload data is called by condition
    [self drawShortcut];
    self.shortcutScrollView.decelerationRate = 0.0f;
    
    if(self.isNeedToBeReloaded){
        [self reloadData];
    }
    
    self.tableView.tableHeaderView = nil;
    [self.restaurantSummaryView drawMerchant:ORDER.merchant
                                 forceHeight:YES
                                  isDelivery:(self.initialCartType == CartTypeDelivery)
                                   showPromo:YES
                               showRoService:(self.initialCartType == CartTypeDelivery)
                                    cartType:self.initialCartType];
    
    self.tableView.tableHeaderView = self.restaurantSummaryView;
    self.tableView.separatorInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    
}

- (void)addFooterIfNeeded
{
    if([[self menuList] count] < 2 && [[self menuList] count] > 0){
        self.tableView.tableFooterView = nil;
    } else {
        MenuCategory *menuCategory = [[self menuList] lastObject];
        
        CGFloat heightSumOfItems = 0.0f;
        
        if([menuCategory.name length] > 0){
            heightSumOfItems += 28.0f;
        }
        
        for(id item in menuCategory.items){
        
            if([item isKindOfClass:[MenuSubCategory class]]){
                MenuSubCategory *subCategory = (MenuSubCategory *)item;
                
                if(subCategory.cellHeight == NSNotFound){
                    self.offScreenMenuSubCategoryCell.bounds = CGRectMake(0.0f, 0.0f,
                                                                          CGRectGetWidth([UIScreen mainScreen].applicationFrame),
                                                                          CGRectGetHeight(self.offScreenMenuSubCategoryCell.bounds));
                    [self.offScreenMenuSubCategoryCell updateConstraints];
                    self.offScreenMenuSubCategoryCell.subCategory = subCategory;
                    subCategory.cellHeight = [self.offScreenMenuSubCategoryCell cellHeight] + 1.0f;
                }
                
                heightSumOfItems += subCategory.cellHeight;
                
            } else {
//                MenuItem *castedMenuItem = (MenuItem *)item;
//                if(castedMenuItem.cellHeight == 0.0f){
//                    [self.offScreenMenuItemCell setItemForHeight:castedMenuItem];
//                    castedMenuItem.cellHeight = [self.offScreenMenuItemCell cellHeight];
//                }
                heightSumOfItems += 81.0f;
            }
            
            if(heightSumOfItems >= self.tableView.height){
                break;
            }
        }
        
        CGFloat expectedHeight = 0.0f;
        
        if(heightSumOfItems < self.tableView.height){
            self.tableView.tableFooterView = nil;
            expectedHeight = (self.tableView.height - heightSumOfItems);
            expectedHeight = MAX((expectedHeight + 20.0f), 60.0f);
            self.tableFooterView.height = expectedHeight;
            self.tableView.tableFooterView = self.tableFooterView;
        } else {
            self.tableView.tableFooterView = nil;
        }
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(!self.didAppeared){
        if(ORDER.merchant != nil){
            
            self.didAppeared = YES;
            
            if([[self menuList] count] == 0 && MENUPAN.menuFetched){
                self.tableView.tableFooterView = self.noItemView;
                self.noMenuInfoLabel.text = NSLocalizedString(@"This restaurant may be configuring its menu so you cannot order right now.", nil);
            }
            
            if(self.firstMatchingIndexPath != nil){
//                [self.tableView scrollToRowAtIndexPath:self.firstMatchingIndexPath
//                                      atScrollPosition:UITableViewScrollPositionMiddle
//                                              animated:YES];
//                
                [self performSelector:@selector(showDetailItem)
                           withObject:nil
                           afterDelay:0.1f];
            }
            
        } else {
        }
    }
}

- (void)showDetailItem
{
    [self.tableView selectRowAtIndexPath:self.firstMatchingIndexPath
                                animated:YES
                          scrollPosition:UITableViewScrollPositionMiddle];
    [self tableView:self.tableView
didSelectRowAtIndexPath:self.firstMatchingIndexPath];
}


- (void)viewDidUnload
{
    [self setTableView:nil];
    [self setNoItemView:nil];
    [self setNoMenuInfoLabel:nil];
    [self setGotoRequestBillButton:nil];
    [super viewDidUnload];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableView Delegate & Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return [[self menuList] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    MenuCategory *menuCategory = [[self menuList] objectAtIndex:section];
    return [menuCategory.items count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MenuCategorySectionView *view = [MenuCategorySectionView viewWithNibName:MENU_REL_XIB_NAME
                                                                     atIndex:2];
    MenuCategory *menuCategory = [[self menuList] objectAtIndex:section];
    view.button.buttonTitle = menuCategory.name;
    if([menuCategory.name length] == 0){
        return nil;
    } else {
        return view;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCategory *menuCategory = [[self menuList] objectAtIndex:indexPath.section];
    id item = [menuCategory.items objectAtIndex:indexPath.row];
    
    if([item isKindOfClass:[MenuSubCategory class]]){
        MenuSubCategory *subCategory = (MenuSubCategory *)item;
        if([subCategory.name length] > 0){
            
            if(subCategory.cellHeight == NSNotFound){
                
                self.offScreenMenuSubCategoryCell.bounds = CGRectMake(0.0f, 0.0f,
                                                                      CGRectGetWidth([UIScreen mainScreen].applicationFrame),
                                                                      CGRectGetHeight(self.offScreenMenuSubCategoryCell.bounds));
                [self.offScreenMenuSubCategoryCell updateConstraints];
                
                self.offScreenMenuSubCategoryCell.subCategory = subCategory;
                
                subCategory.cellHeight = [self.offScreenMenuSubCategoryCell cellHeight];
            }
            
//            PCLog(@"Sub height : %f", subCategory.cellHeight);
            return subCategory.cellHeight;
            
            
        } else {
            return 0.0f;
        }
    } else {
//        MenuItem *castedMenuItem = (MenuItem *)item;
//        if(castedMenuItem.cellHeight == 0.0f){
//            [self.offScreenMenuItemCell setItemForHeight:castedMenuItem];
//            castedMenuItem.cellHeight = [self.offScreenMenuItemCell cellHeight];
//        }
//
//        return castedMenuItem.cellHeight;
        return 81.0f;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    MenuCategory *menuCategory = [[self menuList] objectAtIndex:section];
    
    if([menuCategory.name length] == 0){
        return 0.0f;
    } else {
        return 28.0f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    MenuItemType itemType = MenuItemTypeItem;
    
    MenuCategory *menuCategory = [[self menuList] objectAtIndex:indexPath.section];
    id item = [menuCategory.items objectAtIndex:indexPath.row];
    
    if([item isKindOfClass:[MenuSubCategory class]]){
        itemType = MenuItemTypeSubCategory;
    }
    
    switch(itemType){
        case MenuItemTypeItem:
            return [self tableView:tableView
     menuItemCellForRowAtIndexPath:indexPath
                      withMenuItem:item];
            break;
        case MenuItemTypeSubCategory:
            return [self tableView:tableView
menuSubCategoryCellForRowAtIndexPath:indexPath
               withMenuSubCategory:item];
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView menuItemCellForRowAtIndexPath:(NSIndexPath *)indexPath withMenuItem:(MenuItem *)item
{
    NSString *cellIdentifier = @"MenuItemCell";
    
    MenuItemCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        cell = [MenuItemCell cell];
    }
    
    cell.item = item;
    
//    if([self.referredMenuIds containsObject:[NSNumber numberWithSerial:item.menuItemNo]] ||
//       [self.referredMenuNames containsObject:item.name]){
//        cell.menuNameLabel.textColor = [UIColor blueColor];
//    } else {
//        cell.menuNameLabel.textColor = [UIColor blackColor];
//    }
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView menuSubCategoryCellForRowAtIndexPath:(NSIndexPath *)indexPath withMenuSubCategory:(MenuSubCategory *)subCategory
{
    NSString *cellIdentifier = @"MenuSubCategoryCell";
    
    MenuSubCategoryCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        cell = [MenuSubCategoryCell cellWithNibName:MENU_REL_XIB_NAME
                                            atIndex:1];
        cell.menuSubCategoryNameLabel.preferredMaxLayoutWidth = cell.menuSubCategoryNameLabel.width;
    }
    cell.subCategory = subCategory;
    
    [cell.contentView setNeedsUpdateConstraints];
    [cell.contentView updateConstraintsIfNeeded];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCategory *menuCategory = [[self menuList] objectAtIndex:indexPath.section];
    id item = [menuCategory.items objectAtIndex:indexPath.row];
    
    if([item isKindOfClass:[MenuItem class]]){
        MenuItem *menuItem = item;
        
        self.selectedIndexPath = indexPath;
        
        MenuDetailViewController *viewController = [MenuDetailViewController viewControllerFromNib];
        viewController.item = menuItem;
        [viewController.item resetAllOptions];
        viewController.delegate = self;
        viewController.indexPath = indexPath;
        
        [self presentViewControllerInNavigation:viewController
                                       animated:YES
                                     completion:NULL];
        
#if FLURRY_ENABLED
        [Flurry logEvent:@"See Menu Detail" withParameters:@{@"RestaurantName":ORDER.merchant.name,
                                                             @"RestaurantId":[NSNumber numberWithSerial:ORDER.merchant.merchantNo],
                                                             @"MenuName":menuItem.name}];
#endif
    }
}


- (void)cell:(MenuItemCell *)cell didTouchedAddToCartButtonAtIndexPath:(NSIndexPath *)indexPath
{
    [self addMenuItemAtIndexPath:indexPath];
}

- (void)addMenuItemAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCategory *menuCategory = [[self menuList] objectAtIndex:indexPath.section];
    id item = [menuCategory.items objectAtIndex:indexPath.row];
    
    if([item isKindOfClass:[MenuItem class]]){
        
        MenuItem *menuItem = item;
        
        if(!ORDER.merchant.isOpenHour){
            [UIAlertView alertWithTitle:NSLocalizedString(@"Sorry, restaurant is currently closed!", nil)
                                message:NSLocalizedString(@"Please feel free to browse the menu, but ordering isn't available until the restaurant opens.", nil)
                      cancleButtonTitle:NSLocalizedString(@"Got it!", nil)];
      
            return;
        }
        
        if(ORDER.cart.cartType == CartTypeDelivery ||
           (ORDER.order.isEditable && ORDER.order.orderType == OrderTypeDelivery)){
            if(!ORDER.merchant.isDeliveryHour){
                
                NSString *hourString = [ORDER.merchant nextDeliveryAvailableTime];
                [UIAlertView alertWithTitle:NSLocalizedString(@"Delivery is Currently Unavailable", nil)
                                    message:[NSString stringWithFormat:NSLocalizedString(@"Next Available Time:\n%@", nil), [hourString length] > 0 ? hourString : NSLocalizedString(@"N/A", nil)]];
                
                return;
            }
        }
        
        MenuPan *menuPan = [MENUPAN.activeMenuList objectAtIndex:self.menuPanIndex];
        
        if(!menuPan.isMenuHour){
            NSString *hourString = [menuPan nextAvailableTime];
            [UIAlertView alertWithTitle:NSLocalizedString(@"Menu Item Currently Unavailable", nil)
                                message:[NSString stringWithFormat:NSLocalizedString(@"Next Available Time:\n%@", nil), [hourString length] > 0 ? hourString : NSLocalizedString(@"N/A", nil)]];
            return;
        }

        self.selectedIndexPath = indexPath;
        
        if([menuItem.optionGroups count] > 0 || [menuItem.prices count] > 1){
            MenuDetailViewController *viewController = [MenuDetailViewController viewControllerFromNib];
            viewController.item = item;
            [viewController.item resetAllOptions];
            viewController.delegate = self;
            viewController.indexPath = indexPath;
            
            [self presentViewControllerInNavigation:viewController
                                           animated:YES
                                         completion:NULL];
        } else {
            if(ORDER.order.isEditable){
                [self addMenuItemToOrder:menuItem
                                quantity:1
                     specialInstructions:nil];
            } else {
                [self addMenuItem:menuItem
                         quantity:1
              specialInstructions:nil];
            }
        }
    }
}


- (void)cell:(MenuItemCell *)cell didTouchedPhotoButtonAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCategory *menuCategory = [[self menuList] objectAtIndex:indexPath.section];
    id item = [menuCategory.items objectAtIndex:indexPath.row];
    
    if([item isKindOfClass:[MenuItem class]]){
        MenuPhotoViewController *initialViewController = [MenuPhotoViewController viewControllerFromNib];
        initialViewController.item = item;
        initialViewController.indexPath = indexPath;
        initialViewController.category = menuCategory;
        initialViewController.delegate = self;
        if(cell == nil) initialViewController.shouldMenuDetailPop = YES;
        
        [self.photoPageViewController setViewControllers:@[initialViewController]
                                               direction:UIPageViewControllerNavigationDirectionForward
                                                animated:YES
                                              completion:^(BOOL finished) {
                                                  
                                              }];
        [self presentViewController:self.photoPageViewController
                           animated:YES
                         completion:^{}];
    }
}

#pragma mark - MenuDetailViewControllerDelegate
- (void)menuDetailViewController:(MenuDetailViewController *)viewController
                  didNewMenuItem:(MenuItem *)menuItem
                        quantity:(NSInteger)quantity
             specialInstructions:(NSString *)specialInstructions
{
    [self.tableView deselectRowAtIndexPath:self.selectedIndexPath
                                  animated:YES];
    
    if(ORDER.order.isEditable){
        [self addMenuItemToOrder:menuItem
                        quantity:quantity
             specialInstructions:specialInstructions];
    } else {
        [self addMenuItem:menuItem
                 quantity:quantity
      specialInstructions:specialInstructions];
    }
}

- (void)menuDetailViewControllerDidCancelTouched:(MenuDetailViewController *)viewController
{
    [self.tableView deselectRowAtIndexPath:self.selectedIndexPath
                                  animated:YES];
}

- (void)menuDetailViewControllerDidTouchedPhoto:(MenuDetailViewController *)viewController
                                    atIndexPath:(NSIndexPath *)indexPath
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [self cell:nil didTouchedPhotoButtonAtIndexPath:indexPath];
                             }];
}

- (void)addMenuItemToOrder:(MenuItem *)menuItem quantity:(NSInteger)quantity specialInstructions:(NSString *)specialInstructions
{

    if(!ORDER.merchant.isOpenHour){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Sorry, restaurant is currently closed!", nil)
                            message:NSLocalizedString(@"Please feel free to browse the menu, but ordering isn't available until the restaurant opens.", nil)
                  cancleButtonTitle:NSLocalizedString(@"Got it!", nil)];
        return;
    }
    
    if(ORDER.order.orderType == OrderTypeDelivery){
        if(!ORDER.merchant.isDeliveryHour){
            NSString *hourString = [ORDER.merchant nextDeliveryAvailableTime];
            [UIAlertView alertWithTitle:NSLocalizedString(@"Delivery is Currently Unavailable", nil)
                                message:[NSString stringWithFormat:NSLocalizedString(@"Next Available Time:\n%@", nil), [hourString length] > 0 ? hourString : NSLocalizedString(@"N/A", nil)]];
            return;
        }
    }
    
    MenuPan *menuPan = [MENUPAN.activeMenuList objectAtIndex:self.menuPanIndex];
    if(!menuPan.isMenuHour){
        NSString *hourString = [menuPan nextAvailableTime];
        [UIAlertView alertWithTitle:NSLocalizedString(@"Menu Item Currently Unavailable", nil)
                            message:[NSString stringWithFormat:NSLocalizedString(@"Next Available Time:\n%@", nil), [hourString length] > 0 ? hourString : NSLocalizedString(@"N/A", nil)]];
        return;
    }

    
#if FLURRY_ENABLED
            [Flurry logEvent:@"Add Menu for Adjusting" withParameters:@{@"RestaurantName":ORDER.merchant.name,
                                                          @"RestaurantId":[NSNumber numberWithSerial:ORDER.merchant.merchantNo],
                                                          @"MenuName":menuItem.name}];
#endif
            
    [self setLockReloadBadge:YES];
    [ORDER.order addMenuItem:menuItem
                    quantity:quantity
         specialInstructions:specialInstructions
                     success:^(){
                         
                         self.selectedMenuItem = nil;
                         self.selectedQuantity = 0;
                         self.specialInstructions = nil;
                         
                         [self cartAddAnimate];
                         
                     } failure:^{
                         [self resetSelection];
                         [self setLockReloadBadge:NO];
                     }];
}


- (void)addMenuItem:(MenuItem *)menuItem quantity:(NSInteger)quantity specialInstructions:(NSString *)specialInstructions
{

    if(!ORDER.merchant.isOpenHour){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Sorry, restaurant is currently closed!", nil)
                            message:NSLocalizedString(@"Please feel free to browse the menu, but ordering isn't available until the restaurant opens.", nil)
                  cancleButtonTitle:NSLocalizedString(@"Got it!", nil)];
        return;
    }
    
    if(ORDER.cart.cartType == CartTypeDelivery){
        if(!ORDER.merchant.isDeliveryHour){
            
            NSString *hourString = [ORDER.merchant nextDeliveryAvailableTime];
            [UIAlertView alertWithTitle:NSLocalizedString(@"Delivery is Currently Unavailable", nil)
                                message:[NSString stringWithFormat:NSLocalizedString(@"Next Available Time:\n%@", nil), [hourString length] > 0 ? hourString : NSLocalizedString(@"N/A", nil)]];
            return;
        }
    }
    
    MenuPan *menuPan = [MENUPAN.activeMenuList objectAtIndex:self.menuPanIndex];
    
    if(!menuPan.isMenuHour){
        NSString *hourString = [menuPan nextAvailableTime];
        [UIAlertView alertWithTitle:NSLocalizedString(@"Menu Item Currently Unavailable", nil)
                            message:[NSString stringWithFormat:NSLocalizedString(@"Next Available Time:\n%@", nil), [hourString length] > 0 ? hourString : NSLocalizedString(@"N/A", nil)]];
        return;
    }

    switch(ORDER.cart.cartType){
        case CartTypeUnknown:
            if(ORDER.merchant.isTableBase){
                
                // Table base needs to send server per each item
                // So, check ahead for adding item
                // But, Ticket base needs not to send server, So, check availability for order lately (at making checkout)
                if(self.initialCartType == CartTypeTakeout || self.initialCartType == CartTypeDelivery){
                    
                    ORDER.cart.cartType = self.initialCartType;
                    
                    [self addMenuItem:menuItem
                             quantity:quantity
                  specialInstructions:specialInstructions];
                    
                } else {
                    
                    if(ORDER.tableInfo == nil){
                        
                        self.selectedMenuItem = menuItem;
                        self.selectedQuantity = quantity;
                        self.specialInstructions = specialInstructions;
                        
                        [self.delegate menuCellControllerShowTableNumber:self];
                        
                    } else {
                        if(ORDER.cart.status != CartStatusOrdering){
                            [UIAlertView alertWithTitle:NSLocalizedString(@"Order list is not editable", nil)
                                                message:NSLocalizedString(@"Order list has been submitted. To add/modify/remove orders, you have to postpone the confirmation of your order by \"Change Submitted Order\" button", nil)
                                               delegate:self
                                                    tag:100];
                            [self resetSelection];
                            return;
                        }
                        
                        [self setLockReloadBadge:YES];
                    
#if FLURRY_ENABLED
                        [Flurry logEvent:@"Add Menu" withParameters:@{@"RestaurantName":ORDER.merchant.name,
                                                                      @"RestaurantId":[NSNumber numberWithSerial:ORDER.merchant.merchantNo],
                                                                      @"MenuName":menuItem.name}];
#endif
                        
                        [ORDER.cart addMenuItem:menuItem
                                       quantity:quantity
                            specialInstructions:specialInstructions
                                        success:^(BOOL isOnlyLocal){
                                            if(isOnlyLocal)
                                                TRANS.localDirty = YES;
                                            else
                                                TRANS.dirty = YES;
                                            self.selectedMenuItem = nil;
                                            self.selectedQuantity = 0;
                                            self.specialInstructions = nil;

                                            [self cartAddAnimate];
                                            
                                            
                                        } failure:^{
                                            [self resetSelection];

                                            [self setLockReloadBadge:NO];
                                            if(ORDER.merchant.isTableBase){
                                                [ORDER checkTable:ORDER.tableInfo
                                                          success:^{
                                                              
                                                          }
                                                          failure:^(OrderStatus status){
                                                              
                                                          }];
                                            }
                                        }];
                    }
                }
            } else {
                // Ticket base
                // Take out only restaurant type should not be table base restaurant (accept_ticket-accept_togo)
                switch(ORDER.merchant.servicedBy){
                    case ServicedBySelf:
                        
                        ORDER.cart.cartType = self.initialCartType;
                        [self addMenuItem:menuItem
                                 quantity:quantity
                      specialInstructions:specialInstructions];
                        
                        break;
                    case ServicedByStaff:

                        ORDER.cart.cartType = self.initialCartType;
                        
                            [self addMenuItem:menuItem
                                     quantity:quantity
                          specialInstructions:specialInstructions];

                        
                        break;
                    case ServicedByNone:
                        
                        if(self.initialCartType != CartTypeTakeout
                           && self.initialCartType != CartTypeDelivery){
                            PCError(@"Initial CartType must CartTypeTakeout2 or CartTypeDelivery2");
                            self.initialCartType = CartTypeTakeout; //Temporary
                        }
                        
                        ORDER.cart.cartType = self.initialCartType;
                        [self addMenuItem:menuItem
                                 quantity:quantity
                      specialInstructions:specialInstructions];
                        
                        break;
                    default:
                        PCError(@"Serviced by flag is unknown");
                        break;
                        
                }
                
            }
            
            break;
        case CartTypeCart:
        {
            if(ORDER.cart.status != CartStatusOrdering){
                [UIAlertView alertWithTitle:NSLocalizedString(@"Order list is not editable", nil)
                                    message:NSLocalizedString(@"Order list has been placed. To add/modify/remove orders, you have to postpone the confirmation of your order by \"Change Placed Order\" button", nil)
                                   delegate:self
                                        tag:100];
                [self resetSelection];
                return;
            }
            
#if FLURRY_ENABLED
            [Flurry logEvent:@"Add Menu" withParameters:@{@"RestaurantName":ORDER.merchant.name,
                                                          @"RestaurantId":[NSNumber numberWithSerial:ORDER.merchant.merchantNo],
                                                          @"MenuName":menuItem.name}];
#endif
            
            [self setLockReloadBadge:YES];
            [ORDER.cart addMenuItem:menuItem
                           quantity:quantity
                specialInstructions:specialInstructions
                            success:^(BOOL isOnlyLocal){
                                if(isOnlyLocal)
                                    TRANS.localDirty = YES;
                                else
                                    TRANS.dirty = YES;
                                self.selectedMenuItem = nil;
                                self.selectedQuantity = 0;
                                self.specialInstructions = nil;
                                
                                [self cartAddAnimate];
                                
                            } failure:^{
                                [self resetSelection];

                                [self setLockReloadBadge:NO];
                                
                                if(ORDER.merchant.isTableBase){
                                    [ORDER checkTable:ORDER.tableInfo
                                              success:^{
                                                  
                                              }
                                              failure:^(OrderStatus status){
                                                  
                                              }];
                                }
                            }];
        }
            break;
        case CartTypePickup:
        case CartTypeTakeout:
        case CartTypePickupServiced:
        case CartTypeDelivery:
        {
#if FLURRY_ENABLED
            [Flurry logEvent:@"Add Menu" withParameters:@{@"RestaurantName":ORDER.merchant.name,
                                                          @"RestaurantId":[NSNumber numberWithSerial:ORDER.merchant.merchantNo],
                                                          @"MenuName":menuItem.name}];
#endif
            
            [self setLockReloadBadge:YES];
            [ORDER.cart addMenuItem:menuItem
                           quantity:quantity
                specialInstructions:specialInstructions
                            success:^(BOOL isOnlyLocal){
                                if(isOnlyLocal)
                                    TRANS.localDirty = YES;
                                else
                                    TRANS.dirty = YES;
                                self.selectedMenuItem = nil;
                                self.selectedQuantity = 0;
                                self.specialInstructions = nil;
                                
                                [self cartAddAnimate];
                                
                            } failure:^{
                                [self resetSelection];
                                [self setLockReloadBadge:NO];
                            }];
            //            }
        }
            break;
        default:
            break;
    }
}

- (void)addMenuItemAsSelectedPrevious
{
    [self addMenuItem:self.selectedMenuItem
             quantity:self.selectedQuantity
  specialInstructions:self.specialInstructions];
}

- (void)cartAddAnimate
{
    MenuItemCell *cell = (MenuItemCell *)[self.tableView cellForRowAtIndexPath:self.selectedIndexPath];
    
    [self.delegate menuCellController:self
               cartAddAnimateWithCell:cell];
}

- (void)setLockReloadBadge:(BOOL)value
{
    [self.delegate menuCellController:self
                      lockReloadBadge:value];
}

- (void)resetSelection
{
    self.selectedMenuItem = nil;
    self.selectedQuantity = 0;
    self.selectedIndexPath = nil;
    self.specialInstructions = nil;
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 100:
//            [APP.viewDeckController openRightViewAnimated:YES];
            break;
        default:
            break;
    }
}



#pragma mark - UIPageViewControllerDelegate 
#pragma mark optional
- (void)pageViewController:(UIPageViewController *)pageViewController
willTransitionToViewControllers:(NSArray *)pendingViewControllers
{
}

- (void)pageViewController:(UIPageViewController *)pageViewController
        didFinishAnimating:(BOOL)finished
   previousViewControllers:(NSArray *)previousViewControllers
       transitionCompleted:(BOOL)completed
{
}

//- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController
//                   spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation
//{
//    return UIPageViewControllerSpineLocationMax;
//}

#pragma mark - UIPageViewControllerDataSource
#pragma mark required
- (MenuPhotoViewController *)pageViewController:(UIPageViewController *)pageViewController
             viewControllerBeforeViewController:(MenuPhotoViewController *)viewController
{
    return [self viewControllerBy:-1
                      atIndexPath:viewController.indexPath];
}

- (MenuPhotoViewController *)pageViewController:(UIPageViewController *)pageViewController
              viewControllerAfterViewController:(MenuPhotoViewController *)viewController
{
    return [self viewControllerBy:1
                      atIndexPath:viewController.indexPath];
}

- (MenuPhotoViewController *)viewControllerBy:(NSInteger)direction
                                  atIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *newIndexPath = nil;
    
    MenuCategory *menuCategory = nil;
    id item = nil;
    
    menuCategory = [[self menuList] objectAtIndex:indexPath.section];
    
    if(direction > 0){
        if([menuCategory.items count] > (indexPath.row + 1)){
            //Next row
            newIndexPath = [NSIndexPath indexPathForRow:(indexPath.row + 1)
                                              inSection:indexPath.section];
        } else if([[self menuList] count] > (indexPath.section + 1)){
            //Next section's first row
            menuCategory = [[self menuList] objectAtIndex:(indexPath.section + 1)];
            newIndexPath = [NSIndexPath indexPathForRow:0
                                              inSection:(indexPath.section + 1)];
        }
    } else if(direction < 0){
        if(indexPath.row > 0){
            //Next row
            newIndexPath = [NSIndexPath indexPathForRow:(indexPath.row - 1)
                                              inSection:indexPath.section];
        } else if(indexPath.section > 0){
            //Next section's first row
            menuCategory = [[self menuList] objectAtIndex:(indexPath.section - 1)];
            newIndexPath = [NSIndexPath indexPathForRow:([menuCategory.items count] - 1)
                                              inSection:(indexPath.section - 1)];
        }
    }
    
    if(newIndexPath != nil){
        item = [menuCategory.items objectAtIndex:newIndexPath.row];
        
        MenuPhotoViewController *newViewController = [MenuPhotoViewController viewControllerFromNib];
        newViewController.indexPath = newIndexPath;
        newViewController.item = item;
        newViewController.category = menuCategory;
        newViewController.delegate = self;
        return newViewController;

    } else {
        return nil;
    }
}


#pragma mark optional
//- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
//{
//    return 10;
//}
//
//- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
//{
//    return 3;
//}

#pragma mark - PhotoViewControllerDelegate
- (void)menuPhotoViewController:(MenuPhotoViewController *)viewController
didSelectAddItemButtonAtIndexPath:(NSIndexPath *)indexPath
{
    [self addMenuItemAtIndexPath:indexPath];
}

- (void)menuPhotoViewController:(MenuPhotoViewController *)viewController
 didTouchCloseButtonAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 if(indexPath != nil){
                                     [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
                                 } else {
                                     [self.tableView deselectRowAtIndexPath:self.selectedIndexPath
                                                                   animated:YES];
                                 }
                             }];
}

- (void)menuPhotoViewController:(MenuPhotoViewController *)viewController
   didTouchDescLabelAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 if(indexPath != nil){
                                     
                                     MenuCategory *menuCategory = [[self menuList] objectAtIndex:indexPath.section];
                                     id item = [menuCategory.items objectAtIndex:indexPath.row];
                                     
                                     MenuDetailViewController *viewController = [MenuDetailViewController viewControllerFromNib];
                                     viewController.item = item;
                                     [viewController.item resetAllOptions];
                                     viewController.delegate = self;
                                     viewController.indexPath = indexPath;
                                     
                                     [self presentViewControllerInNavigation:viewController
                                                                    animated:YES
                                                                  completion:NULL];
                                     
                                     
                                 } else {
                                     [self.tableView deselectRowAtIndexPath:self.selectedIndexPath
                                                                   animated:YES];
                                 }
                             }];
}

- (MenuItemCell *)offScreenMenuItemCell
{
    if(_offScreenMenuItemCell == nil){
        _offScreenMenuItemCell = [MenuItemCell cell];
    }
    return _offScreenMenuItemCell;
}

- (MenuSubCategoryCell *)offScreenMenuSubCategoryCell
{
    if(_offScreenMenuSubCategoryCell == nil){
        _offScreenMenuSubCategoryCell = [MenuSubCategoryCell cellWithNibName:MENU_REL_XIB_NAME
                                                                     atIndex:1];
//        [_offScreenMenuSubCategoryCell.contentView updateConstraints];
    }
    return _offScreenMenuSubCategoryCell;
}

#pragma mark - UIScrollViewDelegate


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView == self.shortcutScrollView){
        
        NSUInteger currentPageIndex = (NSUInteger)floor((scrollView.contentOffset.x + (scrollView.width / 2)) / self.widthPerPage);
        currentPageIndex++;
        
        if(self.selectedPageIndex != currentPageIndex){
            self.selectedPageIndex = currentPageIndex;
            for(UIButton *dotButton in self.pageDotContainer.subviews){
                if(dotButton.tag > 0){
                    dotButton.selected = (dotButton.tag == self.selectedPageIndex);
                }
            }
        }
    } else if(scrollView == self.tableView){
        
        if(self.shortcutScrollView.hidden || self.holdSpyScrolling){
            return;
        }
        
        CGPoint offset = scrollView.contentOffset;
        
        offset.y += 3.0f;
        offset.x = 120.0f;
        
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:offset];
        
        if(indexPath == nil){
            offset.y += 26.0f;
            indexPath = [self.tableView indexPathForRowAtPoint:offset];
        }

        if(self.selectedSectionIndex != indexPath.section){
            self.selectedSectionIndex = indexPath.section;
            
            NSArray *subviews = [self.shortcutScrollView subviews];
            
            UIButton *sectionButtonView = nil;
            UIButton *selectedButtonView = nil;
            for(UIButton *aButton in subviews){
                if(aButton.tag >= 0){ //Only Button
                    if(aButton.tag == self.selectedSectionIndex){
                        sectionButtonView = aButton;
                    }
                    if(aButton.selected){
                        selectedButtonView = aButton;
                    }
                }
            }
            
//            CGRect backFrame = self.selectedSectionBackground.frame;
//            backFrame.origin.x = sectionButtonView.x + SEL_BACK_IMAGEVIEW_MARGIN;
//            backFrame.origin.y = IMG_BACK_ORIGIN_Y;
//            backFrame.size.width = sectionButtonView.width - (SEL_BACK_IMAGEVIEW_MARGIN * 2);
            
            [UIView animateWithDuration:0.3f
                                  delay:0.0f
                 usingSpringWithDamping:0.8f
                  initialSpringVelocity:0.3f
                                options:UIViewAnimationOptionBeginFromCurrentState
                             animations:^{
                                 
                                 self.selectMarkLeadingConstraint.constant = sectionButtonView.x + SEL_BACK_IMAGEVIEW_MARGIN;
//                                 self.selectedSectionBackground.frame = backFrame;
//                                 [self.selectedSectionBackground drawBorder];
                                 self.selectedSectionBgWidthConstraint.constant = sectionButtonView.width - (SEL_BACK_IMAGEVIEW_MARGIN * 2);
                                 
                                 [self.selectedSectionBackground layoutIfNeeded];
                                 
//                                 PCLog(@"self.selectedSectionBgWidthConstraint %@", self.selectedSectionBgWidthConstraint);
//                                 PCLog(@"self.selectedSectionBackground %@", self.selectedSectionBackground);
                                 selectedButtonView.selected = NO;
                                 CGPoint curOffset = self.shortcutScrollView.contentOffset;
                                 if(sectionButtonView.x + SEL_BACK_IMAGEVIEW_MARGIN < curOffset.x){
                                     self.shortcutScrollView.contentOffset = CGPointMake(sectionButtonView.x, curOffset.y);
                                 } else if (CGRectGetMaxX(sectionButtonView.frame) > (curOffset.x + self.shortcutScrollView.width)){
                                     self.shortcutScrollView.contentOffset = CGPointMake(CGRectGetMaxX(sectionButtonView.frame) - self.shortcutScrollView.width, curOffset.y);
                                 }
                                 
                             } completion:^(BOOL finished) {
                                 if(finished){
                                     sectionButtonView.selected = YES;
                                 }
                             }];
        }
    }
}

- (void)setReferredMenus:(NSArray *)referredMenus
{
    _referredMenus = referredMenus;
    
    if(referredMenus == nil){
        self.referredMenuIds = nil;
        self.referredMenuNames = nil;
    } else {
        NSMutableArray *ids = [NSMutableArray array];
        NSMutableArray *names = [NSMutableArray array];
        for(NSDictionary *menuDict in referredMenus){
            [ids addObject:[menuDict objectForKey:@"id"]];
            [names addObject:[menuDict objectForKey:@"name"]];
        }
        self.referredMenuIds = ids;
        self.referredMenuNames = names;
    }
}

//- (RestaurantSummaryView *)restaurantSummaryView
//{
//    if(_restaurantSummaryView == nil){
//        _restaurantSummaryView = [RestaurantSummaryView viewWithNibName:@"RestaurantSummaryView"];
//        
////        NSDictionary *views = @{@"restaurantSummaryView" : _restaurantSummaryView};
////        
////        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[restaurantSummaryView]|"
////                                                                       options:0
////                                                                       metrics:nil
////                                                                         views:views];
////        [self.tableView addConstraints:constraints];
////        
////        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[restaurantSummaryView]|"
////                                                              options:0
////                                                              metrics:nil
////                                                                views:views];
////        [self.tableView addConstraints:constraints];
//        
//    }
//    
//    return _restaurantSummaryView;
//}

- (NSMutableArray *)menuList
{
    return [MENUPAN menuListAt:self.menuPanIndex];
}

@end
