//
//  RequestAreaViewController.m
//  RushOrder
//
//  Created by Conan Kim on 5/1/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import "RequestAreaViewController.h"
#import "PCPaymentService.h"
#import "PCCredentialService.h"


@interface RequestAreaViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet PCTextField *nameTextField;
@property (weak, nonatomic) IBOutlet PCTextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet PCTextField *emailTextField;
@property (weak, nonatomic) IBOutlet PCTextField *areaTextField;
@property (strong, nonatomic) CLGeocoder *geocoder;
@end

@implementation RequestAreaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = NSLocalizedString(@"Request Area", nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    CLLocation *centerLoc = nil;

    if(APP.location == nil){
        centerLoc = [[CLLocation alloc] initWithLatitude:34.0613691 longitude:-118.3012045];
    } else {
        centerLoc = APP.location;
    }
    
    self.mapView.region = MKCoordinateRegionMakeWithDistance(centerLoc.coordinate,
                                                             500,
                                                             500);
    
    if(CRED.isSignedIn){
        if([self.nameTextField.text length] == 0){
            self.nameTextField.text = CRED.customerInfo.firstName;
        }
        
        if([self.lastNameTextField.text length] == 0){
            self.lastNameTextField.text = CRED.customerInfo.lastName;
        }
        
        if([self.emailTextField.text length] == 0){
            self.emailTextField.text = CRED.customerInfo.email;
        }
    } else {
        if([self.emailTextField.text length] == 0){
            self.emailTextField.text = [NSUserDefaults standardUserDefaults].emailAddress;
        }
    }
    
    if([self.nameTextField.text length] == 0){
        self.nameTextField.text = [NSUserDefaults standardUserDefaults].custName;
    }
    
    if(APP.location != nil && [self.areaTextField.text length] == 0){
        [self.geocoder reverseGeocodeLocation:[[CLLocation alloc] initWithLatitude:APP.location.coordinate.latitude
                                                                         longitude:APP.location.coordinate.longitude]
                            completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if(error != nil){
                 PCError(@"Reverse geocding Error %@", error);
                 
                 return;
             }
             
             if([placemarks count] > 0){
                 CLPlacemark *firstPlacemark = [placemarks objectAtIndex:0];
                 
                 self.areaTextField.text = firstPlacemark.locality;
             } else {
                 //Nothing Found
             }
             
         }];
    }

}

- (IBAction)sendButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    RequestResult result = RRFail;
    
    
    if(![VALID validate:self.nameTextField
                  title:NSLocalizedString(@"Please provide your First Name.", nil)
                message:nil]){
        return;
    }
    
    if(![VALID validate:self.lastNameTextField
                  title:NSLocalizedString(@"Please provide your Last Name.", nil)
                message:nil]){
        return;
    }
    
    if(![VALID validate:self.emailTextField
                  title:NSLocalizedString(@"Please provide your E-mail Address.", nil)
                message:nil]){
        return;
    }
    
    if(![VALID validateEmail:self.emailTextField
                       title:NSLocalizedString(@"Invalid Email",nil)
                     message:NSLocalizedString(@"Please enter a valid email address", nil)]){
        return;
    }
    
    if(![VALID validate:self.areaTextField
                  title:NSLocalizedString(@"Please provide the Requested Area.", nil)
                message:nil]){
        return;
    }
    
//    NSMutableDictionary *paramDict = [NSMutableDictionary dictionary];
    /*
     "area_name=%@\
     &first_name=%@\
     &last_name=%@\
     &email=%@\
     &push_device_token=%@"
     */
    NSMutableDictionary *paramDict = [NSMutableDictionary dictionaryWithDictionary:@{@"first_name":self.nameTextField.text,
                                                                                     @"last_name":self.lastNameTextField.text,
                                                                                     @"email":self.emailTextField.text,
                                                                                     @"area_name":self.areaTextField.text}];
    NSString *deviceToken = [NSUserDefaults standardUserDefaults].deviceToken;
    if(deviceToken.length > 0){
        [paramDict setObject:deviceToken forKey:@"push_device_token"];
    }
    
    if([CRED.customerInfo.authToken length] > 0){
        [paramDict setObject:CRED.customerInfo.authToken forKey:@"auth_token"];
    }
    
    result = [PAY requestServiceArea:paramDict
                     completionBlock:
              ^(BOOL isSuccess, id response, NSInteger statusCode) {
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      
                      [self dismissViewControllerAnimated:YES
                                               completion:^{
                                                   [UIAlertView alertWithTitle:NSLocalizedString(@"Thanks for the Area Suggestion!", nil)
                                                                       message:NSLocalizedString(@"We'll notify you as soon as we begin servicing your area.", nil)];
                                               }];
                      
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        default:
            [SVProgressHUD showErrorWithStatus:NETWORK_ERROR_MESSAGE];
            break;
    }
     
     
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == self.areaTextField){
        [self sendButtonTouched:textField];
    } else {
        if ([textField isKindOfClass:[PCTextField class]]){
            [[(PCTextField *)textField nextField] becomeFirstResponder];
        }
    }
    return YES;
}


- (CLGeocoder *)geocoder
{
    if(_geocoder == nil){
        _geocoder = [[CLGeocoder alloc] init];
    }
    return _geocoder;
}

@end
