//
//  CreditPointViewController.m
//  RushOrder
//
//  Created by Conan on 4/25/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "CreditPointViewController.h"
#import "PCCredentialService.h"

@interface CreditPointViewController ()

@property (weak, nonatomic) IBOutlet UILabel *creditLabel;
@property (weak, nonatomic) IBOutlet UILabel *creditAmountLabel;
@property (weak, nonatomic) IBOutlet UIView *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitLabel;
@property (weak, nonatomic) IBOutlet NPStretchableButton *verfiyButton;
@property (weak, nonatomic) IBOutlet UIImageView *payselfLogoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *phoneIconImageView;
@property (weak, nonatomic) IBOutlet UIImageView *backImageView;
@property (weak, nonatomic) IBOutlet UILabel *pointBalancetitle;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIView *creditContainerView;

@property (strong, nonatomic) NSString *smsVerificationToken;
@property (nonatomic) NSInteger verificationTimeout;
@end

@implementation CreditPointViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CRED.dirtyCredit = YES;
    
#ifdef SLIDE_LEFT_MENU
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemLeftMenu
                                                                        target:self
                                                                        action:@selector(leftMenuButtonTouched:)];
#endif
    
    self.creditContainerView.layer.borderColor = [[UIColor orangeColor] CGColor];
    self.creditContainerView.layer.borderWidth = 1.0f;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(creditAwardedNotified:)
                                                 name:@"CreditAwardedNotifications"
                                               object:nil];
    
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"orange_real_back.jpg"]];
    self.tableView.backgroundView.layer.zPosition -= 1;
}

- (void)creditAwardedNotified:(NSNotification *)noti
{
    [self requestMyCredit];
    CRED.dirtyCredit = NO;
}

- (void)leftMenuButtonTouched:(id)sender
{
//    [APP.viewDeckController toggleLeftViewAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)requestMyCredit
{
    [self requestMyCredit:NO];
}

- (void)requestMyCredit:(BOOL)withoutToast
{
    if(CRED.customerInfo != nil){
        RequestResult result = RRFail;
        result = [CRED requestMyCreditWithResetBadgeCount:YES
                                          completionBlock:
                  ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                      if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                          switch(response.errorCode){
                              case ResponseSuccess:
                                  CRED.customerInfo.credit = [response integerForKey:@"credit_balance"];
                                  CRED.customerInfo.referralBadgeCount = [response integerForKey:@"referrl_badge_count"];
                                  
                                  if(CRED.customerInfo.referralBadgeCount > 0){
                                      ((UITabBarItem *)APP.tabBarController.tabBar.items[4]).badgeValue = [NSString stringWithFormat:@"%lu",CRED.customerInfo.referralBadgeCount];
                                  } else {
                                      ((UITabBarItem *)APP.tabBarController.tabBar.items[4]).badgeValue = nil;
                                  }
                                  
                                  [UIApplication sharedApplication].applicationIconBadgeNumber = CRED.customerInfo.referralBadgeCount;
                                  
                                  [self updateViews];
                                  break;
                              case ResponseErrorGeneral:{
                                  NSString *message = [response objectForKey:@"message"];
                                  if([message isKindOfClass:[NSString class]]){
                                      [UIAlertView alert:message];
                                  }
                                  break;
                              }
                              default:{
                                  NSString *message = [response objectForKey:@"message"];
                                  if([message isKindOfClass:[NSString class]]){
                                      [UIAlertView alert:message];
                                  } else {
                                      [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting my points", nil)];
                                      
                                  }
                                  break;
                              }
                          }
                      } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                          [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                          [CRED resetUserAccount];
                          //Signed out in requestMyCreditWithCompletionBlock
                      } else {
                          if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                              [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                          } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                              [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                                  ,statusCode]];
                          }
                      }
                      [SVProgressHUD dismiss];
                      [self.refreshControl endRefreshing];
                  }];
        switch(result){
            case RRSuccess:
                if(!withoutToast){
                    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                }
                break;
            case RRParameterError:
                [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                [self.refreshControl endRefreshing];
                break;
            default:
                break;
        }
    } else {
        [self updateViews];
    }
}

- (void)updateViews
{
    if(CRED.customerInfo != nil){
        self.infoLabel.hidden = YES;
        self.verfiyButton.hidden = YES;
        self.creditLabel.hidden = NO;
//        self.creditAmountLabel.hidden = NO;
        self.unitLabel.hidden = NO;
        self.payselfLogoImageView.hidden = NO;
        self.phoneIconImageView.hidden = YES;
        self.pointBalancetitle.hidden = NO;
        self.lineView.hidden = NO;
        self.backImageView.hidden = NO;
//        self.backImageView.image = [[UIImage imageNamed:@"bg_point_balance"]
//                                    resizableImageWithCapInsets:UIEdgeInsetsMake(45.0f,
//                                                                                 10.0f,
//                                                                                 14.0f,
//                                                                                 10.0f)];
        
        self.creditLabel.text = [[NSNumber numberWithCurrency:CRED.customerInfo.credit] pointString];
//        self.creditAmountLabel.text = [[NSNumber numberWithCurrency:CRED.customerInfo.credit] currencyString];
    } else {
        self.infoLabel.hidden = NO;
        self.verfiyButton.hidden = NO;
        self.creditLabel.hidden = YES;
//        self.creditAmountLabel.hidden = YES;
        self.unitLabel.hidden = YES;
        self.payselfLogoImageView.hidden = YES;
        self.phoneIconImageView.hidden = NO;
        self.pointBalancetitle.hidden = YES;
        self.lineView.hidden = YES;
        self.backImageView.hidden = YES;
        
//        self.backImageView.image = [[UIImage imageNamed:@"bg_point_notverified"]
//                                    resizableImageWithCapInsets:UIEdgeInsetsMake(45.0f,
//                                                                                 10.0f,
//                                                                                 14.0f,
//                                                                                 10.0f)];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(CRED.isDirtyCredit){
        [self requestMyCredit];
        CRED.dirtyCredit = NO;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
//    APP.viewDeckController.panningMode = IIViewDeckFullViewPanning;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
//    APP.viewDeckController.panningMode = IIViewDeckNoPanning;
}

- (IBAction)verifyButtonTouched:(id)sender
{
    VerificationViewController *viewController = [VerificationViewController viewControllerFromNib];
    viewController.delegate = self;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^(){
                                 }];
}


-(void)displaySMSComposerSheet
{
	MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
    picker.recipients = [NSArray arrayWithObject:APP.credPhoneNumber];
    picker.body = self.smsVerificationToken;
	picker.messageComposeDelegate = self;
	
	[self presentViewController:picker
                       animated:YES
                     completion:^(){
                     }];
}

#pragma mark - MFMessageComposeViewControllerDelegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    switch (result)
	{
		case MessageComposeResultSent:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
			break;
        case MessageComposeResultCancelled:
        case MessageComposeResultFailed:
		default:
			PCLog(@"Failed");
			break;
	}
	[self dismissViewControllerAnimated:YES
                             completion:^{}];
}

- (void)viewController:(VerificationViewController *)viewController
   successRegistration:(BOOL)success
{
    if(success){
        [self requestMyCredit];
    }
}

- (IBAction)shareNEarnButtonTouched:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NeedToPreparePushingAutoViewController"
                                                        object:self
                                                      userInfo:@{@"type":@"invite"}];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc
{
    CRED.dirtyCredit = YES;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"CreditAwardedNotifications"
                                                  object:nil];
}

- (IBAction)refresh:(UIRefreshControl *)sender
{
    [self requestMyCredit:YES];
}


- (void)viewDidUnload {
    [self setCreditLabel:nil];
    [self setInfoLabel:nil];
    [self setVerfiyButton:nil];
    [self setUnitLabel:nil];
    [self setPayselfLogoImageView:nil];
    
    [super viewDidUnload];
}
@end
