//
//  TakeoutViewController.m
//  RushOrder
//
//  Created by Conan on 11/13/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "TakeoutViewController.h"
#import "PhoneNumberFormatter.h"
#import "OrderManager.h"
#import "PCCredentialService.h"
#import "AccountSwitchingManager.h"
#import "RemoteDataManager.h"
#import "PaymentMethodViewController.h"
#import "NewCardViewController.h"
#import "MenuManager.h"

#define MINUTE_SPAN     15
#define LIMIT_HOUR      3

@interface TakeoutViewController ()
{
    int _textFieldSemaphore;
    __strong PhoneNumberFormatter *_phoneNumberFormatter;
}

@property (strong, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPickerView *timePickerView;
@property (strong, nonatomic) IBOutlet UIToolbar *pickerToolBar;
@property (weak, nonatomic) IBOutlet NPStretchableButton *pickupTimeSelectButton;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel;
@property (strong, nonatomic) NSMutableArray *timeArray;
@property (weak, nonatomic) IBOutlet UILabel *placeHolderLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressTopConstraint;
@property (weak, nonatomic) IBOutlet UIView *emailAdditionView;
@property (weak, nonatomic) IBOutlet PCTextField *emailTextField;
@property (weak, nonatomic) IBOutlet PCTextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UIButton *orderButton;

@property (weak, nonatomic) IBOutlet NPStretchableButton *recentRequestButton;
@property (strong, nonatomic) RecentListViewController *recentRequestViewController;
@property (strong, nonatomic) NSMutableArray *suggestingCustomRequests;
@property (strong, nonatomic) IBOutlet UIButton *suggestButton;

@property (strong, nonatomic) NSMutableArray *mobileCardList;
@property (nonatomic, getter = isCardListWaiting) BOOL cardListWaiting;
@property (nonatomic, getter = isNeedCustomerInfoOverwriting) BOOL needCustomerInfoOverwriting;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deliveryLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *switchButtonSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dineinTrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *etaTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *etaSpaceContraint;
@property (weak, nonatomic) IBOutlet NPStretchableButton *deliveryButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *dineinButton;
@property (nonatomic, getter = isNeedToSaveTempInfo) BOOL needToSaveTempInfo;

@end

@implementation TakeoutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _textFieldSemaphore = 0;
        _phoneNumberFormatter = [[PhoneNumberFormatter alloc] init];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(signedIn:)
                                                     name:SignedInNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(signedOut:)
                                                     name:SignedOutNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(cardListChanged:)
                                                     name:CardListChangeNotification
                                                   object:nil];
        
        self.needToSaveTempInfo = YES;
    }
    return self;
}

- (void)signedIn:(NSNotification *)aNoti
{
    self.mobileCardList = nil;
    self.needCustomerInfoOverwriting = YES;
    // This code makes current name and number nil - so commented out
//    ORDER.tempDeliveryAddress.receiverName = nil;
//    ORDER.tempDeliveryAddress.phoneNumber = nil;
}

- (void)signedOut:(NSNotification *)aNoti
{
    self.mobileCardList = nil;
}

- (void)cardListChanged:(NSNotification *)notification
{
    if(CRED.isSignedIn){
        self.mobileCardList = REMOTE.cards;
        if(self.isCardListWaiting){
            self.cardListWaiting = NO;
            [self stepForPayment];
        }
    } else {
        self.mobileCardList = [MobileCard listAll];
    }
}

- (void)stepForPayment
{
    if(CRED.isSignedIn){
        self.mobileCardList = REMOTE.cards;
        if(!REMOTE.isCardFetched){
            self.cardListWaiting = YES;
            return;
        }
    }
    
    UIViewController *viewController = nil;
    
//    ORDER.tempDeliveryAddress = [[Addresses alloc] init];
    if(self.isNeedToSaveTempInfo){
        ORDER.tempDeliveryAddress.receiverName = self.orderNameField.text;
        ORDER.tempDeliveryAddress.phoneNumber = self.phoneNumberTextField.text;
        if(ORDER.tempDeliveryAddressFilled < 2) ORDER.tempDeliveryAddressFilled = 2;
    }
    
    ORDER.tempAfterMinute = self.afterMinute;
    ORDER.tempCustomerRequest = self.additionalRequest.text;
    
#if FLURRY_ENABLED
    [Flurry logEvent:@"Try to Pay(T)" withParameters:@{@"RestaurantName":ORDER.merchant.name,
                                                       @"RestaurantId":[NSNumber numberWithSerial:ORDER.merchant.merchantNo],
                                                       @"orderTotal":[NSNumber numberWithCurrency:ORDER.order.subTotal]}];
#endif
    
    if([self.mobileCardList count] > 0 || CRED.customerInfo.credit > 0){
        
        viewController = [PaymentMethodViewController viewControllerFromNib];
        ((PaymentMethodViewController *)viewController).payment = self.payment;
        ((PaymentMethodViewController *)viewController).paymentMethodList = self.mobileCardList;
        ((PaymentMethodViewController *)viewController).address = ORDER.tempDeliveryAddress;
        ((PaymentMethodViewController *)viewController).requestMessage = self.additionalRequest.text;
        ((PaymentMethodViewController *)viewController).afterMinute = self.afterMinute;
        
    } else {
        viewController = [NewCardViewController viewControllerFromNib];
        ((NewCardViewController *)viewController).unintendedPresented = YES;
        ((NewCardViewController *)viewController).payment = self.payment;
        ((NewCardViewController *)viewController).address = ORDER.tempDeliveryAddress;
        ((NewCardViewController *)viewController).requestMessage = self.additionalRequest.text;
        ((NewCardViewController *)viewController).afterMinute = self.afterMinute;
    }
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.recentRequestButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
    self.additionalRequest.inputAccessoryView = self.suggestButton;
    self.additionalRequest.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.title = NSLocalizedString(@"Take-out Option", nil);
    
//    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemCancel
//                                                                        target:self
//                                                                        action:@selector(cancelButtonTouched:)];
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemBack
                                                                        target:self
                                                                        action:@selector(backButtonTouched:)];
    
    [self.scrollView setContentSizeWithBottomView:self.orderButton];
    
    self.pickupTimeTextField.inputView = self.timePickerView;
    self.pickupTimeTextField.inputAccessoryView = self.pickerToolBar;
    
//    if(self.afterMinute > 0){
//        self.asapButton.selected = NO;
//        self.pickupTimeButton.selected = YES;
//        
//        NSUInteger i = 0;
//        for(NSDictionary *dict in self.timeArray){
//            if([[dict objectForKey:@"afterMinute"] integerValue] == self.afterMinute){
//                
//                break;
//            }
//            i++;
//        }
//        [self.timePickerView selectRow:i inComponent:0 animated:NO];
//        [self setSelectedValue:i];
//    } else {
//        self.asapButton.selected = YES;
//        self.pickupTimeButton.selected = NO;
//        [self.timePickerView selectRow:0 inComponent:0 animated:NO];
//        [self setSelectedValue:0];
//    }
    [self setPickupTimeSubviews];
    
    if([ORDER.merchant.takeoutRemark length] > 0){
        self.remarkLabel.text = ORDER.merchant.takeoutRemark;
    } else {
        self.remarkLabel.text = NSLocalizedString(@"Thank you for your order", nil);
    }
    
    if(CRED.isSignedIn){
        if([self.orderNameField.text length] == 0){
            self.orderNameField.text = CRED.customerInfo.name;
        }
        
        if([self.phoneNumberTextField.text length] == 0){
            self.phoneNumberTextField.text = CRED.customerInfo.phoneNumber;
        }
    }
    
    if([self.orderNameField.text length] == 0){
        self.orderNameField.text = [NSUserDefaults standardUserDefaults].custName;
    }
    
    if([self.phoneNumberTextField.text length] == 0){
        self.phoneNumberTextField.text = [NSUserDefaults standardUserDefaults].custPhone;
    }
    
    
    if([self.orderNameField.text length] > 0){
        self.orderNameField.placeholder = self.orderNameField.text;
    }
    
    if([self.phoneNumberTextField.text length] > 0){
        self.phoneNumberTextField.placeholder = self.phoneNumberTextField.text;
    }
    
    [self phoneNumberValueChanged:self.phoneNumberTextField];
    
    if([self.initialName length] > 0){
        self.orderNameField.text = self.initialName;
    }
    
    if([self.initialRequestMessage length] > 0){
        self.additionalRequest.text = self.initialRequestMessage;
        self.placeHolderLabel.hidden = ([self.additionalRequest.text length] > 0);
    }
    
    if([self.initialPhoneNumber length] > 0){
        self.phoneNumberTextField.text = self.initialPhoneNumber;
    }
}

- (void)backButtonTouched:(id)sender
{
    if(ORDER.order.status == OrderStatusDraft
       && ORDER.cart.cartType != CartTypeCart){
        if(self.disposeOrderWhenBack){
            ORDER.order = nil;
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
//    ORDER.tempDeliveryAddress = [[Addresses alloc] init];
    if(self.isNeedToSaveTempInfo){
        ORDER.tempDeliveryAddress.receiverName = self.orderNameField.text;
        ORDER.tempDeliveryAddress.phoneNumber = self.phoneNumberTextField.text;
        if(ORDER.tempDeliveryAddressFilled < 2) ORDER.tempDeliveryAddressFilled = 2;
    }
    
    ORDER.tempAfterMinute = self.afterMinute;
    ORDER.tempCustomerRequest = self.additionalRequest.text;
}

- (void)drawTopPartAccordingToOrderType
{
    /*
     @property (weak, nonatomic) IBOutlet NSLayoutConstraint *takeoutLeadingConstraint;
     @property (weak, nonatomic) IBOutlet NSLayoutConstraint *switchButtonSpaceConstraint;
     @property (weak, nonatomic) IBOutlet NSLayoutConstraint *dineinTrailingConstraint;
     @property (weak, nonatomic) IBOutlet NSLayoutConstraint *etaTopConstraint;
     @property (weak, nonatomic) IBOutlet NSLayoutConstraint *etaSpaceContraint;
     @property (weak, nonatomic) IBOutlet NPStretchableButton *takeoutButton;
     @property (weak, nonatomic) IBOutlet NPStretchableButton *dineinButton;
     */
    
    if(ORDER.merchant.isAbleDinein && ORDER.merchant.isAbleDelivery){
        self.deliveryButton.hidden = NO;
        self.dineinButton.hidden = NO;
        self.switchButtonSpaceConstraint.priority = 999;
        self.deliveryLeadingConstraint.priority = 500;
        self.dineinTrailingConstraint.priority = 500;
        self.etaTopConstraint.priority = 500;
        self.etaSpaceContraint.priority = 999;
    } else if(ORDER.merchant.isAbleDinein){
        self.deliveryButton.hidden = YES;
        self.dineinButton.hidden = NO;
        self.switchButtonSpaceConstraint.priority = 500;
        self.deliveryLeadingConstraint.priority = 999;
        self.dineinTrailingConstraint.priority = 999;
        self.etaTopConstraint.priority = 500;
        self.etaSpaceContraint.priority = 999;
    } else if(ORDER.merchant.isAbleDelivery){
        self.deliveryButton.hidden = NO;
        self.dineinButton.hidden = YES;
        self.switchButtonSpaceConstraint.priority = 500;
        self.deliveryLeadingConstraint.priority = 999;
        self.dineinTrailingConstraint.priority = 999;
        self.etaTopConstraint.priority = 500;
        self.etaSpaceContraint.priority = 999;
    } else {
        self.deliveryButton.hidden = YES;
        self.dineinButton.hidden = YES;
        self.switchButtonSpaceConstraint.priority = 999;
        self.deliveryLeadingConstraint.priority = 500;
        self.dineinTrailingConstraint.priority = 500;
        self.etaTopConstraint.priority = 999;
        self.etaSpaceContraint.priority = 500;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self drawTopPartAccordingToOrderType];
    if(ORDER.isSetTempDelivery){
        
        if(ORDER.tempDeliveryAddressFilled >= 1){
            if(ORDER.tempDeliveryAddress.receiverName.length != 0){
                self.orderNameField.text = ORDER.tempDeliveryAddress.receiverName;
            }
        }
        
        if(ORDER.tempDeliveryAddressFilled >= 2){
            if(ORDER.tempDeliveryAddress.phoneNumber.length != 0){
                self.phoneNumberTextField.text = ORDER.tempDeliveryAddress.phoneNumber;
            }
        }
        
        self.additionalRequest.text = ORDER.tempCustomerRequest;
        [self textViewDidChange:self.additionalRequest];
        
        self.afterMinute = ORDER.tempAfterMinute;
        if(self.afterMinute == 0){
            [self asapButtonTouched:self.asapButton];
        } else {
            [self specificButtonTouched:nil];
            NSDate *now = [NSDate date];
            NSDate *afterNow = [now dateByAddingTimeInterval:self.afterMinute * 60];
            self.pickupTimeTextField.text = afterNow.hourMinuteString;
            
            NSInteger safeAfterMinute = MAX(self.afterMinute, MINUTE_SPAN);
            [self.timePickerView selectRow:((safeAfterMinute / MINUTE_SPAN) - 1)
                               inComponent:0
                                  animated:NO];
        }
    }
    
    if(CRED.isSignedIn){
        self.emailAdditionView.hidden = YES;
        self.addressTopConstraint.constant = 19.0f;
        self.phoneNumberTextField.nextField = self.additionalRequest;
        
        
        if(self.isNeedCustomerInfoOverwriting){
            self.orderNameField.text = CRED.customerInfo.name;
        } else {
            if([self.orderNameField.text length] == 0){
                self.orderNameField.text = CRED.customerInfo.name;
            }
        }
        
        //        if([self.phoneNumberTextField.text length] == 0){ // commented reason : Ability to Sign-in During Checkout Process (https://app.asana.com/0/inbox/23035815421587/222338978099920/222338978099925)
        if(self.isNeedCustomerInfoOverwriting){
            self.phoneNumberTextField.text = CRED.customerInfo.phoneNumber;
        } else {
            if([self.phoneNumberTextField.text length] == 0){
                self.phoneNumberTextField.text = CRED.customerInfo.phoneNumber;
            }
        }
        
        self.needCustomerInfoOverwriting = NO;
        
    } else {
        self.emailAdditionView.hidden = NO;
        self.addressTopConstraint.constant = 150.0f;
        self.phoneNumberTextField.nextField = self.emailTextField;
        if([self.emailTextField.text length] == 0){
            self.emailTextField.text = [NSUserDefaults standardUserDefaults].emailAddress;
        }
    }
    
}

- (IBAction)doneButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    [NSUserDefaults standardUserDefaults].custName = self.orderNameField.text;
    [NSUserDefaults standardUserDefaults].custPhone = self.phoneNumberTextField.text;
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    if(self.pickupTimeButton.selected &&
       self.afterMinute == 0){
        [UIAlertView alertWithTitle:NSLocalizedString(@"No Specific Time", nil)
                            message:NSLocalizedString(@"You should select specific time you want." ,nil)];
        [self.pickupTimeTextField becomeFirstResponder];
        return;
    }
    
    // Check for Valid Takeout time
    NSDate *specificDate = [NSDate date];
    
    if(self.pickupTimeButton.selected){
        NSTimeInterval afterInterval = self.afterMinute * 60;
        specificDate = [[NSDate date] dateByAddingTimeInterval:afterInterval];
    }
    
    if(![ORDER.merchant isOpenHourWithDate:specificDate]){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Restaurant is Closed with Your Specific Time", nil)
                            message:NSLocalizedString(@"You can order only when the restaurant is open.", nil)];
        return;
    }
    
    if(![VALID validate:self.orderNameField
                  title:NSLocalizedString(@"Please Enter Name", nil)
                message:NSLocalizedString(@"Name is Required to Complete This Order.", nil)]){
        return;
    }
    
    if(![VALID validate:self.phoneNumberTextField
                  title:NSLocalizedString(@"Please Enter Phone Number", nil)
                message:NSLocalizedString(@"Phone Number is Required to Complete This Order.", nil)]){
        return;
    }
    
    NSString * number = self.phoneNumberTextField.text;
//    NSString * strippedNumber = [number stringByReplacingOccurrencesOfString:@"[^0-9]"
//                                                                  withString:@""
//                                                                     options:NSRegularExpressionSearch
//                                                                       range:NSMakeRange(0, [number length])];
//    if(![VALID validate:self.phoneNumberTextField
//              condition:[strippedNumber length] >= 10
//                  title:NSLocalizedString(@"Please Enter a Valid Phone Number", nil)
//                message:NSLocalizedString(@"A 10-digit phone number is required to complete this order", nil)]){
//        return;
//    }
    
    NSString *phoneRegex2 = @"^(\\([0-9]{3})\\) [0-9]{3}-[0-9]{4}$";
    NSPredicate *phoneTest2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex2];
    BOOL isValid =[phoneTest2 evaluateWithObject:number];
    
    if(![VALID validate:self.phoneNumberTextField
              condition:isValid
                  title:NSLocalizedString(@"Please Enter a Valid Phone Number", nil)
                message:NSLocalizedString(@"A valid phone number is required to complete this order - (XXX) XXX-XXXX.", nil)]){
        return;
    }
    
    if(CRED.isSignedIn){
        [self stepForPayment];
    } else {
        
        if(![VALID validate:self.emailTextField
                      title:NSLocalizedString(@"Email Is Required", nil)
                    message:nil]){
            return;
        }
        
        if(![VALID validateEmail:self.emailTextField
                           title:NSLocalizedString(@"Invalid Email", nil)
                         message:NSLocalizedString(@"Please enter a valid email address", nil)]){
            return;
        }
        
        [NSUserDefaults standardUserDefaults].emailAddress = self.emailTextField.text;
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if([self.passwordTextField.text length] > 0){
            // Sign Up Call if Guest
            // After Signing up - proceed order again.
            [NSUserDefaults standardUserDefaults].lastTriedUserId = self.emailTextField.text;
            
            NSArray *nameArray = [self.orderNameField.text componentsSeparatedByString:@" "];
            
            RequestResult result = RRFail;
            result = [CRED requestSignUp:self.emailTextField.text
                                password:self.passwordTextField.text
                               firstName:[nameArray firstName]
                                lastName:[nameArray lastName]
                             phoneNumber:self.phoneNumberTextField.text
                             deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                         completionBlock:
                      ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                          
                          if(isSuccess && 200 <= statusCode && statusCode < 300){
                              switch(response.errorCode){
                                  case ResponseSuccess:{
                                      
                                      CRED.unattendedSignUp = YES;
                                      
                                      NSArray *customers = [CustomerInfo listAll];
                                      CustomerInfo *customerInfo = nil;
                                      if([customers count] == 1){
                                          customerInfo = [customers objectAtIndex:0];
                                      } else{
                                          PCWarning(@"Customers is not just only 1- sign up");
                                          // Clear
                                          for(CustomerInfo *customer in customers){
                                              if([customer delete]){
                                                  // Success
                                              } else {
                                                  PCError(@"Error to remove customerInfo - sign up");
                                              }
                                          }
                                      }
                                      
                                      if(customerInfo == nil){
                                          customerInfo = [[CustomerInfo alloc] init];
                                      }
                                      
                                      NSDictionary *customerDict = [response objectForKey:@"customer"];
                                      [customerInfo updateWithDictionary:customerDict];
                                      customerInfo.authToken = [response objectForKey:@"auth_token"];
                                      
                                      if([customerInfo save]){
                                          CRED.customerInfo = customerInfo;
                                          
                                          [SWITCH removeLocalDataIncludeCard:NO];
                                          
                                          [[NSNotificationCenter defaultCenter] postNotificationName:SignedInNotification
                                                                                              object:self
                                                                                            userInfo:nil];
                                          
                                          [self stepForPayment];
                                      } else {
                                          [UIAlertView alertWithTitle:NSLocalizedString(@"Can Not Save Account Information", nil)
                                                              message:NSLocalizedString(@"Try to sign in again. If the problem contiue, Please contact us.\nhelp@rushorderapp.com", nil)];
                                      }
                                  }
                                      break;
                                  default:{
                                      NSString *message = [response objectForKey:@"message"];
                                      NSString *title = [response objectForKey:@"title"];
                                      if([message isKindOfClass:[NSString class]]){
                                          if(title != nil){
                                              [UIAlertView alertWithTitle:title
                                                                  message:message];
                                          } else {
                                              [UIAlertView alert:message];
                                          }
                                      } else {
                                          [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while signing up", nil)];
                                      }
                                      
                                      CRED.customerInfo = nil;
                                  }
                                      break;
                              }
                          } else {
                              if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                                  [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                                      object:nil];
                              } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                                  [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                                      ,statusCode]];
                              }
                          }
                          [SVProgressHUD dismiss];
                      }];
            switch(result){
                case RRSuccess:
                    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                    break;
                case RRParameterError:
                    [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                    break;
                default:
                    break;
            }
            
        } else {
            [self stepForPayment];
        }
    }
}


//- (void)proceedOrder
//{
//    if([self.delegate respondsToSelector:@selector(takeoutViewController:didTouchPlaceOrderButton:)]){
//        [self.delegate takeoutViewController:self
//                    didTouchPlaceOrderButton:nil
//         ];
//    }
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIPickerViewDatasource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}



- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.timeArray count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSDictionary *dict = [self.timeArray objectAtIndex:row];
    return [dict objectForKey:@"title"];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self setSelectedValue:row];
}

- (void)setSelectedValue:(NSInteger)index
{
    NSDictionary *dict = [self.timeArray objectAtIndex:index];
    
    NSString *specificTime = [dict objectForKey:@"specificTime"];
    self.pickupTimeTextField.text = specificTime;
    
    self.afterMinute = [[dict objectForKey:@"afterMinute"] integerValue];
}

- (IBAction)timeInputButtonTouched:(id)sender
{
    if(self.pickupTimeTextField.isFirstResponder){
        [self inputDoneButtonTouched:sender];
    } else {
        [self.pickupTimeTextField becomeFirstResponder];
    }
}

- (IBAction)inputDoneButtonTouched:(id)sender
{
    NSInteger selectedRow = [self.timePickerView selectedRowInComponent:0];
    [self setSelectedValue:selectedRow];
    [self.pickupTimeTextField resignFirstResponder];
}


- (NSMutableArray *)timeArray
{
    if(_timeArray == nil){
        _timeArray = [NSMutableArray array];
        
        NSInteger count = LIMIT_HOUR * (60 / MINUTE_SPAN);
        
        for(NSInteger i = 0 ; i < count ; i++){
            
            NSDate *now = [NSDate date];
            NSInteger afterMinute = (MINUTE_SPAN * (i + 1));
            NSTimeInterval afterInterval = afterMinute * 60;
            NSDate *afterNow = [now dateByAddingTimeInterval:afterInterval];
            NSMutableString *afterMinuteString = [NSMutableString stringWithString:[NSString stringWithInteger:((i + 1) * MINUTE_SPAN)]];
            [afterMinuteString appendString:@"Min. ("];
            [afterMinuteString appendString:afterNow.hourMinuteString];
            [afterMinuteString appendString:@")"];
            
            NSDictionary *timeDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                      afterMinuteString, @"title",
                                      [NSNumber numberWithInteger:afterMinute], @"afterMinute",
                                      afterNow.hourMinuteString, @"specificTime",
                                      nil];
                                      
            [_timeArray addObject:timeDict];
        }
    }
    return _timeArray;
}

- (IBAction)asapButtonTouched:(NPToggleButton *)sender
{
    if(!sender.selected){
        sender.selected = YES;
        self.pickupTimeButton.selected = !sender.selected;
        self.afterMinute = 0;
        self.pickupTimeTextField.text = @"00:00";
        [self.timePickerView selectRow:0 inComponent:0 animated:NO];
        [self.pickupTimeTextField resignFirstResponder];
    }
    [self setPickupTimeSubviews];
}

- (IBAction)specificButtonTouched:(NPToggleButton *)sender
{
    if(!self.pickupTimeButton.selected){
        self.pickupTimeButton.selected = YES;
        self.asapButton.selected = !self.pickupTimeButton.selected;
    }
    [self setPickupTimeSubviews];
    if(self.pickupTimeButton.selected && (sender != nil)){
        [self.pickupTimeTextField becomeFirstResponder];
    }
}

- (void)setPickupTimeSubviews
{
    self.pickupTimeSelectButton.enabled = self.pickupTimeButton.selected;
    self.pickupTimeTextField.enabled =  self.pickupTimeButton.selected;
    if(!self.pickupTimeTextField.enabled){
        self.pickupTimeTextField.text = nil;
    }
}

- (IBAction)recentRequestButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    self.recentRequestViewController = [RecentListViewController viewControllerFromNib];
    self.recentRequestViewController.delegate = self;
    
    UIWindow *window = self.view.window;
    self.recentRequestViewController.view.frame = window.frame;
    
    [self.recentRequestViewController.tableView reloadData];
    self.recentRequestViewController.panelView.frame = CGRectMake(((self.recentRequestViewController.view.width - self.recentRequestViewController.panelView.width) / 2),
                                                                  700.0f,
                                                                  self.recentRequestViewController.panelView.width,
                                                                  self.recentRequestViewController.panelView.height);
    self.recentRequestViewController.view.alpha = 0.0f;
    [self.view.window addSubview:self.recentRequestViewController.view];
    
    [UIView animateWithDuration:0.3f
                     animations:^{
                         self.recentRequestViewController.view.alpha = 1.0f;
                         CGRect frame = self.recentRequestViewController.panelView.frame;
                         frame.origin.y = (self.recentRequestViewController.view.height - frame.size.height) / 2;
                         self.recentRequestViewController.panelView.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                     }];
    
}

- (void)recentListViewController:(RecentListViewController *)viewController
                didSelectRequest:(CustomRequest *)selectedRequest
{
    self.additionalRequest.text = selectedRequest.request;
    [self textViewDidChange:self.additionalRequest];
//    [self.additionalRequest becomeFirstResponder];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isKindOfClass:[PCTextField class]]){
        [textField resignFirstResponder];
        [[(PCTextField *)textField nextField] becomeFirstResponder];
    }
    return YES;
}

- (IBAction)phoneNumberValueChanged:(UITextField *)sender
{
    if(_textFieldSemaphore) return;
    
    _textFieldSemaphore = 1;
    
    NSString *locale = [[NSLocale currentLocale] localeIdentifier];
    sender.text = [_phoneNumberFormatter format:sender.text
                                     withLocale:locale];
    _textFieldSemaphore = 0;
}

- (NSMutableArray *)mobileCardList
{
    if(_mobileCardList == nil){
        if(CRED.isSignedIn){
            _mobileCardList = REMOTE.cards; // TODO:If card does not exist? -> Blocking?
        } else {
            _mobileCardList = [MobileCard listAll];
        }
    }
    return _mobileCardList;
}


#pragma mark - TextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    if([textView.text length] > 0){
        self.placeHolderLabel.hidden = YES;
        self.suggestingCustomRequests = [CustomRequest suggestingListWithKeyword:textView.text
                                                                            type:CustomRequestTypeTakeout
                                                                      merchantNo:ORDER.merchant.merchantNo];
        
        if([self.suggestingCustomRequests count] > 0){
            CustomRequest *request = [self.suggestingCustomRequests objectAtIndex:0];
            self.suggestButton.buttonTitle = [request.request stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
        } else {
            self.suggestButton.buttonTitle = nil;
        }
    } else {
        self.placeHolderLabel.hidden = NO;
        self.suggestButton.buttonTitle = nil;
    }
}

- (IBAction)suggestButtonTouched:(id)sender
{
    if(self.suggestButton.buttonTitle.length > 0){
        CustomRequest *request = [self.suggestingCustomRequests objectAtIndex:0];
        self.additionalRequest.text = request.request;
        self.placeHolderLabel.hidden = YES;
        [self.additionalRequest resignFirstResponder];
    } else {
        // Do nothing
    }
}

- (IBAction)switchToDineinButtonTouched:(id)sender
{
    if([MENUPAN resetGraphWithMerchant:ORDER.merchant
                              withType:MenuListTypeDinein]){
        ORDER.order.orderType = OrderTypePickup;
        if(ORDER.merchant.isServicedByStaff){
            ORDER.cart.cartType = CartTypePickupServiced;
        } else {
            ORDER.cart.cartType = CartTypePickup;
        }
        [MENUPAN requestMenus:YES
                      success:^(id response) {
                          [ORDER.cart isAllItemsInHour:YES];
                          [[NSNotificationCenter defaultCenter] postNotificationName:OrderTypeSwitchedNotification
                                                                              object:self];
                          [self backButtonTouched:nil];
                      } andFail:^(NSUInteger errorCode) {
                          [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to renew Menus",nil)
                                              message:NSLocalizedString(@"Sorry, please try again and if it continues, please contact us.",nil)];
                          [self.navigationController popToRootViewControllerAnimated:YES];
                      }];
    } else {
        
    }
}

- (IBAction)switchToDeliveryButtonTouched:(id)sender
{
    if([MENUPAN resetGraphWithMerchant:ORDER.merchant
                              withType:MenuListTypeDelivery]){
        ORDER.cart.cartType = CartTypeDelivery;
        ORDER.order.orderType = OrderTypeDelivery;
        [MENUPAN requestMenus:YES
                      success:^(id response) {
                          self.needToSaveTempInfo = NO;
                          [ORDER.cart isAllItemsInHour:YES];
                          [[NSNotificationCenter defaultCenter] postNotificationName:OrderTypeSwitchedNotification
                                                                              object:self];
                          [self backButtonTouched:nil];
                      } andFail:^(NSUInteger errorCode) {
                          [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to renew Menus",nil)
                                              message:NSLocalizedString(@"Sorry, please try again and if it continues, please contact us.",nil)];
                          [self.navigationController popToRootViewControllerAnimated:YES];
                      }];
    } else {
    }
}

- (IBAction)signInButtonTouched:(id)sender
{
    [self showSignIn];
}

- (void)showSignIn
{
    return [self showSignIn:NO];
}

- (void)showSignIn:(BOOL)showGuide
{
    SignInCustomerViewController *viewController = [SignInCustomerViewController viewControllerFromNib];
    viewController.showGuide = showGuide;
    [self.tabBarController presentViewControllerInNavigation:viewController
                                                    animated:YES
                                                  completion:^{
                                                      
                                                  }];
}

- (IBAction)nextButtonTouched:(id)sender
{
    [self inputDoneButtonTouched:sender];
    [self.orderNameField becomeFirstResponder];
}

@end
