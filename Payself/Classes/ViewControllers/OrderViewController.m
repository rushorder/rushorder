//
//  OrderViewController.m
//  RushOrder
//
//  Created by Conan on 5/14/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "OrderViewController.h"
#import "OrderManager.h"
#import "MenuOrderCell.h"
#import "LineItem.h"
#import "MenuItem.h"
#import "Merchant.h"
#import "TransactionManager.h"
#import "TableSectionItem.h"
#import "PCCredentialService.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "BillViewController.h"
#import "RemoteDataManager.h"
#import "FormatterManager.h"
#import "DeliveryViewController.h"
#import "TakeoutViewController.h"
#import "PaymentMethodViewController.h"
#import "NewCardViewController.h"
#import "TablesetViewController.h"
#import "MerchantViewController.h"
#import "MenuManager.h"

enum{
    SubmitOrderTag = 10,
    CanelSubmitTag = 20,
    CheckOutTag = 30,
    OnlyCheckOutTag = 40,
    OrderDoneTag = 50,
    ErrorButtonTag = 100,
};

@interface OrderViewController ()
@property (strong, nonatomic) NSMutableArray *orderList;
@property (weak, nonatomic) IBOutlet UILabel *subTotalAmtLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxAmtLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalAmtLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountAmtLabel;
@property (strong, nonatomic) IBOutlet UIView *bottomView;
@property (strong, nonatomic) IBOutlet UIView *noItemView;
@property (weak, nonatomic) IBOutlet UIButton *checkInButton;
@property (weak, nonatomic) IBOutlet UILabel *tableNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *tableNoLabel;
@property (weak, nonatomic) IBOutlet UIView *dummyFooterView;
@property (strong, nonatomic) NSMutableArray *mobileCardList;

@property (weak, nonatomic) IBOutlet NPStretchableButton *doneButton;
@property (strong, nonatomic) LineItem *selectedLineItem;
@property (weak, nonatomic) IBOutlet UILabel *guideLabel;
@property (strong, nonatomic) IBOutlet UIView *guideView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *removeButton;
@property (weak, nonatomic) IBOutlet UIView *orderHeaderView;
//@property (strong, nonatomic) UIRefreshControl *refreshControl;

@property (weak, nonatomic) IBOutlet LogoCircleImageView *restaurantLogoImageView;
@property (weak, nonatomic) IBOutlet TouchableLabel *restaurantNameLabel;
@property (weak, nonatomic) IBOutlet TouchableLabel *restaurantAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryAddressLabel;
@property (nonatomic, getter = isCardListWaiting) BOOL cardListWaiting;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discountTopConstraint;
@property (weak, nonatomic) IBOutlet NPStretchableButton *firstSwitchButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *secondSwitchButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstSwitchButtonTrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstSwitchButtonSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoTopSapceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoTopConstraint;


@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

@property (strong, nonatomic) MenuOrderCell *offScreenMenuOrderCell;
@end

@implementation OrderViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    // Custom initialization
    self.exceptDefalutBackground = YES;
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cardListChanged:)
                                                 name:CardListChangeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orderListChanged:)
                                                 name:LineItemChangedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillEnterForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cartItemChanged:)
                                                 name:CartItemUpdatedNotificationKey
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(merchantUpdated:)
                                                 name:MerchantInformationUpdatedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signedIn:)
                                                 name:SignedInNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signedOut:)
                                                 name:SignedOutNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orderTypeSwitched:)
                                                 name:OrderTypeSwitchedNotification
                                               object:nil];
}

- (void)signedIn:(NSNotification *)aNoti
{
    self.mobileCardList = nil;
}

- (void)signedOut:(NSNotification *)aNoti
{
    self.mobileCardList = nil;
}


- (void)circleImageViewActionTriggerred:(id)sender
{
    MerchantViewController *viewController = [UIStoryboard viewController:@"MerchantViewController"
                                                                     from:@"Restaurants"];
    [APP.tabBarController presentViewControllerInNavigation:viewController
                                                   animated:YES
                                                 completion:^{
                                                 }];
}

- (void)touchableLabelActionTriggerred:(id)sender
{
    [self circleImageViewActionTriggerred:sender];
}

- (void)merchantUpdated:(NSNotification *)notification
{
    if(ORDER.cart.isLocal){
        [self reloadData];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Add Refresh Control
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                        initWithFrame:CGRectMake(100.0f, 0.0f, 0.0f, 0.0f)];
    refreshControl.tintColor = [UIColor c11Color];
    [refreshControl addTarget:self
                       action:@selector(refreshControlChanged:)
             forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    [self.tableView addSubview:self.refreshControl];
    /////
    
    // Add Targets
    self.restaurantLogoImageView.target = self;
    self.restaurantNameLabel.target = self;
    self.restaurantAddressLabel.target = self;
    
    [self reloadData];
}

- (void)orderTypeSwitched:(NSNotification *)notification
{
    if(notification.object != self){
        self.needReload = YES;
        self.needAutomaticCheckout = YES;
    }
}

- (void)drawTopPartAccordingToOrderType
{
    self.firstSwitchButton.hidden = YES;
    self.secondSwitchButton.hidden = YES;
    
    if(ORDER.merchant.isAbleDinein && MENUPAN.selectedMenuListType != MenuListTypeDinein){
        self.firstSwitchButton.buttonTitle = NSLocalizedString(@"Switch To Dine-in", nil);
        self.firstSwitchButton.hidden = NO;
        if(ORDER.merchant.isServicedByStaff){
            self.firstSwitchButton.tag = CartTypePickupServiced;
        } else {
            self.firstSwitchButton.tag = CartTypePickup;
        }
    }
    
    if(ORDER.merchant.isAbleTakeout && MENUPAN.selectedMenuListType != MenuListTypeTakeout){
        if(self.firstSwitchButton.hidden){
            self.firstSwitchButton.buttonTitle = NSLocalizedString(@"Switch To Take-out", nil);
            self.firstSwitchButton.hidden = NO;
            self.firstSwitchButton.tag = CartTypeTakeout;
        } else {
            self.secondSwitchButton.buttonTitle = NSLocalizedString(@"Switch To Take-out", nil);
            self.secondSwitchButton.hidden = NO;
            self.secondSwitchButton.tag = CartTypeTakeout;
        }
    }
    
    if(ORDER.merchant.isAbleDelivery && MENUPAN.selectedMenuListType != MenuListTypeDelivery){
        if(self.firstSwitchButton.hidden){
            self.firstSwitchButton.buttonTitle = NSLocalizedString(@"Switch To Delivery", nil);
            self.firstSwitchButton.hidden = NO;
            self.firstSwitchButton.tag = CartTypeDelivery;
        } else {
            self.secondSwitchButton.buttonTitle = NSLocalizedString(@"Switch To Delivery", nil);
            self.secondSwitchButton.hidden = NO;
            self.secondSwitchButton.tag = CartTypeDelivery;
        }
    }
    
    if(self.firstSwitchButton.hidden && self.secondSwitchButton.hidden){
        self.logoTopSapceConstraint.priority = 500;
        self.logoTopConstraint.priority = 999;
    } else {
        self.logoTopSapceConstraint.priority = 999;
        self.logoTopConstraint.priority = 500;
        if(!self.firstSwitchButton.hidden && self.secondSwitchButton.hidden){
            self.firstSwitchButtonSpaceConstraint.priority = 500;
            self.firstSwitchButtonTrailingConstraint.priority = 999;
        } else { // If all button is visible
            self.firstSwitchButtonSpaceConstraint.priority = 999;
            self.firstSwitchButtonTrailingConstraint.priority = 555;
        }
    }
    
    switch(MENUPAN.selectedMenuListType){
        case MenuListTypeDinein:
            self.title = NSLocalizedString(@"Dine-in Order", nil);
            break;
        case MenuListTypeTakeout:
            self.title = NSLocalizedString(@"Take-out Order", nil);
            break;
        case MenuListTypeDelivery:
            self.title = NSLocalizedString(@"Delivery Order", nil);
            break;
        default:
            self.title = NSLocalizedString(@"Order Summary", nil);
            break;
    }
}

- (void)refreshControlChanged:(UIRefreshControl *)refreshControl
{
    if(![self refresh]) [self.refreshControl endRefreshing];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)applicationWillEnterForeground:(NSNotification *)aNoti
{
    [self refresh];
}

// Called when receiving from PushNotification type No 12
- (void)cartItemChanged:(NSNotification *)aNoti
{
    NSDictionary *userInfo = aNoti.userInfo;
    
    PCSerial cartNo = [userInfo serialForKey:@"cart_id"];
    
    if(ORDER.cart.cartNo == cartNo){
        [self refresh];
    }
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setSubTotalAmtLabel:nil];
    [self setDoneButton:nil];
    [self setBottomView:nil];
    [self setNoItemView:nil];
    [self setCheckInButton:nil];
    [self setTaxAmtLabel:nil];
    [self setTotalAmtLabel:nil];
    [self setTableNumberLabel:nil];
    [self setGuideLabel:nil];
    [self setRemoveButton:nil];

    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self drawTopPartAccordingToOrderType];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(self.isNeedReload){
        [self.tableView reloadData];
        self.needReload = NO;
    }
    
    if(self.isNeedAutomaticCheckout){
        self.needAutomaticCheckout = NO;
        [self doneButtonTouched:self.doneButton];
    }
}

#pragma mark - UITableView Delegate & Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    if([ORDER.cart.lineItems count] == 0 && ORDER.cart.status != CartStatusOrdering){
        [self.navigationItem setRightBarButtonItem:nil
                                          animated:YES];
    } else {
        [self.navigationItem setRightBarButtonItem:self.removeButton
                                          animated:YES];
    }
    
    return [ORDER.cart.lineItemsSection count] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    NSUInteger count = 0;
    
    switch(section){
        case 0:{
            if([ORDER.cart.lineItems count] == 0 && [ORDER.order.lineItems count] == 0){
                count = 1;
            } else {
                count = [ORDER.order.lineItems count];
            }
        }
            break;
        default:
        {
            TableSection *tableSection = [ORDER.cart.lineItemsSection objectAtIndex:(section - 1)];
            count = [tableSection.menus count];
        }
            break;
    }
    
    
    return count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    switch(section){
        case 0:
            if(ORDER.merchant.isTableBase){
                if([ORDER.order.lineItems count] > 0){
                    UIView *headerView = [self makeHeaderViewWithTitle:NSLocalizedString(@"Confirmed Order", nil)
                                                             atSection:section];
                    return headerView;
                } else {
                    return nil;
                }
            } else {
                return nil;
            }
            break;
        default:
        {
            if(ORDER.merchant.isTableBase){
                TableSection *tableSection = [ORDER.cart.lineItemsSection objectAtIndex:(section - 1)];
                UIView *headerView = [self makeHeaderViewWithTitle:tableSection.title
                                                         atSection:section];
                
                return headerView;
            } else {
                return nil;
            }
        }
            break;
    }
}

#define SECTION_HEADER_HEIGHT   28.0f

- (UIView *)makeHeaderViewWithTitle:(NSString *)sectionTitle atSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, SECTION_HEADER_HEIGHT)];
    sectionHeaderView.backgroundColor = [UIColor colorWithR:233.0f
                                                          G:229.0f
                                                          B:222.0f];

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(55.0f, 3.0f, 235.0f, SECTION_HEADER_HEIGHT - 3.0f)];
    titleLabel.font = [UIFont systemFontOfSize:12.0f];
    titleLabel.textColor = [UIColor colorWithR:73.0f G:67.0f B:54.0f];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = sectionTitle;
    
    [sectionHeaderView addSubview:titleLabel];
    
    return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch(section){
        case 0:
            if(ORDER.merchant.isTableBase){
                if([ORDER.order.lineItems count] > 0){
                    return SECTION_HEADER_HEIGHT;
                } else {
                    return 0.0f;
                }
            } else {
                return 0.0f;
            }
            break;
        default:
        {
            if(ORDER.merchant.isTableBase){
                return SECTION_HEADER_HEIGHT;
            } else {
                return 0.0f;
            }
        }
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LineItem *lineItem = nil;
    
    NSString *orderComment = nil;
    
    switch(indexPath.section){
        case 0:
            
            if([ORDER.cart.lineItems count] == 0 && [ORDER.order.lineItems count] == 0){
                return 200.0f; //For No Item View
            }
            
            lineItem = [ORDER.order.lineItems objectAtIndex:indexPath.row];
            lineItem.status = LineItemStatusOrdering;
            
            break;
        default:{
            
            TableSection *tableSection = [ORDER.cart.lineItemsSection objectAtIndex:(indexPath.section - 1)];
            lineItem = [tableSection.menus objectAtIndex:indexPath.row];
            
            switch(ORDER.cart.status){
                case CartStatusOrdering:
                    lineItem.status = LineItemStatusOrdering;
                    break;
                case CartStatusSubmitted:
                    lineItem.status = LineItemStatusSubmitted;
                    break;
                case CartStatusFixed:
                default:
                    lineItem.status = LineItemStatusFixed;
                    break;
            }
            break;
        }
    }
    
    self.offScreenMenuOrderCell.bounds = CGRectMake(0.0f,
                                                    0.0f,
                                                    CGRectGetWidth(tableView.bounds),
                                                    CGRectGetHeight(self.offScreenMenuOrderCell.bounds));
    
    self.offScreenMenuOrderCell.lineItem = lineItem;
    [self.offScreenMenuOrderCell fillContentsWithOrderComment:orderComment];
    [self.offScreenMenuOrderCell updateConstraints];
    
    [self.offScreenMenuOrderCell setNeedsUpdateConstraints];
    [self.offScreenMenuOrderCell updateConstraintsIfNeeded];
    [self.offScreenMenuOrderCell setNeedsLayout];
    [self.offScreenMenuOrderCell layoutIfNeeded];
    
    self.offScreenMenuOrderCell.menuNameLabel.preferredMaxLayoutWidth = self.offScreenMenuOrderCell.menuNameLabel.width - 5.0f; //Hevetical Lighgt can not fit auto layout's intrinsic size give the buffer
    self.offScreenMenuOrderCell.qtyLabel.preferredMaxLayoutWidth = self.offScreenMenuOrderCell.qtyLabel.width - 5.0f; //Hevetical Lighgt can not fit auto layout's intrinsic size give the buffer
    
    [self.offScreenMenuOrderCell setNeedsUpdateConstraints];
    [self.offScreenMenuOrderCell updateConstraintsIfNeeded];
    [self.offScreenMenuOrderCell setNeedsLayout];
    [self.offScreenMenuOrderCell layoutIfNeeded];
    
    CGSize cellSize = [self.offScreenMenuOrderCell.contentView
                       systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    return cellSize.height + 1.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    LineItem *lineItem = nil;
    
    NSString *orderComment = nil;
    
    BOOL showNoItemView = NO;
    
    switch(indexPath.section){
        case 0: //Confirmed
            
            if([ORDER.cart.lineItems count] == 0 && [ORDER.order.lineItems count] == 0){
                return [self.tableView dequeueReusableCellWithIdentifier:@"NoItemCell"];
            }
            
            lineItem = [ORDER.order.lineItems objectAtIndex:indexPath.row];
            lineItem.status = LineItemStatusOrdering;
            
            break;
        default:{
            
            TableSection *tableSection = [ORDER.cart.lineItemsSection objectAtIndex:(indexPath.section - 1)];
            
            showNoItemView = ([tableSection.menus count] == 0);
            
            lineItem = [tableSection.menus objectAtIndex:indexPath.row];
            
            switch(ORDER.cart.status){
                case CartStatusOrdering:
                    lineItem.status = LineItemStatusOrdering;
                    break;
                case CartStatusSubmitted:
                    lineItem.status = LineItemStatusSubmitted;
                    break;
                case CartStatusFixed:
                default:
                    lineItem.status = LineItemStatusFixed;
                    break;
            }
            break;
        }
    }
    
    NSString *cellIdentifier = @"MenuOrderCell";
    
    MenuOrderCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                                               forIndexPath:indexPath];
    cell.lineItem = lineItem;
    
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    [cell fillContentsWithOrderComment:orderComment];
    
    cell.menuNameLabel.preferredMaxLayoutWidth = cell.menuNameLabel.width;
    cell.qtyLabel.preferredMaxLayoutWidth = cell.qtyLabel.width;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LineItem *lineItem = nil;
    
    switch(indexPath.section){
        case 0:
            if([ORDER.order.lineItems count] > 0){
                lineItem = [ORDER.order.lineItems objectAtIndex:indexPath.row];
            }
            break;
        default:
        {
            TableSection *tableSection = [ORDER.cart.lineItemsSection objectAtIndex:(indexPath.section - 1)];
            lineItem = [tableSection.menus objectAtIndex:indexPath.row];
        }
            break;
    }
    self.selectedIndexPath = indexPath;
    
    if(lineItem == nil){ return; }
    
    if((lineItem.menuValidStatus & MenuValidStatusMenuRemoved) == MenuValidStatusMenuRemoved){
        
        self.selectedLineItem = lineItem;
        [UIAlertView askWithTitle:NSLocalizedString(@"Sorry, restaurant no longer carries this menu item.",nil)
                          message:NSLocalizedString(@"Remove this item from your cart?",nil)
                         delegate:self
                              tag:301];
        return;
    }
    
    if((lineItem.menuValidStatus & MenuValidStatusNotAvailableOrderType) == MenuValidStatusNotAvailableOrderType){
        if(lineItem.substitutableMenuItem == nil){
            self.selectedLineItem = lineItem;
            [UIAlertView askWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Sorry, this item is not available for %@", nil), MENUPAN.menuListTypeString]
                              message:NSLocalizedString(@"Remove from your cart?",nil)
                             delegate:self
                                  tag:301];
            return;
        }
    }
    
    if((lineItem.menuValidStatus & MenuValidStatusOutOfHours) == MenuValidStatusOutOfHours){
        
        self.selectedLineItem = lineItem;
        [UIAlertView askWithTitle:NSLocalizedString(@"Sorry, this item is from a menu that is not available at this current time.",nil)
                          message:NSLocalizedString(@"Remove this item from your cart?",nil)
                         delegate:self
                              tag:301];
        return;
    }


    MenuDetailViewController *viewController = [MenuDetailViewController viewControllerFromNib];
    viewController.lineItem = lineItem;
    viewController.delegate = self;
    
    [self.tableView deselectRowAtIndexPath:indexPath
                                  animated:YES];
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:NULL];
}

- (void)cell:(MenuOrderCell *)cell didTouchedRemoveFromCartButtonAtIndexPath:(NSIndexPath *)indexPath
{
    LineItem *lineItem = nil;

    switch(indexPath.section){
        case 0:
            if([ORDER.order.lineItems count] > 0){
                lineItem = [ORDER.order.lineItems objectAtIndex:indexPath.row];
            }
            break;
        default:{
            TableSection *tableSection = [ORDER.cart.lineItemsSection objectAtIndex:(indexPath.section - 1)];
            lineItem = [tableSection.menus objectAtIndex:indexPath.row];
        }
            break;
    }
    
    if(ORDER.cart.status == CartStatusOrdering || ORDER.order.isEditable){
        if(lineItem.quantity <= 0){
            UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"%@ will be removed.\nDo you want to proceed?", nil), lineItem.menuName]
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                 destructiveButtonTitle:NSLocalizedString(@"Remove", nil)
                                                      otherButtonTitles:nil];
            self.selectedLineItem = lineItem;
            sheet.tag = 102;
            [sheet showInView:self.view];
        } else {
            if(ORDER.order.isEditable){
                [ORDER.order subtractLineItem:lineItem
                                      success:^{
                                          TRANS.dirty = YES;
                                      } failure:^{
                                          [self refresh];
                                      }];
            } else {
                [ORDER.cart subtractLineItem:lineItem
                                     success:^(BOOL isOnlyLocal){
                                         if(isOnlyLocal)
                                             TRANS.localDirty = YES;
                                         else
                                             TRANS.dirty = YES;
                                     }
                                     failure:^{
                                         [self refresh];
                                     }];
            }
        }
    } else {
        PCWarning(@"Order list(cart) status is not CartStatusOrdering, Remove operation needs CartStatusOrdering status");
    }
}

#pragma mark - MenuDetailViewControllerDelegate
- (void)menuDetailViewController:(MenuDetailViewController *)viewController didUpdateLineItem:(LineItem *)lineItem;
{
    if(ORDER.order.isEditable){
        [ORDER.order updateLineItem:lineItem
                           success:^(){
                               TRANS.dirty = YES;
                               [self reloadData];
                           }
                           failure:^{
                               [self refresh];
                           }];
    } else {
        [ORDER.cart updateLineItem:lineItem
                           success:^(BOOL isOnlyLocal){
                               if(isOnlyLocal)
                                   TRANS.localDirty = YES;
                               else
                                   TRANS.dirty = YES;
                               [self reloadData];
                           }
                           failure:^{
                               [self refresh];
                           }];
    }
}

- (void)menuDetailViewControllerDidCancelTouched:(MenuDetailViewController *)viewController
{
    [self.tableView deselectRowAtIndexPath:self.selectedIndexPath
                                  animated:YES];
}

- (void)orderListChanged:(NSNotification *)notification
{
    [self reloadData];
}

- (void)reloadData
{
    if([ORDER.cart.lineItems count] > 0 || [ORDER.order.lineItems count] > 0){
//        self.tableView.tableFooterView = self.bottomView;
        self.subTotalAmtLabel.text = ORDER.tempSubtotalString;
        self.taxAmtLabel.text = ORDER.tempTaxesString;
        
        ORDER.tempDiscount = 0;
        
        if(CRED.isSignedIn && ORDER.merchant.promotion != nil){
            self.discountTitleLabel.text = NSLocalizedString(@"Discount", nil);
            self.discountAmtLabel.text = ORDER.tempDiscountString;
            self.discountTopConstraint.constant = 1.0f;
        } else {
            self.discountAmtLabel.text = nil;
            self.discountTitleLabel.text = nil;
            self.discountTopConstraint.constant = 0.0f;
        }
        [self.discountAmtLabel updateConstraints];
        
        self.totalAmtLabel.text = ORDER.tempTotalString;
        
        self.doneButton.enabled = YES;
        
        self.tableView.tableFooterView = nil;
        CGSize size = [self.bottomView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        self.bottomView.height = size.height;
        self.tableView.tableFooterView = self.bottomView;
        
    } else {

        // TODO: show No Item cell
        self.subTotalAmtLabel.text = [NSNumber numberWithCurrency:0].currencyString;
        self.taxAmtLabel.text = [NSNumber numberWithCurrency:0].currencyString;
        self.totalAmtLabel.text = [NSNumber numberWithCurrency:0].currencyString;

        
        self.discountAmtLabel.text = nil;
        self.discountTitleLabel.text = nil;
        
        self.doneButton.enabled = NO;
    }
    
    
    if(!ORDER.merchant.isTableBase || ORDER.cart.isLocal || ORDER.canCheckOut){ //Pickup or conditioned table base
        
        if(ORDER.merchant.isAblePayment){
            if(ORDER.order.status == OrderStatusCompleted){
                
                self.doneButton.buttonTitle = NSLocalizedString(@"Done", nil);
                self.doneButton.tag = OrderDoneTag;
                
                if(ORDER.merchant.isTableBase){
                    self.guideLabel.text = NSLocalizedString(@"Your order is complete! Feel free to leave at any time.", nil);
                } else {
                    self.guideLabel.text = nil;
                }
                
            } else {
                
                self.doneButton.buttonTitle = NSLocalizedString(@"Check Out", nil);
                self.doneButton.tag = CheckOutTag;
                
                if(ORDER.merchant.isTableBase){
                    if(ORDER.cart.cartType == CartTypeTakeout || ORDER.cart.cartType == CartTypeDelivery){
                        self.guideLabel.text = nil;
                    } else {
                        if(ORDER.cart.isLocal){
                            // Sit-down but. order is for pickup (not takeout)
                            self.doneButton.enabled = NO;
                            self.guideLabel.text = NSLocalizedString(@"Your order was being made when this restaurant accepted pickup orders now. But, This restaurant has changed not to accept pickup orders. To cancel this order, please \"Remove\" this order by touching the top-right button of this order list", nil);
                        } else {
                            self.guideLabel.text = NSLocalizedString(@"Your order is being prepared. You can check out for this order at any time you want.", nil);
                        }
                    }
                } else {
                    // Pickup
                    self.guideLabel.text = nil;
                }
            }
        } else {
            // Only Menu ordering mode
            self.doneButton.buttonTitle = NSLocalizedString(@"Done", nil);
            self.doneButton.tag = OrderDoneTag;
            
            if(ORDER.merchant.isTableBase){
                self.guideLabel.text = NSLocalizedString(@"Your order is being prepared. And your meal will be served soon. This restaurant does not supply payment service. You have to pay server or restaurant directly.", nil);
            } else {
                self.guideLabel.text = nil;
            }
        }
        
    } else if(ORDER.cart.status == CartStatusOrdering){
        if([self.delegate orderViewControllerInitialCartType:self] == CartTypeCart){
            self.doneButton.buttonTitle = NSLocalizedString(@"Submit Order", nil);
        } else {
            self.doneButton.buttonTitle = NSLocalizedString(@"Place Order", nil);
        }
        self.doneButton.tag = SubmitOrderTag;
        self.guideLabel.text = nil;
        
    } else if(ORDER.cart.status == CartStatusSubmitted){
        
        self.doneButton.buttonTitle = NSLocalizedString(@"Change Submitted Order", nil);
        self.doneButton.tag = CanelSubmitTag;
        
        self.guideLabel.text = NSLocalizedString(@"Your order has been placed and is waiting for confirmation by the restaurant. You can change your order by selecting the button below.", nil);
        
    } else {
        
        self.doneButton.buttonTitle = NSLocalizedString(@"Error", nil);
        self.doneButton.tag = ErrorButtonTag;
        
        self.guideLabel.text = nil;
    }
    
    // TODO: Show Guide Label?
    
    self.checkInButton.hidden = YES;
    self.removeButton.enabled = NO;
    self.tableNumberLabel.hidden = NO;
    if(ORDER.merchant.isTableBase && ORDER.tableInfo == nil && ORDER.order == nil
       && ([self.delegate orderViewControllerInitialCartType:self] == CartTypeCart)){
//        self.checkInButton.hidden = NO;
        self.tableNumberLabel.hidden = YES;
    } else if((ORDER.order == nil && ORDER.cart.status == CartStatusOrdering) ||
              ORDER.order.isEditable){
        self.removeButton.enabled = YES;
    }
    
    self.tableNumberLabel.hidden = !self.checkInButton.hidden;
    
    if(ORDER.tableInfo != nil){
        self.tableNumberLabel.hidden = NO;
        self.tableNoLabel.text = [NSString stringWithFormat:@"%@", ORDER.tableInfo.tableNumber];
    } else {
        self.tableNumberLabel.hidden = YES;
        self.tableNoLabel.text = nil;
    }
    [self.tableNoLabel updateConstraintsIfNeeded];
    self.tableNoLabel.hidden = self.tableNumberLabel.hidden;
    
    
    self.tableView.tableHeaderView = nil;
    
    [self.restaurantLogoImageView sd_setImageWithURL:ORDER.merchant.logoURL
                                 placeholderImage:[UIImage imageNamed:@"placeholder_logo"]];
    self.restaurantNameLabel.text = ORDER.merchant.name;
    self.restaurantAddressLabel.text = ORDER.merchant.displayAddress;
//    self.deliveryAddressLabel =
    
    self.orderHeaderView.height = [self.orderHeaderView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    
    self.tableView.tableHeaderView = self.orderHeaderView;
    
    [self.tableView reloadData];
}

- (IBAction)doneButtonTouched:(NPStretchableButton *)sender
{
    if(sender.tag == SubmitOrderTag || sender.tag == CheckOutTag){
        if(![ORDER.cart isAllItemsInHour]){
            [UIAlertView alertWithTitle:NSLocalizedString(@"Review Cart Updates", nil)
                                message:NSLocalizedString(@"There are item updates in your cart that need your attention before you can check out. Please tap each highlighted item to confirm changes and updates. Please check your order and remove unavailable item(s).", nil)];
            [SVProgressHUD dismiss];
            
            [self.tableView reloadData];
            return;
        }
    }
    switch(sender.tag){
        case SubmitOrderTag:
            if(ORDER.cart.hasAlcohol){
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Please Have Your ID Ready", nil)
                                                                    message:NSLocalizedString(@"You must be 21+ to order alcohol", nil)
                                                                   delegate:self
                                                          cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                          otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                alertView.tag = 403;
                alertView.delegate = self;
                [alertView show];
            } else {
                [self submitOrder];
            }
            break;
        case CanelSubmitTag:
            [self cancelSubmit];
            break;
        case CheckOutTag:
            if(ORDER.cart.hasAlcohol){
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Please Have Your ID Ready", nil)
                                                                    message:NSLocalizedString(@"You must be 21+ to order alcohol", nil)
                                                                   delegate:self
                                                          cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                          otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                alertView.tag = 404;
                alertView.delegate = self;
                [alertView show];
            } else {
                [self checkOut];
            }
            break;
        case OnlyCheckOutTag:
            [self onlyCheckOutTag];
            break;
        case OrderDoneTag:
            [self orderDoneButtonTouched];
            break;
        case ErrorButtonTag:
            [UIAlertView alertWithTitle:NSLocalizedString(@"Unknown error", nil)
                                message:NSLocalizedString(@"Sorry, there must be something wrong. Ask to help to server and retry after restarting RushOrder application.", nil)];
            break;
        default:
            break;
    }
}

- (void)cancelSubmit
{
    if(!ORDER.cart.isMineIncluded){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Unable to Change Order", nil)
                            message:NSLocalizedString(@"Because you don't have any items in this table, you are not allowed to change order", nil)];
        return;
    }
    
    [ORDER.cart updateSubmitted:NO
                     success:^(BOOL isOnlyLocal){
                         if(isOnlyLocal)
                             TRANS.localDirty = YES;
                         else
                             TRANS.dirty = YES;
                     } failure:^(){
                         [self refresh];
                     }];
}

- (void)submitOrder
{
    if([ORDER.cart.lineItems count] == 0){
        [UIAlertView alertWithTitle:NSLocalizedString(@"There are No Items to Order", nil)
                            message:NSLocalizedString(@"You have to add at least one item to submit order", nil)];
        return;
    }
    
    if(!ORDER.cart.isMineIncluded){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot Submit Order", nil)
                            message:NSLocalizedString(@"Because there is no item you ordered in this order, you cannot submit order", nil)];
        return;
    }
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Submit all orders for the table? Please check to see if others at your table have finished placing orders.", nil)
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"Submit", nil)
                                                    otherButtonTitles:nil];
    actionSheet.tag = 101;
    [actionSheet showInView:self.view];
    
}

- (void)checkOut
{
    if(ORDER.cart.hasCorruptedItem){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Review Cart Updates", nil)
                            message:NSLocalizedString(@"There are item updates in your cart that need your attention before you can check out. Please tap each highlighted item to confirm changes and updates.", nil)];
        return;
    }
    
    if(!ORDER.merchant.isOpenHour){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Sorry, restaurant is currently closed!", nil)
                            message:NSLocalizedString(@"Please feel free to browse the menu, but ordering isn't available until the restaurant opens.", nil)
                  cancleButtonTitle:NSLocalizedString(@"Got it!", nil)];
        return;
    }
    
    if(ORDER.cart.cartType == CartTypeDelivery){
        if(!ORDER.merchant.isDeliveryHour){
            NSString *hourString = [ORDER.merchant nextDeliveryAvailableTime];
            [UIAlertView alertWithTitle:NSLocalizedString(@"Delivery is Currently Unavailable", nil)
                                message:[NSString stringWithFormat:NSLocalizedString(@"Next Available Time:\n%@", nil), [hourString length] > 0 ? hourString : NSLocalizedString(@"N/A", nil)]];
            
            return;
        }
    }

    
    if(ORDER.tableInfo != nil){
        BillViewController *viewController = [BillViewController viewControllerFromNib];
        [self.navigationController pushViewController:viewController animated:YES];
    } else {
        
        switch(ORDER.cart.cartType){
            case CartTypePickupServiced:
            case CartTypePickup:
            case CartTypeTakeout:
            case CartTypeDelivery:
            {
                [self payWithCardButtonTouched:nil];
            }
                break;
            case CartTypeUnknown:
            case CartTypeCart:
            default:
            {
                if(ORDER.order.orderNo >= 0){
                    [self payWithCardButtonTouched:nil];
                }
            }
                break;
        }
    }
}

- (void)payWithCardButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    if((ORDER.order.orderType == OrderTypeDelivery || ORDER.cart.cartType == CartTypeDelivery)
       && (ORDER.merchant.deliveryMinOrderAmount > ORDER.tempSubtotal)){
        [UIAlertView alertWithTitle:[NSString stringWithFormat:NSLocalizedString(@"%@ Minimum Purchase", nil),[[NSNumber numberWithCurrency:ORDER.merchant.deliveryMinOrderAmount] currencyString]]
                            message:NSLocalizedString(@"Please add more items from the menu to proceed with your delivery order.", nil)];
        return;
    }
    
    if((ORDER.order.orderType == OrderTypeTakeout || ORDER.cart.cartType == CartTypeTakeout)
       && (ORDER.merchant.takeoutMinOrderAmount > ORDER.tempSubtotal)){
        [UIAlertView alertWithTitle:[NSString stringWithFormat:NSLocalizedString(@"%@ Minimum Purchase", nil),[[NSNumber numberWithCurrency:ORDER.merchant.takeoutMinOrderAmount] currencyString]]
                            message:NSLocalizedString(@"Please add more items from the menu to proceed with your take-out order.", nil)];
        return;
    }
    
    if((ORDER.order.orderType == OrderTypePickup || ORDER.cart.cartType == CartTypePickup || ORDER.cart.cartType == CartTypePickupServiced)
       && (ORDER.merchant.dineinMinOrderAmount > ORDER.tempSubtotal)){
        [UIAlertView alertWithTitle:[NSString stringWithFormat:NSLocalizedString(@"%@ Minimum Purchase", nil),[[NSNumber numberWithCurrency:ORDER.merchant.dineinMinOrderAmount] currencyString]]
                            message:NSLocalizedString(@"Please add more items from the menu to proceed with your dine-in order.", nil)];
        return;
    }
    
    if(ORDER.order.isEditable){
        // Reorder declined order
        
    } else {
        // Cart Order
        if([ORDER.cart.lineItems count] == 0){
            [UIAlertView alert:NSLocalizedString(@"Please add at least one item.", nil)];
            return;
        }
        
        if(ORDER.cart.status == CartStatusOrdering
           && ORDER.order.status == OrderStatusDraft){
            // Do nothing?
        } else {
            if([ORDER.cart.lineItems count] == 0){
                [UIAlertView alert:NSLocalizedString(@"Please add at least one item.", nil)];
                return;
            }
            
            if(ORDER.order == nil){
                ORDER.order = [[Order alloc] initWithDictionary:nil
                                                       merchant:ORDER.merchant];
            }
            
            // Update order
            if(ORDER.order.status == OrderStatusDraft ||
               ORDER.order.status == OrderStatusUnknown){
                
                ORDER.order.subTotal = ORDER.cart.amount;
                ORDER.order.subTotalTaxable = ORDER.cart.subtotalAmountTaxable;
                ORDER.order.taxes = ORDER.cart.taxAmount;
                ORDER.order.status = OrderStatusDraft;
                
                switch(ORDER.cart.cartType){
                    case CartTypeCart:
                        ORDER.order.orderType = OrderTypeCart;
                        break;
                    case CartTypeDelivery:
                        ORDER.order.orderType = OrderTypeDelivery;
                        break;
                    case CartTypeTakeout:
                        ORDER.order.orderType = OrderTypeTakeout;
                        break;
                    case CartTypePickup:
                    case CartTypePickupServiced:
                        ORDER.order.orderType = OrderTypePickup;
                        break;
                    default:
                        ORDER.order.orderType = OrderTypeUnknown;
                        break;
                }
            }
        }
    }
    
    [self proceedPayment];
}

- (void)proceedPayment
{
    if(CRED.isSignedIn){
        self.mobileCardList = REMOTE.cards;
        if(!REMOTE.isCardFetched){
            self.cardListWaiting = YES;
            return;
        }
    }
    
    Payment *payment = [[Payment alloc] init];
    payment.merchantNo = ORDER.order.merchantNo;
    payment.subTotal = ORDER.order.subTotal;
    payment.subTotalTaxable = ORDER.order.subTotalTaxable;
    payment.taxAmount = ORDER.order.taxes;
    payment.orderAmount = payment.subTotal + payment.taxAmount;
    payment.payAmount = payment.orderAmount;
    payment.currency = FORMATTER.currencyFormatter.currencyCode;
    payment.payDate = [NSDate date];
    
    if(ORDER.order.orderType == OrderTypeDelivery){
        [payment applyRoServiceFee:ORDER.merchant];
    }
    
    UIViewController *viewController = nil;
    
    if(ORDER.order.orderType == OrderTypeDelivery || ORDER.cart.cartType == CartTypeDelivery){
        
#if FLURRY_ENABLED
        [Flurry logEvent:@"Step for Delivery Info" withParameters:@{@"RestaurantName":ORDER.merchant.name,
                                                        @"RestaurantId":[NSNumber numberWithSerial:ORDER.merchant.merchantNo],
                                                        @"orderTotal":[NSNumber numberWithCurrency:ORDER.order.subTotal]}];
#endif
        
        viewController = [DeliveryViewController viewControllerFromNib];
        ((DeliveryViewController *)viewController).payment = payment;
        ((DeliveryViewController *)viewController).disposeOrderWhenBack = YES;
        
        if(ORDER.order.isEditable){
            ORDER.tempDeliveryAddress = [[Addresses alloc] init];
            ORDER.tempDeliveryAddress.address1 = ORDER.order.address1;
            ORDER.tempDeliveryAddress.address2 = ORDER.order.address2;
            ORDER.tempDeliveryAddress.zip = ORDER.order.zip;
            ORDER.tempDeliveryAddress.city = ORDER.order.city;
            ORDER.tempDeliveryAddress.state = ORDER.order.state;
            ORDER.tempDeliveryAddress.receiverName = ORDER.order.receiverName;
            ORDER.tempDeliveryAddress.phoneNumber = ORDER.order.phoneNumber;
            ORDER.tempDeliveryAddress.coordinate = ORDER.order.takenCoordinate;
            
            ORDER.tempAfterMinute = ORDER.order.pickupAfter;
            ORDER.tempCustomerRequest = ORDER.order.customerRequest;
        }
    } else if(ORDER.order.orderType == OrderTypeTakeout || ORDER.cart.cartType == CartTypeTakeout){
#if FLURRY_ENABLED
        [Flurry logEvent:@"Step for Takeout Info" withParameters:@{@"RestaurantName":ORDER.merchant.name,
                                                                    @"RestaurantId":[NSNumber numberWithSerial:ORDER.merchant.merchantNo],
                                                                    @"orderTotal":[NSNumber numberWithCurrency:ORDER.order.subTotal]}];
#endif
        
        viewController = [TakeoutViewController viewControllerFromNib];
        TakeoutViewController *takeoutViewController = (TakeoutViewController *) viewController;
        takeoutViewController.payment = payment;
        takeoutViewController.disposeOrderWhenBack = YES;
        
        if(ORDER.order.isEditable){
            takeoutViewController.initialName = ORDER.order.receiverName;
            takeoutViewController.initialPhoneNumber = ORDER.order.phoneNumber;
            takeoutViewController.afterMinute = ORDER.order.pickupAfter;
            takeoutViewController.initialRequestMessage = ORDER.order.customerRequest;
            
            ORDER.tempDeliveryAddress = [[Addresses alloc] init];
            ORDER.tempDeliveryAddress.receiverName = ORDER.order.receiverName;
            ORDER.tempDeliveryAddress.phoneNumber = ORDER.order.phoneNumber;
            ORDER.tempDeliveryAddress.coordinate = ORDER.order.takenCoordinate;
            ORDER.tempAfterMinute = ORDER.order.pickupAfter;
            ORDER.tempCustomerRequest = ORDER.order.customerRequest;
        }
//        takeoutViewController.delegate = self;
        
    } else if(ORDER.order.orderType == OrderTypePickup || ORDER.cart.cartType == CartTypePickup || ORDER.cart.cartType == CartTypePickupServiced){
#if FLURRY_ENABLED
        [Flurry logEvent:@"Step for Dine-In Info" withParameters:@{@"RestaurantName":ORDER.merchant.name,
                                                                   @"RestaurantId":[NSNumber numberWithSerial:ORDER.merchant.merchantNo],
                                                                   @"orderTotal":[NSNumber numberWithCurrency:ORDER.order.subTotal]}];
#endif
        
        viewController = [DineinViewController viewControllerFromNib];
        DineinViewController *dineinViewController = (DineinViewController *) viewController;
        dineinViewController.payment = payment;
        dineinViewController.disposeOrderWhenBack = YES;
        
        if(ORDER.order.isEditable){
            
            if(ORDER.cart.cartType == CartTypePickup){
                dineinViewController.initialName = ORDER.order.receiverName;
            } else { // CartTypePickupServiced
                dineinViewController.initialName = ORDER.cart.flagNumber;
            }
            
            dineinViewController.initialRequestMessage = ORDER.order.customerRequest;
        }
//        dineinViewController.delegate = self;
        
    } else {
        
#if FLURRY_ENABLED
        [Flurry logEvent:@"Try to Pay" withParameters:@{@"RestaurantName":ORDER.merchant.name,
                                                      @"RestaurantId":[NSNumber numberWithSerial:ORDER.merchant.merchantNo],
                                                      @"orderTotal":[NSNumber numberWithCurrency:ORDER.order.subTotal]}];
#endif
        
        if([self.mobileCardList count] > 0 || CRED.customerInfo.credit > 0){
            
            viewController = [PaymentMethodViewController viewControllerFromNib];
            ((PaymentMethodViewController *)viewController).payment = payment;
            ((PaymentMethodViewController *)viewController).paymentMethodList = self.mobileCardList;
            ((PaymentMethodViewController *)viewController).disposeOrderWhenBack = YES;
            if(ORDER.order.isEditable){
                [((PaymentMethodViewController *)viewController) setInitialOptionValueWithOrder:ORDER.order];
            }
            
        } else {
            viewController = [NewCardViewController viewControllerFromNib];
            ((NewCardViewController *)viewController).unintendedPresented = YES;
            ((NewCardViewController *)viewController).payment = payment;
            ((NewCardViewController *)viewController).disposeOrderWhenBack = YES;
            
            if(ORDER.order.isEditable){
                [((NewCardViewController *)viewController) setInitialOptionValueWithOrder:ORDER.order];
            }
        }
    }
    [self.navigationController pushViewController:viewController animated:YES];
}

- (NSMutableArray *)mobileCardList
{
    if(_mobileCardList == nil){
        if(CRED.isSignedIn){
            _mobileCardList = REMOTE.cards; // TODO:If card does not exist? -> Blocking?
        } else {
            _mobileCardList = [MobileCard listAll];
        }
    }
    return _mobileCardList;
}

- (void)cardListChanged:(NSNotification *)notification
{
    if(CRED.isSignedIn){
        self.mobileCardList = REMOTE.cards;
        if(self.isCardListWaiting){
            self.cardListWaiting = NO;
            [self proceedPayment];
        }
    } else {
        self.mobileCardList = [MobileCard listAll];
    }
}

- (void)onlyCheckOutTag
{
//    if([self.delegate respondsToSelector:@selector(orderViewController:didTouchOnlyCheckoutButton:)]){
//        [self.delegate orderViewController:self
//                didTouchOnlyCheckoutButton:nil];
//    }
    TablesetViewController *viewController = [TablesetViewController viewControllerFromNib];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)orderDoneButtonTouched
{
//    if([self.delegate respondsToSelector:@selector(orderViewController:didTouchOrderDoneButton:)]){
//
//        [self.delegate orderViewController:self
//                   didTouchOrderDoneButton:nil];
//        
//    }
    TRANS.dirty = YES;
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (IBAction)checkInButtonTouched:(id)sender
{
    if([self.delegate respondsToSelector:@selector(orderViewController:didTouchCheckinButton:)]){
        [self.delegate orderViewController:self
                     didTouchCheckinButton:sender];
    }
}

//- (void)viewDeckController:(IIViewDeckController*)viewDeckController willOpenViewSide:(IIViewDeckSide)viewDeckSide animated:(BOOL)animated
//{
//    self.view.userInteractionEnabled = YES;
//}
//
//- (void)viewDeckController:(IIViewDeckController*)viewDeckController didOpenViewSide:(IIViewDeckSide)viewDeckSide animated:(BOOL)animated
//{
////    [self.tableView scrollToBottom:YES];
//}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 101:
            //Submit Order
            if(buttonIndex == AlertButtonIndexYES){
                [ORDER.cart updateSubmitted:YES
                                    success:^(BOOL isOnlyLocal){
                                        if(isOnlyLocal)
                                            TRANS.localDirty = YES;
                                        else
                                            TRANS.dirty = YES;
                                        
                                 } failure:^(){
                                     [self refresh];
                                 }];
            }
            break;
        case 103:
            
            break;
        case 301: //
            if (buttonIndex == AlertButtonIndexYES){
                [self removeSelectedItem];
            }
            break;
        case 403:
            if(buttonIndex == AlertButtonIndexYES){
                [self submitOrder];
            }
            break;
        case 404:
            if(buttonIndex == AlertButtonIndexYES){
                [self checkOut];
            }
            break;
        case CheckOutTag:
            
            break;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(actionSheet.tag){
        case 101:
            //Submit Order
            if(buttonIndex == actionSheet.destructiveButtonIndex){
                [ORDER.cart updateSubmitted:YES
                                 success:^(BOOL isOnlyLocal){
                                     if(isOnlyLocal)
                                         TRANS.localDirty = YES;
                                     else
                                         TRANS.dirty = YES;
                                     
                                     //Move to home
                                     if([self.delegate respondsToSelector:@selector(orderViewController:didTouchOrderDoneButton:)]){
                                         [self.delegate orderViewController:self
                                                    didTouchOrderDoneButton:nil];
                                     }
                                 } failure:^(){
                                     [self refresh];
                                 }];
            }
            break;
        case 102:
            //remove item
            if(buttonIndex == actionSheet.destructiveButtonIndex){
                [self subtctSelectedItem];
            }
            break;
        case 103:
        {
            if(buttonIndex == actionSheet.destructiveButtonIndex){
                if(ORDER.order.isEditable){
                    [ORDER.order removeAllFromOrderSuccess:^(BOOL isOnlyLocal) {
                        TRANS.dirty = YES;
                        //Move to home
                        if([self.delegate respondsToSelector:@selector(orderViewController:didTouchOrderDoneButton:)]){
                            [self.delegate orderViewController:self
                                       didTouchOrderDoneButton:nil];
                        }
                    } failure:^{
                        // Do nothing
                    }];
                } else {
                    Cart *cart = ORDER.cart;
                    if(cart.isLocal){
                        ORDER.cart = nil;
                        TRANS.localDirty = YES;
                        [cart delete];

                    } else {
                        [cart removeAllFromCartSuccess:^(BOOL isOnlyLocal) {
                            TRANS.dirty = YES;
                        } failure:^{
                            // Do nothing
                        }];
                    }
                }
            }
        }
            break;
    }
}

- (void)removeSelectedItem
{
    if(ORDER.order.isEditable){
        [ORDER.order removeLineItem:self.selectedLineItem
                            success:^{
                                TRANS.dirty = YES;
                            } failure:^{
                                [self refresh];
                            }];
    } else {
        
        [ORDER.cart removeLineItem:self.selectedLineItem
                           success:^(BOOL isOnlyLocal){
                               if(isOnlyLocal){
                                   
                                   TRANS.localDirty = YES;
                                   if([ORDER.cart.lineItems count] == 0){
                                       Cart *cart = ORDER.cart;
                                       ORDER.cart = nil;
                                       [cart delete];
                                   }
                                   
                               } else {
                                   TRANS.dirty = YES;
                               }
                           }
                           failure:^{
                               [self refresh];
                           }];
    }
}

- (void)subtctSelectedItem
{
    if(ORDER.order.isEditable){
        [ORDER.order subtractLineItem:self.selectedLineItem
                              success:^{
                                  TRANS.dirty = YES;
                              } failure:^{
                                  [self refresh];
                              }];
    } else {
        
        [ORDER.cart subtractLineItem:self.selectedLineItem
                             success:^(BOOL isOnlyLocal){
                                 if(isOnlyLocal){
                                     
                                     TRANS.localDirty = YES;
                                     if([ORDER.cart.lineItems count] == 0){
                                         Cart *cart = ORDER.cart;
                                         ORDER.cart = nil;
                                         [cart delete];
                                     }
                                     
                                 } else {
                                     TRANS.dirty = YES;
                                 }
                                 
                             }
                             failure:^{
                                 [self refresh];
                             }];
    }
}


- (IBAction)removeButtonTouched:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Your order will be removed. Do you want to proceed?", nil)
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"OK", nil)
                                                    otherButtonTitles:nil];
    actionSheet.tag = 103;
    [actionSheet showInView:self.view];
}

#pragma mark - Pull to refresh
- (BOOL)refresh
{
    if(ORDER.merchant.isTableBase){
        return [ORDER checkTable:ORDER.tableInfo
                         success:^{
                             [self.refreshControl endRefreshing];
                         }
                         failure:^(OrderStatus status){
                             [self.refreshControl endRefreshing];
                         }];
    } else {
        [self.tableView reloadData];
        [self.refreshControl endRefreshing];
        return NO;
    }
}

- (IBAction)switchOrderTypeButtonTouched:(UIButton *)sender
{
    CartType cartType = (CartType)sender.tag;
    MenuListType switchMenuListType = MenuListTypeAll;
    switch(cartType){
        case CartTypeTakeout:
            switchMenuListType = MenuListTypeTakeout;
            break;
        case CartTypeDelivery:
            switchMenuListType = MenuListTypeDelivery;
            break;
        case CartTypePickupServiced:
            case CartTypePickup:
            switchMenuListType = MenuListTypeDinein;
            break;
        default:
            [UIAlertView alertWithTitle:NSLocalizedString(@"Menu Switching Error", nil)
                                message:NSLocalizedString(@"Please try again or contact us help@rushorderapp.com", nil)];
            return;
            break;
    }
    
    if([MENUPAN resetGraphWithMerchant:ORDER.merchant
                              withType:switchMenuListType]){
        switch(switchMenuListType){
            case MenuListTypeDinein:
                ORDER.order.orderType = OrderTypePickup;
                break;
            case MenuListTypeTakeout:
                ORDER.order.orderType = OrderTypeTakeout;
                break;
            case MenuListTypeDelivery:
                ORDER.order.orderType = OrderTypeDelivery;
                break;
            default:
                break;
        }
        ORDER.cart.cartType = cartType;
        
        [self drawTopPartAccordingToOrderType];
        
        [MENUPAN requestMenus:YES
                      success:^(id response) {
                          [ORDER.cart isAllItemsInHour:YES];
                          
                          [[NSNotificationCenter defaultCenter] postNotificationName:OrderTypeSwitchedNotification
                                                                              object:self];
                          
                          [self.tableView reloadData];
                      } andFail:^(NSUInteger errorCode) {
                          [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to renew Menus",nil)
                                              message:NSLocalizedString(@"Sorry, please try again and if it continues, please contact us.",nil)];
                          [self.navigationController popToRootViewControllerAnimated:YES];
                      }];
    } else {
    }
}


- (NSDate *)lastUpdatedDate
{
    return ORDER.lastUpdatedDate;
}

- (BOOL)inNewLoading
{
    return ORDER.isLoading;
}

- (BOOL)isThereRefreshableData
{
    if(ORDER.merchant.isTableBase){
        return YES;
    } else {
        return NO;
    }
}

- (MenuOrderCell *)offScreenMenuOrderCell
{
    if(_offScreenMenuOrderCell == nil){
        _offScreenMenuOrderCell = [self.tableView dequeueReusableCellWithIdentifier:@"MenuOrderCell"];
    }
    return _offScreenMenuOrderCell;
}
@end
