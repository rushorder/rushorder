//
//  NewCardViewController.m
//  RushOrder
//
//  Created by Conan on 2/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "NewCardViewController.h"
#import "MobileCard.h"
#import "PCPaymentService.h"
#import "ReceiptViewController.h"
#import "Merchant.h"
#import "PCCredentialService.h"
#import "OrderManager.h"
#import "PCMenuService.h"
#import "TransactionManager.h"
#import "TogoAuthViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "RestaurantViewController.h"
#import "Addresses.h"
#import "RemoteDataManager.h"
#import "PaymentMethodViewController.h"
#import "DiscountViewController.h"
#import "Promotion.h"
#import "CardIOCreditCardInfo.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "AccountSwitchingManager.h"
#import "PaymentMethodViewController.h"
#import "CustomRequest.h"
#import "MenuManager.h"
#import "OrderViewController.h"
#import <Braintree/BraintreeCore.h>
#import <Google/Analytics.h>
@import Firebase;

enum _alertViewTag{
    AlertViewTagStripeError = 1,
    AlertViewTagContinuePayment,
    AlertViewTagContinueOrderPayment,
    AlertViewTagContinueSave,
    AlertViewTagcontinueRemove,
    AlertViewTagInvalidOrderError,
    AlertViewTagPromoOrderAmount,
    AlertViewTagTipInput
};

@interface NewCardViewController ()

@property (weak, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NPStretchableButton *payButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *removeButton;

@property (weak, nonatomic) IBOutlet PCCardNumberTextField *cardNumberTextField;
@property (weak, nonatomic) IBOutlet PCTextField *expirationMonthTextField;
@property (weak, nonatomic) IBOutlet PCTextField *securityCodeTextField;
@property (weak, nonatomic) IBOutlet PCTextField *zipCodeTextField;

@property (weak, nonatomic) IBOutlet UILabel *issuerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *issuerLogo;

@property (strong, nonatomic) IBOutlet UIPickerView *monthYearPickerView;
@property (strong, nonatomic) IBOutlet UIToolbar *inputToolBar;
@property (strong, nonatomic) IBOutlet UIToolbar *cardToolBar;

@property (strong, nonatomic) NSArray *monthList;
@property (strong, nonatomic) NSArray *yearList;

@property (strong, nonatomic) MobileCard *selectedCard;

@property (weak, nonatomic) IBOutlet UIView *inputContainer;

@property (nonatomic) BOOL shouldBackToRoot;

@property (copy, nonatomic) NSString *lineItemJsonList;

@property (copy, nonatomic) NSString *promotionCodeForCheckAfterSigningIn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerTopConstraint;

@property (strong, nonatomic) NSNumber *useCreditNumber;

@property (nonatomic, strong) BTAPIClient *braintreeClient;

@end

@implementation NewCardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(signedIn:)
                                                     name:SignedInNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(signedOut:)
                                                     name:SignedOutNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)signedIn:(NSNotification *)aNoti
{
    if(self.isUnintendedPresented){
        if(REMOTE.isCardDirty) {
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(cardListChanged:)
                                                         name:CardListChangeNotification
                                                       object:nil];
            [REMOTE cards];
        } else {
            if(REMOTE.cards.count > 0){
                // push select card
                [self pushPaymentMethodViewController];
                
            } else {
                if(self.promotionCodeForCheckAfterSigningIn != nil){
                    [self validatePromotionWithCode:self.promotionCodeForCheckAfterSigningIn];
                }
            }
        }
    } else {
        if(!self.isManageMode){
            
//            ORDER.tempDeliveryAddress.receiverName = nil;
//            ORDER.tempDeliveryAddress.phoneNumber = nil;
            
            if(self.promotionCodeForCheckAfterSigningIn != nil){
                [self validatePromotionWithCode:self.promotionCodeForCheckAfterSigningIn];
            }
            [self.paymentSummaryView drawPayment];
        }
    }
    
    if(!self.isManageMode){
        if(CRED.customerInfo.credit > 0){
            [self arrangePaymentSummaryView];
            [self selectUseOranges];
        }
    }
}

- (void)requestMyCredit
{
    if(CRED.customerInfo != nil){
        RequestResult result = RRFail;
        result = [CRED requestMyCreditWithCompletionBlock:
                  ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                      if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                          switch(response.errorCode){
                              case ResponseSuccess:
                              {
                                  CRED.customerInfo.credit = [response integerForKey:@"credit_balance"];
                                  
                                  [self arrangePaymentSummaryView];
                                  
                                  if(CRED.customerInfo.credit > 0){
                                      [self selectUseOranges];
                                  }
                                  
                              }
                                  break;
                              case ResponseErrorGeneral:{
                                  NSString *message = [response objectForKey:@"message"];
                                  if([message isKindOfClass:[NSString class]]){
                                      [UIAlertView alert:message];
                                  }
                                  break;
                              }
                              default:{
                                  NSString *message = [response objectForKey:@"message"];
                                  if([message isKindOfClass:[NSString class]]){
                                      [UIAlertView alert:message];
                                  } else {
                                      [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting my points", nil)];
                                  }
                              }
                                  break;
                          }
                      } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                          [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                          //Signed out in requestMyCreditWithCompletionBlock
                      } else {
                          if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                              [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                                  object:nil];
                          } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                              [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                                  ,statusCode]];
                          }
                      }
                      [SVProgressHUD dismiss];
                  }];
        switch(result){
            case RRSuccess:
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                break;
            case RRParameterError:
                [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                break;
            default:
                break;
        }
    }
}

- (void)selectUseOranges
{
    [self selectUseOranges:YES];
}

- (void)selectUseOranges:(BOOL)forcibly
{
#if USE_ORANGE_CELL
    OrangeCell *cell = self.orangeCell;
    if([cell isKindOfClass:[OrangeCell class]]){
        if(forcibly){
            cell.useButton.selected = YES;
        }
        
        [self orangeCelltoggleButtonTouched:cell.useButton];
    }
#endif
    
#if USE_INLINE_ORANGE
    if(forcibly){
        self.paymentSummaryView.orangeUseSwitch.on = YES;
    }
    [self paymentSummaryViewOrangeSwitchChanged:self.paymentSummaryView.orangeUseSwitch];
#endif
}

- (void)orangeCelltoggleButtonTouched:(NPToggleButton *)sender
{
    OrangeCell *cell = self.orangeCell;
    
    if(sender.selected){
        
        // Use self.payment.payAmountWithoutCents when wanting to remove cent units.
        NSInteger maxUsableCredit = MIN(self.payment.payAmount, CRED.customerInfo.credit);
        self.useCreditNumber = [NSNumber numberWithFloat:maxUsableCredit];
        
    } else {
        self.useCreditNumber = [NSNumber numberWithFloat:0];
    }
    
    self.payment.creditAmount = [self.useCreditNumber longLongValue];
    
    cell.useCreditLabel.text = self.payment.creditAmountString;
    cell.useCreditAmountLabel.text = [NSString stringWithFormat:@"(%@)",self.payment.creditConvertedAmountString];
    
    self.paymentSummaryView.pointAmountLabel.text = self.payment.creditConvertedAmountString;
    self.paymentSummaryView.cardAmountLabel.text = self.payment.netAmountString;
    self.paymentSummaryView.pointConvertedLabel.text = self.payment.creditConvertedAmountStringInParenthesis;
    
    self.amountLabel.text = [NSString stringWithFormat:@"(%@)",self.payment.netAmountString];
}

- (void)paymentSummaryViewOrangeSwitchChanged:(UISwitch *)sender
{
//    if(sender.on){
//        NSInteger maxUsableCredit = MIN(self.payment.payAmount, CRED.customerInfo.credit);
//        self.useCreditNumber = [NSNumber numberWithFloat:maxUsableCredit];
//    } else {
//        self.useCreditNumber = [NSNumber numberWithFloat:0];
//    }
//    
//    self.payment.creditAmount = [self.useCreditNumber longLongValue];
//    
//    if(!self.paymentSummaryView.orangePointLabel.hidden){
//        self.paymentSummaryView.orangePointLabel.text = self.payment.creditConvertedAmountStringAsDiscount;
//    } else {
//        self.paymentSummaryView.orangePointLabel.text = nil;
//    }
//    self.paymentSummaryView.totalPayAmountLabel.text =self.payment.netAmountString;
    
    self.amountLabel.text = [NSString stringWithFormat:@"(%@)",self.payment.netAmountString];
}

- (void)cardListChanged:(NSNotification *)noti
{
    if(self.isUnintendedPresented){
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:CardListChangeNotification
                                                      object:nil];
        
        if(!self.isManageMode){
            if(REMOTE.cards.count > 0){
                // push select card
                [self pushPaymentMethodViewController];
                
            } else {
                if(self.promotionCodeForCheckAfterSigningIn != nil){
                    [self validatePromotionWithCode:self.promotionCodeForCheckAfterSigningIn];
                }
            }
        }
    }
}

- (void)pushPaymentMethodViewController
{
//    [self.navigationController popViewControllerAnimated:NO];
    PaymentMethodViewController *viewController = [PaymentMethodViewController viewControllerFromNib];
    viewController.payment = self.payment;
    viewController.paymentMethodList = REMOTE.cards;
    viewController.address = ORDER.tempDeliveryAddress;
    viewController.requestMessage = self.requestMessage;
    viewController.afterMinute = self.afterMinute;
    viewController.promotionCodeForCheckAfterSigningIn = self.promotionCodeForCheckAfterSigningIn;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)signedOut:(NSNotification *)aNoti
{
    [self.paymentSummaryView drawPayment];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ////////////////////////////////////////////////////////////////////////////
    /// Braintree initialization ////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    
    RequestResult result = RRFail;
    result = [PAY requestBrainTreeClientTokenWithCompletionBlock:^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode) {
        
        if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
            switch(response.errorCode){
                    case ResponseSuccess:{
                        
                        NSString *clientToken = [response objectForKey:@"client_token"];
                        if(clientToken != nil){
                            self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:clientToken];
                        }
                        
                        break;
                    }
                    
                default:{
                    NSString *message = [response objectForKey:@"message"];
                    if([message isKindOfClass:[NSString class]]){
                        [UIAlertView alert:message];
                    } else {
                        [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while creating new order", nil)];
                    }
                }
                    break;
            }
        } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
            [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
            [CRED resetUserAccount];
        } else {
            if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                    object:nil];
            } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                    ,statusCode]];
            }
        }
        [SVProgressHUD dismiss];
    }];
    switch(result){
            case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
            case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    
    
    if(!self.isManageMode){
        [self requestMyCredit];
    }
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemBack
                                                                        target:self
                                                                        action:@selector(backButtonTouched:)];
    
    self.issuerLabel.text = @"";
    
    if(self.isManageMode){
        
        if(self.mobileCard == nil){
            self.title = NSLocalizedString(@"New Card", nil);
        } else {
            self.title = NSLocalizedString(@"Edit Card", nil);
            self.removeButton.hidden = NO;
        }
        
        self.payButton.buttonTitle = NSLocalizedString(@"Save This Card", nil);
        
    } else {
        
        [self.scrollView removeConstraint:self.containerTopConstraint];
        
        [self configureLineItems];
        [self arrangePaymentSummaryView];
        
        if(ORDER.merchant.isTestMode){
            self.title = NSLocalizedString(@"Pay With Test Card", nil);
        } else {
            self.title = NSLocalizedString(@"Payment", nil);
        }
        
        if(ORDER.merchant.isTestMode){
            self.cardNumberTextField.text = @"4242424242424242";
            self.expirationMonthTextField.text = @"08/19";
            self.securityCodeTextField.text = @"7679";
            
            self.payButton.buttonTitle = NSLocalizedString(@"Place Order With Test Card", nil);
            
            Issuer issuer = [FORMATTER.cardNumberFormatter acquireIssuer:self.cardNumberTextField.text];
            self.cardNumberTextField.text = [FORMATTER.cardNumberFormatter format:self.cardNumberTextField.text
                                                                       withIssuer:issuer];
            [self cardNumberTextField:self.cardNumberTextField
                      didChangeIssuer:issuer
                         issuerString:[NSString issuerName:issuer]];
        }
    }
    
    self.cardNumberTextField.forwardDelegate = self;
    
    self.expirationMonthTextField.inputView = self.monthYearPickerView;
    self.expirationMonthTextField.inputAccessoryView = self.inputToolBar;
    self.cardNumberTextField.inputAccessoryView = self.cardToolBar;
    
    if(self.mobileCard != nil){
        
        self.cardNumberTextField.text = [self.mobileCard formattedCardNumber:NO];
        self.expirationMonthTextField.text = [NSString stringWithFormat:@"%02ld/%02ld",
                                              (long)self.mobileCard.cardExpireMonth,
                                              (long)self.mobileCard.cardExpireYear];
        
        self.securityCodeTextField.text = self.mobileCard.cardCvc;
        self.issuerLogo.image = self.mobileCard.cardLogo;
        self.issuerLabel.text = self.mobileCard.bankName;
        
        if(self.issuerLogo.image != nil){
            self.issuerLogo.alpha = 1.0f;
            self.cardNumberTextField.width = 227.0f;
        } else {
            self.issuerLogo.alpha = 0.0f;
            self.issuerLogo.image = [UIImage imageNamed:@"icon_card.png"];
            self.cardNumberTextField.width = 280.0f;
        }
    } else {
        
    }
}



- (void)arrangePaymentSummaryView
{
    if(self.paymentSummaryView == nil){
        self.paymentSummaryView = [PaymentSummaryView view];
    }
    [self.paymentSummaryView removeFromSuperview];
    self.paymentSummaryView.order = ORDER.order;
    self.paymentSummaryView.payment = self.payment;
    self.paymentSummaryView.merchant = ORDER.merchant;
    self.paymentSummaryView.delegate = self;
    
    [self.paymentSummaryView drawPayment];
    self.paymentSummaryView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.scrollView addSubview:self.paymentSummaryView];
    
#if USE_ORANGE_CELL
    if (CRED.customerInfo.credit > 0) {
        
        if(self.orangeCell == nil){
            self.orangeCell = [OrangeCell cellWithNibName:@"OrangeCell"];
        }
        [self.orangeCell removeFromSuperview];
        self.orangeCell.translatesAutoresizingMaskIntoConstraints = NO;
        self.orangeCell.viewDelegate = self;
        
        NSNumber *credit = [NSNumber numberWithInteger:CRED.customerInfo.credit];
        self.orangeCell.creditBalaceLabel.text = [credit pointString];
        self.orangeCell.useCreditLabel.text = [self.useCreditNumber pointString];
        self.orangeCell.useCreditAmountLabel.text = [NSString stringWithFormat:@"(%@)",[self.useCreditNumber currencyString]];
        
        self.amountLabel.text = [NSString stringWithFormat:@"(%@)",self.payment.netAmountString];
        
        [self.scrollView addSubview:self.orangeCell];
        
        NSDictionary *viewsDictionary = @{@"paymentSummaryView" : self.paymentSummaryView,
                                          @"inputContainer" : self.inputContainer,
                                          @"orangeCell" : self.orangeCell};
        
        [self.orangeCell addConstraint:[NSLayoutConstraint constraintWithItem:self.orangeCell
                                                                    attribute:NSLayoutAttributeHeight
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:nil
                                                                    attribute:0
                                                                   multiplier:1.0
                                                                     constant:self.orangeCell.height]];
        
        [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[paymentSummaryView]|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:viewsDictionary]];
        [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[orangeCell]|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:viewsDictionary]];
        [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[paymentSummaryView]-0-[orangeCell]-0-[inputContainer]"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:viewsDictionary]];
        
        self.amountLabel.hidden = NO;
        
    } else {
#endif

        NSDictionary *viewsDictionary = @{@"paymentSummaryView" : self.paymentSummaryView,
                                          @"inputContainer" : self.inputContainer};
        
        [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[paymentSummaryView]|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:viewsDictionary]];
        [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[paymentSummaryView]-[inputContainer]"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:viewsDictionary]];
        self.amountLabel.hidden = YES;
#if USE_ORANGE_CELL
    }
#endif
    
    
    [self.scrollView updateConstraints];
    [self.scrollView setNeedsDisplay];
    [self.scrollView layoutSubviews];
}

- (void)setInitialOptionValueWithOrder:(Order *)order
{
    
    self.address = [[Addresses alloc] init];
    
    self.address.state = order.state;
    self.address.city = order.city;
    self.address.address1 = order.address1;
    self.address.address2 = order.address2;
    self.address.zip = order.zip;
    self.address.phoneNumber = order.phoneNumber;
    self.address.receiverName = order.receiverName;
    self.address.coordinate = order.takenCoordinate;
    
    self.receiverName = order.receiverName;
    self.phoneNumber = order.phoneNumber;
    self.requestMessage = order.customerRequest;
    self.afterMinute = order.pickupAfter;
    
}

- (IBAction)scanButtonTouched:(id)sender
{
    
    CardIOPaymentViewController *viewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
//    viewController.appToken = @"2a897a8f143e4f71a366348b4d7c995f"; // get your app token from the card.io website
    viewController.disableManualEntryButtons = YES;
    viewController.collectCVV = NO;
    viewController.collectExpiry = NO;
    viewController.guideColor = [UIColor c11Color];
    viewController.suppressScanConfirmation = YES;
    viewController.useCardIOLogo = YES;
    [self presentViewController:viewController
                       animated:YES
                     completion:^{
                     }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(!CRED.isSignedIn){
        self.promotionCodeForCheckAfterSigningIn = nil;
    }
    
    [self.paymentSummaryView drawPayment];
    
    [self selectUseOranges:NO];
    
    [CardIOUtilities preload];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)backButtonTouched:(id)sender
{
    if(self.shouldBackToRoot){
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else {
        
        //
        NSArray *viewControllers = [self.navigationController viewControllers];
        if([viewControllers count] > 1){
            PaymentMethodViewController *viewController = [viewControllers objectAtIndex:([viewControllers count] - 2)];
            if([viewController isKindOfClass:[PaymentMethodViewController class]]){
                
                viewController.afterMinute = self.afterMinute;
                viewController.requestMessage = self.requestMessage;
                viewController.phoneNumber = self.phoneNumber;
                
                // ???: Where is receiver name and address for this? (for maintaining session)
            }
        }
        
        if(ORDER.order.status == OrderStatusDraft
           && ORDER.cart.cartType != CartTypeCart){
            if(self.disposeOrderWhenBack){
                ORDER.order = nil;
            }
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)configureLineItems
{
    if([ORDER.order.lineItems count] > 0){
        NSMutableArray *arrayForJSON = [NSMutableArray array];
        
        for(LineItem * lineItem in ORDER.order.lineItems){
            if(!lineItem.isSplitExcept && !lineItem.isPaid){
                [arrayForJSON addObject:[NSNumber numberWithSerial:lineItem.lineItemNo]];
            }
        }
        
        if([arrayForJSON count] > 0){
            self.lineItemJsonList = [arrayForJSON JSONString];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tappedMyPayButton:(id)sender {
    
    // If you haven't already, create and retain a `BTAPIClient` instance with a tokenization
    // key or a client token from your server.
    // Typically, you only need to do this once per session.
    //self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:CLIENT_AUTHORIZATION];
    
    // Create a BTDropInViewController
    BTDropInViewController *dropInViewController = [[BTDropInViewController alloc]
                                                    initWithAPIClient:self.braintreeClient];
    dropInViewController.delegate = self;
    
    // This is where you might want to customize your view controller (see below)
    
    // The way you present your BTDropInViewController instance is up to you.
    // In this example, we wrap it in a new, modally-presented navigation controller:
    UIBarButtonItem *item = [[UIBarButtonItem alloc]
                             initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                             target:self
                             action:@selector(userDidCancelPayment)];
    dropInViewController.navigationItem.leftBarButtonItem = item;
    UINavigationController *navigationController = [[UINavigationController alloc]
                                                    initWithRootViewController:dropInViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}
    
- (void)dropInViewController:(BTDropInViewController *)viewController
didSucceedWithTokenization:(BTPaymentMethodNonce *)paymentMethodNonce {
    // Send payment method nonce to your server for processing
//    [self postNonceToServer:paymentMethodNonce.nonce];
    [self dismissViewControllerAnimated:YES completion:nil];
}
    
- (void)dropInViewControllerDidCancel:(__unused BTDropInViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidCancelPayment {
    [self dismissViewControllerAnimated:YES completion:nil];
}
 
// Designated Paybutton actions originally
- (IBAction)payButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    if(![VALID validate:self.cardNumberTextField
                  title:NSLocalizedString(@"Card number is required", nil)
                message:nil]){
        return;
    }
    
    if(![VALID validate:self.expirationMonthTextField
                  title:NSLocalizedString(@"Expiration date is required", nil)
                message:nil]){
        return;
    }
    
    if(![VALID validate:self.securityCodeTextField
                  title:NSLocalizedString(@"Security code is required", nil)
                message:nil]){
        return;
    }
    
    if(![VALID validate:self.zipCodeTextField
                  title:NSLocalizedString(@"Zip code is required", nil)
                message:nil]){
        return;
    }
    
//    if(![VALID validate:self.zipCodeTextField
//              condition:(self.zipCodeTextField.text.length == 5)
//                  title:NSLocalizedString(@"Please enter correct Zip code", nil)
//                message:nil]){
//        return;
//    }
    
    MobileCard *mobileCard = [[MobileCard alloc] init];
    
    mobileCard.cardNumber = self.cardNumberTextField.text;
    NSString *monthYearString = self.expirationMonthTextField.text;
    NSString *monthString = [monthYearString substringWithRange:NSMakeRange(0, 2)];
    NSString *yearString = [monthYearString substringFromIndex:3];
    mobileCard.cardExpireMonth = [monthString integerValue];
    mobileCard.cardExpireYear = [yearString integerValue];
    mobileCard.cardCvc = self.securityCodeTextField.text;
    mobileCard.zipCode = self.zipCodeTextField.text;
    
    if(self.isManageMode){
        
        if(self.mobileCard != nil){
            mobileCard.rowId = self.mobileCard.rowId;
        }
        
        self.mobileCard = mobileCard;
        
        [UIAlertView askWithTitle:NSLocalizedString(@"Confirm To Proceed", nil)
                          message:nil
                         delegate:self
                              tag:AlertViewTagContinueSave];
    } else {

        if(ORDER.merchant.isAllowTip
           && [self.paymentSummaryView.tipTextField.text length] == 0
           && !self.paymentSummaryView.tipTextField.isHidden){
            
            // !!!:Please do not remove [self.paymentSummaryView.tipTextField becomeFirstResponder]; code
            // This code is workaround for UIAlertView and responder chain bug
            // If we remove this line of code, the becomeFirstResponder will not work properly
            // when using custom input view.
            // I think it's because the UIAlertView memorize the status of input trait and get back it
            // when it's being dismissed.
            [self.paymentSummaryView.tipTextField becomeFirstResponder];
            
//            if(self.paymentSummaryView.isAutoTipSetOnce){
//                [UIAlertView alertWithTitle:NSLocalizedString(@"Please Enter Tip", nil)
//                                    message:NSLocalizedString(@"How much would you like to tip? Please enter it in the blank space next to \"Tip\"", nil)
//                                   delegate:self
//                                        tag:AlertViewTagTipInput];
//            } else {
//                [UIAlertView alertWithTitle:NSLocalizedString(@"Please Enter Tip", nil)
//                                    message:NSLocalizedString(@"How much would you like to tip? Please enter it in the blank space next to \"Tip\"", nil)
//                                   delegate:self
//                                        tag:AlertViewTagTipInput];
//            }
            
            return;
        }
        
        if(ORDER.order.orderNo > 0 && !ORDER.order.isEditable){ //Order exists
            
            if(ORDER.merchant.isDemoMode || ORDER.merchant.isTestMode){
                
                NSString *restaurantType = @"This is DEMO restaurant.";
                
                if(ORDER.merchant.isTestMode){
                    restaurantType = @"This restaurant is in TEST mode.";
                }
                
                if(ORDER.order.tableNumber != nil){
                    [UIAlertView askWithTitle:[NSString stringWithFormat:NSLocalizedString(@"%@ You won't be charged for payments made in this store.", nil), restaurantType]
                                      message:[NSString stringWithFormat:NSLocalizedString(@"Do you want to proceed?\nTable : %@\nAmount : %@\n%@", nil),
                                               ORDER.order.tableNumber,
                                               [[NSNumber numberWithCurrency:self.payment.netAmount] currencyString],
                                               self.cardNumberTextField.text]
                                     delegate:self
                                          tag:AlertViewTagContinuePayment];
                } else {
                    [UIAlertView askWithTitle:[NSString stringWithFormat:NSLocalizedString(@"%@ You won't be charged for payments made in this store.", nil), restaurantType]
                                      message:[NSString stringWithFormat:NSLocalizedString(@"Do you want to proceed?\nAmount : %@\n%@", nil),
                                               [[NSNumber numberWithCurrency:self.payment.netAmount] currencyString],
                                               self.cardNumberTextField.text]
                                     delegate:self
                                          tag:AlertViewTagContinuePayment];
                }
            } else {
                if(ORDER.order.tableNumber != nil){
                    [UIAlertView askWithTitle:NSLocalizedString(@"Do you want to proceed with payment below?", nil)
                                      message:[NSString stringWithFormat:NSLocalizedString(@"Table : %@\nAmount : %@\n%@", nil),
                                               ORDER.order.tableNumber,
                                               [[NSNumber numberWithCurrency:self.payment.netAmount] currencyString],
                                               self.cardNumberTextField.text]
                                     delegate:self
                                          tag:AlertViewTagContinuePayment];
                } else {
                    [UIAlertView askWithTitle:NSLocalizedString(@"Do you want to proceed with payment below?", nil)
                                      message:[NSString stringWithFormat:NSLocalizedString(@"Amount : %@\n%@", nil),
                                               [[NSNumber numberWithCurrency:self.payment.netAmount] currencyString],
                                               self.cardNumberTextField.text]
                                     delegate:self
                                          tag:AlertViewTagContinuePayment];
                }
            }
            
            self.selectedCard = mobileCard;

        } else {
            self.selectedCard = mobileCard;
            if((ORDER.order.isEditable && ORDER.order.orderType == OrderTypeTakeout) ||
               (!ORDER.order.isEditable && ORDER.cart.cartType == CartTypeTakeout)){
                
                [self handleOrder];
                
            } else if((ORDER.order.isEditable && ORDER.order.orderType == OrderTypeDelivery) ||
                      (!ORDER.order.isEditable && ORDER.cart.cartType == CartTypeDelivery)){
                
                [self handleOrder];
                
            } else if((ORDER.order.isEditable && ORDER.order.orderType == OrderTypePickup) ||
                      (!ORDER.order.isEditable && (ORDER.cart.cartType == CartTypePickup || ORDER.cart.cartType == CartTypePickupServiced))){
                
                [self handleOrder];
                
            } else if(!ORDER.order.isEditable && ORDER.cart.cartType == CartTypeCart){
                [self requestNewOrder:self.selectedCard];
            } else {
                [UIAlertView alertWithTitle:NSLocalizedString(@"Broken Order", nil)
                                    message:NSLocalizedString(@"There might be an unordinary function. Pleaes try to order again", nil)];
                PCError(@"(SelectCard)CartType is not Defined %d", ORDER.cart.cartType);
            }
        }
    }    
}


- (void)requestNewOrder:(MobileCard *)card
{
    RequestResult result = RRFail;
    result = [PAY requestNewOrder:ORDER.order
                  completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              [ORDER.order updateWithDictionary:response.data
                                                       merchant:ORDER.merchant
                                                        menuPan:MENUPAN.wholeSerialMenuList];
                              
                              [self configureLineItems];
                              TRANS.dirty = YES;
                              
                              NSString *confirmTitle = NSLocalizedString(@"Confirm To Proceed", nil);
                              if(ORDER.merchant.isTestMode){
                                  confirmTitle = NSLocalizedString(@"[TEST MODE]\nConfirm To Proceed", nil);
                              }
                              
                              if(ORDER.order.tableNumber != nil){
                                  [UIAlertView askWithTitle:confirmTitle
                                                    message:[NSString stringWithFormat:NSLocalizedString(@"Table : %@\nAmount : %@\n%@", nil),
                                                             ORDER.order.tableNumber,
                                                             [[NSNumber numberWithCurrency:self.payment.netAmount] currencyString],
                                                             card.formattedCardNumber]
                                                   delegate:self
                                                        tag:AlertViewTagContinuePayment];
                              } else {
                                  [UIAlertView askWithTitle:confirmTitle
                                                    message:[NSString stringWithFormat:NSLocalizedString(@"Amount : %@\n%@", nil),
                                                             [[NSNumber numberWithCurrency:self.payment.netAmount] currencyString],
                                                             card.formattedCardNumber]
                                                   delegate:self
                                                        tag:AlertViewTagContinuePayment];
                              }
                              
                              self.selectedCard = card;
                            
                              break;
                          }
                          case ResponseErrorUnavailableItems:{
                              [MENUPAN requestMenus:YES
                                            success:^(id response) {
                                                [ORDER.cart isAllItemsInHour:YES];
                                                [self.navigationController popToViewControllerClass:[OrderViewController class] animated:YES];
                                            } andFail:^(NSUInteger errorCode) {
                                                [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to renew Menus",nil)
                                                                    message:NSLocalizedString(@"Sorry, please try again and if it continues, please contact us.",nil)];
                                            }];
                              
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Review Cart Updates", nil)
                                                  message:NSLocalizedString(@"There are item updates in your cart that need your attention before you can check out. Please tap each highlighted item to confirm changes and updates.", nil)];
                              [SVProgressHUD dismiss];
                              
                              
                              break;
                          }
                          case ResponseErrorExists:
                              // TODO: check the money amount. if same go on, or the status is draft update or back and show order
                              [UIAlertView alert:NSLocalizedString(@"The order already exists", nil)];
                              
                              [self backButtonTouched:nil];
                              
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while creating new order", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)requestNewPayment:(MobileCard *)card
{
    if(!ORDER.merchant.isOpenHour){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Sorry, restaurant is currently closed!", nil)
                            message:NSLocalizedString(@"Please feel free to browse the menu, but ordering isn't available until the restaurant opens.", nil)
                  cancleButtonTitle:NSLocalizedString(@"Got it!", nil)];
        
        [SVProgressHUD dismiss];
        return;
    }
    
    if(ORDER.order.orderType == OrderTypeDelivery){
        if(!ORDER.merchant.isDeliveryHour){
            NSString *hourString = [ORDER.merchant nextDeliveryAvailableTime];
            [UIAlertView alertWithTitle:NSLocalizedString(@"Delivery is Currently Unavailable", nil)
                                message:[NSString stringWithFormat:NSLocalizedString(@"Next Available Time:\n%@", nil), [hourString length] > 0 ? hourString : NSLocalizedString(@"N/A", nil)]];
            
            [SVProgressHUD dismiss];
            return;
        }
    }
    
    if(![ORDER.order isAllItemsInHour]){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Review Cart Updates", nil)
                            message:NSLocalizedString(@"There are item updates in your cart that need your attention before you can check out. Please tap each highlighted item to confirm changes and updates.", nil)];
        [SVProgressHUD dismiss];
        return;
    }
    
    [self.payment updateCardInfo:card];
    self.payment.orderNo = ORDER.order.orderNo;
    
    if(ORDER.merchant.isAllowTip){
        float tipRate = MIN(20.0f, roundf(((float)self.payment.tipAmount / (float)self.payment.subTotal) * 100));
        switch(ORDER.order.orderType){
            case OrderTypeDelivery:
                [NSUserDefaults standardUserDefaults].tipDeliveryRate = tipRate;
                break;
            case OrderTypeTakeout:
                [NSUserDefaults standardUserDefaults].tipTakeoutRate = tipRate;
                break;
            case OrderTypeBill:
            case OrderTypeCart:
            case OrderTypePickup:
            default:
                [NSUserDefaults standardUserDefaults].tipRate = tipRate;
                break;
        }
    }
    
    
    
#if FLURRY_ENABLED
    [Flurry logEvent:@"Send Payment(N)" withParameters:@{@"RestaurantName":ORDER.merchant.name,
                                                      @"RestaurantId":[NSNumber numberWithSerial:ORDER.merchant.merchantNo],
                                                      @"orderType":[Order typeStringForOrderType:ORDER.order.orderType],
                                                      @"orderTotal":[NSNumber numberWithCurrency:ORDER.order.subTotal]}];
#endif
    
#if FBAPPEVENT_ENABLED
    [FBSDKAppEvents logEvent:FBSDKAppEventNameInitiatedCheckout
               valueToSum:(self.payment.payAmount / 100.0f)
               parameters:@{ FBSDKAppEventParameterNameCurrency    : @"USD",
                             FBSDKAppEventParameterNameContentType : @"food",
                             @"UUID" : (CRED.udid ? CRED.udid : @"Unknown")}];
#endif
    
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    BTCardClient *cardClient = nil;
    BTCard *btCard = nil;
    
    if(self.braintreeClient != nil){
        cardClient = [[BTCardClient alloc] initWithAPIClient:self.braintreeClient];
        btCard = [[BTCard alloc] initWithNumber:self.payment.cardNumber
                                expirationMonth:[NSString stringWithFormat:@"%ld",(long)self.payment.cardExpireMonth]
                                 expirationYear:[NSString stringWithFormat:@"%ld",(long)self.payment.cardExpireYear]
                                            cvv:self.payment.cardCvc];
        btCard.postalCode = self.payment.zipCode;
        
        [cardClient tokenizeCard:btCard
                      completion:^(BTCardNonce *tokenizedCard, NSError *error) {
                          if(error == nil){
                             [self requestPayment:tokenizedCard
                                         withCard:card];
                          } else {
                              [SVProgressHUD dismiss];
                              [UIAlertView alertWithTitle:error.localizedDescription
                                                  message:error.localizedRecoverySuggestion];
                              return;
                          }
                      }];
    } else {
        [self requestPayment:nil
                    withCard:card];
    }
}

- (void)requestPayment:(BTCardNonce *)tokenizedCard withCard:(MobileCard *)card{
    RequestResult result = RRFail;
    
    result = [PAY requestNewPayment:ORDER.order.orderNo
                            payment:self.payment
                   lineItemJsonList:self.lineItemJsonList
                        deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                   appliedDiscounts:[self.payment appliedDiscountsDictsWithSmallCutAmount:self.paymentSummaryView.smallCutPromotion.appliedDiscountAmount]
                     smallCutAmount:self.paymentSummaryView.smallCutPromotion.appliedDiscountAmount
                 paymentMethodNonce:tokenizedCard.nonce
                    completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              CRED.dirtyCredit = YES;
                              
                              if(CRED.isUnattendedSignUp){
                                  [SWITCH removeLocalData];
                              }
                              
#if FBAPPEVENT_ENABLED
                              [FBSDKAppEvents logPurchase:(self.payment.payAmount / 100.0f)
                                                 currency:@"USD"
                                               parameters:@{ FBSDKAppEventParameterNameCurrency    : @"USD",
                                                             FBSDKAppEventParameterNameContentType : @"food",
                                                             @"UUID" : (CRED.udid ? CRED.udid : @"Unknown")}];
#endif
                              
                              
#if FIREBASE_ENABLED
                              id tracker = [[GAI sharedInstance] defaultTracker];
                              
                              
                              
                              [tracker send:[[GAIDictionaryBuilder createTransactionWithId:(CRED.udid ? CRED.udid : @"Unknown")             // (NSString) Transaction ID
                                                                               affiliation:@"Food"         // (NSString) Affiliation
                                                                                   revenue:@(self.payment.payAmount / 100.0f)                  // (NSNumber) Order revenue (including tax and shipping)
                                                                                       tax:@(ORDER.cart.taxAmount)                  // (NSNumber) Tax
                                                                                  shipping:@(self.payment.deliveryFeeAmount)                  // (NSNumber) Shipping
                                                                              currencyCode:@"USD"] build]];        // (NSString) Currency code
                              
                              
                              //    [tracker send:[[GAIDictionaryBuilder createItemWithTransactionId:@"0_123456"         // (NSString) Transaction ID
                              //                                                                name:@"Space Expansion"  // (NSString) Product Name
                              //                                                                 sku:@"L_789"            // (NSString) Product SKU
                              //                                                            category:@"Game expansions"  // (NSString) Product category
                              //                                                               price:@1.9F               // (NSNumber) Product price
                              //                                                            quantity:@1                  // (NSInteger) Product quantity
                              //                                                        currencyCode:@"USD"] build]];    // (NSString) Currency code
#endif

                              
#if FIREBASE_ENABLED
                              [FIRAnalytics logEventWithName:kFIREventEcommercePurchase
                                                  parameters:@{
                                                               kFIRParameterCurrency:@"USD",
                                                               kFIRParameterValue:@(self.payment.payAmount / 100.0f)
                                                               }];
#endif
                              if(ORDER.order.isTicketBase){
                                  ORDER.order.status = OrderStatusPaid;
                              }
                              
                              self.mobileCard = card;
                              
                              if(CRED.isSignedIn){
                                  REMOTE.ordersDirty = YES;
                                  REMOTE.cardDirty = YES;
                              } else {
                                  [ORDER.merchant save];
                                  [ORDER.order save];
                              }
                              
                              if(ORDER.order.orderType == OrderTypeDelivery){
                                  Addresses *address = [[Addresses alloc] initWithOrder:ORDER.order];
                                  address.coordinate = self.address.coordinate;
                                  [address save];
                              }
                              
                              TRANS.dirty = YES;
                              
                              self.payment.order = ORDER.order;
                              
                              Receipt *receipt = [[Receipt alloc] initWithDictionary:response.data];
                              NSDictionary *paymentDict = [response.data objectForKey:@"payment"];
                              self.payment = receipt.payment;
                              if(!CRED.isSignedIn){
                                  [receipt save];
                                  [self.payment save];
                              }
                              
                              if([paymentDict objectForKey:@"card_id"] != nil){
                                  [card updateWithPaymentDictionary:paymentDict];
                              }
                              
                              if(!CRED.isSignedIn){
                                  if(card != nil){
                                      if(!ORDER.merchant.isTestMode){
                                          [card save];
                                      }
                                  }
                              }
                              
                              
                              [self moveOrdersTap];
                              
                              
                          }
                              break;
                          case ResponseErrorOutOfMenuHours:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              NSString *title = [response objectForKey:@"title"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alertWithTitle:title
                                                      message:message];
                              }
                              break;
                          }
                          case ResponseErrorItemsAlreadyPaid:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Some Items Are Already Paid", nil)
                                                  message:NSLocalizedString(@"Some items you try to pay have been paid by others. Please select items you want to pay again", nil)];
                              break;
                          case ResponseErrorCardError:
                          case ResponseErrorPaymentError:{
                              NSString *param = [[response objectForKey:@"message"] objectForKey:@"param"];
                              if([param isEqualToString:@"amount"]){
                                  [UIAlertView alertWithTitle:NSLocalizedString(@"Minimum Order Value", nil)
                                                      message:NSLocalizedString(@"Minimum payment amount must be at least 50c", nil)
                                                     delegate:self
                                                          tag:AlertViewTagStripeError];
                              } else {
                                  //                                  NSString *message = [[response objectForKey:@"message"] objectForKey:@"message"];
                                  //                                  [UIAlertView alertWithTitle:UNDEFINED_ALERT_TITLE
                                  //                                                      message:message
                                  //                                                     delegate:self
                                  //                                                          tag:AlertViewTagStripeError];
                                  
                                  [UIAlertView alertWithTitle:NSLocalizedString(@"Your card information is not correct.", nil)
                                                      message:nil
                                                     delegate:self
                                                          tag:AlertViewTagStripeError];
                                  
                              }
                          }
                              break;
                          case ResponseErrorFingerprintError:{
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Your Card is Not Valid", nil)
                                                  message:NSLocalizedString(@"Check your card information.", nil)
                                                 delegate:self
                                                      tag:AlertViewTagStripeError];
                          }
                              break;
                          case ResponseErrorInvalidOrder:{
                              [UIAlertView alertWithTitle:NSLocalizedString(@"This Order is Not Valid", nil)
                                                  message:NSLocalizedString(@"This Order may be removed or completed with some reasons. Please ask the restaurant what to do.", nil)
                                                 delegate:self
                                                      tag:AlertViewTagInvalidOrderError];
                              
                              TRANS.dirty = YES;
                              
                              break;
                          }
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while making payment", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            [SVProgressHUD dismiss];
            break;
        default:
            [SVProgressHUD dismiss];
            break;
    }
}

- (void)requestPickupOrder:(MobileCard *)card
           requestMesssage:(NSString *)requestMessage
              receiverName:(NSString *)receiverName
{
    if([requestMessage length] > 0){
        [CustomRequest updateCustomRequest:requestMessage
                                      type:CustomRequestTypeDineIn
                                merchantNo:ORDER.merchant.merchantNo];
    }
    
    RequestResult result = RRFail;
    result = [MENU requestPickupOrder:ORDER.cart.lineItems
                           merchantNo:ORDER.merchant.merchantNo
                           flagNumber:ORDER.cart.flagNumber
                         receiverName:ORDER.cart.cartType == CartTypePickupServiced ? nil : receiverName
                      customerRequest:requestMessage
                          deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                            authToken:CRED.customerInfo.authToken
                       subTotalAmount:ORDER.cart.amount
                subTotalAmountTaxable:ORDER.cart.subtotalAmountTaxable
                            taxAmount:ORDER.cart.taxAmount
                      completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              [ORDER.order updateWithDictionary:response.data
                                                       merchant:ORDER.merchant
                                                        menuPan:MENUPAN.wholeSerialMenuList];
                              
                              [self configureLineItems];
                              TRANS.dirty = YES;
                              
                              [ORDER.cart delete];
                              ORDER.cart = nil;
                              self.shouldBackToRoot = YES;
                              
                              [self requestNewPayment:card];
                              
                              break;
                          }
                          case ResponseErrorUnavailableItems:{
                              [MENUPAN requestMenus:YES
                                            success:^(id response) {
                                                [ORDER.cart isAllItemsInHour:YES];
                                                [self.navigationController popToViewControllerClass:[OrderViewController class] animated:YES];
                                            } andFail:^(NSUInteger errorCode) {
                                                [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to renew Menus",nil)
                                                                    message:NSLocalizedString(@"Sorry, please try again and if it continues, please contact us.",nil)];
                                            }];
                              
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Review Cart Updates", nil)
                                                  message:NSLocalizedString(@"There are item updates in your cart that need your attention before you can check out. Please tap each highlighted item to confirm changes and updates.", nil)];
                              [SVProgressHUD dismiss];
                              
                              
                              break;
                          }
                          case ResponseErrorNotSupportOrderType:{
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Does not support dine-in order", nil)
                                                  message:NSLocalizedString(@"This restaurant does not take dine-in order no more. Try to order later or order at other restaurants.", nil)];
                          }
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                              [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while making ordering", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  if(response.errorCode != ResponseSuccess){
                      [SVProgressHUD dismiss];
                  }
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}


- (void)requestTakeoutOrder:(MobileCard *)card
                 afterTime:(NSInteger)afterTime
           requestMesssage:(NSString *)requestMessage
              receiverName:(NSString *)receiverName
             receiverPhone:(NSString *)receiverPhone
{
    
    if([requestMessage length] > 0){
        [CustomRequest updateCustomRequest:requestMessage
                                      type:CustomRequestTypeTakeout
                                merchantNo:ORDER.merchant.merchantNo];
    }
    
    RequestResult result = RRFail;
    result = [MENU requestTakeoutOrder:ORDER.cart.lineItems
                            merchantNo:ORDER.merchant.merchantNo
                           pickupAfter:afterTime
                       customerRequest:requestMessage
                          receiverName:receiverName
                         receiverPhone:receiverPhone
                           deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                             authToken:CRED.customerInfo.authToken
                        subTotalAmount:ORDER.cart.amount
                 subTotalAmountTaxable:ORDER.cart.subtotalAmountTaxable
                             taxAmount:ORDER.cart.taxAmount
                       completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              [ORDER.order updateWithDictionary:response.data
                                                       merchant:ORDER.merchant
                                                        menuPan:MENUPAN.wholeSerialMenuList];
                              
                              [self configureLineItems];
                              TRANS.dirty = YES;
                              
                              [ORDER.cart delete];
                              ORDER.cart = nil;
                              self.shouldBackToRoot = YES;
                              
                              [self requestNewPayment:card];
                              
                              break;
                          }
                          case ResponseErrorUnavailableItems:{
                              [MENUPAN requestMenus:YES
                                            success:^(id response) {
                                                [ORDER.cart isAllItemsInHour:YES];
                                                [self.navigationController popToViewControllerClass:[OrderViewController class] animated:YES];
                                            } andFail:^(NSUInteger errorCode) {
                                                [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to renew Menus",nil)
                                                                    message:NSLocalizedString(@"Sorry, please try again and if it continues, please contact us.",nil)];
                                            }];
                              
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Review Cart Updates", nil)
                                                  message:NSLocalizedString(@"There are item updates in your cart that need your attention before you can check out. Please tap each highlighted item to confirm changes and updates.", nil)];
                              [SVProgressHUD dismiss];
                              
                              
                              break;
                          }
                          case ResponseErrorNotSupportOrderType:{
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Does not support take-out order", nil)
                                                  message:NSLocalizedString(@"This restaurant does not take take-out order no more. Try to order later or order at other restaurants.", nil)];
                          }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while making ordering", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  if(response.errorCode != ResponseSuccess){
                      [SVProgressHUD dismiss];
                  }
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)requestDeliveryOrder:(MobileCard *)card
                   afterTime:(NSInteger)afterTime
             requestMesssage:(NSString *)requestMessage
                addressState:(NSString *)state
                 addressCity:(NSString *)city
              addressStreet1:(NSString *)street1
              addressStreet2:(NSString *)street2
                  addressZip:(NSString *)zip
                 phoneNumber:(NSString *)phoneNumber
                receiverName:(NSString *)receiverName
           deliveryFeeAmount:(PCCurrency)deliveryFeeAmount
          roServiceFeeAmount:(PCCurrency)roServiceFeeAmount
{
    
    if([requestMessage length] > 0){
        [CustomRequest updateCustomRequest:requestMessage
                                      type:CustomRequestTypeDelivery
                                merchantNo:ORDER.merchant.merchantNo];
    }
    
    RequestResult result = RRFail;
    result = [MENU requestDeliveryOrder:ORDER.cart.lineItems
                             merchantNo:ORDER.merchant.merchantNo
                            pickupAfter:afterTime
                        customerRequest:requestMessage
                            deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                              authToken:CRED.customerInfo.authToken
                         subTotalAmount:ORDER.cart.amount
                  subTotalAmountTaxable:ORDER.cart.subtotalAmountTaxable
                              taxAmount:ORDER.cart.taxAmount
                           addressState:state
                            addressCity:city
                         addressStreet1:street1
                         addressStreet2:street2
                             addressZip:zip
                            phoneNumber:phoneNumber
                           receiverName:receiverName
                      deliveryFeeAmount:deliveryFeeAmount
                     roServiceFeeAmount:roServiceFeeAmount
                        completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              [ORDER.order updateWithDictionary:response.data
                                                       merchant:ORDER.merchant
                                                        menuPan:MENUPAN.wholeSerialMenuList];
                              
                              [self configureLineItems];
                              TRANS.dirty = YES;
                              
                              [ORDER.cart delete];
                              ORDER.cart = nil;
                              self.shouldBackToRoot = YES;
                              
                              [self requestNewPayment:card];
                              
                              break;
                          }
                          case ResponseErrorUnavailableItems:{
                              [MENUPAN requestMenus:YES
                                            success:^(id response) {
                                                [ORDER.cart isAllItemsInHour:YES];
                                                [self.navigationController popToViewControllerClass:[OrderViewController class] animated:YES];
                                            } andFail:^(NSUInteger errorCode) {
                                                [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to renew Menus",nil)
                                                                    message:NSLocalizedString(@"Sorry, please try again and if it continues, please contact us.",nil)];
                                            }];
                              
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Review Cart Updates", nil)
                                                  message:NSLocalizedString(@"There are item updates in your cart that need your attention before you can check out. Please tap each highlighted item to confirm changes and updates.", nil)];
                              [SVProgressHUD dismiss];
                              
                              
                              break;
                          }
                          case ResponseErrorNotSupportOrderType:{
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Does not support delivery order", nil)
                                                  message:NSLocalizedString(@"This restaurant does not take delivery order no more. Try to order later or order at other restaurants.", nil)];
                          }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while making ordering", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  if(response.errorCode != ResponseSuccess){
                      [SVProgressHUD dismiss];
                  }
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)requestRenewOrder:(Order *)order
                 withCard:(MobileCard *)card
{
    RequestResult result = RRFail;
    result = [MENU requestRenewOrder:ORDER.order
                         deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                           authToken:CRED.customerInfo.authToken
                        completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              [ORDER.order updateWithDictionary:response.data
                                                       merchant:ORDER.merchant
                                                        menuPan:MENUPAN.wholeSerialMenuList];
                              
                              TRANS.dirty = YES;
                              
                              // There must not be cart here. But for safty, remove cart
                              [ORDER.cart delete];
                              ORDER.cart = nil;
                              
                              self.shouldBackToRoot = YES;
                              
                              [self requestNewPayment:card];
                              
                              break;
                          }
                          case ResponseErrorUnavailableItems:{
                              [MENUPAN requestMenus:YES
                                            success:^(id response) {
                                                [ORDER.cart isAllItemsInHour:YES];
                                                [self.navigationController popToViewControllerClass:[OrderViewController class] animated:YES];
                                            } andFail:^(NSUInteger errorCode) {
                                                [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to renew Menus",nil)
                                                                    message:NSLocalizedString(@"Sorry, please try again and if it continues, please contact us.",nil)];
                                            }];
                              
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Review Cart Updates", nil)
                                                  message:NSLocalizedString(@"There are item updates in your cart that need your attention before you can check out. Please tap each highlighted item to confirm changes and updates.", nil)];
                              [SVProgressHUD dismiss];
                              
                              
                              break;
                          }
                          case ResponseErrorSaveFail:{
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to place the order", nil)
                                                  message:NSLocalizedString(@"Please try later or contact us", nil)];
                          }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while making ordering", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  if(response.errorCode != ResponseSuccess){
                      [SVProgressHUD dismiss];
                  }
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)takeoutViewController:(TakeoutViewController *)viewController
     didTouchPlaceOrderButton:(id)sender
{
    self.afterMinute = viewController.afterMinute;
    
    if(viewController.asapButton.selected){
        self.afterMinute = 0;
    }
    
    self.receiverName = viewController.orderNameField.text;
    self.phoneNumber = viewController.phoneNumberTextField.text;
    self.requestMessage = viewController.additionalRequest.text;
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [UIAlertView askWithTitle:NSLocalizedString(@"Do you want to proceed with payment below?", nil)
                                                   message:[NSString stringWithFormat:NSLocalizedString(@"Amount : %@\n%@", nil),
                                                            [[NSNumber numberWithCurrency:self.payment.netAmount] currencyString],
                                                            self.cardNumberTextField.text]
                                                  delegate:self
                                                       tag:(ORDER.order.orderNo > 0 && !ORDER.order.isEditable) ? AlertViewTagContinuePayment : AlertViewTagContinueOrderPayment];
                             }];
}

- (void)takeoutViewController:(TakeoutViewController *)viewController
         didTouchSignInButton:(id)sender
{
    self.afterMinute = viewController.afterMinute;
    
    if(viewController.asapButton.selected){
        self.afterMinute = 0;
    }
    
    self.receiverName = viewController.orderNameField.text;
    self.phoneNumber = viewController.phoneNumberTextField.text;
    self.requestMessage = viewController.additionalRequest.text;
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [self showSignIn:NO
                                     withDelegate:self];
                             }];
}

- (void)dineinViewController:(DineinViewController *)viewController
     didTouchPlaceOrderButton:(id)sender
{
    self.requestMessage = viewController.additionalRequest.text;
    self.receiverName = viewController.orderNameField.text;
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [UIAlertView askWithTitle:NSLocalizedString(@"Do you want to proceed with payment below?", nil)
                                                   message:[NSString stringWithFormat:NSLocalizedString(@"Amount : %@\n%@", nil),
                                                            [[NSNumber numberWithCurrency:self.payment.netAmount] currencyString],
                                                            self.cardNumberTextField.text]
                                                  delegate:self
                                                       tag:(ORDER.order.orderNo > 0 && !ORDER.order.isEditable) ? AlertViewTagContinuePayment : AlertViewTagContinueOrderPayment];
                             }];
}

- (void)dineinViewController:(DineinViewController *)viewController
        didTouchSignInButton:(id)sender
{
    self.requestMessage = viewController.additionalRequest.text;
    self.receiverName = viewController.orderNameField.text;
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [self showSignIn:NO
                                     withDelegate:self];
                             }];
}


- (void)deliveryViewController:(DeliveryViewController *)viewController
      didTouchPlaceOrderButton:(id)sender
             addressDictionary:(NSDictionary *)dictionary
{
    self.afterMinute = viewController.afterMinute;
    
    if(viewController.asapButton.selected){
        self.afterMinute = 0;
    }
    
    self.requestMessage = viewController.additionalRequest.text;
    
    self.address = [[Addresses alloc] init];
    
    self.address.state = viewController.stateTextField.text;
    self.address.city = viewController.cityTextField.text;
    self.address.address1 = viewController.address1TextField.text;
    self.address.address2 = viewController.address2TextField.text;
    self.address.zip = viewController.zipCodeTextField.text;
    self.address.phoneNumber = viewController.phoneNumberTextField.text;
    self.address.receiverName = viewController.nameTextField.text;
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [self handleOrder];
                             }];
}

- (void)handleOrder
{
    if(ORDER.order.orderNo > 0 && !ORDER.order.isEditable){ //Order exists
        [self requestNewPayment:self.selectedCard];
    } else {
        [UIAlertView askWithTitle:NSLocalizedString(@"Do you want to proceed with payment below?", nil)
                          message:[NSString stringWithFormat:NSLocalizedString(@"Amount : %@\n%@", nil),
                                   [[NSNumber numberWithCurrency:self.payment.netAmount] currencyString],
                                   self.selectedCard.formattedCardNumber]
                         delegate:self
                              tag:AlertViewTagContinueOrderPayment];
    }
}

- (IBAction)removeButtonTouched:(id)sender
{
    if(self.mobileCard != nil){
        [UIAlertView askWithTitle:NSLocalizedString(@"Confirm To Proceed", nil)
                          message:[NSString stringWithFormat:NSLocalizedString(@"Are you sure?\n%@", nil),
                                   self.cardNumberTextField.text]
                         delegate:self
                              tag:AlertViewTagcontinueRemove];
    }
}

- (void)moveOrdersTap
{
    if(self.tabBarController.selectedIndex == 0 || self.tabBarController.selectedIndex == 1 || self.tabBarController.selectedIndex == 2){
        ORDER.shouldMoveToOrders = YES;
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark - PCCardNumberTextFieldDelegate
- (void)cardNumberTextField:(PCCardNumberTextField *)textField
            didChangeIssuer:(Issuer)issuer
               issuerString:(NSString *)string
{
    self.issuerLabel.text = string;
    self.issuerLogo.image = [UIImage logoImageByIssuer:issuer];
    
    if(self.issuerLogo.image == nil){
        self.issuerLogo.image = [UIImage imageNamed:@"icon_card.png"];
    }
}


#pragma mark - UIPickerViewDelegate & DataSource
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch(component){
        case 0:
            return [self.monthList count];
            break;
        case 1:
            return [self.yearList count];
            break;
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch(component){
        case 0:{ //Month
            NSNumber *month = [self.monthList objectAtIndex:row];
            return [NSString stringWithFormat:@"%02ld", (long)[month integerValue]];
            break;
        }
        case 1:{ //Year
            NSNumber *year = [self.yearList objectAtIndex:row];
            return [NSString stringWithFormat:@"%ld", (long)[year integerValue]];
            break;
        }
    }
    return @"???";
}

- (NSArray *)monthList
{
    if(_monthList == nil){
        NSUInteger i = 0;
        NSMutableArray *monthArray = [NSMutableArray array];
        for(i = 0 ; i < 12 ; i++){
            [monthArray addObject:[NSNumber numberWithUnsignedInteger:i+1]];
        }
        _monthList = [NSArray arrayWithArray:monthArray];
    }
    return _monthList;
}

- (NSArray *)yearList
{
#define VALID_YEAR_SPAN 9
    if(_yearList == nil){
        NSDateComponents *components = [[NSCalendar currentCalendar]
                                        components:NSCalendarUnitYear
                                        fromDate:[NSDate date]];
        
        NSUInteger i = 0;
        NSMutableArray *yearArray = [NSMutableArray array];
        for(i = components.year ; i < components.year + VALID_YEAR_SPAN ; i++){
            [yearArray addObject:[NSNumber numberWithUnsignedInteger:i]];
        }
        _yearList = [NSArray arrayWithArray:yearArray];
    }
    
    return _yearList;
}

- (IBAction)monthPickerDoneButtonTouched:(id)sender
{    
    [self.expirationMonthTextField resignFirstResponder];
    
    NSInteger monthRow = [self.monthYearPickerView selectedRowInComponent:0];
    NSInteger yearRow = [self.monthYearPickerView selectedRowInComponent:1];
    
    NSNumber *month = [self.monthList objectAtIndex:monthRow];
    NSNumber *year = [self.yearList objectAtIndex:yearRow];
    
    self.expirationMonthTextField.text = [NSString stringWithFormat:@"%02ld/%02ld",
                                          (long)[month integerValue],
                                          [year integerValue] - 2000];
    
//    if([self.securityCodeTextField.text length] == 0){
        [self.securityCodeTextField becomeFirstResponder];
//    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if(textField == self.expirationMonthTextField){
        [self monthPickerDoneButtonTouched:textField];
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    NSInteger monthRow = 0;
    NSInteger yearRow = 0;
    
    if([self.expirationMonthTextField.text length] > 0){
        NSString *monthYearString = self.expirationMonthTextField.text;
        NSString *monthString = [monthYearString substringWithRange:NSMakeRange(0, 2)];
        NSString *yearString = [monthYearString substringFromIndex:3];
        monthRow = [monthString integerValue] - 1;
        
        NSInteger year = [yearString integerValue] + 2000;
        
        yearRow = [self.yearList indexOfObject:[NSNumber numberWithInteger:year]];
        [self.monthYearPickerView selectRow:yearRow
                                inComponent:1
                                   animated:YES];
    }
    
    [self.monthYearPickerView selectRow:monthRow
                            inComponent:0
                               animated:YES];
    
    [self.monthYearPickerView selectRow:yearRow
                            inComponent:1
                               animated:YES];
    
    return YES;
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case AlertViewTagTipInput:
            [self.paymentSummaryView.tipTextField becomeFirstResponder];
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case AlertViewTagStripeError:
            break;
        case AlertViewTagInvalidOrderError:
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        case AlertViewTagContinuePayment:
            if(buttonIndex == AlertButtonIndexYES){
                [self requestNewPayment:self.selectedCard];
            }
            break;
        case AlertViewTagContinueOrderPayment:
            if(buttonIndex == AlertButtonIndexYES){
                if(ORDER.order.isEditable){
                    
                    ORDER.order.deliveryFee = self.payment.deliveryFeeAmount;
                    ORDER.order.roServiceFee = self.payment.roServiceFeeAmount;
                    ORDER.order.pickupAfter = self.afterMinute;
                    ORDER.order.customerRequest = self.requestMessage;
                    ORDER.order.state = self.address.state;
                    ORDER.order.city = self.address.city;
                    ORDER.order.address1 = self.address.address1;
                    ORDER.order.address2 = self.address.address2;
                    ORDER.order.zip = self.address.zip;
                    ORDER.order.phoneNumber = self.address.phoneNumber;
                    
                    if(ORDER.cart.cartType == CartTypePickupServiced){
                        ORDER.order.tableNumber = ORDER.cart.flagNumber;
                        ORDER.order.receiverName = nil;
                    } else {
                        ORDER.order.receiverName = self.address.receiverName;
                        ORDER.order.tableNumber = nil;
                    }
                    
                    [self requestRenewOrder:ORDER.order
                                   withCard:self.selectedCard];
                    
                } else {
                    
                    if(!ORDER.merchant.isOpenHour){
                        [UIAlertView alertWithTitle:NSLocalizedString(@"Sorry, restaurant is currently closed!", nil)
                                            message:NSLocalizedString(@"Please feel free to browse the menu, but ordering isn't available until the restaurant opens.", nil)
                                  cancleButtonTitle:NSLocalizedString(@"Got it!", nil)];
                        return;
                    }
                    
                    if(ORDER.order.orderType == OrderTypeDelivery){
                        if(!ORDER.merchant.isDeliveryHour){
                            NSString *hourString = [ORDER.merchant nextDeliveryAvailableTime];
                            [UIAlertView alertWithTitle:NSLocalizedString(@"Delivery is Currently Unavailable", nil)
                                                message:[NSString stringWithFormat:NSLocalizedString(@"Next Available Time:\n%@", nil), [hourString length] > 0 ? hourString : NSLocalizedString(@"N/A", nil)]];
                            
                            return;
                        }
                    }
                    
                    if(![ORDER.cart isAllItemsInHour]){
                        [UIAlertView alertWithTitle:NSLocalizedString(@"Review Cart Updates", nil)
                                            message:NSLocalizedString(@"There are item updates in your cart that need your attention before you can check out. Please tap each highlighted item to confirm changes and updates.", nil)];
                        [SVProgressHUD dismiss];
                        
                        [self.navigationController popToViewControllerClass:[OrderViewController class] animated:YES];
                        return;
                    }
                    
                    switch(ORDER.cart.cartType){
                        case CartTypeTakeout:
                            [self requestTakeoutOrder:self.selectedCard
                                           afterTime:self.afterMinute
                                     requestMesssage:self.requestMessage
                                        receiverName:self.address.receiverName
                                       receiverPhone:self.address.phoneNumber];
                            break;
                        case CartTypeDelivery:
                            [self requestDeliveryOrder:self.selectedCard
                                             afterTime:self.afterMinute
                                       requestMesssage:self.requestMessage
                                          addressState:self.address.state
                                           addressCity:self.address.city
                                        addressStreet1:self.address.address1
                                        addressStreet2:self.address.address2
                                            addressZip:self.address.zip
                                           phoneNumber:self.address.phoneNumber
                                          receiverName:self.address.receiverName
                                     deliveryFeeAmount:self.payment.deliveryFeeAmount
                                    roServiceFeeAmount:self.payment.roServiceFeeAmount];
                            break;
                        case CartTypePickup:
                        case CartTypePickupServiced:
                            [self requestPickupOrder:self.selectedCard
                                     requestMesssage:self.requestMessage
                                        receiverName:self.address.receiverName];
                            break;
                        case CartTypeCart: //Should not be this case
                        default:
                            [UIAlertView alertWithTitle:NSLocalizedString(@"Broken Order", nil)
                                                message:NSLocalizedString(@"There might be an unordinary function. Pleaes try to order again", nil)];
                            PCError(@"(SelectCard)CartType is not Defined %d", ORDER.cart.cartType);
                            break;
                    }
                }
            }
            
            break;
        case AlertViewTagContinueSave:
            if(buttonIndex == AlertButtonIndexYES){
                [self requestSaveCard:self.mobileCard];
            }
            break;
        case AlertViewTagcontinueRemove:
            if(buttonIndex == AlertButtonIndexYES){
                if([self.mobileCard delete]){
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
            break;
        case AlertViewTagPromoOrderAmount:
            if(buttonIndex == AlertButtonIndexYES){
                [self backButtonTouched:nil];
            }
            break;
    }
}

- (void)requestSaveCard:(MobileCard *)card
{
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    BTCardClient *cardClient = nil;
    BTCard *btCard = nil;
    
    if(self.braintreeClient != nil){
        cardClient = [[BTCardClient alloc] initWithAPIClient:self.braintreeClient];
        btCard = [[BTCard alloc] initWithNumber:card.cardNumber
                                        expirationMonth:[NSString stringWithFormat:@"%ld",(long)card.cardExpireMonth]
                                         expirationYear:[NSString stringWithFormat:@"%ld",(long)card.cardExpireYear]
                                                    cvv:card.cardCvc];
        btCard.postalCode = card.zipCode;
        
        [cardClient tokenizeCard:btCard
                      completion:^(BTCardNonce *tokenizedCard, NSError *error) {
                          if(error == nil){
                              [self requestRegisterCard:tokenizedCard
                                               withCard:card];
                          } else {
                              [SVProgressHUD dismiss];
                              [UIAlertView alertWithTitle:error.localizedDescription
                                                  message:error.localizedRecoverySuggestion];
                              
                          }
                      }];
    } else {
        [self requestRegisterCard:nil
                         withCard:card];
    }
}

- (void)requestRegisterCard:(BTCardNonce *)tokenizedCard withCard:(MobileCard *)card{
    RequestResult result = RRFail;
    result = [PAY requestCardRegistration:card
                       paymentMethodNonce:tokenizedCard.nonce
                          completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              if(CRED.isSignedIn){
                                  [REMOTE handleCardsResponse:(NSMutableArray *)response
                                             withNotification:YES];
                                  [self.navigationController popViewControllerAnimated:YES];
                              } else {
                                  if([response count] > 0){
                                      self.mobileCard = [response.listData objectAtIndex:0];
                                  }
                                  
                                  if([self.mobileCard save]){
                                      [self.navigationController popViewControllerAnimated:YES];
                                  } else {
                                      [UIAlertView alert:NSLocalizedString(@"Error has occurred while saving card information.", nil)];
                                  }
                              }
                              break;
                          case ResponseErrorCardError:{
                              NSString *message = [[response objectForKey:@"message"] objectForKey:@"message"];
                              NSString *title = [[response objectForKey:@"message"] objectForKey:@"type"];
                              [UIAlertView alertWithTitle:title
                                                  message:message
                                                 delegate:self
                                                      tag:AlertViewTagStripeError];
                          }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while verifying card.", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            [SVProgressHUD dismiss];
            break;
        default:
            [SVProgressHUD dismiss];
            break;
    }

}

- (IBAction)cardNumberNextButtonTouched:(id)sender
{
    [self.expirationMonthTextField becomeFirstResponder];
}

- (void)paymentSummaryView:(PaymentSummaryView *)summaryView
 didTouchApplyCouponButton:(id)sender
{
    if(CRED.isSignedIn){
        
        NSString *enteredCouponCode = self.paymentSummaryView.couponCodeTextField.text;
        
        for(Promotion *promotion in self.payment.appliedPromotions){
            if(promotion.isRequiredCode){
                if([promotion.couponCode compare:enteredCouponCode
                                         options:NSCaseInsensitiveSearch] == NSOrderedSame){
                    [UIAlertView alertWithTitle:NSLocalizedString(@"Unable to Apply Code", nil)
                                        message:NSLocalizedString(@"Promo code has already been applied to this order.", nil)];
                    return;
                }
            }
        }
        
        [self validatePromotionWithCode:enteredCouponCode];
    } else {
        self.promotionCodeForCheckAfterSigningIn = self.paymentSummaryView.couponCodeTextField.text;
        [self showSignIn:YES];
    }
}


- (void)showSignIn
{
    return [self showSignIn:NO withDelegate:nil];
}

- (void)showSignIn:(BOOL)showGuide
{
    return [self showSignIn:showGuide
               withDelegate:nil];
}

- (void)showSignIn:(BOOL)showGuide withDelegate:(id<SignInCustomerViewControllerDelegate>)delegate
{
    SignInCustomerViewController *viewController = [SignInCustomerViewController viewControllerFromNib];
    viewController.showGuide = showGuide;
    viewController.delegate = delegate;
    [self.tabBarController presentViewControllerInNavigation:viewController
                                                     animated:YES
                                                   completion:^{
                                                   }];
}

- (void)signInCustomerViewControllerNeedDissmissingViewController:(SignInCustomerViewController *)viewController
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [self payButtonTouched:viewController];
                             }];
}

- (void)validatePromotionWithCode:(NSString *)code
{
    RequestResult result = RRFail;
    self.promotionCodeForCheckAfterSigningIn = nil;
    
    result = [PAY requestValidatePromoCode:code
                           completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              if([response objectForKey:@"rewarded_credit"] != nil){
                                  // This is Referall code response
                                  
                                  CRED.customerInfo.credit = [response integerForKey:@"credit_balance"];
                                  self.paymentSummaryView.couponCodeTextField.text = nil;
                                  
//                                  [UIAlertView alertWithTitle:NSLocalizedString(@"Congratulations!", nil)
//                                                      message:NSLocalizedString(@"You've earned 500 Oranges. You can spend your Oranges like money! Have fun!", nil)];
                                  
                                  [self arrangePaymentSummaryView];
                                  if(CRED.customerInfo.credit > 0){
                                      [self selectUseOranges];
                                  }
                                  
                                  // Check First time use Promotion and remove it
//                                  Promotion *firstTimeUsePromotion = nil;
//                                  for(Promotion *promotion in self.payment.appliedPromotions){
//                                      if(promotion.isFirstTimeUse){
//                                          firstTimeUsePromotion = promotion;
//                                          break;
//                                      }
//                                  }
//                                  
//                                  if(firstTimeUsePromotion != nil){
//                                      if ([self.payment removePromotion:firstTimeUsePromotion] == PromotionErrorSuccess){
//                                          [UIAlertView alertWithTitle:NSLocalizedString(@"First Time Use Coupon cannot be applied with Referral Program", nil)
//                                                              message:NSLocalizedString(@"So, we gave you Orange Point by Referral Program but we removed First Time Use Coupon from this order.", nil)];
//                                          [self.paymentSummaryView drawPayment];
//                                          [self selectUseOranges:NO];
//                                      }
//                                  }
                              } else {
                              
                                  Promotion *promotion = [[Promotion alloc] initWithDictionary:response];
                                  
                                  if(promotion.merchantNo == 0 || promotion.merchantNo == ORDER.merchant.merchantNo){
                                      PromotionErrorCode returnCode = [self.payment applyPromotion:promotion];
                                    
                                      switch(returnCode){
                                          case PromotionErrorSuccess:
                                              
                                              self.paymentSummaryView.couponCodeTextField.text = nil;
                                              
                                              break;
                                          case PromotionErrorNotCombinable:
                                          case PromotionErrorNotCombinableExists:
                                          {
                                              [UIAlertView alertWithTitle:NSLocalizedString(@"Could Not Apply Promo Code", nil)
                                                                  message:NSLocalizedString(@"These codes cannot be combined.", nil)];
                                          }
                                              break;
                                          case PromotionErrorLackOrderAmount:
                                          {
                                              NSString *title = [NSString stringWithFormat:NSLocalizedString(@"You Can Get %@ Off If You Order %@ or More", nil),
                                                                 promotion.discountString, promotion.minOrderAmountString];
                                              NSString *message = [NSString stringWithFormat:NSLocalizedString(@"Your current order is only %@, would you like to go back and order more?", nil),
                                                                   self.payment.subTotalString];
                                              
                                              UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                                                              message:message
                                                                                             delegate:self
                                                                                    cancelButtonTitle:NSLocalizedString(@"No, Thanks", nil)
                                                                                    otherButtonTitles:NSLocalizedString(@"Yes", nil), nil];
                                              alert.tag = AlertViewTagPromoOrderAmount;
                                              [alert show];
                                              
                                          }
                                              break;
                                          case PromotionErrorFullUse:
                                              self.paymentSummaryView.couponCodeTextField.text = nil;
                                              break;
                                          default:
                                              
                                              break;
                                      }
                                      
                                      [self.paymentSummaryView drawPayment];
                                      [self selectUseOranges:NO];
                                      
                                  } else {
                                      [UIAlertView alertWithTitle:NSLocalizedString(@"Could Not Apply Promo Code", nil)
                                                          message:NSLocalizedString(@"Promo code is incorrect. Please try again", nil)];
                                  }
                              }
                          }
                              break;
                          
                          case ResponseErrorAlreadyReferring:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Could Not Apply Promo Code", nil)
                                                  message:NSLocalizedString(@"Sorry! You have already applied a referral code to your account.", nil)];
                              
                              break;
                          case ResponseErrorReferreringNotFirst:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Could Not Apply Promo Code", nil)
                                                  message:NSLocalizedString(@"Sorry! This code can only be used on your first order.", nil)];
                              
                              break;
                          case ResponseErrorAlreadyReferringUUID:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Could Not Apply Promo Code", nil)
                                                  message:NSLocalizedString(@"Sorry! You have already applied a referral code on this device.", nil)];
                              
                              break;
                          case ResponseErrorReferreringNotFirstUUID:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Could Not Apply Promo Code", nil)
                                                  message:NSLocalizedString(@"Sorry! This code can only be used on your first order.", nil)];
                              
                              break;
                          case ResponseErrorPromoCodeIncorrect:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Could Not Apply Promo Code", nil)
                                                  message:message];
                          }
                              break;
                          case ResponseErrorPromoInExists:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Could Not Apply Promo Code", nil)
                                                  message:message];
                          }
                              break;
                          case ResponseErrorPromoUsedAlready:
                          case ResponseErrorPromotionNotAssigned:
                          case ResponseErrorPromotionFirstUse:
                          {
                              NSString *title = [response objectForKey:@"title"];
                              NSString *message = [response objectForKey:@"message"];
                              [UIAlertView alertWithTitle:title
                                                  message:message];
                          }
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while validationg coupon code", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)paymentSummaryView:(PaymentSummaryView *)summaryView
      didTouchSignInButton:(id)sender
{
    [self showSignIn];
}


- (void)paymentSummaryView:(PaymentSummaryView *)summaryView
    didTouchDiscountAmount:(Promotion *)promotion
{
    DiscountViewController *viewController = [DiscountViewController viewControllerFromNib];
    viewController.payment = self.payment;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^{
                                     
                                 }];
}

- (void)paymentSummaryView:(PaymentSummaryView *)summaryView
    didTouchFeesAmount:(Promotion *)promotion
{
    DiscountViewController *viewController = [DiscountViewController viewControllerFromNib];
    viewController.payment = self.payment;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^{
                                     
                                 }];
}

- (void)paymentSummaryViewTipAmountChanged:(PaymentSummaryView *)summaryView
{
    [self selectUseOranges:NO];
}

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                
                             }];
}

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)cardInfo inPaymentViewController:(CardIOPaymentViewController *)paymentViewController
{
    PCLog(@"Received card info. Number: %@, expiry: %02i/%i, cvv: %@.", cardInfo.cardNumber, cardInfo.expiryMonth, cardInfo.expiryYear, cardInfo.cvv);
    
    self.cardNumberTextField.text = cardInfo.cardNumber;
    [self.cardNumberTextField valueChanged:self.cardNumberTextField];
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 
                             }];
}

@end
