//
//  MenuCellController.h
//  RushOrder
//
//  Created by Conan on 6/15/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "MenuItemCell.h"
#import "MenuDetailViewController.h"
#import "OrderViewController.h"
#import "Merchant.h"
#import "MenuPhotoViewController.h"

@protocol MenuCellControllerDelegate;

@interface MenuCellController : PCViewController
<
UITableViewDelegate,
UITableViewDataSource,
MenuItemCellDelegate,
MenuDetailViewControllerDelegate,
UITextFieldDelegate,
UIAlertViewDelegate,
UIActionSheetDelegate,
UIPageViewControllerDelegate,
UIPageViewControllerDataSource,
MenuPhotoViewControllerDelegate
>

@property (weak, nonatomic) id<MenuCellControllerDelegate> delegate;
@property (nonatomic) CartType initialCartType;
@property (nonatomic) NSUInteger menuPanIndex;
@property (strong, nonatomic) MenuItem *selectedMenuItem;
@property (strong, nonatomic) NSArray *referredMenus;
@property (strong, nonatomic) NSArray *referredMenuIds;
@property (strong, nonatomic) NSArray *referredMenuNames;
@property (nonatomic, strong) NSIndexPath *firstMatchingIndexPath;

- (void)addMenuItemAsSelectedPrevious;
- (void)resetSelection;
- (void)reloadData;
@end


#pragma mark - MenuCellControllerDelegate
@protocol MenuCellControllerDelegate <NSObject>
@required
- (void)menuCellController:(MenuCellController *) cellController
    cartAddAnimateWithCell:(MenuItemCell *)cell;
- (void)menuCellController:(MenuCellController *) cellController
    lockReloadBadge:(BOOL)value;
- (void)menuCellControllerShowTableNumber:(MenuCellController *) cellController;

@end
