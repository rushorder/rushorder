//
//  DeliveryAddressTableViewController.h
//  RushOrder
//
//  Created by Conan Kim on 2/20/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RemoteDataManager.h"

#pragma mark - DeliveryAddressCell

@protocol DeliveryAddressCellDelegate;

@interface DeliveryAddressCell : UITableViewCell

@property (weak, nonatomic) id<DeliveryAddressCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *nameTextField;
@property (weak, nonatomic) IBOutlet UILabel *addressTextField;
@property (weak, nonatomic) IBOutlet NPToggleButton *defalultButton;

@end

@protocol DeliveryAddressCellDelegate <UITableViewDelegate>
- (void)deliveryAddressCell:(DeliveryAddressCell *)cell
didTouchRemoveButtonAtIndexPath:(NSIndexPath *)indexPath;
@end


#pragma mark - Delivery Address TableViewController
@interface DeliveryAddressTableViewController : UITableViewController
<DeliveryAddressCellDelegate,
UIActionSheetDelegate>

@property (strong, nonatomic) DeliveryAddress *updatedDeliveryAddress;
@end
