//
//  MenuDetailViewController.m
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MenuDetailViewController.h"
#import "MenuOptionGroup.h"
#import "MenuOption.h"
#import "OrderManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MenuManager.h"
#import "MenuSubCategory.h"
#import "MenuCategory.h"
#import "MenuOptionCell.h"
#import "SpecialInstructionCell.h"

#define MENU_DET_SECTION_HEIGHT 30.0f


@interface MenuDetailViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NPStretchableButton *doneButton;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet NPImageButton *photoButton;
@property (weak, nonatomic) IBOutlet UILabel *menuNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *menuDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *menuPriceLabel;
@property (strong, nonatomic) IBOutlet UIStepper *stepper;
@property (strong, nonatomic) IBOutlet UITableViewCell *quantityCell;
@property (weak, nonatomic) IBOutlet UILabel *quantityCellLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (strong, nonatomic) IBOutlet SpecialInstructionCell *specialInstructionCell;
@property (weak, nonatomic) IBOutlet UITextView *specialInstructionTextView;
@property (strong, nonatomic) UILabel *groupHeaderLabelForHeight;
@property (weak, nonatomic) IBOutlet UILabel *placeHolderLabel;
@property (weak, nonatomic) IBOutlet UILabel *problemTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *problemDescLabel;
@property (weak, nonatomic) IBOutlet UIView *problemPanelView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *problemTitleBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *problemTitleTopConstraint;

@property (strong, nonatomic) id<SDWebImageOperation> lastDnOperation;

@property (nonatomic) NSInteger quantity;

@property (nonatomic, getter = isAnyChanged) BOOL anyChanged;

@end

@implementation MenuDetailViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(menuListChanged:)
                                                     name:MenuListChangedNotification
                                                   object:nil];
        
    }
    return self;
}

- (void)menuListChanged:(NSNotification *)notification
{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.photoButton.action = @selector(photoButtonTouched:);
    
    if(self.isModifyingMode){
        if(self.canEdit){
            self.title = NSLocalizedString(@"Edit Item", nil);
            self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Update",nil)
                                                                                      target:self
                                                                                      action:@selector(updateButtonTouched:)];
            
            self.navigationItem.rightBarButtonItem.button.enabled = (self.lineItem.menuValidStatus != MenuValidStatusValid);
            
            self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Cancel",nil)
                                                                                     target:self
                                                                                     action:@selector(closeButtonTouched:)];
        } else {
            self.title = NSLocalizedString(@"Ordered Item", nil);
            self.tableView.tableFooterView = nil;
            
            self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Close",nil)
                                                                                      target:self
                                                                                      action:@selector(closeButtonTouched:)];
        }
        
        [self.doneButton removeTarget:self
                               action:@selector(doneButtonTouched:)
                     forControlEvents:UIControlEventTouchUpInside];
        
        [self.doneButton addTarget:self
                            action:@selector(updateButtonTouched:)
                  forControlEvents:UIControlEventTouchUpInside];
        
        self.doneButton.enabled = (self.lineItem.menuValidStatus != MenuValidStatusValid);
        self.doneButton.buttonTitle = NSLocalizedString(@"Update Ordered Item", nil);
        
    } else {
        self.title = NSLocalizedString(@"Select Options", nil);
        self.doneButton.buttonTitle = NSLocalizedString(@"Add", nil);
        
        self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemPlus
                                                                             target:self
                                                                             action:@selector(doneButtonTouched:)];
        
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Cancel",nil)
                                                                                 target:self
                                                                                 action:@selector(closeButtonTouched:)];
    }
    
    self.stepper.minimumValue = 1;
    self.stepper.maximumValue = 100;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(self.isModifyingMode){
        [self fillLineItemContents];
    } else {
        [self fillContents];
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fillContents
{
    if(self.lineItem == nil){
        self.quantity = 1;
    } else {
        self.quantity = self.lineItem.quantity;
    }
    self.stepper.value = self.quantity;
    
    if(self.item != nil){
        self.menuNameLabel.text = self.item.name;
        self.menuPriceLabel.text = [[NSNumber numberWithCurrency:self.item.price] currencyString];
        self.totalLabel.text = [[NSNumber numberWithCurrency:(self.item.price * self.quantity)] currencyString];

        
        self.photoButton.image = [UIImage imageNamed:@"placeholder_menu"];
        
        [self.lastDnOperation cancel];
        if(self.item.thumbnailURL != nil){
            
            self.lastDnOperation = [[SDWebImageManager sharedManager] downloadImageWithURL:self.item.thumbnailURL
                                                                                   options:0
                                                                                  progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                                                      
                                                                                  } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                                                                      if(finished){
                                                                                          if(error == nil){
                                                                                              self.photoButton.image = image;
                                                                                              self.photoButton.contentMode = UIViewContentModeScaleAspectFill;
                                                                                              
                                                                                          } else {
                                                                                              PCError(@"Image download error %@" , error);
                                                                                              self.photoButton.image = [UIImage imageNamed:@"placeholder_menu"];
                                                                                          }
                                                                                      } else {
                                                                                          
                                                                                      }
                                                                                  }];
        }

        
    
        self.menuDescLabel.text = self.item.desc;
        
    } else {
        self.menuNameLabel.text = self.lineItem.menuName;
        
        self.menuPriceLabel.text = [[NSNumber numberWithCurrency:self.lineItem.unitPrice] currencyString];
        self.totalLabel.text = [[NSNumber numberWithCurrency:(self.lineItem.unitPrice * self.quantity)] currencyString];
        
        self.photoButton.image = [UIImage imageNamed:@"placeholder_menu"];
        
        self.menuDescLabel.text = self.lineItem.serializedOptionNames;
    }
    
    self.problemPanelView.hidden = NO;
    self.problemTitleBottomConstraint.constant = 6.0;
    self.problemTitleTopConstraint.constant = 8.0;
    
    if((self.lineItem.menuValidStatus & MenuValidStatusMenuRemoved) == MenuValidStatusMenuRemoved){
        self.problemTitleLabel.text = NSLocalizedString(@" Item Removed ", nil);
        self.problemDescLabel.text = NSLocalizedString(@"Restaurant no longer carries this menu item.", nil);
    } else if((self.lineItem.menuValidStatus & MenuValidStatusOutOfHours) == MenuValidStatusOutOfHours){
        self.problemTitleLabel.text = NSLocalizedString(@" From Unavailable Menu ", nil);
        self.problemDescLabel.text = NSLocalizedString(@"This item is from a menu that is available only at certain times.", nil);
    } else if((self.lineItem.menuValidStatus & MenuValidStatusNotAvailableOrderType) == MenuValidStatusNotAvailableOrderType){
        if(self.lineItem.substitutableMenuItem != nil){
            self.problemTitleLabel.text = NSLocalizedString(@" Price Change ", nil);
            self.problemDescLabel.text = NSLocalizedString(@"The price of this item has changed. Please review and update ordered item.", nil);
        } else {
            self.problemTitleLabel.text = [NSString stringWithFormat:NSLocalizedString(@" Item Not Available For %@ ", nil), MENUPAN.menuListTypeString];
            self.problemDescLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Sorry, this item is not available for %@", nil), MENUPAN.menuListTypeString];
        }
    } else if((self.lineItem.menuValidStatus & MenuValidStatusOptionChanged) == MenuValidStatusOptionChanged){
        self.problemTitleLabel.text = NSLocalizedString(@" Option Removed ", nil);
        self.problemDescLabel.text = NSLocalizedString(@"Previously selected option unavailable. Choose from available options below, and update ordered item.", nil);
    } else if((self.lineItem.menuValidStatus & MenuValidStatusPriceChanged) == MenuValidStatusPriceChanged){
        self.problemTitleLabel.text = NSLocalizedString(@" Price Change ", nil);
//        self.problemDescLabel.text = [NSString stringWithFormat:NSLocalizedString(@"No longer available at this price(%@). Please update this item.", nil),[[NSNumber numberWithCurrency:self.lineItem.basePrice] currencyString]];
        self.problemDescLabel.text = NSLocalizedString(@"The price of this item has changed. Please review and update ordered item.", nil);
    } else {
        self.problemPanelView.hidden = YES;
        self.problemTitleLabel.text = nil;
        self.problemDescLabel.text = nil;
        self.problemTitleBottomConstraint.constant = 0.0;
        self.problemTitleTopConstraint.constant = 0.0;
    }
    
    self.specialInstructionTextView.text = self.lineItem.specialInstruction;
    self.placeHolderLabel.hidden = ([self.specialInstructionTextView.text length] > 0);
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self sizeHeaderToFit];
}

- (void)sizeHeaderToFit
{
    [self.headerView setNeedsLayout];
    [self.headerView layoutIfNeeded];
    
    self.headerView.height = [self.headerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    self.tableView.tableHeaderView = self.headerView;
    
}

- (void)fillLineItemContents
{
    if(self.lineItem.menuItem == nil){
        [self.lineItem linkMenuItemInMenuList:MENUPAN.wholeSerialMenuList];
    }
    
    if(self.lineItem.menuItem == nil){
        PCWarning(@"Menu Item is not set in line item at -(void)fillLineItemcontents");
    } else {
        
        if((self.lineItem.menuValidStatus & MenuValidStatusNotAvailableOrderType) == MenuValidStatusNotAvailableOrderType && self.lineItem.substitutableMenuItem != nil){
            self.item = self.lineItem.substitutableMenuItem;
        } else {
            self.item = self.lineItem.menuItem;
        }
        
        
        for(MenuPrice *price in self.item.prices){
            if(price.menuPriceNo == self.lineItem.selectedPrice.menuPriceNo){
                price.selected = YES;
            } else {
                price.selected = NO;
            }
        }
        
        for(MenuOptionGroup *group in self.item.optionGroups){
            for(MenuOption *option in group.options){
                
                BOOL gotcha = NO;
                for(MenuOption *menuOption in self.lineItem.selectedOptions){
                    if(option.menuOptionNo == menuOption.menuOptionNo){
                        gotcha = YES;
                        break;
                    }
                }
                option.selected = gotcha;
            }
        }
    }
    
    [self fillContents];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger sectionCount = [self.item.optionGroups count];
    if([self.item.prices count] > 1){
        sectionCount++;
    }
    
    sectionCount += 2; // For quantity and Request

    return sectionCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger adjustedSection = section;
    
    if(section == 0) return 1; //For quantity
    adjustedSection--;
    
    
    if([self.item.prices count] > 1){
        if(section == 1){
            return [self.item.prices count];
        } else {
            adjustedSection--;
        }
    }
    
    if([self.item.optionGroups count] > adjustedSection){
        MenuOptionGroup *group = [self.item.optionGroups objectAtIndex:adjustedSection];
        return [group.options count];
    } else {
        return 1;
    }
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    NSInteger adjustedSection = section;
//    
//    if(section == 0) return NSLocalizedString(@"Quantity", nil); //For quantity
//    adjustedSection--; // For quantity
//    
//    
//    if([self.item.prices count] > 1){
//        if(section == 1){
//            return @"Prices";
//        } else {
//            adjustedSection--;
//        }
//    }
//    
//    MenuOptionGroup *group = [self.item.optionGroups objectAtIndex:adjustedSection];
//    
//    return group.name;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
//    return MENU_DET_SECTION_HEIGHT;
    NSInteger adjustedSection = section;
    
    if(section == 0){
        self.groupHeaderLabelForHeight.text = NSLocalizedString(@"Quantity", nil); //For quantity
    }
    
    adjustedSection--; // For quantity
    
    if([self.item.prices count] > 1){
        if(section == 1){
            self.groupHeaderLabelForHeight.text = NSLocalizedString(@"Prices", nil);
        } else {
            adjustedSection--;
        }
    }
    
    if([self.item.optionGroups count] > adjustedSection){
        MenuOptionGroup *group = [self.item.optionGroups objectAtIndex:adjustedSection];
        
        self.groupHeaderLabelForHeight.text = group.name;
    } else {
        self.groupHeaderLabelForHeight.text = NSLocalizedString(@"Special Instructions", nil);
    }
    
    [self.groupHeaderLabelForHeight sizeToHeightFit];
    return CGRectGetMaxY(self.groupHeaderLabelForHeight.frame) + 8.0f;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger adjustedSection = indexPath.section;
    
    if(adjustedSection == 0){
        return 44.0f;
    } else {
        adjustedSection--;
    }
    
    if([self.item.prices count] > 1){
        if(indexPath.section == 1){
            return 44.0f;
        } else {
            adjustedSection--;
        }
    }
    
    if([self.item.optionGroups count] > adjustedSection){
        return 44.0f;
    } else {
        return 110.0f;
    }
}

- (UILabel *)groupHeaderLabelForHeight
{
    if(_groupHeaderLabelForHeight == nil){
        _groupHeaderLabelForHeight = [[UILabel alloc] initWithFrame:CGRectMake(10.0f,
                                                                            4.0f,
                                                                            300.0f,
                                                                            20.0f)];
        _groupHeaderLabelForHeight.font = [UIFont systemFontOfSize:14.0f];
        _groupHeaderLabelForHeight.numberOfLines = 0;
    }
    
    return _groupHeaderLabelForHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSInteger adjustedSection = section;
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0f,
                                                                 0.0f,
                                                                 320.0f,
                                                                 MENU_DET_SECTION_HEIGHT)];
    titleView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    titleView.backgroundColor = [UIColor c1Color];
    
    UILabel *sectionHeaderTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0f,
                                                                                 4.0f,
                                                                                 300.0f,
                                                                                 20.0f)];
    sectionHeaderTitleLabel.textColor = [UIColor c5Color];
    sectionHeaderTitleLabel.backgroundColor = [UIColor clearColor];
    sectionHeaderTitleLabel.font = [UIFont systemFontOfSize:14.0f];
    sectionHeaderTitleLabel.numberOfLines = 0;
    
    [titleView addSubview:sectionHeaderTitleLabel];
    
    if(section == 0){
        sectionHeaderTitleLabel.text = NSLocalizedString(@"Quantity", nil); //For quantity
        return titleView;
    }
    
    adjustedSection--; // For quantity
    
    if([self.item.prices count] > 1){
        if(section == 1){
            sectionHeaderTitleLabel.text = NSLocalizedString(@"Prices", nil);
            return titleView;
        } else {
            adjustedSection--;
        }
    }
    
    if([self.item.optionGroups count] > adjustedSection){
        MenuOptionGroup *group = [self.item.optionGroups objectAtIndex:adjustedSection];
        
        sectionHeaderTitleLabel.text = group.name;
    } else {
        sectionHeaderTitleLabel.text = NSLocalizedString(@"Special Instructions", nil);
    }
    [sectionHeaderTitleLabel sizeToHeightFit];
    titleView.height = CGRectGetMaxY(sectionHeaderTitleLabel.frame) + 8.0f;
    return titleView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        self.quantityCellLabel.text = [NSString stringWithFormat:@"%ld", (long)self.quantity];
        self.stepper.enabled = self.canEdit;
        return self.quantityCell;
    }
    
    NSInteger adjustedSection = indexPath.section;
    
    NSString *cellIdentifier = @"MenuOptionCell";
    
    MenuOptionCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
//                                      reuseIdentifier:cellIdentifier];
        cell = [MenuOptionCell cell];
    }
    
//    cell.accessoryView = nil;
//    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    
    if([self.item.prices count] > 1){
        if(indexPath.section == 1){
            MenuPrice *price = [self.item.prices objectAtIndex:indexPath.row];
            
            cell.nameLabel.text = price.name;
            cell.priceLabel.text = price.priceString;
            cell.checkImageView.hidden = NO;
            if(price.isSelected){
                cell.checkImageView.image = [UIImage imageNamed:@"button_radio_on_cus"];
            } else {
                cell.checkImageView.image = [UIImage imageNamed:@"button_radio_off_cus"];
            }
            return cell;
        } else {
            adjustedSection--;
        }
    }
    
    adjustedSection--; // For quantity
    
    if([self.item.optionGroups count] > adjustedSection){
        MenuOptionGroup *group = [self.item.optionGroups objectAtIndex:adjustedSection];
        MenuOption *option = [group.options objectAtIndex:indexPath.row];
        
        cell.nameLabel.text = option.name;
        
        if(option.price > 0){
            cell.priceLabel.text = [@"+" stringByAppendingString:option.priceString];
        } else if(option.price < 0){
            cell.priceLabel.text = option.priceString;
        }  else {
            if(group.isPriceSensitive){
                cell.priceLabel.text = NSLocalizedString(@"Free", nil);
            } else {
                cell.priceLabel.text = @"";
            }
        }
        
        switch(group.optionType){
            case MenuOptionTypeAdd:
                cell.checkImageView.image = [UIImage imageNamed:@"icon_check"];
                cell.checkImageView.hidden = !option.isSelected;
                break;
            case MenuOptionTypeChoose:
                cell.checkImageView.hidden = NO;
                if(option.isSelected){
                    cell.checkImageView.image = [UIImage imageNamed:@"button_radio_on_cus"];
                } else {
                    cell.checkImageView.image = [UIImage imageNamed:@"button_radio_off_cus"];
                }
                break;
        }
        
        return cell;
    } else {
        //Special Instruction
        self.specialInstructionTextView.editable = self.canEdit;
        return self.specialInstructionCell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!self.canEdit){
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        return;
    }
    
    NSInteger adjustedSection = indexPath.section;
    
    adjustedSection--; // For quantity
    
    if([self.item.prices count] > 1){
        if(indexPath.section == 1){
            
            NSInteger i = 0;
            for(i = 0 ; i < [self.item.prices count] ; i++){
                MenuPrice *price = [self.item.prices objectAtIndex:i];
                price.selected = (indexPath.row == i);
            }
            
            goto sum;
        } else {
            adjustedSection--;
        }
    }
    
    if([self.item.optionGroups count] <= adjustedSection){
        [self.tableView deselectRowAtIndexPath:indexPath
                                      animated:NO];
        return;
    }
    
    if([self.item.optionGroups count] > 0
       && adjustedSection >= 0){
        
        MenuOptionGroup *group = [self.item.optionGroups objectAtIndex:adjustedSection];

        NSInteger i = 0;
        
        if(group.optionType == MenuOptionTypeChoose){
            for(i = 0 ; i < [group.options count] ; i++){
                MenuOption *option = [group.options objectAtIndex:i];
                if(indexPath.row == i){
//                    option.selected = !option.selected;
                    option.selected = YES; //Mandatory
                } else {
                    option.selected = NO;
                }
            }
        } else {
            
            if(group.maxSelect == 0){
                
                MenuOption *option = [group.options objectAtIndex:indexPath.row];
                option.selected = !option.selected;
                
            } else {
            
                NSInteger selectedCount = 0;
                MenuOption *selectedOption = nil;
                
                for(i = 0 ; i < [group.options count] ; i++){
                    MenuOption *option = [group.options objectAtIndex:i];
                    
                    if(option.selected) {
                        if(indexPath.row == i){
                            selectedOption = option;
                        } else {
                            selectedCount++;
                        }
                    } else {
                        if(indexPath.row == i){
                            selectedOption = option;
                        }
                    }
                    
                    if(selectedCount >= group.maxSelect){
                        break;
                    }
                }
                
                if(selectedCount >= group.maxSelect){
                    [UIAlertView alert:NSLocalizedString(@"You have already added the maximum number of options", nil)];
                } else {
                    selectedOption.selected = !selectedOption.selected;
                }
            }
        }
    }
    
sum:
    
    self.navigationItem.rightBarButtonItem.button.enabled = YES;
    self.doneButton.enabled = YES;
    if(indexPath.section != 0){
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                      withRowAnimation:UITableViewRowAnimationFade];
    }
    
    self.menuPriceLabel.text = [[NSNumber numberWithCurrency:self.item.price] currencyString];
    self.totalLabel.text = [[NSNumber numberWithCurrency:(self.item.price * self.quantity)] currencyString];
}

- (IBAction)closeButtonTouched:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^(){
                                 if([self.delegate respondsToSelector:@selector(menuDetailViewControllerDidCancelTouched:)]){
                                     [self.delegate menuDetailViewControllerDidCancelTouched:self];
                                 }
                             }];
}

- (IBAction)stepperValueChanged:(id)sender
{
    self.quantity = (NSInteger)self.stepper.value;
    self.quantityCellLabel.text = [NSString stringWithFormat:@"%ld", (long)self.quantity];
    
    self.navigationItem.rightBarButtonItem.button.enabled = YES;
    self.doneButton.enabled = YES;
    
    self.totalLabel.text = [[NSNumber numberWithCurrency:(self.item.price * self.quantity)] currencyString];
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setDoneButton:nil];
    [self setStepper:nil];
    [self setHeaderView:nil];
    [self setQuantityCell:nil];
    [self setQuantityCellLabel:nil];
    [super viewDidUnload];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)doneButtonTouched:(id)sender
{
    for(MenuOptionGroup *group in self.item.optionGroups){
        if(group.optionType == MenuOptionTypeChoose){
            BOOL didCooseOnce = NO;
            if([group.options count] > 0){
                for(MenuOption *option in group.options){
                    if(option.isSelected){
                        didCooseOnce = YES;
                        break;
                    }
                }
            } else {
                didCooseOnce = YES;
            }
            
            if(!didCooseOnce){
                [UIAlertView alertWithTitle:NSLocalizedString(@"Select Option", nil)
                                    message:[NSString stringWithFormat:NSLocalizedString(@"You must select an option of %@", nil),
                                             group.name]];
                return;
            }
        }
    }
    
    [self dismissViewControllerAnimated:YES
                             completion:^(){
                                 if([self.delegate respondsToSelector:@selector(menuDetailViewController:didNewMenuItem:quantity:specialInstructions:)]){
                                     [self.delegate menuDetailViewController:self
                                                              didNewMenuItem:self.item
                                                                    quantity:self.quantity
                                                         specialInstructions:self.specialInstructionTextView.text];
                                 }
                             }];
}

- (IBAction)updateButtonTouched:(id)sender
{
    
    for(MenuOptionGroup *group in self.item.optionGroups){
        if(group.optionType == MenuOptionTypeChoose){
            BOOL didCooseOnce = NO;
            if([group.options count] > 0){
                for(MenuOption *option in group.options){
                    if(option.isSelected){
                        didCooseOnce = YES;
                        break;
                    }
                }
            } else {
                didCooseOnce = YES;
            }
            
            if(!didCooseOnce){
                [UIAlertView alertWithTitle:NSLocalizedString(@"Select Option", nil)
                                    message:[NSString stringWithFormat:NSLocalizedString(@"You must select an option of %@", nil),
                                             group.name]];
                return;
            }
        }
    }
    
    [self applyLineItem];
    [self dismissViewControllerAnimated:YES
                             completion:^(){
                                 if([self.delegate respondsToSelector:@selector(menuDetailViewController:didUpdateLineItem:)]){
                                     [self.delegate menuDetailViewController:self
                                                           didUpdateLineItem:self.lineItem];
                                 }
                             }];
}

- (IBAction)photoButtonTouched:(id)sender
{
    if([self.delegate respondsToSelector:@selector(menuDetailViewControllerDidTouchedPhoto:atIndexPath:)]){
        [self.delegate menuDetailViewControllerDidTouchedPhoto:self
                                                   atIndexPath:self.indexPath];
    }
}

- (void)applyLineItem
{
    
    if((self.lineItem.menuValidStatus & MenuValidStatusNotAvailableOrderType) == MenuValidStatusNotAvailableOrderType && self.lineItem.substitutableMenuItem != nil){
        self.lineItem.menuItem = self.item;
    }
    
    if(self.lineItem != nil){
        self.lineItem.menuValidStatus = MenuValidStatusValid;
        self.lineItem.selectedPrice = [self.item selectedMenuPrice];
        self.lineItem.selectedOptions = [self.item selectedOptions];
        self.lineItem.quantity = self.quantity;
        self.lineItem.specialInstruction = self.specialInstructionTextView.text;
    }
}

- (BOOL)isModifyingMode
{
    return (self.lineItem != nil);
}


- (BOOL)canEdit
{
    return ((self.lineItem.isMine && self.lineItem.status == LineItemStatusOrdering) || self.lineItem == nil);
}

#pragma mark - TextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    self.navigationItem.rightBarButtonItem.button.enabled = YES;
    self.doneButton.enabled = YES;
    self.placeHolderLabel.hidden = ([textView.text length] > 0);
}

@end
