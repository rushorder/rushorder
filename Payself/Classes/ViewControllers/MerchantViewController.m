//
//  MerchantViewController.m
//  RushOrder
//
//  Created by Conan on 11/11/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MerchantViewController.h"
#import "OrderManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TransactionManager.h"
#import "MenuManager.h"
#import "RestaurantViewController.h"
#import "IIViewDeckController.h"
#import "PCCredentialService.h"
#import "PCPaymentService.h"
#import "RemoteDataManager.h"
#import "RestaurantMapViewController.h"
#import "DZoneRadius.h"
#import "DZonePolygon.h"
#import "CustomIOSAlertView.h"
#import "DialingOutView.h"
#import "RushOrder-Swift.h"


@interface MerchantViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *storeMapView;
@property (weak, nonatomic) IBOutlet MKMapView *miniMapView;
@property (weak, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet CircleImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *eventFlagImageView;
@property (weak, nonatomic) IBOutlet UILabel *restaurantNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *hoursLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *photoScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *photoPageViewController;


@property (weak, nonatomic) IBOutlet NPStretchableButton *phoneButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *homepageButton;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

@property (weak, nonatomic) IBOutlet NPStretchableButton *processButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *takeoutButton;
@property (weak, nonatomic) IBOutlet DoubleImageButton *deliveryButton;

@property (weak, nonatomic) IBOutlet UIImageView *panelImageView;
@property (weak, nonatomic) IBOutlet UILabel *panelLabel;

@property (weak, nonatomic) IBOutlet UILabel *minimumOrderAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryFeeAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryLimitLabel;
@property (weak, nonatomic) IBOutlet UILabel *averageDeliveryTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *averageDeliveryTitleLabel;

@property (weak, nonatomic) IBOutlet UIView *buttonsContainer;

@property (weak, nonatomic) IBOutlet UILabel *hoursTitleLabel;

@property (strong, nonatomic) IBOutlet NPToggleButton *favoriteButton;

@property (strong, nonatomic) IBOutlet UIView *promotionContainer;
@property (strong, nonatomic) IBOutlet UIView *hoursContainer;
@property (strong, nonatomic) IBOutlet UIView *deliveryContainer;
@property (weak, nonatomic) IBOutlet UIView *locationContainer;

@property (nonatomic) BOOL didRefreshedMerchant;
@property (nonatomic, getter = isViewDirty) BOOL viewDirty;
@property (weak, nonatomic) IBOutlet UIButton *moreDeliveryFeeButton;
@property (weak, nonatomic) IBOutlet UILabel *deliveryFeeTitleLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *panelHeightConstraint;


@end

@implementation MerchantViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if(self){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.viewDirty = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(merchantUpdated:)
                                                 name:MerchantInformationUpdatedNotification
                                               object:nil];
}

- (void)merchantUpdated:(NSNotification *)notification
{
    self.viewDirty = YES;
    self.didRefreshedMerchant = YES;
    
    BOOL invalidMerchant = [notification.userInfo boolForKey:@"InvalidMerchant"];
    if(invalidMerchant){
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        // TODO: if the restaurant enable menu order -> change view to menu order
        [self drawMerchant];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = ORDER.merchant.displayName;
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemClose
                                                                        target:self
                                                                        action:@selector(closeButtonTouched:)];
    
    if(CRED.isSignedIn){
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.favoriteButton];
        self.navigationItem.rightBarButtonItem = rightBarButtonItem;
        [self.favoriteButton applyResizableImageFromCenterForAllState];
    }
    
    self.photoScrollView.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)closeButtonTouched:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 
                             }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(self.isViewDirty){
        [self drawMerchant];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(!self.didRefreshedMerchant){
        [ORDER requestMerchantInfo];
    }
}

- (IBAction)deliveryFeeInfoButtonTouched:(id)sender
{
    PCCurrency deliveryFeeBasedOnLocation = [ORDER.merchant deliveryFeeAt:APP.location];
    if(deliveryFeeBasedOnLocation == NSNotFound){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Outside Deliverable Area", nil)
                            message:NSLocalizedString(@"You are currently outside the restaurant's delivery distance limit.", nil)];
        return;
    }
    
    DeliveryFeeViewController *deliveryFeeViewController = [[DeliveryFeeViewController alloc] initWithNibName:@"DeliveryFeeViewController"
                                                                                                       bundle:nil
                                                                                          deliveryFeeDiscount:ORDER.merchant.deliveryFeeDiscount
                                                                                              baseDeliveryFee:deliveryFeeBasedOnLocation];
    
    PopupDialog *popup = [[PopupDialog alloc] initWithViewController:deliveryFeeViewController
                                                     buttonAlignment:UILayoutConstraintAxisHorizontal
                                                     transitionStyle:PopupDialogTransitionStyleBounceUp
                                                    gestureDismissal:YES
                                                          completion:^{
                                                              
                                                          }];
    
    CancelButton *ok = [[CancelButton alloc] initWithTitle:NSLocalizedString(@"OK", nil)
                                                    height:44.f
                                              dismissOnTap:YES
                                                    action:^{
                                                        
                                                    }];
    ok.titleColor = [UIColor colorWithR:66. G:66. B:66.];
    
    [popup addButtons: @[ok]];
    [self presentViewController:popup animated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)drawMerchant
{
#define SPAN_DELTA  0.0053f
    
    self.viewDirty = NO;
    
    if(ORDER.merchant.isOpenHour){
        if(ORDER.merchant.isDemoMode){
            [self turnDemoFlagOn:YES];
        } else {
            [self turnTestFlagOn:ORDER.merchant.isTestMode];
        }
    } else {
        [self turnCloseFlagOn:YES];
    }
    
    NSURL *imgURL = ORDER.merchant.logoURL;
    if(imgURL == nil) imgURL = ORDER.merchant.imageURL;
    
    
    [self.logoImageView sd_setImageWithURL:imgURL
                          placeholderImage:[UIImage imageNamed:@"placeholder_logo"]
                                   options:0
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {

                                 }];
    
    self.favoriteButton.selected = ORDER.merchant.isFavorite;
    
    self.restaurantNameLabel.text = ORDER.merchant.name;
    if(ORDER.merchant.displayGeoAddress){
        self.addressLabel.text = ORDER.merchant.displayAddress;
    } else {
        self.addressLabel.text = ORDER.merchant.multilineAddress;
    }
    
    
    if(ORDER.merchant.isDemoMode || (APP.location == nil)){
        self.distanceLabel.hidden = YES;
    } else {
        self.distanceLabel.hidden = NO;
        self.distanceLabel.text = ORDER.merchant.distanceString;
    }
    

    self.descriptionLabel.text = ORDER.merchant.desc;
    
    BOOL isPromotion = NO;
    
    if(ORDER.merchant.promotion != nil){
        self.eventLabel.hidden = NO;
//        if([ORDER.merchant.promotion.desc length] > 0){
//            self.eventLabel.text = ORDER.merchant.promotion.desc;
//        } else {
//            self.eventLabel.text = ORDER.merchant.promotion.title;
//        }
        self.eventLabel.text = ORDER.merchant.promotion.desc;
        if([ORDER.merchant.promotion.title length] > 0){
            self.eventTitleLabel.text = ORDER.merchant.promotion.title;
        } else {
            self.eventTitleLabel.text = NSLocalizedString(@"Promo", nil);
        }
        [self turnEventFlagOn:YES];
        isPromotion = YES;
    } else {
        if([ORDER.merchant.creditPolicyName length] > 0){
            self.eventLabel.hidden = NO;
            self.eventLabel.text = ORDER.merchant.creditPolicyName;
            self.eventTitleLabel.text = NSLocalizedString(@"Event", nil);
            [self turnEventFlagOn:YES];
            isPromotion = YES;
        } else {
            self.eventLabel.hidden = YES;
            [self turnEventFlagOn:NO];
        }
    }
    
    [self.promotionContainer removeFromSuperview];
    [self.hoursContainer removeFromSuperview];
    [self.deliveryContainer removeFromSuperview];
    
    if(isPromotion){
        self.promotionContainer.hidden = NO;
        [self.scrollView addSubview:self.promotionContainer];
    } else {
        self.promotionContainer.hidden = YES;
    }
    
    //buttonHeightConstraint
    
    if(self.canDirectOrder){
        [self.buttonsContainer removeAllSubviews];
        
        NPStretchableButton *leftButton = nil;
        NPStretchableButton *middleButton = nil;
        NPStretchableButton *rightButton = nil;
        NPStretchableButton *soleButton = nil;
        
//        self.deliveryButton.subImage = [UIImage imageNamed:@"icon_sf"];
//        self.deliveryButton.subImageOn = ORDER.merchant.hasRoServiceFee;
        
        if(ORDER.merchant.menuOrder){
            if(ORDER.merchant.isAbleTakeout && ORDER.merchant.isAbleDelivery){
                if(ORDER.merchant.isAbleDinein){
                    // Dine-in , Takeout, Delivery
                    
                    [self.buttonsContainer addSubview:self.processButton];
                    leftButton = self.processButton;
                    [self.buttonsContainer addSubview:self.takeoutButton];
                    middleButton = self.takeoutButton;
                    [self.buttonsContainer addSubview:self.deliveryButton];
                    rightButton = self.deliveryButton;
                    
                } else {
                    // Takeout, Delivery
                    [self.buttonsContainer addSubview:self.takeoutButton];
                    leftButton = self.takeoutButton;
                    [self.buttonsContainer addSubview:self.deliveryButton];
                    rightButton = self.deliveryButton;
                    
                }
            } else if(ORDER.merchant.isAbleTakeout){
                if(ORDER.merchant.isAbleDinein){
                    // Dine-in , Takeout
                    [self.buttonsContainer addSubview:self.processButton];
                    leftButton = self.processButton;
                    [self.buttonsContainer addSubview:self.takeoutButton];
                    rightButton = self.takeoutButton;
                    
                } else {
                    // Takeout
                    [self.buttonsContainer addSubview:self.takeoutButton];
                    soleButton = self.takeoutButton;
                }
                
            } else if(ORDER.merchant.isAbleDelivery){
                if(ORDER.merchant.isAbleDinein){
                    // Dine-in, Delivery
                    [self.buttonsContainer addSubview:self.processButton];
                    [self.buttonsContainer addSubview:self.deliveryButton];
                    leftButton = self.processButton;
                    rightButton = self.deliveryButton;
                } else {
                    // Delivery
                    [self.buttonsContainer addSubview:self.deliveryButton];
                    soleButton = self.deliveryButton;
                }
            } else {
                if(ORDER.merchant.isAbleDinein){
                    // Dine-in
                    [self.buttonsContainer addSubview:self.processButton];
                    soleButton = self.processButton;
                    
                } else {
                    //Error
                    
                }
            }
            
            if(ORDER.merchant.isOpenHour){
                [self.deliveryButton setTitleColor:[UIColor colorWithR:96.f G:103.f B:118.f]
                                          forState:UIControlStateNormal];
                [self.takeoutButton setTitleColor:[UIColor colorWithR:96.f G:103.f B:118.f]
                                         forState:UIControlStateNormal];
                [self.processButton setTitleColor:[UIColor colorWithR:96.f G:103.f B:118.f]
                                         forState:UIControlStateNormal];
                [self.deliveryButton setImage:[UIImage imageNamed:@"button_icon_delivery"]
                                     forState:UIControlStateNormal];
                [self.takeoutButton setImage:[UIImage imageNamed:@"button_icon_takeout"]
                                    forState:UIControlStateNormal];
                [self.processButton setImage:[UIImage imageNamed:@"button_icon_dinein"]
                                    forState:UIControlStateNormal];
            } else {
                [self.deliveryButton setTitleColor:[UIColor lightGrayColor]
                                          forState:UIControlStateNormal];
                [self.takeoutButton setTitleColor:[UIColor lightGrayColor]
                                         forState:UIControlStateNormal];
                [self.processButton setTitleColor:[UIColor lightGrayColor]
                                         forState:UIControlStateNormal];
                [self.deliveryButton setImage:[UIImage imageNamed:@"button_icon_delivery_g"]
                                     forState:UIControlStateNormal];
                [self.takeoutButton setImage:[UIImage imageNamed:@"button_icon_takeout_g"]
                                    forState:UIControlStateNormal];
                [self.processButton setImage:[UIImage imageNamed:@"button_icon_dinein_g"]
                                    forState:UIControlStateNormal];
            }
            
            if(!ORDER.merchant.isDeliveryHour){
                [self.deliveryButton setImage:[UIImage imageNamed:@"button_icon_delivery_g"]
                                     forState:UIControlStateNormal];
                [self.deliveryButton setTitleColor:[UIColor lightGrayColor]
                                          forState:UIControlStateNormal];
            }
            
            
            if(ORDER.merchant.isTableBase){
                self.processButton.tag = CartTypeCart;
            } else {
                if(ORDER.merchant.isServicedByStaff){
                    self.processButton.tag = CartTypePickupServiced;
                } else {
                    self.processButton.tag = CartTypePickup;
                }
            }
            
        } else {
            // Dine-in
            [self.buttonsContainer addSubview:self.processButton];
            soleButton = self.processButton;
        }
        
        NSMutableDictionary *viewsDictionary = [NSMutableDictionary dictionary];
        if(leftButton) [viewsDictionary setObject:leftButton forKey:@"leftButton"];
        if(middleButton) [viewsDictionary setObject:middleButton forKey:@"middleButton"];
        if(rightButton) [viewsDictionary setObject:rightButton forKey:@"rightButton"];
        if(soleButton) [viewsDictionary setObject:soleButton forKey:@"soleButton"];
        
        if (leftButton == nil && middleButton == nil && rightButton == nil && soleButton == nil){
            
        } else {
            
            NSMutableString *formatString = [NSMutableString string];
            [formatString appendString:@"|"];
            if(soleButton){
                [formatString appendString:@"[soleButton]"];
            } else {
                if(leftButton){
                    [formatString appendString:@"-0-[leftButton]"];
                }
                if(middleButton){
                    [formatString appendString:@"-0-[middleButton(==leftButton)]"];
                }
                if(rightButton){
                    if(middleButton){
                        [formatString appendString:@"-0-"];
                    } else {
                        [formatString appendString:@"-(-1)-"];
                    }
                    [formatString appendString:@"[rightButton(==leftButton)]"];
                }
            }
            [formatString appendString:@"-0-|"];
            NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:formatString
                                                                           options:0
                                                                           metrics:nil
                                                                             views:viewsDictionary];
            [self.buttonsContainer addConstraints:constraints];
            
            if(soleButton){
                constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[soleButton]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:viewsDictionary];
                [self.buttonsContainer addConstraints:constraints];
            } else {
                if(leftButton){
                    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[leftButton]|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:viewsDictionary];
                    [self.buttonsContainer addConstraints:constraints];
                }
                if(middleButton){
                    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[middleButton]|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:viewsDictionary];
                    [self.buttonsContainer addConstraints:constraints];
                }
                if(rightButton){
                    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[rightButton]|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:viewsDictionary];
                    [self.buttonsContainer addConstraints:constraints];
                }
            }
            
            if(soleButton){
                [soleButton setBackgroundImage:[UIImage imageNamed:@"button_order"]
                                      forState:UIControlStateNormal];
                [soleButton setBackgroundImage:[UIImage imageNamed:@"button_order_h"]
                                      forState:UIControlStateHighlighted];
                [soleButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
                
            } else {
                if(leftButton){
                    [leftButton setBackgroundImage:[UIImage imageNamed:@"button_order_left"]
                                          forState:UIControlStateNormal];
                    [leftButton setBackgroundImage:[UIImage imageNamed:@"button_order_left_h"]
                                          forState:UIControlStateHighlighted];
                    [leftButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
                }
                if(middleButton){
                    [middleButton setBackgroundImage:[UIImage imageNamed:@"button_order_middle"]
                                            forState:UIControlStateNormal];
                    [middleButton setBackgroundImage:[UIImage imageNamed:@"button_order_middle_h"]
                                            forState:UIControlStateHighlighted];
                    [middleButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
                }
                if(rightButton){
                    [rightButton setBackgroundImage:[UIImage imageNamed:@"button_order_right"]
                                           forState:UIControlStateNormal];
                    [rightButton setBackgroundImage:[UIImage imageNamed:@"button_order_right_h"]
                                           forState:UIControlStateHighlighted];
                    [rightButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
                }
            }
        }
    } else {
        
        self.buttonHeightConstraint.constant = 0.0f;
        self.buttonTopConstraint.constant = 0.0f;
        
        self.processButton.hidden = YES;
        self.takeoutButton.hidden = YES;
        self.deliveryButton.hidden = YES;
    }
    
    if([ORDER.merchant.hours length] > 0){
        self.hoursContainer.hidden = NO;
        self.hoursTitleLabel.text = NSLocalizedString(@"Hours", nil);
        self.hoursLabel.text = ORDER.merchant.hours;
        [self.scrollView addSubview:self.hoursContainer];
    } else {
        self.hoursContainer.hidden = YES;
    }
    
    if(ORDER.merchant.isAbleDelivery){
        self.deliveryContainer.hidden = NO;
        [self.scrollView addSubview:self.deliveryContainer];
        
        self.minimumOrderAmountLabel.text = [[NSNumber numberWithCurrency:ORDER.merchant.deliveryMinOrderAmount] currencyString];
        
        if(ORDER.merchant.averageDeliveryTime > 0){
            self.averageDeliveryTimeLabel.text = [NSString stringWithFormat:@"%ld min",(long)ORDER.merchant.averageDeliveryTime];
        } else {
            self.averageDeliveryTimeLabel.text = nil;
            self.averageDeliveryTitleLabel.text = nil;
        }
        
        PCCurrency maxDiscountAmount = 0;
        
        if ([ORDER.merchant.deliveryFeeDiscount count] > 0){
            for(NSDictionary *discountDict in ORDER.merchant.deliveryFeeDiscount){
                if(maxDiscountAmount < [discountDict currencyForKey:@"discount_amount"]){
                    maxDiscountAmount = [discountDict currencyForKey:@"discount_amount"];
                }
            }
        }
        
        switch(ORDER.merchant.deliveryFeePlan){
            case DeliveryFeePlanFlat:
            {
                if(ORDER.merchant.deliveryFee == 0){
                    self.deliveryFeeAmountLabel.text = NSLocalizedString(@"Free", nil);
                } else {
                    
                    NSString *firstString = NSLocalizedString(@"Free", nil);
                    
                    if(maxDiscountAmount > 0){
                        
                        if(ORDER.merchant.deliveryFee - maxDiscountAmount > 0){
                            firstString = [[NSNumber numberWithCurrency:ORDER.merchant.deliveryFee - maxDiscountAmount] currencyString];
                        }
                        
                        self.deliveryFeeAmountLabel.text = [NSString stringWithFormat:@"%@ - %@",
                         firstString,
                         [[NSNumber numberWithCurrency:ORDER.merchant.deliveryFee] currencyString]];
                    } else {
                        self.deliveryFeeAmountLabel.text = [[NSNumber numberWithCurrency:ORDER.merchant.deliveryFee] currencyString];
                    }
                }
                self.deliveryLimitLabel.text = [NSString stringWithDistanceInMile:ORDER.merchant.deliveryDistanceLimit];
            }
                break;
            case DeliveryFeePlanRadius:
            {
                if([ORDER.merchant.deliveryFeeZone count] > 0){
                    DZoneRadius *firstRadius = [ORDER.merchant.deliveryFeeZone objectAtIndex:0];
                    DZoneRadius *lastRadius = [ORDER.merchant.deliveryFeeZone lastObject];
                    self.deliveryLimitLabel.text = [NSString stringWithDistanceInMile:lastRadius.radius];
                    
                    if(firstRadius.feeAmount == lastRadius.feeAmount){
                        if(firstRadius.feeAmount == 0){
                            self.deliveryFeeAmountLabel.text = NSLocalizedString(@"Free", nil);
                        } else {
                            NSString *firstString = NSLocalizedString(@"Free", nil);
                            
                            if(maxDiscountAmount > 0){
                                if(firstRadius.feeAmount - maxDiscountAmount > 0){
                                    firstString = [[NSNumber numberWithCurrency:firstRadius.feeAmount - maxDiscountAmount] currencyString];
                                }
                                self.deliveryFeeAmountLabel.text = [NSString stringWithFormat:@"%@ - %@",
                                                                    firstString,
                                                                    firstRadius.feeAmountString];
                            } else {
                                self.deliveryFeeAmountLabel.text = firstRadius.feeAmountString;
                            }
                        }
                    } else {
                        NSString *firstString = NSLocalizedString(@"Free", nil);
                        NSString *lastString = NSLocalizedString(@"Free", nil);
                        
                        if(firstRadius.feeAmount - maxDiscountAmount > 0) {
                            firstString = [[NSNumber numberWithCurrency:firstRadius.feeAmount - maxDiscountAmount] currencyString];
                        }
                        if(lastRadius.feeAmount > 0) lastString = lastRadius.feeAmountString;
                        
                        self.deliveryFeeAmountLabel.text = [NSString stringWithFormat:@"%@ - %@",
                                                            firstString,
                                                            lastString];
                    }
                    
                    self.deliveryLimitLabel.text = [NSString stringWithDistanceInMile:lastRadius.radius];
                } else {
                    self.deliveryFeeAmountLabel.text = NSLocalizedString(@"Unknown Delivery Fee", nil);
                    self.deliveryLimitLabel.text = NSLocalizedString(@"Unknown Delivery Radius", nil);
                }
            }
                break;
            case DeliveryFeePlanZone:
            {
                PCCurrency smallestFee = 100000000;
                PCCurrency largestFee = -100000000;
                
                for(DZonePolygon *polygon in ORDER.merchant.deliveryFeeZone){
                    if(smallestFee > polygon.feeAmount){
                        smallestFee = polygon.feeAmount;
                    }
                    if(largestFee < polygon.feeAmount){
                        largestFee = polygon.feeAmount;
                    }
                }
                if(smallestFee == largestFee){
                    if(smallestFee == 0){
                        self.deliveryFeeAmountLabel.text = NSLocalizedString(@"Free", nil);
                    } else {
                        if(maxDiscountAmount > 0){
                            NSString *firstString = NSLocalizedString(@"Free", nil);
                            
                            if(smallestFee - maxDiscountAmount > 0){
                                firstString = [[NSNumber numberWithCurrency:smallestFee - maxDiscountAmount] currencyString];
                            }
                            self.deliveryFeeAmountLabel.text = [NSString stringWithFormat:@"%@ - %@",
                                                                firstString,
                                                                [[NSNumber numberWithCurrency:smallestFee] currencyString]];
                        } else {
                            self.deliveryFeeAmountLabel.text = [[NSNumber numberWithCurrency:smallestFee] currencyString];
                        }
                    }
                } else {
                    
                    NSString *firstString = NSLocalizedString(@"Free", nil);
                    
                    if(smallestFee - maxDiscountAmount > 0){
                        firstString = [[NSNumber numberWithCurrency:smallestFee - maxDiscountAmount] currencyString];
                    }

                    
                    self.deliveryFeeAmountLabel.text = [NSString stringWithFormat:@"%@ - %@",
                                                        firstString,
                                                        [[NSNumber numberWithCurrency:largestFee] currencyString]];
                }
                
                self.deliveryLimitLabel.text = NSLocalizedString(@"Customized Zones", nil);
            }
                break;
            default:
                self.deliveryFeeAmountLabel.text = NSLocalizedString(@"Unknown Delivery Plan", nil);
                self.deliveryLimitLabel.text = NSLocalizedString(@"Unknown Delivery Plan", nil);
                break;
        }

    } else {
        self.deliveryContainer.hidden = YES;
    }
    
    NSDictionary *views = @{@"desc":self.descriptionLabel,
                            @"promo":self.promotionContainer,
                            @"button":self.buttonsContainer,
                            @"hours":self.hoursContainer,
                            @"delivery":self.deliveryContainer,
                            @"location":self.locationContainer};
    
    NSArray *exConstraints = [self.scrollView constraintsForAttribute:NSLayoutAttributeTop
                                                       withItem:self.buttonsContainer];
    [self.scrollView removeConstraints:exConstraints];
    
    exConstraints = [self.scrollView constraintsForAttribute:NSLayoutAttributeTop
                                                    withItem:self.locationContainer];
    
    [self.scrollView removeConstraints:exConstraints];
    
    if(!self.promotionContainer.hidden &&
       !self.hoursContainer.hidden &&
       !self.deliveryContainer.hidden){
        
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[desc]-0-[promo]-10-[button]-15-[hours]-12-[delivery]-12-[location]"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:views];
        [self.scrollView addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[promo]|"
                                                              options:0
                                                               metrics:nil
                                                                views:views];
        [self.scrollView addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[hours]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.scrollView addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[delivery]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.scrollView addConstraints:constraints];
        
    } else if(!self.promotionContainer.hidden &&
              !self.hoursContainer.hidden){
        
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[desc]-0-[promo]-10-[button]-15-[hours]-12-[location]"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:views];
        [self.scrollView addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[promo]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.scrollView addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[hours]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.scrollView addConstraints:constraints];

        
    } else if(!self.hoursContainer.hidden &&
              !self.deliveryContainer.hidden){
        
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[desc]-10-[button]-15-[hours]-12-[delivery]-12-[location]"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:views];
        [self.scrollView addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[hours]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.scrollView addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[delivery]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.scrollView addConstraints:constraints];
        
    } else if(!self.promotionContainer.hidden &&
              !self.deliveryContainer.hidden){
        
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[desc]-0-[promo]-10-[button]-15-[delivery]-12-[location]"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:views];
        [self.scrollView addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[promo]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.scrollView addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[delivery]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.scrollView addConstraints:constraints];
        
    } else if(!self.promotionContainer.hidden){
        
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[desc]-0-[promo]-10-[button]-15-[location]"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:views];
        [self.scrollView addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[promo]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.scrollView addConstraints:constraints];
        
    } else if(!self.hoursContainer.hidden){
        
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[desc]-10-[button]-15-[hours]-12-[location]"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:views];
        [self.scrollView addConstraints:constraints];

        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[hours]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.scrollView addConstraints:constraints];
 
        
    } else if(!self.deliveryContainer.hidden){
        
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[desc]-10-[button]-15-[delivery]-12-[location]"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:views];
        [self.scrollView addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[delivery]|"
                                                              options:0
                                                              metrics:nil
                                                                views:views];
        [self.scrollView addConstraints:constraints];
        
    } else {
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[desc]-10-[button]-15-[location]"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:views];
        [self.scrollView addConstraints:constraints];
    }

    self.homepageButton.buttonTitle = ORDER.merchant.homepage;
    if(ORDER.merchant.isRoService){
        self.phoneButton.buttonTitle = NSLocalizedString(@"Call", nil);
    } else {
        self.phoneButton.buttonTitle = ORDER.merchant.phoneNumber;
    }

    self.imageView.hidden = YES;
    self.storeMapView.hidden = NO;
    MKCoordinateSpan span = MKCoordinateSpanMake(SPAN_DELTA, SPAN_DELTA);

    self.miniMapView.region = MKCoordinateRegionMakeWithDistance(ORDER.merchant.location.coordinate,
                                                                 500,
                                                                 500);
    
    if(ORDER.merchant.isDemoMode){
        if(APP.location != nil){
            if(fabs(ORDER.merchant.location.coordinate.latitude) < 0.1 && fabs(ORDER.merchant.location.coordinate.longitude) < 0.1){
                CLLocation *adjustedLocation = [[CLLocation alloc] initWithLatitude:APP.location.coordinate.latitude + ORDER.merchant.location.coordinate.latitude
                                                                          longitude:APP.location.coordinate.longitude + ORDER.merchant.location.coordinate.longitude];
                MKCoordinateRegion region = MKCoordinateRegionMake(adjustedLocation.coordinate,
                                                                   span);
                self.storeMapView.centerCoordinate = adjustedLocation.coordinate;
                self.storeMapView.region = region;
                self.miniMapView.region = region;
            } else {
                if(CLLocationCoordinate2DIsValid(ORDER.merchant.location.coordinate)){
                    MKCoordinateRegion region = MKCoordinateRegionMake(ORDER.merchant.location.coordinate,
                                                                       span);
                    self.storeMapView.centerCoordinate = ORDER.merchant.location.coordinate;
                    self.storeMapView.region = region;
                    self.miniMapView.region = region;
                }
            }
        } else {
            self.storeMapView.hidden = YES;
        }
    } else {
        if(CLLocationCoordinate2DIsValid(ORDER.merchant.location.coordinate)){
            MKCoordinateRegion region = MKCoordinateRegionMake(ORDER.merchant.location.coordinate,
                                                               span);
            self.storeMapView.centerCoordinate = ORDER.merchant.location.coordinate;
            self.storeMapView.region = region;
        }
    }
    
    self.photoScrollView.hidden = YES;
    
    if(ORDER.merchant.photoURLs.count > 0){
        self.photoScrollView.hidden = NO;
        self.imageView.hidden = YES;
        self.storeMapView.hidden = YES;
        
        PCLog(@"Photo count %d", ORDER.merchant.photoURLs.count);
        
        NSUInteger i = 0;
        CGFloat width = self.photoScrollView.bounds.size.width;
        CGFloat height = self.photoScrollView.bounds.size.height;
        
        UIImageView *prevImageView = nil;
        
        for(NSURL *photoURL in ORDER.merchant.photoURLs){
            UIImageView *photoImageView = [[UIImageView alloc] initWithFrame:CGRectMake((width * i), 0.0f, width, height)];
            photoImageView.contentMode = UIViewContentModeScaleAspectFill;
            photoImageView.clipsToBounds = YES;
            photoImageView.contentMode = UIViewContentModeCenter;
            [photoImageView sd_setImageWithURL:photoURL
                              placeholderImage:[UIImage imageNamed:@"placeholder_restaurantlist"]
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                         if(image == nil){
                                             photoImageView.contentMode = UIViewContentModeCenter;
                                         } else {
                                             photoImageView.contentMode = UIViewContentModeScaleAspectFill;
                                         }
                                     }];
            photoImageView.translatesAutoresizingMaskIntoConstraints = NO;
            [self.photoScrollView addSubview:photoImageView];
            
            if(i == 0){
                [self.photoScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[photoImageView(==photoSrollView)]"
                                                                                         options:0
                                                                                         metrics:nil
                                                                                               views:@{@"photoImageView":photoImageView, @"photoSrollView":self.photoScrollView}]];
            } else {
                [self.photoScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[prevImageView][photoImageView(==prevImageView)]"
                                                                                             options:0
                                                                                             metrics:nil
                                                                                               views:@{@"photoImageView":photoImageView, @"prevImageView":prevImageView}]];
            }
            [self.photoScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[photoImageView(==photoSrollView)]|"
                                                                                         options:0
                                                                                         metrics:nil
                                                                                           views:@{@"photoImageView":photoImageView, @"photoSrollView":self.photoScrollView}]];
            
            if(i == ORDER.merchant.photoURLs.count - 1){
                [self.photoScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[photoImageView]|"
                                                                                             options:0
                                                                                             metrics:nil
                                                                                               views:@{@"photoImageView":photoImageView}]];
            }
            
            prevImageView = photoImageView;
            i++;
        }
        self.photoPageViewController.numberOfPages = i;
//        self.photoScrollView.contentSize = CGSizeMake((width * i), height);
        
    } else {
        if(ORDER.merchant.imageURL != nil){
            [self.imageView sd_setImageWithURL:ORDER.merchant.imageURL
                           placeholderImage:[UIImage imageNamed:@"merchant_palceholder"]
                                    options:0
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      if(image != nil){
                                          if(cacheType != SDImageCacheTypeNone){
                                              self.imageView.hidden = NO;
                                              self.storeMapView.hidden = YES;
                                          } else {
                                              self.imageView.alpha = 0.0f;
                                              self.imageView.hidden = NO;
                                              
                                              [UIView animateWithDuration:1.0f
                                                               animations:^{
                                                                   self.imageView.alpha = 1.0f;
                                                                   self.storeMapView.alpha = 0.0f;
                                                               } completion:^(BOOL finished) {
                                                                   if(finished){
                                                                       self.storeMapView.hidden = YES;
                                                                   }
                                                               }];
                                          }
                                      } else {
                                          //Still
                                      }
                                  }];
        }
    }
    if([ORDER.merchant.deliveryFeeDiscount count] == 0){
        self.moreDeliveryFeeButton.hidden = YES;
        self.deliveryFeeTitleLabel.text = NSLocalizedString(@"Delivery Fee :", nil);
        
    } else {
        self.moreDeliveryFeeButton.hidden = NO;
        self.deliveryFeeTitleLabel.text = NSLocalizedString(@"Delivery Fee     :", nil);
    }
    self.photoPageViewController.hidden = self.photoScrollView.hidden;
}

- (IBAction)actionButtonTouched:(UIButton *)sender
{
    if([self.delegate respondsToSelector:@selector(merchantViewController:didTouchActionButton:merchant:withCartType:)]){
        [self.delegate merchantViewController:self
                         didTouchActionButton:sender
                                     merchant:ORDER.merchant
                                 withCartType:(CartType)sender.tag];
    }
}

- (IBAction)favoriteButtonTouched:(NPToggleButton *)sender
{
    if(CRED.isSignedIn){
        
        if(ORDER.merchant.isFavorite){
            [self requestRemoveFavorites:ORDER.merchant];
        } else {
            [self requestAddFavorites:ORDER.merchant];
        }
        
    } else {
        PCWarning(@"OOPS We are not support favorite feature for guests");
    }
}
             
- (void)requestAddFavorites:(Merchant *)merchant
{
    RequestResult result = RRFail;
    result = [PAY requestAddFavorites:@[[NSNumber numberWithSerial:merchant.merchantNo]]
                      currentLocation:APP.location
                      completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              merchant.favorite = YES;
                              self.favoriteButton.selected = YES;

                              [REMOTE handleFavoriteResponse:(NSMutableArray *)response
                                            withNotification:YES];
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while adding favoriets", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)requestRemoveFavorites:(Merchant *)merchant
{
    RequestResult result = RRFail;
    result = [PAY requestRemoveFavorites:@[[NSNumber numberWithSerial:merchant.merchantNo]]
                         currentLocation:APP.location
                      completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              merchant.favorite = NO;
                              self.favoriteButton.selected = NO;
                              
                              [REMOTE handleFavoriteResponse:(NSMutableArray *)response
                                            withNotification:YES];
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while removing favoriets", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)turnEventFlagOn:(BOOL)on
{
    self.eventTitleLabel.hidden = !on;
    self.eventFlagImageView.hidden = !on;
    self.eventLabel.hidden = !on;
}

- (void)turnTestFlagOn:(BOOL)on
{
    self.panelImageView.hidden = !on;
    self.panelLabel.hidden = !on;
    self.panelLabel.text = NSLocalizedString(@"TEST", nil);
    self.panelHeightConstraint.constant = on ? 39.0f : 0.0f;
}

- (void)turnDemoFlagOn:(BOOL)on
{
    self.panelImageView.hidden = !on;
    self.panelLabel.hidden = !on;
    self.panelLabel.text = NSLocalizedString(@"DEMO", nil);
    self.panelHeightConstraint.constant = on ? 39.0f : 0.0f;
}

- (void)turnCloseFlagOn:(BOOL)on
{
    self.panelImageView.hidden = !on;
    self.panelLabel.hidden = !on;
    self.panelLabel.text = NSLocalizedString(@"Closed", nil);
    self.panelHeightConstraint.constant = on ? 39.0f : 0.0f;
}

- (IBAction)homepageButtonTouched:(id)sender
{
    if([ORDER.merchant.homepage length] == 0) return;
    
    NSURL *homepageURL = nil;
    
    if(![ORDER.merchant.homepage hasPrefix:@"http"]){
        homepageURL = [NSURL URLWithString:[@"http://" stringByAppendingString:ORDER.merchant.homepage]];
    } else {
        homepageURL = [NSURL URLWithString:ORDER.merchant.homepage];
    }
    [[UIApplication sharedApplication] openURL:homepageURL];
}

- (IBAction)emailButtonTouched:(id)sender
{
    if([ORDER.merchant.email length] == 0) return;
    
    if(![[UIApplication sharedApplication] openURL:[NSURL URLWithString:[@"mailto://" stringByAppendingString:ORDER.merchant.email]]]){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to Open Mail",nil)
                            message:NSLocalizedString(@"Try to mail manually or contact us", nil)];
    }
}

- (IBAction)phoneButtonTouched:(id)sender
{
    if(ORDER.merchant.isRoService){
        
        CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
        
        DialingOutView *view = [DialingOutView view];
        view.merchantCallButton.buttonTitle = ORDER.merchant.roPhoneNumber;
        NSString *onlyNumberPhoneNumber = [[ORDER.merchant.roPhoneNumber componentsSeparatedByCharactersInSet:
                                            [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                           componentsJoinedByString:@""];
        view.merchantPhoneNumber = onlyNumberPhoneNumber;
        
        [alertView setContainerView:view];
        alertView.buttonTitles = [NSArray arrayWithObject:NSLocalizedString(@"Cancel", nil)];
        
        [alertView show];
        
    } else {
    
        if([ORDER.merchant.phoneNumber length] == 0) return;
        
        NSString *onlyNumberPhoneNumber = [[ORDER.merchant.phoneNumber componentsSeparatedByCharactersInSet:
                                            [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                           componentsJoinedByString:@""];
        if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[@"tel://" stringByAppendingString:onlyNumberPhoneNumber]]]){
            if(![[UIApplication sharedApplication] openURL:[NSURL URLWithString:[@"tel://" stringByAppendingString:onlyNumberPhoneNumber]]]){
                // User's cancel
            }
        } else {
            [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to Call",nil)
                                message:NSLocalizedString(@"Try to call manually or contact us", nil)];
        }
    }
}

- (IBAction)navigationButtonTouched:(NPStretchableButton *)sender
{
    Class itemClass = [MKMapItem class];
    if (itemClass && [itemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)]) {
        // Create an MKMapItem to pass to the Maps app
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:ORDER.merchant.coordinate
                                                       addressDictionary:nil];
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
        [mapItem setName:ORDER.merchant.name];
        // Pass the map item to the Maps app
        
        NSDictionary *launchOptions = nil;
        if(sender.tag == 1){
            launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeWalking};
        } else if(sender.tag == 2){
            launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
        }
        [mapItem openInMapsWithLaunchOptions:launchOptions];
    }
}

- (IBAction)mapButtonTouched:(id)sender
{
    RestaurantMapViewController *viewController = [UIStoryboard viewController:@"RestaurantMapViewController"
                                                                          from:@"Restaurants"];
    viewController.modalTransitionStyle = UIModalTransitionStylePartialCurl;
    viewController.merchantList = [NSMutableArray arrayWithObject:ORDER.merchant];
    viewController.onlyMapMode = YES;
    viewController.title = ORDER.merchant.name;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if(scrollView == self.photoScrollView){
        CGFloat pageWidth = self.photoScrollView.frame.size.width;
        int page = floor((self.photoScrollView.contentOffset.x - (pageWidth / 2) ) / pageWidth) + 1; //this provide you the page number
        self.photoPageViewController.currentPage = page;// this displays the white dot as current page
    }
}

- (IBAction)photoPageControllerChanged:(UIPageControl *)sender {
    PCLog(@"Current Page %d", sender.currentPage);
    CGFloat pageWidth = self.photoScrollView.bounds.size.width;
    [self.photoScrollView setContentOffset:CGPointMake((sender.currentPage - 1) * pageWidth, 0.0f)
                                  animated:YES];
}

@end
