//
//  ContactUsViewController.m
//  RushOrder
//
//  Created by Conan on 7/14/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "ContactUsViewController.h"
#import <Smooch/Smooch.h>
#import "PCCredentialService.h"

@interface ContactUsViewController ()

@end

@implementation ContactUsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Contact Us", nil);
    
#ifdef SLIDE_LEFT_MENU
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemLeftMenu
                                                                        target:self
                                                                        action:@selector(leftMenuButtonTouched:)];
#endif
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)leftMenuButtonTouched:(id)sender
{
//    [APP.viewDeckController toggleLeftViewAnimated:YES];
}

- (IBAction)helpLineButtonTouche:(id)sender
{
    if(CRED.isSignedIn){
        [SKTUser currentUser].firstName = CRED.customerInfo.firstName;
        [SKTUser currentUser].lastName = CRED.customerInfo.lastName;
        [SKTUser currentUser].email = CRED.customerInfo.email;
        [[SKTUser currentUser] addProperties:@{ @"Customer" : [NSString stringWithFormat:@"https://www.rushorderapp.com/customers/customers/%lld", CRED.customerInfo.customerNo]
                                                , @"phoneNumber" : CRED.customerInfo.phoneNumber ? CRED.customerInfo.phoneNumber : @"Unknown"
                                                , @"birthday" : CRED.customerInfo.birthday ? CRED.customerInfo.birthday : @"Unknown"
                                                , @"UUID": [NSString stringWithFormat:@"https://www.rushorderapp.com/douglas/guests/%@", CRED.udid]
                                                }];
        
    } else {
        
        [SKTUser currentUser].firstName = [NSUserDefaults standardUserDefaults].custName ? [NSUserDefaults standardUserDefaults].custName : @"?";
        [SKTUser currentUser].lastName = @"Guest";
        [SKTUser currentUser].email = [NSUserDefaults standardUserDefaults].emailAddress;
        [[SKTUser currentUser] addProperties:@{ @"phoneNumber" : [NSUserDefaults standardUserDefaults].custPhone ? [NSUserDefaults standardUserDefaults].custPhone : @"Unknown"
                                                , @"UUID": [NSString stringWithFormat:@"https://www.rushorderapp.com/douglas/guests/%@", CRED.udid]
                                                }];
    }
    

    [Smooch show];
}

@end

/*

 Order Inquiry
 For immediate assistance regarding an existing order, please call 424-488-3170.
 
 
 Questions/Comments regarding RushOrder
 For general inquiries or comments concerning RushOrder, email us at help@rushorderapp.com or call 310-473-8092.
 
 
 Suggest a Restaurant
 Have a favorite restaurant that you'd like to see on RushOrder? Send us your suggestions at request@rushorderapp.com


*/
