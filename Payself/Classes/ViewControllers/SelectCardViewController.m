//
//  SelectCardViewController.m
//  RushOrder
//
//  Created by Conan on 2/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "SelectCardViewController.h"
#import "NewCardViewController.h"
#import "MobileCard.h"
#import "PCPaymentService.h"
#import "ReceiptViewController.h"
#import "Merchant.h"
#import "OrderManager.h"
#import "TransactionManager.h"
#import "PCCredentialService.h"
#import "RemoteDataManager.h"

enum _alertViewTag{
    AlertViewTagStripeError = 1,
    AlertViewTagContinuePayment,
    AlertViewTagInvalidOrderError
};

@interface SelectCardViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NPStretchableButton *neoCardButton;
@property (strong, nonatomic) IBOutlet UIView *noItemView;
@property (weak, nonatomic) IBOutlet UIView *infoHeaderView;

@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

@end

@implementation SelectCardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Select Card", nil);
    
    if(self.isManageMode){
        self.title = NSLocalizedString(@"Manage Cards", nil);
        self.neoCardButton.buttonTitle = NSLocalizedString(@"Add New Card", nil);
    } else {
        
        self.title = NSLocalizedString(@"Select Card", nil);
        self.neoCardButton.buttonTitle = NSLocalizedString(@"Save", nil);
        
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemCancel
                                                                            target:self
                                                                            action:@selector(cancelButtonTouched:)];
        
        self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemSave
                                                                            target:self
                                                                            action:@selector(saveButtonTouched:)];

    }
    
    self.tableView.rowHeight = 56.0f;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}

- (void)cancelButtonTouched:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{

                             }];
}

- (void)saveButtonTouched:(id)sender
{
    [self requestUpdateMobileCard];
}

- (void)requestUpdateMobileCard
{
    FavoriteOrder *temp = [self.favoriteOrder copy];
    
    temp.card = self.selectedCard;
    
    RequestResult result = RRFail;
    result = [PAY requestUpdateRapidReOrder:temp
                                       type:FOupdateTypePayment
                            completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              [self.favoriteOrder updateWithDictionary:response];
                              
                              [self dismissViewControllerAnimated:YES
                                                       completion:^{
                                                           
                                                       }];
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while updating payment of RapidOrder", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cardListChanged:)
                                                 name:CardListChangeNotification
                                               object:nil];
//    if(self.tableView.contentOffset.y == 0){
//        [self.tableView setContentOffset:CGPointMake(0, self.searchDisplayController.searchBar.height)
//                                animated:NO];
//    }
    

    if(CRED.isSignedIn){
        self.mobileCardList = REMOTE.cards;
    } else {
        self.mobileCardList = [MobileCard listAll];
    }
    
    if([self.mobileCardList count] == 0){
        self.tableView.tableHeaderView = self.noItemView;
    } else {
        self.tableView.tableHeaderView = self.infoHeaderView;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.tableView reloadData];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:CardListChangeNotification
                                                  object:nil];
}

- (void)cardListChanged:(NSNotification *)notification
{

    if(CRED.isSignedIn){
        self.mobileCardList = REMOTE.cards;
    } else {
        self.mobileCardList = [MobileCard listAll];
    }
    
    if([self.mobileCardList count] == 0){
        self.tableView.tableHeaderView = self.noItemView;
    } else {
        self.tableView.tableHeaderView = self.infoHeaderView;
    }
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = [self.mobileCardList count];
    
    if(!self.isManageMode){
        
        self.navigationItem.rightBarButtonItem.enabled = (count > 0);
        self.neoCardButton.enabled = (count > 0);
        
    }

    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MobileCardCell";
    
    MobileCardCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [MobileCardCell cell];
        if(self.isManageMode){
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            cell.checkMarkView.hidden = NO;
            cell.checkMarkView.normalImage = [UIImage imageNamed:@"button_delete_card"];
            [cell.checkMarkView setImage:[UIImage imageNamed:@"button_delete_card"]
                                forState:UIControlStateNormal];
            [cell.checkMarkView setImage:nil
                                forState:UIControlStateSelected];
            cell.checkMarkView.userInteractionEnabled = YES;
            cell.delegate = self;
        } else {
            cell.checkMarkView.hidden = YES;
            [cell.checkMarkView setImage:[UIImage imageNamed:@"icon_check"]
                                forState:UIControlStateNormal];
            cell.checkMarkView.userInteractionEnabled = NO;
        }
    }
    
    if([self.mobileCardList count] > 0){
        
        cell.neoCardLabel.hidden = YES;
        cell.titleLabel.hidden = NO;
        cell.subTitleLabel.hidden = NO;
        cell.thumbnailImageView.hidden = NO;
        
        MobileCard *mobileCard = [self.mobileCardList objectAtIndex:indexPath.row];
        cell.titleLabel.text = mobileCard.formattedCardNumber;
        cell.subTitleLabel.text = [NSString stringWithFormat:@"%@ (%02ld/%02ld)",
                                   mobileCard.bankName,
                                   (long)mobileCard.cardExpireMonth,
                                   (long)mobileCard.cardExpireYear];
        cell.thumbnailImageView.image = mobileCard.cardLogo;
        
        if(!self.isManageMode){
            if(self.selectedCard.cardId == mobileCard.cardId){
                cell.checkMarkView.hidden = NO;
            } else {
                cell.checkMarkView.hidden = YES;
            }
        }
        
    } else {
        
        cell.neoCardLabel.hidden = NO;
        cell.titleLabel.hidden = YES;
        cell.subTitleLabel.hidden = YES;
        cell.thumbnailImageView.hidden = YES;

    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView
             cell:(MobileCardCell *)cell
didTouchActionButtonAtIndex:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Are you sure?", nil)
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"NO", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"YES", nil)
                                                    otherButtonTitles: nil];
    [actionSheet showInView:self.view];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.isManageMode){
        if([self.mobileCardList count] > 0){
            return YES;
        } else {
            return NO;
        }        
    } else {
        return NO;
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.mobileCardList count] > 0){
        return UITableViewCellEditingStyleDelete;
    } else {
        return UITableViewCellEditingStyleNone;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NSLocalizedString(@"Remove", nil);
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.mobileCardList count] == 0){
        return;
    }
    
    if(editingStyle == UITableViewCellEditingStyleDelete){
        [self removeMobileCardAtIndexPath:indexPath];
    }
}

- (void)removeMobileCardAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.mobileCardList count] <= indexPath.row){
        PCError(@"Cannot remove card at IndexPath %@", indexPath);
        return;
    }
    
    [self.tableView beginUpdates];
    MobileCard *mobileCard = [self.mobileCardList objectAtIndex:indexPath.row];
    if([mobileCard delete]){
        [self.mobileCardList removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                              withRowAnimation:UITableViewRowAnimationBottom];
        
        if([self.mobileCardList count] == 0){
            self.tableView.tableHeaderView = self.noItemView;
        }
    }
    [self.tableView endUpdates];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.mobileCardList count] == 0){
        return;
    }
    
    MobileCard *card = [self.mobileCardList objectAtIndex:indexPath.row];
    if(!self.isManageMode){
        self.selectedCard = card;
        [self.tableView reloadData];
    }
}

- (IBAction)newCardButtonTouched:(id)sender
{
    if(self.isManageMode){
        NewCardViewController *viewController = [NewCardViewController viewControllerFromNib];
        viewController.payment = self.payment;
        viewController.manageMode = self.isManageMode;
        [self.navigationController pushViewController:viewController
                                             animated:YES];
    } else {
        [self saveButtonTouched:sender];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == actionSheet.destructiveButtonIndex){
        if(CRED.isSignedIn){
            MobileCard *mobileCard = [self.mobileCardList objectAtIndex:self.selectedIndexPath.row];
            [self requestRemoveCard:mobileCard];
        } else {
            [self removeMobileCardAtIndexPath:self.selectedIndexPath];
        }
    }
}

- (void)requestRemoveCard:(MobileCard *)card
{
    RequestResult result = RRFail;
    result = [PAY requestRemoveCard:card
                    completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              [REMOTE handleCardsResponse:(NSMutableArray *)response
                                         withNotification:NO];
                              
                              self.mobileCardList = REMOTE.cards;
                              
                              if([self.mobileCardList count] == 0){
                                  self.tableView.tableHeaderView = self.noItemView;
                              } else {
                                  self.tableView.tableHeaderView = self.infoHeaderView;
                              }
                              
                              [self.tableView reloadData];
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while removing card", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}


@end
