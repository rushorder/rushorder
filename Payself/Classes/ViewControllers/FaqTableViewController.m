//
//  FaqTableViewController.m
//  RushOrder
//
//  Created by Conan Kim on 3/22/16.
//  Copyright © 2016 Paycorn. All rights reserved.
//

#import "FaqTableViewController.h"
#import "PCGeneralService.h"
#import "FaqCEll.h"
#import "FaqViewController.h"

@interface FaqTableViewController()
@property (strong, nonatomic) NSArray *faqs;
@end

@implementation FaqTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 43.5;
    [self requestFaq];
}

- (void)requestFaq
{
    RequestResult result = RRFail;
    result = [GENERAL requestFaqWithCompletionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              PCLog(@"REsponse : %@", response);
                              
                              self.faqs = (NSArray *)response;
                              [self.tableView reloadData];
                              
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting my points", nil)];
                                  
                              }
                              break;
                          }
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
                  [self.refreshControl endRefreshing];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            [self.refreshControl endRefreshing];
            break;
        default:
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.faqs count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FaqCEll *cell = [tableView dequeueReusableCellWithIdentifier:@"faqTitleCell"];
    cell.faqTitleLabel.text = [[self.faqs objectAtIndex:indexPath.row] objectForKey:@"title"];
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    FaqViewController *viewController = segue.destinationViewController;
    NSIndexPath *selectedIndex = [self.tableView indexPathForCell:sender];
    NSDictionary *selectedFaq = [self.faqs objectAtIndex:selectedIndex.row];
    viewController.faq = selectedFaq;
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
