//
//  MenuPhotoPageViewController.m
//  RushOrder
//
//  Created by Conan on 12/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MenuPhotoPageViewController.h"

@interface MenuPhotoPageViewController ()

@end

@implementation MenuPhotoPageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
