//
//  DeliveryViewController.m
//  RushOrder
//
//  Created by Conan on 11/13/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "DeliveryViewController.h"
#import "PhoneNumberFormatter.h"
#import "OrderManager.h"
#import "RecentAddressCell.h"
#import <AddressBook/AddressBook.h>
#import "PCCredentialService.h"
#import "RemoteDataManager.h"
#import "PaymentMethodViewController.h"
#import "NewCardViewController.h"
#import "LocationViewController.h"
#import "GMPrediction.h"
#import "AccountSwitchingManager.h"
#import "SignInCustomerViewController.h"
#import "MenuManager.h"
#import "DeliveryAddress.h"
@import Firebase;

#define MINUTE_SPAN     15
#define LIMIT_HOUR      3

enum {
    PickerViewTagTime = 1,
    PickerViewTagState = 2
};

@interface DeliveryViewController ()
{
    int _textFieldSemaphore;
    __strong PhoneNumberFormatter *_phoneNumberFormatter;
}
@property (strong, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPickerView *timePickerView;
@property (strong, nonatomic) IBOutlet UIToolbar *pickerToolBar;
@property (weak, nonatomic) IBOutlet UITextField *pickupTimeTextField;
@property (weak, nonatomic) IBOutlet NPStretchableButton *pickupTimeSelectButton;
//@property (weak, nonatomic) IBOutlet UILabel *remarkLabel;
@property (strong, nonatomic) NSMutableArray *timeArray;
@property (strong, nonatomic) NSArray *stateList;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *PickerTitleBarButtonItem;

@property (strong, nonatomic) NSMutableArray *mobileCardList;

@property (strong, nonatomic) IBOutlet UIView *recentAddressView;
@property (weak, nonatomic) IBOutlet UIButton *locationButton;
@property (strong, nonatomic) IBOutlet UIView *recentContainerView;
@property (weak, nonatomic) IBOutlet UIButton *orderButton;

@property (strong, nonatomic) NSMutableArray *addresses;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NPStretchableButton *recentAddressButton;

@property (strong, nonatomic) CLGeocoder *geoCoder;
@property (nonatomic) NSUInteger selectedAddressIndex;

@property (weak, nonatomic) IBOutlet UILabel *placeHolderLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *saveButtonTopConstraint;
@property (weak, nonatomic) IBOutlet UILabel *customerRequestTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *customerRequestBackgroundImageView;
@property (weak, nonatomic) IBOutlet UITextView *customerRequestTextView;
//@property (weak, nonatomic) IBOutlet UILabel *customerRequestPlaceholderLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressTopConstraint;
@property (weak, nonatomic) IBOutlet UIView *emailAdditionView;
@property (weak, nonatomic) IBOutlet PCTextField *emailTextField;
@property (weak, nonatomic) IBOutlet PCTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UILabel *asapAlertLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *asapAlertHeightConstratin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *asapAlertTopConstraint;

@property (weak, nonatomic) IBOutlet NPStretchableButton *recentRequestButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *defaultAddressButton;
@property (strong, nonatomic) RecentListViewController *recentRequestViewController;
@property (strong, nonatomic) NSMutableArray *suggestingCustomRequests;
@property (strong, nonatomic) IBOutlet UIButton *suggestButton;
@property (nonatomic, getter = isCardListWaiting) BOOL cardListWaiting;
@property (nonatomic, getter = isDeliveryAddressWaiting) BOOL deliveryAddressWaiting;
@property (nonatomic, getter = isShowDeliveryAddressListAfterFetching) BOOL showDeliveryAddressListAfterFetching;
@property (nonatomic, getter = isNeedCustomerInfoOverwriting) BOOL needCustomerInfoOverwriting;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *customerRequestTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *customerRequestTopConstraint2;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *takeoutLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *switchButtonSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dineinTrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *etaTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *etaSpaceContraint;
@property (weak, nonatomic) IBOutlet NPStretchableButton *takeoutButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *dineinButton;

@property (nonatomic, getter = isAppeared) BOOL appeared;

@end

@implementation DeliveryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _textFieldSemaphore = 0;
        _phoneNumberFormatter = [[PhoneNumberFormatter alloc] init];
        self.selectedAddressIndex = NSNotFound;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(signedIn:)
                                                     name:SignedInNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(signedOut:)
                                                     name:SignedOutNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(cardListChanged:)
                                                     name:CardListChangeNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(deliveryAddressChanged:)
                                                     name:DeliveryAddressChangeNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)signedIn:(NSNotification *)aNoti
{
    self.mobileCardList = nil;
    self.needCustomerInfoOverwriting = YES;
    // This code makes current name and number nil - so commented out
//    ORDER.tempDeliveryAddress.receiverName = nil;
//    ORDER.tempDeliveryAddress.phoneNumber = nil;
}

- (void)signedOut:(NSNotification *)aNoti
{
    self.mobileCardList = nil;
}

- (void)cardListChanged:(NSNotification *)notification
{
    if(CRED.isSignedIn){
        self.mobileCardList = REMOTE.cards;
        if(self.isCardListWaiting){
            self.cardListWaiting = NO;
            [self stepForPayment];
        }
    } else {
        self.mobileCardList = [MobileCard listAll];
    }
}

- (void)deliveryAddressChanged:(NSNotification *)notification
{
    if(self.isDeliveryAddressWaiting){
        if(REMOTE.defaultDeliveryAddress != nil){
            [self fillDeliveryAddress:REMOTE.defaultDeliveryAddress];
        } else {
            [self fillInitialDeliveryTextFields];
        }
        self.deliveryAddressWaiting = NO;
        if(self.isShowDeliveryAddressListAfterFetching){
            self.showDeliveryAddressListAfterFetching = NO;
            [self recentAddressButtonTouched:notification];
        }
    }
    
    if(self.recentContainerView.superview != nil){
        [self.tableView reloadData];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.customerRequestTextView.autocorrectionType = UITextAutocorrectionTypeNo;
    
//    [self.scrollView setContentSizeWithBottomView:self.orderButton];
    
    self.title = NSLocalizedString(@"Delivery Option", nil);

    if(self.favoriteOrder != nil){
        
        self.title = NSLocalizedString(@"Delivery Address", nil);
        self.orderButton.buttonTitle = NSLocalizedString(@"Save", nil);
        
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemCancel
                                                                            target:self
                                                                            action:@selector(cancelButtonTouched:)];
        
        self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemSave
                                                                             target:self
                                                                             action:@selector(saveButtonTouched:)];
        
        self.saveButtonTopConstraint.constant = 24.0f;
        self.customerRequestTextView.hidden = YES;
        self.customerRequestTitleLabel.hidden = YES;
        self.customerRequestBackgroundImageView.hidden = YES;
        self.placeHolderLabel.hidden = YES;
        self.recentRequestButton.hidden = YES;
        self.defaultAddressButton.hidden = YES;
        
    } else {
        
        if(self.isRootViewController){
            self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemCancel
                                                                                target:self
                                                                                action:@selector(cancelButtonTouched:)];
        } else {
            self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemBack
                                                                                target:self
                                                                                action:@selector(backButtonTouched:)];
        }
        
        if(self.defaultAddressButton.isHidden){
            self.saveButtonTopConstraint.constant = 142.0f;
        } else {
            self.saveButtonTopConstraint.constant = 174.0f;
        }
        
        self.customerRequestTextView.hidden = NO;
        self.customerRequestTitleLabel.hidden = NO;
        self.customerRequestBackgroundImageView.hidden = NO;
        self.placeHolderLabel.hidden = NO;
        self.recentRequestButton.hidden = NO;
//        self.defaultAddressButton.hidden = NO; // Don't show (hidden == NO) here, because it will be decided later (showDefaultAddressButton:)
        
        self.customerRequestTextView.inputAccessoryView = self.suggestButton;
    }
    
    [self.scrollView setContentSizeWithBottomView:self.orderButton];
    
    self.pickupTimeTextField.inputView = self.timePickerView;
    self.pickupTimeTextField.inputAccessoryView = self.pickerToolBar;
    
    self.stateTextField.inputView = self.timePickerView;
    self.stateTextField.inputAccessoryView = self.pickerToolBar;
    
//    if([ORDER.merchant.takeoutRemark length] > 0){
//        self.remarkLabel.text = ORDER.merchant.takeoutRemark;
//    } else {
//        self.remarkLabel.text = NSLocalizedString(@"Thank you for your order", nil);
//    }

    [self.recentAddressButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
    [self.recentRequestButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
    [self.locationButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
    self.stateList = @[@"Alabama",
                       @"Alaska",
                       @"Arizona",
                       @"Arkansas",
                       @"California",
                       @"Colorado",
                       @"Connecticut",
                       @"Delaware",
                       @"Florida",
                       @"Georgia",
                       @"Hawaii",
                       @"Idaho",
                       @"Illinois",
                       @"Indiana",
                       @"Iowa",
                       @"Kansas",
                       @"Kentucky",
                       @"Louisiana",
                       @"Maine",
                       @"Maryland",
                       @"Massachusetts",
                       @"Michigan",
                       @"Minnesota",
                       @"Mississippi",
                       @"Missouri",
                       @"Montana",
                       @"Nebraska",
                       @"Nevada",
                       @"New Hampshire",
                       @"New Jersey",
                       @"New Mexico",
                       @"New York",
                       @"North Carolina",
                       @"North Dakota",
                       @"Ohio",
                       @"Oklahoma",
                       @"Oregon",
                       @"Pennsylvania",
                       @"Rhode Island",
                       @"South Carolina",
                       @"South Dakota",
                       @"Tennessee",
                       @"Texas",
                       @"Utah",
                       @"Vermont",
                       @"Virginia",
                       @"Washington",
                       @"West Virginia",
                       @"Wisconsin",
                       @"Wyoming"];
    
    
    if(REMOTE.isDeliveryAddressFetched){
        if(REMOTE.defaultDeliveryAddress != nil){
            [self fillDeliveryAddress:REMOTE.defaultDeliveryAddress];
        } else {
            [self fillInitialDeliveryTextFields];
        }
    } else {
        self.deliveryAddressWaiting = YES;
        [REMOTE requestDeliveryAddresses];
    }
    
    NSDate *minimumDateForDelivery = [NSDate dateWithTimeIntervalSinceNow:(60 * 90)]; // 1hour 30 min
    
    BOOL ableToSelectSpecificTime = YES;
    

    if(![ORDER.merchant isOpenHourWithDate:minimumDateForDelivery]){
        ableToSelectSpecificTime = NO;
    } else if(![ORDER.merchant isDeliveryHourWithDate:minimumDateForDelivery]){
        ableToSelectSpecificTime = NO;
    }
    
    if(ableToSelectSpecificTime){
        self.asapAlertLabel.hidden = YES;
        self.asapAlertHeightConstratin.constant = 0.0f;
        self.asapAlertTopConstraint.constant = 0.0f;
        self.pickupTimeButton.enabled = YES;
    } else {
        self.pickupTimeButton.enabled = NO;
    }
    
    if(self.favoriteOrder != nil){
        
        self.address1TextField.text = self.favoriteOrder.address1;
        self.address2TextField.text = self.favoriteOrder.address2;
        self.zipCodeTextField.text = self.favoriteOrder.zip;
        self.cityTextField.text = self.favoriteOrder.city;
        self.stateTextField.text = self.favoriteOrder.state;
        
        self.nameTextField.text = self.favoriteOrder.receiverName;
        self.phoneNumberTextField.text = self.favoriteOrder.phoneNumber;
        
        if(self.favoriteOrder.pickupAfter == 0 || !self.pickupTimeButton.enabled){
            [self asapButtonTouched:self.asapButton];
        } else {
            [self specificButtonTouched:nil];
            NSDate *now = [NSDate date];
            NSDate *afterNow = [now dateByAddingTimeInterval:self.favoriteOrder.pickupAfter * 60];
            self.pickupTimeTextField.text = afterNow.hourMinuteString;
            self.afterMinute = self.favoriteOrder.pickupAfter;

            [self.timePickerView selectRow:self.favoriteOrder.pickupAfter / MINUTE_SPAN
                               inComponent:0
                                  animated:NO];
        }
    }
}

- (void)fillInitialDeliveryTextFields
{
    if(CRED.isSignedIn){
        
        if(self.isNeedCustomerInfoOverwriting){ // reason : Ability to Sign-in During Checkout Process (https://app.asana.com/0/inbox/23035815421587/222338978099920/222338978099925)
            self.nameTextField.text = CRED.customerInfo.name;
        } else {
            if([self.nameTextField.text length] == 0){
                self.nameTextField.text = CRED.customerInfo.name;
            }
        }
        
        if(self.isNeedCustomerInfoOverwriting){ // reason : Ability to Sign-in During Checkout Process (https://app.asana.com/0/inbox/23035815421587/222338978099920/222338978099925)
            self.phoneNumberTextField.text = CRED.customerInfo.phoneNumber;
        } else {
            if([self.phoneNumberTextField.text length] == 0){
                self.phoneNumberTextField.text = CRED.customerInfo.phoneNumber;
            }
        }
    }
    
    if([self.nameTextField.text length] == 0){
        self.nameTextField.text = [NSUserDefaults standardUserDefaults].custName;
    }
    
    if([self.phoneNumberTextField.text length] == 0){
        self.phoneNumberTextField.text = [NSUserDefaults standardUserDefaults].custPhone;
    }
    
    if([self.nameTextField.text length] > 0){
        self.nameTextField.placeholder = self.nameTextField.text;
    }
    
    if([self.phoneNumberTextField.text length] > 0){
        self.phoneNumberTextField.placeholder = self.phoneNumberTextField.text;
    }
    
    [self phoneNumberValueChanged:self.phoneNumberTextField];
    
    if([self.addresses count] > 0){
        Addresses *address = [self.addresses objectAtIndex:0];
        [self fillAddresses:address];
    }
    
    self.needCustomerInfoOverwriting = NO;
}

- (void)saveButtonTouched:(id)sender
{
    [self doneButtonTouched:sender];
}

- (void)backButtonTouched:(id)sender
{
    if(ORDER.order.status == OrderStatusDraft
       && ORDER.cart.cartType != CartTypeCart){
        if(self.disposeOrderWhenBack){
            ORDER.order = nil;
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)cancelButtonTouched:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{

                             }];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.appeared = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
//    ORDER.tempDeliveryAddress = [[Addresses alloc] init];
    ORDER.tempDeliveryAddress.address1 = self.address1TextField.text;
    ORDER.tempDeliveryAddress.address2 = self.address2TextField.text;
    ORDER.tempDeliveryAddress.zip = self.zipCodeTextField.text;
    ORDER.tempDeliveryAddress.city = self.cityTextField.text;
    ORDER.tempDeliveryAddress.state = self.stateTextField.text;
    ORDER.tempDeliveryAddress.receiverName = self.nameTextField.text;
    ORDER.tempDeliveryAddress.phoneNumber = self.phoneNumberTextField.text;
    ORDER.tempDeliveryAddress.coordinate = self.geocodedCoords;
    ORDER.tempDeliveryAddress.needToBeUpdatedToDefault = self.defaultAddressButton.selected;
    if(ORDER.tempDeliveryAddressFilled < 3) ORDER.tempDeliveryAddressFilled = 3;
    
    ORDER.tempAfterMinute = self.afterMinute;
    ORDER.tempCustomerRequest = self.customerRequestTextView.text;
}

- (void)drawTopPartAccordingToOrderType
{
    /*
     @property (weak, nonatomic) IBOutlet NSLayoutConstraint *takeoutLeadingConstraint;
     @property (weak, nonatomic) IBOutlet NSLayoutConstraint *switchButtonSpaceConstraint;
     @property (weak, nonatomic) IBOutlet NSLayoutConstraint *dineinTrailingConstraint;
     @property (weak, nonatomic) IBOutlet NSLayoutConstraint *etaTopConstraint;
     @property (weak, nonatomic) IBOutlet NSLayoutConstraint *etaSpaceContraint;
     @property (weak, nonatomic) IBOutlet NPStretchableButton *takeoutButton;
     @property (weak, nonatomic) IBOutlet NPStretchableButton *dineinButton;
     */
    
    if(ORDER.merchant.isAbleDinein && ORDER.merchant.isAbleTakeout){
        self.takeoutButton.hidden = NO;
        self.dineinButton.hidden = NO;
        self.switchButtonSpaceConstraint.priority = 999;
        self.takeoutLeadingConstraint.priority = 500;
        self.dineinTrailingConstraint.priority = 500;
        self.etaTopConstraint.priority = 500;
        self.etaSpaceContraint.priority = 999;
    } else if(ORDER.merchant.isAbleDinein){
        self.takeoutButton.hidden = YES;
        self.dineinButton.hidden = NO;
        self.switchButtonSpaceConstraint.priority = 500;
        self.takeoutLeadingConstraint.priority = 999;
        self.dineinTrailingConstraint.priority = 999;
        self.etaTopConstraint.priority = 500;
        self.etaSpaceContraint.priority = 999;
    } else if(ORDER.merchant.isAbleTakeout){
        self.takeoutButton.hidden = NO;
        self.dineinButton.hidden = YES;
        self.switchButtonSpaceConstraint.priority = 500;
        self.takeoutLeadingConstraint.priority = 999;
        self.dineinTrailingConstraint.priority = 999;
        self.etaTopConstraint.priority = 500;
        self.etaSpaceContraint.priority = 999;
    } else {
        self.takeoutButton.hidden = YES;
        self.dineinButton.hidden = YES;
        self.switchButtonSpaceConstraint.priority = 999;
        self.takeoutLeadingConstraint.priority = 500;
        self.dineinTrailingConstraint.priority = 500;
        self.etaTopConstraint.priority = 999;
        self.etaSpaceContraint.priority = 500;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self drawTopPartAccordingToOrderType];
    
    if(self.isNeedCustomerInfoOverwriting){
        
        if(REMOTE.isDeliveryAddressFetched){
            if(REMOTE.defaultDeliveryAddress != nil){
                [self fillDeliveryAddress:REMOTE.defaultDeliveryAddress];
            } else {
                [self fillInitialDeliveryTextFields];
            }
            self.needCustomerInfoOverwriting = NO;
        } else {
            self.deliveryAddressWaiting = YES;
            [REMOTE requestDeliveryAddresses];
        }
    } else {
        if(ORDER.isSetTempDelivery){
            if(ORDER.tempDeliveryAddressFilled >= 3){
                self.address1TextField.text = ORDER.tempDeliveryAddress.address1;
                self.address2TextField.text = ORDER.tempDeliveryAddress.address2;
                self.zipCodeTextField.text = ORDER.tempDeliveryAddress.zip;
                self.cityTextField.text = ORDER.tempDeliveryAddress.city;
                self.stateTextField.text = ORDER.tempDeliveryAddress.state;
                self.geocodedCoords = ORDER.tempDeliveryAddress.coordinate;
            }
            
            if(ORDER.tempDeliveryAddressFilled >= 1){
                if(ORDER.tempDeliveryAddress.receiverName.length > 0){
                    self.nameTextField.text = ORDER.tempDeliveryAddress.receiverName;
                }
            }
            
            if(ORDER.tempDeliveryAddressFilled >= 2){
                if(ORDER.tempDeliveryAddress.phoneNumber.length > 0){
                    self.phoneNumberTextField.text = ORDER.tempDeliveryAddress.phoneNumber;
                }
            }
            
            self.customerRequestTextView.text = ORDER.tempCustomerRequest;
            [self textViewDidChange:self.customerRequestTextView];
            
            self.afterMinute = ORDER.tempAfterMinute;
            if(self.afterMinute == 0 || !self.pickupTimeButton.enabled){
                [self asapButtonTouched:self.asapButton];
            } else {
                [self specificButtonTouched:nil];
                NSDate *now = [NSDate date];
                NSDate *afterNow = [now dateByAddingTimeInterval:self.afterMinute * 60];
                self.pickupTimeTextField.text = afterNow.hourMinuteString;
            }
            
            if(ORDER.tempDeliveryAddressFilled >= 3){
                self.defaultAddressButton.selected = ORDER.tempDeliveryAddress.isNeedToBeUpdatedToDefault;
            }
        }
    }
    
    if(CRED.isSignedIn){
        self.emailAdditionView.hidden = YES;
        self.addressTopConstraint.constant = 24.0f;
        self.phoneNumberTextField.nextField = self.address1TextField;
        
        if(self.isNeedCustomerInfoOverwriting){ // reason : Ability to Sign-in During Checkout Process (https://app.asana.com/0/inbox/23035815421587/222338978099920/222338978099925)
            self.nameTextField.text = CRED.customerInfo.name;
        } else {
            if([self.nameTextField.text length] == 0){
                self.nameTextField.text = CRED.customerInfo.name;
            }
        }
        
        if(self.isNeedCustomerInfoOverwriting){ // reason : Ability to Sign-in During Checkout Process (https://app.asana.com/0/inbox/23035815421587/222338978099920/222338978099925)
            self.phoneNumberTextField.text = CRED.customerInfo.phoneNumber;
        } else {
            if([self.phoneNumberTextField.text length] == 0){
                self.phoneNumberTextField.text = CRED.customerInfo.phoneNumber;
            }
        }
        
        self.needCustomerInfoOverwriting = NO;
        
    } else {
        self.emailAdditionView.hidden = NO;
        self.addressTopConstraint.constant = 150.0f;
        self.phoneNumberTextField.nextField = self.emailTextField;
        if([self.emailTextField.text length] == 0){
            self.emailTextField.text = [NSUserDefaults standardUserDefaults].emailAddress;
        }
    }
}


- (IBAction)doneButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    [NSUserDefaults standardUserDefaults].custName = self.nameTextField.text;
    [NSUserDefaults standardUserDefaults].custPhone = self.phoneNumberTextField.text;
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if(self.pickupTimeButton.selected &&
       self.afterMinute == 0){
        [UIAlertView alertWithTitle:NSLocalizedString(@"No Specific Time", nil)
                            message:NSLocalizedString(@"You should select specific time you want." ,nil)];
        [self.pickupTimeTextField becomeFirstResponder];
        return;
    }
    
    // Check for Valid Delivery Time
    NSDate *specificDate = [NSDate date];
    
    if(self.pickupTimeButton.selected){
        NSTimeInterval afterInterval = self.afterMinute * 60;
        specificDate = [[NSDate date] dateByAddingTimeInterval:afterInterval];
    }
    

    if(![ORDER.merchant isOpenHourWithDate:specificDate]){
        [UIAlertView alertWithTitle:NSLocalizedString(@"At this time, we are only able to accept delivery orders for ASAP.", nil)
                            message:nil];
        return;
    }
    
    if(![ORDER.merchant isDeliveryHourWithDate:specificDate]){
        NSString *hourString = [ORDER.merchant nextDeliveryAvailableTime];
        [UIAlertView alertWithTitle:NSLocalizedString(@"At this time, we are only able to accept delivery orders for ASAP", nil)
                            message:[NSString stringWithFormat:NSLocalizedString(@"Next Available Time:\n%@", nil), [hourString length] > 0 ? hourString : NSLocalizedString(@"N/A", nil)]];
        
        return;
    }

    
    if(![VALID validate:self.nameTextField
                  title:NSLocalizedString(@"Please Enter Name", nil)
                message:NSLocalizedString(@"Name is Required to Complete This Order.", nil)]){
        return;
    }
    
    if(![VALID validate:self.phoneNumberTextField
                  title:NSLocalizedString(@"Please Enter Phone Number", nil)
                message:NSLocalizedString(@"A Phone Number is Required to Complete This Order.", nil)]){
        return;
    }
    
    NSString * number = self.phoneNumberTextField.text;
    
    NSString *phoneRegex2 = @"^(\\([0-9]{3})\\) [0-9]{3}-[0-9]{4}$";
    NSPredicate *phoneTest2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex2];
    BOOL isValid =[phoneTest2 evaluateWithObject:number];
    
    if(![VALID validate:self.phoneNumberTextField
              condition:isValid
                  title:NSLocalizedString(@"Please Enter a Valid Phone Number", nil)
                message:NSLocalizedString(@"A valid phone number is required to complete this order - (XXX) XXX-XXXX", nil)]){
        return;
    }
    
    if(![VALID validate:self.address1TextField
                  title:NSLocalizedString(@"Please Enter Street Address1", nil)
                message:NSLocalizedString(@"Address1 is Required to Complete This Order", nil)]){
        return;
    }
    
    if(![VALID validate:self.zipCodeTextField
                  title:NSLocalizedString(@"Please Enter Zip code.", nil)
                message:NSLocalizedString(@"Zip code is Required to Complete This Order.", nil)]){
        return;
    }
    
    if(![VALID validate:self.zipCodeTextField
              condition:self.zipCodeTextField.text.length == 5
                  title:NSLocalizedString(@"Please Enter a Valid Zip Code.", nil)
                message:nil]){
        return;
    }
    
    if(![VALID validate:self.cityTextField
                  title:NSLocalizedString(@"Please Enter City", nil)
                message:NSLocalizedString(@"City is Required to Complete This Order.", nil)]){
        return;
    }
    
    if(![VALID validate:self.stateTextField
                  title:NSLocalizedString(@"Please Select State", nil)
                message:NSLocalizedString(@"State is Required to Complete This Order.", nil)]){
        return;
    }
    
    if(CRED.isSignedIn){
        [self checkDeliveryAvailableAndOrder];
    } else {
        
        if(![VALID validate:self.emailTextField
                      title:NSLocalizedString(@"Email Is Required", nil)
                    message:nil]){
            return;
        }
        
        if(![VALID validateEmail:self.emailTextField
                           title:NSLocalizedString(@"Invalid Email", nil)
                         message:NSLocalizedString(@"Please enter a valid email address.", nil)]){
            return;
        }
        
        [NSUserDefaults standardUserDefaults].emailAddress = self.emailTextField.text;
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if([self.passwordTextField.text length] > 0){
            // Sign Up Call if Guest
            // After Signing up - proceed order again.
            [NSUserDefaults standardUserDefaults].lastTriedUserId = self.emailTextField.text;
            
            NSArray *nameArray = [self.nameTextField.text componentsSeparatedByString:@" "];
            
            RequestResult result = RRFail;
            result = [CRED requestSignUp:self.emailTextField.text
                                password:self.passwordTextField.text
                               firstName:[nameArray firstName]
                                lastName:[nameArray lastName]
                             phoneNumber:self.phoneNumberTextField.text
                             deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                         completionBlock:
                      ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                          
                          if(isSuccess && 200 <= statusCode && statusCode < 300){
                              switch(response.errorCode){
                                  case ResponseSuccess:{
                                      
                                      CRED.unattendedSignUp = YES;
                                      
                                      NSArray *customers = [CustomerInfo listAll];
                                      CustomerInfo *customerInfo = nil;
                                      if([customers count] == 1){
                                          customerInfo = [customers objectAtIndex:0];
                                      } else{
                                          PCWarning(@"Customers is not just only 1- sign up");
                                          // Clear
                                          for(CustomerInfo *customer in customers){
                                              if([customer delete]){
                                                  // Success
                                              } else {
                                                  PCError(@"Error to remove customerInfo - sign up");
                                              }
                                          }
                                      }
                                  
                                      if(customerInfo == nil){
                                          customerInfo = [[CustomerInfo alloc] init];
                                      }
                                      
                                      NSDictionary *customerDict = [response objectForKey:@"customer"];
                                      [customerInfo updateWithDictionary:customerDict];
                                      customerInfo.authToken = [response objectForKey:@"auth_token"];
                                      
                                      if([customerInfo save]){
                                          CRED.customerInfo = customerInfo;
                                          
                                          [SWITCH removeLocalDataIncludeCard:NO];
                                          
                                          [[NSNotificationCenter defaultCenter] postNotificationName:SignedInNotification
                                                                                              object:self
                                                                                            userInfo:nil];
                                          [self checkDeliveryAvailableAndOrder];
                                      } else {
                                          [UIAlertView alertWithTitle:NSLocalizedString(@"Can Not Save Account Information", nil)
                                                              message:NSLocalizedString(@"Try to sign in again. If the problem contiue, Please contact us.\nhelp@rushorderapp.com.", nil)];
                                      }
                                  }
                                      break;
                                  default:{
                                      NSString *message = [response objectForKey:@"message"];
                                      NSString *title = [response objectForKey:@"title"];
                                      if([message isKindOfClass:[NSString class]]){
                                          if(title != nil){
                                              [UIAlertView alertWithTitle:title
                                                                  message:message];
                                          } else {
                                              [UIAlertView alert:message];
                                          }
                                      } else {
                                          [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while signing up.", nil)];
                                      }
                                      
                                      CRED.customerInfo = nil;
                                  }
                                      break;
                              }
                          } else {
                              if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                                  [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                                      object:nil];
                              } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                                  [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d.", nil)
                                                      ,statusCode]];
                              }
                          }
                          [SVProgressHUD dismiss];
                      }];
            switch(result){
                case RRSuccess:
                    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                    break;
                case RRParameterError:
                    [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                    break;
                default:
                    break;
            }
            
        } else {
            [self checkDeliveryAvailableAndOrder];
        }
    }
}

- (void)checkDeliveryAvailableAndOrder
{
    [self.view endEditing:YES];
    
    NSDictionary *locationDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                        self.cityTextField.text, kABPersonAddressCityKey,
                                        @"United States", kABPersonAddressCountryKey,
                                        @"us", kABPersonAddressCountryCodeKey,
                                        self.stateTextField.text, kABPersonAddressStateKey,
                                        self.address1TextField.text, kABPersonAddressStreetKey,
                                        self.zipCodeTextField.text, kABPersonAddressZIPKey,
                                        nil];
    

    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    [self.geoCoder geocodeAddressDictionary:locationDictionary
                          completionHandler:^(NSArray *placemarks, NSError *error){
                              [SVProgressHUD dismiss];
                              if (error){
                                  PCError(@"Geocode failed with error: %@", error);
                                  [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot Locate Address", nil)
                                                      message:NSLocalizedString(@"Your address might be not valid or some other problems occured.", nil)];
                                  return;
                              }
                              
//                              PCLog(@"Received placemarks: %@", placemarks);
                              
                              BOOL hasValidAddress = NO;
                              
                              for(CLPlacemark *placemark in placemarks){
                                  
                                  if([placemark.addressDictionary objectForKey:@"Street"] == nil){
                                      continue;
                                  }
                                  
                                  self.geocodedCoords = placemark.location.coordinate;
                                  
                                  PCCurrency feeAmount = [ORDER.merchant deliveryFeeAt:placemark.location andSubtotal:self.payment.subTotal];
                                  
                                  if(feeAmount != NSNotFound){
                                      if(self.payment.total >= ORDER.merchant.deliveryBigOrderAmount && ORDER.merchant.deliveryBigOrderAmount > 0){
                                          self.payment.deliveryFeeAmount = ORDER.merchant.deliveryFeeAmountForBigOrder;
                                      } else {
                                          self.payment.deliveryFeeAmount = feeAmount;
                                      }
                                      
                                      if(self.delegate == nil){
                                          
                                          [self stepForPayment];
                                          
                                      } else {
                                          if([self.delegate respondsToSelector:@selector(deliveryViewController:didTouchPlaceOrderButton:addressDictionary:)]){
                                              NSMutableDictionary *rtnDictionary = [NSMutableDictionary dictionaryWithDictionary:locationDictionary];
                                              [rtnDictionary setObject:[NSNumber numberWithCurrency:feeAmount] forKey:@"deliveryFeeAmount"];
                                              [rtnDictionary setObject:placemark.location forKey:@"deliveryLocation"];
                                              
                                              [self.delegate deliveryViewController:self
                                                           didTouchPlaceOrderButton:nil
                                                                  addressDictionary:rtnDictionary
                                               ];
                                          }
                                      }
                                      hasValidAddress = YES;
                                  } else {
                                      
                                  }
                              }
                              
                              if(!hasValidAddress){
                                  self.payment.deliveryFeeAmount = 0;
                                  PCLog(@"Out of range for delivery");
                                  switch(ORDER.merchant.deliveryFeePlan){
                                      case DeliveryFeePlanFlat:
                                      case DeliveryFeePlanRadius:
                                          [UIAlertView alertWithTitle:NSLocalizedString(@"Delivery Distance Limit", nil)
                                                              message:NSLocalizedString(@"Sorry, we do not deliver to this location.", nil)];
                                          break;
                                      case DeliveryFeePlanZone:
                                          [UIAlertView alertWithTitle:NSLocalizedString(@"Outside of Delivery Area", nil)
                                                              message:NSLocalizedString(@"Sorry, we do not deliver to this location.", nil)];
                                          break;
                                      default:
                                          [UIAlertView alertWithTitle:NSLocalizedString(@"Delivery Area Error", nil)
                                                              message:NSLocalizedString(@"Sorry, we cannot decide this location is in the delivery area.", nil)];
                                          break;
                                  }
                              }
                          }];
}

- (void)stepForPayment
{
    if(CRED.isSignedIn){
        self.mobileCardList = REMOTE.cards;
        if(!REMOTE.isCardFetched){
            self.cardListWaiting = YES;
            return;
        }
    }
    
    
    UIViewController *viewController = nil;
    
//    ORDER.tempDeliveryAddress = [[Addresses alloc] init];
    ORDER.tempDeliveryAddress.address1 = self.address1TextField.text;
    ORDER.tempDeliveryAddress.address2 = self.address2TextField.text;
    ORDER.tempDeliveryAddress.zip = self.zipCodeTextField.text;
    ORDER.tempDeliveryAddress.city = self.cityTextField.text;
    ORDER.tempDeliveryAddress.state = self.stateTextField.text;
    ORDER.tempDeliveryAddress.receiverName = self.nameTextField.text;
    ORDER.tempDeliveryAddress.phoneNumber = self.phoneNumberTextField.text;
    ORDER.tempDeliveryAddress.coordinate = self.geocodedCoords;
    ORDER.tempDeliveryAddress.needToBeUpdatedToDefault = self.defaultAddressButton.selected;
    if(ORDER.tempDeliveryAddressFilled < 3) ORDER.tempDeliveryAddressFilled = 3;
    
    ORDER.tempAfterMinute = self.afterMinute;
    ORDER.tempCustomerRequest = self.customerRequestTextView.text;
    
#if FLURRY_ENABLED
    [Flurry logEvent:@"Try to Pay(D)" withParameters:@{@"RestaurantName":ORDER.merchant.name,
                                                  @"RestaurantId":[NSNumber numberWithSerial:ORDER.merchant.merchantNo],
                                                  @"orderTotal":[NSNumber numberWithCurrency:ORDER.order.subTotal]}];
#endif
    
    
    if([self.mobileCardList count] > 0 || CRED.customerInfo.credit > 0){
        
        viewController = [PaymentMethodViewController viewControllerFromNib];
        ((PaymentMethodViewController *)viewController).payment = self.payment;
        ((PaymentMethodViewController *)viewController).paymentMethodList = self.mobileCardList;
        ((PaymentMethodViewController *)viewController).address = ORDER.tempDeliveryAddress;
        ((PaymentMethodViewController *)viewController).requestMessage = self.additionalRequest.text;
        ((PaymentMethodViewController *)viewController).afterMinute = self.afterMinute;
        
    } else {
        viewController = [NewCardViewController viewControllerFromNib];
        ((NewCardViewController *)viewController).unintendedPresented = YES;
        ((NewCardViewController *)viewController).payment = self.payment;
        ((NewCardViewController *)viewController).address = ORDER.tempDeliveryAddress;
        ((NewCardViewController *)viewController).requestMessage = self.additionalRequest.text;
        ((NewCardViewController *)viewController).afterMinute = self.afterMinute;
    }
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIPickerViewDatasource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch(pickerView.tag){
        case PickerViewTagTime:
            return [self.timeArray count];
            break;
        case PickerViewTagState:
            return [self.stateList count];
            break;
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch(pickerView.tag){
        case PickerViewTagTime:
        {
            NSDictionary *dict = [self.timeArray objectAtIndex:row];
            return [dict objectForKey:@"title"];
        }
            break;
        case PickerViewTagState:
            return [self.stateList objectAtIndex:row];
            break;
    }
    return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    switch(pickerView.tag){
        case PickerViewTagTime:
            [self setSelectedValue:row];
            break;
        case PickerViewTagState:
            return [self selectStateAtIndex:row];
            break;
    }
}

- (void)setSelectedValue:(NSInteger)index
{
    NSDictionary *dict = [self.timeArray objectAtIndex:index];
    
    NSString *specificTime = [dict objectForKey:@"specificTime"];
    self.pickupTimeTextField.text = specificTime;
    
    self.afterMinute = [[dict objectForKey:@"afterMinute"] integerValue];
}

- (void)selectStateAtIndex:(NSInteger)index
{
    self.stateTextField.text = [self.stateList objectAtIndex:index];
}

- (IBAction)timeInputButtonTouched:(id)sender
{
    if(self.pickupTimeTextField.isFirstResponder){
        [self inputDoneButtonTouched:sender];
    } else {
        [self.pickupTimeTextField becomeFirstResponder];
    }
}

- (IBAction)inputDoneButtonTouched:(id)sender
{
    NSInteger selectedRow = [self.timePickerView selectedRowInComponent:0];
    switch(self.timePickerView.tag){
        case PickerViewTagTime:
        {
            [self setSelectedValue:selectedRow];
            [self.pickupTimeTextField resignFirstResponder];
        }
            break;
        case PickerViewTagState:
        {
            [self selectStateAtIndex:selectedRow];
            [self.stateTextField resignFirstResponder];
        }
            break;
    }
}

- (NSMutableArray *)timeArray
{
    if(_timeArray == nil){
        _timeArray = [NSMutableArray array];
        
        NSInteger count = LIMIT_HOUR * (60 / MINUTE_SPAN);
        
        for(NSInteger i = 0 ; i < count ; i++){
            
            if(i < 5) continue;
            
            NSDate *now = [NSDate date];
            NSInteger afterMinute = (MINUTE_SPAN * (i + 1));
            NSTimeInterval afterInterval = afterMinute * 60;
            NSDate *afterNow = [now dateByAddingTimeInterval:afterInterval];
            NSMutableString *afterMinuteString = [NSMutableString stringWithString:[NSString stringWithInteger:((i + 1) * MINUTE_SPAN)]];
            [afterMinuteString appendString:@"Min. ("];
            [afterMinuteString appendString:afterNow.hourMinuteString];
            [afterMinuteString appendString:@")"];
            
            NSDictionary *timeDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                      afterMinuteString, @"title",
                                      [NSNumber numberWithInteger:afterMinute], @"afterMinute",
                                      afterNow.hourMinuteString, @"specificTime",
                                      nil];
                                      
            [_timeArray addObject:timeDict];
        }
    }
    return _timeArray;
}

- (IBAction)asapButtonTouched:(NPToggleButton *)sender
{
    if(!sender.selected){
        sender.selected = YES;
        self.pickupTimeButton.selected = !sender.selected;
        self.afterMinute = 0;
        self.pickupTimeTextField.text = @"00:00";
        [self.timePickerView selectRow:0 inComponent:0 animated:NO];
        [self.pickupTimeTextField resignFirstResponder];
    }
    [self setPickupTimeSubviews];
}

- (IBAction)specificButtonTouched:(NPToggleButton *)sender
{
    if(!self.pickupTimeButton.selected){
        self.pickupTimeButton.selected = YES;
        self.asapButton.selected = !self.pickupTimeButton.selected;
    }
    [self setPickupTimeSubviews];
    if(self.pickupTimeButton.selected && (sender != nil)){
        [self.pickupTimeTextField becomeFirstResponder];
    }
}

- (void)setPickupTimeSubviews
{
    self.pickupTimeSelectButton.enabled = self.pickupTimeButton.selected;
    self.pickupTimeTextField.enabled =  self.pickupTimeButton.selected;
    if(!self.pickupTimeTextField.enabled){
        self.pickupTimeTextField.text = nil;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == self.stateTextField || textField == self.pickupTimeTextField){
        if(self.timePickerView.tag != textField.tag){
            self.timePickerView.tag = textField.tag;
            switch(self.timePickerView.tag){
                case PickerViewTagTime:
                    self.PickerTitleBarButtonItem.title = NSLocalizedString(@"Select Time", nil);
                    break;
                case PickerViewTagState:
                    self.PickerTitleBarButtonItem.title = NSLocalizedString(@"Select State", nil);
                    break;
            }
            [self.timePickerView reloadAllComponents];
        }
    }
    return YES;
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
//    PCLog(@"Updated Text is %@", updatedText);
//    return YES;
//}

//- (BOOL)textFieldShouldClear:(UITextField *)textField
//{
//    PCLog(@"**********Cleard");
//    return YES;
//}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == self.stateTextField ||
       textField == self.cityTextField ||
       textField == self.address1TextField ||
       textField == self.address2TextField ||
       textField == self.nameTextField ||
       textField == self.phoneNumberTextField ||
       textField == self.zipCodeTextField){
        
        DeliveryAddress *similarAddress = nil;
        DeliveryAddress *sameAddress = nil;
        for(DeliveryAddress *anAddress in REMOTE.deliveryAddresses){
            
            DeliveryAddressSimilarity rtn = [anAddress sameWithStreet:self.address1TextField.text
                                                              street2:self.address2TextField.text
                                                                 city:self.cityTextField.text
                                                                state:self.stateTextField.text
                                                                  zip:self.zipCodeTextField.text
                                                          phoneNumber:self.phoneNumberTextField.text
                                                                 name:self.nameTextField.text];
            switch(rtn){
                case DeliveryAddressSimilaritySimilar:
                    if(similarAddress == nil || !similarAddress.isDefaultAddress){
                        similarAddress = anAddress;
                    }
                    break;
                case DeliveryAddressSimilaritySame:
                    sameAddress = anAddress;
                    break;
                default:
                    break;
            }
            
            if(sameAddress != nil){
                break;
            }
        }
        
        if(sameAddress != nil){
            if(ORDER.lastUsedDeliveryAddress == nil || ORDER.lastUsedDeliveryAddress.no != sameAddress.no){
                ORDER.lastUsedDeliveryAddress = sameAddress;
                self.defaultAddressButton.selected = sameAddress.isDefaultAddress;
                [self showDefaultAddressButton:!sameAddress.isDefaultAddress];
            }
        } else if(similarAddress != nil){
            if(ORDER.lastUsedDeliveryAddress == nil || ORDER.lastUsedDeliveryAddress.no != similarAddress.no){
                ORDER.lastUsedDeliveryAddress = similarAddress;
                self.defaultAddressButton.selected = similarAddress.isDefaultAddress;
                [self showDefaultAddressButton:!similarAddress.isDefaultAddress];
            }
        } else {
            if(ORDER.lastUsedDeliveryAddress != nil){
                ORDER.lastUsedDeliveryAddress = nil;
                self.defaultAddressButton.selected = NO;
                [self showDefaultAddressButton:YES];
            }
        }
    }
}

- (IBAction)recentAddressButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    if(REMOTE.isDeliveryAddressFetched){
        if([REMOTE.deliveryAddresses count] > 0){
            UIWindow *window = self.view.window;
            self.recentContainerView.frame = window.frame;
            
            [self.tableView reloadData];
            self.recentAddressView.frame = CGRectMake(((self.recentContainerView.width - self.recentAddressView.width) / 2),
                                                      700.0f,
                                                      self.recentAddressView.width,
                                                      self.recentAddressView.height);
            self.recentContainerView.alpha = 0.0f;
            
            [self.view.window addSubview:self.recentContainerView];
            
            [UIView animateWithDuration:0.3f
                             animations:^{
                                 self.recentContainerView.alpha = 1.0f;
                                 CGRect frame = self.recentAddressView.frame;
                                 frame.origin.y = (self.recentContainerView.height - frame.size.height) / 2;
                                 self.recentAddressView.frame = frame;
                                 
                             } completion:^(BOOL finished) {
                                 
                             }];
        } else {
            [UIAlertView alertWithTitle:@"No Saved Addresses"
                                message:@"Sorry, you currently do not have any saved delivery addresses."];
        }
    } else {
        if(!self.isDeliveryAddressWaiting){
            self.deliveryAddressWaiting = YES;
            [REMOTE requestDeliveryAddresses];
        }
        self.showDeliveryAddressListAfterFetching = YES;
    }
}


- (IBAction)recentRequestButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    self.recentRequestViewController = [RecentListViewController viewControllerFromNib];
    self.recentRequestViewController.delegate = self;
    
    UIWindow *window = self.view.window;
    self.recentRequestViewController.view.frame = window.frame;
    
    [self.recentRequestViewController.tableView reloadData];
    self.recentRequestViewController.panelView.frame = CGRectMake(((self.recentRequestViewController.view.width - self.recentRequestViewController.panelView.width) / 2),
                                              700.0f,
                                              self.recentRequestViewController.panelView.width,
                                              self.recentRequestViewController.panelView.height);
    self.recentRequestViewController.view.alpha = 0.0f;
    [self.view.window addSubview:self.recentRequestViewController.view];
    
    [UIView animateWithDuration:0.3f
                     animations:^{
                         self.recentRequestViewController.view.alpha = 1.0f;
                         CGRect frame = self.recentRequestViewController.panelView.frame;
                         frame.origin.y = (self.recentRequestViewController.view.height - frame.size.height) / 2;
                         self.recentRequestViewController.panelView.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                     }];

}

- (IBAction)defaultAddressButtonTouched:(NPToggleButton *)sender
{
//    if(ORDER.lastUsedDeliveryAddress.isDefaultAddress){
//        [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot make this address as not default option", nil)
//                            message:NSLocalizedString(@"You can change default address only by making other address as default option", nil)];
//    } else {
        sender.selected = !sender.isSelected;
//    }
}

- (void)recentListViewController:(RecentListViewController *)viewController
                didSelectRequest:(CustomRequest *)selectedRequest
{
    self.customerRequestTextView.text = selectedRequest.request;
    [self textViewDidChange:self.customerRequestTextView];
//    [self.customerRequestTextView becomeFirstResponder];
}

- (IBAction)locationButtonTouched:(id)sender
{
    LocationViewController *viewController = [UIStoryboard viewController:@"LocationViewController"
                                                                     from:@"Restaurants"];
    AddressAnnotation *annotation = [[AddressAnnotation alloc] init];
    if(self.geocodedCoords.latitude == 0.0 && self.geocodedCoords.longitude == 0.0){
        annotation.coordinate = APP.location.coordinate;
//        annotation.title = self.address1TextField.text;
//        annotation.subtitle = self.address1TextField.text;
    } else {
        annotation.coordinate = self.geocodedCoords;
        annotation.title = self.address1TextField.text;
        annotation.subtitle = self.address1TextField.text;
    }
    viewController.selectedAddress = annotation;
    viewController.delegate = self;
    
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^{
                                     
                                 }];
}

- (void)locationViewController:(LocationViewController *)viewController
            didApplyCoordinate:(CLLocationCoordinate2D)location
                          name:(NSString *)name
            placemarkOrAddress:(id)obj
{
    if([obj isKindOfClass:[Addresses class]]){
        Addresses *address = (Addresses *)obj;
        [self fillAddresses:address];
        
    } else if([obj isKindOfClass:[CLPlacemark class]]){
        CLPlacemark *place = (CLPlacemark *)obj;
        
        [self fillAddressesWithPlacemark:place];
        if([self.address1TextField.text length] == 0){
            [self.address1TextField becomeFirstResponder];
        } else if([self.address2TextField.text length] == 0){
            [self.address2TextField becomeFirstResponder];
        }
    } else if([obj isKindOfClass:[GMPrediction class]]){
        GMPrediction *prediction = (GMPrediction *)obj;
        [self fillAddressesWithPrediction:prediction];
        
        if([self.address1TextField.text length] == 0){
            [self.address1TextField becomeFirstResponder];
        } else if([self.address2TextField.text length] == 0){
            [self.address2TextField becomeFirstResponder];
        }
    }
}

- (void)didCancelLocationViewController:(LocationViewController *)viewController
{
    
}

#pragma mark - TableViewDatasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return MIN(5, [self.addresses count]);
    return REMOTE.deliveryAddresses.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"RecentAddressCell";
    
    RecentAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil){
        cell = [RecentAddressCell cell];
    }
    
    DeliveryAddress *anAddress = [REMOTE.deliveryAddresses objectAtIndex:indexPath.row];
    
    cell.deliveryAddress = anAddress;
    [cell fillContent];
    
//    cell.addreses = [self.addresses objectAtIndex:indexPath.row];
//    [cell fillContent];
    
    if(self.selectedAddressIndex != NSNotFound){
        if(self.selectedAddressIndex == indexPath.row){
            cell.checkImageView.hidden = NO;
        } else {
            cell.checkImageView.hidden = YES;
        }
    } else {
        cell.checkImageView.hidden = YES;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedAddressIndex = indexPath.row;
    
//    Addresses *address = [self.addresses objectAtIndex:self.selectedAddressIndex];
    DeliveryAddress *anAddress = [REMOTE.deliveryAddresses objectAtIndex:self.selectedAddressIndex];
    
    [self fillDeliveryAddress:anAddress];
    
    [self.tableView reloadData];
    
//    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    [self performSelector:@selector(closeAfterDelay)
               withObject:nil
               afterDelay:0.5f];
}

- (void)fillAddresses:(Addresses *)addresses
{
    ORDER.lastUsedDeliveryAddress = nil;
    
    self.defaultAddressButton.selected = NO;
//    self.defaultAddressButton.enabled = YES;
    [self showDefaultAddressButton:YES];
    
    self.address1TextField.text = addresses.address1;
    self.address2TextField.text = addresses.address2;
    self.zipCodeTextField.text = addresses.zip;
    self.cityTextField.text = addresses.city;
    self.stateTextField.text = addresses.state;
    self.geocodedCoords = addresses.coordinate;
}

/*
 Should it be changed to default?
 Default address should be filled at first?
 
 */

- (void)fillDeliveryAddress:(DeliveryAddress *)deliveryAddress
{
    ORDER.lastUsedDeliveryAddress = deliveryAddress;
    self.nameTextField.text = deliveryAddress.name;
    self.phoneNumberTextField.text = deliveryAddress.phoneNumber;
    self.address1TextField.text = deliveryAddress.street;
    self.address2TextField.text = deliveryAddress.street2;
    self.zipCodeTextField.text = deliveryAddress.zip;
    self.cityTextField.text = deliveryAddress.city;
    self.stateTextField.text = deliveryAddress.state;
    self.geocodedCoords = deliveryAddress.coordinate;
    
    self.defaultAddressButton.selected = deliveryAddress.isDefaultAddress;
    if(deliveryAddress.isDefaultAddress){
        self.defaultAddressButton.selected = YES;
        [self showDefaultAddressButton:NO];
//        self.defaultAddressButton.enabled = NO;
    } else {
        self.defaultAddressButton.selected = NO;
        [self showDefaultAddressButton:YES];
//        self.defaultAddressButton.enabled = YES;
    }
    
    
    if([deliveryAddress.deliveryInstruction length] > 0){
        NSString *exText = self.customerRequestTextView.text;
        if([exText length] > 0){
            if(![exText containsString:deliveryAddress.deliveryInstruction]){
                self.customerRequestTextView.text = [[exText stringByAppendingString:@", "]
                                                     stringByAppendingString:deliveryAddress.deliveryInstruction];
            }
        } else {
            self.customerRequestTextView.text = deliveryAddress.deliveryInstruction;
        }
        [self textViewDidChange:self.customerRequestTextView];
    }
}

- (void)fillAddressesWithPlacemark:(CLPlacemark *)placemark
{
    ORDER.lastUsedDeliveryAddress = nil;
    
    self.defaultAddressButton.selected = NO;
//    self.defaultAddressButton.enabled = YES;
    [self showDefaultAddressButton:YES];
    
    self.address1TextField.text = [placemark.addressDictionary objectForKey:@"Street"];
    self.address2TextField.text = @"";
    self.zipCodeTextField.text = [placemark.addressDictionary objectForKey:@"ZIP"];
    self.cityTextField.text = [placemark.addressDictionary objectForKey:@"City"];
    self.stateTextField.text = [placemark.addressDictionary objectForKey:@"State"];
    self.geocodedCoords = placemark.location.coordinate;
}

- (void)fillAddressesWithPrediction:(GMPrediction *)prediction
{
    ORDER.lastUsedDeliveryAddress = nil;
    
    self.defaultAddressButton.selected = NO;
//    self.defaultAddressButton.enabled = YES;
    [self showDefaultAddressButton:YES];
    
    self.address1TextField.text = prediction.street;
    self.address2TextField.text = @"";
    self.zipCodeTextField.text = prediction.zip;
    self.cityTextField.text = prediction.city;
    self.stateTextField.text = prediction.state;
    self.geocodedCoords = prediction.coordinate;
}

//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return UITableViewCellEditingStyleDelete;
//}
//
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if(editingStyle == UITableViewCellEditingStyleDelete){
//        self.selectedAddressIndex = NSNotFound;
//        Addresses *address = [self.addresses objectAtIndex:indexPath.row];
//        if([address delete]){
//            [self.addresses removeObjectAtIndex:indexPath.row];
//            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
//                                  withRowAnimation:UITableViewRowAnimationLeft];
//        }
//    }
//}

- (void)closeAfterDelay
{
    [self recentAddressCloseButtonTouched:nil];
}

- (NSMutableArray *)addresses
{
    if(_addresses == nil){
        NSMutableArray *addresses = [Addresses listAll];
        if([addresses count] > 5){
            [addresses removeObjectsInRange:NSMakeRange(5, [addresses count] - 5)];
        }
        _addresses = addresses;
    }
    return _addresses;
}

- (IBAction)recentAddressCloseButtonTouched:(id)sender
{
    [UIView animateWithDuration:0.3f
                     animations:^{
                         self.recentContainerView.alpha = 0.0f;
                         CGRect frame = self.recentAddressView.frame;
                         frame.origin.y = 700.0f;
                         self.recentAddressView.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         if(finished){
                             [self.recentContainerView removeFromSuperview];
                             self.selectedAddressIndex = NSNotFound;
                         }
                     }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isKindOfClass:[PCTextField class]]){
        [textField resignFirstResponder];
        [[(PCTextField *)textField nextField] becomeFirstResponder];
    }
    return YES;
}

- (IBAction)nextButtonTouched:(id)sender
{
    [self inputDoneButtonTouched:sender];
    switch(self.timePickerView.tag){
        case PickerViewTagTime:
        {
            [self.nameTextField becomeFirstResponder];
        }
            break;
        case PickerViewTagState:
        {
            
            [self.additionalRequest becomeFirstResponder];
        }
            break;
    }
}

- (IBAction)phoneNumberValueChanged:(UITextField *)sender
{
    if(_textFieldSemaphore) return;
    
    _textFieldSemaphore = 1;
    
    NSString *locale = [[NSLocale currentLocale] localeIdentifier];
    sender.text = [_phoneNumberFormatter format:sender.text
                                     withLocale:locale];
    _textFieldSemaphore = 0;
}

- (CLGeocoder *)geoCoder
{
    if(_geoCoder == nil){
        _geoCoder = [[CLGeocoder alloc] init];
    }
    return _geoCoder;
}

- (NSMutableArray *)mobileCardList
{
    if(_mobileCardList == nil){
        if(CRED.isSignedIn){
            _mobileCardList = REMOTE.cards; // TODO:If card does not exist? -> Blocking?
        } else {
            _mobileCardList = [MobileCard listAll];
        }
    }
    return _mobileCardList;
}

#pragma mark - TextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    if([textView.text length] > 0){
        self.placeHolderLabel.hidden = YES;
        self.suggestingCustomRequests = [CustomRequest suggestingListWithKeyword:textView.text
                                                                            type:CustomRequestTypeDelivery
                                                                      merchantNo:ORDER.merchant.merchantNo];
        
        if([self.suggestingCustomRequests count] > 0){
            CustomRequest *request = [self.suggestingCustomRequests objectAtIndex:0];
            self.suggestButton.buttonTitle = [request.request stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
        } else {
            self.suggestButton.buttonTitle = nil;
        }
    } else {
        self.placeHolderLabel.hidden = NO;
        self.suggestButton.buttonTitle = nil;
    }
}

- (void)showDefaultAddressButton:(BOOL)show
{
    [self showDefaultAddressButton:show
                     withAnimation:YES];
}

- (void)showDefaultAddressButton:(BOOL)show withAnimation:(BOOL)animation
{
    if(self.defaultAddressButton.isHidden != show){
        return;
    }
    if(show){
        self.customerRequestTopConstraint.priority = 999;
        self.customerRequestTopConstraint2.priority = 500;
        self.saveButtonTopConstraint.constant = 174.0f;
    } else {
        self.customerRequestTopConstraint2.priority = 999;
        self.customerRequestTopConstraint.priority = 500;
        self.saveButtonTopConstraint.constant = 142.0f;
    }
    
    if(show){
        self.defaultAddressButton.hidden = NO;
    }
    if(animation && self.isAppeared){
        self.defaultAddressButton.alpha = show ? 0.0 : 1.0;
        [UIView animateWithDuration:.3 animations:^{
            self.defaultAddressButton.alpha = show ? 1.0 : 0.0;
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            if(!show){
                self.defaultAddressButton.hidden = YES;
            }
        }];
    } else {
        if(!show){
            self.defaultAddressButton.hidden = YES;
        }
    }
}

- (IBAction)suggestButtonTouched:(id)sender
{
    if(self.suggestButton.buttonTitle.length > 0){
        CustomRequest *request = [self.suggestingCustomRequests objectAtIndex:0];
        self.customerRequestTextView.text = request.request;
        self.placeHolderLabel.hidden = YES;
        [self.customerRequestTextView resignFirstResponder];
    } else {
        // Do nothing
    }
}

- (IBAction)switchToTakeoutButtonTouched:(id)sender
{
    if([MENUPAN resetGraphWithMerchant:ORDER.merchant
                              withType:MenuListTypeTakeout]){
        ORDER.cart.cartType = CartTypeTakeout;
        ORDER.order.orderType = OrderTypeTakeout;
        
        [MENUPAN requestMenus:YES
                      success:^(id response) {
                          [ORDER.cart isAllItemsInHour:YES];
                          [[NSNotificationCenter defaultCenter] postNotificationName:OrderTypeSwitchedNotification
                                                                              object:self];
                          [self backButtonTouched:nil];
                      } andFail:^(NSUInteger errorCode) {
                          [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to renew Menus",nil)
                                              message:NSLocalizedString(@"Sorry, please try again and if it continues, please contact us.",nil)];
                          [self.navigationController popToRootViewControllerAnimated:YES];
                      }];
        
    } else {
    }
}

- (IBAction)switchToDineinButtonTouched:(id)sender
{
    if([MENUPAN resetGraphWithMerchant:ORDER.merchant
                              withType:MenuListTypeDinein]){
        ORDER.order.orderType = OrderTypePickup;
        if(ORDER.merchant.isServicedByStaff){
             ORDER.cart.cartType = CartTypePickupServiced;
        } else {
             ORDER.cart.cartType = CartTypePickup;
        }
        
        [MENUPAN requestMenus:YES
                      success:^(id response) {
                          [ORDER.cart isAllItemsInHour:YES];
                          [[NSNotificationCenter defaultCenter] postNotificationName:OrderTypeSwitchedNotification
                                                                              object:self];
                          [self backButtonTouched:nil];
                      } andFail:^(NSUInteger errorCode) {
                          [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to renew Menus",nil)
                                              message:NSLocalizedString(@"Sorry, please try again and if it continues, please contact us.",nil)];
                          [self.navigationController popToRootViewControllerAnimated:YES];
                      }];
    } else {
    }
}

- (IBAction)signInButtonTouched:(id)sender
{
    [self showSignIn];
}

- (void)showSignIn
{
    return [self showSignIn:NO];
}

- (void)showSignIn:(BOOL)showGuide
{
    SignInCustomerViewController *viewController = [SignInCustomerViewController viewControllerFromNib];
    viewController.showGuide = showGuide;
    [self.tabBarController presentViewControllerInNavigation:viewController
                                                    animated:YES
                                                  completion:^{
                                                      
                                                  }];
}


@end
