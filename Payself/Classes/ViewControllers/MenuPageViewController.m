//
//  MenuPageViewController.m
//  RushOrder
//
//  Created by Conan on 5/14/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "MenuPageViewController.h"
#import "PCPaymentService.h"
#import "IIViewDeckController.h"
#import "TablesetViewController.h"
#import "BillViewController.h"
#import "MenuCategory.h"
#import "MenuSubCategory.h"
#import "MenuOptionGroup.h"
#import "MenuOption.h"
#import "MenuItem.h"
#import "OrderManager.h"
#import "MenuManager.h"
#import "PCMenuService.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PCCredentialService.h"
#import "NewCardViewController.h"
#import "RestaurantSummaryView.h"
#import "MenuPhotoPageViewController.h"
#import "RemoteDataManager.h"
#import "MenuPan.h"
#import "CustomIOSAlertView.h"
#import "ServiceFeeGuideView.h"
#import "OperationHour.h"
#import "TransactionManager.h"
#import "SingleScrollPageViewController.h"

#define MENU_REL_XIB_NAME   @"MenuItemCell"
#define SEL_BACK_IMAGEVIEW_MARGIN 5.0f


typedef enum menuItemType_{
    MenuItemTypeSubCategory = 1,
    MenuItemTypeItem
} MenuItemType;

@interface MenuPageViewController ()

@property (strong, nonatomic) IBOutlet SingleScrollPageViewController *pageViewController;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableNumberTopConstraint;
@property (strong, nonatomic) IBOutlet UIView *tableNumberView;
@property (strong, nonatomic) IBOutlet NPKeyboardAwareScrollView *tableNumberScrollView;
@property (nonatomic) NSInteger selectedQuantity;
@property (copy, nonatomic) NSString *specialInstructions;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (strong, nonatomic) NSIndexPath *firstMatchingIndexPath;
@property (strong, nonatomic) IBOutlet UIPickerView *tableNumberPickerView;
@property (strong, nonatomic) IBOutlet UIToolbar *tableNumberToolBar;
@property (weak, nonatomic) IBOutlet PCTextField *tableNumberTextField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *toggleBarButtonItem;
//@property (strong, nonatomic) NSMutableArray *mobileCardList;

@property (nonatomic, getter = isLockReloadBadge) BOOL lockReloadBadge;
@property (nonatomic) BOOL didAppeared;
@property (nonatomic) BOOL checkedNoMenuItem;
@property (strong, nonatomic) IBOutlet UIView *noItemView;
@property (weak, nonatomic) IBOutlet UILabel *noMenuInfoLabel;
@property (weak, nonatomic) IBOutlet NPStretchableButton *gotoRequestBillButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *takeoutTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *takeoutHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet NPStretchableButton *dineinButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonsSpanConstraint;
@property (weak, nonatomic) IBOutlet UIView *pageDotContainer;
@property (weak, nonatomic) IBOutlet UIImageView *leftFadeImageView;
@property (weak, nonatomic) IBOutlet UIImageView *rightFadeImageView;


@property (strong, nonatomic) MenuItemCell *offScreenMenuItemCell;
@property (strong, nonatomic) MenuSubCategoryCell *offScreenMenuSubCategoryCell;
@property (nonatomic) NSInteger selectedSectionIndex;

@property (nonatomic) NSUInteger selectedPageIndex;
@property (nonatomic) NSUInteger reservedPageIndex;
//@property (nonatomic, getter = isCardListWaiting) BOOL cardListWaiting;

@property (nonatomic) NSMutableArray *cellViewControllers;

@property (strong, nonatomic) MenuCellController *currentCellController;
@property (weak, nonatomic) IBOutlet UIScrollView *menupanNavigationScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertViewVerticalConstraint;
@property (weak, nonatomic) IBOutlet UIView *locationAlertView;

@property (weak, nonatomic) IBOutlet UIButton *prePageButton;
@property (weak, nonatomic) IBOutlet UIButton *nextPageButton;
@end

@implementation MenuPageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self != nil){
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cartUnsolicitChanged:)
                                                 name:CartUnsolicitChangedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orderListChanged:)
                                                 name:LineItemChangedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(menuListChanged:)
                                                 name:MenuListChangedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tableListChanged:)
                                                 name:TableListChangedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didResignActive:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orderTypeSwitched:)
                                                 name:OrderTypeSwitchedNotification
                                               object:nil];
    self.selectedSectionIndex = -1;
    
    

}

- (void)orderTypeSwitched:(NSNotification *)notification
{
    self.initialCartType = ORDER.cart.cartType;
}

- (void)didResignActive:(NSNotification *)noti
{
    [self locationAlertCloseButtonTouched:nil];
}


//- (void)cardListChanged:(NSNotification *)notification
//{
//    if(CRED.isSignedIn){
//        self.mobileCardList = REMOTE.cards;
//        if(self.isCardListWaiting){
//            self.cardListWaiting = NO;
//            [self proceedPayment];
//        }
//    } else {
//        self.mobileCardList = [MobileCard listAll];
//    }
//}


- (void)cartUnsolicitChanged:(NSNotification *)notification
{
    if(self.tableInfo != nil){
        [ORDER checkTable:self.tableInfo
                  success:^(){
                      if(ORDER.shouldOpenRightSlide){                          
//                          [APP.viewDeckController openRightViewAnimated:YES
//                                                             completion:^(IIViewDeckController *controller, BOOL success) {
//                                                                 
//                                                             }];
                          // TODO: Show alert message
                      }
                  }
                  failure:^(OrderStatus status){
                      
                  }];
    }
}

- (void)orderListChanged:(NSNotification *)notification
{
    if(self.isLockReloadBadge){
        return;
    }
    
    [self reloadBadgeNumber:[NSNumber numberWithBool:NO]];
}

- (void)menuListChanged:(NSNotification *)notification
{
    self.gotoRequestBillButton.hidden = YES;
    
    if([MENUPAN.activeMenuList count] > 0){
        self.menupanNavigationScrollView.hidden = NO;
        [self.noItemView removeFromSuperview];
        self.leftFadeImageView.hidden = NO;
        self.rightFadeImageView.hidden = NO;
        self.prePageButton.hidden = NO;
        self.nextPageButton.hidden = NO;
        self.pageDotContainer.hidden = NO;
        [self reloadData];
        
    } else {
        [self showNoItemView];
        
        [self reloadData];
    }
}

- (void)showNoItemView
{
    self.menupanNavigationScrollView.hidden = YES;
    self.leftFadeImageView.hidden = YES;
    self.rightFadeImageView.hidden = YES;
    self.prePageButton.hidden = YES;
    self.nextPageButton.hidden = YES;
    self.pageDotContainer.hidden = YES;
    
    [self.view addSubview:self.noItemView];
    
    self.noItemView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[noItemView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"noItemView":self.noItemView}]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[noItemView]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:@{@"noItemView":self.noItemView}]];
    
    self.noMenuInfoLabel.text = NSLocalizedString(@"This restaurant may be configuring its menu so you cannot order right now.", nil);
}

#define DOT_SPACE 12.0f

- (void)reloadData
{
    self.cellViewControllers = nil;
    self.firstMatchingIndexPath = nil;
    CGRect pageViewFrame = self.view.bounds;
    
    BOOL found = NO;
    
    // This is for suggestion menus for showing, here, I'll save selectedPageIndex for first matching menus
    // And I'll give a chance to scroll to exact that item and showing detail view to MenuCellController
    if([self.referredMenus count] > 0){
        BOOL candiFound = NO;
        NSDictionary *refMenuDict = self.referredMenus[0];
        NSInteger panIndex = 0;
        NSInteger section = 0;
        NSInteger row = 0;
        
        NSInteger panIndexCandi = 0;
        NSInteger sectionCandi = 0;
        NSInteger rowCandi = 0;
        
        NSNumber *refMenuItemNo = [refMenuDict objectForKey:@"id"];
        NSString *refMenuName = [refMenuDict objectForKey:@"name"];
        
        for(MenuPan *pan in MENUPAN.activeMenuList){
            section = 0;
            for(MenuCategory *menuCategory in pan.menus){
                row = 0;
                for(id item in menuCategory.items){
                    if([item isKindOfClass:[MenuItem class]]){
                        MenuItem *menu = (MenuItem *)item;
                        if(menu.menuItemNo == [refMenuItemNo serialValue]){
                            found = YES;
                            break;
                        } else if ([refMenuName isEqualToString:menu.name]){
                            panIndexCandi = panIndex;
                            sectionCandi = section;
                            rowCandi = row;
                            candiFound = YES;
                        }
                    }
                    row++;
                }
                if(found){
                    break;
                }
                section++;
            }
            if(found){
                break;
            }
            panIndex++;
        }
        
        if(found){
            self.selectedPageIndex = panIndex + 1;
            self.firstMatchingIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
        } else if(candiFound){
            self.selectedPageIndex = panIndexCandi + 1;
            self.firstMatchingIndexPath = [NSIndexPath indexPathForRow:rowCandi inSection:sectionCandi];
        }
    }
    
    // If there is no referred menus or that menus could not be found in list => Let's just use selection according to Menu Hours.
    if(!found){
        NSUInteger initialIndex = -1;
        NSUInteger firstAlwaysMenuIndex = NSNotFound;
        NSMutableArray *allMenuHours = [NSMutableArray array];
        
        for(MenuPan *pan in MENUPAN.activeMenuList){
            initialIndex++;
            if(pan.isMenuHour){
                found = YES;
                break;
            }
            for(OperationHour *hour in pan.menuHours){
                if([pan.menuHours count] > 0 && !pan.isAlwaysOn){
                    NSDictionary *hourDict = @{@"panIndex":[NSNumber numberWithUnsignedInteger:initialIndex],
                                               @"hour":hour};
                    NSUInteger firstMatchingIndex = [allMenuHours indexOfObject:hourDict
                                                                  inSortedRange:NSMakeRange(0, [allMenuHours count])
                                                                        options:NSBinarySearchingInsertionIndex | NSBinarySearchingFirstEqual
                                                                usingComparator:^NSComparisonResult(NSDictionary *dict1, NSDictionary *dict2) {
                                                                    OperationHour *obj1 = [dict1 objectForKey:@"hour"];
                                                                    OperationHour *obj2 = [dict2 objectForKey:@"hour"];
                                                                    
                                                                    if(obj1.eventTime > obj2.eventTime){
                                                                        return NSOrderedDescending;
                                                                    } else if(obj1.eventTime < obj2.eventTime){
                                                                        return NSOrderedAscending;
                                                                    } else {
                                                                        if(obj1 == hour) return NSOrderedDescending;
                                                                        else if(obj2 == hour) return NSOrderedAscending;
                                                                        else {
                                                                            if(obj1.operationEvent == OperationEventOpen){
                                                                                return NSOrderedDescending;
                                                                            } else if (obj1.operationEvent == OperationEventClose){
                                                                                return NSOrderedAscending;
                                                                            } else {
                                                                                return NSOrderedSame;
                                                                            }
                                                                        }
                                                                    }
                                                                }];
                
                    [allMenuHours insertObject:hourDict atIndex:firstMatchingIndex];
                } else {
                    if(firstAlwaysMenuIndex == NSNotFound){
                        firstAlwaysMenuIndex = initialIndex;
                    }
                }
            }
        }
        
        if(!found){
            if([allMenuHours count] > 0){
                NSCalendar *calendar = [NSCalendar currentCalendar];
                calendar.timeZone = ORDER.merchant.timeZone;
                
                NSDateComponents *currentComponents = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekday
                                                                  fromDate:[NSDate date]];
                NSInteger weekday = (currentComponents.weekday - 1);
                if(weekday == 0) weekday = 7;
                
                OperationHour *currentHour = [[OperationHour alloc] init];
                
                currentHour.eventTime = [[NSString stringWithFormat:@"%ld%02ld%02ld",
                                          (long)weekday,
                                          (long)currentComponents.hour,
                                          (long)currentComponents.minute] integerValue];
                PCLog(@"allMenuHours : %@", allMenuHours);
                PCLog(@"Current Hour : %@", currentHour);
                NSDictionary *currentDict = @{@"hour":currentHour};
                
                NSUInteger firstMatchingIndex = [allMenuHours indexOfObject:currentDict
                                                              inSortedRange:NSMakeRange(0, [allMenuHours count])
                                                                    options:NSBinarySearchingInsertionIndex | NSBinarySearchingFirstEqual
                                                            usingComparator:^NSComparisonResult(NSDictionary *dict1, NSDictionary *dict2) {
                                                                OperationHour *obj1 = [dict1 objectForKey:@"hour"];
                                                                OperationHour *obj2 = [dict2 objectForKey:@"hour"];
                                                                
                                                                if(obj1.eventTime > obj2.eventTime){
                                                                    return NSOrderedDescending;
                                                                } else if(obj1.eventTime < obj2.eventTime){
                                                                    return NSOrderedAscending;
                                                                } else {
                                                                    if(obj1 == currentHour) return NSOrderedDescending;
                                                                    else if(obj2 == currentHour) return NSOrderedAscending;
                                                                    else {
                                                                        if(obj1.operationEvent == OperationEventOpen){
                                                                            return NSOrderedDescending;
                                                                        } else if (obj1.operationEvent == OperationEventClose){
                                                                            return NSOrderedAscending;
                                                                        } else {
                                                                            return NSOrderedSame;
                                                                        }
                                                                    }
                                                                }
                                                            }];

                if(firstMatchingIndex == [allMenuHours count]){
                    firstMatchingIndex = 0;
                }
                NSDictionary *nearestHourDict = [allMenuHours objectAtIndex:firstMatchingIndex];
                initialIndex = ((NSNumber *)[nearestHourDict objectForKey:@"panIndex"]).unsignedIntegerValue;

            } else {
                if(firstAlwaysMenuIndex == NSNotFound){
                    initialIndex = 0;
                } else {
                    initialIndex = firstAlwaysMenuIndex;
                }
            }
        }
        self.selectedPageIndex = initialIndex + 1;
    }
    
    [self.menupanNavigationScrollView removeAllSubviews];
    self.menupanNavigationScrollView.contentOffset = CGPointZero;
    
    [self.pageDotContainer removeAllSubviews];
    
    if([MENUPAN.activeMenuList count] > 1){
        
        self.menupanNavigationScrollView.hidden = NO;
        [self.noItemView removeFromSuperview];
        self.leftFadeImageView.hidden = NO;
        self.rightFadeImageView.hidden = NO;
        self.prePageButton.hidden = NO;
        self.nextPageButton.hidden = NO;
        self.pageDotContainer.hidden = NO;
        
        pageViewFrame.origin.y = CGRectGetMaxY(self.menupanNavigationScrollView.frame);
        pageViewFrame.size.height -= pageViewFrame.origin.y;
        
        NSUInteger panIndex = 0;
        CGFloat sideMargin = 29.0f;
        
        for(MenuPan *pan in MENUPAN.activeMenuList){
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake((panIndex * APP.window.width) + sideMargin,
                                                                       0.0f, APP.window.width - (sideMargin * 2),
                                                                       self.menupanNavigationScrollView.height - 8.0f)];
            label.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:13.0f];
            label.textColor = [UIColor c12Color];
            label.textAlignment = NSTextAlignmentCenter;
            label.text = pan.name;
            label.backgroundColor = [UIColor clearColor];
            [self.menupanNavigationScrollView addSubview:label];
            
            panIndex++;
        }
        
        [self.menupanNavigationScrollView setContentSize:CGSizeMake(APP.window.width * panIndex,
                                                                    10.0f)];
        [self.menupanNavigationScrollView setContentOffset:CGPointMake((self.selectedPageIndex - 1) * APP.window.width, 0.0f)
                                                  animated:NO];
        
        NSUInteger i = 0;
        
        for(i = 0; i < [MENUPAN.activeMenuList count] ; i++){
            
            CGFloat viewWidth = APP.window.width;
            
            CGFloat originX = (viewWidth - ([MENUPAN.activeMenuList count] * DOT_SPACE)) / 2;
            UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(originX + (i * DOT_SPACE),
                                                                          0.0f,
                                                                          DOT_SPACE,
                                                                          5.0f)];
            button.tag = (i+1);
            [button setImage:[UIImage imageNamed:@"point_slide_off"]
                    forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"point_slide_on"]
                    forState:UIControlStateSelected];
            
            [self.pageDotContainer addSubview:button];
            
            if(i+1 == self.selectedPageIndex){
                button.selected = YES;
            }
        }
    } else {
        
        self.menupanNavigationScrollView.hidden = YES;
        self.leftFadeImageView.hidden = YES;
        self.rightFadeImageView.hidden = YES;
        self.prePageButton.hidden = YES;
        self.nextPageButton.hidden = YES;
        self.pageDotContainer.hidden = YES;
        
        if([MENUPAN.activeMenuList count] == 0){
            if(MENUPAN.isMenuFetched){
                [self showNoItemView];
            }
        } else { // count == 1
            
        }
    }
    
    self.pageViewController.view.frame = pageViewFrame;
    
    MenuCellController *menuCellController = [self menuCellControllerAtIndex:(self.selectedPageIndex - 1)];
    
    if(self.firstMatchingIndexPath != nil){
        menuCellController.firstMatchingIndexPath = self.firstMatchingIndexPath;
    }
    [menuCellController reloadData];
    
    if(menuCellController != nil){
        [self.pageViewController setViewControllers:@[menuCellController]
                                          direction:UIPageViewControllerNavigationDirectionForward
                                           animated:NO
                                         completion:nil];
    } else {
        // TODO: show no item view
    }
}

- (IBAction)pageMoveButtonTouched:(UIButton *)sender
{
    NSInteger pageIndexBasedZero = self.selectedPageIndex - 1;
    NSInteger newIndex = 0;
    
    switch(sender.tag){
        case 1: //Backward
            if(pageIndexBasedZero > 0){
                newIndex = pageIndexBasedZero - 1;
            } else {
                PCWarning(@"Out of bound of page index : Downflow");
                newIndex = 0;
            }
            break;
        case 2: //Forward
            if(pageIndexBasedZero < ([MENUPAN.activeMenuList count] - 1)){
                newIndex = pageIndexBasedZero + 1;
            } else {
                PCWarning(@"Out of bound of page index : Overflow");
                newIndex = ([MENUPAN.activeMenuList count] - 1);
            }
            break;
    }
    
    [self.menupanNavigationScrollView setContentOffset:CGPointMake(newIndex * self.menupanNavigationScrollView.width, 0.0f)
                                              animated:YES];
    [self scrollMenuPanPage:(newIndex+1)];
}

- (void)scrollMenuPanPage:(NSInteger)currentPageIndex
{
    UIPageViewControllerNavigationDirection direction = UIPageViewControllerNavigationDirectionForward;
    if(self.selectedPageIndex != currentPageIndex){
        if(self.selectedPageIndex > currentPageIndex){
            direction = UIPageViewControllerNavigationDirectionReverse;
        }
        self.selectedPageIndex = currentPageIndex;

        MenuCellController *cellController = [self menuCellControllerAtIndex:(self.selectedPageIndex - 1)];

        if(self.pageViewController.viewControllers.count > 0){

            MenuCellController *currentCellController = [self.pageViewController.viewControllers objectAtIndex:0];
            PCLog(@"menuCellController %d at currentCellController", currentCellController.menuPanIndex);

            if(currentCellController.menuPanIndex != cellController.menuPanIndex){
                if(cellController != nil){
                    [self.pageViewController setViewControllers:@[cellController]
                                                      direction:direction
                                                       animated:YES
                                                     completion:^(BOOL finished) {

                                                     }];
                } else {
                    PCWarning(@"Ooops no menu cell Controller more at %d", self.selectedPageIndex);
                }
            }
        } else {
            PCLog(@"================================= pageviewController count 0 =====================");
        }
    }
    
    for(UIButton *dotButton in self.pageDotContainer.subviews){
        if(dotButton.tag > 0){
            dotButton.selected = (dotButton.tag == self.selectedPageIndex);
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView == self.menupanNavigationScrollView){
        self.prePageButton.alpha = scrollView.contentOffset.x / scrollView.width;
        self.nextPageButton.alpha = (scrollView.contentSize.width - (scrollView.contentOffset.x + scrollView.width)) / scrollView.width;
    }
}

- (IBAction)gotoRequestBillButton:(id)sender
{
    [self pushTablesetViewController];
}

- (void)pushTablesetViewController
{
    TablesetViewController *viewController = [TablesetViewController viewControllerFromNib];    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)tableListChanged:(NSNotification *)notification
{
    [self.tableNumberPickerView reloadAllComponents];

    // When Picker first
//    if([self.tableNumberTextField.text length] == 0){
//        if([ORDER.tableList count] > 0){
//            TableInfo *firstTableInfo = [ORDER.tableList objectAtIndex:0];
//            self.tableNumberTextField.text = firstTableInfo.tableNumber;
//        }
//    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableNumberScrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableNumberView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.tableNumberScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[tableNumberView(==tableNumberScrollView)]"
                                                                                       options:0
                                                                                       metrics:nil
                                                                                         views:@{@"tableNumberScrollView":self.tableNumberScrollView,
                                                                                                 @"tableNumberView":self.tableNumberView}]];
    
    [self.tableNumberScrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[tableNumberView(==tableNumberScrollView)]"
                                                                                       options:0
                                                                                       metrics:nil
                                                                                         views:@{@"tableNumberScrollView":self.tableNumberScrollView,
                                                                                                 @"tableNumberView":self.tableNumberView}]];
    
    self.alertViewVerticalConstraint.constant = -200.0f;
    
    [self reloadData];
    
    
    [self addChildViewController:self.pageViewController];
    
    [self.view insertSubview:self.pageViewController.view
                belowSubview:self.locationAlertView];
    
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemCart
                                                                              target:self
                                                                              action:@selector(orderButtonTouched:)];

//    [self.tableNumberScrollView setContentSize:CGSizeMake(320.f, 433.0f)];
    
    self.tableNumberScrollView.adjustingOffsetY = -100.0f;
    
    self.tableNumberTextField.inputView = self.tableNumberPickerView;
    self.toggleBarButtonItem.title = NSLocalizedString(@"Keyboard", nil);
    self.tableNumberTextField.inputAccessoryView = self.tableNumberToolBar;
    
    [self hideTableNumber:NO];

    if(self.tableInfo != nil){
        [ORDER checkTable:self.tableInfo
                  success:^(){
                      if(self.didAppeared){
                          if(ORDER.shouldOpenRightSlide){
                              [self showOrderViewController];
                          }
                      }
                  }
                  failure:^(OrderStatus status){
                      
                  }];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(MENUPAN.isDirty){
        [self reloadData];
    }
    
    switch(MENUPAN.selectedMenuListType){
        case MenuListTypeDinein:
            self.title = NSLocalizedString(@"Dine-in Menu", nil);
            [self showTableNumber:NO];
            [self hideTableNumber:NO];
            break;
        case MenuListTypeTakeout:
            self.title = NSLocalizedString(@"Take-out Menu", nil);
            break;
        case MenuListTypeDelivery:
            self.title = NSLocalizedString(@"Delivery Menu", nil);
            break;
        default:
            self.title = NSLocalizedString(@"Menu", nil);
            break;
    }
}

- (MenuCellController *)menuCellControllerAtIndex:(NSInteger)index
{
    PCLog(@"--- menuCellControllerAtIndex index %d", index);
    if([MENUPAN.activeMenuList count] == 0){
        return nil;
    }

    if(index < 0) {
        return nil;
    }

    if(index >= [MENUPAN.activeMenuList count]){
        return nil;
    }

    MenuCellController *menuCellController = nil;
    
    if([self.cellViewControllers count] > index){

        menuCellController = [self.cellViewControllers objectAtIndex:index];
    }
    
    if(menuCellController == nil || [menuCellController isKindOfClass:[NSNull class]]){

        menuCellController = [MenuCellController viewControllerFromNib];
        menuCellController.referredMenus = self.referredMenus;
        menuCellController.menuPanIndex = index;
        menuCellController.initialCartType = self.initialCartType;
        menuCellController.delegate = self;
        [self.cellViewControllers replaceObjectAtIndex:index
                                            withObject:menuCellController];
    }
    
    return menuCellController;
}


//- (void)addFooterIfNeeded
//{
//    if([MENUPAN.menuList count] < 2 && [MENUPAN.menuList count] > 0){
//        self.tableView.tableFooterView = nil;
//    } else {
//        
//        MenuCategory *menuCategory = [MENUPAN.menuList lastObject];
//        
//        CGFloat heightSumOfItems = 0.0f;
//        
//        if([menuCategory.name length] > 0){
//            heightSumOfItems += 28.0f;
//        }
//        
//        for(id item in menuCategory.items){
//        
//            if([item isKindOfClass:[MenuSubCategory class]]){
//                MenuSubCategory *subCategory = (MenuSubCategory *)item;
//                if([subCategory.name length] > 0){
//                    heightSumOfItems += 28.0f;
//                }
//            } else {
//                MenuItem *castedMenuItem = (MenuItem *)item;
//                if(castedMenuItem.cellHeight == 0.0f){
//                    [self.offScreenMenuItemCell setItemForHeight:castedMenuItem];
//                    castedMenuItem.cellHeight = [self.offScreenMenuItemCell cellHeight];
//                }
//                heightSumOfItems += castedMenuItem.cellHeight;
//            }
//            
//            if(heightSumOfItems >= self.tableView.height){
//                break;
//            }
//        }
//        
//        CGFloat expectedHeight = 0.0f;
//        
//        if(heightSumOfItems < self.tableView.height){
//            self.tableView.tableFooterView = nil;
//            expectedHeight = (self.tableView.height - heightSumOfItems);
//            expectedHeight = MAX((expectedHeight + 3.0f), 60.0f);
//            self.tableFooterView.height = expectedHeight;
//            self.tableView.tableFooterView = self.tableFooterView;
//        } else {
//            self.tableView.tableFooterView = nil;
//        }
//    }
//}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    if(self.initialCartType == CartTypeDelivery){
//        if(![NSUserDefaults standardUserDefaults].warnedServiceFpee){
//
//            if(ORDER.merchant.hasRoServiceFee){
//                
//                CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
//                
//                ServiceFeeGuideView *view = [ServiceFeeGuideView view];
//                view.guideLabel.text = [NSString stringWithFormat:NSLocalizedString(@"To offer delivery from places that don't normally deliver, we sometimes partner with 3rd parties who charge an added Service Fee. We're working hard to get these fees down, so just hang on tight!", nil),
//                                        [[NSNumber numberWithFloat:ORDER.merchant.roServiceRate] percentString]];
//                [alertView setContainerView:view];
//                alertView.buttonTitles = [NSArray arrayWithObject:NSLocalizedString(@"Got it!", nil)];
//                [alertView show];
//                
//                [view startAnimation];
//                
//                [NSUserDefaults standardUserDefaults].warnedServiceFee = YES;
//            }
//        }
        if(self.isEnteredFromList && !ORDER.shouldOpenRightSlide){
            [self checkDeliveryDistanceAndAlertIfNeeded];
        }
        
    }
    
    [self reloadBadgeNumber:[NSNumber numberWithBool:NO]];
    
    if(!self.didAppeared){
        if(self.isForceOpenOrderViewController || ORDER.shouldOpenRightSlide){
            self.forceOpenOrderViewController = NO;
            [self showOrderViewController];
        }

        if(ORDER.merchant != nil){
            [ORDER requestMerchantInfo];
            self.didAppeared = YES;
        } else {
            
        }
    }
}

- (void)checkDeliveryDistanceAndAlertIfNeeded
{
    if(((ORDER.merchant.distance * METERS_TO_FEET)/FEET_IN_MILES) >= ORDER.merchant.deliveryDistanceLimit){
        if(self.locationAlertView == nil){
            return;
        }
        if(self.alertViewVerticalConstraint.constant != 0.0f){
            [UIView animateWithDuration:0.3f
                             animations:^{
                                 self.alertViewVerticalConstraint.constant = 0.0f;
                                 [self.locationAlertView.superview layoutIfNeeded];
                             } completion:^(BOOL finished) {
                                 [self performSelector:@selector(locationAlertCloseButtonTouched:)
                                            withObject:self
                                            afterDelay:20.0f];
                             }];
        }
    }
}

- (IBAction)locationAlertCloseButtonTouched:(id)sender
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(locationAlertCloseButtonTouched:)
                                               object:self];
    if(self.locationAlertView != nil){
        [UIView animateWithDuration:0.3f
                              delay:0.2f
             usingSpringWithDamping:0.9f
              initialSpringVelocity:0.1f
                            options:0
                         animations:^{
                             self.alertViewVerticalConstraint.constant = -200.0f;
                             [self.locationAlertView.superview layoutIfNeeded];
                         } completion:^(BOOL finished) {
                             [self.locationAlertView removeFromSuperview];
                             self.locationAlertView = nil;
                         }];
    }
}

- (void)orderButtonTouched:(id)sender
{
    if(ORDER.merchant.isTableBase && ORDER.tableInfo == nil && ORDER.order == nil
       && (self.initialCartType == CartTypeCart)){
        [self showTableNumber:YES];
    } else {
        [self showOrderViewController];
    }
}

- (void)showOrderViewController
{
    OrderViewController *viewController = [UIStoryboard initialViewControllerFrom:@"Order"];
    viewController.delegate = self;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
                                           
}


- (void)viewDidUnload
{
    [self setTableNumberView:nil];
    [self setTableNumberScrollView:nil];
    [self setTableNumberTextField:nil];

    [self setToggleBarButtonItem:nil];
    [self setNoItemView:nil];
    [self setNoMenuInfoLabel:nil];
    [self setGotoRequestBillButton:nil];
    [super viewDidUnload];
}

- (void)dealloc
{
    self.locationAlertView = nil;
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(locationAlertCloseButtonTouched:)
                                               object:self];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (void)showTableNumber:(BOOL)animated
{
    [APP.window addSubview:self.tableNumberScrollView];
    [APP.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[tableNumberScrollView]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:@{@"tableNumberScrollView":self.tableNumberScrollView}]];
    
    [APP.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tableNumberScrollView]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:@{@"tableNumberScrollView":self.tableNumberScrollView}]];
    
    self.tableNumberTopConstraint.constant = self.view.height;
    
    if(animated){
        [UIView animateWithDuration:0.3
                              delay:0.0f
             usingSpringWithDamping:0.85f
              initialSpringVelocity:0.5f
                            options:0
                         animations:^{
                             [self moveTableNumber:YES];
                         } completion:^(BOOL finished) {
                             
                         }];
    } else {
        [self moveTableNumber:YES];
    }
}

- (void)hideTableNumber:(BOOL)animated
{
    if(animated){
        [UIView animateWithDuration:0.3
                              delay:0.0f
             usingSpringWithDamping:0.85f
              initialSpringVelocity:0.5f
                            options:0
                         animations:^{
                             [self moveTableNumber:NO];
                         } completion:^(BOOL finished) {
                             if(finished){
                                 [self.tableNumberScrollView removeFromSuperview];
                             }
                         }];
    } else {
        [self moveTableNumber:NO];
        [self.tableNumberScrollView removeFromSuperview];
    }
}

- (void)moveTableNumber:(BOOL)show
{
    self.tableNumberTopConstraint.constant = show ? 0.0f : self.view.height;
    self.tableNumberScrollView.alpha = show ? 1.0f : 0.0f;
    [self.tableNumberView layoutIfNeeded];
}


- (IBAction)confirmButtonTouched:(id)sender
{
    if(ORDER.merchant.isTableBase
       && [self.tableNumberTextField.text length] == 0){
        [self.tableNumberTextField becomeFirstResponder];
        return;
    }
    
    if(![ORDER isNearMerchant]){
        if(ORDER.merchant.isAbleTakeoutOrDelivery){
            [UIAlertView alertWithTitle:NSLocalizedString(@"It is Too Far to Order", nil)
                                message:NSLocalizedString(@"You can make an order at restaurant which is within 1 mile. You can make take-out or delivery order without restriction of distance (1 mile).", nil)];
        } else {
            [UIAlertView alertWithTitle:NSLocalizedString(@"It is Too Far to Order", nil)
                                message:NSLocalizedString(@"You can make an order at restaurant which is within 1 mile.", nil)];
        }
        return;
    }
    
    [self doneButtonTouched:sender];
    
    // !!!: Same code in doneButtonTouched
    if(self.tableInfo == nil
       && ORDER.cart.cartType == CartTypeUnknown){
        // TalbeNumber 또는 주문 타입이 결정되지 않은 경우
        return;
    }
    
    [self hideTableNumber:YES];
    
    if(ORDER.merchant.isTableBase){
        [ORDER checkTable:self.tableInfo
               assertMine:NO
                  success:^(){
                      
                      ORDER.cart.cartType = self.initialCartType;
                      if(self.currentCellController.selectedMenuItem != nil){
                          [self.currentCellController addMenuItemAsSelectedPrevious];
                      } else {
                          // Just Open the order list - like Check - In button
                          [self orderButtonTouched:self];
                      }
                  }
                  failure:^(OrderStatus status){
                      if(status == OrderStatusCompleted){
                          [UIAlertView alertWithTitle:NSLocalizedString(@"Prior Table Orders Not Settled", nil)
                                              message:NSLocalizedString(@"Please wait for the restaurant to clear prior table orders. If table is not cleared for an extended period of time, please ask restaurant staff for assistance", nil)];
                      }
                  }];
    } else {
        if(self.currentCellController != nil){
            [self.currentCellController addMenuItemAsSelectedPrevious];
        } else {

        }
    }
}




- (void)orderViewController:(OrderViewController *)orderViewController didTouchOrderDoneButton:(id)sender
{
    TRANS.dirty = YES;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

//- (void)orderViewController:(OrderViewController *)orderViewController didTouchOnlyCheckoutButton:(id)sender
//{
////    [APP.viewDeckController closeRightViewAnimated:YES
////                                        completion:^(IIViewDeckController *controller, BOOL success) {
////                                            if(success){
////                                                TablesetViewController *viewController = [TablesetViewController viewControllerFromNib];
////                                                [self.navigationController pushViewController:viewController animated:YES];
////                                            }
////                                        }];
//    TablesetViewController *viewController = [TablesetViewController viewControllerFromNib];
//    [self.navigationController pushViewController:viewController animated:YES];
//}

- (void)orderViewController:(OrderViewController *)orderViewController didTouchCheckinButton:(id)sender
{
//    [APP.viewDeckController closeRightViewAnimated:YES
//                                        completion:
//     ^(IIViewDeckController *controller, BOOL success) {
//         if(success){
//             [self showTableNumber:YES];
//         }
//     }];
    [self showTableNumber:YES];
}

- (CartType)orderViewControllerInitialCartType:(OrderViewController *)orderViewController
{
    return self.initialCartType;
}

- (IBAction)tableNumberCancelButton:(id)sender
{
    [self.currentCellController resetSelection];
    [self hideTableNumber:YES];
}

#pragma mark - UIPickerViewDataSource
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger count = [ORDER.tableList count];
    
    return MAX(count, 1);
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if([ORDER.tableList count] > row){
        TableInfo *table = [ORDER.tableList objectAtIndex:row];
        return table.tableNumber;
    }
    
    return @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if([ORDER.tableList count] > row){
        TableInfo *table = [ORDER.tableList objectAtIndex:row];
        self.tableInfo = table;
        self.tableNumberTextField.text = table.tableNumber;
    }
}

- (IBAction)toggleButtonTouched:(id)sender
{
    [self.tableNumberTextField resignFirstResponder];
    
    if(self.tableNumberTextField.inputView != nil){
        self.tableNumberTextField.inputView = nil;
        self.toggleBarButtonItem.title = NSLocalizedString(@"Picker", nil);
    } else {
        self.tableNumberTextField.inputView = self.tableNumberPickerView;
        self.toggleBarButtonItem.title = NSLocalizedString(@"Keyboard", nil);
    }
    
    [self.tableNumberTextField becomeFirstResponder];
}

// Table number Picker Done button touched
- (IBAction)doneButtonTouched:(id)sender
{
    if(ORDER.cart.cartType == CartTypePickup){
        // Pass this case
    } else {
        if(!ORDER.merchant.isTableBase && ORDER.merchant.servicedBy != ServicedByStaff){
            ORDER.cart.cartType = CartTypePickup;
        } else {
            if(self.tableNumberTextField.inputView == nil){
                // When the user input table number manually. Not by Picker view
                NSString *inputTableNumber = self.tableNumberTextField.text;
                
                if([inputTableNumber length] > 0){
                    
                    self.tableInfo = nil;
                    TableInfo *selectedTableInfo = nil;
                    for(TableInfo *tableInfo in ORDER.tableList){
                        
                        if([tableInfo.tableNumber isCaseInsensitiveEqual:inputTableNumber]){
                            selectedTableInfo = tableInfo;
                            break;
                        }
                    }
                    
                    if(selectedTableInfo == nil){
                        if(!ORDER.merchant.isTableBase && ORDER.merchant.servicedBy == ServicedByStaff){
                            [UIAlertView alertWithTitle:NSLocalizedString(@"Invalid Table Number", nil)
                                                message:NSLocalizedString(@"You have entered a table number outside the range established by the restaurant.  Please check and re-enter.", nil)];
                        } else {
                            [UIAlertView alertWithTitle:NSLocalizedString(@"Invalid Table Number", nil)
                                                message:NSLocalizedString(@"You have entered a table number outside the range established by the restaurant.  Please check and re-enter.", nil)];
                        }
                        [self.tableNumberTextField becomeFirstResponder];
                    }
                    
                    if(ORDER.merchant.isTableBase){
                        self.tableInfo = selectedTableInfo;
                    } else {
                        if(ORDER.merchant.servicedBy == ServicedByStaff){
                            ORDER.cart.cartType = CartTypePickupServiced;
                            ORDER.cart.flagNumber = selectedTableInfo.tableNumber;
                        } else {
                            PCError(@"Merchant does not support table service : Keyboard");
                        }
                    }
                } else {
                    // Nothing to do
                }
            } else {
                // By picker view
                NSInteger selRow = [self.tableNumberPickerView selectedRowInComponent:0];
                
                if([ORDER.tableList count] > selRow){
                    TableInfo *table = [ORDER.tableList objectAtIndex:selRow];
                    
                    if(ORDER.merchant.isTableBase){
                        self.tableInfo = table;
                    } else {
                        if(ORDER.merchant.servicedBy == ServicedByStaff){
                            ORDER.cart.cartType = CartTypePickupServiced;
                            ORDER.cart.flagNumber = table.tableNumber;
                        } else {
                            PCError(@"Merchant does not support table service : Picker");
                        }
                    }
                    self.tableNumberTextField.text = table.tableNumber;
                }
            }
        }
    }
    
    // !!!: Same code in confirmButtonTouched
    if(self.tableInfo == nil
       && ORDER.cart.cartType == CartTypeUnknown){
        // TalbeNumber 또는 주문 타입이 결정되지 않은 경우
    } else {
        [self.tableNumberTextField resignFirstResponder];
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#define ANIMA_DURATION 0.55f

- (void)menuCellController:(MenuCellController *) cellController
    cartAddAnimateWithCell:(MenuItemCell *)cell
{
    CGRect rect = [APP.window convertRect:cell.menuImageButton.frame fromView:cell.menuImageButton.superview];
//    rect = CGRectMake(5, (rect.origin.y*-1)-10, imgV.frame.size.width, imgV.frame.size.height);
//    NSLog(@"rect is %f,%f,%f,%f",rect.origin.x,rect.origin.y,rect.size.width,rect.size.height);
    
    // create new duplicate image
    
//    UIView *aniView = [[UIView alloc] initWithFrame:rect];
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:cell.menuImageButton.image];
    imgView.contentMode = cell.menuImageButton.contentMode;
    imgView.clipsToBounds = YES;
    imgView.frame = rect;
    imgView.backgroundColor = [UIColor colorWithR:244.0f
                                                 G:239.0f
                                                 B:234.0f];
//    [aniView addSubview:imgView];
    
//    UIImageView *frameView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"edge_menu_frame"]];
//    frameView.frame = CGRectMake(0.0f, 0.0f, rect.size.width, rect.size.height);
//    [aniView addSubview:frameView];
    
    [APP.window addSubview:imgView];
    
    // begin ---- apply position animation
    CAKeyframeAnimation *pathAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    pathAnimation.calculationMode = kCAAnimationPaced;
    pathAnimation.fillMode = kCAFillModeForwards;
    pathAnimation.removedOnCompletion = YES;
    pathAnimation.duration=ANIMA_DURATION;
    [pathAnimation setValue:imgView forKey:@"animationView"];
//    pathAnimation.delegate = self;
    
    // tab-bar right side item frame-point = end point
    CGFloat rightEndX = CGRectGetMaxX(self.view.frame);
    CGPoint endPoint = CGPointMake(rightEndX - 26.0f, 38.0f);
    
    CGMutablePathRef curvedPath = CGPathCreateMutable();
    CGPathMoveToPoint(curvedPath, NULL, imgView.center.x, imgView.center.y);
    CGPathAddCurveToPoint(curvedPath, NULL, endPoint.x, imgView.frame.origin.y, endPoint.x, imgView.frame.origin.y, endPoint.x, endPoint.y);
    pathAnimation.path = curvedPath;
    CGPathRelease(curvedPath);
    // end ---- apply position animation
    
    // apply transform animation
    CABasicAnimation *basic=[CABasicAnimation animationWithKeyPath:@"transform"];
    [basic setToValue:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.01, 0.01, 0.01)]];
    [basic setAutoreverses:NO];
//    basic.cumulative = YES;
    basic.delegate = self;
    [basic setValue:imgView forKey:@"animationView"];
    [basic setDuration:ANIMA_DURATION];
    basic.removedOnCompletion = YES;
    
    [imgView.layer addAnimation:pathAnimation forKey:@"curveAnimation"];
    [imgView.layer addAnimation:basic forKey:@"transform"];
    
}

- (void)menuCellController:(MenuCellController *) cellController
           lockReloadBadge:(BOOL)value
{
    self.lockReloadBadge = value;
}

- (void)menuCellControllerShowTableNumber:(MenuCellController *) cellController
{
    self.currentCellController = cellController;
    [self showTableNumber:YES];
}

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag
{
    UIImageView *view = [theAnimation valueForKey:@"animationView"];
    if (view) {
        [view removeFromSuperview];
        [theAnimation setValue:nil forKey:@"animationView"];
    }
    [self reloadBadgeNumber:[NSNumber numberWithBool:YES]];
    [self wiggleCart];
}

- (void)reloadBadgeNumber:(NSNumber *)withBounce
{
    NSInteger badgeCount = 0;
    if(ORDER.order.isEditable){
        badgeCount = ORDER.order.quantities;
    } else if(ORDER.cart.status == CartStatusOrdering){
        badgeCount = ORDER.cart.quantities;
    }
    
    NSString *countString = nil;
    
    if((ORDER.cart.quantities == 0 && ORDER.order.status == OrderStatusConfirmed) ||
       (!ORDER.merchant.isTableBase && ORDER.order.status == OrderStatusDraft)){
        if(ORDER.merchant.isAblePayment){
            countString = NSLocalizedString(@"Check", nil);
        } else {
            countString = NSLocalizedString(@"Done", nil);
        }
    } else {
        if(badgeCount > 0)
            countString = [NSString stringWithFormat:@"%ld", (long)badgeCount];
    }

    self.navigationItem.rightBarButtonItem.button.badgeValue = countString;
    
//    if([withBounce boolValue]){
//        [APP.viewDeckController previewBounceView:IIViewDeckRightSide];
//    }
    // TODO: show animation for cart ziggling?

    self.lockReloadBadge = NO;
}

- (void)wiggleCart
{
    [CATransaction begin];
    
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
    animation.keyPath = @"position.x";
    animation.values = @[ @0, @-3, @2, @-0.5, @0 ];
    animation.keyTimes = @[ @0, @(3.0 / 11.0), @(6.0 / 11.0), @(9.0 / 11.0), @1 ];
    animation.duration = 0.3f;
    animation.additive = YES;
    animation.removedOnCompletion = YES;
    
    CAKeyframeAnimation *animation2 = [CAKeyframeAnimation animation];
    animation2.keyPath = @"position.y";
    animation2.values = @[ @0, @2, @(-1.5), @1, @0 ];
    animation2.keyTimes = @[ @0, @(3.0 / 11.0), @(6.0 / 11.0), @(9.0 / 11.0), @1 ];
    animation2.duration = 0.3f;
    animation2.additive = YES;
    animation2.removedOnCompletion = YES;
    
    [self.navigationItem.rightBarButtonItem.button.layer addAnimation:animation2
                                                               forKey:@"wiggle2"];
    
    [self.navigationItem.rightBarButtonItem.button.layer addAnimation:animation
                                                               forKey:@"wiggle"];
    
    [CATransaction commit];
}


- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(actionSheet.tag){
        case 201:
            break;
        case 301:
            break;
        default:
            break;
    }
}

#pragma mark - UIPageViewControllerDelegate 
#pragma mark optional
- (void)pageViewController:(UIPageViewController *)pageViewController
willTransitionToViewControllers:(NSArray *)pendingViewControllers
{
    MenuCellController *newController = [pendingViewControllers objectAtIndex:0];
    self.reservedPageIndex = newController.menuPanIndex;
}


- (void)pageViewController:(UIPageViewController *)pageViewController
        didFinishAnimating:(BOOL)finished
   previousViewControllers:(NSArray *)previousViewControllers
       transitionCompleted:(BOOL)completed
{
    if(completed){
        [self.menupanNavigationScrollView setContentOffset:CGPointMake(self.reservedPageIndex * self.menupanNavigationScrollView.width, 0.0f)
                                                  animated:YES];
        
        self.selectedPageIndex = self.reservedPageIndex + 1;

        for(UIButton *dotButton in self.pageDotContainer.subviews){
            if(dotButton.tag > 0){
                dotButton.selected = (dotButton.tag == self.selectedPageIndex);
            }
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self locationAlertCloseButtonTouched:nil];
}

//- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController
//                   spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation
//{
//    return UIPageViewControllerSpineLocationMax;
//}

#pragma mark - UIPageViewControllerDataSource
#pragma mark required
- (MenuCellController *)pageViewController:(UIPageViewController *)pageViewController
             viewControllerBeforeViewController:(MenuCellController *)viewController
{
    return [self menuCellControllerAtIndex:viewController.menuPanIndex-1];
}

- (MenuCellController *)pageViewController:(UIPageViewController *)pageViewController
              viewControllerAfterViewController:(MenuCellController *)viewController
{
    return [self menuCellControllerAtIndex:viewController.menuPanIndex+1];
}


- (NSMutableArray *)cellViewControllers
{
    if(_cellViewControllers == nil){
        _cellViewControllers = [NSMutableArray arrayWithCapacity:[MENUPAN.activeMenuList count]];
        for(NSUInteger i = 0 ; i < [MENUPAN.activeMenuList count] ; i++){
            [_cellViewControllers addObject:[NSNull null]];
        }
    }
    return _cellViewControllers;
}

@end
