//
//  DiscountViewController.m
//  RushOrder
//
//  Created by Conan on 5/23/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "DiscountViewController.h"

@interface DiscountViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation DiscountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Applied Discount", nil);
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemClose
                                                                        target:self
                                                                        action:@selector(closeButtonTouched:)];
}

- (void)closeButtonTouched:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 
                             }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.payment.appliedPromotions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellString = @"DiscountTableViewCell";
    
    DiscountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellString];
    
    if(cell == nil){
        cell = [DiscountTableViewCell cell];
        cell.delegate = self;
    }
    
    Promotion *promotion = [self.payment.appliedPromotions objectAtIndex:indexPath.row];
    cell.promotion = promotion;
    [cell fillContent];
    return cell;
}

- (void)discountTableViewCell:(DiscountTableViewCell *)cell
didTouchRemoveButtonAtIndexPath:(NSIndexPath *)indexPath
{
    Promotion *promotion = [self.payment.appliedPromotions objectAtIndex:indexPath.row];
    if(promotion.isRequiredCode){
        [self.payment removePromotion:promotion];
    } else {
        PCWarning(@"Tried to remove non promo code Prmotion -> Do nothing");
    }
    
    [self.tableView reloadData];
}


@end