//
//  DeliveryViewController.h
//  RushOrder
//
//  Created by Conan on 02/26/14.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "Payment.h"
#import "LocationViewController.h"
#import "FavoriteOrder.h"
#import "RecentListViewController.h"

@protocol DeliveryViewControllerDelegate;

@interface DeliveryViewController : PCViewController
<
UIPickerViewDataSource,
UIPickerViewDelegate,
UITextFieldDelegate,
UITextViewDelegate,
LocationViewControllerDelegate,
RecentListViewControllerDelegate
>

@property (nonatomic, weak) id<DeliveryViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextView *additionalRequest;
@property (weak, nonatomic) IBOutlet NPToggleButton *asapButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *pickupTimeButton;

@property (weak, nonatomic) IBOutlet PCTextField *nameTextField;
@property (weak, nonatomic) IBOutlet PCTextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet PCTextField *address1TextField;
@property (weak, nonatomic) IBOutlet PCTextField *address2TextField;
@property (weak, nonatomic) IBOutlet PCTextField *zipCodeTextField;
@property (weak, nonatomic) IBOutlet PCTextField *cityTextField;
@property (weak, nonatomic) IBOutlet PCTextField *stateTextField;

@property (nonatomic) CLLocationCoordinate2D geocodedCoords;

@property (strong, nonatomic) Payment *payment;

@property (nonatomic) BOOL disposeOrderWhenBack;

@property (nonatomic) NSInteger afterMinute;

@property (strong, nonatomic) FavoriteOrder *favoriteOrder;

@end

@protocol DeliveryViewControllerDelegate <NSObject>
@optional
- (void)deliveryViewController:(DeliveryViewController *)viewController
      didTouchPlaceOrderButton:(id)sender
             addressDictionary:(NSDictionary *)dictionary;
@end