//
//  OOAViewController.h
//  RushOrder
//
//  Created by Conan Kim on 5/1/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import "PCViewController.h"

@protocol OOAViewControllerDelegate;

@interface OOAViewController : PCViewController
@property (weak, nonatomic) id<OOAViewControllerDelegate> delegate;
@end

@protocol OOAViewControllerDelegate <NSObject>
- (void)ooaViewController:(OOAViewController *)viewController didTouchChangeLocationButton:(id)sender;
@end

