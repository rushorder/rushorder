//
//  RestaurantMapViewController.h
//  RushOrder
//
//  Created by Conan on 11/29/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestaurantMiniSummaryView.h"

@protocol RestaurantMapViewControllerDelegate;
@interface RestaurantMapViewController : UIViewController
<RestaurantMiniSummaryViewDelegate,
LogoCircleImageViewAction,
TouchableLabelAction
>

@property (weak, nonatomic) id <RestaurantMapViewControllerDelegate> delegate;
@property (strong, nonatomic) NSMutableArray *merchantList;
@property (strong, nonatomic) NSMutableArray *unavailableList;
@property (nonatomic) BOOL onlyMapMode;
@end


@protocol RestaurantMapViewControllerDelegate <NSObject>
@optional

- (void)restaurantMapViewController:(RestaurantMapViewController *)viewController
                didSelectRestaurant:(Merchant *)merchant
                       withCartType:(CartType)cartType;
@end
