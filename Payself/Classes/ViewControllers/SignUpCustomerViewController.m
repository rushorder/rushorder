//
//  SignUpCustomerViewController.m
//  RushOrder
//
//  Created by Conan on 2/19/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import "SignUpCustomerViewController.h"
#import "PCCredentialService.h"
#import "ProfileViewController.h"
#import "TransactionManager.h"
#import "AccountSwitchingManager.h"

@interface SignUpCustomerViewController ()

@property (weak, nonatomic) IBOutlet PCTextField *emailTextField;
@property (weak, nonatomic) IBOutlet PCTextField *nameTextField;
@property (weak, nonatomic) IBOutlet PCTextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet PCTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet PCTextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NPStretchableButton *createButton;
@end

@implementation SignUpCustomerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(syncingDone:)
                                                     name:AccountDataSwitchedNotification
                                                   object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Sign Up", nil);
    
    if(self.isRootViewController){
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Cancel",nil)
                                                                                  target:self
                                                                                  action:@selector(closeButtonTouched:)];
    } else {
        self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Cancel",nil)
                                                                                 target:self
                                                                                 action:@selector(closeButtonTouched:)];
    }
    self.emailTextField.text = [NSUserDefaults standardUserDefaults].lastTriedUserId;
    [self.scrollView setContentSizeWithBottomView:self.createButton];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)syncingDone:(NSNotification *)aNoti
{
    PCLog(@"Sync Done Sign Up");
    // TODO: Fail Handling
    
    // Go to profile editing
    [self pushProfileViewController];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SignedInNotification
                                                        object:self
                                                      userInfo:nil];
}

- (void)closeButtonTouched:(id)sender
{
    if([self.delegate respondsToSelector:@selector(signInCustomerViewControllerNeedDissmissingViewController:)]){
        [self.delegate signInCustomerViewControllerNeedDissmissingViewController:self];
    } else {
        [self dismissViewControllerAnimated:YES
                                 completion:^{
                                     
                                 }];
    }
}

- (IBAction)createButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    /*
     
     https://app.asana.com/0/30788832481633/421242599225367/f (Suggested Error Messages for Sign-Up Page)
     
     Title: Oops!
     Message: Please enter a valid e-mail address to continue signing up.
     
     Title: Oops!
     Message: Please enter your first name to continue signing up.
     
     Title: Oops!
     Message: Please enter your last name to continue signing up.
     
     Title: Oops!
     Message: Please enter a password to continue signing up.
     
     Title: Oops!
     Message: Please enter matching passwords to continue signing up.
     
     */
    
    if(![VALID validate:self.emailTextField
                  title:NSLocalizedString(@"Oops!", nil)
                message:NSLocalizedString(@"Please enter a valid e-mail address to continue signing up.", nil)]){
        return;
    }
    
    if(![VALID validateEmail:self.emailTextField
                       title:NSLocalizedString(@"Oops!", nil)
                     message:NSLocalizedString(@"Please enter a valid e-mail address to continue signing up.", nil)]) return;
    
    if(![VALID validate:self.nameTextField
                  title:NSLocalizedString(@"Oops!", nil)
                message:NSLocalizedString(@"Please enter your first name to continue signing up.", nil)]){
        return;
    }
    
    if(![VALID validate:self.lastNameTextField
                  title:NSLocalizedString(@"Oops!", nil)
                message:NSLocalizedString(@"Please enter your last name to continue signing up.", nil)]){
        return;
    }
    
    if(![VALID validate:self.passwordTextField
                  title:NSLocalizedString(@"Oops!", nil)
                message:NSLocalizedString(@"Please enter a password to continue signing up.", nil)]){
        return;
    }
    
    if(![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text]){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Oops!", nil)
                            message:NSLocalizedString(@"Please enter matching passwords to continue signing up.", nil)
                           delegate:self
                                tag:101];
        return;
    }
    
    [NSUserDefaults standardUserDefaults].lastTriedUserId = self.emailTextField.text;
    
    RequestResult result = RRFail;
    result = [CRED requestSignUp:self.emailTextField.text
                        password:self.passwordTextField.text
                       firstName:self.nameTextField.text
                        lastName:self.lastNameTextField.text
                     deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                 completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              NSArray *customers = [CustomerInfo listAll];
                              CustomerInfo *customerInfo = nil;
                              if([customers count] == 1){
                                  customerInfo = [customers objectAtIndex:0];
                              } else{
                                  PCWarning(@"Customers is not just only 1- sign up");
                                  // Clear
                                  for(CustomerInfo *customer in customers){
                                      if([customer delete]){
                                          // Success
                                      } else {
                                          PCError(@"Error to remove customerInfo - sign up");
                                      }
                                  }
                              }
                              
                              if(customerInfo == nil){
                                  customerInfo = [[CustomerInfo alloc] init];
                              }
                              
                              NSDictionary *customerDict = [response objectForKey:@"customer"];
                              [customerInfo updateWithDictionary:customerDict];
                              customerInfo.authToken = [response objectForKey:@"auth_token"];
                              
                              if([customerInfo save]){
                                  CRED.customerInfo = customerInfo;
                                  
                                  
                                      [SWITCH removeLocalData];

                                      [self pushProfileViewController];
                                      
                                      [[NSNotificationCenter defaultCenter] postNotificationName:SignedInNotification
                                                                                          object:self
                                                                                        userInfo:nil];

                              } else {
                                  [UIAlertView alertWithTitle:NSLocalizedString(@"Can Not Save Account Information", nil)
                                                      message:NSLocalizedString(@"Try to sign in again. If the problem contiue, Please contact us.\nhelp@rushorderapp.com.", nil)];
                              }
                          }
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              NSString *title = [response objectForKey:@"title"];
                              if([message isKindOfClass:[NSString class]]){
                                  if(title != nil){
                                      [UIAlertView alertWithTitle:title
                                                          message:message];
                                  } else {
                                      [UIAlertView alert:message];
                                  }
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while signing up.", nil)];
                              }
                              
                              CRED.customerInfo = nil;
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d.", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)pushProfileViewController
{
    ProfileViewController *viewController = [ProfileViewController viewControllerFromNib];
    viewController.delegate = self.delegate;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 101:
            [self.confirmPasswordTextField becomeFirstResponder];
            break;
        case 103:
            if(buttonIndex == AlertButtonIndexNO){
                
                [SWITCH removeLocalData];
                
                [self pushProfileViewController];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:SignedInNotification
                                                                    object:self
                                                                  userInfo:nil];
                
            } else if(buttonIndex == AlertButtonIndexYES){
                // Move to account
                [SWITCH startSync];
            }
            break;
        default:
            break;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isKindOfClass:[PCTextField class]]){
        if(textField == self.confirmPasswordTextField){
            [self createButtonTouched:nil];
        } else {
            [[(PCTextField *)textField nextField] becomeFirstResponder];
        }
    }
    return YES;
}
@end
