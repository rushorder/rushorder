//
//  TakeoutViewController.h
//  RushOrder
//
//  Created by Conan on 11/13/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "RecentListViewController.h"
#import "Payment.h"

@protocol TakeoutViewControllerDelegate;

@interface TakeoutViewController : PCViewController
<
UIPickerViewDataSource,
UIPickerViewDelegate,
UITextFieldDelegate,
UITextViewDelegate,
RecentListViewControllerDelegate
>

@property (nonatomic, weak) id<TakeoutViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextView *additionalRequest;
@property (weak, nonatomic) IBOutlet NPToggleButton *asapButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *pickupTimeButton;
@property (weak, nonatomic) IBOutlet PCTextField *orderNameField;
@property (weak, nonatomic) IBOutlet PCTextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *pickupTimeTextField;

@property (nonatomic) NSInteger afterMinute;

@property (copy, nonatomic) NSString *initialName;
@property (copy, nonatomic) NSString *initialPhoneNumber;
@property (copy, nonatomic) NSString *initialRequestMessage;

@property (strong, nonatomic) Payment *payment;
@property (nonatomic) BOOL disposeOrderWhenBack;

@end


@protocol TakeoutViewControllerDelegate <NSObject>
@optional
- (void)takeoutViewController:(TakeoutViewController *)viewController
     didTouchPlaceOrderButton:(id)sender;
- (void)takeoutViewController:(TakeoutViewController *)viewController
     didTouchSignInButton:(id)sender;
@end
