//
//  RapidCollectionViewController.m
//  RushOrder
//
//  Created by Conan on 8/12/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import "RapidCollectionViewController.h"
#import "FavoriteRestCollectionViewCell.h"
#import "Merchant.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PaymentHistoryViewController.h"
#import "FavoriteOrder.h"
#import "PCPaymentService.h"
#import "TogoAuthViewController.h"
#import "OrderManager.h"
#import "MenuManager.h"
#import "FormatterManager.h"
#import "PCCredentialService.h"
#import "RemoteDataManager.h"
#import "TransactionManager.h"
#import "FavoriteOrderHeaderView.h"
#import "FavoriteOrderSignIn.h"
#import "SignInCustomerViewController.h"
#import "FavoriteOrderGuide.h"
#import "MenuPageViewController.h"
#import <Google/Analytics.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "DeliveryAddress.h"

#define RADIANS(degrees) ((degrees * M_PI) / 180.0)

@interface RapidCollectionViewController ()

@property (strong, nonatomic) NSMutableArray *favoriteOrders;
@property (strong, nonatomic) NSArray *favoriteRestaurants;
@property (weak, nonatomic) IBOutlet UILabel *guideLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addBarButtonItem;

@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

@property (nonatomic, getter = isEditMode) BOOL editMode;

@property (strong, nonatomic) FavoriteOrderCollectionViewCell *selectedCell;

@property (strong, nonatomic) NSArray *insertedIndexes;
@property (strong, nonatomic) NSArray *updatedIndexes;

@property (strong, nonatomic) Payment *payment;
@property (strong, nonatomic) Promotion *smallCutPromotion;
@property (strong, nonatomic) IBOutlet UICollectionView *favoriteOrderCollectionView;

@property (strong, nonatomic) FavoriteOrderGuide *favoriteOrderGuide;

@end

@implementation RapidCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(favoriteOrderUpdated:)
                                                 name:FavoriteOrderUpdatedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(favoriteOrderInserted:)
                                                 name:FavoriteOrderInsertedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signedIn:)
                                                 name:SignedInNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signedOut:)
                                                 name:SignedOutNotification
                                               object:nil];
    
    [self addObserver:self
           forKeyPath:@"editMode"
              options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
              context:NULL];
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    //    PCLog(@"change %@", change);
    
    if(object == self && [keyPath isEqual:@"editMode"]){
        BOOL newValue = [change boolForKey:@"new"];
        
        if(newValue){
            // TODO: Animation
//            for(FavoriteOrderCollectionViewCell *cell in self.collectionView.visibleCells){
//                if([cell isKindOfClass:[FavoriteOrderCollectionViewCell class]]){
//                    [cell startJiggling];
//                }
//            }
        } else {
//            [self.collectionView.visibleCells  makeObjectsPerformSelector:@selector(startJiggling)];
        }
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}


- (void)didEnterBackground:(NSNotification *)notification
{
    if(self.isEditMode){
        [self editButtonTouched:nil];
    }
    
    self.dirty = YES;
}

- (void)willEnterForeground:(NSNotification *)notification
{
    if(!CRED.isSignedIn){
        [self addSignInView];
    } else {
        [self checkAndAddGuideView];
    }
    [self.collectionView reloadData];
}

- (void)signedIn:(NSNotification *)notification
{
    self.dirty = YES;
    self.favoriteOrders = nil;
    if(self.isEditMode){
        [self editButtonTouched:nil];
    }
    
    [self checkAndAddGuideView];
    
    [self.collectionView reloadData];
}

- (void)signedOut:(NSNotification *)notification
{
    self.dirty = YES;
    self.favoriteOrders = nil;
    if(self.isEditMode){
        [self editButtonTouched:nil];
    }
    
    [self.collectionView reloadData];
    
    [self addSignInView];
}

- (void)favoriteOrderUpdated:(NSNotification *)notification
{
    NSIndexPath *updatedIndexPath = [notification.userInfo objectForKey:@"IndexPath"];
    if(updatedIndexPath != nil){
        self.updatedIndexes = @[updatedIndexPath];
    }
}

- (void)favoriteOrderInserted:(NSNotification *)notification
{
    FavoriteOrder *neoFavoriteOrder = [notification.userInfo objectForKey:@"FavoriteOrder"];
    self.insertedIndexes = @[[NSIndexPath indexPathForRow:[self.favoriteOrders count]
                                                inSection:0]];
    [self.favoriteOrders addObject:neoFavoriteOrder];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemEdit
                                                                        target:self
                                                                        action:@selector(editButtonTouched:)];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didEnterBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willEnterForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];

    if(!CRED.isSignedIn){
        
        [self addSignInView];
        
    } else {
        [self checkAndAddGuideView];
        
        if((self.insertedIndexes != nil || self.updatedIndexes != nil) && ([self.favoriteOrders count] > 1)){
                if(self.insertedIndexes != nil){
                    [self.collectionView insertItemsAtIndexPaths:self.insertedIndexes];
                    self.insertedIndexes = nil;
                }
                
                if(self.updatedIndexes != nil){
                    [self.collectionView reloadItemsAtIndexPaths:self.updatedIndexes];
                    self.updatedIndexes = nil;
                }
        } else {
            if(self.view == self.favoriteOrderCollectionView){
                [self.collectionView reloadData];
            }
        }
        
        self.addBarButtonItem.enabled = CRED.isSignedIn && !self.isEditMode;
    }
    
    if(ORDER.shouldMoveToOrders){
        [self.tabBarController setSelectedIndex:3];
        UINavigationController *tab1NavigationController = (UINavigationController *)self.tabBarController.viewControllers[3];
        [tab1NavigationController popToRootViewControllerAnimated:YES];
        ORDER.shouldMoveToOrders = NO;
        
        PCLog(@"Order Type just finished %d", ORDER.order.orderType);
        if(ORDER.order.orderType == OrderTypeDelivery && ORDER.isSetTempDelivery){
            PCLog(@"Temp Delivery Addr %@", ORDER.tempDeliveryAddress.receiverName);
            PCLog(@"Temp Delivery Addr %@", ORDER.lastUsedDeliveryAddress.name);
            
            DeliveryAddress *similarAddress = nil;
            DeliveryAddress *sameAddress = nil;
            
            for(DeliveryAddress *anAddress in REMOTE.deliveryAddresses){
                DeliveryAddressSimilarity rtn = [anAddress sameWithAddresses:ORDER.tempDeliveryAddress];
                switch(rtn){
                    case DeliveryAddressSimilaritySimilar:
                        if(similarAddress == nil || !similarAddress.isDefaultAddress){
                            similarAddress = anAddress;
                        }
                        break;
                    case DeliveryAddressSimilaritySame:
                        sameAddress = anAddress;
                        break;
                    default:
                        break;
                }
                
                if(sameAddress != nil){
                    break;
                }
            }
            
            if(sameAddress != nil){
                ORDER.lastUsedDeliveryAddress = sameAddress;
                if(!sameAddress.isDefaultAddress){
                    if(ORDER.tempDeliveryAddress.needToBeUpdatedToDefault){
                        // Update to default
                        [DeliveryAddress updateDeliveryAddress:sameAddress withAddress:nil];
                    } else {
                        // Do nothing
                    }
                } else {
                    // Do nothing!
                }
            } else if(similarAddress != nil){
                ORDER.lastUsedDeliveryAddress = similarAddress;
                // Ask to update one or create one?
                UIAlertView  *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Update saved delivery address?", nil)
                                                                     message:[NSString stringWithFormat:NSLocalizedString(@"%@\nTO\n%@", nil), ORDER.lastUsedDeliveryAddress.fullAddress, ORDER.tempDeliveryAddress.fullAddress]
                                                                    delegate:self
                                                           cancelButtonTitle:NSLocalizedString(@"No", nil)
                                                           otherButtonTitles:NSLocalizedString(@"Yes", nil), NSLocalizedString(@"No, save as new", nil), nil];
                alertView.tag = 212;
                [alertView show];
                
            } else {
                if(ORDER.tempDeliveryAddress.needToBeUpdatedToDefault){
                    [DeliveryAddress newDeliveryAddressWithAddress:ORDER.tempDeliveryAddress];
                } else {
                    [UIAlertView askWithTitle:NSLocalizedString(@"Save this delivery address?", nil)
                                      message:nil //ORDER.tempDeliveryAddress.fullAddress
                                     delegate:self
                                          tag:211];
                }
            }
        }

    } else {
        [TRANS requestMyOrders]; //For refreshing local cart when existing from the ordering status
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 211: // New
            if(buttonIndex == AlertButtonIndexYES){
                [DeliveryAddress newDeliveryAddressWithAddress:ORDER.tempDeliveryAddress];
            }
            break;
        case 212: // Update to Default
            if(buttonIndex == AlertButtonIndexYES){
                [DeliveryAddress updateDeliveryAddress:ORDER.lastUsedDeliveryAddress withAddress:ORDER.tempDeliveryAddress];
            } else if(buttonIndex == 2) {
                ORDER.tempDeliveryAddress.needToBeUpdatedToDefault = NO;
                [DeliveryAddress newDeliveryAddressWithAddress:ORDER.tempDeliveryAddress];
            }
            break;
        case 213: // Update
            if(buttonIndex == AlertButtonIndexYES){
                [DeliveryAddress updateDeliveryAddress:ORDER.lastUsedDeliveryAddress withAddress:ORDER.tempDeliveryAddress];
            }
            break;
        case 301:
            if(buttonIndex == AlertButtonIndexYES){
                FavoriteOrder *favoriteOrder = [self.favoriteOrders objectAtIndex:self.selectedIndexPath.row];
                [self pushMenuViewControllerWithFavoriteOrder:favoriteOrder];
            }
            break;
    }
}

- (void)addSignInView
{
    FavoriteOrderSignIn *view = [FavoriteOrderSignIn view];
    [view.signInButton addTarget:self
                          action:@selector(signInButtonTouched:)
                forControlEvents:UIControlEventTouchUpInside];
    self.view = view;
}

- (void)addGuideView
{
    self.view = self.favoriteOrderGuide;
}

- (void)addButtonTouched:(id)sender
{
    [self performSegueWithIdentifier:@"neoRapidOrder"
                              sender:sender];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidEnterBackgroundNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    self.navigationItem.leftBarButtonItem.enabled = ([self.favoriteOrders count] > 0 && CRED.isSignedIn);
    
    self.addBarButtonItem.enabled = CRED.isSignedIn && !self.isEditMode;
    
    switch(section){
        case 0:
            return [self.favoriteOrders count] + (self.isEditMode ? 0 : 1);
            break;
        case 1:
            return [self.favoriteRestaurants count];
            break;
    }
    return 0;
}

- (void)checkAndAddGuideView
{
    if([self.favoriteOrders count] == 0){
        [self editButtonTouched:nil];
        [self addGuideView];
    } else {
        self.view = self.favoriteOrderCollectionView;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch(indexPath.section){
        case 0:{
            if(indexPath.row >= [self.favoriteOrders count]){
                UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PlusCollectionViewCell"
                                                                                            forIndexPath:indexPath];
                return cell;
            }
            FavoriteOrderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FavoriteOrderCollectionViewCell"
                                                                                              forIndexPath:indexPath];
            FavoriteOrder *favoriteOrder = [self.favoriteOrders objectAtIndex:indexPath.row];
            
            if(favoriteOrder.imageURL != nil){
                [cell.backImageView sd_setImageWithURL:favoriteOrder.imageURL];
            } else {
                [cell.backImageView sd_setImageWithURL:favoriteOrder.merchant.imageURL];
            }
            cell.nameLabel.text = favoriteOrder.title;
            cell.delegate = self;
            
            cell.removeButton.hidden = !self.isEditMode;
            
            return cell;
        }
            break;
        default:{
            FavoriteRestCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FavoriteRestCollectionViewCell"
                                                                                              forIndexPath:indexPath];
            Merchant *merchant = [self.favoriteRestaurants objectAtIndex:indexPath.row];
            [cell.backImageView sd_setImageWithURL:merchant.logoURL];
            cell.nameLabel.text = merchant.name;
            return cell;
        }
            break;
    }
}

- (void)cellDidTouchRemoveButton:(FavoriteOrderCollectionViewCell *)cell
{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
    FavoriteOrder *favoriteOrder = [self.favoriteOrders objectAtIndex:indexPath.row];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Are you sure you want to delete this? %@", nil), favoriteOrder.title]
                                                             delegate:self
                                                    cancelButtonTitle:@"NO"
                                               destructiveButtonTitle:@"YES"
                                                    otherButtonTitles:nil];
    actionSheet.tag = 101;
    self.selectedCell = cell;
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(actionSheet.tag == 101){
        if(actionSheet.destructiveButtonIndex == buttonIndex){
            [self requestRemoveFavoriteOrder];
        }
    } else if(actionSheet.tag == 102){
        if(actionSheet.destructiveButtonIndex == buttonIndex){
            [self requestOrderFavoriteOrder];
        } else if(actionSheet.cancelButtonIndex == buttonIndex){
            NSLog(@"Just cancel");
        } else {
            switch(buttonIndex){
                case 1:{
                    FavoriteOrder *favoriteOrder = [self.favoriteOrders objectAtIndex:self.selectedIndexPath.row];
                    [self pushMenuViewControllerWithFavoriteOrder:favoriteOrder];
                }
                    break;
            }
        }
    }
}

- (void)pushMenuViewControllerWithFavoriteOrder:(FavoriteOrder *)favoriteOrder
{
    
    MenuListType neoMenuListType = MenuListTypeNone;
    
    
    switch (favoriteOrder.orderType) {
        case OrderTypeTakeout:
            neoMenuListType = MenuListTypeTakeout;
            break;
        case OrderTypeDelivery:
            neoMenuListType = MenuListTypeDelivery;
            break;
        default:
            neoMenuListType = MenuListTypeDinein;
            break;
    }
    
    BOOL isMenuReset = [MENUPAN resetGraphWithMerchant:favoriteOrder.merchant
                                              withType:neoMenuListType];
    
    CartType cartType = CartTypeUnknown;
    
    switch (favoriteOrder.orderType) {
        case OrderTypeTakeout:
            cartType = CartTypeTakeout;
            break;
        case OrderTypeDelivery:
            cartType = CartTypeDelivery;
            break;
        case OrderTypePickup:
            if([favoriteOrder.tableLabel length] > 0){
                cartType = CartTypePickupServiced;
            } else {
                cartType = CartTypePickup;
            }
            break;
        default:
            
            break;
    }
    
    if(isMenuReset){
        [MENUPAN requestMenusWithSuccess:^(id response) {
            [self pushMenuPageViewController:favoriteOrder cartType:cartType];
        } andFail:^(NSUInteger errorCode) {
            PCError(@"Can't continue to Modify and Order for requestMenusWithSuccess");
            [UIAlertView alertWithTitle:@"Oops! Unexpected Error"
                                message:@"Sorry, something seems a little buggy! Please try exiting the app and restarting. Please don't hesitate to reach out to us if the problem persists."];
        }];
        
    } else {
        [self pushMenuPageViewController:favoriteOrder cartType:cartType];
    }
}

- (void)pushMenuPageViewController:(FavoriteOrder *)favoriteOrder cartType:(CartType)cartType
{
    MenuPageViewController *viewController = [MenuPageViewController viewControllerFromNib];
    viewController.forceOpenOrderViewController = YES;
    viewController.initialCartType = cartType;
    [ORDER resetGraphWithMerchant:favoriteOrder.merchant];
    viewController.hidesBottomBarWhenPushed = YES;
    
    ORDER.cart = [[Cart alloc] initWithFavroiteOrder:favoriteOrder menuPan:MENUPAN.wholeSerialMenuList];
    
    ORDER.tempDeliveryAddress = [[Addresses alloc] init];
    ORDER.tempDeliveryAddress.address1 = favoriteOrder.address1;
    ORDER.tempDeliveryAddress.address2 = favoriteOrder.address2;
    ORDER.tempDeliveryAddress.zip = favoriteOrder.zip;
    ORDER.tempDeliveryAddress.city = favoriteOrder.city;
    ORDER.tempDeliveryAddress.state = favoriteOrder.state;
    ORDER.tempDeliveryAddress.receiverName = favoriteOrder.receiverName;
    ORDER.tempDeliveryAddress.phoneNumber = favoriteOrder.phoneNumber;
    ORDER.tempDeliveryAddress.coordinate = favoriteOrder.location.coordinate;
    
    ORDER.tempCustomerRequest = favoriteOrder.customerRequest;
    ORDER.tempAfterMinute = favoriteOrder.pickupAfter;
    ORDER.flagNumber = favoriteOrder.tableLabel;
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)requestRemoveFavoriteOrder
{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:(UICollectionViewCell *)self.selectedCell];
    
    FavoriteOrder *favoriteOrder = [self.favoriteOrders objectAtIndex:indexPath.row];
    
    
    RequestResult result = RRFail;
    result = [PAY requestDeleteRapidReOrder:favoriteOrder
                            completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              [self.favoriteOrders removeObjectAtIndex:indexPath.row];
                              [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
                              if([self.favoriteOrders count] == 0){
                                  [self editButtonTouched:nil];
                              }
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while removing RapidOrder", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)signInButtonTouched:(id)sender
{
    [self showSignIn:NO];
}

- (void)showSignIn:(BOOL)showGuide
{
    SignInCustomerViewController *viewController = [SignInCustomerViewController viewControllerFromNib];
    viewController.showGuide = showGuide;
    [self.tabBarController presentViewControllerInNavigation:viewController
                                                    animated:YES
                                                  completion:^{
                                                  }];
}

- (void)scrollToTop
{
    [self.collectionView setContentOffset:CGPointZero
                                 animated:YES];
}

- (void)requestOrderFavoriteOrder
{
    RequestResult result = RRFail;

    FavoriteOrder *favoriteOrder = [self.favoriteOrders objectAtIndex:self.selectedIndexPath.row];
    
    
    result = [PAY requestOrderFavoriteOrder:favoriteOrder.favoriteOrderNo
                                    payment:self.payment
                                deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                           appliedDiscounts:[self.payment appliedDiscountsDictsWithSmallCutAmount:self.smallCutPromotion.appliedDiscountAmount]
                             smallCutAmount:self.smallCutPromotion.appliedDiscountAmount
                            completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                                  if(CRED.isSignedIn){
                                      REMOTE.ordersDirty = YES;
                                  } else {
                                      [ORDER.merchant save];
                                      [ORDER.order save];
                                  }
                                  
                                  
    #if FBAPPEVENT_ENABLED
                                  [FBSDKAppEvents logPurchase:(favoriteOrder.grandTotal / 100.0f)
                                                     currency:@"USD"
                                                   parameters:@{ FBSDKAppEventParameterNameCurrency    : @"USD",
                                                                 FBSDKAppEventParameterNameContentType : @"food(Rapid)",
                                                                 @"UUID" : (CRED.udid ? CRED.udid : @"Unknown")}];
    #endif
                                  
                                  
    #if FIREBASE_ENABLED
                                  id tracker = [[GAI sharedInstance] defaultTracker];
                                  
                                  
                                  
                                  [tracker send:[[GAIDictionaryBuilder createTransactionWithId:(CRED.udid ? CRED.udid : @"Unknown")             // (NSString) Transaction ID
                                                                                   affiliation:@"Food(Rapid)"         // (NSString) Affiliation
                                                                                       revenue:@(favoriteOrder.grandTotal / 100.0f)                  // (NSNumber) Order revenue (including tax and shipping)
                                                                                           tax:@(favoriteOrder.taxes)                  // (NSNumber) Tax
                                                                                      shipping:@(favoriteOrder.deliveryFee)                  // (NSNumber) Shipping
                                                                                  currencyCode:@"USD"] build]];        // (NSString) Currency code
                                  
                                  
                                  //    [tracker send:[[GAIDictionaryBuilder createItemWithTransactionId:@"0_123456"         // (NSString) Transaction ID
                                  //                                                                name:@"Space Expansion"  // (NSString) Product Name
                                  //                                                                 sku:@"L_789"            // (NSString) Product SKU
                                  //                                                            category:@"Game expansions"  // (NSString) Product category
                                  //                                                               price:@1.9F               // (NSNumber) Product price
                                  //                                                            quantity:@1                  // (NSInteger) Product quantity
                                  //                                                        currencyCode:@"USD"] build]];    // (NSString) Currency code
    #endif
                                  
                                  
                                  
                                  TRANS.dirty = YES;
                                  [self moveOrdersTap];
                          }
                              break;
                          case ResponseErrorMenuItemChanged:
                              
                              [UIAlertView askWithTitle:NSLocalizedString(@"Menu Updated", nil)
                                                message:NSLocalizedString(@"The restaurant's menu has been updated. Would you try to Modify and Order?", nil)
                                               delegate:self
                                                    tag:301];
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  NSString *message = [[response objectForKey:@"message"] objectForKey:@"message"];
                                  if([message isKindOfClass:[NSString class]]){
                                      [UIAlertView alertWithTitle:UNDEFINED_ALERT_TITLE
                                                          message:message
                                                         delegate:self
                                                              tag:9001];
                                  } else {
                                      [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while ordering favorite order", nil)];
                                  }
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)moveOrdersTap
{
    [self.tabBarController setSelectedIndex:3];
    UINavigationController *tab1NavigationController = (UINavigationController *)self.tabBarController.viewControllers[3];
    [tab1NavigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)editButtonTouched:(id)sender
{
    if(!self.isEditMode && sender != nil){
        self.editMode = YES;
        self.navigationItem.leftBarButtonItem.button.buttonTitle = @"Done";
//        self.guideLabel.text = @"To delete item, touch '-' button or To modify select item";
        self.addBarButtonItem.enabled = NO;
    } else {
        self.editMode = NO;
        self.navigationItem.leftBarButtonItem.button.buttonTitle = @"Edit";
//        self.guideLabel.text = @"RushOrder let you repeat your favorite past orders. Start your RapidOrder with tap '+' button";
        self.addBarButtonItem.enabled = YES;
    }
    
    [self.collectionView reloadData];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    
    FavoriteOrder *favoriteOrder = nil;
    
    if(indexPath.row < ([self.favoriteOrders count])){
        
        favoriteOrder = [self.favoriteOrders objectAtIndex:indexPath.row];
        
        if(self.isEditMode){
            [self performSegueWithIdentifier:@"rapidDetailView"
                                      sender:favoriteOrder];
        } else {
            
            FavoriteOrder *favoriteOrder = [self.favoriteOrders objectAtIndex:self.selectedIndexPath.row];

            
            if(!favoriteOrder.merchant.isOpenHour){
                [UIAlertView alertWithTitle:NSLocalizedString(@"RapidOrder Unavailable", nil)
                                    message:NSLocalizedString(@"The restaurant you are trying to place a RapidOrder from is currently closed. Please try again during the restaurant's normal business hours.", nil)
                          cancleButtonTitle:NSLocalizedString(@"Got it!", nil)];
                return; // !!!:Stop Ordering
            }
            
            if(favoriteOrder.orderType == OrderTypeDelivery){
                if(!favoriteOrder.merchant.isDeliveryHour){
                    
                    NSString *hourString = [favoriteOrder.merchant nextDeliveryAvailableTime];
                    [UIAlertView alertWithTitle:NSLocalizedString(@"Delivery is Currently Unavailable", nil)
                                        message:[NSString stringWithFormat:NSLocalizedString(@"Next Available Time:\n%@", nil), [hourString length] > 0 ? hourString : NSLocalizedString(@"N/A", nil)]];
                    return; // !!!:Stop Ordering
                }
            }
            
            self.payment = [[Payment alloc] init];
            
            self.payment.merchantNo = favoriteOrder.merchantNo;
            self.payment.tipAmount = favoriteOrder.tips;
            self.payment.subTotal = favoriteOrder.subTotal;
            self.payment.subTotalTaxable = favoriteOrder.subTotalTaxable;
            
            // TODO: If tax rate different re calculate
            self.payment.taxAmount = roundf(self.payment.subTotalTaxable * (favoriteOrder.merchant.taxRate / 100.0f));
            if(favoriteOrder.taxes != self.payment.taxAmount){
                PCError(@"Tax has been changed in RapidOrder %lld, from %ld to %ld", favoriteOrder.favoriteOrderNo, favoriteOrder.taxes, self.payment.taxAmount);
            }
            self.payment.orderAmount = self.payment.subTotal + self.payment.taxAmount;
            self.payment.payAmount = self.payment.orderAmount;
            self.payment.currency = FORMATTER.currencyFormatter.currencyCode;
            self.payment.payDate = [NSDate date];

            
            if(favoriteOrder.merchant.promotion != nil){
                if(CRED.isSignedIn){
                    for(Promotion *promotion in [favoriteOrder.merchant promotionsOnOrderType:favoriteOrder.orderType]){
                        [self.payment applyPromotion:promotion];
                    }
                }
            }
            
            if(favoriteOrder.orderType == OrderTypeDelivery){
                [self.payment applyRoServiceFee:favoriteOrder.merchant];
                
                PCCurrency deliveryFeeAmount = [favoriteOrder.merchant deliveryFeeAt:favoriteOrder.location andSubtotal:self.payment.subTotal];
                
                if(deliveryFeeAmount != NSNotFound){
                    if(self.payment.total >= favoriteOrder.merchant.deliveryBigOrderAmount && favoriteOrder.merchant.deliveryBigOrderAmount > 0){
                        self.payment.deliveryFeeAmount = favoriteOrder.merchant.deliveryFeeAmountForBigOrder;
                    } else {
                        self.payment.deliveryFeeAmount = deliveryFeeAmount;
                    }
                    
                } else {
                    self.payment.deliveryFeeAmount = 0;
                    
                    switch(favoriteOrder.merchant.deliveryFeePlan){
                        case DeliveryFeePlanFlat:
                        case DeliveryFeePlanRadius:
                            [UIAlertView alertWithTitle:NSLocalizedString(@"Delivery Distance Limit", nil)
                                                message:NSLocalizedString(@"Sorry, we do not deliver to this location.", nil)];
                            break;
                        case DeliveryFeePlanZone:
                            [UIAlertView alertWithTitle:NSLocalizedString(@"Outside of Delivery Area", nil)
                                                message:NSLocalizedString(@"Sorry, we do not deliver to this location.", nil)];
                            break;
                        default:
                            [UIAlertView alertWithTitle:NSLocalizedString(@"Delivery Area Error", nil)
                                                message:NSLocalizedString(@"Sorry, we cannot decide this location is in the delivery area.", nil)];
                            break;
                    }
                    return; // !!!:Stop Ordering

                }
            }
            
            self.payment.payAmount
            = self.payment.tipAmount
            + self.payment.orderAmount
            + self.payment.deliveryFeeAmount
            + self.payment.roServiceFeeAmount
            - self.payment.discountAmount;
            
            self.smallCutPromotion = nil;
            
            if(0 < self.payment.payAmount && self.payment.payAmount < 50){
                self.smallCutPromotion = [Promotion smallCutPromotion];
                self.smallCutPromotion.discountAmount = self.payment.payAmount;
                self.smallCutPromotion.appliedDiscountAmount = self.payment.payAmount;
            }
            
            self.payment.payAmount
            = self.payment.tipAmount
            + self.payment.orderAmount
            + self.payment.deliveryFeeAmount
            + self.payment.roServiceFeeAmount
            - self.payment.discountAmount
            - self.smallCutPromotion.appliedDiscountAmount;
            

            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"Are you sure you want to proceed?\nAmount:%@\n%@", self.payment.payAmountString, favoriteOrder.card.formattedCardNumber]
                                                                     delegate:self
                                                            cancelButtonTitle:@"Cancel"
                                                       destructiveButtonTitle:@"Pay Now"
                                                            otherButtonTitles:@"Modify and Order", nil]; //@"Modify and Order"

            actionSheet.tag = 102;
            [actionSheet showFromTabBar:self.tabBarController.tabBar];
        }
    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat sideLength = (collectionView.bounds.size.width - 2) / 3;
    
    return CGSizeMake(sideLength, sideLength);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    FavoriteOrderHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                             withReuseIdentifier:@"collectionHeaderView"
                                                                                    forIndexPath:indexPath];
    
//    [headerView setNeedsUpdateConstraints];
//    [headerView updateConstraintsIfNeeded];
//    [headerView setNeedsLayout];
//    [headerView layoutIfNeeded];
//    
//    headerView.guideLabel.preferredMaxLayoutWidth = headerView.guideLabel.width;
//    
//    headerView.height = [headerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    return headerView;
}

//- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.destinationViewController isKindOfClass:[TogoAuthViewController class]]){
        TogoAuthViewController *viewController = (TogoAuthViewController *)segue.destinationViewController;
        
        FavoriteOrder *favoriteOrder = sender;
        
        Merchant *merchant = favoriteOrder.merchant;
        
        [ORDER resetGraphWithMerchant:merchant];
        [MENUPAN resetGraphWithMerchant:merchant];
        viewController.payment = nil;
        viewController.rapidRO = YES;
        
        if(favoriteOrder != nil){
            viewController.favoriteOrder = favoriteOrder;
            viewController.indexPath = self.selectedIndexPath;
        }
        
    } else if([segue.destinationViewController isKindOfClass:[UINavigationController class]]){
        UINavigationController *navi = (UINavigationController *)segue.destinationViewController;
        PaymentHistoryViewController *viewController = (PaymentHistoryViewController *)navi.rootViewController;
        viewController.rapidRO = YES;
    }
}

- (NSMutableArray *)favoriteOrders
{
    if(_favoriteOrders == nil || self.isDirty){
        if(_favoriteOrders == nil){
           _favoriteOrders = [NSMutableArray array];
        }
        [self getFavoriteOrders];
    }
    return _favoriteOrders;
}

- (void)getFavoriteOrders
{
    if(self.isProgressing){
        return;
    }
    
    self.progressing = YES;
    
    RequestResult result = RRFail;
    result = [PAY requestGetRapidOrdersWithCompletionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              self.favoriteOrders = (NSMutableArray *)response;
                              
                              [self checkAndAddGuideView];
                              
                              [self.collectionView reloadData];
                              
                              self.dirty = NO;
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting RapidOrder list", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  
                  [SVProgressHUD dismiss];
                  self.progressing = NO;
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            self.progressing = NO;
            break;
        default:
            self.progressing = NO;
            break;
    }
}

- (FavoriteOrderGuide *)favoriteOrderGuide
{
    if(_favoriteOrderGuide == nil){
        _favoriteOrderGuide = [FavoriteOrderGuide view];
        
        [_favoriteOrderGuide.addButton addTarget:self
                                          action:@selector(addButtonTouched:)
                                forControlEvents:UIControlEventTouchUpInside];
    }
    return _favoriteOrderGuide;
}

@end
