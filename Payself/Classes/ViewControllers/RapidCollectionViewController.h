//
//  RapidCollectionViewController.h
//  RushOrder
//
//  Created by Conan on 8/12/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavoriteOrderCollectionViewCell.h"


@interface RapidCollectionViewController : UICollectionViewController
<
UICollectionViewDataSource,
UICollectionViewDelegate,
UIActionSheetDelegate,
FavoriteOrderCollectionViewCellDelegate,
UICollectionViewDelegateFlowLayout,
UIAlertViewDelegate
>

@property (nonatomic, getter = isAddItemSimul) BOOL addItemSimul;

@property (nonatomic, getter = isDirty) BOOL dirty;
@property (nonatomic, getter = isProgressing) BOOL progressing;
@end
