//
//  ReceiptViewController.m
//  RushOrder
//
//  Created by Conan on 2/22/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "ReceiptViewController.h"
#import "PCCredentialService.h"
#import "PCPaymentService.h"
#import "OrderManager.h"
#import "ReceiptItemCell.h"
#import "ReceiptOptionCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MerchantViewController.h"

#define LINE_SPAN 10.0f
#define LINE_SPAN_W 15.0f

//#define ENABLE_PROMOTION_NOTICE_JOIN    1

@interface ReceiptViewController ()

//@property (strong, nonatomic) NSNumberFormatter* currencyFormatter;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NPStretchableButton *doneButton;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet LogoCircleImageView *logoImageView;

@property (weak, nonatomic) IBOutlet TouchableLabel *merchantNameLabel;
@property (weak, nonatomic) IBOutlet TouchableLabel *merchantDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxesTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxesLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryFeeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryFeeLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceFeeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceFeeLabel;

@property (weak, nonatomic) IBOutlet UILabel *tipsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;
@property (weak, nonatomic) IBOutlet UILabel *grandTotalTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderNoLabel;

@property (weak, nonatomic) IBOutlet UILabel *tipsLabel;
@property (weak, nonatomic) IBOutlet UILabel *grandTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *cardTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *accNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointBalanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointBalaceTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *paidCreditTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *paidCreditLabel;
@property (weak, nonatomic) IBOutlet UILabel *paidTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *paidLabel;
@property (weak, nonatomic) IBOutlet UILabel *rewardTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *rewardLabel;
@property (weak, nonatomic) IBOutlet UILabel *tableTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *tableNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *payDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *thankyouLabel;

@property (strong, nonatomic) NSString *smsVerificationToken;
@property (strong, nonatomic) IBOutlet UIView *creditSuggestionView;

@property (strong, nonatomic) IBOutlet UIView *receiptHeaderView;


@property (weak, nonatomic) IBOutlet UILabel *promotionName;
@property (weak, nonatomic) IBOutlet UIView *promotionView;
@property (weak, nonatomic) IBOutlet UIView *promotionCardView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deliveryFeeTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discountTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tipTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *serviceFeeTopSpaceConstraint;

@property (nonatomic, getter = isRemovedReceipt) BOOL removedReceipt;

@property (nonatomic) NSInteger verificationTimeout;

@property (nonatomic, getter = isDirty) BOOL dirty;

@property (nonatomic) BOOL didAppeared;
@property (weak, nonatomic) IBOutlet UIImageView *refundIcon;
@property (weak, nonatomic) IBOutlet UIImageView *iconTest;
@property (weak, nonatomic) IBOutlet UIView *receiptSummaryView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *doneButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *doneButtonTopConstraint;
@end

@implementation ReceiptViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.backToRoot = YES;
        self.dirty = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self showAllLabels:NO];
    
    if(self.order == nil) self.order = ORDER.order;
    
    if(self.isBackToRoot){ //In payment porgress
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Complete", nil)
                                                                                 target:self
                                                                                 action:@selector(doneButtonTouched:)];
        
        
        self.doneButton.hidden = NO;
        
        self.doneButtonHeightConstraint.constant = 44.0f;
        self.doneButtonTopConstraint.constant = 16.0f;
        
    } else { // Recent activity
        self.doneButton.hidden = YES;
        self.doneButtonHeightConstraint.constant = 0.0f;
        self.doneButtonTopConstraint.constant = 0.0f;

        self.thankyouLabel.text = NSLocalizedString(@"Thank you for dining with us", nil);
        
    }
    
    // !!!: don't move below code to the top. this code should be placed under fillPaymentIfNil method
    self.title = NSLocalizedString(@"Receipt", nil);
    
//    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Email", nil)
//                                                                              target:self
//                                                                              action:@selector(mailMeButtonTouched:)];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonEmail
                                                                         target:self
                                                                         action:@selector(mailMeButtonTouched:)];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    self.logoImageView.target = self;
    
    self.merchantNameLabel.target = self;
    self.merchantDescLabel.target = self;
    [self requestReceipt];
}

- (void)showAllLabels:(BOOL)isShow
{
    for(UIView *subview in self.footerView.subviews){
        if([subview isKindOfClass:[UILabel class]]){
            subview.hidden = !isShow;
        }
    }
    
    for(UIView *subview in self.receiptSummaryView.subviews){
        if([subview isKindOfClass:[UILabel class]]){
            if(subview.tag != 1)
                subview.hidden = !isShow;
        }
    }
    
    for(UIView *subview in self.receiptHeaderView.subviews){
        if([subview isKindOfClass:[UILabel class]]){
            subview.hidden = !isShow;
        }
    }
    
    self.logoImageView.hidden = !isShow;
}


- (void)circleImageViewActionTriggerred:(id)sender
{
    //    [ORDER resetGraphWithMerchant:self.merchant];
    MerchantViewController *viewController = [UIStoryboard viewController:@"MerchantViewController"
                                                                     from:@"Restaurants"];
    [APP.tabBarController presentViewControllerInNavigation:viewController
                                                   animated:YES
                                                 completion:^{
                                                     
                                                 }];
}

- (void)touchableLabelActionTriggerred:(id)sender
{
    [self circleImageViewActionTriggerred:sender];
}

- (void)requestReceipt
{
    RequestResult result = RRFail;
    result = [PAY requestGetReceiptWithPaymentNo:self.payment.paymentNo
                               completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              [self showAllLabels:YES];
                              
                              if(response.isNSArray){
                                  if([((NSArray *)response) count] > 0){
                                      NSDictionary *receiptDict = [((NSArray *)response) lastObject];
                                      self.receipt = [[Receipt alloc] initWithDictionary:receiptDict];
                                  }
                              } else {
                                  self.receipt = [[Receipt alloc] initWithDictionary:response.data];
                              }
                              
                              if(self.receipt == nil){
                                  if(self.didAppeared){
                                      [self.navigationController popViewControllerAnimated:YES];
                                      [UIAlertView alertWithTitle:NSLocalizedString(@"Removed Receipt", nil)
                                                          message:NSLocalizedString(@"Receipt you want to access looks removed one", nil)];
                                  } else {
                                      self.removedReceipt = YES;
                                  }
                              } else {
                                  [self fillReceipt];
                              }
                              break;
                          case ResponseErrorNoReceipt:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Can Not Find Receipt", nil)
                                                  message:NSLocalizedString(@"You might not paid for this order yet, or there is something wrong", nil)];
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting a receipt", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)fillReceiptIfNil
{
    if(!CRED.isSignedIn){
        if(self.receipt == nil){
            NSArray *receiptList = [Receipt listWithCondition:[NSString stringWithFormat:@"where orderNo = %lld", self.order.orderNo]];
            if([receiptList count] > 0){
                self.receipt = [receiptList objectAtIndex:0];
                
                NSMutableArray *lineItems = [LineItem listWithCondition:[NSString stringWithFormat:@"where receiptNo = %lld", self.receipt.receiptNo]];
                self.receipt.lineItems = lineItems;
                
                self.payment = [Payment paymentWithPaymentNo:self.receipt.paymentNo];
                self.receipt.payment = self.payment;
                
                lineItems = [LineItem listWithCondition:[NSString stringWithFormat:@"where paymentNo = %lld", self.receipt.payment.paymentNo]];
                self.receipt.payment.lineItems = lineItems;
                [self.receipt.payment summation];
                
                [self.receipt summation];
            } else {
                self.removedReceipt = YES;
            }
        }
    }
}

- (void)mailMeButtonTouched:(id)sender
{
    NSString *mailAddress = [NSUserDefaults standardUserDefaults].emailAddress;
    if([mailAddress length] > 0){
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Do you want to send receipt to %@", nil), mailAddress]
                                                                 delegate:self
                                                        cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                   destructiveButtonTitle:NSLocalizedString(@"Send", nil)
                                                        otherButtonTitles:NSLocalizedString(@"Change email address", nil), nil];
        actionSheet.tag = 101;
        [actionSheet showInView:self.view];
        
    } else {
        MailMeViewController *viewController = [MailMeViewController viewControllerFromNib];
        viewController.delegate = self;
        
        [self presentViewControllerInNavigation:viewController
                                       animated:YES completion:^{
                                           
                                       }];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(actionSheet.tag){
        case 101:
        {
            if(actionSheet.destructiveButtonIndex == buttonIndex){
                
                [self requestMailMe:self.receipt
                             toMail:[NSUserDefaults standardUserDefaults].emailAddress
                        autoMailing:NO
                       isSavedEmail:YES];
                
            } else if(buttonIndex == 1){
                MailMeViewController *viewController = [MailMeViewController viewControllerFromNib];
                viewController.delegate = self;
                [self presentViewControllerInNavigation:viewController
                                               animated:YES completion:^{
                                                   
                                               }];
            }
        }
            break;
    }
}

- (IBAction)memberJoinCancelButtonTouched:(id)sender
{
    [self.creditSuggestionView removeFromSuperview];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(self.isDirty){
        [self fillReceipt];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(self.isRemovedReceipt){
        [self.navigationController popViewControllerAnimated:YES];
        [UIAlertView alertWithTitle:NSLocalizedString(@"Removed Receipt", nil)
                            message:NSLocalizedString(@"Receipt you want to access was removed when App was removed", nil)];
    }
    
    self.didAppeared = YES;
}

- (void)fillReceipt
{
    if(self.receipt == nil){
        return;
    }
    
    self.dirty = NO;
    
    self.merchantNameLabel.text = self.receipt.merchantName;
    self.merchantDescLabel.text = self.receipt.merchantDesc;
    
    self.refundIcon.hidden = (self.receipt.payment.status != PaymentStatusRefunded);
    
    NSURL *imgURL = [NSURL URLWithString:self.receipt.merchantLogoURLString];
    if(imgURL == nil) imgURL = [NSURL URLWithString:self.receipt.merchantImageURLString];
    
    [self.logoImageView sd_setImageWithURL:imgURL
                       placeholderImage:[UIImage imageNamed:@"placeholder_logo"]
                                options:0
                              completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                              }];
    
    self.orderNoLabel.text = [NSString stringWithFormat:@"%lld",self.receipt.payment.paymentNo];
    if(self.receipt.tableNumber == nil){
        
        self.tableTitleLabel.text = NSLocalizedString(@"Ticket No.", nil);
        self.tableNumberLabel.text = [NSString stringWithFormat:@"%@",self.receipt.ticketNumber];
    } else {
        
        self.tableTitleLabel.text = NSLocalizedString(@"Table No.", nil);
        self.tableNumberLabel.text = self.receipt.tableNumber;
    }
    
    
    self.payDateLabel.text = self.receipt.payment.payDate.secondsString;
//    self.order.tips = self.receipt.payment.tipAmount; //Temp for calculating grand total.
    
    self.tableView.tableHeaderView = nil;
    self.receiptHeaderView.height = [self.receiptHeaderView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    [self.receiptHeaderView updateConstraints];
    self.tableView.tableHeaderView = self.receiptHeaderView;
    
    self.subTotalLabel.text = self.receipt.subTotalString;
    self.taxesLabel.text = self.receipt.taxNroServiceFeeAmountString;
    
    if(self.receipt.payment.roServiceFeeAmount > 0){
        self.taxesTitleLabel.text = NSLocalizedString(@"Tax & Fees", nil);
    } else {
        self.taxesTitleLabel.text = NSLocalizedString(@"Tax", nil);
    }
    
    if(self.receipt.payment.deliveryFeeAmount > 0 || self.order.orderType == OrderTypeDelivery){
        self.deliveryFeeLabel.text = self.receipt.payment.deliveryFeeAmountString;
        self.deliveryFeeTitleLabel.text = NSLocalizedString(@"Delivery", nil);
        self.deliveryFeeTopSpaceConstraint.constant = 5.0f;
    } else {
        self.deliveryFeeLabel.text = nil;
        self.deliveryFeeTitleLabel.text = nil;
        self.deliveryFeeTopSpaceConstraint.constant = 0.0f;
    }
    
    // Commented Out - Merge Service Fee + Tax in one line
//    if(self.receipt.payment.roServiceFeeAmount > 0){
//        self.serviceFeeLabel.text = self.receipt.payment.roServiceFeeAmountString;
//        self.serviceFeeTitleLabel.text = NSLocalizedString(@"Service Fee", nil);
//        self.serviceFeeTopSpaceConstraint.constant = 5.0f;
//    } else {
//        self.serviceFeeLabel.text = nil;
//        self.serviceFeeTitleLabel.text = nil;
//        self.serviceFeeTopSpaceConstraint.constant = 0.0f;
//    }
    
        self.serviceFeeLabel.text = nil; // Instead hide
        self.serviceFeeTitleLabel.text = nil; // Instead hide
        self.serviceFeeTopSpaceConstraint.constant = 0.0f; // Instead hide

    
    if(self.receipt.payment.tipAmount > 0){
        self.tipsLabel.text = self.receipt.payment.tipAmountString;
        self.tipsTitleLabel.text = NSLocalizedString(@"Tip", nil);
        self.tipTopSpaceConstraint.constant = 5.0f;
    } else {
        self.tipsLabel.text = nil;
        self.tipsTitleLabel.text = nil;
        self.tipTopSpaceConstraint.constant = 0.0f;
    }
    
    
    
    if(self.receipt.payment.discountAmount > 0){
        self.discountLabel.text = self.receipt.payment.discountAmountString;
        self.discountTitleLabel.text = NSLocalizedString(@"Discount", nil);
        self.discountTopSpaceConstraint.constant = 5.0f;
    } else {
        self.discountLabel.text = nil;
        self.discountTitleLabel.text = nil;
        self.discountTopSpaceConstraint.constant = 0.0f;
    }
    
    self.grandTotalLabel.text = self.receipt.grandTotalString;
    
    if(self.receipt.payment.creditAmount > 0){
        self.paidCreditLabel.hidden = NO;
        self.paidCreditTitleLabel.hidden = NO;
//        self.paidCreditLabel.text = [NSString stringWithFormat:@"%@ Oranges (%@)",
//                                     self.receipt.payment.creditAmountString,
//                                     self.receipt.payment.creditConvertedAmountString];
        
        self.paidCreditLabel.text = self.receipt.payment.creditConvertedAmountString;
    } else {
        self.paidCreditLabel.hidden = YES;
        self.paidCreditTitleLabel.hidden = YES;
    }
    self.paidLabel.text = [[NSNumber numberWithCurrency:self.receipt.payment.netAmount] currencyString];
    
    
    self.receipt.payment.method = PaymentMethodPayself;
    self.cardTypeLabel.text = self.receipt.payment.cardType;
    self.accNoLabel.text = [self.receipt.payment formattedCardNumber];
    
    if(self.receipt.payment.rewardedAmount > 0){
        self.rewardLabel.hidden = NO;
        self.rewardTitleLabel.hidden = NO;
        self.rewardLabel.text = [[NSNumber numberWithCurrency:self.receipt.payment.rewardedAmount] pointString];
    } else {
        self.rewardLabel.hidden = YES;
        self.rewardTitleLabel.hidden = YES;
    }
    
    self.pointBalanceLabel.text = self.receipt.payment.pinPointBalanceString;
    
#if ENABLE_PROMOTION_NOTICE_JOIN
    if((self.receipt.payment.creditPolicyNo > 0 || self.receipt.payment.globalCreditPolicyNo > 0) && !ORDER.merchant.isTestMode){
        
        if(CRED.customerInfo != nil){
            
        } else {
            
            if(self.isBackToRoot){
                
                NSMutableString *promotionName = [NSMutableString string];
                
                if(self.receipt.payment.creditPolicyName != nil) [promotionName appendString:self.receipt.payment.creditPolicyName];
                if([promotionName length] > 0) [promotionName appendString:@"\n"];
                if(self.receipt.payment.globalCreditPolicyName != nil) [promotionName appendString:self.receipt.payment.globalCreditPolicyName];
                
                self.promotionName.text = promotionName;
                [self.promotionName sizeToHeightFit];
                
                self.promotionView.height = self.promotionName.height + (self.promotionName.y * 2);
                self.promotionCardView.y = CGRectGetMaxY(self.promotionView.frame) + 8.0f;
                
                
                [self.view addSubview:self.creditSuggestionView];
                
                
            } else {
                
            }
            
        }
        
    } else {
        
    }
#endif
    

    self.tableView.tableFooterView = nil;
    
    [self.footerView updateConstraints];
//    [self.footerView setNeedsUpdateConstraints];
    
    self.footerView.height = [self.footerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    self.tableView.tableFooterView = self.footerView;
    
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneButtonTouched:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)viewDidUnload {
    [self setDoneButton:nil];
    
    self.merchantNameLabel = nil;
    self.merchantDescLabel = nil;
    self.subTotalLabel = nil;
    self.taxesLabel = nil;
    self.tipsLabel = nil;
    self.grandTotalLabel = nil;
    self.paidLabel = nil;
    self.tableNumberLabel = nil;
    self.payDateLabel = nil;
    self.thankyouLabel = nil;
    
    [self setPaidCreditLabel:nil];
    [self setPaidCreditTitleLabel:nil];
    [self setPaidTitleLabel:nil];
    [self setRewardLabel:nil];
    [self setRewardTitleLabel:nil];
    [self setCreditSuggestionView:nil];
    [self setPromotionName:nil];
    [self setTableView:nil];
    [self setTableTitleLabel:nil];
    [self setPromotionView:nil];
    [self setPromotionCardView:nil];
    [self setTipsTitleLabel:nil];
    [self setGrandTotalTitleLabel:nil];
    [super viewDidUnload];
}

- (IBAction)joinButtonTouched:(id)sender
{
    VerificationViewController *viewController = [VerificationViewController viewControllerFromNib];
    viewController.delegate = self;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^(){
                                 }];
}

-(void)displaySMSComposerSheet
{   
    MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
    picker.recipients = [NSArray arrayWithObject:APP.credPhoneNumber];
    picker.body = self.smsVerificationToken;
    picker.messageComposeDelegate = self;
    
    [self presentViewController:picker
                       animated:YES
                     completion:^(){
                     }];

}

#pragma mark - MFMessageComposeViewControllerDelegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    switch (result)
	{
		case MessageComposeResultSent:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
			break;
        case MessageComposeResultCancelled:
        case MessageComposeResultFailed:
		default:
			PCLog(@"Failed");
			break;
	}
	[self dismissViewControllerAnimated:YES
                             completion:^{}];
}


- (void)requsetReward
{
    RequestResult result = RRFail;
    result = [PAY requestGetRewardInPayment:self.receipt.payment
                            completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              // TODO: refresh receit view
                              [self.receipt.payment updatePayment:response.data];
                              [self.receipt.payment save];
                              
                              [self fillReceipt];
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while rewarding credit", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

#pragma mark - UITableViewDatasource & Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([self.receipt.allItems count] > 0){
        return [self.receipt.allItems count];
    } else {
        return [self.receipt.payment.lineItems count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id obj = nil;
    
    if([self.receipt.allItems count] > 0){
        obj = [self.receipt.allItems objectAtIndex:indexPath.row];
    } else {
        obj = [self.receipt.payment.lineItems objectAtIndex:indexPath.row];
    }
    
    if([obj isKindOfClass:[LineItem class]]){
        return 28.0f;
    } else {
        return 28.0f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"ReceiptItemCell";
    
    id obj = nil;
    
    if([self.receipt.allItems count] > 0){
        obj = [self.receipt.allItems objectAtIndex:indexPath.row];
    } else {
        obj = [self.receipt.payment.lineItems objectAtIndex:indexPath.row];
    }
    
    if([obj isKindOfClass:[LineItem class]]){

        ReceiptItemCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(cell == nil){
            cell = [ReceiptItemCell cell];
        }
        
        cell.lineItem = obj;
        return cell;
        
    } else {
        
        cellIdentifier = @"ReceiptOptionCell";
        ReceiptOptionCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(cell == nil){
            cell = [ReceiptOptionCell cell];
        }
        
        cell.optionDict = obj;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
}

- (void)mailMeViewController:(MailMeViewController *)viewController
          didTouchDoneButton:(id)sender
{
    if(![VALID validate:viewController.mailTextField
                message:NSLocalizedString(@"Please provide your E-mail Address", nil)]){
        return;
    }
    
    if(![VALID validateEmail:viewController.mailTextField
                       title:NSLocalizedString(@"Invalid Email",nil)
                     message:NSLocalizedString(@"Please enter a valid email address", nil)]){
        return;
    }
    
    [self requestMailMe:self.receipt
                 toMail:viewController.mailTextField.text
            autoMailing:viewController.autoMailingButton.selected
           isSavedEmail:NO];
}

- (void)requestMailMe:(Receipt *)receipt
               toMail:(NSString *)mailAddress
          autoMailing:(BOOL)autoMailing
         isSavedEmail:(BOOL)isSavedEmail
{
    
    RequestResult result = RRFail;
    result = [PAY requestMailMeReceipt:receipt
                          emailAddress:mailAddress
                       completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              if(isSavedEmail){
                                  
                              } else {
                                  [NSUserDefaults standardUserDefaults].emailAddress = mailAddress;
                                  
                                  [self dismissViewControllerAnimated:YES
                                                           completion:^{}];
                              }
                              
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Receipt Sent", nil)
                                                  message:[NSString stringWithFormat:NSLocalizedString(@"Your receipt has been sent to <%@>",nil)
                                                           ,[NSUserDefaults standardUserDefaults].emailAddress]];
                              
                              break;
                          case ResponseErrorAuthorize:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Authorization Error", nil)
                                                  message:NSLocalizedString(@"Receipt mailing request can be sent to only device which is used for payment", nil)];
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while sending email", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }

}


- (void)viewController:(VerificationViewController *)viewController
   successRegistration:(BOOL)success
{
    if(success){
        [self memberJoinCancelButtonTouched:nil];
        [self requsetReward];
    }
}

@end
