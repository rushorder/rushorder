//
//  CloudPhotoCollectionViewController.h
//  RushOrder
//
//  Created by Conan Kim on 2/6/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol CloudPhotoCollectionViewDelegate;

@interface CloudPhotoCollectionViewController : UICollectionViewController
@property (weak, nonatomic) id<CloudPhotoCollectionViewDelegate> delegate;
@end

@protocol CloudPhotoCollectionViewDelegate <NSObject>
- (void)cloudPhotoCollectionViewController:(CloudPhotoCollectionViewController *)viewController
                            didSelectPhoto:(NSDictionary *) dictionary;
@end
