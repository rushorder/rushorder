//
//  MenuViewController.h
//  RushOrder
//
//  Created by Conan on 5/14/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "MenuItemCell.h"
#import "MenuDetailViewController.h"
#import "OrderViewController.h"
#import "Merchant.h"
#import "MenuPhotoViewController.h"

@interface MenuViewController : PCViewController
<
UITableViewDelegate,
UITableViewDataSource,
UIPickerViewDataSource,
UIPickerViewDelegate,
MenuItemCellDelegate,
MenuDetailViewControllerDelegate,
OrderViewControllerDelegate,
UITextFieldDelegate,
UIAlertViewDelegate,
UIActionSheetDelegate,
UIPageViewControllerDelegate,
UIPageViewControllerDataSource,
MenuPhotoViewControllerDelegate
>

@property (strong, nonatomic) TableInfo *tableInfo;
@property (nonatomic) CartType initialCartType;

- (void)cartUnsolicitChanged:(NSNotification *)notification;

@end