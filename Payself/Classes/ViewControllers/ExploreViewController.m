//
//  ExploreViewController.m
//  RushOrder
//
//  Created by Conan Kim on 7/4/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

#import "ExploreViewController.h"

@interface ExploreViewController ()
@property (weak, nonatomic) IBOutlet UIView *tabbarContainerView;
@property (weak, nonatomic) IBOutlet UIButton *nearYouButton;
@property (weak, nonatomic) IBOutlet UIButton *popularButton;

@end

@implementation ExploreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)nearYouButtonTouched:(id)sender {
    UITabBarController *tabbarController = (UITabBarController *)self.childViewControllers.firstObject;
    tabbarController.selectedIndex = 0;
    self.nearYouButton.selected = YES;
    self.popularButton.selected = NO;
}

- (IBAction)popularButtonTouched:(id)sender {
    UITabBarController *tabbarController = (UITabBarController *)self.childViewControllers.firstObject;
    tabbarController.selectedIndex = 1;
    self.nearYouButton.selected = NO;
    self.popularButton.selected = YES;
}

- (void)scrollToTop
{
//    [self.tableView setContentOffset:CGPointZero
//                            animated:YES];
    UITabBarController *tabbarController = (UITabBarController *)self.childViewControllers.firstObject;
//    switch(tabbarController.selectedIndex){
//        case 0:
//           
//            break;
//        case 1:
//            break;
//    }
    
    UIViewController *viewController = [tabbarController.viewControllers objectAtIndex:tabbarController.selectedIndex];
    if ([viewController respondsToSelector:@selector(scrollToTop)]) {
        [viewController performSelector:@selector(scrollToTop)];
    }
}

@end
