//
//  MerchantViewController.h
//  RushOrder
//
//  Created by Conan on 11/11/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "Merchant.h"

@protocol MerchantViewControllerDelegate;

@interface MerchantViewController : PCViewController

@property (weak, nonatomic) id <MerchantViewControllerDelegate> delegate;
@property (nonatomic) BOOL canDirectOrder;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpaceConstraint;

@end

@protocol MerchantViewControllerDelegate <NSObject>
@optional
- (void)merchantViewController:(MerchantViewController *)merchantViewController
          didTouchActionButton:(id)sender
                      merchant:(Merchant *)merchant
                  withCartType:(CartType)cartType;
@end
