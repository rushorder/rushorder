//
//  VerificationViewController.m
//  RushOrder
//
//  Created by Conan on 4/10/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "VerificationViewController.h"
#import "PhoneNumberFormatter.h"
#import "PCCredentialService.h"

@interface VerificationViewController ()
{
    int _textFieldSemaphore;
    __strong PhoneNumberFormatter *_phoneNumberFormatter;
}
@property (weak, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;

@property (weak, nonatomic) IBOutlet PCTextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet NPStretchableButton *requestButton;
@property (weak, nonatomic) IBOutlet PCTextField *verificationCodeTextField;
@property (weak, nonatomic) IBOutlet NPStretchableButton *verificationButton;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *verificationInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *staticPhoneNumberLabel;

@property (weak, nonatomic) IBOutlet UILabel *verificationCodeLabel;
@property (weak, nonatomic) IBOutlet UIView *verificationContainer;

@property (copy, nonatomic) NSString *formattedPhoneNumber;

@property (nonatomic) NSInteger elapsedCount;
@end

@implementation VerificationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _textFieldSemaphore = 0;
        _phoneNumberFormatter = [[PhoneNumberFormatter alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Verification", nil);
    
    if(self.isRootViewController){
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemCancel
                                                                            target:self
                                                                            action:@selector(cancelButtonTouched:)];
    } else {
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemBack
                                                                            target:self
                                                                            action:@selector(cancelButtonTouched:)];
    }
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self transitVerificationView:NO
                         animated:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.phoneNumberTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)phoneNumberValueChanged:(UITextField *)sender
{
    if(_textFieldSemaphore) return;

    _textFieldSemaphore = 1;
    
    NSString *locale = [[NSLocale currentLocale] localeIdentifier];
    sender.text = [_phoneNumberFormatter format:sender.text
                                     withLocale:locale];
    _textFieldSemaphore = 0;
}

- (void)viewDidUnload {
    [self setPhoneNumberTextField:nil];
    [self setRequestButton:nil];
    [self setVerificationCodeTextField:nil];
    [self setVerificationButton:nil];
    [self setVerificationCodeLabel:nil];
    [self setVerificationContainer:nil];
    [self setPhoneNumberInfoLabel:nil];
    [self setVerificationInfoLabel:nil];
    [self setPhoneNumberLabel:nil];
    [self setStaticPhoneNumberLabel:nil];
    [self setScrollView:nil];
    [super viewDidUnload];
}

- (void)enableRequestButton:(id)sender
{
    if(self.elapsedCount <= 1){
        self.requestButton.buttonTitle = NSLocalizedString(@"Request verification code", nil);
        self.requestButton.enabled = YES;
    } else {
        self.elapsedCount--;
        self.requestButton.buttonTitle = [NSString stringWithFormat:NSLocalizedString(@"Request verification code (%d)", nil),
                                          self.elapsedCount];
        [self.requestButton setTitle:self.requestButton.buttonTitle
                            forState:UIControlStateDisabled];
        [self performSelector:@selector(enableRequestButton:)
                   withObject:nil
                   afterDelay:1.0f];
    }
}

- (IBAction)requestButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    self.staticPhoneNumberLabel.text = self.phoneNumberTextField.text;

    
    RequestResult result = RRFail;
    result = [CRED requestVerificationCode:self.staticPhoneNumberLabel.text
                           completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  self.requestButton.enabled = YES;
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              self.formattedPhoneNumber = [response objectForKey:@"phone_number"];
                              self.staticPhoneNumberLabel.text = self.formattedPhoneNumber;
                              self.phoneNumberTextField.text = self.formattedPhoneNumber;
                              [self transitVerificationView:YES
                                                   animated:YES];
                              
                              self.elapsedCount = 30;
                              self.requestButton.buttonTitle = [NSString stringWithFormat:NSLocalizedString(@"Request verification code(%d)", nil),
                                                                self.elapsedCount];
                              self.requestButton.enabled = NO;
                              
                              [self performSelector:@selector(enableRequestButton:)
                                         withObject:nil
                                         afterDelay:1.0f];
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                              
                              
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while requesting verification code", nil)];
                              }
                              
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                      
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            
            self.requestButton.enabled = NO;
            
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            
            break;
        default:
            break;
    }
}

- (void)transitVerificationView:(BOOL)verifyMode
                       animated:(BOOL)animated
{
    if(animated){
        [UIView animateWithDuration:0.4f
                         animations:^(){
                             [self viewTransit:verifyMode];
                         } completion: ^(BOOL finished){
                             if(verifyMode){
//                                 [self.verificationCodeTextField becomeFirstResponder];
                                 [self.scrollView setContentSizeWithBottomView:self.verificationContainer];
                             } else {
                                 [self.phoneNumberTextField becomeFirstResponder];
                                 
                                 [self.scrollView setContentSizeWithBottomView:self.requestButton];
                             }
                         }];
    } else {
        [self viewTransit:verifyMode];
        if(verifyMode){
//            [self.verificationCodeTextField becomeFirstResponder];
            [self.scrollView setContentSizeWithBottomView:self.verificationContainer];
        } else {
            if(animated == YES){
                [self.phoneNumberTextField becomeFirstResponder];
            }
            [self.scrollView setContentSizeWithBottomView:self.requestButton];
        }
    }
}

- (void)viewTransit:(BOOL)verifyMode
{
    CGFloat phoneAlpha = !verifyMode ? 1.0f : 0.0f;
    CGFloat verificationAlpha = verifyMode ? 1.0f : 0.0f;
    
    self.phoneNumberLabel.alpha = phoneAlpha;
    self.phoneNumberTextField.alpha = phoneAlpha;
    self.requestButton.alpha = phoneAlpha;
    self.phoneNumberInfoLabel.alpha = phoneAlpha;
    
    self.verificationCodeLabel.alpha = verificationAlpha;
    self.verificationContainer.alpha = verificationAlpha;
    self.verificationInfoLabel.alpha = verificationAlpha;
    self.staticPhoneNumberLabel.alpha = verificationAlpha;
    
    if(verifyMode){
        self.verificationContainer.y = 112.0f;
        self.staticPhoneNumberLabel.y = self.phoneNumberLabel.y;
        self.phoneNumberTextField.y = self.staticPhoneNumberLabel.y;
    } else {
        self.verificationContainer.y = 152.0f;
        self.staticPhoneNumberLabel.y = 100.0f;
        self.phoneNumberTextField.y = self.staticPhoneNumberLabel.y;
    }
}

- (void)cancelButtonTouched:(id)sender
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(enableRequestButton:)
                                               object:nil];
    if(self.isRootViewController){
        [self dismissViewControllerAnimated:YES
                                 completion:^{
                                     
                                 }];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)verifiyMyPhonenumberButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    if([self.formattedPhoneNumber length] == 0){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Phone number required", nil)
                            message:NSLocalizedString(@"Some problem has been found. Try later again from the begining please.", nil)];
        return;
    }
    
    RequestResult result = RRFail;
    result = [CRED requestVerification:self.formattedPhoneNumber
                      verificationCode:self.verificationCodeTextField.text
                           deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                       completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              [self cancelButtonTouched:nil];
                              
                              if([self.delegate respondsToSelector:@selector(viewController:successRegistration:)]){
                                  [self.delegate viewController:self
                                            successRegistration:YES];
                              }
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Your phone number is verified", nil)
                                                  message:NSLocalizedString(@"Now you can access your credit", nil)];
                              
                              
                              break;
                          case ResponseErrorVerificatoin:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Verification failed", nil)
                                                  message:response.message];
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while verifying your verification code", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (IBAction)requestAgainButtonTouched:(id)sender
{
    [self transitVerificationView:NO
                         animated:YES];
}

@end
