//
//  TablesetViewController.h
//  RushOrder
//
//  Created by Conan on 2/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Merchant.h"

@interface TablesetViewController : PCViewController

<
UITextFieldDelegate,
UIPickerViewDataSource,
UIPickerViewDelegate
>


@end
