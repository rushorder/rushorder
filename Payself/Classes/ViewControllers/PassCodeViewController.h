//
//  PassCodeViewController.h
//  RushOrder
//
//  Created by Conan on 3/28/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"

typedef enum passCodeMode_{
    PassCodeModeChange = 0,
    PassCodeModeOff,
    PassCodeModeRun
} PassCodeMode;

@protocol PassCodeViewControllerDelegate;

@interface PassCodeViewController : PCViewController
<
UITextFieldDelegate
>
@property (weak, nonatomic) id<PassCodeViewControllerDelegate> delegate;
@property (nonatomic, getter = isPassMode) BOOL passMode;
@property (nonatomic, getter = isModal) BOOL modal;
@property (nonatomic) PassCodeMode passCodeMode;
@end

@protocol PassCodeViewControllerDelegate <NSObject>
@optional
- (void)didPassCodeAuthorized:(PassCodeViewController *)passCodeViewController;
@end