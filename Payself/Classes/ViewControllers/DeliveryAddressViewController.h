//
//  DeliveryAddressViewController.h
//  RushOrder
//
//  Created by Conan Kim on 2/20/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeliveryAddress.h"
#import "LocationViewController.h"

@interface DeliveryAddressViewController : UIViewController
< LocationViewControllerDelegate>
@property(strong, nonatomic) DeliveryAddress *deliveryAddress;

- (void)drawDeliveryAddress;

@end
