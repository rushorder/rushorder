//
//  FaqViewController.h
//  RushOrder
//
//  Created by Conan Kim on 3/22/16.
//  Copyright © 2016 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FaqViewController : UIViewController

@property (strong, nonatomic) NSDictionary *faq;
@end
