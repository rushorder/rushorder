//
//  NewCardViewController.h
//  RushOrder
//
//  Created by Conan on 2/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"
#import "Payment.h"
#import "PCCardNumberTextField.h"
#import "TakeoutViewController.h"
#import "DineinViewController.h"
#import "DeliveryViewController.h"
#import "PaymentSummaryView.h"
#import "CardIO.h"
#import "SignInCustomerViewController.h"
//#import "CardIOPaymentViewController.h"
#import "Addresses.h"
#import "OrangeCell.h"
//#import "BraintreeUI.h"
#import <Braintree/BraintreeUI.h>
#import <Braintree/BraintreeCard.h>


@interface NewCardViewController : PCViewController
<
PCCardNumberTextFieldDelegate,
UIPickerViewDataSource,
UIPickerViewDelegate,
UITextFieldDelegate,
UIAlertViewDelegate,
TakeoutViewControllerDelegate,
DineinViewControllerDelegate,
DeliveryViewControllerDelegate,
PaymentSummaryViewDelegate,
CardIOPaymentViewControllerDelegate,
OrangeViewDelegate,
BTDropInViewControllerDelegate,
SignInCustomerViewControllerDelegate
>

@property (nonatomic, getter = isManageMode) BOOL manageMode;
@property (strong, nonatomic) Payment *payment;
@property (strong, nonatomic) MobileCard *mobileCard;
@property (weak, nonatomic) PaymentSummaryView *paymentSummaryView;
@property (strong, nonatomic) OrangeCell *orangeCell;

@property (strong, nonatomic) Addresses *address;
@property (nonatomic, copy) NSString *receiverName;
@property (nonatomic, copy) NSString *phoneNumber;
@property (copy, nonatomic) NSString *requestMessage;
@property (nonatomic) NSInteger afterMinute;

@property (nonatomic) PromotionValidStatus promotionValidStatus;
@property (nonatomic) BOOL disposeOrderWhenBack;
@property (nonatomic, getter = isUnintendedPresented) BOOL unintendedPresented;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

@property (nonatomic) BOOL initialCreditSelection;

- (void)setInitialOptionValueWithOrder:(Order *)order;
@end
