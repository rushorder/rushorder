//
//  MainTabbarController.h
//  RushOrder
//
//  Created by Conan Kim on 9/6/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTabbarController : UITabBarController

- (BOOL)showIntroView;

@end
