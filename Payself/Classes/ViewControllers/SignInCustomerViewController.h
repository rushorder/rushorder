//
//  SignInCustomerViewController.h
//  RushOrder
//
//  Created by Conan on 2/19/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import "PCViewController.h"

@protocol SignInCustomerViewControllerDelegate;

@interface SignInCustomerViewController : PCViewController
<
UITextFieldDelegate,
UIAlertViewDelegate
>

@property (weak, nonatomic) id<SignInCustomerViewControllerDelegate> delegate;

@property (nonatomic, getter = isShowGuide) BOOL showGuide;
@end


@protocol SignInCustomerViewControllerDelegate <NSObject>
@optional
- (void)signInCustomerViewControllerNeedDissmissingViewController:(PCViewController *)viewController;
@end

