//
//  CreditPointViewController.h
//  RushOrder
//
//  Created by Conan on 4/25/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import <MessageUI/MessageUI.h>
#import "VerificationViewController.h"

@interface CreditPointViewController : PCTableViewController
<
MFMessageComposeViewControllerDelegate,
VerificationViewControllerDelegate
>

@end
