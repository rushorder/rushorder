//
//  MenuViewController.m
//  RushOrder
//
//  Created by Conan on 5/14/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "MenuViewController.h"
#import "PCPaymentService.h"
#import "IIViewDeckController.h"
#import "TablesetViewController.h"
#import "BillViewController.h"
#import "MenuCategory.h"
#import "MenuSubCategory.h"
#import "MenuOptionGroup.h"
#import "MenuOption.h"
#import "MenuItem.h"
#import "OrderManager.h"
#import "MenuManager.h"
#import "PCMenuService.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FormatterManager.h"
#import "PCCredentialService.h"
#import "PaymentMethodViewController.h"
#import "NewCardViewController.h"
#import "RestaurantSummaryView.h"
#import "MenuPhotoPageViewController.h"
#import "RemoteDataManager.h"
#import "DeliveryViewController.h"

#import "TransactionManager.h"

#define MENU_REL_XIB_NAME   @"MenuItemCell"
#define SEL_BACK_IMAGEVIEW_MARGIN 5.0f


typedef enum menuItemType_{
    MenuItemTypeSubCategory = 1,
    MenuItemTypeItem
} MenuItemType;

@interface MenuViewController ()


@property (weak, nonatomic) IBOutlet UIScrollView *shortcutScrollView;
@property (strong, nonatomic) OrderViewController *orderViewController;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *tableNumberView;
@property (strong, nonatomic) IBOutlet NPKeyboardAwareScrollView *tableNumberScrollView;
@property (strong, nonatomic) MenuItem *selectedMenuItem;
@property (nonatomic) NSInteger selectedQuantity;
@property (copy, nonatomic) NSString *specialInstructions;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (strong, nonatomic) IBOutlet UIPickerView *tableNumberPickerView;
@property (strong, nonatomic) IBOutlet UIToolbar *tableNumberToolBar;
@property (weak, nonatomic) IBOutlet PCTextField *tableNumberTextField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *toggleBarButtonItem;
@property (strong, nonatomic) NSMutableArray *mobileCardList;
@property (strong, nonatomic) IBOutlet RestaurantSummaryView *restaurantSummaryView;

@property (nonatomic, getter = isLockReloadBadge) BOOL lockReloadBadge;
@property (nonatomic) BOOL didAppeared;
@property (nonatomic) BOOL checkedNoMenuItem;
@property (strong, nonatomic) IBOutlet UIView *noItemView;
@property (weak, nonatomic) IBOutlet UILabel *noMenuInfoLabel;
@property (weak, nonatomic) IBOutlet NPStretchableButton *gotoRequestBillButton;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *takeoutTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *takeoutHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet NPStretchableButton *takeoutButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *dineinButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonsSpanConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shortcutHeightConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *shortcutBackImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineHeightConstraint;

@property (strong, nonatomic) MenuItemCell *offScreenMenuItemCell;
@property (strong, nonatomic) MenuSubCategoryCell *offScreenMenuSubCategoryCell;
@property (nonatomic) NSInteger selectedSectionIndex;

@property (strong, nonatomic) IBOutlet MenuPhotoPageViewController *photoPageViewController;
@property (strong, nonatomic) IBOutlet UIImageView *selectedSectionBackground;
@property (strong, nonatomic) IBOutlet UIView *tableFooterView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectedSectionBgWidthConstraint;
@property (weak, nonatomic) IBOutlet UIView *pageDotContainer;
@property (nonatomic) NSUInteger selectedPageIndex;
@property (nonatomic) NSUInteger totalPageCount;
@property (nonatomic) CGFloat widthPerPage;
@property (nonatomic, getter = isCardListWaiting) BOOL cardListWaiting;

@property (nonatomic) BOOL holdSpyScrolling;
@end

@implementation MenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(cartUnsolicitChanged:)
                                                     name:CartUnsolicitChangedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(orderListChanged:)
                                                     name:LineItemChangedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(menuListChanged:)
                                                     name:MenuListChangedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(tableListChanged:)
                                                     name:TableListChangedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(merchantUpdated:)
                                                     name:MerchantInformationUpdatedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(cardListChanged:)
                                                     name:CardListChangeNotification
                                                   object:nil];
        
        self.selectedSectionIndex = -1;
    }
    return self;
}

- (void)cardListChanged:(NSNotification *)notification
{
    if(CRED.isSignedIn){
        self.mobileCardList = REMOTE.cards;
        if(self.isCardListWaiting){
            self.cardListWaiting = NO;
            [self proceedPayment];
        }
    } else {
        self.mobileCardList = [MobileCard listAll];
    }
}

- (void)merchantUpdated:(NSNotification *)notification
{
    BOOL invalidMerchant = [notification.userInfo boolForKey:@"InvalidMerchant"];
    if(invalidMerchant){
        
        if([APP.viewDeckController isSideOpen:IIViewDeckRightSide]){
            [APP.viewDeckController closeRightViewAnimated:YES
                                                completion:^(IIViewDeckController *controller, BOOL success) {
                                                    if(success){
                                                        TRANS.dirty = YES;
                                                        [self.navigationController popToRootViewControllerAnimated:YES];
                                                    }
                                                }];
        }
        
        
    } else {
         // TODO: if the restaurant disabled menu order -> change view to table set view controller

        self.tableView.tableHeaderView = nil;
        [self.restaurantSummaryView drawMerchant:ORDER.merchant
                                     forceHeight:YES
                                      isDelivery:(self.initialCartType == CartTypeDelivery)
                                       showPromo:YES];
        switch(self.initialCartType){
            case CartTypePickupServiced:
            case CartTypePickup:
                if(ORDER.merchant.isTableBase){
                    self.initialCartType = CartTypeCart;
                }
            case CartTypeCart:
                if(!ORDER.merchant.isTableBase){
                    if(ORDER.merchant.servicedBy == ServicedByStaff){
                        self.initialCartType = CartTypePickupServiced;
                    } else {
                        self.initialCartType = CartTypePickup;
                    }
                }
                break;
            default:
                
                break;
        }
        self.tableView.tableHeaderView = self.restaurantSummaryView;
    }
}

- (void)cartUnsolicitChanged:(NSNotification *)notification
{
    if(self.tableInfo != nil){
        [ORDER checkTable:self.tableInfo
                  success:^(){
                      if(ORDER.shouldOpenRightSlide){                          
                          [APP.viewDeckController openRightViewAnimated:YES
                                                             completion:^(IIViewDeckController *controller, BOOL success) {
                                                                 
                                                             }];
                      }
                  }
                  failure:^(OrderStatus status){
                      
                  }];
    }
}

- (void)orderListChanged:(NSNotification *)notification
{
    if(self.isLockReloadBadge){
        return;
    }
    
    [self reloadBadgeNumber:[NSNumber numberWithBool:NO]];
}

- (void)menuListChanged:(NSNotification *)notification
{
    self.gotoRequestBillButton.hidden = YES;
    
    if([MENUPAN.menuList count] > 0){
        [self reloadData];
        if(self.tableView.tableFooterView == self.noItemView){
            self.tableView.tableFooterView = nil;
        }
    } else if(ORDER.merchant.isAblePayment){
        self.tableView.tableFooterView = self.noItemView;
        self.noMenuInfoLabel.text = NSLocalizedString(@"This restaurant may be configuring its menu so you cannot order right now.", nil);
//        self.gotoRequestBillButton.hidden = NO;
//        if(self.didAppeared){
//            [self pushTablesetViewController];
//        } else {
//            self.checkedNoMenuItem = YES;
//        }
        
        
    } else {
        self.tableView.tableFooterView = self.noItemView;
        self.noMenuInfoLabel.text = NSLocalizedString(@"This restaurant may be configuring its menu so you cannot order right now.", nil);
        
        [self reloadData];
    }
}

- (void)reloadData
{
    [self.tableView reloadData];
    [self drawShortcut];
    //Can be estimated
    [self addFooterIfNeeded];
}

- (void)drawShortcut
{
    if([MENUPAN.menuList count] < 2){
        self.shortcutHeightConstraint.constant = 0.0f;
        self.lineHeightConstraint.constant = 0.0f;
        self.shortcutScrollView.hidden = YES;
        self.shortcutBackImageView.hidden = YES;
        return;
    } else {
        self.shortcutHeightConstraint.constant = 38.0f;
        self.lineHeightConstraint.constant = 1.0f;
        self.shortcutScrollView.hidden = NO;
        self.shortcutBackImageView.hidden = NO;
    }
    
    CGFloat xOrigin = 0.0f;
    
    [self.shortcutScrollView removeAllSubviews];
    [self.pageDotContainer removeAllSubviews];
    
    NSInteger i = 0;
    for(MenuCategory *menuCategory in MENUPAN.menuList){
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(xOrigin,
                                                                      0.0f,
                                                                      100.0f,
                                                                      38.0f)];
        button.buttonTitle = menuCategory.name;
        if([menuCategory.name length] == 0 && i == 0){
            button.buttonTitle = NSLocalizedString(@"Top", nil);
        }
        button.titleLabel.font = [UIFont systemFontOfSize:14.0f];

        [button setTitleColor:[UIColor c5Color] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor c11Color] forState:UIControlStateHighlighted];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        
        button.titleLabel.textAlignment = NSTextAlignmentCenter;
        button.tag = i;
        button.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [button addTarget:self
                   action:@selector(shortcutButtonTouched:)
         forControlEvents:UIControlEventTouchUpInside];
        button.titleEdgeInsets = UIEdgeInsetsMake(-9.0f,
                                                  0.0f,
                                                  0.0f,
                                                  0.0f);
        CGSize size = [button intrinsicContentSize];
        size.width += 40.0f;
        size.height = self.shortcutScrollView.height;
        button.size = size;
        xOrigin += button.width;
        
        if(i == 0){
            [self.shortcutScrollView addSubview:self.selectedSectionBackground];
            CGRect backFrame = self.selectedSectionBackground.frame;
            backFrame.origin.x = button.x + SEL_BACK_IMAGEVIEW_MARGIN;
            backFrame.origin.y = 4.0f;
            backFrame.size.width = button.width - (SEL_BACK_IMAGEVIEW_MARGIN * 2);
            self.selectedSectionBackground.frame = backFrame;
            self.selectedSectionBgWidthConstraint.constant = backFrame.size.width;
            button.selected = YES;
            self.selectedSectionIndex = 0;
        }
        
        [self.shortcutScrollView addSubview:button];
        i++;
    }
    
    
    self.totalPageCount = (NSUInteger)ceil(xOrigin / self.shortcutScrollView.width);
    self.widthPerPage = xOrigin / (CGFloat)self.totalPageCount;
    
#define DOT_SPACE 12.0f
    
    for(i = 0; i < self.totalPageCount ; i++){
        CGFloat viewWidth = self.pageDotContainer.width;
        
        CGFloat originX = (viewWidth - (self.totalPageCount * DOT_SPACE)) / 2;
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(originX + (i * DOT_SPACE),
                                                                      0.0f,
                                                                      DOT_SPACE,
                                                                      5.0f)];
        button.tag = (i+1);
        [button addTarget:self
                   action:@selector(pageControlButtonTouched:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setImage:[UIImage imageNamed:@"point_slide_off"]
                forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"point_slide_on"]
                forState:UIControlStateSelected];
        
        [self.pageDotContainer addSubview:button];
        
        if(i == 0){
            button.selected = YES;
            self.selectedPageIndex = 1;
        }
    }
    
    PCLog(@"self.shortcutScrollView.height %f", self.shortcutScrollView.height);
    [self.shortcutScrollView setContentSize:CGSizeMake(xOrigin, (self.shortcutScrollView.height - 5.0f))];
}

- (void)shortcutButtonTouched:(UIButton *)sender
{
    NSInteger selectedSection = sender.tag;

    CGRect sectionRect = [self.tableView rectForSection:selectedSection];
    sectionRect.size.height = self.tableView.frame.size.height;
    [self.tableView scrollRectToVisible:sectionRect animated:YES];
    self.holdSpyScrolling = YES;
    [self performSelector:@selector(autoScrollCompletion)
               withObject:nil
               afterDelay:0.25f];
}

- (void)autoScrollCompletion
{
    self.holdSpyScrolling = NO;
    [self scrollViewDidScroll:self.tableView];
}

- (void)pageControlButtonTouched:(UIButton *)sender
{
    [self.shortcutScrollView setContentOffset:CGPointMake((sender.tag - 1) * self.widthPerPage, 0.0f)
                                     animated:YES];
}


- (IBAction)gotoRequestBillButton:(id)sender
{
    [self pushTablesetViewController];
}

- (void)pushTablesetViewController
{
    TablesetViewController *viewController = [TablesetViewController viewControllerFromNib];    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)tableListChanged:(NSNotification *)notification
{
    [self.tableNumberPickerView reloadAllComponents];

    // When Picker first
//    if([self.tableNumberTextField.text length] == 0){
//        if([ORDER.tableList count] > 0){
//            TableInfo *firstTableInfo = [ORDER.tableList objectAtIndex:0];
//            self.tableNumberTextField.text = firstTableInfo.tableNumber;
//        }
//    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self drawShortcut];
    
    self.shortcutScrollView.decelerationRate = 0.0f;
    
    switch(MENUPAN.selectedMenuListType){
        case MenuListTypeDinein:
            self.title = NSLocalizedString(@"Dine-in Menu", nil);
            break;
        case MenuListTypeTakeout:
            self.title = NSLocalizedString(@"Take-out Menu", nil);
            break;
        case MenuListTypeDelivery:
            self.title = NSLocalizedString(@"Delivery Menu", nil);
            break;
        default:
            self.title = NSLocalizedString(@"Menu", nil);
            break;
    }
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemCart
                                                                              target:self
                                                                              action:@selector(orderButtonTouched:)];
    
    self.orderViewController = [OrderViewController viewControllerFromNib];
    self.orderViewController.delegate = self;

    [self.tableNumberScrollView setContentSize:CGSizeMake(320.f, 433.0f)];
    self.tableNumberScrollView.adjustingOffsetY = -70.0f;
    
    self.tableNumberTextField.inputView = self.tableNumberPickerView;
    self.toggleBarButtonItem.title = NSLocalizedString(@"Keyboard", nil);
    self.tableNumberTextField.inputAccessoryView = self.tableNumberToolBar;
    
    self.tableView.tableHeaderView = nil;
    [self.restaurantSummaryView drawMerchant:ORDER.merchant
                                 forceHeight:YES
                                  isDelivery:(self.initialCartType == CartTypeDelivery)
                                   showPromo:YES];
    
    self.tableView.tableHeaderView = self.restaurantSummaryView;
    
    [self hideTableNumber:NO];

    if(self.tableInfo != nil){
        [ORDER checkTable:self.tableInfo
                  success:^(){
                      if(self.didAppeared){
                          if(ORDER.shouldOpenRightSlide){
                              [APP.viewDeckController openRightViewAnimated:YES
                                                                 completion:^(IIViewDeckController *controller, BOOL success) {
                                                                     
                                                                 }];
                          }
                      }
                  }
                  failure:^(OrderStatus status){
                      
                  }];
    }
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)addFooterIfNeeded
{
    if([MENUPAN.menuList count] < 2 && [MENUPAN.menuList count] > 0){
        self.tableView.tableFooterView = nil;
    } else {
        
        MenuCategory *menuCategory = [MENUPAN.menuList lastObject];
        
        CGFloat heightSumOfItems = 0.0f;
        
        if([menuCategory.name length] > 0){
            heightSumOfItems += 28.0f;
        }
        
        for(id item in menuCategory.items){
        
            if([item isKindOfClass:[MenuSubCategory class]]){
                MenuSubCategory *subCategory = (MenuSubCategory *)item;
                if([subCategory.name length] > 0){
                    heightSumOfItems += 28.0f;
                }
            } else {
                MenuItem *castedMenuItem = (MenuItem *)item;
                if(castedMenuItem.cellHeight == 0.0f){
                    [self.offScreenMenuItemCell setItemForHeight:castedMenuItem];
                    castedMenuItem.cellHeight = [self.offScreenMenuItemCell cellHeight];
                }
                heightSumOfItems += castedMenuItem.cellHeight;
            }
            
            if(heightSumOfItems >= self.tableView.height){
                break;
            }
        }
        
        CGFloat expectedHeight = 0.0f;
        
        if(heightSumOfItems < self.tableView.height){
            self.tableView.tableFooterView = nil;
            expectedHeight = (self.tableView.height - heightSumOfItems);
            expectedHeight = MAX((expectedHeight + 3.0f), 60.0f);
            self.tableFooterView.height = expectedHeight;
            self.tableView.tableFooterView = self.tableFooterView;
        } else {
            self.tableView.tableFooterView = nil;
        }
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
//    APP.viewDeckController.panningMode = IIViewDeckNoPanning;
    
    APP.viewDeckController.rightController = self.orderViewController;
    [self reloadBadgeNumber:[NSNumber numberWithBool:NO]];
    
    if(!self.didAppeared){
        if(ORDER.shouldOpenRightSlide){
            [APP.viewDeckController openRightViewAnimated:YES
                                               completion:^(IIViewDeckController *controller, BOOL success) {
                                               }];
        }
        
        //Can be estimated
        [self addFooterIfNeeded];
    }
    
    if(!self.didAppeared){
        if(ORDER.merchant != nil){
            [ORDER requestMerchantInfo];
            
            self.didAppeared = YES;
            
            if([MENUPAN.menuList count] == 0 && MENUPAN.menuFetched){
                self.tableView.tableFooterView = self.noItemView;
                self.noMenuInfoLabel.text = NSLocalizedString(@"This restaurant may be configuring its menu so you cannot order right now.", nil);
//                self.gotoRequestBillButton.hidden = NO;
            }
            
        } else {
            
        }
    }
}

- (void)orderButtonTouched:(id)sender
{
    if(ORDER.merchant.isTableBase && ORDER.tableInfo == nil && ORDER.order == nil
       && (self.initialCartType == CartTypeCart)){
        [self showTableNumber:YES];
    } else {
        IIViewDeckController *deckController = (IIViewDeckController *)APP.window.rootViewController;
        [APP.viewDeckController.slidingControllerView.layer removeAnimationForKey:@"previewBounceAnimation"];
        [deckController toggleRightViewAnimated:YES];
    }
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [self setTableNumberView:nil];
    [self setTableNumberScrollView:nil];
    [self setTableNumberTextField:nil];

    [self setToggleBarButtonItem:nil];
    [self setNoItemView:nil];
    [self setNoMenuInfoLabel:nil];
    [self setGotoRequestBillButton:nil];
    [super viewDidUnload];
    
    IIViewDeckController *deckController = (IIViewDeckController *)APP.window.rootViewController;
    if(deckController.rightController == self.orderViewController)
        deckController.rightController = nil;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if(APP.viewDeckController.rightController == self.orderViewController)
        APP.viewDeckController.rightController = nil;
    
    // TODO: this code is not in Effect - need to be moved somewhere
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

#pragma mark - UITableView Delegate & Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return [MENUPAN.menuList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    MenuCategory *menuCategory = [MENUPAN.menuList objectAtIndex:section];
    return [menuCategory.items count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MenuCategorySectionView *view = [MenuCategorySectionView viewWithNibName:MENU_REL_XIB_NAME
                                                                     atIndex:2];
    MenuCategory *menuCategory = [MENUPAN.menuList objectAtIndex:section];
    view.button.buttonTitle = menuCategory.name;
    if([menuCategory.name length] == 0){
        return nil;
    } else {
        return view;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCategory *menuCategory = [MENUPAN.menuList objectAtIndex:indexPath.section];
    id item = [menuCategory.items objectAtIndex:indexPath.row];
    
    if([item isKindOfClass:[MenuSubCategory class]]){
        MenuSubCategory *subCategory = (MenuSubCategory *)item;
        if([subCategory.name length] > 0){
            
            if(subCategory.cellHeight == 0.0f){
                self.offScreenMenuSubCategoryCell.subCategory = subCategory;
                subCategory.cellHeight = [self.offScreenMenuSubCategoryCell cellHeight];
            }
            
            return subCategory.cellHeight;
            
            
        } else {
            return 0.0f;
        }
    } else {
        MenuItem *castedMenuItem = (MenuItem *)item;
        if(castedMenuItem.cellHeight == 0.0f){
            [self.offScreenMenuItemCell setItemForHeight:castedMenuItem];
            castedMenuItem.cellHeight = [self.offScreenMenuItemCell cellHeight];
        }

        return castedMenuItem.cellHeight;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    MenuCategory *menuCategory = [MENUPAN.menuList objectAtIndex:section];
    
    if([menuCategory.name length] == 0){
        return 0.0f;
    } else {
        return 28.0f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    MenuItemType itemType = MenuItemTypeItem;
    
    MenuCategory *menuCategory = [MENUPAN.menuList objectAtIndex:indexPath.section];
    id item = [menuCategory.items objectAtIndex:indexPath.row];
    
    if([item isKindOfClass:[MenuSubCategory class]]){
        itemType = MenuItemTypeSubCategory;
    }
    
    switch(itemType){
        case MenuItemTypeItem:
            return [self tableView:tableView
     menuItemCellForRowAtIndexPath:indexPath
                      withMenuItem:item];
            break;
        case MenuItemTypeSubCategory:
            return [self tableView:tableView
menuSubCategoryCellForRowAtIndexPath:indexPath
               withMenuSubCategory:item];
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView menuItemCellForRowAtIndexPath:(NSIndexPath *)indexPath withMenuItem:(MenuItem *)item
{
    NSString *cellIdentifier = @"MenuItemCell";
    
    MenuItemCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        cell = [MenuItemCell cell];
    }
    
    cell.item = item;
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView menuSubCategoryCellForRowAtIndexPath:(NSIndexPath *)indexPath withMenuSubCategory:(MenuSubCategory *)subCategory
{
    NSString *cellIdentifier = @"MenuSubCategoryCell";
    
    MenuSubCategoryCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        cell = [MenuSubCategoryCell cellWithNibName:MENU_REL_XIB_NAME
                                            atIndex:1];
    }
    cell.subCategory = subCategory;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCategory *menuCategory = [MENUPAN.menuList objectAtIndex:indexPath.section];
    id item = [menuCategory.items objectAtIndex:indexPath.row];
    
    if([item isKindOfClass:[MenuItem class]]){
        MenuItem *menuItem = item;
        
        self.selectedIndexPath = indexPath;
        
        MenuDetailViewController *viewController = [MenuDetailViewController viewControllerFromNib];
        viewController.item = menuItem;
        viewController.delegate = self;
        viewController.indexPath = indexPath;
        
        [self presentViewControllerInNavigation:viewController
                                       animated:YES
                                     completion:NULL];
    }
}


- (void)cell:(MenuItemCell *)cell didTouchedAddToCartButtonAtIndexPath:(NSIndexPath *)indexPath
{
    [self addMenuItemAtIndexPath:indexPath];
}

- (void)addMenuItemAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCategory *menuCategory = [MENUPAN.menuList objectAtIndex:indexPath.section];
    id item = [menuCategory.items objectAtIndex:indexPath.row];
    
    if([item isKindOfClass:[MenuItem class]]){
        MenuItem *menuItem = item;
        
        self.selectedIndexPath = indexPath;
        
        if([menuItem.optionGroups count] > 0 || [menuItem.prices count] > 1){
            MenuDetailViewController *viewController = [MenuDetailViewController viewControllerFromNib];
            viewController.item = item;
            viewController.delegate = self;
            viewController.indexPath = indexPath;
            
            [self presentViewControllerInNavigation:viewController
                                           animated:YES
                                         completion:NULL];
        } else {
            [self addMenuItem:menuItem quantity:1 specialInstructions:nil];
        }
    }

}


- (void)cell:(MenuItemCell *)cell didTouchedPhotoButtonAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCategory *menuCategory = [MENUPAN.menuList objectAtIndex:indexPath.section];
    id item = [menuCategory.items objectAtIndex:indexPath.row];
    
    if([item isKindOfClass:[MenuItem class]]){
        MenuPhotoViewController *initialViewController = [MenuPhotoViewController viewControllerFromNib];
        initialViewController.item = item;
        initialViewController.indexPath = indexPath;
        initialViewController.category = menuCategory;
        initialViewController.delegate = self;
        if(cell == nil) initialViewController.shouldMenuDetailPop = YES;
        
        [self.photoPageViewController setViewControllers:@[initialViewController]
                                               direction:UIPageViewControllerNavigationDirectionForward
                                                animated:YES
                                              completion:^(BOOL finished) {
                                                  
                                              }];
        [self presentViewController:self.photoPageViewController
                           animated:YES
                         completion:^{}];
    }
}

#pragma mark - MenuDetailViewControllerDelegate
- (void)menuDetailViewController:(MenuDetailViewController *)viewController
                  didNewMenuItem:(MenuItem *)menuItem
                        quantity:(NSInteger)quantity
             specialInstructions:(NSString *)specialInstructions
{
    [self.tableView deselectRowAtIndexPath:self.selectedIndexPath
                                  animated:YES];
    
    [self addMenuItem:menuItem quantity:quantity specialInstructions:specialInstructions];
}

- (void)menuDetailViewControllerDidCancelTouched:(MenuDetailViewController *)viewController
{
    [self.tableView deselectRowAtIndexPath:self.selectedIndexPath
                                  animated:YES];
}

- (void)menuDetailViewControllerDidTouchedPhoto:(MenuDetailViewController *)viewController
                                    atIndexPath:(NSIndexPath *)indexPath
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [self cell:nil didTouchedPhotoButtonAtIndexPath:indexPath];
                             }];
}

- (void)showTableNumber:(BOOL)animated
{
    [APP.window addSubview:self.tableNumberScrollView];
    [UIView animateWithDuration:0.3f
                     animations:^{
                         [self moveTableNumber:YES];
                     }];
}

- (void)hideTableNumber:(BOOL)animated
{
    if(animated){
        [UIView animateWithDuration:0.3f
                         animations:^{
                                       [self moveTableNumber:NO];
                                   }
                         completion:^(BOOL finished){
                             if(finished){
                                 [self.tableNumberScrollView removeFromSuperview];
                             }
                         }];
    } else {
        [self moveTableNumber:NO];
        [self.tableNumberScrollView removeFromSuperview];
    }
}

- (void)moveTableNumber:(BOOL)show
{
    self.tableNumberView.frame = CGRectMake(self.tableNumberView.x, show ? 76.0f : 600.0f,
                                            self.tableNumberView.frame.size.width,
                                            self.tableNumberView.frame.size.height);
    
    self.tableNumberScrollView.alpha = show ? 1.0f : 0.0f;
}

//- (void)addMenuItem:(MenuItem *)menuItem quantity:(NSInteger)quantity
//{
//    [self addMenuItem:menuItem quantity:quantity specialInstructions:nil];
//}

- (void)addMenuItem:(MenuItem *)menuItem quantity:(NSInteger)quantity specialInstructions:(NSString *)specialInstructions
{
    if(!ORDER.merchant.isOpenHour){
        [UIAlertView alertWithTitle:NSLocalizedString(@"This restaurant is closed", nil)
                            message:NSLocalizedString(@"You can order only when the restaurant is open.", nil)];
        return;
    }
    
    switch(ORDER.cart.cartType){
        case CartTypeUnknown:
            if(ORDER.merchant.isTableBase){
                
                // Table base needs to send server per each item
                // So, check ahead for adding item
                // But, Ticket base needs not to send server, So, check availability for order lately (at making checkout)
                if(self.initialCartType == CartTypeTakeout || self.initialCartType == CartTypeDelivery){
                    
                    ORDER.cart.cartType = self.initialCartType;
                    
                    [self addMenuItem:menuItem
                             quantity:quantity
                  specialInstructions:specialInstructions];
                    
                } else {
                    
                    if(ORDER.tableInfo == nil){
                        
                        self.selectedMenuItem = menuItem;
                        self.selectedQuantity = quantity;
                        self.specialInstructions = specialInstructions;
                        
                        [self showTableNumber:YES];
                        
                    } else {
                        if(ORDER.cart.status != CartStatusOrdering){
                            [UIAlertView alertWithTitle:NSLocalizedString(@"Order list is not editable", nil)
                                                message:NSLocalizedString(@"Order list has been submitted. To add/modify/remove orders, you have to postpone the confirmation of your order by \"Change Submitted Order\" button", nil)
                                               delegate:self
                                                    tag:100];
                            [self resetSelection];
                            return;
                        }
                        self.lockReloadBadge = YES;
                        [ORDER.cart addMenuItem:menuItem
                                       quantity:quantity
                            specialInstructions:specialInstructions
                                        success:^(BOOL isOnlyLocal){
                                            if(isOnlyLocal)
                                                TRANS.localDirty = YES;
                                            else
                                                TRANS.dirty = YES;
                                            self.selectedMenuItem = nil;
                                            self.selectedQuantity = 0;
                                            self.specialInstructions = nil;
                                            [self cartAddAnimate];
                                            
                                        } failure:^{
                                            [self resetSelection];
                                            self.lockReloadBadge = NO;
                                            
                                            if(ORDER.merchant.isTableBase){
                                                [ORDER checkTable:ORDER.tableInfo
                                                          success:^{
                                                              
                                                          }
                                                          failure:^(OrderStatus status){
                                                              
                                                          }];
                                            }
                                        }];
                    }
                }
            } else {
                // Ticket base
                // Take out only restaurant type should not be table base restaurant (accept_ticket-accept_togo)
                switch(ORDER.merchant.servicedBy){
                    case ServicedBySelf:
                        
                        ORDER.cart.cartType = self.initialCartType;
                        [self addMenuItem:menuItem
                                 quantity:quantity
                      specialInstructions:specialInstructions];
                        
                        break;
                    case ServicedByStaff:

                        ORDER.cart.cartType = self.initialCartType;
                        
//                        if(self.initialCartType == CartTypeTakeout
//                           || self.initialCartType == CartTypeDelivery){
                            [self addMenuItem:menuItem
                                     quantity:quantity
                          specialInstructions:specialInstructions];
                        
//                        } else {
//                            // CartTypePickupServiced - So, need table number
//                            
//                            self.selectedMenuItem = menuItem;
//                            self.selectedQuantity = quantity;
//                            self.specialInstructions = specialInstructions;
//                            
//                            [self showTableNumber:YES];
//                        }
                        
                        break;
                    case ServicedByNone:
                        
                        if(self.initialCartType != CartTypeTakeout
                           && self.initialCartType != CartTypeDelivery){
                            PCError(@"Initial CartType must CartTypeTakeout2 or CartTypeDelivery2");
                        }
                        
                        ORDER.cart.cartType = self.initialCartType;
                        [self addMenuItem:menuItem
                                 quantity:quantity
                      specialInstructions:specialInstructions];
                        break;
                    default:
                        PCError(@"Serviced by flag is unknown");
                        break;
                        
                }
                
            }
            
            break;
        case CartTypeCart:
        {
            if(ORDER.cart.status != CartStatusOrdering){
                [UIAlertView alertWithTitle:NSLocalizedString(@"Order list is not editable", nil)
                                    message:NSLocalizedString(@"Order list has been placed. To add/modify/remove orders, you have to postpone the confirmation of your order by \"Change Placed Order\" button", nil)
                                   delegate:self
                                        tag:100];
                [self resetSelection];
                return;
            }
            
            self.lockReloadBadge = YES;
            [ORDER.cart addMenuItem:menuItem
                           quantity:quantity
                specialInstructions:specialInstructions
                            success:^(BOOL isOnlyLocal){
                                if(isOnlyLocal)
                                    TRANS.localDirty = YES;
                                else
                                    TRANS.dirty = YES;
                                self.selectedMenuItem = nil;
                                self.selectedQuantity = 0;
                                self.specialInstructions = nil;
                                [self cartAddAnimate];
                                
                            } failure:^{
                                [self resetSelection];
                                self.lockReloadBadge = NO;
                                
                                if(ORDER.merchant.isTableBase){
                                    [ORDER checkTable:ORDER.tableInfo
                                              success:^{
                                                  
                                              }
                                              failure:^(OrderStatus status){
                                                  
                                              }];
                                }
                            }];
        }
            break;
        case CartTypePickup:
        case CartTypeTakeout:
        case CartTypePickupServiced:
        case CartTypeDelivery:
        {
//            if(ORDER.cart.cartType == CartTypePickupServiced
//               && ORDER.cart.flagNumber == nil){
//                
//                self.selectedMenuItem = menuItem;
//                self.selectedQuantity = quantity;
//                self.specialInstructions = specialInstructions;
//                [self showTableNumber:YES]; //Only table falg number
//                
//            } else {
                self.lockReloadBadge = YES;
                [ORDER.cart addMenuItem:menuItem
                               quantity:quantity
                    specialInstructions:specialInstructions
                                success:^(BOOL isOnlyLocal){
                                    if(isOnlyLocal)
                                        TRANS.localDirty = YES;
                                    else
                                        TRANS.dirty = YES;
                                    self.selectedMenuItem = nil;
                                    self.selectedQuantity = 0;
                                    self.specialInstructions = nil;
                                    [self cartAddAnimate];
                                    
                                } failure:^{
                                    [self resetSelection];
                                    self.lockReloadBadge = NO;
                                }];
//            }
        }
            break;
        default:
            break;
    }
}

- (void)resetSelection
{
    self.selectedMenuItem = nil;
    self.selectedQuantity = 0;
    self.selectedIndexPath = nil;
    self.specialInstructions = nil;
}

- (IBAction)confirmButtonTouched:(id)sender
{
    if((ORDER.merchant.isServicedByStaff
       || ORDER.merchant.isTableBase)
       && [self.tableNumberTextField.text length] == 0){
        [self.tableNumberTextField becomeFirstResponder];
        return;
    }
    

    if(![ORDER isNearMerchant]){
        if(ORDER.merchant.isAbleTakeoutOrDelivery){
            [UIAlertView alertWithTitle:NSLocalizedString(@"It is Too Far to Order", nil)
                                message:NSLocalizedString(@"You can make an order at restaurant which is within 1 mile. You can make take-out or delivery order without restriction of distance (1 mile).", nil)];
        } else {
            [UIAlertView alertWithTitle:NSLocalizedString(@"It is Too Far to Order", nil)
                                message:NSLocalizedString(@"You can make an order at restaurant which is within 1 mile.", nil)];
        }
        return;
    }
    
    [self doneButtonTouched:sender];
    
    // !!!: Same code in doneButtonTouched
    if(self.tableInfo == nil
       && ORDER.cart.cartType == CartTypeUnknown){
        // TalbeNumber 또는 주문 타입이 결정되지 않은 경우
        return;
    }
    
    [self hideTableNumber:YES];
    
    if(ORDER.merchant.isTableBase){
        [ORDER checkTable:self.tableInfo
               assertMine:NO
                  success:^(){

//                      UIActionSheet *actionSeet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"", nil)
//                                                                              delegate:self
//                                                                     cancelButtonTitle:NSLocalizedString(@"", nil)
//                                                                destructiveButtonTitle:NSLocalizedString(@"", nil)
//                                                                     otherButtonTitles:nil];
//                      actionSeet.tag = 301;
//                      [actionSeet showInView:self.view];
                      
                      ORDER.cart.cartType = self.initialCartType;
                      if(self.selectedMenuItem != nil){
                          [self addMenuItem:self.selectedMenuItem quantity:self.selectedQuantity specialInstructions:self.specialInstructions];
                      } else {
                          // Just Open the order list - like Check - In button
                          [self orderButtonTouched:self];
                      }
                  }
                  failure:^(OrderStatus status){
                      if(status == OrderStatusCompleted){
                          [UIAlertView alertWithTitle:NSLocalizedString(@"Prior Table Orders Not Settled", nil)
                                              message:NSLocalizedString(@"Please wait for the restaurant to clear prior table orders. If table is not cleared for an extended period of time, please ask restaurant staff for assistance", nil)];
                      }
                  }];
    } else {
        if(self.selectedMenuItem != nil){
            [self addMenuItem:self.selectedMenuItem quantity:self.selectedQuantity specialInstructions:self.specialInstructions];
        } else {

        }
    }
}

- (IBAction)takeoutButtonTouched:(id)sender
{
    [self hideTableNumber:YES];
    
    ORDER.cart.cartType = CartTypeTakeout;
    
    if(self.selectedMenuItem != nil){
        [self addMenuItem:self.selectedMenuItem quantity:self.selectedQuantity specialInstructions:self.specialInstructions];
    } else {
        
    }
}

#pragma mark - OrderViewControllerDelegate
- (void)orderViewController:(OrderViewController *)orderViewController didTouchCheckoutButton:(id)sender
{
    // Payment possible anywhere
//    if(![ORDER isNearMerchant]){
//        [UIAlertView alertWithTitle:NSLocalizedString(@"It is too far to check Out", nil)
//                            message:NSLocalizedString(@"You can check out at restaurant which is within 1 mile.", nil)];
//        return;
//    }
    
    // Pickup - self/serviced check open flag
    // Sitdown check when add item flag
    if(!ORDER.merchant.isOpenHour){
        [UIAlertView alertWithTitle:NSLocalizedString(@"This restaurant is closed", nil)
                            message:NSLocalizedString(@"You can check out only when the restaurant is open.", nil)];
        return;
    }
    
    if(ORDER.tableInfo != nil){
        
        [APP.viewDeckController closeRightViewAnimated:YES
                                            completion:^(IIViewDeckController *controller, BOOL success) {
                                                if(success){
                                                    BillViewController *viewController = [BillViewController viewControllerFromNib];
                                                    [self.navigationController pushViewController:viewController animated:YES];
                                                }
                                            }];
    } else {
        
        switch(ORDER.cart.cartType){
            case CartTypePickupServiced:
            case CartTypePickup:
            case CartTypeTakeout:
            case CartTypeDelivery:
            {
                [APP.viewDeckController closeRightViewAnimated:YES
                                                    completion:^(IIViewDeckController *controller, BOOL success) {
                                                        if(success){
                                                            [self payWithCardButtonTouched:sender];
                                                        }
                                                    }];
            }
                break;
            case CartTypeUnknown:
            case CartTypeCart:
            default:
            {
                if(ORDER.order.orderNo >= 0){
                    [APP.viewDeckController closeRightViewAnimated:YES
                                                        completion:^(IIViewDeckController *controller, BOOL success) {
                                                            if(success){
                                                                [self payWithCardButtonTouched:sender];
                                                            }
                                                        }];
                }
            }
                break;
        }
    }
}

- (void)orderViewController:(OrderViewController *)orderViewController didTouchOrderDoneButton:(id)sender
{
    [APP.viewDeckController closeRightViewAnimated:YES
                                        completion:^(IIViewDeckController *controller, BOOL success) {
                                            if(success){
                                                TRANS.dirty = YES;
                                                [self.navigationController popToRootViewControllerAnimated:YES];
                                            }
                                        }];
}

- (void)orderViewController:(OrderViewController *)orderViewController didTouchOnlyCheckoutButton:(id)sender
{
    [APP.viewDeckController closeRightViewAnimated:YES
                                        completion:^(IIViewDeckController *controller, BOOL success) {
                                            if(success){
                                                TablesetViewController *viewController = [TablesetViewController viewControllerFromNib];
                                                [self.navigationController pushViewController:viewController animated:YES];
                                            }
                                        }];
}

- (void)orderViewController:(OrderViewController *)orderViewController didTouchCheckinButton:(id)sender
{
    [APP.viewDeckController closeRightViewAnimated:YES
                                        completion:
     ^(IIViewDeckController *controller, BOOL success) {
         if(success){
             [self showTableNumber:YES];
         }
     }];
}

- (CartType)orderViewControllerInitialCartType:(OrderViewController *)orderViewController
{
    return self.initialCartType;
}

- (IBAction)tableNumberCancelButton:(id)sender
{
    [self resetSelection];
    [self hideTableNumber:YES];
}

#pragma mark - UIPickerViewDataSource
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger count = [ORDER.tableList count];
    
    return MAX(count, 1);
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if([ORDER.tableList count] > row){
        TableInfo *table = [ORDER.tableList objectAtIndex:row];
        return table.tableNumber;
    }
    
    return @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if([ORDER.tableList count] > row){
        TableInfo *table = [ORDER.tableList objectAtIndex:row];
        self.tableInfo = table;
        self.tableNumberTextField.text = table.tableNumber;
    }
}

- (IBAction)toggleButtonTouched:(id)sender
{
    [self.tableNumberTextField resignFirstResponder];
    
    if(self.tableNumberTextField.inputView != nil){
        self.tableNumberTextField.inputView = nil;
        self.toggleBarButtonItem.title = NSLocalizedString(@"Picker", nil);
    } else {
        self.tableNumberTextField.inputView = self.tableNumberPickerView;
        self.toggleBarButtonItem.title = NSLocalizedString(@"Keyboard", nil);
    }
    
    [self.tableNumberTextField becomeFirstResponder];
}

// Table number Picker Done button touched
- (IBAction)doneButtonTouched:(id)sender
{
    if(ORDER.cart.cartType == CartTypePickup){
        // Pass this case
    } else {
        if(!ORDER.merchant.isTableBase && ORDER.merchant.servicedBy != ServicedByStaff){
            ORDER.cart.cartType = CartTypePickup;
        } else {
            if(self.tableNumberTextField.inputView == nil){
                // When the user input table number manually. Not by Picker view
                NSString *inputTableNumber = self.tableNumberTextField.text;
                
                if([inputTableNumber length] > 0){
                    
                    self.tableInfo = nil;
                    TableInfo *selectedTableInfo = nil;
                    for(TableInfo *tableInfo in ORDER.tableList){
                        
                        if([tableInfo.tableNumber isCaseInsensitiveEqual:inputTableNumber]){
                            selectedTableInfo = tableInfo;
                            break;
                        }
                    }
                    
                    if(selectedTableInfo == nil){
                        if(!ORDER.merchant.isTableBase && ORDER.merchant.servicedBy == ServicedByStaff){
                            [UIAlertView alertWithTitle:NSLocalizedString(@"Invalid Table Number", nil)
                                                message:NSLocalizedString(@"You have entered a table number outside the range established by the restaurant.  Please check and re-enter.", nil)];
                        } else {
                            [UIAlertView alertWithTitle:NSLocalizedString(@"Invalid Table Number", nil)
                                                message:NSLocalizedString(@"You have entered a table number outside the range established by the restaurant.  Please check and re-enter.", nil)];
                        }
                        [self.tableNumberTextField becomeFirstResponder];
                    }
                    
                    if(ORDER.merchant.isTableBase){
                        self.tableInfo = selectedTableInfo;
                    } else {
                        if(ORDER.merchant.servicedBy == ServicedByStaff){
                            ORDER.cart.cartType = CartTypePickupServiced;
                            ORDER.cart.flagNumber = selectedTableInfo.tableNumber;
                        } else {
                            PCError(@"Merchant does not support table service : Keyboard");
                        }
                    }
                } else {
                    // Nothing to do
                }
            } else {
                // By picker view
                NSInteger selRow = [self.tableNumberPickerView selectedRowInComponent:0];
                
                if([ORDER.tableList count] > selRow){
                    TableInfo *table = [ORDER.tableList objectAtIndex:selRow];
                    
                    if(ORDER.merchant.isTableBase){
                        self.tableInfo = table;
                    } else {
                        if(ORDER.merchant.servicedBy == ServicedByStaff){
                            ORDER.cart.cartType = CartTypePickupServiced;
                            ORDER.cart.flagNumber = table.tableNumber;
                        } else {
                            PCError(@"Merchant does not support table service : Picker");
                        }
                    }
                    self.tableNumberTextField.text = table.tableNumber;
                }
            }
        }
    }
    
    // !!!: Same code in confirmButtonTouched
    if(self.tableInfo == nil
       && ORDER.cart.cartType == CartTypeUnknown){
        // TalbeNumber 또는 주문 타입이 결정되지 않은 경우
    } else {
        [self.tableNumberTextField resignFirstResponder];
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if([ORDER.tableList count] == 0){
//        [self requestTableLables];
    }
}

#define ANIMA_DURATION 0.55f

- (void)cartAddAnimate{

    // grab the cell using indexpath
    MenuItemCell *cell = (MenuItemCell *)[self.tableView cellForRowAtIndexPath:self.selectedIndexPath];
    // grab the imageview using cell
    UIView *container = cell.imageContainerView;

    CGRect rect = [APP.window convertRect:container.frame fromView:container.superview];
//    rect = CGRectMake(5, (rect.origin.y*-1)-10, imgV.frame.size.width, imgV.frame.size.height);
//    NSLog(@"rect is %f,%f,%f,%f",rect.origin.x,rect.origin.y,rect.size.width,rect.size.height);
    
    // create new duplicate image
    
    UIView *aniView = [[UIView alloc] initWithFrame:rect];
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[cell.menuImageButton imageForState:UIControlStateNormal]];
    imgView.contentMode = cell.menuImageButton.contentMode;
    imgView.clipsToBounds = YES;
    imgView.frame = CGRectMake(0.0f, 0.0f, rect.size.width, rect.size.height);
    imgView.backgroundColor = [UIColor colorWithR:244.0f
                                                 G:239.0f
                                                 B:234.0f];
    [aniView addSubview:imgView];
    
    UIImageView *frameView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"edge_menu_frame"]];
    frameView.frame = CGRectMake(0.0f, 0.0f, rect.size.width, rect.size.height);
    [aniView addSubview:frameView];
    
    [APP.window addSubview:aniView];
    
    // begin ---- apply position animation
    CAKeyframeAnimation *pathAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    pathAnimation.calculationMode = kCAAnimationPaced;
    pathAnimation.fillMode = kCAFillModeForwards;
    pathAnimation.removedOnCompletion = NO;
    pathAnimation.duration=ANIMA_DURATION;
    [pathAnimation setValue:aniView forKey:@"animationView"];
    pathAnimation.delegate=self;
    
    // tab-bar right side item frame-point = end point
    CGFloat rightEndX = CGRectGetMaxX(self.view.frame);
    CGPoint endPoint = CGPointMake(rightEndX - 30.0f, 42.0f);
    
    CGMutablePathRef curvedPath = CGPathCreateMutable();
    CGPathMoveToPoint(curvedPath, NULL, aniView.center.x, aniView.center.y);
    CGPathAddCurveToPoint(curvedPath, NULL, endPoint.x, aniView.frame.origin.y, endPoint.x, aniView.frame.origin.y, endPoint.x, endPoint.y);
    pathAnimation.path = curvedPath;
    CGPathRelease(curvedPath);
    // end ---- apply position animation
    
    // apply transform animation
    CABasicAnimation *basic=[CABasicAnimation animationWithKeyPath:@"transform"];
    [basic setToValue:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.01, 0.01, 0.01)]];
    [basic setAutoreverses:NO];
    basic.cumulative = YES;
    [basic setDuration:ANIMA_DURATION + 0.1f];
    basic.removedOnCompletion = NO;
    
    
    [aniView.layer addAnimation:pathAnimation forKey:@"curveAnimation"];
    [aniView.layer addAnimation:basic forKey:@"transform"];
    
}

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag
{
    UIImageView *view = [theAnimation valueForKey:@"animationView"];
    if (view) {
        [view removeFromSuperview];
        [theAnimation setValue:nil forKey:@"animationView"];
    }
    [self reloadBadgeNumber:[NSNumber numberWithBool:YES]];

}

- (void)reloadBadgeNumber:(NSNumber *)withBounce
{
    NSInteger badgeCount = 0;
    if(ORDER.cart.status == CartStatusOrdering) badgeCount = ORDER.cart.quantities;
    
    NSString *countString = nil;
    
    if((ORDER.cart.quantities == 0 && ORDER.order.status == OrderStatusConfirmed) ||
       (!ORDER.merchant.isTableBase && ORDER.order.status == OrderStatusDraft)){
        if(ORDER.merchant.isAblePayment){
            countString = NSLocalizedString(@"Check", nil);
        } else {
            countString = NSLocalizedString(@"Done", nil);
        }
    } else {
        if(badgeCount > 0)
            countString = [NSString stringWithFormat:@"%d", badgeCount];
    }

    self.navigationItem.rightBarButtonItem.button.badgeValue = countString;

    if([withBounce boolValue]){
        [APP.viewDeckController previewBounceView:IIViewDeckRightSide];
    }

    self.lockReloadBadge = NO;
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 100:
            [APP.viewDeckController openRightViewAnimated:YES];
            break;
        default:
            break;
    }
}

- (IBAction)payWithCardButtonTouched:(id)sender
{
#ifdef FLURRY_ENABLED
    [Flurry logEvent:NSStringFromSelector(_cmd)];
#endif
    
    [self.view endEditing:YES];
    
    self.tableView.editing = NO;
    
    if((ORDER.order.orderType == OrderTypeDelivery || ORDER.cart.cartType == CartTypeDelivery)
       && (ORDER.merchant.deliveryMinOrderAmount > ORDER.cart.amount)){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Too Small for Delivery Order", nil)
                            message:[NSString stringWithFormat:NSLocalizedString(@"You should order at least %@ for delivery ordering", nil),
         [[NSNumber numberWithCurrency:ORDER.merchant.deliveryMinOrderAmount] currencyString]]];
         return;
    }
    
    if(ORDER.cart.status == CartStatusOrdering
       && ORDER.order.status == OrderStatusDraft){
        
//        if(ORDER.order.subTotal == 0){
        if([ORDER.cart.lineItems count] == 0){
            [UIAlertView alert:NSLocalizedString(@"Please add at least one item.", nil)];
            return;
        }
        
    } else {
//        if(ORDER.cart.amount == 0){
        if([ORDER.cart.lineItems count] == 0){
            [UIAlertView alert:NSLocalizedString(@"Please add at least one item.", nil)];
            return;
        }
        
        if(ORDER.order == nil){
            ORDER.order = [[Order alloc] initWithDictionary:nil
                                                   merchant:ORDER.merchant];
        }
        
        // Update order
        if(ORDER.order.status == OrderStatusDraft ||
           ORDER.order.status == OrderStatusUnknown){
            
            ORDER.order.subTotal = ORDER.cart.amount;
            ORDER.order.taxes = ORDER.cart.taxAmount;
            ORDER.order.status = OrderStatusDraft;
            
            switch(ORDER.cart.cartType){
                case CartTypeCart:
                    ORDER.order.orderType = OrderTypeCart;
                    break;
                case CartTypeDelivery:
                    ORDER.order.orderType = OrderTypeDelivery;
                    break;
                case CartTypeTakeout:
                    ORDER.order.orderType = OrderTypeTakeout;
                    break;
                case CartTypePickup:
                case CartTypePickupServiced:
                    ORDER.order.orderType = OrderTypePickup;
                    break;
                default:
                    ORDER.order.orderType = OrderTypeUnknown;
                    break;
            }
        }
    }
    [self proceedPayment];
}

- (void)proceedPayment
{
    if(CRED.isSignedIn){
        self.mobileCardList = REMOTE.cards;
        if(!REMOTE.isCardFetched){
            self.cardListWaiting = YES;
            return;
        }
    }
    
    Payment *payment = [[Payment alloc] init];
    //Moved to DeliveryViewController
//    if((ORDER.order.orderType == OrderTypeDelivery || ORDER.cart.cartType == CartTypeDelivery)
//       && (ORDER.merchant.deliveryFee > 0)){
//        payment.deliveryFeeAmount = ORDER.merchant.deliveryFee;
//    }
    payment.merchantNo = ORDER.order.merchantNo;
    payment.taxAmount = ORDER.order.taxes;
    payment.subTotal = ORDER.order.subTotal;
    payment.orderAmount = payment.subTotal + payment.taxAmount;
    payment.payAmount = payment.orderAmount;
    payment.currency = FORMATTER.currencyFormatter.currencyCode;
    payment.payDate = [NSDate date];
    
    UIViewController *viewController = nil;
    
    if(ORDER.order.orderType == OrderTypeDelivery || ORDER.cart.cartType == CartTypeDelivery){
        viewController = [DeliveryViewController viewControllerFromNib];
        ((DeliveryViewController *)viewController).payment = payment;
        ((DeliveryViewController *)viewController).disposeOrderWhenBack = YES;
    } else {
        if([self.mobileCardList count] > 0 || CRED.customerInfo.credit > 0){
            
            viewController = [PaymentMethodViewController viewControllerFromNib];
            ((PaymentMethodViewController *)viewController).payment = payment;
            ((PaymentMethodViewController *)viewController).paymentMethodList = self.mobileCardList;
            ((PaymentMethodViewController *)viewController).disposeOrderWhenBack = YES;
            
        } else {
            viewController = [NewCardViewController viewControllerFromNib];
            ((NewCardViewController *)viewController).payment = payment;
            ((NewCardViewController *)viewController).disposeOrderWhenBack = YES;
        }
    }
    [self.navigationController pushViewController:viewController animated:YES];
}

- (NSMutableArray *)mobileCardList
{
    if(_mobileCardList == nil){
        if(CRED.isSignedIn){
            _mobileCardList = REMOTE.cards; // TODO:If card does not exist? -> Blocking?
        } else {
            _mobileCardList = [MobileCard listAll];
        }
    }
    return _mobileCardList;
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(actionSheet.tag){
        case 201:
            break;
        case 301:
            break;
        default:
            break;
    }
}

#pragma mark - UIPageViewControllerDelegate 
#pragma mark optional
- (void)pageViewController:(UIPageViewController *)pageViewController
willTransitionToViewControllers:(NSArray *)pendingViewControllers
{
}

- (void)pageViewController:(UIPageViewController *)pageViewController
        didFinishAnimating:(BOOL)finished
   previousViewControllers:(NSArray *)previousViewControllers
       transitionCompleted:(BOOL)completed
{
}

//- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController
//                   spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation
//{
//    return UIPageViewControllerSpineLocationMax;
//}

#pragma mark - UIPageViewControllerDataSource
#pragma mark required
- (MenuPhotoViewController *)pageViewController:(UIPageViewController *)pageViewController
             viewControllerBeforeViewController:(MenuPhotoViewController *)viewController
{
    return [self viewControllerBy:-1
                      atIndexPath:viewController.indexPath];
}

- (MenuPhotoViewController *)pageViewController:(UIPageViewController *)pageViewController
              viewControllerAfterViewController:(MenuPhotoViewController *)viewController
{
    return [self viewControllerBy:1
                      atIndexPath:viewController.indexPath];
}

- (MenuPhotoViewController *)viewControllerBy:(NSInteger)direction
                                  atIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *newIndexPath = nil;
    
    MenuCategory *menuCategory = nil;
    id item = nil;
    
    menuCategory = [MENUPAN.menuList objectAtIndex:indexPath.section];
    
    if(direction > 0){
        if([menuCategory.items count] > (indexPath.row + 1)){
            //Next row
            newIndexPath = [NSIndexPath indexPathForRow:(indexPath.row + 1)
                                              inSection:indexPath.section];
        } else if([MENUPAN.menuList count] > (indexPath.section + 1)){
            //Next section's first row
            menuCategory = [MENUPAN.menuList objectAtIndex:(indexPath.section + 1)];
            newIndexPath = [NSIndexPath indexPathForRow:0
                                              inSection:(indexPath.section + 1)];
        }
    } else if(direction < 0){
        if(indexPath.row > 0){
            //Next row
            newIndexPath = [NSIndexPath indexPathForRow:(indexPath.row - 1)
                                              inSection:indexPath.section];
        } else if(indexPath.section > 0){
            //Next section's first row
            menuCategory = [MENUPAN.menuList objectAtIndex:(indexPath.section - 1)];
            newIndexPath = [NSIndexPath indexPathForRow:([menuCategory.items count] - 1)
                                              inSection:(indexPath.section - 1)];
        }
    }
    
    if(newIndexPath != nil){
        item = [menuCategory.items objectAtIndex:newIndexPath.row];
        
        MenuPhotoViewController *newViewController = [MenuPhotoViewController viewControllerFromNib];
        newViewController.indexPath = newIndexPath;
        newViewController.item = item;
        newViewController.category = menuCategory;
        newViewController.delegate = self;
        return newViewController;

    } else {
        return nil;
    }
}


#pragma mark optional
//- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
//{
//    return 10;
//}
//
//- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
//{
//    return 3;
//}

#pragma mark - PhotoViewControllerDelegate
- (void)menuPhotoViewController:(MenuPhotoViewController *)viewController
didSelectAddItemButtonAtIndexPath:(NSIndexPath *)indexPath
{
    [self addMenuItemAtIndexPath:indexPath];
}

- (void)menuPhotoViewController:(MenuPhotoViewController *)viewController
 didTouchCloseButtonAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 if(indexPath != nil){
                                     [self tableView:nil
                             didSelectRowAtIndexPath:indexPath];
                                 } else {
                                     [self.tableView deselectRowAtIndexPath:self.selectedIndexPath
                                                                   animated:YES];
                                 }
                             }];
}

- (MenuItemCell *)offScreenMenuItemCell
{
    if(_offScreenMenuItemCell == nil){
        _offScreenMenuItemCell = [MenuItemCell cell];
    }
    return _offScreenMenuItemCell;
}

- (MenuSubCategoryCell *)offScreenMenuSubCategoryCell
{
    if(_offScreenMenuSubCategoryCell == nil){
        _offScreenMenuSubCategoryCell = [MenuSubCategoryCell cellWithNibName:MENU_REL_XIB_NAME
                                                                     atIndex:1];
    }
    return _offScreenMenuSubCategoryCell;
}

#pragma mark - UIScrollViewDelegate


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView == self.shortcutScrollView){
        
        NSUInteger currentPageIndex = (NSUInteger)floor((scrollView.contentOffset.x + (scrollView.width / 2)) / self.widthPerPage);
        currentPageIndex++;
        
        if(self.selectedPageIndex != currentPageIndex){
            self.selectedPageIndex = currentPageIndex;
            for(UIButton *dotButton in self.pageDotContainer.subviews){
                if(dotButton.tag > 0){
                    dotButton.selected = (dotButton.tag == self.selectedPageIndex);
                }
            }
        }
    } else {
        
        if(self.shortcutScrollView.hidden || self.holdSpyScrolling){
            return;
        }
        
        CGPoint offset = scrollView.contentOffset;
        
        offset.y += 3.0f;
        offset.x = 120.0f;
        
//        PCLog(@"Offset : %@",NSStringFromCGPoint(offset));
        
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:offset];
        
        if(indexPath == nil){
            offset.y += 26.0f;
            indexPath = [self.tableView indexPathForRowAtPoint:offset];
        }
        
//        PCLog(@"IndexPath section %@, %d", indexPath, indexPath.section);
        
        if(self.selectedSectionIndex != indexPath.section){
            self.selectedSectionIndex = indexPath.section;
            
    //        UIButton *sectionButtonView = (UIButton *)[self.shortcutScrollView viewWithTag:self.selectedSectionIndex];
            
            NSArray *subviews = [self.shortcutScrollView subviews];
            
            UIButton *sectionButtonView = nil;
            UIButton *selectedButtonView = nil;
            for(UIButton *aButton in subviews){
                if(aButton.tag >= 0){ //Only Button
                    if(aButton.tag == self.selectedSectionIndex){
                        sectionButtonView = aButton;
                    }
                    if(aButton.selected){
                        selectedButtonView = aButton;
                    }
                }
            }
            
            CGRect backFrame = self.selectedSectionBackground.frame;
            backFrame.origin.x = sectionButtonView.x + SEL_BACK_IMAGEVIEW_MARGIN;
            backFrame.origin.y = 4.0f;
            backFrame.size.width = sectionButtonView.width - (SEL_BACK_IMAGEVIEW_MARGIN * 2);
            
            [UIView animateWithDuration:0.3f
                                  delay:0.0f
                                options:UIViewAnimationOptionBeginFromCurrentState
                             animations:^{
                                 
                                 self.selectedSectionBackground.frame = backFrame;
                                 self.selectedSectionBgWidthConstraint.constant = backFrame.size.width;
                                 
                                 selectedButtonView.selected = NO;
                                 CGPoint curOffset = self.shortcutScrollView.contentOffset;
                                 if(backFrame.origin.x < curOffset.x){
                                     self.shortcutScrollView.contentOffset = CGPointMake(sectionButtonView.x, curOffset.y);
                                 } else if (CGRectGetMaxX(sectionButtonView.frame) > (curOffset.x + self.shortcutScrollView.width)){
                                     self.shortcutScrollView.contentOffset = CGPointMake(CGRectGetMaxX(sectionButtonView.frame) - self.shortcutScrollView.width, curOffset.y);
                                 }
                                 
                             } completion:^(BOOL finished) {
                                 if(finished){
                                     sectionButtonView.selected = YES;
                                 }
                             }];
        }
    }
}

//- (RestaurantSummaryView *)restaurantSummaryView
//{
//    if(_restaurantSummaryView == nil){
//        _restaurantSummaryView = [RestaurantSummaryView viewWithNibName:@"RestaurantSummaryView"];
//        
////        NSDictionary *views = @{@"restaurantSummaryView" : _restaurantSummaryView};
////        
////        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[restaurantSummaryView]|"
////                                                                       options:0
////                                                                       metrics:nil
////                                                                         views:views];
////        [self.tableView addConstraints:constraints];
////        
////        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[restaurantSummaryView]|"
////                                                              options:0
////                                                              metrics:nil
////                                                                views:views];
////        [self.tableView addConstraints:constraints];
//        
//    }
//    
//    return _restaurantSummaryView;
//}

@end
