//
//  MailMeViewController.h
//  RushOrder
//
//  Created by Conan on 11/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "Payment.h"

@protocol MailMeViewControllerDelegate;

@interface MailMeViewController : PCViewController
@property (weak, nonatomic) id<MailMeViewControllerDelegate> delegate;

@property (strong, nonatomic) Payment *payment;
@property (weak, nonatomic) IBOutlet UITextField *mailTextField;
@property (weak, nonatomic) IBOutlet NPToggleButton *autoMailingButton;


@end

@protocol MailMeViewControllerDelegate <NSObject>
@optional
- (void)mailMeViewController:(MailMeViewController *)viewController didTouchDoneButton:(id)sender;
@end
