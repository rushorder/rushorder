//
//  ProfileViewController.m
//  RushOrder
//
//  Created by Conan on 2/20/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import "ProfileViewController.h"
#import "PhoneNumberFormatter.h"
#import "ChangePasswordViewController.h"
#import "PCCredentialService.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import <SDWebImage/UIImageView+WebCache.h>


@interface ProfileViewController ()
{
    int _textFieldSemaphore;
    __strong PhoneNumberFormatter *_phoneNumberFormatter;
}

@property (weak, nonatomic) IBOutlet PCTextField *nameTextField;
@property (weak, nonatomic) IBOutlet PCTextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet PCTextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet PCTextField *birthdayTextField;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UILabel *pointLabel;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet UIToolbar *dateToolBar;
@property (strong, nonatomic) NSDate *birthday;
@property (copy, nonatomic) CustomerInfo *editingCustomerInfo;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *pointGuideLabel;
@property (weak, nonatomic) IBOutlet NPStretchableButton *phoneVerifyButton;
@property (weak, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NPStretchableButton *saveButton;
@property (weak, nonatomic) IBOutlet UIView *phoneNumberLineView;
@end

@implementation ProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _textFieldSemaphore = 0;
        _phoneNumberFormatter = [[PhoneNumberFormatter alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(CRED.customerInfo == nil){
        PCError(@"Customer Info is null on Profile Page - Something goes wrong");
    } else {
//        [self requestMyCredit];
    }
    
    self.title = NSLocalizedString(@"My Profile", nil);
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Cancel",nil)
                                                                             target:self
                                                                             action:@selector(closeButtonTouched:)];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Save",nil)
                                                                              target:self
                                                                              action:@selector(saveButtonTouched:)];
    self.birthdayTextField.inputView = self.datePicker;
    self.birthdayTextField.inputAccessoryView = self.dateToolBar;
    
    self.editingCustomerInfo = CRED.customerInfo;
    
    self.datePicker.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    [self fillCustomer];
}

- (void)fillCustomer
{
    if([self.editingCustomerInfo.firstName length] > 0 || [self.editingCustomerInfo.lastName length] > 0){
        self.nameTextField.text = self.editingCustomerInfo.firstName;
        self.lastNameTextField.text = self.editingCustomerInfo.lastName;
    } else {
        if([self.editingCustomerInfo.name length] > 0){
            NSArray *nameArray = [self.editingCustomerInfo.name componentsSeparatedByString:@" "];
            if([nameArray count] == 1){
                self.nameTextField.text = [nameArray objectAtIndex:0];
            } else {
                self.nameTextField.text = [nameArray objectAtIndex:0];
                self.lastNameTextField.text = [nameArray objectAtIndex:1];
            }
        }
    }
    
    self.birthday = [self.editingCustomerInfo.birthday dateWithYMD];
    self.phoneNumberTextField.text = self.editingCustomerInfo.phoneNumber;
    [self phoneNumberValueChanged:self.phoneNumberTextField];
    
    if(self.birthday == nil){
        self.datePicker.date = [NSDate date];
    } else {
        self.datePicker.date = self.birthday;
    }
    self.birthdayTextField.text = [self.birthday gmtDateStringForHumanReadableFormat];
    self.pointLabel.text = [[NSNumber numberWithCurrency:self.editingCustomerInfo.credit] pointString];
    
    if(self.editingCustomerInfo.photo != nil){
        self.photoImageView.image = self.editingCustomerInfo.photo;
    } else {
        
        if(CRED.customerInfo.photo != nil){
            self.photoImageView.image = CRED.customerInfo.photo;
        } else {
            if(CRED.customerInfo.photoURL == nil){
                self.photoImageView.image = [UIImage imageNamed:@"placeholder_profile_photo_light"];
            } else {
                [self.photoImageView sd_setImageWithURL:CRED.customerInfo.photoURL
                                    placeholderImage:[UIImage imageNamed:@"placeholder_profile_photo_light"]
                                             options:0
                                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                               if(error != nil){
                                                   // Error
                                                   self.photoImageView.image = CRED.customerInfo.photo;
                                               } else {
                                                   // Success
                                                   CRED.customerInfo.photo = image;
                                               }
                                           }];
            }
        }
    }
    
    [self.scrollView setContentSizeWithBottomView:self.saveButton];
}

- (void)closeButtonTouched:(id)sender
{
    if([self.delegate respondsToSelector:@selector(signInCustomerViewControllerNeedDissmissingViewController:)]){
        [self.delegate signInCustomerViewControllerNeedDissmissingViewController:self];
    } else {
        [self dismissViewControllerAnimated:YES
                                 completion:^{

                                 }];
    }
}

- (void)changePasswordButtonTouched:(id)sender
{
    ChangePasswordViewController *viewController = [ChangePasswordViewController viewControllerFromNib];
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    if(![VALID validate:self.nameTextField
                  title:NSLocalizedString(@"First Name is Required", nil)
                message:NSLocalizedString(@"Enter your First Name for your account. Recommand your real name", nil)]){
        return;
    }
    
    if(![VALID validate:self.lastNameTextField
                  title:NSLocalizedString(@"Last Name is Required", nil)
                message:NSLocalizedString(@"Enter your Last Name for your account. Recommand your real name", nil)]){
        return;
    }
    
    self.editingCustomerInfo.firstName = self.nameTextField.text;
    self.editingCustomerInfo.lastName = self.lastNameTextField.text;
    self.editingCustomerInfo.birthday = [self.birthday gmtDateStringWithDashedFormat];
    self.editingCustomerInfo.phoneNumber = self.phoneNumberTextField.text;
    
    RequestResult result = RRFail;
    result = [CRED requestUpdateProfile:self.editingCustomerInfo
                        completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              [CRED.customerInfo updateWithDictionary:response];
                              if(self.editingCustomerInfo.photo != nil){
                                  CRED.customerInfo.photo = self.editingCustomerInfo.photo;
                              }
                              self.editingCustomerInfo.photo = nil;
                              [CRED.customerInfo save];

                              // Same name of profile picture
                              [[SDWebImageManager sharedManager].imageCache removeImageForKey:[CRED.customerInfo.photoURL absoluteString]];
                              
                              [self closeButtonTouched:sender];
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while Updating your proifle", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (IBAction)datePickerValueChanged:(id)sender
{
    self.birthday = [self.datePicker.date gmtDateInHour:0];
    self.birthdayTextField.text = [self.birthday gmtDateStringForHumanReadableFormat];
}

- (IBAction)editButtonTouched:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"Choose From Album", nil)
                                                    otherButtonTitles:NSLocalizedString(@"Take Photo", nil)
                                  , nil];
    [actionSheet showInView:self.view];
    
}

- (BOOL)startMediaBrowserFromViewController: (UIViewController*) controller
                               usingDelegate: (id <UIImagePickerControllerDelegate,
                                               UINavigationControllerDelegate>) delegate {
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypePhotoLibrary] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.navigationBar.tintColor = [UIColor c12Color];
    mediaUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    // Displays saved pictures and movies, if both are available, from the
    // Camera Roll album.
    mediaUI.mediaTypes =
    [UIImagePickerController availableMediaTypesForSourceType:
     UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    mediaUI.allowsEditing = YES;
    
    mediaUI.delegate = delegate;
    
    [controller presentViewController:mediaUI
                             animated:YES
                           completion:^{
                               
                           }];
    return YES;
}

- (BOOL) startCameraFromViewController: (UIViewController*) controller
                         usingDelegate: (id <UIImagePickerControllerDelegate,
                                         UINavigationControllerDelegate>) delegate {
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeCamera] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    // Displays saved pictures and movies, if both are available, from the
    // Camera Roll album.
    mediaUI.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    mediaUI.allowsEditing = YES;
    
    mediaUI.delegate = delegate;
    
    [controller presentViewController:mediaUI
                             animated:YES
                           completion:^{
                               
                           }];
    return YES;
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    UIImage *originalImage, *editedImage, *imageToUse;
    
    // Handle a still image picked from a photo album
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0)
        == kCFCompareEqualTo) {
        
        editedImage = (UIImage *) [info objectForKey:
                                   UIImagePickerControllerEditedImage];
        
        originalImage = (UIImage *) [info objectForKey:
                                     UIImagePickerControllerOriginalImage];
        
        if (editedImage) {
            imageToUse = editedImage;
        } else {
            imageToUse = originalImage;
        }
        
        self.editingCustomerInfo.photo = imageToUse;
    }
    
    self.photoImageView.image = self.editingCustomerInfo.photo;
    self.photoImageView.hidden = NO;
    self.photoImageView.alpha = 0.0;
    [picker dismissViewControllerAnimated:YES
                               completion:^{
                                   [UIView animateWithDuration:0.4f
                                                    animations:^{
                                                        self.photoImageView.alpha = 1.0f;
                                                    }];
                               }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES
                               completion:^{

                               }];
}

- (IBAction)cancelButtonTouched:(id)sender
{
    [self.birthdayTextField resignFirstResponder];
}

- (IBAction)doneButtonTouched:(id)sender
{
    [self datePickerValueChanged:sender];
    [self.birthdayTextField resignFirstResponder];
}

- (IBAction)verifyPhoneNumberButtonTouched:(id)sender
{
    VerificationViewController *viewController = [VerificationViewController viewControllerFromNib];
    viewController.delegate = self;
    
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)viewController:(VerificationViewController *)viewController
   successRegistration:(BOOL)success
{
    if(success){
        [self fillCustomer];
//        [self requestMyCredit];
    }
}

- (void)requestMyCredit
{
    if(CRED.customerInfo != nil){
        RequestResult result = RRFail;
        result = [CRED requestMyCreditWithCompletionBlock:
                  ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                      if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                          switch(response.errorCode){
                              case ResponseSuccess:
                                  CRED.customerInfo.credit = [response integerForKey:@"credit_balance"];
                                  
                                  [self updateViews];
                                  break;
                              case ResponseErrorGeneral:{
                                  NSString *message = [response objectForKey:@"message"];
                                  if([message isKindOfClass:[NSString class]]){
                                      [UIAlertView alert:message];
                                  }
                                  break;
                              }
                              default:{
                                  NSString *message = [response objectForKey:@"message"];
                                  if([message isKindOfClass:[NSString class]]){
                                      [UIAlertView alert:message];
                                  } else {
                                      [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting my points", nil)];
                                      
                                  }
                                  break;
                              }
                          }
                      } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                          [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                          //Signed out in requestMyCreditWithCompletionBlock
                      } else {
                          if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                              [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                          } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                              [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                                  ,statusCode]];
                          }
                      }
                      [SVProgressHUD dismiss];
                  }];
        switch(result){
            case RRSuccess:
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                break;
            case RRParameterError:
                [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                break;
            default:
                break;
        }
    } else {
        [self updateViews];
    }
}

- (void)updateViews
{
    if(CRED.customerInfo != nil){
        
        self.pointLabel.text = [[NSNumber numberWithCurrency:CRED.customerInfo.credit] pointString];
    } else {
        self.pointLabel.text = @"0P";
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(actionSheet.cancelButtonIndex != buttonIndex){
        if(actionSheet.destructiveButtonIndex == buttonIndex){
            if([self startMediaBrowserFromViewController:self
                                           usingDelegate:self]){
            }
        } else {
            if([self startCameraFromViewController:self
                                     usingDelegate:self]){
            }
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isKindOfClass:[PCTextField class]]){
        [textField resignFirstResponder];
        [[(PCTextField *)textField nextField] becomeFirstResponder];
    }
    return YES;
}

- (IBAction)phoneNumberValueChanged:(UITextField *)sender
{
    if(_textFieldSemaphore) return;
    
    _textFieldSemaphore = 1;
    
    NSString *locale = [[NSLocale currentLocale] localeIdentifier];
    sender.text = [_phoneNumberFormatter format:sender.text
                                     withLocale:locale];
    _textFieldSemaphore = 0;
}

@end
