//
//  TogoAuthViewController.h
//  RushOrder
//
//  Created by Conan on 7/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "Receipt.h"
#import "Order.h"
#import "FavoriteOrder.h"
#import "Payment.h"
#import "PaymentInfoTableViewCell.h"
#import "DeliveryInfoTableViewCell.h"
#import "DeliveryViewController.h"
#import "CloudPhotoCollectionViewController.h"

extern NSString * const FavoriteOrderInsertedNotification;
extern NSString * const FavoriteOrderUpdatedNotification;
extern NSString * const NeedToShowAppRatingNotification;

@interface TogoAuthViewController : PCTableViewController
<
UIActionSheetDelegate,
PaymentInfoTableViewCellDelegate,
UIImagePickerControllerDelegate,
UINavigationControllerDelegate,
DeliveryInfoTableViewCellDelegate,
DeliveryViewControllerDelegate,
CloudPhotoCollectionViewDelegate
>

@property (strong, nonatomic) Receipt *receipt;
@property (strong, nonatomic) Order *order;
@property (strong, nonatomic) Payment *payment;
@property (strong, nonatomic) MobileCard *mobileCard;

@property (nonatomic, getter = isRapidRO) BOOL rapidRO;
@property (nonatomic, strong) FavoriteOrder *favoriteOrder;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (weak, nonatomic) IBOutlet UILabel *orderTypeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *orderTypeImageView;
@end
