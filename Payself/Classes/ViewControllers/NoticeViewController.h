//
//  NoticeViewController.h
//  RushOrder
//
//  Created by Conan on 6/13/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"

@interface NoticeViewController : PCViewController

@property (nonatomic) PCSerial noticeNo;
@property (nonatomic) NSString *format;
@property (copy, nonatomic) NSString *content;
@property (strong, nonatomic) IBOutlet UIWebView *noticeWebView;
@end