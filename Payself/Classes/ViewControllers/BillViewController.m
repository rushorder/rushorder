//
//  BillViewController.m
//  RushOrder
//
//  Created by Conan on 2/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "BillViewController.h"
#import "PaymentMethodViewController.h"
#import "NewCardViewController.h"
#import "MobileCard.h"
#import "PCCredentialService.h"
#import "Merchant.h"
#import "OrderManager.h"
#import "NPBatchTableView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "RemoteDataManager.h"

#define USE_SIMPLE_TIP

@interface BillViewController ()

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet NPBatchTableView *tableView;
@property (strong, nonatomic) Payment *payment;
@property (strong, nonatomic) NSMutableArray *mobileCardList;

@property (weak, nonatomic) IBOutlet UIButton *selectCardButton;

@property (weak, nonatomic) IBOutlet PCCurrencyTextField *subTotalTextField;
@property (weak, nonatomic) IBOutlet PCCurrencyTextField *taxesTextField;
@property (weak, nonatomic) IBOutlet PCCurrencyTextField *totalTextField;
@property (weak, nonatomic) IBOutlet PCCurrencyTextField *payAmountTextField;
@property (weak, nonatomic) IBOutlet PCCurrencyTextField *paidAmountTextField;
@property (weak, nonatomic) IBOutlet PCCurrencyTextField *discountTextField;
@property (weak, nonatomic) IBOutlet UILabel *discountTitleTextField;

@property (weak, nonatomic) IBOutlet UILabel *paidAmountTitleLabel;
@property (strong, nonatomic) IBOutlet UIToolbar *textFieldAccessoryView;
@property (weak, nonatomic) IBOutlet UIView *bottomContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@property (weak, nonatomic) IBOutlet UILabel *merchantNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tableNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *merchantAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *restaurantTypeLabel;

@property (nonatomic) PCCurrency tempPayAmount;
@property (nonatomic) PCCurrency tempDiscountAmount;

@property (strong, nonatomic) BillOrderCell *offScreenBillOrderCell;

@property (nonatomic) PCCurrency prevPayAmount;

@property (nonatomic, getter = isDirty) BOOL dirty;
@property (nonatomic, getter = isDefaultTipApplied) BOOL defaultTipApplied;

@property (nonatomic, getter = isTipChecked) BOOL tipChecked;

@property (nonatomic, strong) NSMutableSet *selectedLineItemIds;

@property (nonatomic, getter = isCardListWaiting) BOOL cardListWaiting;

@property (nonatomic) PCCurrency paymentSubtotal;
@property (nonatomic) PCCurrency paymentTax;
@end

@implementation BillViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(orderListChanged:)
                                                     name:LineItemChangedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(cardListChanged:)
                                                     name:CardListChangeNotification
                                                   object:nil];
        
        self.dirty = YES;
        
        if(CRED.isSignedIn){
            self.mobileCardList = REMOTE.cards;
        } else {
            self.mobileCardList = [MobileCard listAll];
        }
        
        self.tipChecked = NO;
        
        for(LineItem *lineItem in ORDER.order.lineItems){
            lineItem.splitExcept = !lineItem.isMine;
            if(lineItem.isPaid){
                lineItem.splitExcept = YES;
            }
            if(!lineItem.splitExcept){
                [self.selectedLineItemIds addObject:[NSNumber numberWithSerial:lineItem.lineItemNo]];
            }
        }
        
        [self requestMyCredit];
        
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.tableView removeObserver:self
                        forKeyPath:@"editing"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Bill", nil);
    
    self.payAmountTextField.inputAccessoryView = self.textFieldAccessoryView;

//    self.tableView.rowHeight = 65.0f;
    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    
    if([ORDER.order.lineItems count] > 0){
        
        self.payAmountTextField.enabled = NO;
        self.payAmountTextField.background = nil;
        
    } else {

    }
    
    self.tableView.tableHeaderView = self.headerView;
    
    [self.tableView addObserver:self
                     forKeyPath:@"editing"
                        options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                        context:NULL];
    
//    APP.viewDeckController.rightController = nil;
    
    self.tableView.adjustingOffsetY = 12.0f;
    
}

- (void)orderListChanged:(NSNotification *)aNoti
{
    for(LineItem *lineItem in ORDER.order.lineItems){
        BOOL found = NO;
        for(NSNumber *number in self.selectedLineItemIds){
            if(lineItem.lineItemNo == [number longLongValue]){
                if(!lineItem.isPaid){
                    lineItem.splitExcept = NO;
                } else {
                    lineItem.splitExcept = YES;
                }
                found = YES;
                break;
            }
        }
        if(!found){
            lineItem.splitExcept = YES;
        }
    }
    [self.tableView reloadData];
    [self calculateSplit];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
//    PCLog(@"change %@", change);
    
    if(object == self.tableView && [keyPath isEqual:@"editing"]){
        BOOL newValue = [change boolForKey:@"new"];
        
        if(newValue){
            self.navigationItem.rightBarButtonItem.button.buttonTitle = NSLocalizedString(@"Done", nil);
        } else {
            self.navigationItem.rightBarButtonItem.button.buttonTitle = NSLocalizedString(@"Split", nil);
        }
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}


- (void)splitButtonTouched:(id)sender
{
    [self.tableView setEditing:!self.tableView.editing animated:YES];
    
    if(self.tableView.isEditing){
        self.navigationItem.rightBarButtonItem.button.buttonTitle = NSLocalizedString(@"Done", nil);
    } else {
        self.navigationItem.rightBarButtonItem.button.buttonTitle = NSLocalizedString(@"Split", nil);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(self.isDirty){
        [self fillOrder];
    }
}

- (void)cardListChanged:(NSNotification *)notification
{
    if(CRED.isSignedIn){
        self.mobileCardList = REMOTE.cards;
        if(self.isCardListWaiting){
            self.cardListWaiting = NO;
            [self proceedPayment];
        }
    } else {
        self.mobileCardList = [MobileCard listAll];
    }
    
    [self fillOrder];
}

- (void)fillOrder
{
    self.dirty = NO;
    self.merchantNameLabel.text = ORDER.order.merchantName;
    self.merchantAddressLabel.text = ORDER.merchant.displayAddress;
    self.restaurantTypeLabel.text = ORDER.merchant.restaurantTypeString;
    
    NSURL *imgURL = ORDER.merchant.logoURL;
    if(imgURL == nil) imgURL = ORDER.merchant.imageURL;
    
    [self.logoImageView sd_setImageWithURL:imgURL
                          placeholderImage:[UIImage imageNamed:@"placeholder_logo"]
                                   options:0
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                 }];
    
    self.tableNumberLabel.text = [NSString stringWithFormat:@"%@",ORDER.order.tableNumber];
    
    if(ORDER.order.status == OrderStatusConfirmed ||
       ORDER.order.status == OrderStatusFixed){
        self.subTotalTextField.enabled = NO;
        self.taxesTextField.enabled = NO;
    } else {
        self.subTotalTextField.enabled = YES;
        self.taxesTextField.enabled = YES;
    }
    self.subTotalTextField.amount = ORDER.order.subTotal;
    self.taxesTextField.amount = ORDER.order.taxes;
    
    if(ORDER.order.payAmounts == 0){ //Hide Paid Amount
        self.paidAmountTextField.hidden = YES;
        self.paidAmountTitleLabel.hidden = YES;
        
        if(CRED.isSignedIn && ORDER.merchant.promotion != nil){
            self.discountTextField.y = CGRectGetMaxY(self.totalTextField.frame) + 20.0f;
            self.discountTitleTextField.y = self.discountTextField.y;
            self.bottomContainerView.y = CGRectGetMaxY(self.discountTextField.frame) + 5.0f;
        } else {
            self.discountTextField.hidden = YES;
            self.discountTitleTextField.hidden = YES;
            self.bottomContainerView.y = CGRectGetMaxY(self.totalTextField.frame) + 20.0f;
        }
        
    } else {
        self.paidAmountTextField.amount = ORDER.order.payAmounts - ORDER.order.tips;
        
        if(CRED.isSignedIn && ORDER.merchant.promotion != nil){
            self.discountTextField.y = CGRectGetMaxY(self.paidAmountTextField.frame) + 6.0f;
            self.discountTitleTextField.y = self.discountTextField.y;
            self.bottomContainerView.y = CGRectGetMaxY(self.discountTextField.frame) + 5.0f;
        } else {
            self.discountTextField.hidden = YES;
            self.discountTitleTextField.hidden = YES;
            self.bottomContainerView.y = CGRectGetMaxY(self.paidAmountTextField.frame) + 6.0f;
        }
    }
    
    self.selectCardButton.y = CGRectGetMaxY(self.bottomContainerView.frame) + 10.0f;
    
    [self updateTotalAmount];
    [self updatePayAmount];
    
    if(CRED.isSignedIn && !REMOTE.isCardFetched){
        self.selectCardButton.buttonTitle = NSLocalizedString(@"Fetching Cards...", nil);
        self.selectCardButton.enabled = NO;
    } else {
        self.selectCardButton.enabled = YES;
        if([self.mobileCardList count] > 0){
            self.selectCardButton.buttonTitle = NSLocalizedString(@"Pay With Card", nil);
        } else {
            if(ORDER.merchant.isTestMode){
                self.selectCardButton.buttonTitle = NSLocalizedString(@"Pay With Test Card", nil);
            } else {
                self.selectCardButton.buttonTitle = NSLocalizedString(@"Pay With New Card", nil);
            }
        }
    }
}

- (IBAction)payWithCardButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    self.tableView.editing = NO;
    
    // Update order
    if(ORDER.order.status == OrderStatusDraft ||
       ORDER.order.status == OrderStatusUnknown){
        if(![VALID validate:self.subTotalTextField
                  condition:(self.subTotalTextField.amount > 0)
                    message:NSLocalizedString(@"Subtotal amount should be greater than $0.00.", nil)]){
            return;
        }
        
        ORDER.order.subTotal = self.subTotalTextField.amount;
        ORDER.order.taxes = self.taxesTextField.amount;
        ORDER.order.status = OrderStatusDraft;
    }
    
    if([self.selectedLineItemIds count] > 0){
        
    } else {
        [UIAlertView alertWithTitle:NSLocalizedString(@"Select Items", nil)
                            message:NSLocalizedString(@"You should select at least one item.", nil)];
        
        return;
    }
    
    [self proceedPayment];
}

- (void)proceedPayment
{
    if(CRED.isSignedIn){
        self.mobileCardList = REMOTE.cards;
        if(!REMOTE.isCardFetched){
            self.cardListWaiting = YES;
            return;
        }
    }
    
    self.payment = [[Payment alloc] init];
    self.payment.merchantNo = ORDER.order.merchantNo;
    self.payment.subTotal = self.paymentSubtotal;
    self.payment.taxAmount = self.paymentTax;
    self.payment.orderAmount = self.tempPayAmount;

    self.payment.payAmount = self.payment.orderAmount;
    self.payment.currency = self.payAmountTextField.currencyCode;
    self.payment.payDate = [NSDate date];
    
    UIViewController *viewController = nil;
    if([self.mobileCardList count] > 0 || CRED.customerInfo.credit > 0){
        
        viewController = [PaymentMethodViewController viewControllerFromNib];
        ((PaymentMethodViewController *)viewController).payment = self.payment;
        ((PaymentMethodViewController *)viewController).paymentMethodList = self.mobileCardList;
        ((PaymentMethodViewController *)viewController).disposeOrderWhenBack = YES;
        
    } else {
        viewController = [NewCardViewController viewControllerFromNib];
        ((NewCardViewController *)viewController).unintendedPresented = YES;
        ((NewCardViewController *)viewController).payment = self.payment;
        ((NewCardViewController *)viewController).disposeOrderWhenBack = YES;
    }
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)updateTotalAmount
{
    PCCurrency totalAmount = 0.0f;
    
    totalAmount += self.subTotalTextField.amount;
    totalAmount += self.taxesTextField.amount;
    self.totalTextField.amount = totalAmount;
}

- (void)updatePayAmount
{
    if([ORDER.order.lineItems count] == 0){
        PCCurrency payAmount = self.totalTextField.amount - self.paidAmountTextField.amount;
        payAmount = MAX(payAmount, 0.0);
        
        self.tempPayAmount = payAmount;
        self.payAmountTextField.amount = self.tempPayAmount;
        
    } else {
        [self calculateSplit];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self.tableView setEditing:NO animated:YES];
    
    if(self.tableView.isEditing){
        self.navigationItem.rightBarButtonItem.button.buttonTitle = NSLocalizedString(@"Done", nil);
    } else {
        self.navigationItem.rightBarButtonItem.button.buttonTitle = NSLocalizedString(@"Split", nil);
    }
    
    return YES;
}


- (IBAction)textFieldValueChanged:(UITextField *)textField
{
    if(textField == self.subTotalTextField || textField == self.taxesTextField){
        
        self.totalTextField.amount = (self.subTotalTextField.amount + self.taxesTextField.amount);
        
    } else if(textField == self.totalTextField){
        if(self.payAmountTextField.amount == 0.0f ||
           self.payAmountTextField.amount == self.prevPayAmount){
            PCCurrency payAmount = self.totalTextField.amount - self.paidAmountTextField.amount;
            payAmount = MAX(payAmount, 0.0);
            self.tempPayAmount = payAmount;
            self.payAmountTextField.amount = self.tempPayAmount;
        }
        
        self.prevPayAmount = (self.totalTextField.amount - self.paidAmountTextField.amount);
    }
}

- (void)viewDidUnload {
    [self.tableView removeObserver:self
                        forKeyPath:@"editing"];
    
    [self setPaidAmountTextField:nil];
    [self setPaidAmountTitleLabel:nil];
    [self setBottomContainerView:nil];
    [self setMerchantAddressLabel:nil];
    [self setTextFieldAccessoryView:nil];
    [self setTableView:nil];
    [self setHeaderView:nil];
    [super viewDidUnload];
}

- (void)requestMyCredit
{
    if(CRED.customerInfo != nil){
        RequestResult result = RRFail;
        result = [CRED requestMyCreditWithCompletionBlock:
                  ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                      if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                          switch(response.errorCode){
                              case ResponseSuccess:
                                  CRED.customerInfo.credit = [response integerForKey:@"credit_balance"];
                                  break;
                              case ResponseErrorGeneral:{
                                  NSString *message = [response objectForKey:@"message"];
                                  if([message isKindOfClass:[NSString class]]){
                                      [UIAlertView alert:message];
                                  }
                                  break;
                              }
                              default:{
                                  NSString *message = [response objectForKey:@"message"];
                                  if([message isKindOfClass:[NSString class]]){
                                      [UIAlertView alert:message];
                                  } else {
                                      [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting my points", nil)];
                                  }
                              }
                                  break;
                          }
                      } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                          [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                          [CRED resetUserAccount];
                          //Signed out in requestMyCreditWithCompletionBlock
                      } else {
                          if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                              [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                          } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                              [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                                  ,statusCode]];
                          }
                      }
                      [SVProgressHUD dismiss];
                  }];
        switch(result){
            case RRSuccess:
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                break;
            case RRParameterError:
                [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                break;
            default:
                break;
        }
    }
}

- (IBAction)nextButtonTouched:(id)sender
{
    [self.payAmountTextField resignFirstResponder];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        default:
            break;
    }
}

- (BOOL)textFieldShouldClear:(PCCurrencyTextField *)textField
{
    return NO;
}

#pragma mark - UITableViewDatasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ORDER.order.lineItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LineItem *lineItem = [ORDER.order.lineItems objectAtIndex:indexPath.row];
    
    NSString *cellIdentifier = @"BillOrderCell";
    
    BillOrderCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        cell = [BillOrderCell cell];
    }
    
    cell.lineItem = lineItem;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LineItem *lineItem = [ORDER.order.lineItems objectAtIndex:indexPath.row];
    
    self.offScreenBillOrderCell.lineItem = lineItem;
    [self.offScreenBillOrderCell setNeedsLayout];
    [self.offScreenBillOrderCell layoutIfNeeded];
    
    CGSize size = [self.offScreenBillOrderCell.contentView
                   systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    return size.height;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    LineItem *lineItem = [ORDER.order.lineItems objectAtIndex:indexPath.row];
    return !lineItem.isPaid;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LineItem *lineItem = [ORDER.order.lineItems objectAtIndex:indexPath.row];
    return lineItem.isSplitExcept ? UITableViewCellEditingStyleInsert : UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    LineItem *lineItem = [ORDER.order.lineItems objectAtIndex:indexPath.row];
    
    UITableViewRowAnimation aniType = UITableViewRowAnimationBottom;
    
    if(editingStyle == UITableViewCellEditingStyleDelete){
        lineItem.splitExcept = YES;
    } else if(editingStyle == UITableViewCellEditingStyleInsert){
        lineItem.splitExcept = NO;
        aniType = UITableViewRowAnimationTop;
    }
    
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                          withRowAnimation:aniType];
    [self calculateSplit];
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

- (void)calculateSplit
{
    self.paymentSubtotal = 0;
    self.paymentTax = 0;
    
    PCCurrency totalAmount = 0;
    PCCurrency discountAmount = 0;
    
    for(LineItem *lineItem in ORDER.order.lineItems){
        if(!lineItem.isSplitExcept && !lineItem.isPaid){
            self.paymentSubtotal += lineItem.lineAmount;
        }
    }
    
    self.paymentTax = roundf(((self.paymentSubtotal * ORDER.merchant.taxRate) / 100.0f));
    
    totalAmount = self.paymentSubtotal;
    

    if(ORDER.merchant.promotion != nil){
        if(CRED.isSignedIn){
            
            for(Promotion *promotion in ORDER.merchant.promotions){
                
                if(!promotion.isActive){
                    continue;
                }
                
                if([promotion.endDate timeIntervalSinceDate:[NSDate date]] < 0){
                    continue;
                }
                
                if([promotion.startDate timeIntervalSinceDate:[NSDate date]] > 0){
                    continue;
                }
                
                if(promotion.minOrderAmount > totalAmount){
                    continue;
                }
                
                PCCurrency appliedDiscountAmount = 0;
                if(promotion.isFlatDiscount){
                    appliedDiscountAmount = MIN(promotion.discountAmount, (totalAmount - appliedDiscountAmount));
                } else {
                    PCCurrency limitedDiscountAmount = roundf(totalAmount * (promotion.discountRate / 100.0f));
//                    if(promotion.limitDiscountAmount > 0){
                        limitedDiscountAmount = MIN(promotion.limitDiscountAmount, limitedDiscountAmount);
//                    }
                    appliedDiscountAmount = MIN(limitedDiscountAmount, (totalAmount - appliedDiscountAmount));
                }
                discountAmount += appliedDiscountAmount;
                
                if(discountAmount >= totalAmount){
                    break;
                }
            }
        } else {
        }
    } else {
    }
    
    self.tempPayAmount = self.paymentSubtotal + self.paymentTax;
    self.tempDiscountAmount = discountAmount;
    self.discountTextField.amount = -self.tempDiscountAmount;
    self.payAmountTextField.amount = self.tempPayAmount - self.tempDiscountAmount;

}


-(void)cell:(BillOrderCell *)cell didTouchCheckButtonAtIndexPath:(NSIndexPath *)indexPath
{
    LineItem *lineItem = [ORDER.order.lineItems objectAtIndex:indexPath.row];
    
    lineItem.splitExcept = !lineItem.splitExcept;
    
    if(lineItem.splitExcept){
        [self.selectedLineItemIds removeObject:[NSNumber numberWithSerial:lineItem.lineItemNo]];
    } else {
        [self.selectedLineItemIds addObject:[NSNumber numberWithSerial:lineItem.lineItemNo]];
    }
    
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                          withRowAnimation:UITableViewRowAnimationNone];
    [self calculateSplit];
}

- (BillOrderCell *)offScreenBillOrderCell
{
    if(_offScreenBillOrderCell == nil){
        _offScreenBillOrderCell = [BillOrderCell cell];
    }
    return _offScreenBillOrderCell;
}

- (NSMutableSet *)selectedLineItemIds
{
    if(_selectedLineItemIds == nil){
        _selectedLineItemIds = [NSMutableSet set];
    }
    return _selectedLineItemIds;
}

@end
