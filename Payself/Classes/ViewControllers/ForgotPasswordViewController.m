//
//  ForgotPasswordViewController.m
//  RushOrder
//
//  Created by Conan on 2/20/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "PCCredentialService.h"

@interface ForgotPasswordViewController ()

@property (weak, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NPStretchableButton *submitButton;
@property (weak, nonatomic) IBOutlet PCTextField *emailTextField;
@end

@implementation ForgotPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Forgot Password", nil);
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Cancel",nil)
                                                                             target:self
                                                                             action:@selector(closeButtonTouched:)];
    
    [self.scrollView setContentSizeWithBottomView:self.submitButton];
}

- (void)closeButtonTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)submitButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    if([self.emailTextField.text length] == 0){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Email Address Needed", nil)
                            message:NSLocalizedString(@"Password reset email will be sent to you. Please enter email you signed up", nil)
                           delegate:self
                                tag:102];
        return;
    }
    
    RequestResult result = RRFail;
    result = [CRED requestForgotPassword:self.emailTextField.text
                         completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              [UIAlertView alertWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Password reset instructions have been sent to the email address you provided.", nil)]
                                                  message:nil
                                                 delegate:self
                                                      tag:101];
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while sending email", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 101:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 102:
            [self.emailTextField becomeFirstResponder];
            break;
        default:
            break;
    }
}

@end
