//
//  RestaurantMapViewController.m
//  RushOrder
//
//  Created by Conan on 11/29/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "RestaurantMapViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "RestaurantAnnotationView.h"
#import "OrderManager.h"
#import "MerchantViewController.h"

@interface RestaurantMapViewController ()

@property (strong, nonatomic) RestaurantMiniSummaryView *restaurantView;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) id<MKAnnotation> selectedAnnotation;
@property (nonatomic) MKCoordinateRegion selectedRegion;

@property (nonatomic, getter = isRegionSet) BOOL regionSet;
@property (nonatomic, getter = isDirty) BOOL dirty;
@property (nonatomic, getter = isProgressing) BOOL progressing;
@property (strong, nonatomic) Merchant *selectedMerchant;
@property (weak, nonatomic) IBOutlet UIButton *locationButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoContainerBottomConstraint;

@property (weak, nonatomic) IBOutlet UIView *restaurantInfoContainer;

@end

@implementation RestaurantMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self != nil){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.regionSet = NO;
    self.dirty = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidEnterBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillEnterForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(locationUpdated:)
                                                 name:locationUpdatedNotificationKey
                                               object:nil];
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.infoContainerBottomConstraint.constant = -800.0f;
    
    self.locationButton.layer.cornerRadius = 6.0f;
    [self.locationButton drawBorderWithColor:[UIColor whiteColor]];
    
    if(!self.onlyMapMode){
        self.title = NSLocalizedString(@"Map", nil);
//        self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemHome
//                                                                             target:self
//                                                                             action:@selector(rightMenuButtonTouched:)];
        
        self.restaurantView = [RestaurantMiniSummaryView viewWithNibName:@"RestaurantMiniSummaryView"];
        self.restaurantView.translatesAutoresizingMaskIntoConstraints = NO;
        self.restaurantView.delegate = self;
        self.restaurantView.logoImage.target = self;
        self.restaurantView.titleLabel.target = self;
        self.restaurantView.addressLabel.target =self;
        
//        self.restaurantView.backgroundColor = [UIColor clearColor];
        [self.restaurantInfoContainer addSubview:self.restaurantView];
        
        NSDictionary *viewsDictionary = @{@"restaurantView":self.restaurantView};
        
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[restaurantView]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:viewsDictionary];
        [self.restaurantInfoContainer addConstraints:constraints];
        
        constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[restaurantView]|"
                                                              options:0
                                                              metrics:nil
                                                                views:viewsDictionary];
        [self.restaurantInfoContainer addConstraints:constraints];
    } else {
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemBack
                                                                            target:self
                                                                            action:@selector(backButtonTouched:)];
    }
    
    [self initMapView];
}

- (void)backButtonTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)circleImageViewActionTriggerred:(id)sender
{
    [ORDER resetGraphWithMerchant:self.restaurantView.merchant];
    MerchantViewController *viewController = [UIStoryboard viewController:@"MerchantViewController"
                                                                     from:@"Restaurants"];
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^{
                                     
                                 }];
}

- (void)touchableLabelActionTriggerred:(id)sender
{
    [self circleImageViewActionTriggerred:sender];
}

- (void)initMapView
{
    [self removeAllAnnotationWithoutUserLocation];
    
    NSMutableArray *restaurantList = [NSMutableArray array];
    
    for(Merchant *merchant in self.merchantList){
        if([merchant isKindOfClass:[Merchant class]]){
            [restaurantList addObject:merchant];
        }
    }
    
    for(Merchant *merchant in self.unavailableList){
        if([merchant isKindOfClass:[Merchant class]]){
            [restaurantList addObject:merchant];
        }
    }
    
    [self.mapView addAnnotations:restaurantList];
    [self mapBoundForContents:NO];
    
    if(APP.location != nil){
        self.regionSet = YES;
    }
}

- (void)applicationWillEnterForeground:(NSNotification *)noti
{
    [APP startLocationUpdates];
}

- (void)applicationDidEnterBackground:(NSNotification *)noti
{
    self.dirty = YES;
}

- (void)locationUpdated:(NSNotification *)noti
{
    CLLocation *location = [noti.userInfo objectForKey:@"location"];
    
    [self.mapView setCenterCoordinate:location.coordinate animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)leftMenuButtonTouched:(UIBarButtonItem *)sender
{
//    [APP.viewDeckController toggleLeftViewAnimated:YES];
    
}

- (IBAction)rightMenuButtonTouched:(UIBarButtonItem *)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 
                             }];
}

- (void)removeAllAnnotationWithoutUserLocation
{
    for(id <MKAnnotation>annotation in self.mapView.annotations){
        if(![annotation isKindOfClass:[MKUserLocation class]])
            [self.mapView removeAnnotation:annotation];
    }
}

#pragma mark - MapViewController
- (void)mapBoundForContents:(BOOL)animated
{
    if(self.isRegionSet) return;
    
    CLLocationDegrees maxLat = -90;
    CLLocationDegrees maxLon = -180;
    CLLocationDegrees minLat = 90;
    CLLocationDegrees minLon = 180;
    
    id<MKAnnotation> firstAnnotation = nil;
    id<MKAnnotation> prevSelectedAnnotation = self.selectedAnnotation;
    
    self.selectedAnnotation = nil;
    
    if([self.mapView.annotations count] > 0){
        
        NSArray *combinedArray = [self.merchantList arrayByAddingObjectsFromArray:self.unavailableList];
        NSInteger ctr = 0;
        for(id<MKAnnotation> annotation in combinedArray){
            
            //            if([annotation isKindOfClass:[MKUserLocation class]]){
            //                continue;
            //            }
            
            if(ctr >= 3) break;
            
            if([annotation isKindOfClass:[Merchant class]]){
                if(APP.location != nil && !self.onlyMapMode){
                    Merchant *merchant = (Merchant *)annotation;
                    CLLocationDistance distanceFromMe = [APP.location distanceFromLocation:merchant.location];
                    if(distanceFromMe > AMILE * 30){
                        continue;
                    } else {
                        if(!merchant.isDemoMode)
                            ctr++;
                    }
                }
            }
            
            
            CLLocationCoordinate2D coordinate = annotation.coordinate;
            
            if(coordinate.latitude == 0 || coordinate.longitude == 0) continue;
            
            if(coordinate.latitude > maxLat)
                maxLat = coordinate.latitude;
            if(coordinate.latitude < minLat)
                minLat = coordinate.latitude;
            if(coordinate.longitude > maxLon)
                maxLon = coordinate.longitude;
            if(coordinate.longitude < minLon)
                minLon = coordinate.longitude;
            
            if([annotation isKindOfClass:[Merchant class]]){
                if(firstAnnotation == nil){
                    firstAnnotation = annotation;
                }
                
                Merchant *selectedMerchant = (Merchant *)prevSelectedAnnotation;
                Merchant *merchant = (Merchant *)annotation;
                
                if(merchant.merchantNo == selectedMerchant.merchantNo){
                    self.selectedAnnotation = annotation;
                }
            }
        }
    }
    
    if(maxLat == -90 && maxLon == -180 && minLat == 90 && minLon == 180){
        if(APP.location != nil){
            CLLocationCoordinate2D coord = APP.location.coordinate;
            MKCoordinateSpan span = {0.01, 0.01};
            MKCoordinateRegion region = {coord, span};
            
            [self.mapView setRegion:region animated:animated];
        } else {
            // Nothing to set region
        }
    } else {
        MKCoordinateRegion region;
        region.center.latitude     = (maxLat + minLat) / 2;
        region.center.longitude    = (maxLon + minLon) / 2;
        region.span.latitudeDelta  = maxLat - minLat + 0.04;
        region.span.longitudeDelta = maxLon - minLon + 0.04;
        
        [self.mapView setRegion:region animated:animated];
        
    }
    
    if(!self.onlyMapMode){
        if(self.selectedAnnotation == nil){
            self.selectedAnnotation = firstAnnotation;
        }
        
        if(self.selectedAnnotation != nil){
            [self.mapView selectAnnotation:self.selectedAnnotation animated:YES];
        }
    }
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    
}


#pragma mark - MKMapViewDelegate
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
	static NSString *identifier = @"RestaurantAnnotationView";
	
    RestaurantAnnotationView *annotationView = (RestaurantAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    [annotationView drawBorder];
    
	if(annotationView == nil) {
		annotationView = [[RestaurantAnnotationView alloc] initWithAnnotation:annotation
                                                         reuseIdentifier:identifier];
	}
    
    if([annotation isKindOfClass:[Merchant class]]){
        Merchant *merchant = (Merchant *)annotation;
        annotationView.merchant = merchant;
        
        [annotationView drawRestaurant];
    }
    
	annotationView.annotation=annotation;
    
	annotationView.canShowCallout = NO;
    
    return annotationView;
}


- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if(self.onlyMapMode){
        return;
    }
    
    if(![view isKindOfClass:[RestaurantAnnotationView class]]){
        return;
    }
    
    self.selectedAnnotation = view.annotation;
    
    if(!view.isSelected) view.selected = YES;
    [((RestaurantAnnotationView *)view) drawRestaurant];
    [self.mapView setNeedsDisplay];
    
    if([view.annotation isKindOfClass:[Merchant class]]){
//        [self fillMerchant:(Merchant *)view.annotation];
        
        self.restaurantView.merchant = (Merchant *)view.annotation;
        [self.restaurantView drawRestaurant:NO];
        
        [UIView animateWithDuration:0.3f
                         animations:^{
                             self.infoContainerBottomConstraint.constant = 0.0f;
                             [self.view layoutIfNeeded];
                         }];
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    if([view isKindOfClass:[RestaurantAnnotationView class]]){
        
        self.selectedAnnotation = view.annotation;
        
        [((RestaurantAnnotationView *)view) drawRestaurant];
        [self.mapView setNeedsDisplay];
        
    }
    
    [UIView animateWithDuration:0.3f
                     animations:^{
                         self.infoContainerBottomConstraint.constant = -800.0f;
                         [self.view layoutIfNeeded];
                     }];
}

//- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
//{
//    Merchant *merchant = (Merchant *)view.annotation;
//
//    [self dismissViewControllerAnimated:YES
//                             completion:^{
//                                 if([self.delegate respondsToSelector:@selector(restaurantMapViewController:didSelectRestaurant:withCartType:)]){
//                                     [self.delegate restaurantMapViewController:self
//                                                            didSelectRestaurant:merchant
//                                                                   withCartType:];
//                                 }
//                             }];
//}

- (void)restaurantMiniSummaryView:(RestaurantMiniSummaryView *)viewController
             didTouchActionButton:(UIButton *)sender
{
    if([self.selectedAnnotation isKindOfClass:[Merchant class]]){
        Merchant *merchant = (Merchant *)self.selectedAnnotation;
        
        [self dismissViewControllerAnimated:YES
                                 completion:^{
                                     if([self.delegate respondsToSelector:@selector(restaurantMapViewController:didSelectRestaurant:withCartType:)]){
                                         [self.delegate restaurantMapViewController:self
                                                                didSelectRestaurant:merchant
                                                                       withCartType:(CartType)sender.tag];
                                     }
                                 }];
    }
}

- (IBAction)myPositionButtonTouched:(id)sender
{
    [APP startLocationUpdates];
    [self.mapView setCenterCoordinate:self.mapView.userLocation.coordinate animated:YES];
}




@end
