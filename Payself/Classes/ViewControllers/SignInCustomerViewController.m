//
//  SignInCustomerViewController.m
//  RushOrder
//
//  Created by Conan on 2/19/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import "SignInCustomerViewController.h"
#import "SignUpCustomerViewController.h"
#import "ForgotPasswordViewController.h"
#import "PCCredentialService.h"
#import "PCMenuService.h"
#import "TransactionManager.h"
#import "Receipt.h"
#import "AccountSwitchingManager.h"
#import <TwitterKit/TwitterKit.h>

@interface SignInCustomerViewController ()

@property (weak, nonatomic) IBOutlet PCTextField *emailTextField;
@property (weak, nonatomic) IBOutlet PCTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NPStretchableButton *neoAccountButton;
@property (weak, nonatomic) IBOutlet UILabel *guideLabel;
@property (weak, nonatomic) IBOutlet UIView *contentContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentTopConstraint;
@end

@implementation SignInCustomerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    TWTRLogInButton *logInButton = [TWTRLogInButton buttonWithLogInCompletion:^(TWTRSession *session, NSError *error) {
//        // play with Twitter session
//    }];
//    logInButton.center = self.view.center;
//    [self.view addSubview:logInButton];
    
    self.title = NSLocalizedString(@"Sign In", nil);
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Cancel",nil)
                                                                             target:self
                                                                             action:@selector(closeButtonTouched:)];
    
    [self.scrollView setContentSizeWithBottomView:self.neoAccountButton];
    
    self.emailTextField.text = [NSUserDefaults standardUserDefaults].lastTriedUserId;
    
    self.guideLabel.hidden = !self.isShowGuide;
    if(self.isShowGuide){
        self.contentTopConstraint.constant = 62.0f;
    } else {
        self.contentTopConstraint.constant = 0.0f;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(syncingDone:)
                                                 name:AccountDataSwitchedNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)syncingDone:(NSNotification *)aNoti
{
    // TODO: Fail Handling
    if([self.delegate respondsToSelector:@selector(signInCustomerViewControllerNeedDissmissingViewController:)]){
        [self.delegate signInCustomerViewControllerNeedDissmissingViewController:self];
    } else {
        [self dismissViewControllerAnimated:YES
                                 completion:^{
                                     
                                 }];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SignedInNotification
                                                        object:self
                                                      userInfo:nil];
}


- (void)closeButtonTouched:(id)sender
{
    if([self.delegate respondsToSelector:@selector(signInCustomerViewControllerNeedDissmissingViewController:)]){
        [self.delegate signInCustomerViewControllerNeedDissmissingViewController:self];
    } else {
        [self dismissViewControllerAnimated:YES
                                 completion:^{
                                     
                                 }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (IBAction)signUpButtonTouched:(id)sender
{
    [NSUserDefaults standardUserDefaults].lastTriedUserId = self.emailTextField.text;
    SignUpCustomerViewController *viewController = [SignUpCustomerViewController viewControllerFromNib];
    viewController.delegate = self.delegate;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (IBAction)forgotPasswordButtonTouched:(id)sender
{
    ForgotPasswordViewController *viewController = [ForgotPasswordViewController viewControllerFromNib];
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)buttonsEnabled:(BOOL)enabled
{
    self.navigationItem.leftBarButtonItem.enabled = enabled;
}

- (IBAction)signInButtonTouched:(id)sender
{
    if(![VALID validate:self.emailTextField
                  title:NSLocalizedString(@"Email Is Required", nil)
                message:NSLocalizedString(@"Enter your Email for signing in. If you don't have account yet, you can create your new account", nil)]){
        return;
    }
    
    if(![VALID validate:self.passwordTextField
                  title:NSLocalizedString(@"Password is Required", nil)
                message:NSLocalizedString(@"Enter your Password for signing in. If you can't remember the password, please use Forgot Password?", nil)]){
        return;
    }
    
    [self buttonsEnabled:NO];
    [NSUserDefaults standardUserDefaults].lastTriedUserId = self.emailTextField.text;
    
    RequestResult result = RRFail;
    result = [CRED requestSignIn:self.emailTextField.text
                        password:self.passwordTextField.text
                     deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                 completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                           
                              NSArray *customers = [CustomerInfo listAll];
                              CustomerInfo *customerInfo = nil;
                              if([customers count] == 1){
                                  customerInfo = [customers objectAtIndex:0];
                              } else{
                                  PCWarning(@"Customers is not just only 1 - sign in");
                                  // Clear
                                  for(CustomerInfo *customer in customers){
                                      if([customer delete]){
                                          // Success
                                      } else {
                                          PCError(@"Error to remove customerInfo - sign in");
                                      }
                                  }
                              }
                              
                              if(customerInfo == nil){
                                  customerInfo = [[CustomerInfo alloc] init];
                              }
                              
                              NSDictionary *customerDict = [response objectForKey:@"customer"];
                              [customerInfo updateWithDictionary:customerDict];
                              customerInfo.authToken = [response objectForKey:@"auth_token"];
                              if([customerInfo save]){
                                  CRED.customerInfo = customerInfo;
                                  
                                  [SWITCH removeLocalData];
                                  
                                  if([self.delegate respondsToSelector:@selector(signInCustomerViewControllerNeedDissmissingViewController:)]){
                                      [self.delegate signInCustomerViewControllerNeedDissmissingViewController:self];
                                  } else {
                                      [self dismissViewControllerAnimated:YES
                                                               completion:^{
                                                                   
                                                               }];
                                  }
                                  
                                  [[NSNotificationCenter defaultCenter] postNotificationName:SignedInNotification
                                                                                      object:self
                                                                                    userInfo:nil];
                                  
                              } else {
                                  [UIAlertView alertWithTitle:NSLocalizedString(@"Can Not Save Account Information", nil)
                                                      message:NSLocalizedString(@"Try to sign in again. If the problem contiue, Please contact us.\nhelp@rushorderapp.com", nil)];
                              }
                          }
                              break;
                          case ResponseErrorIdIncorrect:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Incorrect Email", nil)
                                                  message:[NSString stringWithFormat:NSLocalizedString(@"There is no account for %@", nil), self.emailTextField.text]];
                              
                              [self.emailTextField becomeFirstResponder];
                              
                              CRED.customerInfo = nil;
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while signing in", nil)];
                              }
                              
                              CRED.customerInfo = nil;
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode == HTTP_STATUS_UNAUTHORIZED && (response.errorCode == ResponseErrorIncorrect)){
                          [UIAlertView alertWithTitle:NSLocalizedString(@"Password is Incorrect", nil)
                                              message:NSLocalizedString(@"Please enter correct password, and try to sign in again", nil)];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
                  [self buttonsEnabled:YES];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [self buttonsEnabled:YES];
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            [self buttonsEnabled:YES];
            break;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == self.passwordTextField){
        [self.passwordTextField resignFirstResponder];
        [self signInButtonTouched:textField];
    } else {
        if ([textField isKindOfClass:[PCTextField class]]){
            [[(PCTextField *)textField nextField] becomeFirstResponder];
        }
    }
    return YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 103:
            if(buttonIndex == AlertButtonIndexNO){
                
                // Remove
                [SWITCH removeLocalData];
                if([self.delegate respondsToSelector:@selector(signInCustomerViewControllerNeedDissmissingViewController:)]){
                    [self.delegate signInCustomerViewControllerNeedDissmissingViewController:self];
                } else {
                    [self dismissViewControllerAnimated:YES
                                             completion:^{
                                                 
                                             }];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:SignedInNotification
                                                                    object:self
                                                                  userInfo:nil];
                
            } else if(buttonIndex == AlertButtonIndexYES){
                // Move to account
                [SWITCH startSync];
            }
            break;
        default:
            break;
    }
}
@end
