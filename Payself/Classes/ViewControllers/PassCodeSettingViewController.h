//
//  PassCodeSettingViewController.h
//  RushOrder
//
//  Created by Conan on 3/28/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "PassCodeViewController.h"

@interface PassCodeSettingViewController : PCViewController
<
UITableViewDelegate,
UITableViewDataSource,
PassCodeViewControllerDelegate
>
@end
