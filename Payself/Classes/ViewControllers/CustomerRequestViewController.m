//
//  CustomerRequestViewController.m
//  RushOrder
//
//  Created by Conan on 1/21/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import "CustomerRequestViewController.h"
#import "PCPaymentService.h"

@interface CustomerRequestViewController ()

@property (weak, nonatomic) IBOutlet UITextView *customerRequestTextView;
@end

@implementation CustomerRequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.customerRequestTextView.text = self.favoriteOrder.customerRequest;
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemCancel
                                                                        target:self
                                                                        action:@selector(cancelButtonTouched:)];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemSave
                                                                        target:self
                                                                        action:@selector(saveButtonTouched:)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)saveButtonTouched:(id)sender
{
    [self requestUpdateFavoriteOrder:FOupdateTypeRequest];
}

- (IBAction)cancelButtonTouched:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 
                             }];
}

- (void)requestUpdateFavoriteOrder:(FOupdateType)type
{
    FavoriteOrder *temp = [self.favoriteOrder copy];
    
    temp.customerRequest = self.customerRequestTextView.text;
    
    RequestResult result = RRFail;
    result = [PAY requestUpdateRapidReOrder:temp
                                       type:type
                            completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              [self.favoriteOrder updateWithDictionary:response];

                              [self dismissViewControllerAnimated:YES
                                                       completion:^{
                                                           
                                                       }];
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while creating RapidOrder", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
