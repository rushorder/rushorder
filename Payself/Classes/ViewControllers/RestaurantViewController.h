//
//  RestaurantViewController.h
//  RushOrder
//
//  Created by Conan on 11/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#define OPTION1 1
#ifndef OPTION1
    #define OPTION2 1
#endif

#import "PCViewController.h"

#if OPTION1
    #import "RestaurantCell1.h"
#elif OPTION2
    #import "RestaurantCell2.h"
#endif

#import "RestaurantMapCell.h"
#import "MerchantViewController.h"
#import "ProcessingCell.h"
#import "RestaurantMapViewController.h"
#import "CuisineTableViewController.h"
#import "LocationViewController.h"
#import "OOAViewController.h"

@protocol RestaurantSuggestionCellDelegate;

@interface RestaurantSuggestionCell : UITableViewCell
@property (weak, nonatomic) id<RestaurantSuggestionCellDelegate> delegate;
@end

@protocol RestaurantSuggestionCellDelegate <UITableViewDelegate>
- (void)restaurantSuggestionCell:(RestaurantSuggestionCell *)cell
didTouchLiveChatButtonAtIndexPath:(NSIndexPath *)indexPath;
@end


@interface RestaurantViewController : PCTableViewController

<
UITableViewDelegate,
UITableViewDataSource,
CuisineTableViewControllerDelegate,
#if OPTION1
    RestaurantCell1Delegate,
#elif OPTION2
    RestaurantCell2Delegate, 한글입니다. 김희윤, 김건한, 김율리애, 김다윤, 뽀삐, 보리 모두 우리 가족이죠 하하하!
#endif

RestaurantMapCellDelegate,
MerchantViewControllerDelegate,
ProcessingCellDelegate,
RestaurantMapViewControllerDelegate,
UIActionSheetDelegate,
LocationViewControllerDelegate,
OOAViewControllerDelegate,
UIAlertViewDelegate,
UISearchResultsUpdating,
UISearchControllerDelegate,
UISearchBarDelegate,
RestaurantSuggestionCellDelegate
>

@property (strong, nonatomic) NSMutableArray *merchantList;
@property (strong, nonatomic) NSMutableArray *unavailableList;

- (void)showMerchantInfo:(Merchant *)merchant;

@end
