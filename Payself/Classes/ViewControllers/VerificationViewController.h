//
//  VerificationViewController.h
//  RushOrder
//
//  Created by Conan on 4/10/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"

@protocol VerificationViewControllerDelegate;

@interface VerificationViewController : PCViewController

@property (weak, nonatomic) id<VerificationViewControllerDelegate> delegate;
@end

@protocol VerificationViewControllerDelegate <NSObject>
- (void)viewController:(VerificationViewController *)viewController
   successRegistration:(BOOL)success;
@end