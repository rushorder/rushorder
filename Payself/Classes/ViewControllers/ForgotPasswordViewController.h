//
//  ForgotPasswordViewController.h
//  RushOrder
//
//  Created by Conan on 2/20/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import "PCViewController.h"

@interface ForgotPasswordViewController : PCViewController
<UIAlertViewDelegate>
@end
