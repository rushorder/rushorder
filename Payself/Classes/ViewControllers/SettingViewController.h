//
//  SettingViewController.h
//  RushOrder
//
//  Created by Conan on 2/22/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MailMeViewController.h"

@interface SettingViewController : PCTableViewController
<
UIAlertViewDelegate,
MailMeViewControllerDelegate,
UIActionSheetDelegate
>

@property (nonatomic) BOOL showShareAndEarn;
@end
