//
//  PaymentHistoryViewController.h
//  RushOrder
//
//  Created by Conan on 2/22/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProcessingCell.h"
//#import "BTRatingView.h"
#import "CustomIOSAlertView.h"

@interface PaymentHistoryViewController : PCTableViewController
<
ProcessingCellDelegate,
UIActionSheetDelegate,
UIAlertViewDelegate,
//BTRatingViewDelegate,
CustomIOSAlertViewDelegate
>

@property (nonatomic, getter = isRapidRO) BOOL rapidRO;
@property (nonatomic, getter = isLocalDirty) BOOL localDirty;

@end
