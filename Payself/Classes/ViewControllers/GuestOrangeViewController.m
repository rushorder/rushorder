//
//  GuestOrangeViewController.m
//  RushOrder
//
//  Created by Conan Kim on 1/26/16.
//  Copyright © 2016 Paycorn. All rights reserved.
//

#import "GuestOrangeViewController.h"
#import "PCCredentialService.h"
#import "SignUpCustomerViewController.h"
#import "SignInCustomerViewController.h"

@interface GuestOrangeViewController ()

@property (nonatomic) BOOL outdated;

@end

@implementation GuestOrangeViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signedIn:)
                                                 name:SignedInNotification
                                               object:nil];
}

- (void)signedIn:(NSNotification *)aNoti
{
    self.outdated = YES;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(self.outdated){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NeedToPreparePushingAutoViewController"
                                                            object:self
                                                          userInfo:@{@"type":@"mycredit"}];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)singUpButtonTouched:(id)sender
{
    SignUpCustomerViewController *viewController = [SignUpCustomerViewController viewControllerFromNib];
    [self.tabBarController presentViewControllerInNavigation:viewController
                                                    animated:YES
                                                  completion:^{
                                                      
                                                  }];
}

- (IBAction)signInButtonTouched:(id)sender
{
    [self showSignIn:NO];
}

- (void)showSignIn:(BOOL)showGuide
{
    SignInCustomerViewController *viewController = [SignInCustomerViewController viewControllerFromNib];
    viewController.showGuide = showGuide;
    [self.tabBarController presentViewControllerInNavigation:viewController
                                                    animated:YES
                                                  completion:^{
                                                  }];
}

- (IBAction)shareNEarnButtonTouched:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NeedToPreparePushingAutoViewController"
                                                        object:self
                                                      userInfo:@{@"type":@"invite"}];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
