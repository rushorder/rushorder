//
//  RequestAreaViewController.h
//  RushOrder
//
//  Created by Conan Kim on 5/1/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import "PCViewController.h"

@interface RequestAreaViewController : PCViewController
<UITextFieldDelegate>
@end
