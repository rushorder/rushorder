//
//  OrderViewController.h
//  RushOrder
//
//  Created by Conan on 5/14/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "MenuDetailViewController.h"
#import "NPBatchTableView.h"

@protocol OrderViewControllerDelegate;

@interface OrderViewController : PCTableViewController
<
UITableViewDataSource,
UITableViewDelegate,
MenuDetailViewControllerDelegate,
UIAlertViewDelegate,
UIActionSheetDelegate,
//PullToRefreshing,
UIActionSheetDelegate,
LogoCircleImageViewAction,
TouchableLabelAction
>

@property (weak, nonatomic) id <OrderViewControllerDelegate> delegate;
@property (nonatomic, getter = isNeedReload) BOOL needReload;
@property (nonatomic, getter = isNeedAutomaticCheckout) BOOL needAutomaticCheckout;
@end

@protocol OrderViewControllerDelegate <NSObject>
@required
- (CartType)orderViewControllerInitialCartType:(OrderViewController *)orderViewController;

@optional
- (void)orderViewController:(OrderViewController *)orderViewController didTouchCheckoutButton:(id)sender;
- (void)orderViewController:(OrderViewController *)orderViewController didTouchOrderDoneButton:(id)sender;
- (void)orderViewController:(OrderViewController *)orderViewController didTouchOnlyCheckoutButton:(id)sender;
- (void)orderViewController:(OrderViewController *)orderViewController didTouchCheckinButton:(id)sender;


@end
