//
//  SelectCardViewController.h
//  RushOrder
//
//  Created by Conan on 2/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"
#import "Payment.h"
#import "MobileCardCell.h"
#import "FavoriteOrder.h"

@interface SelectCardViewController : PCViewController
<
UITableViewDelegate,
UITableViewDataSource,
UIAlertViewDelegate,
MobileCardCellDelegate,
UIActionSheetDelegate
>
@property (strong, nonatomic) Payment *payment;
@property (strong, nonatomic) NSMutableArray *mobileCardList;
@property (strong, nonatomic) MobileCard *selectedCard;

@property (nonatomic, getter = isManageMode) BOOL manageMode;
@property (nonatomic) FavoriteOrder *favoriteOrder;
@end