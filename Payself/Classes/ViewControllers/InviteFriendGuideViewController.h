//
//  InviteFriendGuideViewController.h
//  RushOrder
//
//  Created by Conan Kim on 1/21/16.
//  Copyright © 2016 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface InviteFriendGuideViewController : UIViewController
<
MFMessageComposeViewControllerDelegate,
FBSDKSharingDelegate
>

@property (nonatomic, getter = isBackToRestaurantPage) BOOL backToRestaurantPage;

@end
