//
//  ProfileViewController.h
//  RushOrder
//
//  Created by Conan on 2/20/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "VerificationViewController.h"
#import "SignInCustomerViewController.h"

@interface ProfileViewController : PCViewController
<
UIImagePickerControllerDelegate,
UINavigationControllerDelegate,
VerificationViewControllerDelegate,
UIActionSheetDelegate,
UITextFieldDelegate
>

@property (weak, nonatomic) id<SignInCustomerViewControllerDelegate> delegate;

@end
