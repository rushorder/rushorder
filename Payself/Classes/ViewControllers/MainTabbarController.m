//
//  MainTabbarController.m
//  RushOrder
//
//  Created by Conan Kim on 9/6/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

#import "MainTabbarController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface MainTabbarController ()

@property (strong, nonatomic) IBOutlet UIView *introView;
@end

@implementation MainTabbarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)showIntroView
{
//    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
//    // Optional: Place the button in the center of your view.
//    loginButton.center = CGPointMake(self.view.center.x, self.view.center.y + 100.0f)  ;
//    [self.introView addSubview:loginButton];
    
    
    [self.view addSubview:self.introView];
    
    return YES;
}

@end
