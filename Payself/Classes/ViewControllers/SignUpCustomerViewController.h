//
//  SignUpCustomerViewController.h
//  RushOrder
//
//  Created by Conan on 2/19/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "SignInCustomerViewController.h"

@interface SignUpCustomerViewController : PCViewController
<
UIAlertViewDelegate,
UITextFieldDelegate
>

@property (weak, nonatomic) id<SignInCustomerViewControllerDelegate> delegate;

@end
