//
//  ReachabilityService.m
//  RushOrder
//
//  Created by Conan on 12/3/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "ReachabilityService.h"


NSString * const ReachabilityStatusChangedNotification = @"ReachabilityStatusChangedNotification";

@implementation ReachabilityService

+ (ReachabilityService *)sharedService
{
    static ReachabilityService *aService = nil;
    
    @synchronized(self){
        if(aService == nil){
            aService = [[self alloc] init];
        }
    }
    
    return aService;
}

- (id)init
{
    self = [super init];
    if(self != nil){
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didEnterBackground:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(willEnterForeground:)
                                                     name:UIApplicationWillEnterForegroundNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
        
        //Change the host name here to change the server you want to monitor.
//        NSString *remoteHostName = SERVER_URL_ADDRESS;
//        NSString *port = nil;
//        NSArray *serverPort = [remoteHostName componentsSeparatedByString:@":"];
//        
//        if([serverPort count] >= 2){
//            remoteHostName = [serverPort objectAtIndex:0];
//            port = [serverPort objectAtIndex:1];
//        }
//        
//        self.reachability = [Reachability reachabilityWithHostName:remoteHostName];
        self.reachability = [Reachability reachabilityForInternetConnection];
        self.status = [self.reachability currentReachabilityStatus];
        [self.reachability startNotifier];
        
//        PCLog(@"remoteHostName %@", remoteHostName);
    }
    
    return self;
}

- (void)didEnterBackground:(NSNotification *)aNoti
{
    self.lastStatus = self.status;
    self.status = NotReachable;
}

- (void)willEnterForeground:(NSNotification *)aNoti
{
    [self performSelector:@selector(refreshReachability)
               withObject:nil
               afterDelay:0.5f];
}

- (void)refreshReachability
{
    self.status = [self.reachability currentReachabilityStatus];
    
    if(self.status == self.lastStatus){
        // 다를 경우 Reachability에서 직접 쏴주니까... reachabilityChanged: Call됨
        [[NSNotificationCenter defaultCenter] postNotificationName:ReachabilityStatusChangedNotification
                                                            object:self
                                                          userInfo:nil];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)reachabilityChanged:(NSNotification *)notification
{
    PCLog(@"Reach Notification %@" , notification);
    Reachability* curReach = [notification object];
	NSParameterAssert([curReach isKindOfClass:[Reachability class]]);

    self.status = [curReach currentReachabilityStatus];

    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ReachabilityStatusChangedNotification
                                                        object:self
                                                      userInfo:nil];
}


@end
