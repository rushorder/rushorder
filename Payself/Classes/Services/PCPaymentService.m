//
//  PCPaymentService.m
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCPaymentService.h"
#import "Merchant.h"
#import "PCCredentialService.h"
#import "TableInfo.h"
#import "RemoteDataManager.h"

@implementation PCPaymentService

+ (PCPaymentService *)sharedService
{
    static PCPaymentService *aService = nil;
    
    @synchronized(self){
        if(aService == nil){
            aService = [[self alloc] init];
        }
    }
    
    return aService;
}

- (id)init
{
    self = [super init];
    if(self != nil){
        // Initializer
    }
    return self;
}

- (RequestResult)requestMerchant:(PCSerial)merchantNo
                 completionBlock:(CompletionBlock)completionBlock
{    
    if(merchantNo == 0){
        return RRParameterError;
    }
    
    NSString *apiUrlString = [NSString stringWithFormat:MERCHANT_API_URL,
                              merchantNo];
    
    return [super requestWithAPI:apiUrlString
                       parameter:nil
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestMerchants:(NSString *)name
                  currentLocation:(CLLocation *)currentLocation
                             page:(NSUInteger)page
                  completionBlock:(CompletionBlock)completionBlock
{
    
    NSString *baseParameter = nil;
    
    NSString *authToken = CRED.customerInfo.authToken;

    baseParameter = [NSString stringWithFormat:MERCHANTS_LIST_BASE_PARAM,
                     [authToken length] > 0 ? authToken : @""];
    
    if([name length] > 0){
        baseParameter = [baseParameter stringByAppendingString:[NSString stringWithFormat:MERCHANTS_KEYWORD_PARAM,
                         [name urlEncodedString]]];
    } else {
        baseParameter = [baseParameter stringByAppendingString:[NSString stringWithFormat:@"&page=%lu",
                                                                (unsigned long)page]];
    }
    
    if(currentLocation != nil){
        baseParameter = [baseParameter stringByAppendingString:[NSString stringWithFormat:MERCHANTS_LOCATION_PARAM,
                         currentLocation.coordinate.latitude,
                         currentLocation.coordinate.longitude]];
    }

    return [super requestWithAPI:MERCHANTS_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSArray *arrayResponse = nil;
                        
                        arrayResponse = [response listData];
                        
                        NSMutableArray *merchantList = [self handleMerchantList:arrayResponse
                                                                 isSearchResult:([name length] > 0)
                                                                currentLocation:currentLocation
                                                              isDistanceSorting:NO
                                                                     isFavorite:NO
                                                                    deliverable:NO];
                                                       
                                                        
                        NSDictionary *composedResponseDict = @{@"nearby":merchantList,
                                                               @"resultCount":[NSNumber numberWithInteger:[arrayResponse count]]};
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, composedResponseDict, statusCode);
                        });
                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestMerchantsWithFilter:(NSDictionary *)dictionary
                            currentLocation:(CLLocation *)currentLocation
                                       page:(NSUInteger)page
                            completionBlock:(CompletionBlock)completionBlock
{
    NSMutableString *baseParameter = [NSMutableString string];
    
    [baseParameter appendString:[NSString stringWithFormat:@"page=%lu", (unsigned long)page]];
    
    if(currentLocation != nil){
        [baseParameter appendString:[NSString stringWithFormat:MERCHANTS_LOCATION_PARAM,
                                     currentLocation.coordinate.latitude,
                                     currentLocation.coordinate.longitude]];
    }
    
    BOOL deliverable = [dictionary boolForKey:@"deliverable"];
    
    [dictionary enumerateKeysAndObjectsUsingBlock:^(id key, NSString *val, BOOL *stop) {
        [baseParameter appendString:[NSString stringWithFormat:@"&%@=%@",
         key, [val urlEncodedString]]];
    }];
    
    [baseParameter appendString:@"&sort=open_status"];
    
    return [super requestWithAPI:MERCHANTS_FILTER_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSArray *arrayResponse = nil;
                        
                        arrayResponse = [[response data] objectForKey:@"list"];
                        NSNumber *isOutOfServiceArea = [[response data] objectForKey:@"out_of_service_area"];
                        NSMutableArray *merchantList = [self handleMerchantList:arrayResponse
                                                                 isSearchResult:NO
                                                                currentLocation:currentLocation
                                                              isDistanceSorting:NO
                                                                     isFavorite:NO
                                                                    deliverable:deliverable];
                        
                        NSMutableArray *unavailableMerchantList = [NSMutableArray array];
                        
//                        for(Merchant *aMerchant in merchantList){
//                            if(!aMerchant.openStatus || (!aMerchant.deliveryOpenStatus && !aMerchant.isAbleDinein && !aMerchant.isAbleTakeout)){
//                                [unavailableMerchantList addObject:aMerchant];
//                                [merchantList removeObject:aMerchant];
//                            }
//                        }
                        
                        for(int i = (int)([merchantList count] - 1) ; i > 0 ; i--){
                            Merchant *aMerchant = [merchantList objectAtIndex:i];
                            if(!aMerchant.openStatus || (!aMerchant.deliveryOpenStatus && !aMerchant.isAbleDinein && !aMerchant.isAbleTakeout && aMerchant.isAbleDelivery)){
                                [unavailableMerchantList insertObject:aMerchant atIndex:0];
                                [merchantList removeObjectAtIndex:i];
                            }
                        }
                        
                        NSDictionary *composedResponseDict = @{@"nearby":merchantList,
                                                               @"unavailable":unavailableMerchantList,
                                                               @"resultCount":[NSNumber numberWithInteger:[arrayResponse count]],
                                                               @"out_of_service_area": isOutOfServiceArea,
                                                               @"detected_area_name": [[response data] objectForKey:@"detected_area_name"]};
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, composedResponseDict, statusCode);
                        });
                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (NSMutableArray *)handleMerchantList:(NSArray *)array
                        isSearchResult:(BOOL)isSearchResult
                       currentLocation:(CLLocation *)currentLocation
{
    return [self handleMerchantList:array
                     isSearchResult:isSearchResult
                    currentLocation:currentLocation
                  isDistanceSorting:YES
                         isFavorite:NO
                        deliverable:NO];
}

- (NSMutableArray *)handleMerchantList:(NSArray *)array
                        isSearchResult:(BOOL)isSearchResult
                       currentLocation:(CLLocation *)currentLocation
                     isDistanceSorting:(BOOL)isDistanceSorting
{
    return [self handleMerchantList:array
                     isSearchResult:isSearchResult
                    currentLocation:currentLocation
                  isDistanceSorting:isDistanceSorting
                         isFavorite:NO
                        deliverable:NO];
}

- (NSMutableArray *)handleMerchantList:(NSArray *)array
                        isSearchResult:(BOOL)isSearchResult
                       currentLocation:(CLLocation *)currentLocation
                     isDistanceSorting:(BOOL)isDistanceSorting
                            isFavorite:(BOOL)isFavorite
                           deliverable:(BOOL)deliverable
{
    NSMutableArray *merchantList = [NSMutableArray array];
    
    BOOL isDemoOn = NO;//[NSUserDefaults standardUserDefaults].isDemoOn;
//    BOOL isTestOn = [NSUserDefaults standardUserDefaults].isTestOn;
    
    for(NSDictionary *aDict in array){
        
        Merchant *aMerchant = [[Merchant alloc] initWithDictionary:aDict];
        if(isFavorite){
            aMerchant.favorite = YES;
        } else {
            //Match Favorite
            aMerchant.favorite = [REMOTE isFavoriteMerchant:aMerchant];
        }
        
        if(aMerchant.isTestMode && !aMerchant.isDemoMode && !isSearchResult){
#if !DEBUG
//            if(currentLocation != nil && isTestOn){
//                CLLocationDistance distance1 = [currentLocation distanceFromLocation:aMerchant.location];
//                if(distance1 > 160000.0f){ //Over 1600 * 100 meters (100 miles) - drop
//                    continue;
//                }
//            } else {
//                continue;
//            }
#endif
        } else if(aMerchant.isDemoMode){
            if(!isDemoOn){
                continue;
            }
        }
        
        if(currentLocation != nil){
            
            aMerchant.distance = [APP.location distanceFromLocation:aMerchant.location];
            aMerchant.deliveryDistance = [currentLocation distanceFromLocation:aMerchant.location];
            
            if(deliverable){
                if(aMerchant.deliveryFeePlan == DeliveryFeePlanZone){
                    if([aMerchant polygonDeliveryFeeAt:currentLocation] == NSNotFound){
                        PCLog(@"Skipped by local evaluation:%@",aMerchant);
                        continue;
                    }
                }
            }
            
            if(isDistanceSorting){
                NSUInteger index = [merchantList indexOfObject:aMerchant
                                                 inSortedRange:NSMakeRange(0, [merchantList count])
                                                       options:NSBinarySearchingInsertionIndex | NSBinarySearchingFirstEqual
                                               usingComparator:
                                    ^NSComparisonResult(Merchant *merchant1, Merchant *merchant2) {
                                        
                                        if(merchant1.isDemoMode){
                                            return NSOrderedAscending;
                                        } else if(merchant2.isDemoMode){
                                            return NSOrderedDescending;
                                        }
                                        
                                        if(merchant1.distance > merchant2.distance){
                                            return NSOrderedDescending;
                                        } else if(merchant1.distance < merchant2.distance){
                                            return NSOrderedAscending;
                                        } else {
                                            if(merchant1.merchantNo < merchant2.merchantNo){
                                                return NSOrderedAscending;
                                            } else if (merchant1.merchantNo > merchant2.merchantNo){
                                                return NSOrderedDescending;
                                            } else {
                                                return NSOrderedSame;
                                            }
                                        }
                                    }];
                
                
                [merchantList insertObject:aMerchant atIndex:index];
            } else {
                [merchantList addObject:aMerchant];
            }
        } else {
            [merchantList addObject:aMerchant];
        }
    }
    
    return merchantList;
}

- (RequestResult)requestFavoriteMerchantsWithCurrentLocation:(CLLocation *)currentLocation
                                             completionBlock:(CompletionBlock)completionBlock
{
    if([CRED.customerInfo.authToken length] == 0){
        PCError(@"Auth token is empty for requesting favorite restaurants");
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               CRED.customerInfo.authToken];
    
    return [super requestWithAPI:FAVORITE_MERCHANTS_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSArray *arrayResponse = [response listData];
                        
                        NSMutableArray *favoriteList = [self handleMerchantList:arrayResponse
                                                                 isSearchResult:NO
                                                                currentLocation:currentLocation
                                                              isDistanceSorting:NO
                                                                     isFavorite:YES
                                                                    deliverable:NO];
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, favoriteList, statusCode);
                        });
                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestDeliveryAddressesWithCompletionBlock:(CompletionBlock)completionBlock
{
    NSString *baseParameter = nil;
    
    if([CRED.customerInfo.authToken length] == 0){
        
    } else {
        baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                         CRED.customerInfo.authToken];
    }
    
    return [super requestWithAPI:DELIVERY_ADDRESSES_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSMutableArray *deliveryAddressArray = [NSMutableArray array];
                        
                        for(NSDictionary *deliveryAddressDict in [response listData]){
                            DeliveryAddress *deliveryAddress = [[DeliveryAddress alloc] initWithDictionary:deliveryAddressDict];
                            [deliveryAddressArray addObject:deliveryAddress];
                        }
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, deliveryAddressArray, statusCode);
                        });
                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestAddDeliveryAddress:(DeliveryAddress *)deliveryAddress
                           completionBlock:(CompletionBlock)completionBlock
{
    if(deliveryAddress == nil){
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:DELIVERY_ADDRESSES_PARAM,
                               deliveryAddress.name.urlEncodedString,
                               deliveryAddress.phoneNumber.urlEncodedString,
                               deliveryAddress.street.urlEncodedString,
                               deliveryAddress.street2 ? deliveryAddress.street2.urlEncodedString : @"",
                               deliveryAddress.city.urlEncodedString,
                               deliveryAddress.state.urlEncodedString,
                               deliveryAddress.zip.urlEncodedString,
                               deliveryAddress.deliveryInstruction ? deliveryAddress.deliveryInstruction.urlEncodedString : @"",
                               deliveryAddress.coordinate.latitude,
                               deliveryAddress.coordinate.longitude,
                               deliveryAddress.isDefaultAddress ? @"true" : @"false"];
    
    baseParameter = [baseParameter stringByAppendingFormat:@"&auth_token=%@",
                     CRED.customerInfo.authToken ? CRED.customerInfo.authToken : @""];
    
    return [super requestPostWithAPI:DELIVERY_ADDRESSES_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSMutableArray *deliveryAddressArray = [NSMutableArray array];
                        
                        for(NSDictionary *deliveryAddressDict in [response listData]){
                            DeliveryAddress *deliveryAddress = [[DeliveryAddress alloc] initWithDictionary:deliveryAddressDict];
                            [deliveryAddressArray addObject:deliveryAddress];
                        }
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, deliveryAddressArray, statusCode);
                        });

                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestUpdateDeliveryAddress:(DeliveryAddress *)deliveryAddress
                              completionBlock:(CompletionBlock)completionBlock
{
    if(deliveryAddress == nil){
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:DELIVERY_ADDRESSES_PARAM,
                               deliveryAddress.name.urlEncodedString,
                               deliveryAddress.phoneNumber.urlEncodedString,
                               deliveryAddress.street.urlEncodedString,
                               deliveryAddress.street2 ? deliveryAddress.street2.urlEncodedString : @"",
                               deliveryAddress.city.urlEncodedString,
                               deliveryAddress.state.urlEncodedString,
                               deliveryAddress.zip.urlEncodedString,
                               deliveryAddress.deliveryInstruction ? deliveryAddress.deliveryInstruction.urlEncodedString : @"",
                               deliveryAddress.coordinate.latitude,
                               deliveryAddress.coordinate.longitude,
                               deliveryAddress.isDefaultAddress ? @"true" : @"false"];
    
    baseParameter = [baseParameter stringByAppendingFormat:@"&auth_token=%@",
                     CRED.customerInfo.authToken ? CRED.customerInfo.authToken : @""];
    
    return [super requestPutWithAPI:[NSString stringWithFormat:DELIVERY_ADDRESS_API_URL, deliveryAddress.no]
                          parameter:baseParameter
                withCompletionBlock:completionBlock
                withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSMutableArray *deliveryAddressArray = [NSMutableArray array];
                        
                        for(NSDictionary *deliveryAddressDict in [response listData]){
                            DeliveryAddress *deliveryAddress = [[DeliveryAddress alloc] initWithDictionary:deliveryAddressDict];
                            [deliveryAddressArray addObject:deliveryAddress];
                        }
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, deliveryAddressArray, statusCode);
                        });

                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}


- (RequestResult)requestDeleteDeliveryAddress:(DeliveryAddress *)deliveryAddress
                              completionBlock:(CompletionBlock)completionBlock
{
    if(deliveryAddress == nil){
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:@"&auth_token=%@",
                               CRED.customerInfo.authToken ? CRED.customerInfo.authToken : @""];
    
    return [super requestDeleteWithAPI:[NSString stringWithFormat:DELIVERY_ADDRESS_API_URL, deliveryAddress.no]
                             parameter:baseParameter
                   withCompletionBlock:completionBlock
                   withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSMutableArray *deliveryAddressArray = [NSMutableArray array];
                        
                        for(NSDictionary *deliveryAddressDict in [response listData]){
                            DeliveryAddress *deliveryAddress = [[DeliveryAddress alloc] initWithDictionary:deliveryAddressDict];
                            [deliveryAddressArray addObject:deliveryAddress];
                        }
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, deliveryAddressArray, statusCode);
                        });

                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestUpdateRecentDeliveryAddress:(NSArray *)deliveryAddresses
                                               name:(NSString *)name
                                        phoneNumber:(NSString *)phoneNumber
                                    completionBlock:(CompletionBlock)completionBlock
{
    if(deliveryAddresses == nil){
        return RRParameterError;
    }
    
    NSString *jsonString = [deliveryAddresses JSONString];
    
    NSString *baseParameter = [NSString stringWithFormat:DELIVERY_ADDRESSES_BATCH_RECENT_PARAM,
                               name.urlEncodedString,
                               phoneNumber.urlEncodedString,
                               [jsonString urlEncodedString],
                               CRED.customerInfo.authToken ? CRED.customerInfo.authToken : @""];
    
    return [super requestPostWithAPI:DELIVERY_ADDRESSES_BATCH_RECENT_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSMutableArray *deliveryAddressArray = [NSMutableArray array];
                        
                        for(NSDictionary *deliveryAddressDict in [response listData]){
                            DeliveryAddress *deliveryAddress = [[DeliveryAddress alloc] initWithDictionary:deliveryAddressDict];
                            [deliveryAddressArray addObject:deliveryAddress];
                        }
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, deliveryAddressArray, statusCode);
                        });
                        
                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestAddFavorites:(NSArray *)favoriteList
                     currentLocation:(CLLocation *)currentLocation
                     completionBlock:(CompletionBlock)completionBlock
{
    if([CRED.customerInfo.authToken length] == 0){
        PCError(@"Auth token is empty for requesting adding favorite restaurants");
        return RRParameterError;
    }
    
    if([favoriteList count] == 0){
        PCError(@"Not assigned merchant ids for adding Favorites");
        return RRParameterError;
    }
    
    NSString *jsonList = [favoriteList JSONString];
    
    NSString *baseParameter = [NSString stringWithFormat:ADD_FAVORITE_PARAM,
                               jsonList,
                               CRED.customerInfo.authToken];
    
    return [super requestPostWithAPI:ADD_FAVORITES_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSArray *arrayResponse = [response listData];
                        
                        NSMutableArray *favoriteList = [self handleMerchantList:arrayResponse
                                                                 isSearchResult:NO
                                                                currentLocation:currentLocation
                                                              isDistanceSorting:NO
                                                                     isFavorite:YES
                                                                    deliverable:NO];
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, favoriteList, statusCode);
                        });
                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestRemoveFavorites:(NSArray *)favoriteList
                        currentLocation:(CLLocation *)currentLocation
                        completionBlock:(CompletionBlock)completionBlock
{
    if([CRED.customerInfo.authToken length] == 0){
        PCError(@"Auth token is empty for requesting adding favorite restaurants");
        return RRParameterError;
    }
    
    if([favoriteList count] == 0){
        PCError(@"Not assigned merchant ids for removing");
        return RRParameterError;
    }
    
    if([favoriteList count] > 1){
        PCWarning(@"Remove favorites doesn't allow multiple record yet");
    }

    NSNumber *merchantIdNumber = [favoriteList objectAtIndex:0];
    
    PCSerial merchantId = [merchantIdNumber longLongValue];
    
    NSString *baseParameter = [NSString stringWithFormat:REMOVE_FAVORITE_PARAM,
                               merchantId,
                               CRED.customerInfo.authToken];
    
    return [super requestDeleteWithAPI:REMOVE_FAVORITES_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSArray *arrayResponse = [response listData];
                        
                        NSMutableArray *favoriteList = [self handleMerchantList:arrayResponse
                                                                 isSearchResult:NO
                                                                currentLocation:currentLocation
                                                              isDistanceSorting:NO
                                                                     isFavorite:YES
                                                                    deliverable:NO];
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, favoriteList, statusCode);
                        });
                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestVisitedMerchantsWithCurrentLocation:(CLLocation *)currentLocation
                                            completionBlock:(CompletionBlock)completionBlock
{
    if([CRED.customerInfo.authToken length] == 0){
        PCError(@"Auth token is empty for requesting visited restaurants");
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               CRED.customerInfo.authToken];
    
    return [super requestWithAPI:VISITED_MERCHANTS_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSArray *arrayResponse = [response listData];
                        
                        NSMutableArray *visitedList = [self handleMerchantList:arrayResponse
                                                                isSearchResult:NO
                                                               currentLocation:currentLocation
                                                             isDistanceSorting:NO];
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, visitedList, statusCode);
                        });
                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestAddVisited:(NSArray *)visitedMerchantList
                     currentLocation:(CLLocation *)currentLocation
                     completionBlock:(CompletionBlock)completionBlock
{
    if([CRED.customerInfo.authToken length] == 0){
        PCError(@"Auth token is empty for requesting adding visited restaurants");
        return RRParameterError;
    }
    
    if([visitedMerchantList count] == 0){
        PCError(@"Not assigned merchant ids for adding Visited");
        return RRParameterError;
    }
    
    NSString *jsonList = [visitedMerchantList JSONString];
    
    NSString *baseParameter = [NSString stringWithFormat:ADD_VISITED_PARAM,
                               jsonList,
                               CRED.customerInfo.authToken];
    
    return [super requestPostWithAPI:ADD_VISITED_MERCHANTS_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSArray *arrayResponse = [response listData];
                        
                        NSMutableArray *visitedList = [self handleMerchantList:arrayResponse
                                                                isSearchResult:NO
                                                               currentLocation:currentLocation
                                                             isDistanceSorting:NO];
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, visitedList, statusCode);
                        });
                    });
                    
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}



- (RequestResult)requestAddVisitedMerchantsWithCurrentLocation:(CLLocation *)currentLocation
                                               completionBlock:(CompletionBlock)completionBlock
{
    if([CRED.customerInfo.authToken length] == 0){
        PCError(@"Auth token is empty for requesting favorite restaurants");
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               CRED.customerInfo.authToken];
    
    return [super requestWithAPI:ADD_VISITED_MERCHANTS_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSArray *arrayResponse = [response listData];
                        
                        NSMutableArray *visitedList = [self handleMerchantList:arrayResponse
                                                                isSearchResult:NO
                                                               currentLocation:currentLocation
                                                             isDistanceSorting:NO];
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, visitedList, statusCode);
                        });
                    });
                
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestCheckTable:(PCSerial)merchantNo
                       tableNumber:(NSString *)tableNumber
                   completionBlock:(CompletionBlock)completionBlock
{
    if(merchantNo <= 0){
        return RRParameterError;
    }
    
    if(tableNumber <= 0){
        return RRParameterError;
    }
    
    NSString *apiUrlString = [NSString stringWithFormat:MERCHANT_TABLE_ORDER_API_URL,
                              merchantNo];
    
    NSString *baseParameter = [NSString stringWithFormat:TABLE_CHECK_PARAM,
                               [[tableNumber uppercaseString] urlEncodedString]];
    
    return [super requestWithAPI:apiUrlString
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_async(aQueue, ^{
                    
                    NSMutableDictionary *mutableResponse = [response mutableCopy];
                    
                    if([response objectForKey:@"id"] == nil){
                        if([mutableResponse integerForKey:@"error_code"] == 0){
                            [mutableResponse setObject:[NSNumber numberWithInteger:ResponseErrorEmpty]
                                                forKey:@"error_code"];
                        }
                    } else if([[mutableResponse.data objectForKey:@"status"] isEqual:@"completed"]
                              || [[mutableResponse.data objectForKey:@"status"] isEqual:@"canceled"]){
                        
                        [mutableResponse removeObjectForKey:@"data"];
                        
                        if([mutableResponse integerForKey:@"error_code"] == 0){
                            [mutableResponse setObject:[NSNumber numberWithInteger:ResponseErrorEmpty]
                                                                            forKey:@"error_code"];
                        }
                    }
                    
                    dispatch_async(dispatch_get_main_queue(),^{
                        completionBlock(YES, mutableResponse, statusCode);
                    });
                });
            }];
}

- (RequestResult)requestTheBill:(PCSerial)merchantNo
                    tableNumber:(NSString *)tableNumber
                    deviceToken:(NSString *)deviceToken
                completionBlock:(CompletionBlock)completionBlock
{
    if(merchantNo <= 0){
        return RRParameterError;
    }
    
    if(tableNumber <= 0){
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:BILL_REQUESTS_PARAM,
                               deviceToken ? deviceToken : @"",
                               [[tableNumber uppercaseString] urlEncodedString],
                               merchantNo,
                               CRED.customerInfo.authToken ? CRED.customerInfo.authToken : @""];
    
    return [super requestPostWithAPI:BILL_REQUESTS_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}


- (RequestResult)requestMyBillRequests:(NSString *)deviceToken
                       completionBlock:(CompletionBlock)completionBlock
{
    if([deviceToken length] == 0){
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:MY_BILL_REQUESTS_PARAM,
                               deviceToken];
    
    return [super requestWithAPI:BILL_REQUESTS_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_async(aQueue, ^{
                    
//                    NSMutableDictionary *mutableResponse = nil;
//                    if([response isNSDictionary]){
//                        mutableResponse = [response mutableCopy];
//                        if([mutableResponse.data isEqual:@"empty"]){
//                            [mutableResponse removeObjectForKey:@"data"];
//                        }
//                    } else {
//                        if(response != nil){
//                            mutableResponse = [NSMutableDictionary dictionary];
//                            [mutableResponse setObject:response
//                                                forKey:@"data"];
//                        }
//                    }

                    dispatch_async(dispatch_get_main_queue(),^{
                        completionBlock(YES, response, statusCode);
                    });
                });
            }];
}



- (RequestResult)requestNewOrder:(Order *)order
                 completionBlock:(CompletionBlock)completionBlock
{
    if(order.merchantNo <= 0){
        return RRParameterError;
    }
    
    if(order.tableNumber <= 0){
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:NEW_ORDER_PARAM,
                               (long)order.subTotal,
                               (long)order.subTotalTaxable,
                               (long)order.taxes,
                               order.statusString,
                               order.merchantNo,
                               order.tableNumber == nil ? @"" : [order.tableNumber uppercaseString],
                               @""];
    
    return [super requestPostWithAPI:ORDER_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestNewPayment:(PCSerial)orderNo
                           payment:(Payment *)payment
                       deviceToken:(NSString *)deviceToken
                  appliedDiscounts:(NSArray *)appliedDiscounts
                    smallCutAmount:(PCCurrency)smallCutAmount
                   completionBlock:(CompletionBlock)completionBlock
{
    return [self requestNewPayment:orderNo
                           payment:payment
                  lineItemJsonList:nil
                       deviceToken:deviceToken
                  appliedDiscounts:appliedDiscounts
                    smallCutAmount:smallCutAmount
                paymentMethodNonce:nil
                   completionBlock:completionBlock];
}

- (RequestResult)requestNewPayment:(PCSerial)orderNo
                           payment:(Payment *)payment
                  lineItemJsonList:(NSString *)lineItemJsonList
                       deviceToken:(NSString *)deviceToken
                  appliedDiscounts:(NSArray *)appliedDiscounts
                    smallCutAmount:(PCCurrency)smallCutAmount
                paymentMethodNonce:(NSString *)paymentMethodNonce
                   completionBlock:(CompletionBlock)completionBlock
{
    if(payment.merchantNo <= 0){
        PCError(@"MerchantNo shoud be greater than 0");
        return RRParameterError;
    }
    
    if(payment.orderNo <= 0){
        PCError(@"OrderNo shoud be greater than 0");
        return RRParameterError;
    }
    
    NSString *baseParameter = nil;
    NSString *appliedDiscountsJsonFormat = [appliedDiscounts JSONString];
    
    PCLog(@"appliedDiscountsJsonFormat : %@", appliedDiscountsJsonFormat);

    if(payment.cardFingerPrint != nil){
        baseParameter = [NSString stringWithFormat:PAYMENT_WITH_EXISTING_CARD,
                         (long)payment.netAmount,
                         (long)payment.creditAmount,
                         (long)payment.tipAmount,
                         (long)payment.taxAmount,
                         (long)payment.deliveryFeeAmount,
                         (long)payment.discountAmountByRO + smallCutAmount,
                         (long)payment.discountAmountByMerchant,
                         (long)payment.roServiceFeeAmount,
                         payment.currency,
                         payment.orderNo,
                         payment.merchantNo,
                         payment.pgType, // pg_tye = Braintree implemented
                         CRED.customerInfo.authToken ? CRED.customerInfo.authToken : @"",
                         appliedDiscountsJsonFormat ? [appliedDiscountsJsonFormat urlEncodedString] : @"",
                         deviceToken ? deviceToken : @"",
                         payment.cardFingerPrint];
    } else {
        baseParameter = [NSString stringWithFormat:PAYMENT_WITH_NEW_CARD,
                         (long)payment.netAmount,
                         (long)payment.creditAmount,
                         (long)payment.tipAmount,
                         (long)payment.taxAmount,
                         (long)payment.deliveryFeeAmount,
                         (long)payment.discountAmountByRO + smallCutAmount,
                         (long)payment.discountAmountByMerchant,
                         (long)payment.roServiceFeeAmount,
                         payment.currency,
                         payment.orderNo,
                         payment.merchantNo,
                         1, // pg_tye = Braintree implemented
                         CRED.customerInfo.authToken ? CRED.customerInfo.authToken : @"",
                         appliedDiscountsJsonFormat ? [appliedDiscountsJsonFormat urlEncodedString] : @"",
                         deviceToken ? deviceToken : @"",
                         payment.cardNumber,
                         (long)payment.cardExpireMonth,
                         (long)payment.cardExpireYear,
                         payment.cardCvc,
                         payment.zipCode,
                         paymentMethodNonce == nil ? @"": paymentMethodNonce];
    }
    
//    BOOL autoReceiptMailing = [NSUserDefaults standardUserDefaults].autoMailingMe;
    NSString *mailAddress = [NSUserDefaults standardUserDefaults].emailAddress;
//    if(autoReceiptMailing && [mailAddress length] > 0){
    if([mailAddress length] > 0){
        baseParameter = [baseParameter stringByAppendingFormat:@"&email=%@", mailAddress];
    }
    
    if([lineItemJsonList length] > 0){
        baseParameter = [baseParameter stringByAppendingString:[NSString stringWithFormat:PAYMENT_WITH_LINEITEMS_PARAM,
                                                                lineItemJsonList]];
    }
    
    return [super requestPostWithAPI:PAYMENT_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

//- (RequestResult)requestTableLabels:(PCSerial)merchantNo
//                    completionBlock:(CompletionBlock)completionBlock
//{
//    if(merchantNo <= 0){
//        return RRParameterError;
//    }
//    
//    NSString *apiURL = [NSString stringWithFormat:TABLE_API_URL,
//                        merchantNo];
//    
//    return [super requestWithAPI:apiURL
//                       parameter:nil
//             withCompletionBlock:completionBlock
//             withDataHandleBlock:
//            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
//                dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//                dispatch_async(aQueue, ^{
//                    
//                    /*
//                     "created_at" = "2013-03-19T10:17:27Z";
//                     id = 63;
//                     label = 1;
//                     "merchant_id" = 9;
//                     "updated_at" = "2013-03-19T10:17:27Z";&*/
//                    
//                    NSMutableArray *tableList = nil;
//                    if(response.errorCode == ResponseSuccess){
//                        tableList = [NSMutableArray array];
//                        for(NSDictionary *tableDict in response.data){
//                            TableInfo *tableInfo = [[TableInfo alloc] initWithModelDictionary:tableDict];
//                            [tableList addObject:tableInfo];
//                        }                        
//                    }
//                    
//                    dispatch_async(dispatch_get_main_queue(),^{
//                        completionBlock(YES, tableList, statusCode);
//                    });
//                });
//            }];
//}


- (RequestResult)requestCardRegistration:(MobileCard *)card
                      paymentMethodNonce:(NSString *)paymentMethodNonce
                         completionBlock:(CompletionBlock)completionBlock
{
    if(card == nil){
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:MOBILE_CARD_PARAM,
                               card.cardNumber,
                               (long)card.cardExpireMonth,
                               (long)card.cardExpireYear,
                               card.cardCvc,
                               card.zipCode,
                               paymentMethodNonce];
    
    baseParameter = [baseParameter stringByAppendingFormat:@"&auth_token=%@",
                     CRED.customerInfo.authToken ? CRED.customerInfo.authToken : @""];
    
    return [super requestPostWithAPI:CARDS_CREATE_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{

                        NSMutableArray *cardsArray = [NSMutableArray array];

                        for(NSDictionary *cardDict in [response listData]){
                            MobileCard *card = [[MobileCard alloc] initWithDictionary:cardDict];
                            [cardsArray addObject:card];
                        }
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, cardsArray, statusCode);
                        });
                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestRemoveCard:(MobileCard *)card
                   completionBlock:(CompletionBlock)completionBlock
{
    if(card == nil){
        return RRParameterError;
    }
    
    if(CRED.customerInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    NSString *apiURL = [NSString stringWithFormat:REMOVE_CARDS_API_URL,
                        card.cardId];
    
    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               CRED.customerInfo.authToken];
    
    return [super requestDeleteWithAPI:apiURL
                             parameter:baseParameter
                   withCompletionBlock:completionBlock
                   withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSMutableArray *cardsArray = [NSMutableArray array];
                        
                        for(NSDictionary *cardDict in [response listData]){
                            MobileCard *card = [[MobileCard alloc] initWithDictionary:cardDict];
                            [cardsArray addObject:card];
                        }
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, cardsArray, statusCode);
                        });
                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestCardListWithCompletionBlock:(CompletionBlock)completionBlock
{
    if(CRED.customerInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }

    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               CRED.customerInfo.authToken];
    
    return [super requestWithAPI:CARDS_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSMutableArray *cardsArray = [NSMutableArray array];
                        
                        for(NSDictionary *cardDict in [response listData]){
                            MobileCard *card = [[MobileCard alloc] initWithDictionary:cardDict];
                            [cardsArray addObject:card];
                        }
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, cardsArray, statusCode);
                        });
                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}


- (RequestResult)requestReceiptListWithCompletionBlock:(CompletionBlock)completionBlock
{
    if([CRED.customerInfo.authToken length] == 0){
        PCError(@"Auth token is empty for requesting receipt list");
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               CRED.customerInfo.authToken];
    
    return [super requestWithAPI:RECEIPT_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSArray *arrayResponse = [response listData];
                        
                        NSMutableArray *receiptList = [NSMutableArray array];
                        
                        for(NSDictionary *dict in arrayResponse){
                            Receipt *receipt = [[Receipt alloc] initWithDictionary:dict];
                            [receiptList addObject:receipt];
                        }
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, receiptList, statusCode);
                        });
                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestGetReceiptWithPaymentNo:(PCSerial)paymentNo
                              completionBlock:(CompletionBlock)completionBlock
{
    if(paymentNo == 0){
        PCError(@"Payment Id should be specified for getting receipt");
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:GET_RECEIPT_PARAM,
                               paymentNo,
                               CRED.customerInfo.authToken ? CRED.customerInfo.authToken : @""];
    
    return [super requestWithAPI:GET_RECEIPT_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestRemoveReceipt:(Receipt *)receipt
                   completionBlock:(CompletionBlock)completionBlock
{
    if(receipt == nil){
        return RRParameterError;
    }
    
    if(CRED.customerInfo.authToken == nil){
        PCError(@"Auth token is not specified. Delete receipt process needs auth token.");
        return RRAuthenticateError;
    }
    
    NSString *apiURL = [NSString stringWithFormat:REMOVE_RECEIPT_API_URL,
                        receipt.receiptNo];
    
    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               CRED.customerInfo.authToken];
    
    return [super requestDeleteWithAPI:apiURL
                             parameter:baseParameter
                   withCompletionBlock:completionBlock
                   withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestRemoveOrder:(Order *)order
                      completionBlock:(CompletionBlock)completionBlock
{
    if(order == nil){
        return RRParameterError;
    }
    
    
    NSString *baseParameter = [NSString stringWithFormat:REMOVE_ORDER_PARAM,
                               CRED.customerInfo.authToken ? CRED.customerInfo.authToken : @"",
                               order.orderNo];
    
    return [super requestWithAPI:REMOVE_ORDER_API_URL
                             parameter:baseParameter
                   withCompletionBlock:completionBlock
                   withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}


- (RequestResult)requestAddReceipts:(NSArray *)receiptsList
                   completionBlock:(CompletionBlock)completionBlock
{
    if([CRED.customerInfo.authToken length] == 0){
        PCError(@"Auth token is empty for requesting adding receipts");
        return RRParameterError;
    }
    
    if([receiptsList count] == 0){
        PCError(@"Not assigned receipts ids for adding");
        return RRParameterError;
    }
    
    NSString *jsonList = [receiptsList JSONString];
    
    NSString *baseParameter = [NSString stringWithFormat:ADD_RECEIPTS_PARAM,
                               jsonList,
                               CRED.customerInfo.authToken];
    
    return [super requestPostWithAPI:ADD_RECEIPTS_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSArray *arrayResponse = [response listData];
                        
                        NSMutableArray *receiptList = [NSMutableArray array];
                        
                        for(NSDictionary *dict in arrayResponse){
                            Receipt *receipt = [[Receipt alloc] initWithDictionary:dict];
                            [receiptList addObject:receipt];
                        }
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, receiptList, statusCode);
                        });
                    });
                    
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestGetRewardInPayment:(Payment *)payment
                           completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.customerInfo.authToken == nil){
        PCError(@"Auth token is not specified. Get reward process needs auth token.");
        return RRAuthenticateError;
    }
    
    if(payment.merchantNo <= 0){
        PCError(@"MerchantNo shoud be greater than 0 In Payment");
        return RRParameterError;
    }
    
    if(payment.orderNo <= 0){
        PCError(@"OrderNo shoud be greater than 0 In Payment");
        return RRParameterError;
    }
    
    NSString *baseParameter = nil;
    
    baseParameter = [NSString stringWithFormat:REWARD_GIVE_PARAM,
                     payment.paymentNo,
                     CRED.customerInfo.authToken];
    
    return [super requestWithAPI:REWARD_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestValidatePromotion:(Promotion *)promotion
                                 merchant:(Merchant *)merchant
                           completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.customerInfo.authToken == nil){
        PCError(@"Auth token is not specified. Validate Promotion needs auth token.");
        return RRAuthenticateError;
    }
    
    if(promotion.promotionNo <= 0){
        PCError(@"Promomotion No shoud be greater than 0 to validate promotion");
        return RRParameterError;
    }
    
    NSString *baseParameter = nil;
    
    baseParameter = [NSString stringWithFormat:VALIDATE_PROMOTION_PARAM,
                     promotion.promotionNo,
                     merchant.merchantNo,
                     CRED.customerInfo.authToken];
    
    return [super requestWithAPI:VALIDATE_PROMOTION_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestValidatePromoCode:(NSString *)promoCode
                          completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.customerInfo.authToken == nil){
        PCError(@"Auth token is not specified. Validate Promotion needs auth token.");
        return RRAuthenticateError;
    }
    
    if([promoCode length] <= 0){
        return RRParameterError;
    }
    
    promoCode = [promoCode uppercaseString];
    
    NSString *baseParameter = nil;
    
    baseParameter = [NSString stringWithFormat:VALIDATE_PROMO_CODE_PARAM,
                     promoCode ? [promoCode urlEncodedString] : @"",
                     CRED.customerInfo.authToken];
    
    return [super requestWithAPI:VALIDATE_PROMO_CODE_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestNoticeWithCompletionBlock:(CompletionBlock)completionBlock
{
    return [super requestWithAPI:NOTICE_API_URL
                       parameter:@"target=customer"
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestCustomerCloseOrder:(PCSerial)orderNo
                           completionBlock:(CompletionBlock)completionBlock
{
    NSString *baseParameter = nil;
    
    if([CRED.customerInfo.authToken length] > 0){
        baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                                   CRED.customerInfo.authToken];
    }
    
    NSString *apiURL = [NSString stringWithFormat:CUSTOMER_CLOSE_ORDER_API_URL,
                        orderNo];
    
    return [super requestWithAPI:apiURL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestMailMeReceipt:(Receipt *)receipt
                         emailAddress:(NSString *)emailAddress
                      completionBlock:(CompletionBlock)completionBlock
{
    if(receipt.receiptNo == 0){
        return [self requestMailMeOldReceipt:receipt.paymentNo
                                emailAddress:emailAddress
                             completionBlock:completionBlock];
    }
    
    NSString *baseParameter = nil;
    
    baseParameter = [NSString stringWithFormat:MAIL_ME_PARAM,
                     [emailAddress urlEncodedString],
                     CRED.customerInfo.authToken ? CRED.customerInfo.authToken : @""];
    
    NSString *apiURL = [NSString stringWithFormat:MAIL_ME_API_URL,
                        receipt.receiptNo];
    
    return [super requestWithAPI:apiURL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestMailMeOldReceipt:(PCSerial)paymentNo
                            emailAddress:(NSString *)emailAddress
                         completionBlock:(CompletionBlock)completionBlock
{
    NSString *baseParameter = nil;
    
    baseParameter = [NSString stringWithFormat:MAIL_ME_OLD_PARAM,
                     [emailAddress urlEncodedString]];
    
    NSString *apiURL = [NSString stringWithFormat:MAIL_ME_OLD_API_URL,
                        paymentNo];
    
    return [super requestWithAPI:apiURL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestOrdersAt:(NSUInteger)page
                 completionBlock:(CompletionBlock)completionBlock
{
    NSString *baseParameter = nil;
    
    NSString *authToken = CRED.customerInfo.authToken;
    
    if(CRED.isSignedIn){
        baseParameter = [NSString stringWithFormat:MY_ORDERS_PARAM,
                         (unsigned long)page,
                         authToken ? authToken : @""];
    } else {
        baseParameter = [NSString stringWithFormat:MY_ORDERS_PARAM_NO_KEY,
                         (unsigned long)page];
    }
    
    return [super requestWithAPI:MY_ORDERS_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                
                dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_async(aQueue, ^{
                    
                    NSArray *completedOrderDicts = [response objectForKey:@"closed"];
                    NSArray *activeOrderDicts = [response objectForKey:@"active"];
                    
                    NSMutableArray *completedOrders = [NSMutableArray array];
                    NSMutableArray *activeOrders = [NSMutableArray array];
                    
                    for(NSDictionary *dict in activeOrderDicts){
                        Order *order = [[Order alloc] initWithDictionary:dict];
                        order.customerClose = NO;
                        [activeOrders addObject:order];
                    }
                    
                    for(NSDictionary *dict in completedOrderDicts){
                        Order *order = [[Order alloc] initWithDictionary:dict];
                        order.customerClose = YES;
                        [completedOrders addObject:order];
                    }
                    
                    NSDictionary *responseDict = @{@"completed":completedOrders?completedOrders:[NSNull null],
                                                   @"active":activeOrders?activeOrders:[NSNull null]};
                    
                    dispatch_async(dispatch_get_main_queue(),^{
                        completionBlock(YES, responseDict, statusCode);
                    });
                });

            }];
}

- (RequestResult)requestCreateRapid:(PCSerial)orderNo
                              title:(NSString *)title
                              image:(UIImage *)image
                           location:(CLLocation *)location
                    completionBlock:(CompletionBlock)completionBlock
{
    NSString *authToken = CRED.customerInfo.authToken;
    
    if(authToken == nil){
        PCError(@"Auth token is not specified. Creating RapidOrder process is required.");
        return RRAuthenticateError;
    }
    
    NSString *baseParameter = nil;
    
    baseParameter = [NSString stringWithFormat:RAPID_ORDER_CREATE_PARAM,
                     authToken,
                     orderNo,
                     title,
                     location.coordinate.latitude,
                     location.coordinate.longitude
                     ];
    
    if(image == nil){
        return [super requestPostWithAPI:RAPID_ORDER_API_URL
                              parameter:baseParameter
                    withCompletionBlock:completionBlock
                    withDataHandleBlock:
                ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                    if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                        if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                            // Profile update done
                            
                        } else {
                            // Profile update faile
                            
                        }
                    }
                    completionBlock(YES, response, statusCode);
                }];
    } else {
        return [super requestWithAPI:RAPID_ORDER_API_URL
                               image:image
                              method:RequestMethodPost
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
                ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                    if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                        // Profile update done
                        
                    } else {
                        // Profile update faile
                    }
                    completionBlock(YES, response, statusCode);
                }];
    }
}

- (RequestResult)requestUpdateRapidReOrder:(FavoriteOrder *)favoriteOrder
                                      type:(FOupdateType)type
                           completionBlock:(CompletionBlock)completionBlock;
{
    NSString *authToken = CRED.customerInfo.authToken;
    
    if(authToken == nil){
        PCError(@"Auth token is not specified. Updating RapidOrder process is required.");
        return RRAuthenticateError;
    }
    
    NSMutableString *baseParameter = nil;
    
    NSString *apiURL = [NSString stringWithFormat:MODIFY_RAPID_ORDER_API_URL,
                        favoriteOrder.favoriteOrderNo];
    
    baseParameter = [NSMutableString stringWithFormat:AUTH_TOKEN_PARAM,
                     authToken
                     ];
    

    switch(type){
        case FOupdateTypeMain:
            // Nothing - Addeed below!
            break;
        case FOupdateTypeRequest:
            [baseParameter appendFormat:@"&rorder[customer_request]=%@",[favoriteOrder.customerRequest urlEncodedString]];
            break;
        case FOupdateTypePayment:
            [baseParameter appendFormat:@"&rorder[card_id]=%lld",favoriteOrder.card.cardId];
            break;
        case FOupdateTypeAddress:
            [baseParameter appendFormat:@"&rorder[delivery_address_street_1]=%@",[favoriteOrder.address1 urlEncodedString]];
            [baseParameter appendFormat:@"&rorder[delivery_address_street_2]=%@",[favoriteOrder.address2 urlEncodedString]];
            [baseParameter appendFormat:@"&rorder[delivery_address_zipcode]=%@",[favoriteOrder.zip urlEncodedString]];
            [baseParameter appendFormat:@"&rorder[delivery_phone_number]=%@",[favoriteOrder.phoneNumber urlEncodedString]];
            [baseParameter appendFormat:@"&rorder[delivery_customer_name]=%@",[favoriteOrder.receiverName urlEncodedString]];
            [baseParameter appendFormat:@"&rorder[delivery_address_city]=%@",[favoriteOrder.city urlEncodedString]];
            [baseParameter appendFormat:@"&rorder[delivery_address_state]=%@",[favoriteOrder.state urlEncodedString]];
            [baseParameter appendFormat:@"&rorder[pickup_after]=%ld",(long)favoriteOrder.pickupAfter];
            
            [baseParameter appendFormat:@"&rorder[delivery_latitude]=%f",favoriteOrder.location.coordinate.latitude];
            [baseParameter appendFormat:@"&rorder[delivery_longitude]=%f",favoriteOrder.location.coordinate.longitude];
            
            break;
        default:
            break;
    }
    
     if(favoriteOrder.image != nil && type == FOupdateTypeMain){
         [baseParameter appendFormat:@"&rorder[title]=%@",favoriteOrder.title];
         
         return [super requestWithAPI:apiURL
                                image:favoriteOrder.image
                               method:RequestMethodPut
                            parameter:baseParameter
                  withCompletionBlock:completionBlock
                  withDataHandleBlock:
                 ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                     if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                         // Favorite Order Update done
                     } else {
                         // Favorite Order Update faile
                     }
                     completionBlock(YES, response, statusCode);
                 }];
        
    } else {
        
        if(type == FOupdateTypeMain){
                [baseParameter appendFormat:@"&rorder[title]=%@", [favoriteOrder.title urlEncodedString]];
        }
        
        return [super requestPutWithAPI:apiURL
                              parameter:baseParameter
                    withCompletionBlock:completionBlock
                    withDataHandleBlock:
                ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                    if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                        if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                            // Profile update done
                        } else {
                            // Profile update faile
                        }
                    }
                    completionBlock(YES, response, statusCode);
                }];
    }
}

- (RequestResult)requestDeleteRapidReOrder:(FavoriteOrder *)favoriteOrder
                           completionBlock:(CompletionBlock)completionBlock
{
    NSString *authToken = CRED.customerInfo.authToken;
    
    if(authToken == nil){
        PCError(@"Auth token is not specified. Updating RapidOrder process is required.");
        return RRAuthenticateError;
    }
    
    NSString *baseParameter = nil;
    
    NSString *apiURL = [NSString stringWithFormat:MODIFY_RAPID_ORDER_API_URL,
                        favoriteOrder.favoriteOrderNo];
    
    baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                     authToken];
    
    return [super requestDeleteWithAPI:apiURL
                             parameter:baseParameter
                   withCompletionBlock:completionBlock
                   withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                    if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                        // Favorite Order Delete done
                        
                    } else {
                        // Favorite Order Delete faile
                        
                    }
                }
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestGetRapidOrdersWithCompletionBlock:(CompletionBlock)completionBlock
{
    NSString *authToken = CRED.customerInfo.authToken;
    
    if(authToken == nil){
        // Remove this logs for improving speed when entering Rapid Order tab
//        PCError(@"Auth token is not specified. Getting RapidOrder process is required.");
        return RRAuthenticateError;
    }
    
    NSString *baseParameter = nil;
    
    baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                     authToken];
    
    return [super requestWithAPI:RAPID_ORDER_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){

                if([response isKindOfClass:[NSArray class]]){
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSMutableArray *resultArray = [NSMutableArray array];
                        if([response isKindOfClass:[NSArray class]]){
                            for(NSDictionary *reorderDict in response){
                                FavoriteOrder *favoriteOrder = [[FavoriteOrder alloc] initWithDictionary:reorderDict];
                            [resultArray addObject:favoriteOrder];
                            }
                        }
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, resultArray, statusCode);
                        });
                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestOrderFavoriteOrder:(PCSerial)favoriteOrderNo
                                   payment:(Payment *)payment
                               deviceToken:(NSString *)deviceToken
                          appliedDiscounts:(NSArray *)appliedDiscounts
                            smallCutAmount:(PCCurrency)smallCutAmount
                           completionBlock:(CompletionBlock)completionBlock
{
    NSString *authToken = CRED.customerInfo.authToken;
    
    if(authToken == nil){
//        PCError(@"Auth token is not specified. Getting RapidOrder process is required.");
        return RRAuthenticateError;
    }
    
    NSString *baseParameter = nil;
    NSString *appliedDiscountsJsonFormat = [appliedDiscounts JSONString];
    
    PCLog(@"appliedDiscountsJsonFormat : %@", appliedDiscountsJsonFormat);
    
    baseParameter = [NSString stringWithFormat:ORDER_RAPID_ORDER_PARAM,
                     authToken,
                     favoriteOrderNo,
                     deviceToken,
                     (long)payment.netAmount,
                     (long)payment.tipAmount,
                     (long)payment.taxAmount,
                     (long)payment.subTotalTaxable,
                     (long)payment.deliveryFeeAmount,
                     (long)payment.discountAmountByRO + smallCutAmount,
                     (long)payment.discountAmountByMerchant,
                     (long)payment.roServiceFeeAmount,
                     appliedDiscountsJsonFormat ? [appliedDiscountsJsonFormat urlEncodedString] : @""];
    
//    BOOL autoReceiptMailing = [NSUserDefaults standardUserDefaults].autoMailingMe;
    NSString *mailAddress = [NSUserDefaults standardUserDefaults].emailAddress;
//    if(autoReceiptMailing && [mailAddress length] > 0){
    if([mailAddress length] > 0){
        baseParameter = [baseParameter stringByAppendingFormat:@"&email=%@", mailAddress];
    }
    
    return [super requestPostWithAPI:ORDER_RAPID_ORDER_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestServiceArea:(NSDictionary *)paramDict
                    completionBlock:(CompletionBlock)completionBlock
{
    NSString *baseParameter = nil;
    
    baseParameter = paramDict.urlString;
    
    return [super requestPostWithAPI:REQUEST_SERVICE_AREA_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestAppRating:(NSInteger)step
                             rate:(NSInteger)rate
                          comment:(NSString *)comment
                  completionBlock:(CompletionBlock)completionBlock
{
    NSString *baseParameter = nil;
    
    baseParameter = [NSString stringWithFormat:CREATE_RATING_PARAM,
                     CRED.customerInfo.authToken ? CRED.customerInfo.authToken : @"",
                     (long)step,
                     (long)rate,
                     comment ? comment : @""];
    
    return [super requestPostWithAPI:CREATE_RATING_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestBrainTreeClientTokenWithCompletionBlock:(CompletionBlock)completionBlock
{
    return [super requestWithAPI:BRAINTREE_CLIENT_TOKEN_API_URL
                       parameter:nil
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}





@end
