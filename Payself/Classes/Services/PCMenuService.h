//
//  PCMenuService.h
//  RushOrder
//
//  Created by Conan on 5/29/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCServiceBase.h"
#import "Cart.h"
#import "Merchant.h"

#define MENU [PCMenuService sharedService]

typedef enum menuListType_{
    MenuListTypeNone        = 0,
    MenuListTypeDinein      = 1 << 0,
    MenuListTypeTakeout     = 1 << 1,
    MenuListTypeDelivery    = 1 << 2,
    MenuListTypeAll         = MenuListTypeDinein | MenuListTypeTakeout | MenuListTypeDelivery,
} MenuListType;

@interface PCMenuService : PCServiceBase

+ (PCMenuService *)sharedService;

- (RequestResult)requestMenu:(Merchant *)merchant
                        type:(MenuListType)type
             completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestMenuPan:(Merchant *)merchant
                completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestAddItems:(NSArray *)lineItems
                      merchantNo:(PCSerial)merchantNo
                      tableLabel:(NSString *)tableLabel
                     deviceToken:(NSString *)deviceToken
                       authToken:(NSString *)authToken
                 completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestUpdateItems:(Cart *)cart
                          lineItems:(NSArray *)lineItems
                        deviceToken:(NSString *)deviceToken
                    completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestDeleteItems:(Cart *)cart
                          lineItems:(NSArray *)lineItems
                        deviceToken:(NSString *)deviceToken
                    completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestAddItemsToOrder:(Order *)order
                              lineItems:(NSArray *)lineItems
                            deviceToken:(NSString *)deviceToken
                              authToken:(NSString *)authToken
                        completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestUpdateItemsToOrder:(Order *)order
                                 lineItems:(NSArray *)lineItems
                               deviceToken:(NSString *)deviceToken
                           completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestDeleteItemsFromOrder:(Order *)order
                                   lineItems:(NSArray *)lineItems
                                 deviceToken:(NSString *)deviceToken
                                   authToken:(NSString *)authToken
                             completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestDeleteItemsFromOrder:(Order *)order
                                   lineItems:(NSArray *)lineItems
                           withCustomerClose:(BOOL)withCustomerClose
                                 deviceToken:(NSString *)deviceToken
                                   authToken:(NSString *)authToken
                             completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestOrderAndCart:(PCSerial)merchantId
                           tableInfo:(TableInfo *)tableInfo
                     completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestUpdateCart:(Cart *)cart
                       deviceToken:(NSString *)deviceToken
                   completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestGettingCart:(Cart *)cart
                    completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestMyOrderListAuthToken:(NSString *)authToken
                             completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestMyOrderId:(PCSerial)orderId
                        authToken:(NSString *)authToken
                  completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestAddOrdersAuthToken:(NSString *)authToken
                           completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestNearMeAuthToken:(NSString *)authToken
                               location:(CLLocation *)currentLocation
                                   page:(NSInteger)page
                                   seed:(NSInteger)seed
                        completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestPopularMenuInLocation:(CLLocation *)currentLocation
                                         page:(NSInteger)page
                                         seed:(NSInteger)seed
                              completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestPickupOrder:(NSArray *)lineItems
                         merchantNo:(PCSerial)merchantNo
                         flagNumber:(NSString *)flagNumber
                       receiverName:(NSString *)receiverName
                    customerRequest:(NSString *)customerRequest
                        deviceToken:(NSString *)deviceToken
                          authToken:(NSString *)authToken
                     subTotalAmount:(PCCurrency)subTotalAmount
              subTotalAmountTaxable:(PCCurrency)subTotalAmountTaxable
                          taxAmount:(PCCurrency)taxAmount
                    completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestTakeoutOrder:(NSArray *)lineItems
                          merchantNo:(PCSerial)merchantNo
                         pickupAfter:(NSInteger)pickupAfter
                     customerRequest:(NSString *)customerRequest
                        receiverName:(NSString *)receiverName
                       receiverPhone:(NSString *)receiverPhone
                         deviceToken:(NSString *)deviceToken
                           authToken:(NSString *)authToken
                      subTotalAmount:(PCCurrency)subTotalAmount
               subTotalAmountTaxable:(PCCurrency)subTotalAmountTaxable
                           taxAmount:(PCCurrency)taxAmount
                     completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestDeliveryOrder:(NSArray *)lineItems
                           merchantNo:(PCSerial)merchantNo
                          pickupAfter:(NSInteger)pickupAfter
                      customerRequest:(NSString *)customerRequest
                          deviceToken:(NSString *)deviceToken
                            authToken:(NSString *)authToken
                       subTotalAmount:(PCCurrency)subTotalAmount
                subTotalAmountTaxable:(PCCurrency)subTotalAmountTaxable
                            taxAmount:(PCCurrency)taxAmount
                         addressState:(NSString *)state
                          addressCity:(NSString *)city
                       addressStreet1:(NSString *)street1
                       addressStreet2:(NSString *)street2
                           addressZip:(NSString *)zip
                          phoneNumber:(NSString *)phoneNumber
                         receiverName:(NSString *)receiverName
                    deliveryFeeAmount:(PCCurrency)deliveryFeeAmount
                   roServiceFeeAmount:(PCCurrency)roServiceFeeAmount
                      completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestRenewOrder:(Order *)order
                       deviceToken:(NSString *)deviceToken
                         authToken:(NSString *)authToken
                   completionBlock:(CompletionBlock)completionBlock;


- (RequestResult)requestUpdateSubmittedForCart:(Cart *)cart
                               completionBlock:(CompletionBlock)completionBlock;


@end
