//
//  PCPendingRequest.h
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestAPI.h"

@class PCPendingRequest;

typedef void (^CompletionBlock)(BOOL isSuccess, id response, NSInteger statusCode);
typedef void (^DataHandleBlock)(id response, CompletionBlock completionBlock, NSInteger statusCode);

@interface PCPendingRequest : NSObject

@property (strong, nonatomic) NSURLRequest *request;
@property (strong, nonatomic) NSURLResponse *response;
@property (strong, nonatomic) CompletionBlock completionBlock;
@property (strong, nonatomic) DataHandleBlock dataHandleBlock;
@property (strong, nonatomic) NSMutableData *data;
@property (strong, nonatomic) NSURLConnection *connection;
@property (nonatomic) NSInteger statusCode;

+ (PCPendingRequest *)pendingRequestWithCompletionBlock:(CompletionBlock)completionBlock
                                    withDataHandleBlock:(DataHandleBlock)dataHandleBlock;
- (id)initWithCompletionBlock:(CompletionBlock)completionBlock
          withDataHandleBlock:(DataHandleBlock)dataHandleBlock;
@end