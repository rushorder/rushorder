//
//  PCPendingRequest.m
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCPendingRequest.h"

@implementation PCPendingRequest

+ (PCPendingRequest *)pendingRequestWithCompletionBlock:(CompletionBlock)completionBlock
                                    withDataHandleBlock:(DataHandleBlock)dataHandleBlock
{
    PCPendingRequest *pendingRequest = [[PCPendingRequest alloc]
                                        initWithCompletionBlock:completionBlock
                                        withDataHandleBlock:dataHandleBlock];
    return pendingRequest;
}

- (void)dealloc
{
    self.completionBlock = nil;
    self.dataHandleBlock = nil;
}

- (id)initWithCompletionBlock:(CompletionBlock)completionBlock
          withDataHandleBlock:(DataHandleBlock)dataHandleBlock
{
    self = [super init];
    if(self != nil){
        self.completionBlock = completionBlock;
        self.dataHandleBlock = dataHandleBlock;
        self.data = [NSMutableData data];
    }
    
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ { \n\t Data:%@ \n\t Completion Block:%@ \n\t Data Handle block:%@ \n }",
            [super description],
            self.data,
            self.completionBlock,
            self.dataHandleBlock];
}
@end
