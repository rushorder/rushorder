//
//  RestAPI.h
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#ifndef RushOrder_RestAPI_h
#define RushOrder_RestAPI_h

#import "http-status.h"

#pragma mark - Customer verification

/////////////////////////////////////////////////////
#pragma mark for User Account

#define CUSTOMER_GENERAL_API_URL  @"/api/v2/customers"
#define CUSTOMER_SIGN_UP_PARAM @\
"customer[email]=%@\
&customer[password]=%@\
&customer[first_name]=%@\
&customer[last_name]=%@\
&customer[phone_number]=%@\
&customer[customer_device_token]=%@"

#define CUSTOMER_PROFILE_PARAM @\
"customer[first_name]=%@\
&customer[last_name]=%@\
&customer[birthday]=%@\
&customer[phone_number]=%@\
&auth_token=%@"

#define CUSTOMER_PASSWORD_PARAM @\
"customer[current_password]=%@\
&customer[password]=%@\
&auth_token=%@"

#define CUSTOMER_SIGN_IN_API_URL  @"/api/v2/customers/sign_in"
#define CUSTOMER_SIGN_IN_PARAM @\
"customer[email]=%@\
&customer[password]=%@\
&customer[customer_device_token]=%@"

#define CUSTOMER_SIGN_OUT_API_URL  @"/api/v2/customers/sign_out"

#define CUSTOMER_PROFILE_API_URL @"/api/v2/customers/profile"

#define CUSTOMER_FORGOT_PASSWORD_API_URL @"/api/v2/customers/password"
#define CUSTOMER_FORGOT_PASSWORD_PARAM @\
"email=%@"

/////////////////////////////////////////////////////
#pragma mark for SMS disabled device(like iPod)

#define CUSTOMER_REQUEST_VERIFICATION_API_URL @"/api/v2/customers/request_verify_code"

#define CUSTOMER_REQUEST_VERIFICATION_CODE_PARAM @\
"phone_number=%@\
&auth_token=%@"

#define CUSTOMER_VERIFICATION_NOTSMS_API_URL  @"/api/v2/customers/verify_phone_number"

#define CUSTOMER_VERIFICATION_NOTSMS_PARAM @\
"phone_number=%@\
&verification_code=%@\
&auth_token=%@"

/////////////////////////////////////////////////////
#pragma mark customer update

#define CUSTOMERS_API_URL @"/api/v2/customers"

#define CUSTOMER_UPDATE_DEVICE_TOKEN_PARAM @\
"device_token=%@\
&auth_token=%@"

/////////////////////////////////////////////////////
#pragma mark CREDIT
#define CREDIT_API_URL @"/api/v2/customers/credit" // !!!: &reset_referral_count=true is included in the code

#pragma mark EVENT
#define MERCHANT_EVENT_API_URL @"/api/v2/customer/event"
#define MERCHANT_EVENT_PARAM @\
"merchant_id=%lld"

#define PAYMENT_CREDIT_API_URL @"/api/v2/payments/reward"
#define PAYMENT_CREDIT_PARAM @\
"payment_id=%lld\
&auth_token=%@"

#define PAYMENT_CREDIT_POLICY_API_URL @"/api/v2/merchants/credit_policy"
#define PAYMENT_CREDIT_POLICY_PARAM @\
"merchant_id=%lld"

#define CONFIG_API_URL @"/api/v2/config"
#define CONFIG_PARAM @\
"customer_ios_version=%@"

#pragma mark REFERRAL_CODE
#define REFERRAL_API_URL @"/api/v2/customers/referral_code"

//////////////////////////////////////////////////

#define AUTH_TOKEN_PARAM @\
"auth_token=%@"

#define APPENDING_AUTH_TOKEN_PARAM @\
"&auth_token=%@"


#pragma mark SIGN UP
#define SIGN_UP_API_URL @"/api/v2/owners/"
#define SIGN_UP_PARAM @\
"owner[email]=%@\
&owner[password]=%@\
&owner[role]=%@\
&owner[name]=%@"

//#pragma mark SIGN IN
//#define SIGN_IN_API_URL @"/api/v2/owners/sign_in"
//#define SIGN_IN_PARAM @\
//"owner[email]=%@\
//&owner[password]=%@\
//&owner[device_token]=%@\
//&merchant_version=%@"

#pragma mark SIGN IN
#define SIGN_IN_API_URL @"/api/v2/owners/sign_in"
#define SIGN_IN_PARAM @\
"owner[email]=%@\
&owner[password]=%@\
&owner[device_token]=%@\
&device_name=%@\
&merchant_version=%@"

#define SIGN_IN_MERCHANT_PARAM @\
"&current_merchant_id=%lld"

#pragma mark SIGN OUT
#define SIGN_OUT_API_URL @"/api/v2/owners/sign_out"


#define MERCHANT_FORGOT_PASSWORD_API_URL @"/api/v2/owners/password"
#define MERCHANT_FORGOT_PASSWORD_PARAM @\
"email=%@"

#define MERCHANT_PASSWORD_API @"/api/v2/owners/"

#define MERCHANT_PASSWORD_PARAM @\
"owner[current_password]=%@\
&owner[password]=%@\
&auth_token=%@"


#pragma mark - General Service
#define FAQ_API_URL @"/customer/faq.json"
#define CLOUD_PHOTOS_API_URL @"/api/v2/cloud_photos"

#pragma mark - Merchant Service
#pragma mark MERCHANTS

#define MERCHANT_API_URL @"/api/v2/merchants/%lld.json"

#define MY_MERCHANT_API_URL @"/api/v2/merchants/mine.json"

#define MERCHANTS_API_URL @"/api/v2/merchants.json"

#define VISITED_MERCHANTS_API_URL @"/api/v2/customers/visited_merchants"

#define ADD_VISITED_MERCHANTS_API_URL @"/api/v2/customers/add_visited_merchants"

#define ADD_VISITED_PARAM @\
"merchant_ids=%@\
&auth_token=%@"

#define FAVORITE_MERCHANTS_API_URL @"/api/v2/customers/favorite_merchants"

#define ADD_FAVORITES_API_URL @"/api/v2/customers/add_favorite_merchants"

#define ADD_FAVORITE_PARAM @\
"merchant_ids=%@\
&auth_token=%@"

#define REMOVE_FAVORITES_API_URL @"/api/v2/customers/remove_favorite_merchant"

#define REMOVE_FAVORITE_PARAM @\
"merchant_id=%lld\
&auth_token=%@"

#define DELIVERY_ADDRESSES_API_URL @"/api/v2/delivery_addresses"

#define DELIVERY_ADDRESS_API_URL @"/api/v2/delivery_addresses/%lld"

#define DELIVERY_ADDRESSES_PARAM @\
"delivery_address[name]=%@\
&delivery_address[phone_number]=%@\
&delivery_address[street]=%@\
&delivery_address[street2]=%@\
&delivery_address[city]=%@\
&delivery_address[state]=%@\
&delivery_address[zip]=%@\
&delivery_address[delivery_instruction]=%@\
&delivery_address[latitude]=%lf\
&delivery_address[longitude]=%lf\
&delivery_address[default]=%@"

#define DELIVERY_ADDRESSES_BATCH_RECENT_API_URL @"/api/v2/delivery_addresses/recent_address_batch_generate"
#define DELIVERY_ADDRESSES_BATCH_RECENT_PARAM @\
"name=%@\
&phone_number=%@\
&recent_addresses=%@\
&auth_token=%@"

#define MERCHANTS_LIST_BASE_PARAM @\
"auth_token=%@"

#define MERCHANTS_KEYWORD_PARAM @\
"&search=%@"

#define MERCHANTS_LOCATION_PARAM @\
"&latitude=%lf\
&longitude=%lf"

#define MERCHANTS_NEW_PARAM @\
"merchant[name]=%@\
&merchant[ownername]=%@\
&merchant[address]=%@\
&merchant[address_country]=%@\
&merchant[address_state]=%@\
&merchant[address_city]=%@\
&merchant[address_street_1]=%@\
&merchant[address_street_2]=%@\
&merchant[address_zipcode]=%@\
&merchant[latitude]=%lf\
&merchant[longitude]=%lf\
&merchant[geo_address]=%@\
&merchant[mode]=%@\
&merchant[phone]=%@\
&merchant[numoftable]=%d\
&merchant[accept_pay_only]=%@\
&merchant[accept_menu_pay]=%@\
&merchant[accept_to_go]=%@\
&merchant[accept_takeout]=%@\
&merchant[accept_delivery]=%@\
&merchant[served_by]=%@\
&auth_token=%@"

#define MERCHANTS_FILTER_API_URL @"/api/v2/merchants/list"
/*
latitude=%lf\
&longitude=%lf\
&order_type=%@\
&category_ids=%@\
&price=%@\
&free_delivery=%@\
&open_now=%@\
&deals_only=%@\
&free_service_fee=%@\
&favorites=%@\
&page=%d\
&auth_token=%@"
 */

#define MERCHANT_DEMO_PARAM @\
"merchant[mode]=%@\
&auth_token=%@"

#define MERCHANT_OPEN_PARAM @\
"merchant[open]=%@\
&auth_token=%@"

#define MERCHANT_PUBLISH_PARAM @\
"merchant[published]=%@\
&auth_token=%@"

#define MERCHANT_TAX_PARAM @\
"merchant[tax_rate]=%f\
&merchant[delivery_fee_flat]=%d\
&merchant[delivery_min_order_amount]=%d\
&merchant[delivery_distance]=%f\
&merchant[tip_enable]=%@\
&auth_token=%@"

#define MERCHANT_ACCOUNT_PARAM @\
"merchant[bank_account_owner_name]=%@\
&merchant[bank_account_number]=%@\
&merchant[bank_routing_number]=%@\
&merchant[bank_name]=%@\
&merchant[ein]=%@\
&merchant[ssn]=%@\
&auth_token=%@"

#define MERCHANT_LOCATION_PARAM @\
"merchant[latitude]=%lf\
&merchant[longitude]=%lf\
&merchant[geo_address]=%@\
&auth_token=%@"

#define MERCHANT_ADITIONAL_PARAM @\
"merchant[description]=%@\
&merchant[takeout_remark]=%@\
&merchant[homepage]=%@\
&auth_token=%@"

#pragma mark BILL
#define BILL_REQUESTS_API_URL @"/api/v2/bill_requests.json"
#define BILL_REQUESTS_PARAM @\
"bill_request[customer_device_token]=%@\
&bill_request[table_label]=%@\
&bill_request[merchant_id]=%lld\
&auth_token=%@"

#define MY_BILL_REQUESTS_PARAM @\
"customer_device_token=%@"


#pragma mark ORDER (& TABLES)
#define TABLE_STATUS_API_URL @"/api/v2/merchants/%lld/table_status.json"

#define ORDER_API_URL @"/api/v2/orders.json"

#define A_ORDER_API_URL @"/api/v2/orders/%lld.json"

#define BASE_ORDER_PARAM @\
"order[sub_total_amount]=%ld\
&order[sub_total_amount_taxable]=%ld\
&order[tax_amount]=%ld\
&order[status]=%@"

#define NEW_ORDER_PARAM BASE_ORDER_PARAM @\
"&order[merchant_id]=%lld\
&order[table_label]=%@\
&auth_token=%@"

#define UPDATE_ORDER_PARAM BASE_ORDER_PARAM @\
"&order[complete_reason]=%@\
&order[expected_at]=%@\
&order[expected_preparing_at]=%@\
&auth_token=%@"

#define ORDER_BY_DATE_PARAM @\
"start_date=%@\
&end_date=%@\
&merchant_id=%lld\
&auth_token=%@"

#define ORDER_STATUS_API_URL @"/api/v2/merchants/%lld/order_status/"

#define TABLE_API_URL @"/api/v2/merchants/%lld/tables.json"

#define SET_TABLE_LABEL_API_URL @"/api/v2/merchants/%lld/set_table_labels.json"

#define TABLE_LABEL_SET_PARAM @\
"auth_token=%@\
&list=%@"

#define MERCHANT_TABLE_ORDER_API_URL @"/api/v2/merchants/%lld/table_order.json"
#define TABLE_CHECK_PARAM @\
"table_label=%@"

#pragma mark PAYMENT

#define PAYMENT_API_URL @"/api/v2/payments/create_v2"

#define NEW_PAYMENT_PARAM @\
"payment[actual_amount]=%ld\
&payment[credit_amount]=%ld\
&payment[tip_amount]=%ld\
&payment[tax_amount]=%ld\
&payment[delivery_fee_amount]=%ld\
&payment[discount_amount_by_ro]=%ld\
&payment[discount_amount_by_merchant]=%ld\
&payment[ro_service_fee_amount]=%ld\
&payment[currency]=%@\
&payment[order_id]=%lld\
&payment[merchant_id]=%lld\
&pg_type=%d\
&auth_token=%@\
&payment[payment_method]=payself\
&discount_lists=%@\
&customer_device_token=%@"

#define PAYMENT_POS_API_URL @"/api/v2/payments/pos.json"

#define POS_PAYMENT_PARAM @\
"payment[actual_amount]=%d\
&payment[credit_amount]=%d\
&payment[currency]=%@\
&payment[tip_amount]=%d\
&payment[merchant_id]=%lld\
&payment[order_id]=%lld\
&auth_token=%@\
&payment[payment_method]=%@"

#define MOBILE_CARD_PARAM @\
"&card[number]=%@\
&card[exp_month]=%02ld\
&card[exp_year]=%02ld\
&card[cvc]=%@\
&card[zipcode]=%@\
&card[payment_method_nonce]=%@"

#define CARD_FINGERPRINT_PARAM @\
"&card[fingerprint]=%@"

#define PAYMENT_WITH_NEW_CARD NEW_PAYMENT_PARAM MOBILE_CARD_PARAM

#define PAYMENT_WITH_EXISTING_CARD NEW_PAYMENT_PARAM CARD_FINGERPRINT_PARAM

#define PAYMENT_WITH_LINEITEMS_PARAM @\
"&lineitem_ids=%@"

#define CARDS_API_URL @"/api/v2/cards.json"

#define CARDS_CREATE_API_URL @"/api/v2/cards/create_v2"

#define REMOVE_CARDS_API_URL @"/api/v2/cards/%lld"

#define RECEIPT_API_URL @"/api/v2/receipts"

#define REMOVE_RECEIPT_API_URL @"/api/v2/receipts/%lld"

#define REMOVE_ORDER_API_URL @"/api/v2/orders/customer_remove_order"

#define REMOVE_ORDER_PARAM @\
"auth_token=%@\
&order_id=%lld"

#define GET_RECEIPT_API_URL @"/api/v2/receipts/get_receipt"

#define GET_RECEIPT_PARAM @\
"payment_id=%lld\
&auth_token=%@"

#define ADD_RECEIPTS_API_URL @"/api/v2/customers/add_receipts"

#define ADD_RECEIPTS_PARAM @\
"receipt_ids=%@\
&auth_token=%@"

#define REWARD_API_URL @"/api/v2/customers/reward"
#define REWARD_GIVE_PARAM @\
"payment_id=%lld\
&auth_token=%@"

#define VALIDATE_PROMOTION_API_URL @"/api/v2/customers/check_promotion"
#define VALIDATE_PROMOTION_PARAM @\
"promotion_id=%lld\
&merchant_id=%lld\
&auth_token=%@"

#define VALIDATE_PROMO_CODE_API_URL @"/api/v2/customers/check_promo_code"
#define VALIDATE_PROMO_CODE_PARAM @\
"promotion_code=%@\
&auth_token=%@"

#define REQUEST_SERVICE_AREA_API_URL @"/api/v2/customers/request_service_area"
#define REQUEST_SERVICE_AREA_PARAM @\
"area_name=%@\
&first_name=%@\
&last_name=%@\
&email=%@\
&push_device_token=%@"


#define NOTICE_API_URL @"/api/v2/notices"

#define STAFF_JOIN_API_URL @"/api/v2/join_requests"
#define STAFF_JOIN_PARAM @\
"auth_token=%@\
&code=%@"

#define STAFF_JOIN_REQUEST_URL @"/api/v2/join_requests/"

#define SELECT_MERCHANT_URL @"/api/v2/merchants/select_merchant"

#define SELECT_MERCHANT_PARAM @\
"merchant_id=%lld\
&auth_token=%@"

////****************************************************************************
#pragma mark BRAIN_TREE
////****************************************************************************

#define BRAINTREE_CLIENT_TOKEN_API_URL @"/api/v2/payments/braintree_client_token"

////****************************************************************************
#pragma mark MENU
////****************************************************************************

#define CART_CONFIRM_API_URL @"/api/v2/orders/cart_confirm"
#define CART_CONFIRM_PARAM @\
"order[cart_id]=%lld\
&order[merchant_id]=%lld\
&auth_token=%@"

#define CART_ADD_CONFIRM_API_URL @"/api/v2/orders/%lld/cart_add"
#define CART_ADD_CONFIRM_PARAM @\
"order[cart_id]=%lld\
&order[merchant_id]=%lld\
&auth_token=%@"

#define CART_API_URL @"/api/v2/carts/%lld"

#define UPDATE_CART_PARAM @\
"cart[status]=%@\
&customer_device_token=%@"

//#define MENU_LIST_API_URL @"/api/v2/menus/"

#define MENU_ACTIVE_API_URL @"/api/v2/menus/set_active"
#define MENU_ACTIVE_PARAM @\
"merchant_id=%lld\
&menu_list_id=%lld\
&auth_token=%@"

#define MENU_LISTS_API_URL @"/api/v2/menus/menu_lists/"
#define MENU_LIST_PARAM @\
"merchant_id=%lld"

#define MENUPAN_LISTS_API_URL @"/api/v2/menus/all_menu_lists/"
#define MENUPAN_LISTS_PARAM @\
"merchant_id=%lld"

#define POPULAR_MENUS_API_URL @"/api/v2/menus/populars/"
#define POPULAR_MENUS_PARAMS @\
"latitude=%lf\
&longitude=%lf\
&page=%ld\
&seed=%ld"


#define CART_AND_ORDER_API_URL @"/api/v2/merchants/%lld/table_cart_order"
#define CART_AND_ORDER_PARAM @\
"table_label=%@"

#define ADD_LINEITEM_API_URL @"/api/v2/carts/add_lineitem"
#define ADD_LINEITEM_PARAM @\
"merchant_id=%lld\
&table_label=%@\
&customer_device_token=%@\
&lineitems=%@\
&auth_token=%@"

#define UPDATE_LINEITEM_API_URL @"/api/v2/carts/%lld/update_lineitem"
#define UPDATE_LINEITEM_PARAM @\
"lineitems=%@"

#define DELETE_LINEITEM_API_URL @"/api/v2/carts/%lld/delete_lineitem"
#define DELETE_LINEITEM_PARAM @\
"lineitems=%@"

#define ADD_ORDER_LINEITEM_API_URL @"/api/v2/orders/%lld/add_lineitem"
#define ADD_ORDER_LINEITEM_PARAM @\
"lineitems=%@\
&customer_device_token=%@\
&auth_token=%@"

#define UPDATE_ORDER_LINEITEM_API_URL @"/api/v2/orders/%lld/update_lineitem"
#define UPDATE_ORDER_LINEITEM_PARAM @\
"lineitems=%@"

#define DELETE_ORDER_LINEITEM_API_URL @"/api/v2/orders/%lld/delete_lineitem"
#define DELETE_ORDER_LINEITEM_PARAM @\
"lineitems=%@\
&with_customer_close=%@\
&auth_token=%@"

#define ADD_LINEITEM_POS_API_URL @"/api/v2/carts/pos_add_lineitem"
#define ADD_LINEITEM_POS_PARAM @\
"table_label=%@\
&auth_token=%@\
&lineitems=%@"

#define UPDATE_LINEITEM_POS_API_URL @"/api/v2/carts/%lld/pos_update_lineitem"
#define UPDATE_LINEITEM_POS_PARAM @\
"lineitems=%@\
&auth_token=%@"

#define DELETE_LINEITEM_POS_API_URL @"/api/v2/carts/%lld/pos_delete_lineitem"
#define DELETE_LINEITEM_POS_PARAM @\
"lineitems=%@\
&auth_token=%@"

#define ADD_ORDERS_API_URL @"/api/v2/customers/add_orders"

#define MY_ORDER_API_URL @"/api/v2/orders/customer_active_v2"

#define MY_ORDERS_API_URL @"/api/v2/orders/customer_orders"
#define MY_ORDERS_PARAM @\
"page=%lu\
&auth_token=%@"

#define MY_ORDERS_PARAM_NO_KEY @\
"page=%lu"

#define MY_ORDER_DETAIL_API_URL @"/api/v2/orders/customer_order_detail"
#define MY_ORDER_DETAIL_PARAM @\
"order_id=%lld\
&auth_token=%@"

#define PICKUP_ORDER_API_URL @"/api/v2/orders/ticket"
#define PICKUP_ORDER_PARAM @\
"lineitems=%@\
&sub_total_amount=%ld\
&sub_total_amount_taxable=%ld\
&tax_amount=%ld\
&merchant_id=%lld\
&table_label=%@\
&customer_device_token=%@\
&delivery_customer_name=%@\
&customer_request=%@\
&auth_token=%@"

#define TAKEOUT_ORDER_API_URL @"/api/v2/orders/takeout"
#define TAKEOUT_ORDER_PARAM @\
"lineitems=%@\
&sub_total_amount=%ld\
&sub_total_amount_taxable=%ld\
&tax_amount=%ld\
&merchant_id=%lld\
&pickup_after=%ld\
&customer_request=%@\
&delivery_customer_name=%@\
&delivery_phone_number=%@\
&customer_device_token=%@\
&auth_token=%@"

#define DELIVERY_ORDER_API_URL @"/api/v2/orders/delivery"
#define DELIVERY_ORDER_PARAM @\
"&delivery_address_city=%@\
&delivery_address_state=%@\
&delivery_address_street_1=%@\
&delivery_address_street_2=%@\
&delivery_address_zipcode=%@\
&delivery_fee_amount=%ld\
&ro_service_fee_amount=%ld"

#define RENEW_ORDER_API_URL @"/api/v2/orders/%lld/renew"
#define RENEW_ORDER_PARAM @\
"pickup_after=%ld\
&customer_request=%@\
&table_label=%@\
&delivery_customer_name=%@\
&delivery_phone_number=%@\
&auth_token=%@"

#define REJECT_ORDER_API_URL @"/api/v2/orders/%lld/reject"
#define REJECT_ORDER_PARAM @\
"reject_reason=%@\
&need_to_adjustment=%@\
&auth_token=%@"

#define REFUND_PAYMENT_API_URL @"/api/v2/payments/%lld/refund"
#define REFUND_PAYMENT_PARAM @\
"auth_token=%@"

#define CUSTOMER_CLOSE_ORDER_API_URL @"/api/v2/orders/%lld/customer_close"
#define CUSTOMER_CLOSE_ORDER_PARAM @\
""

#define NEAR_ME_ORDER_API_URL @"/api/v2/orders/near_me"
#define NEAR_ME_ORDER_PARAM @\
"latitude=%lf\
&longitude=%lf\
&page=%ld\
&seed=%ld"


#define MAIL_ME_API_URL @"/api/v2/receipts/%lld/email"
#define MAIL_ME_PARAM @\
"email=%@\
&auth_token=%@"

#define MAIL_ME_OLD_API_URL @"/api/v2/payments/%lld/email"
#define MAIL_ME_OLD_PARAM @\
"email=%@"

#define APP_START_API_URL @"/api/v2/start"

#define UPDATE_DEVICE_TOKEN_API_URL @"/api/v2/owners/update_device_token"
#define UPDATE_DEVICE_TOKEN_PARAM @\
"device_token=%@\
&auth_token=%@"

#define UPDATE_CART_SUBMITTED_API_URL @"/api/v2/carts/%lld/cart_submitted"
#define UPDATE_CART_SUBMITTED_PARAM @\
"is_submitted=%@"

#define UPDATE_CART_ORDERING_API_URL @"/api/v2/carts/%lld/cart_ordering"
#define UPDATE_CART_ORDERING_PARAM @\
"auth_token=%@"

#define GET_LAST_NOTIFICATION_TIME @"/api/v2/merchants/last_notification_time"

#define RAPID_ORDER_API_URL @"/api/v2/rorders.json"

#define RAPID_ORDER_CREATE_PARAM @\
"auth_token=%@\
&rorder[order_id]=%lld\
&rorder[title]=%@\
&rorder[delivery_latitude]=%lf\
&rorder[delivery_longitude]=%lf"

#define MODIFY_RAPID_ORDER_API_URL @"/api/v2/rorders/%lld"

#define ORDER_RAPID_ORDER_API_URL @"/api/v2/rorders/order"

#define ORDER_RAPID_ORDER_PARAM @\
"auth_token=%@\
&rorder_id=%lld\
&customer_device_token=%@\
&actual_amount=%ld\
&tip_amount=%ld\
&tax_amount=%ld\
&sub_total_amount_taxable=%ld\
&delivery_fee_amount=%ld\
&discount_amount_by_ro=%ld\
&discount_amount_by_merchant=%ld\
&ro_service_fee_amount=%ld\
&discount_lists=%@"

#define UPDATE_RAPID_ORDER_PARAM @\
"auth_token=%@\
&rorder_id=%f"

#define CREATE_RATING_API_URL @"/api/v2/app_ratings"

#define CREATE_RATING_PARAM @\
"auth_token=%@\
&step=%ld\
&rate=%ld\
&comment=%@"


#endif
