//
//  PCServiceBase.h
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PCPendingRequest.h"

#define MERCHANT_WEB_SIGNIN_URL @"http://"SERVER_URL_ADDRESS"/owners/sign_in"
#define MERCHANT_MENU_LIST_URL @"http://"SERVER_URL_ADDRESS"/dashboard/menu_lists?auth_token=%@"

#ifdef RUSHORDER_CUSTOMER
    #define UNAUTHORIZED_ERROR_TITLE  NSLocalizedString(@"You Have Been Signed Out", nil)
    #define UNAUTHORIZED_ERROR_MESSAGE  NSLocalizedString(@"If you are not signed in on another device, please contact us at\nhelp@rushorderapp.com.", nil)
#else
    #define UNAUTHORIZED_ERROR_TITLE NSLocalizedString(@"You Have Signed Out", nil)
    #define UNAUTHORIZED_ERROR_MESSAGE  NSLocalizedString(@"You need to sign in or sign up before continuing.", nil)
#endif

#define NETWORK_ERROR_MESSAGE       NSLocalizedString(@"Network condition is too bad. Try it later please.", nil)
#define PARAMETER_ERROR_MESSAGE     NSLocalizedString(@"An(Some) essential(s) value is(are) missed.", nil)

typedef enum _requestResult{
    RRSuccess = 0,
    RRConnectionError,
    RRParameterError,
    RRInvalidError,
    RRAuthenticateError,
    RRNotReachable,
    RRProgressing,
    RRFail = 101
} RequestResult;

typedef enum _httpURLRequestMethod{
    RequestMethodGet = 1,
    RequestMethodPost,
    RequestMethodPut,
    RequestMethodDelete,
    RequestMethodMultiPartPost,
    RequestMethodMultiPartPut,
} httpURLRequestMethod;

typedef enum _responseErrorCode{
    ResponseErrorShortPassword          = -5,
    ResponseSuccess                     = 0,
    ResponseErrorSignUp                 = 10,
    ResponseErrorIdIncorrect            = 11,
    ResponseErrorIncorrect              = 12,
    ResponseErrorAuthorize              = 13,
    ResponseErrorNoMerchant             = 14,
    ResponseErrorEmpty                  = 15,
    ResponseErrorInvalid                = 16,
    ResponseErrorExists                 = 17,
    ResponseErrorCardError              = 18,
    ResponseErrorPaymentError           = 19,
    ResponseErrorVerificatoin           = 20,
    ResponseErrorFingerprintError       = 22,
    ResponseErrorNoCreditPolicy         = 25,
    ResponseErrorInvalidParameters      = 26,
    ResponseErrorCartFixed              = 28,
    ResponseErrorNoCart                 = 29,
    ResponseErrorCartUnchangable        = 30,
    ResponseErrorNoLineItem             = 31,
    ResponseErrorCartIsNotSubmitted     = 32,
    ResponseErrorInvalidOrder           = 33,
    ResponseErrorIncompleteOrderExists  = 35,
    ResponseErrorActiveOrderExists      = 36,
    ResponseErrorUnauthorized           = 37,
    ResponseErrorNotSupportOrderType    = 38,
    ResponseErrorTableLabelDuplicated   = 40,
    ResponseErrorStaffJoinAlreadyRequested      = 41,
    ResponseErrorStaffJoinAlreadyJoined         = 43,
    ResponseErrorPasswordIncorrect      = 59,
    ResponseErrorItemsAlreadyPaid       = 62,
    ResponseErrorNoReceipt              = 65,
    ResponseErrorPromoCodeIncorrect     = 67,
    ResponseErrorPromoInExists          = 68,
    ResponseErrorPromoUsedAlready       = 69,
    ResponseErrorUnavailableItems       = 78,
    
    ResponseErrorMenuItemChanged        = 81,
    
    ResponseErrorReferreringNotFirst        = 87,
    ResponseErrorAlreadyReferring           = 88,
    ResponseErrorReferreringNotFirstUUID    = 89,
    ResponseErrorAlreadyReferringUUID       = 90,
    
    ResponseErrorOutOfMenuHours         = 91,
    
    ResponseErrorBraintreePaymentError  = 101,
    
    ResponseErrorPromotionNotAssigned   = 102,
    ResponseErrorPromotionFirstUse      = 103,
    
    ResponseErrorGeneral                = 777,
    ResponseErrorUnknownServer          = 998,
    ResponseErrorSaveFail               = 999,
    
    ResponseErrorConnectionCanceled     = NSNotFound - 2,
    ResponseErrorNetworkError           = NSNotFound - 1,
    ResponseErrorUnknown                = NSNotFound,
} ResponseErrorCode;


extern NSString * serviceUUID;
extern NSString * clientAppVersion;

@interface PCServiceBase : NSObject

@property (strong, nonatomic) NSMutableArray *requestQueue;
@property (strong, nonatomic) NSMutableDictionary *requestedDict;
@property (copy, nonatomic) NSString *serverAPIURL;

#pragma mark - Methods for requests
- (RequestResult)requestWithAPI:(NSString *)api
                      parameter:(NSString *)parameter
            withCompletionBlock:(CompletionBlock)completionBlock
            withDataHandleBlock:(DataHandleBlock)dataHandleBlock;

- (RequestResult)requestPostWithAPI:(NSString *)api
                          parameter:(NSString *)parameter
                withCompletionBlock:(CompletionBlock)completionBlock
                withDataHandleBlock:(DataHandleBlock)dataHandleBlock;

- (RequestResult)requestPutWithAPI:(NSString *)api
                         parameter:(NSString *)parameter
               withCompletionBlock:(CompletionBlock)completionBlock
               withDataHandleBlock:(DataHandleBlock)dataHandleBlock;

- (RequestResult)requestDeleteWithAPI:(NSString *)api
                            parameter:(NSString *)parameter
                  withCompletionBlock:(CompletionBlock)completionBlock
                  withDataHandleBlock:(DataHandleBlock)dataHandleBlock;

- (RequestResult)requestWithAPI:(NSString *)api
                      parameter:(NSString *)parameter
                         method:(httpURLRequestMethod)method
            withCompletionBlock:(CompletionBlock)completionBlock
            withDataHandleBlock:(DataHandleBlock)dataHandleBlock;

#pragma mark - Methods for image uploads
- (RequestResult)requestWithAPI:(NSString *)api
                          image:(UIImage *)image
                         method:(httpURLRequestMethod)method
                      parameter:(NSString *)parameter
            withCompletionBlock:(CompletionBlock)completionBlock
            withDataHandleBlock:(DataHandleBlock)dataHandleBlock;

- (RequestResult)requestWithAPI:(NSString *)api
                          image:(UIImage *)image
                         method:(httpURLRequestMethod)method
                      paramDict:(NSDictionary *)paramDict
            withCompletionBlock:(CompletionBlock)completionBlock
            withDataHandleBlock:(DataHandleBlock)dataHandleBlock;

- (RequestResult)requestWithAPI:(NSString *)api
                      imageData:(NSData *)imageData
                         method:(httpURLRequestMethod)method
                      paramDict:(NSDictionary *)paramDict
            withCompletionBlock:(CompletionBlock)completionBlock
            withDataHandleBlock:(DataHandleBlock)dataHandleBlock;

#pragma mark - Methods for movie uploads
- (RequestResult)requestWithAPI:(NSString *)api
                      moviePath:(NSString *)moviePath
                         method:(httpURLRequestMethod)method
                      paramDict:(NSDictionary *)paramDict
            withCompletionBlock:(CompletionBlock)completionBlock
            withDataHandleBlock:(DataHandleBlock)dataHandleBlock;

- (RequestResult)requestWithAPI:(NSString *)api
                      movieData:(NSData *)movieData
                         method:(httpURLRequestMethod)method
                      paramDict:(NSDictionary *)paramDict
            withCompletionBlock:(CompletionBlock)completionBlock
            withDataHandleBlock:(DataHandleBlock)dataHandleBlock;

- (RequestResult)requestWithFullAPI:(NSString *)apURLString
                           bodyData:(NSData *)bodyData
                             method:(httpURLRequestMethod)method
                withCompletionBlock:(CompletionBlock)completionBlock
                withDataHandleBlock:(DataHandleBlock)dataHandleBlock;

#pragma mark -
- (RequestResult)request:(NSURLRequest *)request
         completionBlock:(CompletionBlock)completionBlock
         dataHandleBlock:(DataHandleBlock)dataHandleBlock;

- (void)connectionDidFinishLoading:(NSURLConnection *)connection;
@end
