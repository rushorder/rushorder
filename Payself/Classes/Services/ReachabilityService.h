//
//  ReachabilityService.h
//  RushOrder
//
//  Created by Conan on 12/3/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

#define REACH [ReachabilityService sharedService]

extern NSString * const ReachabilityStatusChangedNotification;

@interface ReachabilityService : NSObject

@property (strong, nonatomic) Reachability *reachability;
@property (nonatomic) NetworkStatus status;
@property (nonatomic) NetworkStatus lastStatus;

+ (ReachabilityService *)sharedService;
- (void)refreshReachability;

@end
