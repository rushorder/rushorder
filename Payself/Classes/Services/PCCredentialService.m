//
//  PCCredentialService.m
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCCredentialService.h"
#import "Merchant.h"
#import "PCPaymentService.h"
#import "RemoteDataManager.h"

//#define SIMULATE_PHONE_VERIFICATION

NSString * const SignedInNotification = @"SignedInNotification";
NSString * const SignedOutNotification = @"SignedOutNotification";

@implementation PCCredentialService

+ (PCCredentialService *)sharedService
{
    static PCCredentialService *aService = nil;
    
    @synchronized(self){
        if(aService == nil){
            aService = [[self alloc] init];
        }
    }
    
    return aService;
}


- (id)init
{
    self = [super init];
    if(self != nil){
        // Initializer
        self.userAccountItem = [[KeychainItemWrapper alloc] initWithIdentifier:ATTR_ACCOUNT
                                                                   accessGroup:nil];
        
        self.appPINItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"PayselfApplicationPIN"
                                                              accessGroup:nil];
        
        self.payselfUUID = [[KeychainItemWrapper alloc] initWithIdentifier:@"PayselfApplicationUUIDs"
                                                               accessGroup:@"773V8DE3EH.com.payselfmobile"];
    }
    return self;
}

- (RequestResult)requestSignUp:(NSString *)email
                      password:(NSString *)password
                     firstName:(NSString *)firstName
                      lastName:(NSString *)lastName
                   deviceToken:(NSString *)deviceToken
               completionBlock:(CompletionBlock)completionBlock

{
    return [self requestSignUp:email
                      password:password
                     firstName:firstName
                      lastName:lastName
                   phoneNumber:nil
                   deviceToken:deviceToken
               completionBlock:completionBlock];
}

- (RequestResult)requestSignUp:(NSString *)email
                      password:(NSString *)password
                     firstName:(NSString *)firstName
                      lastName:(NSString *)lastName
                   phoneNumber:(NSString *)phoneNumber
                   deviceToken:(NSString *)deviceToken
               completionBlock:(CompletionBlock)completionBlock
{
    if([email length] == 0){
        PCError(@"Email should be specified to signing up");
        return RRParameterError;
    }
    
    if([password length] == 0){
        PCError(@"Password should be specified to signing up");
        return RRParameterError;
    }
    
    email = [email lowercaseString];
    
    NSString *baseParameter = [NSString stringWithFormat:CUSTOMER_SIGN_UP_PARAM,
                               [email urlEncodedString],
                               [password urlEncodedString],
                               [firstName urlEncodedString],
                               lastName? [lastName urlEncodedString] : @"",
                               phoneNumber ? [phoneNumber urlEncodedString] : @"",
                               [deviceToken length] > 0 ? [deviceToken urlEncodedString] : @""];
    
    return [super requestPostWithAPI:CUSTOMER_GENERAL_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                    // Success Sgin - up
                    
                    NSString *authToken = [response objectForKey:@"auth_token"];
                    if([authToken length] > 0){
                        [self.userAccountItem setObject:email
                                                 forKey:(__bridge id)(kSecAttrAccount)];
                        [self.userAccountItem setObject:authToken
                                                 forKey:(__bridge id)(kSecValueData)];
                        [NSUserDefaults standardUserDefaults].autoLogin = YES;
                        [NSUserDefaults standardUserDefaults].emailAddress = email;
                        
                    } else {
                        [self resetUserAccount];
                    }
                    
                } else {
                    // Fail Sign - up
                    [self resetUserAccount];
                }
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestSignIn:(NSString *)email
                      password:(NSString *)password
                   deviceToken:(NSString *)deviceToken
               completionBlock:(CompletionBlock)completionBlock
{
    if([email length] == 0){
        PCError(@"Email should be specified to signing in");
        return RRParameterError;
    }
    
    if([password length] == 0){
        PCError(@"Password should be specified to signing in");
        return RRParameterError;
    }
    
    email = [email lowercaseString];
    
    NSString *baseParameter = [NSString stringWithFormat:CUSTOMER_SIGN_IN_PARAM,
                               [email urlEncodedString],
                               [password urlEncodedString],
                               deviceToken];
    

    return [super requestPostWithAPI:CUSTOMER_SIGN_IN_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                    // Success Sgin - in
                    
                    NSString *authToken = [response objectForKey:@"auth_token"];
                    if([authToken length] > 0){
                        
                        [self.userAccountItem setObject:email
                                                 forKey:(__bridge id)(kSecAttrAccount)];
                        [self.userAccountItem setObject:authToken
                                                 forKey:(__bridge id)(kSecValueData)];
                        
                        [NSUserDefaults standardUserDefaults].autoLogin = YES;
                        
                        if ([NSUserDefaults standardUserDefaults].emailAddress.length == 0){
                            [NSUserDefaults standardUserDefaults].emailAddress = email;
                        }
                        
                    } else {
                        [self resetUserAccount];
                    }
                } else {
                    // Fail Sign - in
                    [self resetUserAccount];
                }
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestSignOutWithCompletionBlock:(CompletionBlock)completionBlock
{
    if(self.customerInfo.authToken == nil){
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               self.customerInfo.authToken];
    
    return [super requestDeleteWithAPI:CUSTOMER_SIGN_OUT_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if((HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES) || statusCode == HTTP_STATUS_UNAUTHORIZED){
                    if(response.errorCode == ResponseSuccess){
                    // Success Sgin - out
                        [self resetUserAccount];
                    }
                } else {
                    
                    
                }
                completionBlock(YES, response, statusCode);
            }];
}


- (RequestResult)requestProfileWithCompletionBlock:(CompletionBlock)completionBlock
{
    if([self.customerInfo.authToken length] == 0){
        PCError(@"Profile request must have authToken");
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               self.customerInfo.authToken];
    
    
    return [super requestWithAPI:CUSTOMER_PROFILE_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                    //Return customer info
                    
                    [self.customerInfo updateWithDictionary:response];
                    
                } else {
                    // Fail
                    
                }
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestUpdateProfile:(CustomerInfo *)newCustomerInfo
             completionBlock:(CompletionBlock)completionBlock
{
    if([self.customerInfo.authToken length] == 0){
        PCError(@"Profile request must have authToken");
        return RRParameterError;
    }
    
    if(newCustomerInfo.photo == nil){
        
        NSString *baseParameter = [NSString stringWithFormat:CUSTOMER_PROFILE_PARAM,
                                   [newCustomerInfo.firstName urlEncodedString],
                                   [newCustomerInfo.lastName urlEncodedString],
                                   [newCustomerInfo.birthday urlEncodedString],
                                   [newCustomerInfo.phoneNumber urlEncodedString],
                                   self.customerInfo.authToken];
        
        return [super requestPutWithAPI:CUSTOMER_GENERAL_API_URL
                               parameter:baseParameter
                     withCompletionBlock:completionBlock
                     withDataHandleBlock:
                ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                    if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                        if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                            // Profile update done
                        } else {
                            // Profile update fail
                        }
                    }
                    completionBlock(YES, response, statusCode);
                }];
    } else {
        
        // !!!: Don't url encoding here!
        NSString *baseParameter = [NSString stringWithFormat:CUSTOMER_PROFILE_PARAM,
                                   newCustomerInfo.firstName,
                                   newCustomerInfo.lastName,
                                   newCustomerInfo.birthday,
                                   newCustomerInfo.phoneNumber,
                                   self.customerInfo.authToken];
        
        return [super requestWithAPI:CUSTOMER_GENERAL_API_URL
                               image:newCustomerInfo.photo
                              method:RequestMethodPut
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
                ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                    if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                        // Profile update done
                        
                    } else {
                        // Profile update faile
                    }
                    completionBlock(YES, response, statusCode);
                }];
    }
}


- (RequestResult)requestChangePassword:(NSString *)currentPassword
                  newPassword:(NSString *)newPassword
              completionBlock:(CompletionBlock)completionBlock
{
    if([currentPassword length] == 0){
        PCError(@"currentPassword should be specified to change password");
        return RRParameterError;
    }
    
    if([newPassword length] == 0){
        PCError(@"newPassword should be specified to change password");
        return RRParameterError;
    }
    
    if([self.customerInfo.authToken length] == 0){
        PCError(@"Profile request must have authToken");
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:CUSTOMER_PASSWORD_PARAM,
                               [currentPassword urlEncodedString],
                               [newPassword urlEncodedString],
                               self.customerInfo.authToken];
    
    
    return [super requestPutWithAPI:CUSTOMER_GENERAL_API_URL
                          parameter:baseParameter
                withCompletionBlock:completionBlock
                withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                    // Success Change Password
                    
                } else {
                    // Fail to Change Password
                    
                }
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestForgotPassword:(NSString *)email
              completionBlock:(CompletionBlock)completionBlock
{
    if([email length] == 0){
        PCError(@"email should be specified to Find password");
        return RRParameterError;
    }
    
    
    NSString *baseParameter = [NSString stringWithFormat:CUSTOMER_FORGOT_PASSWORD_PARAM,
                               [email urlEncodedString]];
    
    
    return [super requestPostWithAPI:CUSTOMER_FORGOT_PASSWORD_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                    // Success email
                    
                } else {
                    // Fail to email
                    
                }
                completionBlock(YES, response, statusCode);
            }];
}

- (BOOL)generateUserAccount
{
    if(![NSUserDefaults standardUserDefaults].emergencySignedOut){
        [self resetUserAccount];
        [NSUserDefaults standardUserDefaults].emergencySignedOut = YES;
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    // email
    NSString *email = [self.userAccountItem objectForKey:(__bridge id)(kSecAttrAccount)];
    // authtoken
    NSString *authToken = [self.userAccountItem objectForKey:(__bridge id)(kSecValueData)];
    
    if([email length] > 0 && [authToken length] > 0){
        
        NSArray *customers = [CustomerInfo listAll];
        CustomerInfo *customerInfo = nil;
        if([customers count] == 1){
            customerInfo = [customers objectAtIndex:0];
        } else{
            PCWarning(@"Customers is not just only 1 - sign in");
            // Clear
            for(CustomerInfo *customer in customers){
                if([customer delete]){
                    // Success
                } else {
                    PCError(@"Error to remove customerInfo - sign in");
                }
            }
        }
        
        if(customerInfo != nil){
            if([customerInfo.email isEqualToString:email]){
                customerInfo.authToken = authToken;
                self.customerInfo = customerInfo;
                
                [REMOTE favoriteRestaurants];
                
            } else {
                PCError(@"KeyChain Email is different with the one in db");
            }
            
            return YES;
        } else {
            return NO;
        }
    } else {
        return NO;
    }
}

- (void)signOut
{
    [self resetUserAccount];
}

- (void)resetUserAccount
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    [self.userAccountItem resetKeychainItem];
    self.customerInfo = nil;
    userDefault.autoLogin = NO;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SignedOutNotification
                                                        object:self
                                                      userInfo:nil];
}


- (RequestResult)requestVerificationCode:(NSString *)phoneNumber
                         completionBlock:(CompletionBlock)completionBlock
{
    if([phoneNumber length] == 0){
        PCError(@"Phone number should be specified");
        return RRAuthenticateError;
    }
    
    NSString *purifiedPhoneNumber = [[phoneNumber componentsSeparatedByCharactersInSet:
                                      [[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"]
                                       invertedSet]]
                                     componentsJoinedByString:@""];
    
    
    NSString *baseParameter = [NSString stringWithFormat:CUSTOMER_REQUEST_VERIFICATION_CODE_PARAM,
                               [purifiedPhoneNumber urlEncodedString],
                               self.customerInfo.authToken];
    
#ifndef SIMULATE_PHONE_VERIFICATION
    return [super requestWithAPI:CUSTOMER_REQUEST_VERIFICATION_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock((HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES), response, statusCode);
            }];
#else
    completionBlock(YES, nil, 200);
#endif
}

// phoneNumber should be formatted with Twillio service, maybe +82XXXXX
- (RequestResult)requestVerification:(NSString *)phoneNumber
                    verificationCode:(NSString *)verificationCode
                         deviceToken:(NSString *)deviceToken
                     completionBlock:(CompletionBlock)completionBlock
{
    if([phoneNumber length] == 0){
        PCError(@"Phone number should be specified");
        return RRAuthenticateError;
    }
    
    if([verificationCode length] == 0){
        PCError(@"Verification code should be specified");
        return RRAuthenticateError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:CUSTOMER_VERIFICATION_NOTSMS_PARAM,
                               [phoneNumber urlEncodedString],
                               [verificationCode urlEncodedString],
                               self.customerInfo.authToken];
    
#ifndef SIMULATE_PHONE_VERIFICATION
    return [super requestWithAPI:CUSTOMER_VERIFICATION_NOTSMS_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                    [self.customerInfo updateWithDictionary:response];
                    [self.customerInfo save];
                } else {
//                    [self resetUserAccount];
                }
                completionBlock(YES, response, statusCode);
            }];
#else
    
  
    completionBlock(YES, nil, 200);
#endif
    
}

- (RequestResult)requestMyCreditWithCompletionBlock:(CompletionBlock)completionBlock
{
    return [self requestMyCreditWithResetBadgeCount:NO
                                    completionBlock:completionBlock];
}

- (RequestResult)requestMyCreditWithResetBadgeCount:(BOOL)resetBadgeCount
                                    completionBlock:(CompletionBlock)completionBlock

{
    if(self.customerInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               self.customerInfo.authToken];
    
    if(resetBadgeCount){
        baseParameter = [baseParameter stringByAppendingString:@"&reset_referral_count=true"];
    }
    
#ifndef SIMULATE_PHONE_VERIFICATION
    return [super requestWithAPI:CREDIT_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                    if(response.errorCode == HTTP_STATUS_UNAUTHORIZED){
                        [CRED resetUserAccount];
                    } else {
                        self.customerInfo.credit = [response integerForKey:@"total_credit"];
                    }
                } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                    [CRED resetUserAccount];
                }
                completionBlock(YES, response, statusCode);
            }];
#else
    self.customerInfo.credit = 99;
    completionBlock(YES, nil, 200);
#endif

}


- (RequestResult)requestMyReferralCodeWithCompletionBlock:(CompletionBlock)completionBlock
{
    if(self.customerInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               self.customerInfo.authToken];
    

    return [super requestWithAPI:REFERRAL_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                    if(response.errorCode == HTTP_STATUS_UNAUTHORIZED){
                        [CRED resetUserAccount];
                    } else {
                        self.customerInfo.referralCode = [[response objectForKey:@"referral_code"] lowercaseString];
                        self.customerInfo.referredCode = [response objectForKey:@"referred_code"];
                        self.customerInfo.isReferredCodeRewarded = [response boolForKey:@"referred_code_rewarded"];
                        self.customerInfo.totalEarnedCredit = [response currencyForKey:@"total_earned_credit"];
                    }
                } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                    [CRED resetUserAccount];
                }
                completionBlock(YES, response, statusCode);
            }];
}


- (RequestResult)requestUpdateDeviceToken:(NSString *)deviceToken
                         completionBlock:(CompletionBlock)completionBlock
{
    if([deviceToken length] == 0){
        PCError(@"Device token should be specified");
        return RRAuthenticateError;
    }

    if([deviceToken length] == 0){
        PCError(@"Device token is not set to update device token - customer");
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:CUSTOMER_UPDATE_DEVICE_TOKEN_PARAM,
                               [deviceToken urlEncodedString],
                               [self.customerInfo.authToken urlEncodedString]];
    
    return [super requestPutWithAPI:CUSTOMERS_API_URL
                          parameter:baseParameter
                withCompletionBlock:completionBlock
                withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock((HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES), response, statusCode);
            }];
}

- (BOOL)isPinSet
{
    return [NSUserDefaults standardUserDefaults].isPinSet;
}

- (RequestResult)requestConfigWithVersion:(NSString *)clientVersion
                          completionBlock:(CompletionBlock)completionBlock
{
    if(clientVersion == nil){
        PCError(@"Client version should be specified");
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:CONFIG_PARAM,
                               clientVersion];
    
    return [super requestWithAPI:CONFIG_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestAppStartWithCompletionBlock:(CompletionBlock)completionBlock
{
    return [super requestWithAPI:APP_START_API_URL
                       parameter:nil
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (BOOL)isSignedIn
{
    return (self.customerInfo != nil);
}
@end
