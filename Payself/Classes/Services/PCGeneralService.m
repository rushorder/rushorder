//
//  PCGeneralService.m
//  RushOrder
//
//  Created by Conan on 3/22/16.
//  Copyright (c) 2016 RushOrder. All rights reserved.
//

#import "PCGeneralService.h"
#import "Merchant.h"
#import "PCCredentialService.h"
#import "TableInfo.h"
#import "RemoteDataManager.h"

@implementation PCGeneralService

+ (PCGeneralService *)sharedService
{
    static PCGeneralService *aService = nil;
    
    @synchronized(self){
        if(aService == nil){
            aService = [[self alloc] init];
        }
    }
    
    return aService;
}

- (id)init
{
    self = [super init];
    if(self != nil){
        // Initializer
    }
    return self;
}

- (RequestResult)requestFaqWithCompletionBlock:(CompletionBlock)completionBlock
{    
    return [super requestWithAPI:FAQ_API_URL
                       parameter:nil
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestCloudPhotosInCriteria:(CloudPhotoCriteria)criteria
                      withCompletionBlock:(CompletionBlock)completionBlock
{
    NSString *params = [NSString stringWithFormat:@"criteria=%ld",criteria];
    
    return [super requestWithAPI:CLOUD_PHOTOS_API_URL
                       parameter:params
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

@end
