//
//  PCPaymentService.h
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCServiceBase.h"
#import <CoreLocation/CoreLocation.h>
#import "Order.h"
#import "Payment.h"
#import "Receipt.h"
#import "FavoriteOrder.h"
#import "DeliveryAddress.h"

#define PAY [PCPaymentService sharedService]

typedef enum fOupdateType_{
    FOupdateTypeNone = 0,
    FOupdateTypeMain,
    FOupdateTypeRequest,
    FOupdateTypePayment,
    FOupdateTypeAddress
} FOupdateType;

@interface PCPaymentService : PCServiceBase

+ (PCPaymentService *)sharedService;

// Get a merchant
- (RequestResult)requestMerchant:(PCSerial)merchantNo
                 completionBlock:(CompletionBlock)completionBlock;

// Search merchants
- (RequestResult)requestMerchants:(NSString *)name
                  currentLocation:(CLLocation *)currentLocation
                             page:(NSUInteger)page
                  completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestMerchantsWithFilter:(NSDictionary *)dictionary
                            currentLocation:(CLLocation *)currentLocation
                                       page:(NSUInteger)page
                            completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestFavoriteMerchantsWithCurrentLocation:(CLLocation *)currentLocation
                                             completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestDeliveryAddressesWithCompletionBlock:(CompletionBlock)completionBlock;
- (RequestResult)requestAddDeliveryAddress:(DeliveryAddress *)card
                           completionBlock:(CompletionBlock)completionBlock;
- (RequestResult)requestUpdateDeliveryAddress:(DeliveryAddress *)deliveryAddress
                              completionBlock:(CompletionBlock)completionBlock;
- (RequestResult)requestDeleteDeliveryAddress:(DeliveryAddress *)deliveryAddress
                              completionBlock:(CompletionBlock)completionBlock;
- (RequestResult)requestUpdateRecentDeliveryAddress:(NSArray *)deliveryAddresses
                                               name:(NSString *)name
                                        phoneNumber:(NSString *)phoneNumber
                                    completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestAddFavorites:(NSArray *)favoriteList
                     currentLocation:(CLLocation *)currentLocation
                     completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestRemoveFavorites:(NSArray *)favoriteList
                        currentLocation:(CLLocation *)currentLocation
                        completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestVisitedMerchantsWithCurrentLocation:(CLLocation *)currentLocation
                                            completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestAddVisited:(NSArray *)visitedMerchantList
                   currentLocation:(CLLocation *)currentLocation
                   completionBlock:(CompletionBlock)completionBlock;

// Check table
- (RequestResult)requestCheckTable:(PCSerial)merchantNo
                       tableNumber:(NSString *)tableNumber
                   completionBlock:(CompletionBlock)completionBlock;

// Request bill
- (RequestResult)requestTheBill:(PCSerial)merchantNo
                    tableNumber:(NSString *)tableNumber
                    deviceToken:(NSString *)deviceToken
                completionBlock:(CompletionBlock)completionBlock;

// New order
- (RequestResult)requestNewOrder:(Order *)order
                 completionBlock:(CompletionBlock)completionBlock;

//
- (RequestResult)requestMyBillRequests:(NSString *)deviceToken
                       completionBlock:(CompletionBlock)completionBlock;

// New payment
- (RequestResult)requestNewPayment:(PCSerial)orderNo
                           payment:(Payment *)payment
                       deviceToken:(NSString *)deviceToken
                  appliedDiscounts:(NSArray *)appliedDiscounts
                    smallCutAmount:(PCCurrency)smallCutAmount
                   completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestNewPayment:(PCSerial)orderNo
                           payment:(Payment *)payment
                  lineItemJsonList:(NSString *)lineItemJsonList
                       deviceToken:(NSString *)deviceToken
                  appliedDiscounts:(NSArray *)appliedDiscounts
                    smallCutAmount:(PCCurrency)smallCutAmount
                paymentMethodNonce:(NSString *)paymentMethodNonce
                   completionBlock:(CompletionBlock)completionBlock;

// Table labels
//- (RequestResult)requestTableLabels:(PCSerial)merchantNo
//                    completionBlock:(CompletionBlock)completionBlock;

// Card registration
- (RequestResult)requestCardRegistration:(MobileCard *)card
                      paymentMethodNonce:(NSString *)paymentMethodNonce
                         completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestRemoveCard:(MobileCard *)card
                   completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestCardListWithCompletionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestReceiptListWithCompletionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestRemoveReceipt:(Receipt *)receipt
                      completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestRemoveOrder:(Order *)order
                    completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestGetReceiptWithPaymentNo:(PCSerial)paymentNo
                                completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestAddReceipts:(NSArray *)receiptsList
                    completionBlock:(CompletionBlock)completionBlock;

// Get reward
- (RequestResult)requestGetRewardInPayment:(Payment *)payment
                           completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestValidatePromotion:(Promotion *)promotion
                                 merchant:(Merchant *)merchant
                          completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestValidatePromoCode:(NSString *)promoCode
                          completionBlock:(CompletionBlock)completionBlock;

// Notice
- (RequestResult)requestNoticeWithCompletionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestCustomerCloseOrder:(PCSerial)orderNo
                           completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestMailMeReceipt:(Receipt *)receipt
                         emailAddress:(NSString *)emailAddress
                      completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestMailMeOldReceipt:(PCSerial)paymentNo
                            emailAddress:(NSString *)emailAddress
                         completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestOrdersAt:(NSUInteger)page
                 completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestCreateRapid:(PCSerial)orderNo
                              title:(NSString *)title
                              image:(UIImage *)image
                           location:(CLLocation *)location
                    completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestUpdateRapidReOrder:(FavoriteOrder *)favoriteOrder
                                      type:(FOupdateType) type
                           completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestDeleteRapidReOrder:(FavoriteOrder *)favoriteOrder
                           completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestGetRapidOrdersWithCompletionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestOrderFavoriteOrder:(PCSerial)favoriteOrderNo
                                   payment:(Payment *)payment
                               deviceToken:(NSString *)deviceToken
                          appliedDiscounts:(NSArray *)appliedDiscounts
                            smallCutAmount:(PCCurrency)smallCutAmount
                           completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestServiceArea:(NSDictionary *)paramDict
                    completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestAppRating:(NSInteger)step
                             rate:(NSInteger)rate
                          comment:(NSString *)comment
                  completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestBrainTreeClientTokenWithCompletionBlock:(CompletionBlock)completionBlock;

@end
