//
//  PCOrderService.h
//  RushOrder
//
//  Created by Conan on 6/7/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCServiceBase.h"
#import "Payment.h"

#define ORDER [PCOrderService sharedService]

@interface PCOrderService : PCServiceBase

+ (PCOrderService *)sharedService;

- (RequestResult)requestConfirmCart:(Cart *)cart
                         merchantId:(PCSerial)merchantId
                    completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestConfirmAddCart:(Cart *)cart
                               toOrder:(Order *)order
                            merchantId:(PCSerial)merchantId
                       completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestNewPayment:(PCSerial)orderNo
                           payment:(Payment *)payment
                         lineItems:(NSArray *)lineItems
                   completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestReject:(PCSerial)orderNo
                        reason:(NSString *)reason
       allowCustomerAdjustment:(BOOL)allowCustomerAdjustment
               completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestRefund:(PCSerial)paymentNo
               completionBlock:(CompletionBlock)completionBlock;

@end
