//
//  PCCredentialService.h
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCServiceBase.h"
#import "CustomerInfo.h"
#import "KeychainItemWrapper.h"

extern NSString * const SignedInNotification;
extern NSString * const SignedOutNotification;

#if RUSHORDER_CUSTOMER
    #define CRED [PCCredentialService sharedService]
#endif

@interface PCCredentialService : PCServiceBase

@property (strong, atomic) CustomerInfo *customerInfo;
@property (readonly) BOOL isPinSet;
@property (nonatomic, strong) KeychainItemWrapper *appPINItem;
@property (nonatomic, strong) KeychainItemWrapper *userAccountItem;
@property (nonatomic, strong) KeychainItemWrapper *payselfUUID;

@property (nonatomic, readonly) BOOL isSignedIn;

@property (copy, nonatomic) NSString *udid;

@property (nonatomic, getter = isUnattendedSignUp) BOOL unattendedSignUp;

@property (nonatomic, getter = isDirtyCredit) BOOL dirtyCredit;

+ (PCCredentialService *)sharedService;

- (RequestResult)requestSignUp:(NSString *)email
                      password:(NSString *)password
                     firstName:(NSString *)firstName
                      lastName:(NSString *)lastName
                   deviceToken:(NSString *)deviceToken
               completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestSignUp:(NSString *)email
                      password:(NSString *)password
                     firstName:(NSString *)firstName
                      lastName:(NSString *)lastName
                   phoneNumber:(NSString *)phoneNumber
                   deviceToken:(NSString *)deviceToken
               completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestSignIn:(NSString *)email
                      password:(NSString *)password
                   deviceToken:(NSString *)deviceToken
               completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestSignOutWithCompletionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestProfileWithCompletionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestUpdateProfile:(CustomerInfo *)newCustomerInfo
             completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestChangePassword:(NSString *)currentPassword
                  newPassword:(NSString *)newPassword
              completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestForgotPassword:(NSString *)email
              completionBlock:(CompletionBlock)completionBlock;

- (BOOL)generateUserAccount;
- (void)signOut;
- (void)resetUserAccount;

- (RequestResult)requestVerificationCode:(NSString *)phoneNumber
                         completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestVerification:(NSString *)phoneNumber
                    verificationCode:(NSString *)verificationCode
                         deviceToken:(NSString *)deviceToken
                     completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestUpdateDeviceToken:(NSString *)deviceToken
                          completionBlock:(CompletionBlock)completionBlock;

// Request my credit
- (RequestResult)requestMyCreditWithCompletionBlock:(CompletionBlock)completionBlock;
- (RequestResult)requestMyCreditWithResetBadgeCount:(BOOL)resetBadgeCount
                                    completionBlock:(CompletionBlock)completionBlock;
- (RequestResult)requestMyReferralCodeWithCompletionBlock:(CompletionBlock)completionBlock;
- (RequestResult)requestConfigWithVersion:(NSString *)clientVersion
                          completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestAppStartWithCompletionBlock:(CompletionBlock)completionBlock;
@end