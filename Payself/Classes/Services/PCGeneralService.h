//
//  PCGeneralService.h
//  RushOrder
//
//  Created by Conan on 3/22/16.
//  Copyright (c) 2016 RushOrder. All rights reserved.
//

#import "PCServiceBase.h"

#define GENERAL [PCGeneralService sharedService]

typedef NS_ENUM(NSInteger, CloudPhotoCriteria) {
    CloudPhotoCriteriaAll,
    CloudPhotoCriteriaRapidOrder,
};

@interface PCGeneralService : PCServiceBase

+ (PCGeneralService *)sharedService;

// Get Faqs
- (RequestResult)requestFaqWithCompletionBlock:(CompletionBlock)completionBlock;
// Get Cloud Photos
- (RequestResult)requestCloudPhotosInCriteria:(CloudPhotoCriteria)criteria
                      withCompletionBlock:(CompletionBlock)completionBlock;
@end
