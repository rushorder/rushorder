//
//  PCOrderService.m
//  RushOrder
//
//  Created by Conan on 6/7/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCOrderService.h"

@implementation PCOrderService

+ (PCOrderService *)sharedService
{
    static PCOrderService *aService = nil;
    
    @synchronized(self){
        if(aService == nil){
            aService = [[self alloc] init];
        }
    }
    
    return aService;
}

- (id)init
{
    self = [super init];
    if(self != nil){
        // Initializer
    }
    return self;
}

- (RequestResult)requestConfirmCart:(Cart *)cart
                         merchantId:(PCSerial)merchantId
                    completionBlock:(CompletionBlock)completionBlock
{
    if(cart == nil){
        return RRParameterError;
    }

    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:CART_CONFIRM_PARAM,
                               cart.cartNo,
                               merchantId,
                               CRED.userInfo.authToken
                               ];
    
    return [super requestPostWithAPI:CART_CONFIRM_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_async(aQueue, ^{
                    dispatch_async(dispatch_get_main_queue(),^{
                        completionBlock(YES, response, statusCode);
                    });
                });
            }];
}

- (RequestResult)requestConfirmAddCart:(Cart *)cart
                               toOrder:(Order *)order
                            merchantId:(PCSerial)merchantId
                       completionBlock:(CompletionBlock)completionBlock
{
    if(cart == nil){
        return RRParameterError;
    }

    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    NSString *apiURL = [NSString stringWithFormat:CART_ADD_CONFIRM_API_URL, order.orderNo];
    
    NSString *baseParameter = [NSString stringWithFormat:CART_ADD_CONFIRM_PARAM,
                               cart.cartNo,
                               merchantId,
                               CRED.userInfo.authToken];
    
    return [super requestPutWithAPI:apiURL
                          parameter:baseParameter
                withCompletionBlock:completionBlock
                withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_async(dispatch_get_main_queue(),^{
                    completionBlock(YES, response, statusCode);
                });
            }];
}

- (RequestResult)requestNewPayment:(PCSerial)orderNo
                           payment:(Payment *)payment
                         lineItems:(NSArray *)lineItems
                   completionBlock:(CompletionBlock)completionBlock
{
    if(payment.merchantNo <= 0){
        PCError(@"MerchantNo shoud be greater than 0");
        return RRParameterError;
    }
    
    if(payment.orderNo <= 0){
        PCError(@"OrderNo shoud be greater than 0");
        return RRParameterError;
    }
    
    NSString *baseParameter = nil;
    
    baseParameter = [NSString stringWithFormat:POS_PAYMENT_PARAM,
                     payment.netAmount,
                     payment.creditAmount,
                     payment.currency,
                     payment.tipAmount,
                     payment.merchantNo,
                     payment.orderNo,
                     CRED.userInfo.authToken ? CRED.userInfo.authToken : @"",
                     payment.methodString];
    
    
    if([lineItems count] > 0){
        NSMutableArray *arrayForJSON = [NSMutableArray array];
        
        for(LineItem * lineItem in lineItems){
            if(!lineItem.isSplitExcept && !lineItem.isPaid){
                [arrayForJSON addObject:[NSNumber numberWithSerial:lineItem.lineItemNo]];
            }
        }
        
        if([arrayForJSON count] > 0){
            baseParameter = [baseParameter stringByAppendingString:[NSString stringWithFormat:PAYMENT_WITH_LINEITEMS_PARAM,
                                                                    [arrayForJSON JSONString]]];
        }
    }
    
    
    return [super requestPostWithAPI:PAYMENT_POS_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_async(dispatch_get_main_queue(),^{
                    completionBlock(YES, response, statusCode);
                });
            }];
}

- (RequestResult)requestReject:(PCSerial)orderNo
                        reason:(NSString *)reason
       allowCustomerAdjustment:(BOOL)allowCustomerAdjustment
               completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    if(orderNo <= 0){
        PCError(@"OrderNo shoud be greater than 0");
        return RRParameterError;
    }
    
    NSString *apiURL = [NSString stringWithFormat:REJECT_ORDER_API_URL,
                        orderNo];
    
    NSString *baseParameter = nil;
    
    baseParameter = [NSString stringWithFormat:REJECT_ORDER_PARAM,
                     reason ? reason.urlEncodedString : @"",
                     allowCustomerAdjustment ? @"true" : @"false",
                     CRED.userInfo.authToken];

    return [super requestWithAPI:apiURL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_async(dispatch_get_main_queue(),^{
                    completionBlock(YES, response, statusCode);
                });
            }];
}

- (RequestResult)requestRefund:(PCSerial)paymentNo
               completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    if(paymentNo <= 0){
        PCError(@"PaymentNo shoud be greater than 0");
        return RRParameterError;
    }
    
    NSString *apiURL = [NSString stringWithFormat:REFUND_PAYMENT_API_URL,
                        paymentNo];
    
    NSString *baseParameter = nil;
    
    baseParameter = [NSString stringWithFormat:REFUND_PAYMENT_PARAM,
                     CRED.userInfo.authToken];
    
    return [super requestWithAPI:apiURL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_async(dispatch_get_main_queue(),^{
                    completionBlock(YES, response, statusCode);
                });
            }];
}

@end
