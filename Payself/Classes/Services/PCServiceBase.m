//
//  PCServiceBase.m
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCServiceBase.h"
#import "JSONKit.h"
#import "ReachabilityService.h"

#define MAX_NUMBER_OF_QUEUE 5
#define REQUEST_TIMEOUT_INTERVAL 50.0f
#define MULTIPART_REQUEST_TIMEOUT_INTERVAL 200.0f
#define PAYMENT_REQUEST_TIMEOUT_INTERVAL 200.0f
#define MULTIPART_BOUNDARY_STRING @"-----------------------------7db32224203ee"
#define IMAGE_COMP_RATIO 0.6f

#if DEBUG
    #define DEBUG_RAW_RESPONSE  1
#endif

NSString * serviceUUID;
NSString * clientAppVersion;

@interface PCServiceBase()
@property (nonatomic, getter = isPendingRequesting) BOOL pendingRequesting;
@end

@implementation PCServiceBase

- (id)init
{
    self = [super init];
    if(self != nil){
        self.requestedDict = [NSMutableDictionary dictionary];
        
        [self testDriveModeChanged:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didEnterBackground:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reachabilityChanged:)
                                                     name:ReachabilityStatusChangedNotification
                                                   object:nil];
    }
    
    return self;
}

- (void)reachabilityChanged:(NSNotification *)aNoti
{
    if(REACH.status != NotReachable){
//        dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//        dispatch_async(aQueue, ^{
            if(!self.pendingRequesting){
                self.pendingRequesting = YES;
                [self requestFirstRequest];
            } else {
                PCLog(@"In requesting pendings...");
            }
//        });
    } else {
        // OOOPS
    }
}

- (void)requestFirstRequest
{
    if([self.requestQueue count] == 0){
        self.pendingRequesting = NO;
        return;
    }
    
    PCPendingRequest *pendingRequest = [self.requestQueue objectAtIndex:0];
    if([self connectWithRequest:pendingRequest] == RRSuccess){
        PCLog(@"Pending request called %@, current Request Queue%@", pendingRequest.request.URL, self.requestQueue);
        [self.requestQueue removeObjectAtIndex:0];
    }
    [self requestFirstRequest];
}

- (void)didEnterBackground:(NSNotification *)aNoti
{
    for(id key in [self.requestedDict allKeys]){
        PCPendingRequest *request = [self.requestedDict objectForKey:key];
        [request.connection cancel];
        NSError *error = [NSError errorWithDomain:@"NSErrorDomain"
                                             code:NSURLErrorCancelled
                                         userInfo:nil];
        
        [self connection:request.connection didFailWithError:error];
        [self.requestedDict removeObjectForKey:key];
    }
}

- (void)testDriveModeChanged:(NSNotification *)aNoti
{
#ifdef SSL_ENABLED
    NSString *urlProtocol = @"https://";
#else
    NSString *urlProtocol = @"http://";
#endif
    
    self.serverAPIURL = [urlProtocol stringByAppendingString:SERVER_URL_ADDRESS];
}

// Default method to RequestMethodPost
- (RequestResult)requestWithAPI:(NSString *)api
                      parameter:(NSString *)parameter
            withCompletionBlock:(CompletionBlock)completionBlock
            withDataHandleBlock:(DataHandleBlock)dataHandleBlock
{
    return [self requestWithAPI:api
                      parameter:parameter
                         method:RequestMethodGet
            withCompletionBlock:completionBlock
            withDataHandleBlock:dataHandleBlock];
}

- (RequestResult)requestPostWithAPI:(NSString *)api
                          parameter:(NSString *)parameter
                withCompletionBlock:(CompletionBlock)completionBlock
                withDataHandleBlock:(DataHandleBlock)dataHandleBlock
{
    return [self requestWithAPI:api
                      parameter:parameter
                         method:RequestMethodPost
            withCompletionBlock:completionBlock
            withDataHandleBlock:dataHandleBlock];
}

- (RequestResult)requestPutWithAPI:(NSString *)api
                          parameter:(NSString *)parameter
                withCompletionBlock:(CompletionBlock)completionBlock
                withDataHandleBlock:(DataHandleBlock)dataHandleBlock
{
    return [self requestWithAPI:api
                      parameter:parameter
                         method:RequestMethodPut
            withCompletionBlock:completionBlock
            withDataHandleBlock:dataHandleBlock];
}


- (RequestResult)requestDeleteWithAPI:(NSString *)api
                          parameter:(NSString *)parameter
                withCompletionBlock:(CompletionBlock)completionBlock
                withDataHandleBlock:(DataHandleBlock)dataHandleBlock
{
    return [self requestWithAPI:api
                      parameter:parameter
                         method:RequestMethodDelete
            withCompletionBlock:completionBlock
            withDataHandleBlock:dataHandleBlock];
}

- (RequestResult)requestWithAPI:(NSString *)api
                      parameter:(NSString *)parameter
                         method:(httpURLRequestMethod)method
            withCompletionBlock:(CompletionBlock)completionBlock
            withDataHandleBlock:(DataHandleBlock)dataHandleBlock
{
    PCLog(@"\n[REQUEST] %@%@\n[PARAMS] %@", self.serverAPIURL, api, (parameter == nil) ? @"No extra parameters" : parameter);
    
    return [self requestWithAPI:api
                       bodyData:[parameter dataUsingEncoding:NSUTF8StringEncoding]
                         method:method
            withCompletionBlock:completionBlock
            withDataHandleBlock:dataHandleBlock];
}

#pragma mark - Methods for image uploads
- (RequestResult)requestWithAPI:(NSString *)api
                          image:(UIImage *)image
                         method:(httpURLRequestMethod)method
                      parameter:(NSString *)parameter
            withCompletionBlock:(CompletionBlock)completionBlock
            withDataHandleBlock:(DataHandleBlock)dataHandleBlock
{
    
    NSMutableDictionary *paramDict = nil;
    
    if([parameter length] > 0){
        paramDict = [NSMutableDictionary dictionary];
        
        NSArray *hashedParam = [parameter componentsSeparatedByString:@"&"];
        
        NSString *key = nil;
        NSString *value = nil;
        for(NSString *token in hashedParam){
            NSArray *keyValue = [token componentsSeparatedByString:@"="];
            if([keyValue count] > 0){
                key = [keyValue objectAtIndex:0];
                value = [keyValue objectAtIndex:1];
                
                if(key != nil && value != nil){
                    [paramDict setObject:value forKey:key];
                }
            }
        }
    }
    
    return [self requestWithAPI:api
                          image:image
                         method:method
                      paramDict:paramDict
            withCompletionBlock:completionBlock
            withDataHandleBlock:dataHandleBlock];
}

- (RequestResult)requestWithAPI:(NSString *)api
                          image:(UIImage *)image
                         method:(httpURLRequestMethod)method
                      paramDict:(NSDictionary *)paramDict
            withCompletionBlock:(CompletionBlock)completionBlock
            withDataHandleBlock:(DataHandleBlock)dataHandleBlock
{
    return [self requestWithAPI:api
                      imageData:UIImageJPEGRepresentation(image, IMAGE_COMP_RATIO)
                         method:method
                      paramDict:(NSDictionary *)paramDict
            withCompletionBlock:completionBlock
            withDataHandleBlock:dataHandleBlock];
}

- (RequestResult)requestWithAPI:(NSString *)api
                      imageData:(NSData *)imageData
                         method:(httpURLRequestMethod)method
                      paramDict:(NSDictionary *)paramDict
            withCompletionBlock:(CompletionBlock)completionBlock
            withDataHandleBlock:(DataHandleBlock)dataHandleBlock
{
#define MUTIPART_IMAGE_FORMAT @"\
\r\n--%@\r\n\
Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n\
Content-Type: application/octet-stream\r\n\r\n"
    
#define MUTIPART_IMAGE_FORMAT_CLOSE @"\
\r\n--%@--\r\n"
    
    PCLog(@"\n[UPLOAD] %@\n[IMGDATA]:%d\n%@", api, [imageData length], paramDict);
    
    static NSString *boundary = MULTIPART_BOUNDARY_STRING;
    
    NSString *key = @"customer[photo]";
    NSString *filename = @"profilePhoto.jpg";
    
    if([api hasPrefix:@"/api/v2/rorders"]){
        key = @"rorder[image]";
        filename = @"rorderImage.jpg";
    }
    
    NSMutableData *bodyData = [NSMutableData data];
    [bodyData appendData:[paramDict HTTPFormatDataWithBoundary:boundary]];
    [bodyData appendData:[[NSString stringWithFormat:MUTIPART_IMAGE_FORMAT,
                           boundary, key, filename] dataUsingEncoding:NSUTF8StringEncoding]];
    [bodyData appendData:imageData];
    [bodyData appendData:[[NSString stringWithFormat:MUTIPART_IMAGE_FORMAT_CLOSE,
                           boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return [self requestWithAPI:api
                       bodyData:bodyData
                         method:method == RequestMethodPost ? RequestMethodMultiPartPost : RequestMethodMultiPartPut
            withCompletionBlock:completionBlock
            withDataHandleBlock:dataHandleBlock];
}

#pragma mark - Methods for movie uploads
- (RequestResult)requestWithAPI:(NSString *)api
                      moviePath:(NSString *)moviePath
                         method:(httpURLRequestMethod)method
                      paramDict:(NSDictionary *)paramDict
            withCompletionBlock:(CompletionBlock)completionBlock
            withDataHandleBlock:(DataHandleBlock)dataHandleBlock
{
    PCLog(@"\n[UPLOAD] %@\n[MOVPATH]:%@", api, moviePath);
    
    return [self requestWithAPI:api
                      movieData:[NSData dataWithContentsOfFile:moviePath]
                         method:method
                      paramDict:(NSDictionary *)paramDict
            withCompletionBlock:completionBlock
            withDataHandleBlock:dataHandleBlock];
}

- (RequestResult)requestWithAPI:(NSString *)api
                      movieData:(NSData *)movieData
                         method:(httpURLRequestMethod)method
                      paramDict:(NSDictionary *)paramDict
            withCompletionBlock:(CompletionBlock)completionBlock
            withDataHandleBlock:(DataHandleBlock)dataHandleBlock
{
    
    //k=Ger2vN%2FffasBAGryp9T%2Bw2xbs8C5Y0o3EoUM4QTcr80%3D&c=기본분류
#define MUTIPART_MOVIE_FORMAT @"\
\r\n--%@\r\n\
Content-Disposition: form-data; name=\"file_name\"; filename=\"movie.mov\"\r\n\
Content-Type: multipart/mixed\r\n\r\n"
    
#define MUTIPART_MOVIE_FORMAT_CLOSE @"\
\r\n--%@--\r\n"
    
    static NSString *boundary = MULTIPART_BOUNDARY_STRING;
    
    NSMutableData *bodyData = [NSMutableData data];
    [bodyData appendData:[paramDict HTTPFormatDataWithBoundary:boundary]];
    [bodyData appendData:[[NSString stringWithFormat:MUTIPART_MOVIE_FORMAT,
                           boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [bodyData appendData:movieData];
    [bodyData appendData:[[NSString stringWithFormat:MUTIPART_MOVIE_FORMAT_CLOSE,
                           boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return [self requestWithFullAPI:api
                           bodyData:bodyData
                             method:method == RequestMethodPost ? RequestMethodMultiPartPost : RequestMethodMultiPartPut
                withCompletionBlock:completionBlock
                withDataHandleBlock:dataHandleBlock];
}

#pragma mark - Base Methods for request
- (RequestResult)requestWithAPI:(NSString *)api
                       bodyData:(NSData *)bodyData
                         method:(httpURLRequestMethod)method
            withCompletionBlock:(CompletionBlock)completionBlock
            withDataHandleBlock:(DataHandleBlock)dataHandleBlock
{

    NSString *apURLString = [self.serverAPIURL stringByAppendingPathComponent:api];
    return [self requestWithFullAPI:apURLString
                           bodyData:bodyData
                             method:method
                withCompletionBlock:completionBlock
                withDataHandleBlock:dataHandleBlock];
}

- (RequestResult)requestWithFullAPI:(NSString *)apURLString
                           bodyData:(NSData *)bodyData
                             method:(httpURLRequestMethod)method
                withCompletionBlock:(CompletionBlock)completionBlock
                withDataHandleBlock:(DataHandleBlock)dataHandleBlock
{
    if((method != RequestMethodPost) &&
       ([bodyData length] > 0) &&
       (method != RequestMethodMultiPartPut) &&
       (method != RequestMethodMultiPartPost)){
        NSString *paramString = [[NSString alloc] initWithData:bodyData
                                                      encoding:NSUTF8StringEncoding];
        apURLString = [[apURLString stringByAppendingString:@"?"]
                       stringByAppendingString:paramString];
    }
    
    NSURL *apiURL = [NSURL URLWithString:apURLString];
    
    NSTimeInterval timeout = ((method == RequestMethodMultiPartPost) || (method == RequestMethodMultiPartPut)
                              ? MULTIPART_REQUEST_TIMEOUT_INTERVAL
                              : REQUEST_TIMEOUT_INTERVAL);
    
    if([apURLString isEqualToString:PAYMENT_API_URL]){
        timeout = PAYMENT_REQUEST_TIMEOUT_INTERVAL;
    }
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:apiURL
                                                                cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                            timeoutInterval:timeout];
    [request addValue:serviceUUID
   forHTTPHeaderField:@"X-PS-DeviceId"];
    [request addValue:@"ios"
   forHTTPHeaderField:@"X-PS-RO-OS"];
    
#if RUSHORDER_MERCHANT
    [request addValue:clientAppVersion
   forHTTPHeaderField:@"X-PS-ROMERCHANT-Version"];
#else
    [request addValue:clientAppVersion
   forHTTPHeaderField:@"X-PS-RO-Version"];
#endif

    switch(method){
        case RequestMethodGet:
            [request setHTTPMethod:@"GET"];
            break;
        case RequestMethodPost:
            [request setHTTPMethod:@"POST"];
            [request addValue:@"gzip, deflate" forHTTPHeaderField:@"Accept-Encoding"];
            [request setHTTPBody:bodyData];
            break;
        case RequestMethodPut:
            [request setHTTPMethod:@"PUT"];
            break;
        case RequestMethodDelete:
            [request setHTTPMethod:@"DELETE"];
            [request addValue:@"gzip, deflate" forHTTPHeaderField:@"Accept-Encoding"];
            [request setHTTPBody:bodyData];
            break;
        case RequestMethodMultiPartPost:
        case RequestMethodMultiPartPut:{
            static NSString * boundary = MULTIPART_BOUNDARY_STRING;
            [request setHTTPMethod: method == RequestMethodMultiPartPost ? @"POST" : @"PUT"];
            [request addValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary]
           forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:bodyData];
            break;
        }
    }
    
    return [self request:request
         completionBlock:completionBlock
         dataHandleBlock:dataHandleBlock];
}

- (RequestResult)request:(NSURLRequest *)request
         completionBlock:(CompletionBlock)completionBlock
         dataHandleBlock:(DataHandleBlock)dataHandleBlock
{
    //    CompletionBlock copiedBlock = (CompletionBlock)Block_copy(completionBlock);
    
    PCPendingRequest *pendingRequest = [PCPendingRequest
                                        pendingRequestWithCompletionBlock:completionBlock
                                        withDataHandleBlock:dataHandleBlock];
    pendingRequest.request = request;
    
    if(REACH.status == NotReachable){
        
        if([self.requestQueue count] > MAX_NUMBER_OF_QUEUE){
//            [self.requestQueue removeObjectAtIndex:0];
            PCWarning(@"Not reachable request queue is full");
        } else {
            
            NSInteger foundIndex = NSNotFound;
            NSInteger i = 0;
            for(PCPendingRequest *request in self.requestQueue){
                if([request.request.URL isEqual:pendingRequest.request.URL]){
                    foundIndex = i;
                    break;
                }
                i++;
            }
            if(foundIndex != NSNotFound){
                PCLog(@"Removed found item at %ld for %@", foundIndex, pendingRequest);
                [self.requestQueue removeObjectAtIndex:foundIndex];
            }
            [self.requestQueue addObject:pendingRequest];
        }
        
        if([self.requestQueue count] >= 2){
            [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                object:nil];
        }
        
        PCLog(@"Not reachable Add to queue, now queue is : \n%@", self.requestQueue);
        
        return RRNotReachable;
    }
    
    return [self connectWithRequest:pendingRequest];
}

- (RequestResult)connectWithRequest:(PCPendingRequest *)pendingRequest
{
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:pendingRequest.request
                                                                  delegate:self];
    if(connection == nil){
        PCError(@"Connection error, connection is nil");
        return RRConnectionError;
    } else {
        pendingRequest.connection = connection;
        [self.requestedDict setObject:pendingRequest forKey:connection.description];
        
        if([self.requestedDict count] > 10){
            PCWarning(@"Too many pending request!. Current requestedDict is : \n %@",self.requestedDict);
        }
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        return RRSuccess;
    }
}

#pragma mark - NSURLConnection delegates
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    PCPendingRequest *pendingRequest = [self.requestedDict objectForKey:connection.description];
    [pendingRequest.data setLength:0];

    NSHTTPURLResponse *httpRespnose = (NSHTTPURLResponse *)response;
    pendingRequest.statusCode = httpRespnose.statusCode;
    
#if DEBUG_RAW_RESPONSE
    PCLog(@"statusCode : %d", httpRespnose.statusCode);
#endif
    
    pendingRequest.response = response;
    
    
//    NSArray *cookies;
//    NSDictionary *responseHeaderFields;
//    responseHeaderFields = [httpRespnose allHeaderFields];
//    
//    if(responseHeaderFields != nil)
//    {
//        // responseHeaderFields에 포함되어 있는 항목들 출력
//        for(NSString *key in responseHeaderFields)
//        {
//            NSLog(@"Header: %@ = %@", key, [responseHeaderFields objectForKey:key]);
//        }
//        
//        // cookies에 포함되어 있는 항목들 출력
//        cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:responseHeaderFields
//                                                         forURL:[httpRespnose URL]];
//        
//        if(cookies != nil)
//        {
//            for(NSHTTPCookie *a in cookies)
//            {
//                NSLog(@"Cookie: %@ = %@", [a name], [a value]);
//            }
//        }
//    }
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{    
    PCPendingRequest *pendingRequest = [self.requestedDict objectForKey:connection.description];
    [pendingRequest.data appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    PCPendingRequest *pendingRequest = [self.requestedDict objectForKey:connection.description];
    
    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(aQueue, ^{
        
        NSError *error;
        id responseObject = nil;

        responseObject = [pendingRequest.data objectFromJSONData];
        
#if DEBUG_RAW_RESPONSE
        NSString *responseStringForLog = [[NSString alloc] initWithData:pendingRequest.data
                                                         encoding:NSUTF8StringEncoding];
        PCLog(@"Response text is : %@", responseStringForLog);
#endif
        if(responseObject == nil){
            NSString *responseString = [[NSString alloc] initWithData:pendingRequest.data
                                                             encoding:NSUTF8StringEncoding];
            PCError(@"Response text is : %@", responseString);
            responseObject = [NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:ResponseErrorUnknown]
                                                                                     forKey:ResponseErrorCodeKey];
        }
        
#ifdef USE_XML
        NSDictionary *xmlDict = [XMLReader dictionaryForXMLData:pendingRequest.data
                                                          error:&error];
#if DEBUG_RAW_RESPONSE
        NSString *responseString = [[NSString alloc] initWithData:pendingRequest.data
                                                         encoding:NSUTF8StringEncoding];
        PCLog(@"Response text is : %@", responseString);
#endif
        if(error == nil){
            responseObject = xmlDict;
        } else {
            PCError(@"XML Parsing error : %@", error);
            NSString *responseString = [[NSString alloc] initWithData:pendingRequest.data
                                                             encoding:NSUTF8StringEncoding];
            PCError(@"Response text is : %@", responseString);
            
            responseObject = nil;
            
        }
#endif
        
        if(pendingRequest.dataHandleBlock != nil){
            dispatch_async(dispatch_get_main_queue(),^{
                [[NSNotificationCenter defaultCenter] postNotificationName:NetworkGoodReportNotification
                                                                    object:self
                                                                  userInfo:nil];
#if DEBUG_RAW_RESPONSE
                PCLog(@"[RESPONSE]%@", responseObject);
#endif
                pendingRequest.dataHandleBlock(responseObject, pendingRequest.completionBlock, pendingRequest.statusCode);
            });
        } else {
            dispatch_async(dispatch_get_main_queue(),^{
                pendingRequest.completionBlock((error == nil), responseObject, pendingRequest.statusCode);
            });
        }
    });
    
    [self.requestedDict removeObjectForKey:connection.description];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    PCPendingRequest *pendingRequest = [self.requestedDict objectForKey:connection.description];
    
    if(error.code == NSURLErrorCancelled){
        pendingRequest.statusCode = HTTP_STATUS_CONNECTION_CANCELED;
    } else {
        pendingRequest.statusCode = HTTP_STATUS_CONNECTION_ERROR;
    }
    
    if(pendingRequest.dataHandleBlock != nil){
        if(error.code == NSURLErrorCancelled){
            pendingRequest.dataHandleBlock([NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:ResponseErrorConnectionCanceled]
                                                                       forKey:@"error_code"]
                                           , pendingRequest.completionBlock, pendingRequest.statusCode);
        } else {
            pendingRequest.dataHandleBlock([NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:ResponseErrorNetworkError]
                                                                       forKey:@"error_code"]
                                            , pendingRequest.completionBlock, pendingRequest.statusCode);
        }
    } else {
        dispatch_async(dispatch_get_main_queue(),^{
            pendingRequest.completionBlock(NO, nil, pendingRequest.statusCode);
        });
    }
    
    [self.requestedDict removeObjectForKey:connection.description];
    
    // inform the user
    if(error.code != NSURLErrorCancelled){
        PCError(@"Connection failed! Error - %@ %@",
                [error localizedDescription],
                [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    } else {
        PCWarning(@"Connection canceled - %@ %@",
                  [error localizedDescription],
                  [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    }
}

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if ([challenge previousFailureCount] > 0) {
        PCWarning(@"Authentication Failure");
        [connection cancel];
    }
    else
    {
        if ([challenge.protectionSpace.authenticationMethod
             isEqualToString:NSURLAuthenticationMethodServerTrust])
        {
            NSString *host = nil;
            
            host = SERVER_URL_ADDRESS;
            
            // we only trust our own domain
            if ([challenge.protectionSpace.host isEqualToString:host])
            {
                NSURLCredential *credential =
				[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
                [challenge.sender useCredential:credential forAuthenticationChallenge:challenge];
            }
        }
        
        [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
    }
}

- (BOOL)connectionShouldUseCredentialStorage:(NSURLConnection *)connection
{
    return YES;
}

- (NSMutableArray *)requestQueue
{
    if(_requestQueue == nil){
        _requestQueue = [NSMutableArray array];
    }
    return _requestQueue;
}


@end