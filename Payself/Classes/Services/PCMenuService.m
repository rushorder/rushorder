//
//  PCMenuService.m
//  RushOrder
//
//  Created by Conan on 5/29/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCMenuService.h"
#import "MenuPan.h"
#import "MenuCategory.h"
#import "MenuSubCategory.h"
#import "MenuItem.h"
#import "LineItem.h"
#import "JSONKit.h"
#import "TableInfo.h"
#import "Cart.h"
#import "Order.h"
#import "BlockUtil.h"
#if RUSHORDER_CUSTOMER
    #import "RemoteDataManager.h"
#endif

@implementation PCMenuService

+ (PCMenuService *)sharedService
{
    static PCMenuService *aService = nil;
    
    @synchronized(self){
        if(aService == nil){
            aService = [[self alloc] init];
        }
    }
    
    return aService;
}

- (id)init
{
    self = [super init];
    if(self != nil){
        // Initializer
    }
    return self;
}


- (RequestResult)requestMenu:(Merchant *)merchant
                        type:(MenuListType)type
             completionBlock:(CompletionBlock)completionBlock
{
    if(merchant.merchantNo == 0){
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:MENU_LIST_PARAM,
                               merchant.merchantNo];
    
    return [super requestWithAPI:MENU_LISTS_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                
                if(response.errorCode == ResponseSuccess){
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSMutableArray *activeMenuList = [NSMutableArray array];
                        NSMutableArray *menuPans = [NSMutableArray array];
                        NSMutableArray *wholeMenuList = [NSMutableArray array];
                        
                        for(NSDictionary *aMenu in response){
                            
                            NSArray *menuArray = [aMenu objectForKey:@"menus"];
                            
                            if([menuArray count] > 0){
                                NSMutableArray *menuList = [NSMutableArray array];
                                
                                MenuCategory *menuCategory = nil;
                                MenuSubCategory *menuSubCategory = nil;
                                
                                NSInteger i = 0;
                                
                                for(i = 0 ; i < [menuArray count] ; i++){
                                    
                                    NSDictionary *dict = [menuArray objectAtIndex:i];
                                    
                                    if([[dict objectForKey:@"menu_type"] isEqual:@"section"]){
                                        if(menuCategory == nil){
                                            menuCategory = [[MenuCategory alloc] initWithDictionary:dict];
                                            [menuList addObject:menuCategory];
                                            continue;
                                        } else {
                                            
                                            if(menuSubCategory != nil && [menuSubCategory.items count] == 0){
                                                [menuCategory.subCategories removeLastObject];
                                                [menuCategory.items  removeObject:menuSubCategory];
                                            }
                                            
                                            if(menuCategory != nil && [menuCategory.items count] == 0){
                                                [menuList removeLastObject];
                                            }
                                            
                                            menuCategory = nil;
                                            menuSubCategory = nil;
                                            
                                            i--;
                                            continue;
                                        }
                                    }
                                    
                                    if(menuCategory == nil){
                                        menuCategory = [[MenuCategory alloc] init];
                                        [menuList addObject:menuCategory];
                                    }
                                    
                                    if([[dict objectForKey:@"menu_type"] isEqual:@"subsection"]){
                                        if(menuSubCategory == nil){
                                            menuSubCategory = [[MenuSubCategory alloc] initWithDictionary:dict];
                                            [menuCategory.subCategories addObject:menuSubCategory];
                                            [menuCategory.items addObject:menuSubCategory];
                                            continue;
                                        } else {
                                            
                                            if(menuSubCategory != nil && [menuSubCategory.items count] == 0){
                                                [menuCategory.subCategories removeLastObject];
                                                [menuCategory.items  removeObject:menuSubCategory];
                                            }
                                            
                                            menuSubCategory = nil;
                                            i--;
                                            continue;
                                        }
                                    }
                                    
                                    if(menuSubCategory == nil){
                                        menuSubCategory = [[MenuSubCategory alloc] init];
                                        [menuCategory.subCategories addObject:menuSubCategory];
                                        [menuCategory.items addObject:menuSubCategory];
                                    }
                                    
                                    if([[dict objectForKey:@"menu_type"] isEqual:@"item"]){
                                        MenuItem *item = [[MenuItem alloc] initWithDictionary:dict];
                                        BOOL canBeAppended = NO;
                                        
                                        if((type & MenuListTypeDinein) == MenuListTypeDinein){
                                            canBeAppended = item.forDineIn;
                                        }
                                        
                                        if(!canBeAppended){
                                            if((type & MenuListTypeTakeout) == MenuListTypeTakeout){
                                                canBeAppended = item.forTakeout;
                                            }
                                        }
                                        
                                        if(!canBeAppended){
                                            if((type & MenuListTypeDelivery) == MenuListTypeDelivery){
                                                canBeAppended = item.forDelivery;
                                            }
                                        }
                                        
                                        if(canBeAppended){
                                            item.taxRate = merchant.taxRate;
                                            [menuSubCategory.items addObject:item];
                                            [menuCategory.items addObject:item];
                                        }
                                        [wholeMenuList addObject:item];
                                    }
                                }
                                
                                if(menuCategory != nil && [menuCategory.items count] <= 1){
                                    if([menuCategory.items count] == 1){
                                        id orphanItem = [menuCategory.items objectAtIndex:0];
                                        if([orphanItem isKindOfClass:[MenuSubCategory class]]){
                                            if([((MenuSubCategory *)orphanItem).items count] == 0){
                                                [menuList removeLastObject];
                                            }
                                        }
                                    } else {
                                        [menuList removeLastObject];
                                    }
                                }
                                
                                MenuPan *aPan = [[MenuPan alloc] initWithDictionary:aMenu
                                                                       withTimeZone:merchant.timeZone];
                                aPan.menus = menuList;
//                                [aPan.menus makeObjectsPerformSelector:@selector(setMenuPan:)
//                                                            withObject:aPan];
                                for(MenuCategory *category in aPan.menus){
                                    for(MenuSubCategory *sub in category.subCategories){
                                        [sub.items makeObjectsPerformSelector:@selector(linkMenuPan:)
                                                                   withObject:aPan];
                                    }
                                }
                                [menuPans addObject:aPan];
                                
                                if(aPan.isActive){
                                    [activeMenuList addObject:aPan];
                                }
                                
                            }//if([menuArray count] > 0)
                            
                        } //for(NSDictionary aMenu in response
                        
                        NSDictionary *returnDict = nil;
                        
                        if(activeMenuList == nil){
                            returnDict = @{@"wholeMenuList":menuPans,
                                           @"wholeSerialMenuList":wholeMenuList};
                        } else {
                            returnDict = @{@"activeMenuList":activeMenuList,
                                           @"wholeMenuList":menuPans,
                                           @"wholeSerialMenuList":wholeMenuList};
                        }
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, returnDict, statusCode);
                        });
                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}


- (RequestResult)requestMenuPan:(Merchant *)merchant
                completionBlock:(CompletionBlock)completionBlock
{
    if(merchant.merchantNo == 0){
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:MENUPAN_LISTS_PARAM,
                               merchant.merchantNo];
    
    return [super requestWithAPI:MENUPAN_LISTS_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}


- (RequestResult)requestOrderAndCart:(PCSerial)merchantId
                           tableInfo:(TableInfo *)tableInfo
                     completionBlock:(CompletionBlock)completionBlock
{
    if(merchantId == 0){
        return RRParameterError;
    }
    
    if(tableInfo == nil){
        return RRParameterError;
    }
    
    NSString *apiURL = [NSString stringWithFormat:CART_AND_ORDER_API_URL,
                        merchantId];
    
    NSString *baseParameter = [NSString stringWithFormat:CART_AND_ORDER_PARAM,
                               [[tableInfo.tableNumber uppercaseString] urlEncodedString]];
    
    return [super requestWithAPI:apiURL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_async(dispatch_get_main_queue(),^{
                    completionBlock(YES, response, statusCode);
                });
            }];
}

- (RequestResult)requestAddItems:(NSArray *)lineItems
                      merchantNo:(PCSerial)merchantNo
                      tableLabel:(NSString *)tableLabel
                     deviceToken:(NSString *)deviceToken
                       authToken:(NSString *)authToken
                 completionBlock:(CompletionBlock)completionBlock
{
    if(merchantNo == 0){
        return RRParameterError;
    }
    
    NSMutableArray *arrayForJSON = [NSMutableArray array];
    for(LineItem * lineItem in lineItems){
        [arrayForJSON addObject:[lineItem dictRepWithDeviceToken:deviceToken]];
    }
    
    NSString *jsonString = [arrayForJSON JSONString];
    
    NSString *baseParameter = [NSString stringWithFormat:ADD_LINEITEM_PARAM,
                               merchantNo,
                               [[tableLabel uppercaseString] urlEncodedString],
                               deviceToken ? deviceToken.urlEncodedString : @"",
                               [jsonString urlEncodedString],
                               authToken ? authToken : @""];
    
    return [super requestPostWithAPI:ADD_LINEITEM_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_async(dispatch_get_main_queue(),^{
                    completionBlock(YES, response, statusCode);
                });
            }];
}

- (RequestResult)requestUpdateItems:(Cart *)cart
                          lineItems:(NSArray *)lineItems
                        deviceToken:(NSString *)deviceToken
                    completionBlock:(CompletionBlock)completionBlock
{
    if(cart == nil){
        return RRParameterError;
    }
    
    NSMutableArray *arrayForJSON = [NSMutableArray array];
    for(LineItem * lineItem in lineItems){
        [arrayForJSON addObject:[lineItem updateDictRepWithDeviceToken:deviceToken]];
    }
    
    NSString *apiURL = [NSString stringWithFormat:UPDATE_LINEITEM_API_URL, cart.cartNo];
    
    NSString *jsonString = [arrayForJSON JSONString];
    NSString *baseParameter = [NSString stringWithFormat:UPDATE_LINEITEM_PARAM,
                               [jsonString urlEncodedString]];
    
    return [super requestPutWithAPI:apiURL
                          parameter:baseParameter
                withCompletionBlock:completionBlock
                withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_async(dispatch_get_main_queue(),^{
                    completionBlock(YES, response, statusCode);
                });
            }];
}

- (RequestResult)requestDeleteItems:(Cart *)cart
                          lineItems:(NSArray *)lineItems
                        deviceToken:(NSString *)deviceToken
                    completionBlock:(CompletionBlock)completionBlock
{
    if(cart == nil){
        return RRParameterError;
    }
    
    NSMutableArray *arrayForJSON = [NSMutableArray array];
    for(LineItem * lineItem in lineItems){
        [arrayForJSON addObject:[NSNumber numberWithSerial:lineItem.lineItemNo]];
    }
    
    NSString *apiURL = [NSString stringWithFormat:DELETE_LINEITEM_API_URL, cart.cartNo];
    
    NSString *baseParameter = [NSString stringWithFormat:DELETE_LINEITEM_PARAM,
                               [[arrayForJSON JSONString] urlEncodedString]];
    
    return [super requestDeleteWithAPI:apiURL
                             parameter:baseParameter
                   withCompletionBlock:completionBlock
                   withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_async(dispatch_get_main_queue(),^{
                    completionBlock(YES, response, statusCode);
                });
            }];
}

- (RequestResult)requestAddItemsToOrder:(Order *)order
                              lineItems:(NSArray *)lineItems
                            deviceToken:(NSString *)deviceToken
                              authToken:(NSString *)authToken
                        completionBlock:(CompletionBlock)completionBlock
{
    if(order == nil){
        return RRParameterError;
    }
    
    NSMutableArray *arrayForJSON = [NSMutableArray array];
    for(LineItem * lineItem in lineItems){
        [arrayForJSON addObject:[lineItem dictRepWithDeviceToken:deviceToken]];
    }
    
    NSString *jsonString = [arrayForJSON JSONString];
    
    NSString *apiURL = [NSString stringWithFormat:ADD_ORDER_LINEITEM_API_URL, order.orderNo];
    
    NSString *baseParameter = [NSString stringWithFormat:ADD_ORDER_LINEITEM_PARAM,
                               [jsonString urlEncodedString],
                               deviceToken ? deviceToken.urlEncodedString : @"",
                               authToken ? authToken : @""];
    
    return [super requestPostWithAPI:apiURL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_async(dispatch_get_main_queue(),^{
                    completionBlock(YES, response, statusCode);
                });
            }];
}

- (RequestResult)requestUpdateItemsToOrder:(Order *)order
                                 lineItems:(NSArray *)lineItems
                               deviceToken:(NSString *)deviceToken
                           completionBlock:(CompletionBlock)completionBlock
{
    if(order == nil){
        return RRParameterError;
    }
    
    NSMutableArray *arrayForJSON = [NSMutableArray array];
    for(LineItem * lineItem in lineItems){
        [arrayForJSON addObject:[lineItem updateDictRepWithDeviceToken:deviceToken]];
    }
    
    NSString *apiURL = [NSString stringWithFormat:UPDATE_ORDER_LINEITEM_API_URL, order.orderNo];
    
    NSString *jsonString = [arrayForJSON JSONString];
    PCLog(@"jsonString %@", jsonString);
    NSString *baseParameter = [NSString stringWithFormat:UPDATE_ORDER_LINEITEM_PARAM,
                               [jsonString urlEncodedString]];
    
    return [super requestPutWithAPI:apiURL
                          parameter:baseParameter
                withCompletionBlock:completionBlock
                withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_async(dispatch_get_main_queue(),^{
                    completionBlock(YES, response, statusCode);
                });
            }];
}

- (RequestResult)requestDeleteItemsFromOrder:(Order *)order
                                   lineItems:(NSArray *)lineItems
                                 deviceToken:(NSString *)deviceToken
                                   authToken:(NSString *)authToken
                             completionBlock:(CompletionBlock)completionBlock
{
    return [self requestDeleteItemsFromOrder:order
                                   lineItems:lineItems
                           withCustomerClose:NO
                                 deviceToken:deviceToken
                                   authToken:authToken
                             completionBlock:completionBlock];
}

- (RequestResult)requestDeleteItemsFromOrder:(Order *)order
                                   lineItems:(NSArray *)lineItems
                           withCustomerClose:(BOOL)withCustomerClose
                                 deviceToken:(NSString *)deviceToken
                                   authToken:(NSString *)authToken
                             completionBlock:(CompletionBlock)completionBlock
{
    if(order == nil){
        return RRParameterError;
    }
    
    NSMutableArray *arrayForJSON = [NSMutableArray array];
    for(LineItem * lineItem in lineItems){
        [arrayForJSON addObject:[NSNumber numberWithSerial:lineItem.lineItemNo]];
    }
    
    NSString *apiURL = [NSString stringWithFormat:DELETE_ORDER_LINEITEM_API_URL,
                        order.orderNo];
    
    NSString *baseParameter = [NSString stringWithFormat:DELETE_ORDER_LINEITEM_PARAM,
                               [[arrayForJSON JSONString] urlEncodedString],
                               withCustomerClose ? @"true" : @"false",
                               authToken ? authToken : @""];
    
    return [super requestDeleteWithAPI:apiURL
                             parameter:baseParameter
                   withCompletionBlock:completionBlock
                   withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_async(dispatch_get_main_queue(),^{
                    completionBlock(YES, response, statusCode);
                });
            }];
}



- (RequestResult)requestUpdateCart:(Cart *)cart
                       deviceToken:(NSString *)deviceToken
                   completionBlock:(CompletionBlock)completionBlock
{
    if(cart == nil){
        return RRParameterError;
    }
    
    NSString *apiURL = [NSString stringWithFormat:CART_API_URL, cart.cartNo];
    
    NSString *baseParameter = [NSString stringWithFormat:UPDATE_CART_PARAM,
                               cart.statusString,
                               deviceToken ? deviceToken.urlEncodedString : @""];
    
    return [super requestPutWithAPI:apiURL
                          parameter:baseParameter
                withCompletionBlock:completionBlock
                withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_async(dispatch_get_main_queue(),^{
                    completionBlock(YES, response, statusCode);
                });
            }];
}

- (RequestResult)requestGettingCart:(Cart *)cart
                    completionBlock:(CompletionBlock)completionBlock
{
    if(cart == nil){
        return RRParameterError;
    }
    
    NSString *apiURL = [NSString stringWithFormat:CART_API_URL, cart.cartNo];
    
    return [super requestWithAPI:apiURL
                             parameter:nil
                   withCompletionBlock:completionBlock
                   withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_async(dispatch_get_main_queue(),^{
                    completionBlock(YES, response, statusCode);
                });
            }];
}

- (RequestResult)requestMyOrderListAuthToken:(NSString *)authToken
                             completionBlock:(CompletionBlock)completionBlock
{
    NSString *baseParameter = nil;
    
    if([authToken length] > 0){
        baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                                   authToken];
    }
    
    return [super requestWithAPI:MY_ORDER_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSMutableArray *orderCartList = [self handleMyOrders:response];
                        
                        NSMutableDictionary *mutableResponse = [NSMutableDictionary dictionaryWithDictionary:response];
                        
                        if([orderCartList count] > 0){
                            [mutableResponse setObject:orderCartList
                                                forKey:@"OrderCartList"];
                        }
                        
                        if([response objectForKey:@"referral_badge_count"] != nil){
                            [mutableResponse setObject:[response objectForKey:@"referral_badge_count"]
                                                forKey:@"referral_badge_count"];
                        }
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, mutableResponse, statusCode);
                        });
                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestMyOrderId:(PCSerial)orderId
                        authToken:(NSString *)authToken
                  completionBlock:(CompletionBlock)completionBlock
{
    NSString *baseParameter = nil;
    
    
    baseParameter = [NSString stringWithFormat:MY_ORDER_DETAIL_PARAM,
                     orderId,
                     authToken ? authToken : @""];

    
    return [super requestWithAPI:MY_ORDER_DETAIL_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        
                        NSMutableArray *orderCartList = [self handleMyOrders:response];
                        
                        
                        NSMutableDictionary *mutableResponse = [NSMutableDictionary dictionaryWithDictionary:response];
                        
                        if([orderCartList count] > 0){
                            [mutableResponse setObject:orderCartList
                                                forKey:@"OrderCartList"];
                        }
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, mutableResponse, statusCode);
                        });
                    });
                    
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestAddOrdersAuthToken:(NSString *)authToken
                           completionBlock:(CompletionBlock)completionBlock
{
    if([authToken length] == 0){
        PCError(@"Auth token is empty for requesting adding orders");
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               authToken];
    
    return [super requestPostWithAPI:ADD_ORDERS_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSArray *completedOrderDicts = [response objectForKey:@"closed"];
                        NSArray *activeOrderDicts = [response objectForKey:@"active"];
                        
                        NSMutableArray *completedOrders = [NSMutableArray array];
                        NSMutableArray *activeOrders = [NSMutableArray array];
                        
                        for(NSDictionary *dict in activeOrderDicts){
                            Order *order = [[Order alloc] initWithDictionary:dict];
                            order.customerClose = NO;
                            [activeOrders addObject:order];
                        }
                        
                        for(NSDictionary *dict in completedOrderDicts){
                            Order *order = [[Order alloc] initWithDictionary:dict];
                            order.customerClose = YES;
                            [completedOrders addObject:order];
                        }
                        
                        NSDictionary *responseDict = @{@"completed":completedOrders?completedOrders:[NSNull null],
                                                       @"active":activeOrders?activeOrders:[NSNull null]};
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, responseDict, statusCode);
                        });
                    });
                    
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestNearMeAuthToken:(NSString *)authToken
                               location:(CLLocation *)currentLocation
                                   page:(NSInteger)page
                                   seed:(NSInteger)seed
                        completionBlock:(CompletionBlock)completionBlock
{
    NSString *baseParameter = [NSString stringWithFormat:NEAR_ME_ORDER_PARAM,
                               currentLocation.coordinate.latitude,
                               currentLocation.coordinate.longitude,
                               (long)page,
                               (long)seed];
    
    if([authToken length] > 0){
        baseParameter = [baseParameter stringByAppendingFormat:APPENDING_AUTH_TOKEN_PARAM,
                         authToken];
    }
    
    return [super requestWithAPI:NEAR_ME_ORDER_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSMutableArray *orderList = [NSMutableArray array];
                        
                        for(NSDictionary *anOrderDict in response){
                            Order *order = [[Order alloc] initWithDictionary:anOrderDict];
                            [orderList addObject:order];
                        }
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, orderList, statusCode);
                        });
                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}

- (RequestResult)requestPopularMenuInLocation:(CLLocation *)currentLocation
                                         page:(NSInteger)page
                                         seed:(NSInteger)seed
                              completionBlock:(CompletionBlock)completionBlock
{
    NSString *baseParameter = [NSString stringWithFormat:POPULAR_MENUS_PARAMS,
                               currentLocation.coordinate.latitude,
                               currentLocation.coordinate.longitude,
                               (long)page,
                               (long)seed];

    
    return [super requestWithAPI:POPULAR_MENUS_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(response.errorCode == ResponseSuccess){
                    
                    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                    dispatch_async(aQueue, ^{
                        
                        NSMutableArray *menuList = [NSMutableArray array];
                        
                        for(NSDictionary *aMenuDict in response){
                            MenuItem *menuItem = [[MenuItem alloc] initWithDictionary:aMenuDict];
                            [menuList addObject:menuItem];
                        }
                        
                        dispatch_async(dispatch_get_main_queue(),^{
                            completionBlock(YES, menuList, statusCode);
                        });
                    });
                } else {
                    completionBlock(YES, response, statusCode);
                }
            }];
}



- (NSMutableArray *)handleMyOrders:(NSDictionary *)response
{
    NSMutableArray *orderCartList = [NSMutableArray array];
    
    NSArray *cartList = [response objectForKey:@"carts"];
    if([cartList count] > 0){
        for(NSDictionary *cartDict in cartList){
            Cart *cart = [[Cart alloc] init];
            
            cart.merchant = [[Merchant alloc] initWithDictionary:[cartDict objectForKey:@"merchant"]];
#if RUSHORDER_CUSTOMER
            cart.merchant.favorite = [REMOTE isFavoriteMerchant:cart.merchant];
#endif
            if(APP.location != nil){
                cart.merchant.distance = [APP.location distanceFromLocation:cart.merchant.location];
            }
            
            [cart updateWithDictionary:cartDict menuPan:nil];
            [cart updateMerchantWithDict:[cartDict objectForKey:@"merchant"]];
            cart.tableLabel = [cartDict objectForKey:@"table_label"];
            
            NSInteger insertIndex = [orderCartList indexOfObject:cart
                                                   inSortedRange:NSMakeRange(0, [orderCartList count])
                                                         options:NSBinarySearchingInsertionIndex
                                                 usingComparator:orderOrCartComparator];
            [orderCartList insertObject:cart
                                atIndex:insertIndex];
        }
    }
    
    NSArray *orderList = [response objectForKey:@"orders"];
    NSArray *billOrderList = [response objectForKey:@"bill_orders"];
    
    orderList = [orderList arrayByAddingObjectsFromArray:billOrderList];
    
    if([orderList count] > 0){
        for(NSDictionary *orderDict in orderList){
            
            NSString *tableLabel = [orderDict objectForKey:@"table_label"];
            PCSerial merchantId = [orderDict serialForKey:@"merchant_id"];
            
            BOOL isSameTableCart = NO;
            // This roution is right based on the orderCartList is sorted by Cart and Order...
            for(id obj in orderCartList){
                if([obj isKindOfClass:[Cart class]]){
                    Cart *castedCart = (Cart *)obj;
                    if([castedCart.tableLabel isEqual:tableLabel] &&
                       castedCart.merchantNo == merchantId){
                        castedCart.additionalCart = YES;
                        isSameTableCart = YES;
                        break;
                    }
                } else {
                    break;
                }
            }
            
            if(!isSameTableCart){
                Merchant *merchant = [[Merchant alloc] initWithDictionary:[orderDict objectForKey:@"merchant"]];
#if RUSHORDER_CUSTOMER
                merchant.favorite = [REMOTE isFavoriteMerchant:merchant];
#endif
                if(APP.location != nil){
                    merchant.distance = [APP.location distanceFromLocation:merchant.location];
                }
                Order *order = [[Order alloc] initWithDictionary:orderDict
                                                        merchant:merchant];
                
                order.merchant = merchant;
                
                NSInteger insertIndex = [orderCartList indexOfObject:order
                                                       inSortedRange:NSMakeRange(0, [orderCartList count])
                                                             options:NSBinarySearchingInsertionIndex
                                                     usingComparator:orderOrCartComparator];
                [orderCartList insertObject:order
                                    atIndex:insertIndex];
                
            }
        }
    }
    
    return orderCartList;
}

- (RequestResult)requestPickupOrder:(NSArray *)lineItems
                         merchantNo:(PCSerial)merchantNo
                         flagNumber:(NSString *)flagNumber
                       receiverName:(NSString *)receiverName
                    customerRequest:(NSString *)customerRequest
                        deviceToken:(NSString *)deviceToken
                          authToken:(NSString *)authToken
                     subTotalAmount:(PCCurrency)subTotalAmount
              subTotalAmountTaxable:(PCCurrency)subTotalAmountTaxable
                          taxAmount:(PCCurrency)taxAmount
                    completionBlock:(CompletionBlock)completionBlock
{
    if(merchantNo == 0){
        return RRParameterError;
    }
    
    NSMutableArray *arrayForJSON = [NSMutableArray array];
    for(LineItem * lineItem in lineItems){
        [arrayForJSON addObject:[lineItem dictRepWithDeviceToken:deviceToken]];
    }
    
    NSString *jsonString = [arrayForJSON JSONString];
    
    NSString *baseParameter = [NSString stringWithFormat:PICKUP_ORDER_PARAM,
                               [jsonString urlEncodedString],
                               (long)subTotalAmount,
                               (long)subTotalAmountTaxable,
                               (long)taxAmount,
                               merchantNo,
                               flagNumber ? [[flagNumber uppercaseString] urlEncodedString] : @"",
                               deviceToken ? deviceToken.urlEncodedString : @"",
                               receiverName ? [receiverName urlEncodedString] : @"",
                               customerRequest ? [customerRequest urlEncodedString] : @"",
                               authToken ? authToken : @""];
    
    return [super requestPostWithAPI:PICKUP_ORDER_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_async(dispatch_get_main_queue(),^{
                    completionBlock(YES, response, statusCode);
                });
            }];
}

- (RequestResult)requestTakeoutOrder:(NSArray *)lineItems
                          merchantNo:(PCSerial)merchantNo
                         pickupAfter:(NSInteger)pickupAfter
                     customerRequest:(NSString *)customerRequest
                        receiverName:(NSString *)receiverName
                       receiverPhone:(NSString *)receiverPhone
                         deviceToken:(NSString *)deviceToken
                           authToken:(NSString *)authToken
                      subTotalAmount:(PCCurrency)subTotalAmount
               subTotalAmountTaxable:(PCCurrency)subTotalAmountTaxable
                           taxAmount:(PCCurrency)taxAmount
                     completionBlock:(CompletionBlock)completionBlock
{
    if(merchantNo == 0){
        return RRParameterError;
    }
    
    NSMutableArray *arrayForJSON = [NSMutableArray array];
    for(LineItem * lineItem in lineItems){
        [arrayForJSON addObject:[lineItem dictRepWithDeviceToken:deviceToken]];
    }
    
    NSString *jsonString = [arrayForJSON JSONString];
    
    NSString *baseParameter = [NSString stringWithFormat:TAKEOUT_ORDER_PARAM,
                               [jsonString urlEncodedString],
                               (long)subTotalAmount,
                               (long)subTotalAmountTaxable,
                               (long)taxAmount,
                               merchantNo,
                               (long)pickupAfter,
                               customerRequest ? [customerRequest urlEncodedString] : @"",
                               receiverName ? receiverName.urlEncodedString : @"",
                               receiverPhone ? receiverPhone.urlEncodedString : @"",
                               deviceToken ? deviceToken.urlEncodedString : @"",
                               authToken ? authToken : @""];
    
    return [super requestPostWithAPI:TAKEOUT_ORDER_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_async(dispatch_get_main_queue(),^{
                    completionBlock(YES, response, statusCode);
                });
            }];
}

- (RequestResult)requestDeliveryOrder:(NSArray *)lineItems
                           merchantNo:(PCSerial)merchantNo
                          pickupAfter:(NSInteger)pickupAfter
                      customerRequest:(NSString *)customerRequest
                          deviceToken:(NSString *)deviceToken
                            authToken:(NSString *)authToken
                       subTotalAmount:(PCCurrency)subTotalAmount
                subTotalAmountTaxable:(PCCurrency)subTotalAmountTaxable
                            taxAmount:(PCCurrency)taxAmount
                         addressState:(NSString *)state
                          addressCity:(NSString *)city
                       addressStreet1:(NSString *)street1
                       addressStreet2:(NSString *)street2
                           addressZip:(NSString *)zip
                          phoneNumber:(NSString *)phoneNumber
                         receiverName:(NSString *)receiverName
                    deliveryFeeAmount:(PCCurrency)deliveryFeeAmount
                   roServiceFeeAmount:(PCCurrency)roServiceFeeAmount
                      completionBlock:(CompletionBlock)completionBlock
{
    if(merchantNo == 0){
        return RRParameterError;
    }
    
    NSMutableArray *arrayForJSON = [NSMutableArray array];
    for(LineItem * lineItem in lineItems){
        [arrayForJSON addObject:[lineItem dictRepWithDeviceToken:deviceToken]];
    }
    
    NSString *jsonString = [arrayForJSON JSONString];
    
    NSString *baseParameter = [NSString stringWithFormat:TAKEOUT_ORDER_PARAM DELIVERY_ORDER_PARAM,
                               [jsonString urlEncodedString],
                               (long)subTotalAmount,
                               (long)subTotalAmountTaxable,
                               (long)taxAmount,
                               merchantNo,
                               (long)pickupAfter,
                               customerRequest ? [customerRequest urlEncodedString] : @"",
                               [receiverName urlEncodedString],
                               phoneNumber ? [phoneNumber urlEncodedString] : @"",
                               deviceToken ? deviceToken.urlEncodedString : @"",
                               authToken ? authToken : @"",
                               [city urlEncodedString],
                               [state urlEncodedString],
                               [street1 urlEncodedString],
                               street2 ? [street2 urlEncodedString] : @"",
                               [zip urlEncodedString],
                               (long)deliveryFeeAmount,
                               (long)roServiceFeeAmount];
    
    return [super requestPostWithAPI:DELIVERY_ORDER_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_async(dispatch_get_main_queue(),^{
                    completionBlock(YES, response, statusCode);
                });
            }];
}

- (RequestResult)requestRenewOrder:(Order *)order
                       deviceToken:(NSString *)deviceToken
                         authToken:(NSString *)authToken
                   completionBlock:(CompletionBlock)completionBlock
{
    if(order == nil){
        return RRParameterError;
    }
    
    NSString *apiUrlString = [NSString stringWithFormat:RENEW_ORDER_API_URL,
                              order.orderNo];
    
    NSString *baseParameter = [NSString stringWithFormat:RENEW_ORDER_PARAM DELIVERY_ORDER_PARAM,
                               (long)order.pickupAfter,
                               order.customerRequest ? [order.customerRequest urlEncodedString] : @"",
                               order.tableNumber ? order.tableNumber : @"",
                               order.receiverName ? [order.receiverName urlEncodedString] : @"",
                               order.phoneNumber ? [order.phoneNumber urlEncodedString] : @"",
                               authToken ? authToken : @"",
                               [order.city urlEncodedString],
                               [order.state urlEncodedString],
                               [order.address1 urlEncodedString],
                               order.address2 ? [order.address2 urlEncodedString] : @"",
                               [order.zip urlEncodedString],
                               (long)order.deliveryFee,
                               (long)order.roServiceFee];
    
    return [super requestPutWithAPI:apiUrlString
                          parameter:baseParameter
                withCompletionBlock:completionBlock
                withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_async(dispatch_get_main_queue(),^{
                    completionBlock(YES, response, statusCode);
                });
            }];
}

- (RequestResult)requestUpdateSubmittedForCart:(Cart *)cart
                               completionBlock:(CompletionBlock)completionBlock
{
    if(cart.status != CartStatusOrdering
       && cart.status != CartStatusSubmitted){
        PCError(@"Cart status is not valid, cart status should be CartStatusOrdering, CartStatusSubmitted");
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:UPDATE_CART_SUBMITTED_PARAM,
                               (cart.status == CartStatusSubmitted) ? @"true" : @"false"];
    
    NSString *apiUrlString = [NSString stringWithFormat:UPDATE_CART_SUBMITTED_API_URL,
                              cart.cartNo];
    
    return [super requestWithAPI:apiUrlString
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}


@end
