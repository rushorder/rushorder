//
//  UIView+utils.m
//  RushOrder
//
//  Created by Conan on 2/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "UIView+utils.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIView (utils)

+ (id)view
{
    return [self viewWithNibName:NSStringFromClass(self)];
}

+ (id)viewWithNibName:(NSString *)nibName
{
    return [self viewWithNibName:nibName atIndex:0];
}

+ (id)viewWithNibName:(NSString *)nibName atIndex:(NSUInteger)objectIndex
{
    NSArray *topLevelObject = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    UIView *rtnView = (UIView *)[topLevelObject objectAtIndex:objectIndex];
    return rtnView;
}

- (CGPoint)origin
{
    return self.frame.origin;
}

- (CGSize)size
{
    return self.frame.size;
}

- (CGFloat)x
{
    return self.origin.x;
}

- (CGFloat)y
{
    return self.origin.y;
}

- (CGFloat)width
{
    return self.size.width;
}

- (CGFloat)height
{
    return self.size.height;
}

- (void)setOrigin:(CGPoint)newOrigin
{
    CGRect frame = self.frame;
    frame.origin = newOrigin;
    self.frame = frame;
}

- (void)setSize:(CGSize)newSize
{
    CGRect frame = self.frame;
    frame.size = newSize;
    self.frame = frame;
}

- (void)setX:(CGFloat)newX
{
    CGRect frame = self.frame;
    frame.origin.x = newX;
    self.frame = frame;
}

- (void)setY:(CGFloat)newY
{
    CGRect frame = self.frame;
    frame.origin.y = newY;
    self.frame = frame;
}

- (void)setWidth:(CGFloat)newWidth
{
    CGRect frame = self.frame;
    frame.size.width = newWidth;
    self.frame = frame;
}

- (void)setHeight:(CGFloat)newHeight
{
    CGRect frame = self.frame;
    frame.size.height = newHeight;
    self.frame = frame;
}

- (void)drawBorder
{
    [self drawBorderWithColor:[UIColor redColor]];
}

- (void)drawBorderWithColor:(UIColor *)lineColor
{
    self.layer.borderColor = [lineColor CGColor];
    self.layer.borderWidth = 1.0f;
}

- (UIView *)findFirstResponder
{
    for(UIView *aView in self.subviews){
        if([aView isFirstResponder]){
            return aView;
        } else {
            UIView *theOne = [aView findFirstResponder];
            if(theOne != nil){
                return theOne;
            }
        }
    }
    return nil;
}

- (void)removeAllSubviews
{
    for(UIView *aView in self.subviews){
        [aView removeFromSuperview];
    }
}

- (void)addDefaultBackgroundImage
{
    [self addDefaultBackgroundImage:UIViewContentModeTopLeft];    
}

- (void)addDefaultBackgroundImageFill
{
    [self addDefaultBackgroundImage:UIViewContentModeScaleToFill];
}

- (void)addDefaultBackgroundImage:(UIViewContentMode)contentMode
{
    UIImageView *backgroundImageView = [[UIImageView alloc]
                                        initWithImage:[UIImage imageNamed:@"background_light_noise.png"]];
    backgroundImageView.contentMode = contentMode;
    backgroundImageView.frame = self.bounds;
    backgroundImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    
    if([self isKindOfClass:[UITableView class]]){
        UITableView *tableView = (UITableView *)self;
        tableView.backgroundView = backgroundImageView;
    } else {
        [self insertSubview:backgroundImageView
                    atIndex:0];
    }
}

- (NSArray *)constraintsForAttribute:(NSLayoutAttribute)attribute
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstAttribute = %d", attribute];
    NSArray *filteredArray = [[self constraints] filteredArrayUsingPredicate:predicate];
    
    return filteredArray;
}

- (NSLayoutConstraint *)constraintForAttribute:(NSLayoutAttribute)attribute
{
    return [self constraintForAttribute:attribute withItem:nil];
}

- (NSLayoutConstraint *)constraintForAttribute:(NSLayoutAttribute)attribute withItem:(id)anItem
{
    NSArray *constraints = [self constraintsForAttribute:attribute];
    
    NSLayoutConstraint *rtnConstraint = nil;
    
    if([constraints count] > 0){
        
        if(anItem == nil){
            rtnConstraint = [constraints objectAtIndex:0];
        } else {
            for(NSLayoutConstraint *aConstraint in constraints){
                if(aConstraint.firstItem == anItem){
                    rtnConstraint = aConstraint;
                    break;
                }
            }
        }
    }
    
    return rtnConstraint;
}

- (NSArray *)constraintsForAttribute:(NSLayoutAttribute)attribute withItem:(id)anItem
{
    NSArray *constraints = [self constraintsForAttribute:attribute];
    
    if([constraints count] > 0){
        
        if(anItem == nil){
            return constraints;
        } else {
            NSMutableArray *rtnConstraints = [NSMutableArray array];
            for(NSLayoutConstraint *aConstraint in constraints){
                if(aConstraint.firstItem == anItem){
                    [rtnConstraints addObject:aConstraint];
                    break;
                }
            }
            return rtnConstraints;
        }
    } else {
        return nil;
    }
}

- (NSArray *)constraintsForAttribute:(NSLayoutAttribute)attribute withSecondItem:(id)anItem
{
    NSArray *constraints = [self constraintsForAttribute:attribute];
    
    if([constraints count] > 0){
        
        if(anItem == nil){
            return constraints;
        } else {
            NSMutableArray *rtnConstraints = [NSMutableArray array];
            for(NSLayoutConstraint *aConstraint in constraints){
                if(aConstraint.secondItem == anItem){
                    [rtnConstraints addObject:aConstraint];
                    break;
                }
            }
            return rtnConstraints;
        }
    } else {
        return nil;
    }
}

@end