//
//  NPValidation.h
//  RushOrder
//
//  Created by Conan on 11/23/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>

#define VALID [NPValidation sharedValidator]

@interface NPValidation : NSObject <UIAlertViewDelegate>

+ (NPValidation *)sharedValidator;

- (BOOL)validate:(NSObject *)validatedObject
         message:(NSString *)message;

- (BOOL)validate:(NSObject *)validatedObject
           title:(NSString *)title
         message:(NSString *)message;

- (BOOL)validateEmail:(NSObject *)validatedObject
              message:(NSString *)message;

- (BOOL)validateEmail:(NSObject *)validatedObject
                title:(NSString *)title
              message:(NSString *)message;

- (BOOL)validate:(NSObject *)validatedField
       condition:(BOOL)condition
         message:(NSString *)message;

- (BOOL)validate:(NSObject *)validatedObject
       condition:(BOOL)condition
           title:(NSString *)title
         message:(NSString *)message;

- (BOOL)validate:(NSObject *)obj;
- (BOOL)validate:(NSObject *)obj
      logMessage:(NSString *)logMessage;

- (BOOL)validate:(NSObject *)obj
       condition:(BOOL)condition
      logMessage:(NSString *)logMessage;
@end
