//
//  NSUserDefaults+utils.h
//  RushOrder
//
//  Created by Conan on 11/20/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const deviceTokenUserDefaultsKey;
extern NSString * const autoLoginUserDefaultsKey;
extern NSString * const lastTrieduserIdUserDefaultsKey;
extern NSString * const testDriveModeDefaultsKey;
extern NSString * const demoOnDefaultsKey;
extern NSString * const testOnDefaultsKey;
extern NSString * const HideCoinKey;
extern NSString * const adoptLastTipRate;
extern NSString * const adoptLastDeliveryTipRate;
extern NSString * const adoptLastTakeoutTipRate;
extern NSString * const tipRateLastUsed;
extern NSString * const tipRateLastUsedDelivery;
extern NSString * const tipRateLastUsedTakeout;
extern NSString * const customRequestSynced;
extern NSString * const DemoRestaurantOnChangedNotification;
extern NSString * const TestRestaurantOnChangedNotification;


@interface NSUserDefaults (utils)

@property (nonatomic, strong) NSString *deviceToken;
@property (nonatomic) BOOL autoLogin;
@property (nonatomic, weak) NSString *lastTriedUserId;

@property (nonatomic, getter = isDemoOn) BOOL demoOn;
@property (nonatomic, getter = isTestOn) BOOL testOn;

@property (nonatomic, getter = isPinSet) BOOL pinSet;
@property (nonatomic, getter = isVirtualAdded) BOOL virtualAdded;

@property (nonatomic, getter = isHideCoin) BOOL hideCoin;

@property (nonatomic) float tipRate;
@property (nonatomic) float tipTakeoutRate;
@property (nonatomic) float tipDeliveryRate;
@property (nonatomic) BOOL adoptLastTipRate;
@property (nonatomic) BOOL adoptLastDeliveryTipRate;
@property (nonatomic) BOOL adoptLastTakeoutTipRate;
@property (nonatomic) BOOL didAlertPushNotification;
@property (nonatomic) BOOL didUploadRecentAddresses;

@property (nonatomic) BOOL customRequestSynced;

@property (nonatomic) PCSerial lastAccessedNoticeId;

@property (nonatomic) NSString *emailAddress;
@property (nonatomic) BOOL autoMailingMe;

@property (nonatomic) NSString *custName;
@property (nonatomic) NSString *custPhone;

@property (nonatomic) BOOL emergencySignedOut;

@property (nonatomic) BOOL warnedServiceFee;

#pragma mark - main folding
@property (nonatomic) BOOL favoraiteCollapsed;
@property (nonatomic) BOOL visitedCollapsed;
@property (nonatomic) BOOL nearbyCollapsed;

#pragma mark - For Merchant
#if RUSHORDER_MERCHANT
@property (nonatomic) BOOL autoKitchenPrint;
@property (nonatomic) BOOL autoBillPrint;
@property (nonatomic) BOOL autoReceiptPrint;

@property (nonatomic) NSArray *receiptPrinters;
@property (nonatomic) BOOL walletCommercialClosed;

@property (nonatomic) NSInteger lastUsedInterval;
@property (nonatomic) NSInteger lastUsedDeliveryInterval;

@property (nonatomic) BOOL blinkWithNewOrder;
@property (nonatomic) BOOL directSwitchMenu;
@property (nonatomic) BOOL oneTouchLocationUpdate;
@property (nonatomic) BOOL showedIntroView;

@property (nonatomic) NSDate *dateQuerySettingDate;
@property (nonatomic) NSDate *startDate;
@property (nonatomic) NSDate *endDate;
@property (nonatomic) NSDate *lastNotificationTime;

@property (nonatomic) BOOL printWhenNewOrder;

@property (nonatomic) PCSerial lastNewPrintedNumber;
@property (nonatomic) PCSerial lastNewPrintedCartNumber;
@property (nonatomic) PCSerial lastPaymentPrintedNumber;



#endif



- (PCSerial)lastLogedInMerchantIdWithId:(NSString *)email;
- (void)setLastLogedInMerchantId:(PCSerial)lastLogedInMerchantId withId:(NSString *)email;
@end
