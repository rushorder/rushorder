//
//  UIViewController+utils.m
//  RushOrder
//
//  Created by Conan on 2/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "UIViewController+utils.h"
#import "PCNavigationController.h"

@implementation UIViewController (utils)

+ (id)viewControllerFromNib
{
    return [[self alloc] initWithNibName:NSStringFromClass(self)
                                  bundle:nil];
}


- (void)presentViewControllerInNavigation:(UIViewController *)viewControllerToPresent
                                 animated:(BOOL)flag
                               completion:(void (^)(void))completion
{
    PCNavigationController *navi = [[PCNavigationController alloc] initWithRootViewController:viewControllerToPresent];
    navi.navigationBar.barStyle = UIBarStyleDefault;
    navi.navigationBar.translucent = NO;
    navi.modalPresentationStyle = viewControllerToPresent.modalPresentationStyle;
    navi.modalTransitionStyle = viewControllerToPresent.modalTransitionStyle;
    [self presentViewController:navi
                       animated:flag
                     completion:completion];
}

- (BOOL)isRootViewController
{
    return (self.navigationController.rootViewController == self);
}

@end