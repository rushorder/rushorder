//
//  NSString+utils.m
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "NSString+utils.h"

@implementation NSString (utils)

+ (NSString *)sqlStringWithBool:(BOOL)value
{
    return value ? @"1" : @"0";
}

+ (NSString *)stringWithBool:(BOOL)value
{
    return value ? @"true" : @"false";
}

+ (NSString *)stringWithEmptyBool:(BOOL)value
{
    return value ? @"true" : @"";
}

+ (NSString *)stringWithInteger:(NSInteger)value
{
    return [self stringWithInteger:value allowZero:YES];
}

+ (NSString *)stringWithEmptyInteger:(NSInteger)value
{
    return [self stringWithInteger:value allowZero:NO];
}

+ (NSString *)stringWithInteger:(NSInteger)value allowZero:(BOOL)allowZero
{
    if(allowZero)
        return [self stringWithFormat:@"%ld", (long)value];
    else
        return (value > 0) ? [self stringWithFormat:@"%ld", (long)value] : @"";
}

+ (NSString *)stringWithLongLong:(int64_t)value
{
    return (value != 0.0f) ? [self stringWithFormat:@"%lld", value] : @"";
}

+ (NSString *)stringWithDouble:(double)value
{
    return (value != 0.0f) ? [self stringWithFormat:@"%lf", value] : @"";
}

+ (NSString *)stringWithFloat:(float)value
{
    return (value != 0.0f) ? [self stringWithFormat:@"%f", value] : @"";
}

- (CGSize) sizeWithFontOrAttributes:(UIFont *) font {
    if (OVER_IOS7) {
        NSDictionary *fontWithAttributes = @{NSFontAttributeName:font};
        return [self sizeWithAttributes:fontWithAttributes];
    } else {
        return [self sizeWithFont:font];
    }
}


+ (NSString *)issuerLogoImageName:(Issuer)issuer
{
    switch(issuer){
        case IssuerVisa:
        case IssuerVisaElectron:
            return @"Visa-card-2.jpg";
        case IssuerAmericanExpress:
            return @"american-express-logo.png";
        case IssuerJCB:
            return @"JCB_logo.png";
        case IssuerMasterCard:
            return @"mcard2.jpg";
        case IssuerDiscover:
            return @"discover3.jpg";
        case IssuerDinersClubCarteBlanche:
        case IssuerDinersClubEnRoute:
        case IssuerDinersClubInternational:
        case IssuerDinersClubUnitedStatesNCanada:
            return @"diners_club_logo.png";
            
        default:
            return nil;
            break;
    }
}

+ (NSString *)issuerName:(Issuer)issuer
{
    NSString *issuerName = nil;
    switch(issuer){
        case IssuerAmericanExpress:
            issuerName = @"American Express";
            break;
        case IssuerBankcard:
            issuerName = @"Bank card";
            break;
        case IssuerChinaUnionPay:
            issuerName = @"China UnionPay";
            break;
        case IssuerDinersClubCarteBlanche:
            issuerName = @"Diners Club Carte Blanche";
            break;
        case IssuerDinersClubEnRoute:
            issuerName = @"Diners Club enRoute";
            break;
        case IssuerDinersClubInternational:
            issuerName = @"Diners Club International";
            break;
        case IssuerDinersClubUnitedStatesNCanada:
            issuerName = @"Diners Club";
            break;
        case IssuerDiscover:
            issuerName = @"Discover Card";
            break;
        case IssuerInstaPayment:
            issuerName = @"InstaPayment";
            break;
        case IssuerJCB:
            issuerName = @"JCB";
            break;
        case IssuerLaser:
            issuerName = @"Laser";
            break;
        case IssuerMaestro:
            issuerName = @"Maestro";
            break;
        case IssuerMasterCard:
            issuerName = @"MasterCard";
            break;
        case IssuerSolo:
            issuerName = @"Solo";
            break;
        case IssuerSwitch:
            issuerName = @"Switch";
            break;
        case IssuerVisa:
            issuerName = @"Visa";
            break;
        case IssuerVisaElectron:
            issuerName = @"Visa Election";
            break;
        case IssuerTest:
            issuerName = @"Test";
            break;
        default:
            issuerName = @"";
            break;
    }
    
    return issuerName;
}

- (Issuer)cardIssuer
{
    if([self isEqualToString:@"Visa"]){
        return IssuerVisa;
    } else if([self  isEqualToString:@"MasterCard"]){
        return IssuerMasterCard;
    } else if([self  isEqualToString:@"American Express"]){
        return IssuerAmericanExpress;
    } else if([self  isEqualToString:@"Discover"]){
        return IssuerDiscover;
    } else if([self  isEqualToString:@"Diners Club"]){
        return IssuerDinersClubInternational;
    } else if([self  isEqualToString:@"JCB"]){
        return IssuerJCB;
    } else if([self  isEqualToString:@"Test"]){
        return IssuerTest;
    } else {
        return IssuerUnknown;
    }
}

- (BOOL)isValidEmail
{
    BOOL isValid = NO;
    
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    isValid = [emailTest evaluateWithObject:self];
    
    if(isValid){
        if([self hasSuffix:@".con"] || [self hasSuffix:@".clm"]){
            isValid = NO;
        }
    }
    
    return isValid;
}

- (NSDate *)dateWithYMD
{
    return [self dateFromFormatString:@"yyyy-MM-dd"];
}

- (NSDate *)date
{
    return [self dateFromFormatString:@"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
}

- (NSDate *)dateFromFormatString:(NSString *)dateFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = dateFormat;
    dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSDate *rtnDate = [dateFormatter dateFromString:self];
    return rtnDate;
}

- (BOOL)hasPrefixes:(NSArray *)prefixList
{
    for(NSString *prefix in prefixList){
        if([self hasPrefix:prefix]){
            return YES;
        }
    }
    
    return NO;
}

// Return substring from 0 to length
- (NSString *)substringWithLength:(NSInteger)length
{
    if([self length] > length){
        
        return [self substringToIndex:length];
        
    } else {
        
        return self;
        
    }
    return nil;
}

- (NSInteger)integerWithLength:(NSInteger)length
{
    NSString *substring = [self substringWithLength:length];
    if(substring == nil){
        return NSNotFound;
    } else {
        return [substring integerValue];
    }
}


- (NSString *)urlEncodedString
{
//    return [[[self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
//             stringByReplacingOccurrencesOfString:@":" withString:@"%3A"]
//            stringByReplacingOccurrencesOfString:@"," withString:@"%2C"];
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                               (CFStringRef)self,
                                                               NULL,
                                                               (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                               CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding)));
}

- (BOOL)isCaseInsensitiveEqual:(NSString *)other
{
    if(other == nil) return NO;
    
    return ([self compare:other
                  options:NSCaseInsensitiveSearch] == NSOrderedSame);
}

- (OrderType)orderType
{
    OrderType orderType = OrderTypeUnknown;
    
    if([self isEqualToString:@"cart"]){
        orderType = OrderTypeCart;
    } else if([self isEqualToString:@"togo"]){
        orderType = OrderTypePickup;
    } else if([self isEqualToString:@"takeout"]){
        orderType = OrderTypeTakeout;
    } else if([self isEqualToString:@"menu"]){
        orderType = OrderTypeOrderOnly;
    } else if([self isEqualToString:@"delivery"]){
        orderType = OrderTypeDelivery;
    } else { // pay
        orderType = OrderTypeBill;
    }
    
    return orderType;
}


@end
