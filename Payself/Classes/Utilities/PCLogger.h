//
//  PCLogger.h
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum _logLevel{
    LevelLog = 1,
    LevelWarning = 10,
    LevelError = 20
} LogLevel;

@interface PCLogger : NSObject

@property (nonatomic) LogLevel logLevel;
+ (PCLogger *)shared;

- (void)start;
- (void)logLevel:(LogLevel)level format:(NSString *)format args:(va_list)args;
@end

void PCWarning (NSString *format, ...);
void PCError (NSString *format, ...);
void PCLog (NSString *format, ...);
void PCLogl (LogLevel logLevel, NSString *format, ...);
