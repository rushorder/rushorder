//
//  UILabel+utils.h
//  RushOrder
//
//  Created by Conan on 2/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (utils)

@property (nonatomic, copy) NSString *fontName;
//@property (nonatomic, strong) NSString *attrText;

- (void)sizeToHeightFit;
- (void)sizeToHeightFitLimitHeight:(CGFloat)limitHeight;
@end
