//
//  UINavigationController+utils.m
//  RushOrder
//
//  Created by Conan on 10/7/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "UINavigationController+utils.h"

@implementation UINavigationController (utils)

- (UIViewController *)rootViewController
{
    NSArray *viewControllers = self.viewControllers;
    if([viewControllers count] > 0){
        return [viewControllers objectAtIndex:0];
    }
    return nil;
}

- (void)popToViewControllerClass:(Class)viewControllerClass
                        animated:(BOOL)animated
{
    for(UIViewController *viewController in self.viewControllers){
        if([viewController class] == viewControllerClass){
            [self popToViewController:viewController animated:animated];
            return;
        }
    }
}
@end
