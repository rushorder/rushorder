//
//  UIStoryboard+utils.m
//  RushOrder2
//
//  Created by Conan on 8/25/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import "UIStoryboard+utils.h"

@implementation UIStoryboard (utils)

+ (id)initialViewControllerFrom:(NSString *)storyboardName
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:storyboardName
                                                         bundle:nil];
    UIViewController *viewController = [storyBoard instantiateInitialViewController];
    return viewController;
}

+ (id)viewController:(NSString *)identifier
                from:(NSString *)storyboardName
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:storyboardName
                                                         bundle:nil];
    UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:identifier];
    
    return viewController;
}

@end
