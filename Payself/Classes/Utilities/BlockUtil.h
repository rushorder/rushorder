//
//  BlockUtil.h
//  RushOrder
//
//  Created by Conan on 12/10/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#ifndef RushOrder_BlockUtil_h
#define RushOrder_BlockUtil_h

#import "Order.h"
#import "Cart.h"

static NSComparator orderOrCartComparator = ^NSComparisonResult(id obj1, id obj2){
    NSDate *time1 = nil;
    NSDate *time2 = nil;
    
    typedef enum classType_{
        classtypeUnknown = 0,
        classtypeCart = 1,
        classtypeOrder,
    } ClassType;
    
    ClassType obj1Type = classtypeUnknown;
    ClassType obj2Type = classtypeUnknown;
    
    PCSerial uId1 = NSNotFound;
    PCSerial uId2 = NSNotFound;
    
    if([obj1 isKindOfClass:[Cart class]]){
        obj1Type = classtypeCart;
        time1 = [obj1 createdDate];
        uId1 = ((Cart *)obj1).cartNo;
    } else if ([obj1 isKindOfClass:[Order class]]){
        obj1Type = classtypeOrder;
        time1 = [obj1 orderDate];
        uId1 = ((Order *)obj1).orderNo;
    }
    
    if([obj2 isKindOfClass:[Cart class]]){
        obj2Type = classtypeCart;
        time2 = [obj2 createdDate];
        uId2 = ((Cart *)obj2).cartNo;
    } else if ([obj2 isKindOfClass:[Order class]]){
        obj2Type = classtypeOrder;
        time2 = [obj2 orderDate];
        uId2 = ((Order *)obj2).orderNo;
    }

    if(uId1 != 0 && uId2 != 0){
        if(obj1Type == obj2Type && uId1 == uId2){
            // Same object
            return NSOrderedSame;
        }
    }
    
    NSComparisonResult rtn = [time2 compare:time1];
    
    if(rtn == NSOrderedSame){
        if(obj1Type > obj2Type){
            rtn = NSOrderedAscending;
        } else if (obj1Type < obj2Type){
            rtn = NSOrderedDescending;
        } else {
            if(uId1 > uId2){
                rtn = NSOrderedAscending;
            } else if (uId1 < uId2){
                rtn = NSOrderedDescending;
            } else {
                rtn = NSOrderedSame; //Not happen case
            }
        }
    }
    
    return rtn;
};

#endif
