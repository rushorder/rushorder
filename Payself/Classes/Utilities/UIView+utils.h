//
//  UIView+utils.h
//  RushOrder
//
//  Created by Conan on 2/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (utils)

@property (nonatomic) CGPoint origin;
@property (nonatomic) CGSize size;
@property (nonatomic) CGFloat x;
@property (nonatomic) CGFloat y;
@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;

+ (id)view;
+ (id)viewWithNibName:(NSString *)nibName;
+ (id)viewWithNibName:(NSString *)nibName atIndex:(NSUInteger)objectIndex;

- (UIView *)findFirstResponder; // Return the first reponder in subviews hierachy

- (void)drawBorder;
- (void)drawBorderWithColor:(UIColor *)lineColor;

- (void)removeAllSubviews;

- (void)addDefaultBackgroundImage;
- (void)addDefaultBackgroundImageFill;

// NSConstraint
- (NSArray *)constraintsForAttribute:(NSLayoutAttribute)attribute;
- (NSLayoutConstraint *)constraintForAttribute:(NSLayoutAttribute)attribute;
- (NSLayoutConstraint *)constraintForAttribute:(NSLayoutAttribute)attribute withItem:(id)secondItem;
- (NSArray *)constraintsForAttribute:(NSLayoutAttribute)attribute withItem:(id)anItem;
- (NSArray *)constraintsForAttribute:(NSLayoutAttribute)attribute withSecondItem:(id)anItem;
@end
