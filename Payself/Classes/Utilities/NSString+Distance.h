//
//  NSString+Distance.h
//  RushOrder
//
//  Created by Conan on 3/25/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import <Foundation/Foundation.h>

#define METERS_TO_FEET  3.2808399
#define METERS_TO_MILES 0.000621371192
#define METERS_CUTOFF   1000
#define FEET_CUTOFF     0
#define FEET_IN_MILES   5280

@interface NSString (Distance)
+ (NSString *)stringWithDistanceInMile:(double)distance;
+ (NSString *)stringWithDistance:(double)distance;
+ (NSString *)stringWithDouble:(double)value;

@end