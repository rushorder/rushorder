//
//  NSObject+utils.h
//  RushOrder
//
//  Created by Conan on 2/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (utils)

@property (nonatomic, readonly) BOOL isUITextField;
@property (nonatomic, readonly) BOOL isUIButton;
@property (nonatomic, readonly) BOOL isNSArray;
@property (nonatomic, readonly) BOOL isNSDictionary;

@end
