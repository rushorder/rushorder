//
//  UITextField+utils.h
//  RushOrder
//
//  Created by Conan on 11/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (utils)

@property (nonatomic, copy) NSString *fontName;

@end
