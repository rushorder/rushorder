//
//  UIColor+utils.m
//  RushOrder
//
//  Created by Conan on 11/19/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import "UIColor+utils.h"

#define DEFAULT_TINT_COLOR  [UIColor colorWithR:21.0f G:96.0f B:76.0f]

@implementation UIColor (utils)
+ (UIColor *)colorWithR:(CGFloat)red G:(CGFloat)green B:(CGFloat)blue
{
    return [self colorWithR:red G:green B:blue A:1.0f];
}

+ (UIColor *)colorWithR:(CGFloat)red G:(CGFloat)green B:(CGFloat)blue A:(CGFloat)alpha
{
    return [self colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:alpha];
}

+ (UIColor *)lightBlueColor
{
    return [UIColor colorWithR:67.0f G:138.0f B:207.0f];
}

+ (UIColor *)moreLightGrayColor
{
    return [UIColor colorWithWhite:0.85f alpha:1.0f];
}

+ (UIColor *)alertCellTextLabelColorDarkGray
{
    return [UIColor colorWithR:132. G:135. B:142.];
}

+ (UIColor *)cellTextLabelColorDarkGray
{
    return [UIColor colorWithR:77. G:77. B:77.];
}

+ (UIColor *)cellTextLabelColorLightBlue
{
    return [UIColor colorWithR:65. G:150. B:255.];
}

+ (UIColor *)cellHighlightedColor
{
    return [UIColor colorWithR:235. G:235. B:235.];
}

+ (UIColor *)warningTextColor
{
    return [UIColor colorWithR:255 G:118 B:130];
}

+ (UIColor *)safeTextColor
{
    return [UIColor colorWithR:65 G:150 B:255];
}

+ (UIColor *)infoTextColor
{
    return [UIColor colorWithR:132 G:135 B:142];
}

+ (UIColor *)defaultTintColor
{
    return DEFAULT_TINT_COLOR;
}

+ (UIColor *)randomColor
{
    return [self randomColor:255];
}

+ (UIColor *)darkRandomColor
{
    return [self randomColor:100];
}

+ (UIColor *)randomColor:(NSInteger)colorDepth
{
    int red = arc4random() % 255;
    int blue = arc4random() % 255;
    int green = arc4random() % 255;
    
    return [self colorWithR:(CGFloat)red G:(CGFloat)green B:(CGFloat)blue];
}

+ (UIColor *)disabledColor
{
    return [UIColor colorWithR:255.0f
                             G:177.0f
                             B:126.0f];
}

+ (UIColor *)c1Color
{
    return [UIColor colorWithR:242. G:242. B:242.];
}

+ (UIColor *)c2Color
{
    return [UIColor colorWithR:218. G:218. B:218.];
}
+ (UIColor *)c3Color
{
    return [UIColor colorWithR:204. G:204. B:204.];
}
+ (UIColor *)c4Color
{
    return [UIColor colorWithR:163. G:163. B:163.];
}
+ (UIColor *)c5Color
{
    return [UIColor colorWithR:129. G:137. B:152.];
}
+ (UIColor *)c6Color
{
    return [UIColor colorWithR:96. G:103. B:118.];
}
+ (UIColor *)c7Color
{
    return [UIColor colorWithR:70. G:72. B:92.];
}
+ (UIColor *)c8Color
{
    return [UIColor colorWithR:0. G:190. B:32.];
}
+ (UIColor *)c9Color
{
    return [UIColor colorWithR:76. G:76. B:76.];
}
+ (UIColor *)c10Color
{
    return [UIColor colorWithR:53. G:53. B:53.];
}
+ (UIColor *)c11Color //Orange Color
{
    return [UIColor colorWithR:255.0 G:102.0 B:0.0];
}
+ (UIColor *)c12Color
{
    return [UIColor colorWithR:255 G:255 B:255];
}
+ (UIColor *)c13Color
{
    return [UIColor colorWithR:0 G:123 B:228];
}
+ (UIColor *)c14Color
{
    return [UIColor colorWithR:255 G:43 B:63];
}
+ (UIColor *)c15Color
{
    return [UIColor colorWithR:140 G:129 B:16];
}
+ (UIColor *)c16Color
{
    return [UIColor colorWithR:251 G:248 B:228];
}

@end
