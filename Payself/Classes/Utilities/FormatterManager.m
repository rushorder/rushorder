//
//  FormatterManager.m
//  RushOrder
//
//  Created by Conan on 2/24/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "FormatterManager.h"

@interface FormatterManager()

@end

@implementation FormatterManager

+ (FormatterManager *)sharedManager
{
    static FormatterManager *formatterManager;
    @synchronized(self){
        if(formatterManager == nil){
            formatterManager = [[FormatterManager alloc] init];
        }
    }
    
    return formatterManager;
    
}

- (id)init
{
    self = [super init];
    if(self != nil){
        NSMutableCharacterSet *numberSet = [[NSCharacterSet decimalDigitCharacterSet] mutableCopy];
        [numberSet formUnionWithCharacterSet:[NSCharacterSet whitespaceCharacterSet]];
        [numberSet formUnionWithCharacterSet:[NSCharacterSet characterSetWithCharactersInString:@"."]];
        self.nonNumberSet = [numberSet invertedSet];
    }
    return self;
}

- (NSNumberFormatter *)currencyFormatter
{
    if(_currencyFormatter == nil){
        
        _currencyFormatter = [NSNumberFormatter currencyFormatter];
        
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en-US"];
        [_currencyFormatter setLocale:locale];
        _currencyFormatter.negativePrefix = [NSString stringWithFormat:@"−%@", _currencyFormatter.currencySymbol];
        _currencyFormatter.negativeSuffix = @"";
        
    }
    return _currencyFormatter;
}

- (NSNumberFormatter *)currencyFormatterWithoutCent
{
    if(_currencyFormatterWithoutCent == nil){
        
        _currencyFormatterWithoutCent = [NSNumberFormatter currencyFormatterWithoutFraction:YES];
        
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en-US"];
        [_currencyFormatterWithoutCent setLocale:locale];
        _currencyFormatterWithoutCent.negativePrefix = [NSString stringWithFormat:@"−%@", _currencyFormatterWithoutCent.currencySymbol];
        _currencyFormatterWithoutCent.negativeSuffix = @"";
        
    }
    return _currencyFormatter;
}



- (NSNumberFormatter *)numberFormatter
{
    if(_numberFormatter == nil){
        _numberFormatter = [[NSNumberFormatter alloc] init];
        [_numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    }
    return _numberFormatter;
}

- (NSNumberFormatter *)decimalFormatter
{
    if(_decimalFormatter == nil){
        _decimalFormatter = [[NSNumberFormatter alloc] init];
        [_decimalFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [_decimalFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    }
    return _decimalFormatter;
}

- (NSNumberFormatter *)percentFormatter
{
    if(_percentFormatter == nil){
        _percentFormatter = [[NSNumberFormatter alloc] init];
        _percentFormatter.numberStyle = NSNumberFormatterPercentStyle;
        _percentFormatter.allowsFloats = YES;
        _percentFormatter.maximum = [NSNumber numberWithInteger:1];
        _percentFormatter.maximumFractionDigits = 4;
    }
    return _percentFormatter;
}

- (CardNumberFormatter *)cardNumberFormatter
{
    if(_cardNumberFormatter == nil){
        _cardNumberFormatter = [[CardNumberFormatter alloc] init];
    }
    return _cardNumberFormatter;
}


#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(PCCurrencyTextField *)textField
{
    id<PCCurrencyTextFieldDelegate> forwardedDelegate = textField.forwardDelegate;
    if([forwardedDelegate respondsToSelector:@selector(textFieldShouldBeginEditing:)]){
        return [forwardedDelegate textFieldShouldBeginEditing:textField];
    } else {
        return YES;
    }
}

- (void)textFieldDidBeginEditing:(PCCurrencyTextField *)textField
{
    id<PCCurrencyTextFieldDelegate> forwardedDelegate = textField.forwardDelegate;
    if([forwardedDelegate respondsToSelector:@selector(textFieldDidBeginEditing:)]){
        [forwardedDelegate textFieldDidBeginEditing:textField];
    } else {

    }
}

- (BOOL)textFieldShouldEndEditing:(PCCurrencyTextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(PCCurrencyTextField *)textField
{
}

- (BOOL)textFieldShouldClear:(PCCurrencyTextField *)textField
{
    [textField reset];
    
    id<PCCurrencyTextFieldDelegate> forwardedDelegate = textField.forwardDelegate;
    if([forwardedDelegate respondsToSelector:@selector(textFieldShouldClear:)]){
        return [forwardedDelegate textFieldShouldClear:textField];
    } else {
        return NO;
    }
}

- (BOOL)textFieldShouldReturn:(PCCurrencyTextField *)textField
{
    return YES;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(PCCurrencyTextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([textField isKindOfClass:[PCCurrencyTextField class]]){
        
        BOOL result = NO; //default to reject
        
        if([string length] == 0){ //backspace
            result = YES;
        }
        else{
            if([string stringByTrimmingCharactersInSet:self.nonNumberSet].length > 0){
                result = YES;
            }
        }
        
        //here we deal with the UITextField on our own
        if(result){
            //grab a mutable copy of what's currently in the UITextField
            NSMutableString* mstring = [[textField text] mutableCopy];
            if([mstring length] == 0){
                //special case...nothing in the field yet, so set a currency symbol first
                [mstring appendString:[[NSLocale currentLocale] objectForKey:NSLocaleCurrencySymbol]];
                
                //now append the replacement string
                [mstring appendString:string];
            }
            else{
                //adding a char or deleting?
                if([string length] > 0){
                    if([string isEqualToString:@"."]){
                        
                    } else {
                        [mstring insertString:string atIndex:range.location];
                    }
                }
                else {
                    //delete case - the length of replacement string is zero for a delete
                    [mstring deleteCharactersInRange:range];
                }
            }
            
            //to get the grouping separators properly placed
            //first convert the string into a number. The function
            //will ignore any grouping symbols already present -- NOT in iOS4!
            //fix added below - remove locale specific currency separators first
            NSNumber* number = [self.currencyFormatter numberFromString:mstring];
            
            
            NSNumber *adjustedNumber = nil;
            if([string length] > 0){
                adjustedNumber = [[NSNumber alloc] initWithFloat:[number floatValue] * (float)([string length] * 10)];
            } else {
                if(range.length > 0){
                    adjustedNumber = [[NSNumber alloc] initWithFloat:[number floatValue] / (float)(range.length * 10)];
                }
            }
            
            if(adjustedNumber == nil) adjustedNumber = number;
            //now format the number back to the proper currency string
            //and get the grouping separators added in and put it in the UITextField
            [textField setText:[self.currencyFormatter stringFromNumber:adjustedNumber]];
        }
        
        
        [textField sendMessageToTargets];
        
        
        //always return no since we are manually changing the text field
        return NO;
    } else {
        BOOL result = NO; //default to reject
        
        if([string length] == 0){ //backspace
            result = YES;
        }
        else{
            if([string stringByTrimmingCharactersInSet:self.nonNumberSet].length > 0){
                result = YES;
            }
        }
        
        if(result){

            NSNumber* number = [self.numberFormatter numberFromString:[textField text]];
            
            NSMutableString* mstring = [[number stringValue] mutableCopy];
            
            if(mstring == nil){
                mstring = [NSMutableString string];
            }
            
            //adding a char or deleting?
            if([string length] > 0 && range.length == 0){
                [mstring insertString:string atIndex:range.location];
            }
            else {
                
                
            }
            
            if(range.length > 0){
                [mstring deleteCharactersInRange:range];
            }
            
            number = [NSNumber numberWithInteger:[mstring integerValue]];
            
            [textField setText:[self.numberFormatter stringFromNumber:number]];
        }
        
        
        [textField sendMessageToTargets];
        
        
        //always return no since we are manually changing the text field
        return NO;
    }
}
@end
