//
//  UIStoryboardSegue+utils.h
//  RushOrder
//
//  Created by Conan on 9/16/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboardSegue (utils)

- (id)isDestClass:(Class)aClass;
@end
