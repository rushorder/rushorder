//
//  NSNumberFormatter+utils.h
//  RushOrder
//
//  Created by Conan on 2/24/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumberFormatter (utils)

+ (NSNumberFormatter *)currencyFormatter;
+ (NSNumberFormatter *)currencyFormatterWithoutFraction:(BOOL)withoutFraction;
@end
