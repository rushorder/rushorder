//
//  NSNumber+utils.h
//  RushOrder
//
//  Created by Conan on 2/24/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (utils)

@property (nonatomic, readonly) PCSerial serialValue;

+ (NSNumber *)numberWithCurrency:(PCCurrency)currency;
+ (NSNumber *)numberWithSerial:(PCSerial)serial;

- (NSString *)currencyString;
- (NSString *)currencyStringWithoutCent;
- (NSString *)percentString;
- (NSString *)pointString;
- (NSString *)decimalString;
@end