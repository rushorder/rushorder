//
//  NSNumber+utils.m
//  RushOrder
//
//  Created by Conan on 2/24/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "NSNumber+utils.h"
#import "FormatterManager.h"

@implementation NSNumber (utils)

+ (NSNumber *)numberWithCurrency:(PCCurrency)currency
{
    return [NSNumber numberWithInteger:currency];
}

+ (NSNumber *)numberWithSerial:(PCSerial)serial
{
    return [NSNumber numberWithLongLong:serial];
}

- (NSString *)currencyString
{
    NSNumber *number = [NSNumber numberWithFloat:[self integerValue] / 100.0f];
    NSString *string = [FORMATTER.currencyFormatter stringFromNumber:number];
    
    return string;
}

- (NSString *)currencyStringWithoutCent
{
    float dollorValue = [self integerValue] / 100.0f;
    
    NSNumber *number = [NSNumber numberWithFloat:dollorValue];
    
    NSString *string = nil;
    string = [[FORMATTER currencyFormatter] stringFromNumber:number];
    
    if((dollorValue - ([self integerValue] / 100)) > 0){
        
    } else {
        string = [string substringToIndex:[string length]-3];
    }

    return string;
}

- (NSString *)percentString
{
    NSNumber *number = [NSNumber numberWithFloat:([self floatValue] / 100.0f)];
    
    NSString *string = [FORMATTER.percentFormatter stringFromNumber:number];
    
    return string;
}

- (NSString *)pointString
{
    NSNumber *number = [NSNumber numberWithInteger:[self integerValue]];
    NSString *string = [NSString stringWithFormat:@"%@",[FORMATTER.decimalFormatter stringFromNumber:number]];
    return string;
    
//    float pointValue = [self integerValue] / 100.0f;
//    
//    NSNumber *number = [NSNumber numberWithFloat:pointValue];
//    
//    NSString *string = nil;
//    string = [[FORMATTER decimalFormatter] stringFromNumber:number];
//    
//    return string;
}

- (NSString *)decimalString
{
    NSNumber *number = [NSNumber numberWithInteger:[self integerValue]];
    NSString *string = [FORMATTER.decimalFormatter stringFromNumber:number];
    return string;
}

- (PCSerial)serialValue
{
    return [self longLongValue];
}

@end