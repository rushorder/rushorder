//
//  UITextView+utils.h
//  RushOrder
//
//  Created by Conan on 11/26/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (utils)

@property (nonatomic, copy) NSString *fontName;

@end