//
//  UIBarButtonItem+utils.m
//  RushOrder
//
//  Created by Conan on 19/11/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import "UIBarButtonItem+utils.h"
#import "UIColor+utils.h"
#import "UIButton+utils.h"

#define MINIMUM_BUTTON_WIDTH                30.0f
#define BUTTON_HEIGHT                       33.0f
#define BUTTON_TITLE_FONT_SIZE              15.0f

#define MARGIN_HORIZONTAL_BAR_BUTTON_ITEM   10.0f //Including default UIBarButtonItem's margin (5.0f)


@implementation UIBarButtonItem (utils)

+ (UIBarButtonItem *)barButtonItemWithTitle:(NSString *)title 
                                     target:(id)target 
                                     action:(SEL)action
{
    return [self barButtonItemWithTitle:title
                                 target:target
                                 action:action
                               backType:NPBarButtonBackTypeAppDefault];
}

+ (UIBarButtonItem *)barButtonItemWithTitle:(NSString *)title 
                                     target:(id)target 
                                     action:(SEL)action
                                   backType:(NPBarButtonBackType)backType
{
    UIImage *backButtonImageView = nil;
    UIImage *buttonImageView = nil;
    
    if(backType != NPBarButtonBackTypeClear){
//        backButtonImageView = [UIImage imageNamed:BARBUTTON_BACK_IMAGE_BACK];
        buttonImageView = [UIImage imageNamed:BARBUTTON_BACK_IMAGE];
    }
    
    if(backType == NPBarButtonBackTypeBack){
//        backButtonImageView = [UIImage imageNamed:BARBUTTON_BACK_IMAGE_BACK];
    } else if(backType == NPBarButtonBackTypeAppDefault){
        buttonImageView = [UIImage imageNamed:BARBUTTON_BACK_IMAGE];
    }
    
    
    if(backType == NPBarButtonBackTypeDefault ||
       (backType == NPBarButtonBackTypeAppDefault && buttonImageView == nil)){
        
        return [[self alloc ]initWithTitle:title
                                     style:UIBarButtonItemStylePlain
                                    target:target
                                    action:action];
        
    } else if(backType == NPBarButtonBackTypeDefaultArrow ||
              (backType == NPBarButtonBackTypeBack && backButtonImageView == nil)){
        
        return [[self alloc ]initWithTitle:title
                                     style:UIBarButtonItemStylePlain
                                    target:target
                                    action:action];
        
    }
//    return [[[UIBarButtonItem alloc] initWithTitle:title
//                                             style:UIBarButtonItemStyleBordered 
//                                            target:target
//                                            action:action] autorelease];
    
    CGSize titleSize = [title sizeWithFontOrAttributes:[UIFont fontWithName:@"HelveticaNeue-Light" size:BUTTON_TITLE_FONT_SIZE]];
    

    CGFloat additionalMargin = 0.0f;
    if(backType == NPBarButtonBackTypeBack)
        additionalMargin = 13.0f;
    CGFloat buttonWidth = MAX(titleSize.width + ((MARGIN_HORIZONTAL_BAR_BUTTON_ITEM - 1.0f) * 2) + additionalMargin, MINIMUM_BUTTON_WIDTH);
    
    UIButton *customButton = [UIButton buttonWithType:UIButtonTypeCustom];

    customButton.frame = CGRectMake(0.0f,
                                    0.0f,
                                    buttonWidth,
                                    BUTTON_HEIGHT);
    [customButton setTitle:title
                  forState:UIControlStateNormal];
    
    customButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:BUTTON_TITLE_FONT_SIZE];
    
    UIControlState stateForResizing = UIControlStateNormal;
    
    // Title color
    [customButton setTitleColor:[UIColor c12Color]
                       forState:UIControlStateNormal];
    [customButton setTitleColor:[UIColor disabledColor]
                       forState:UIControlStateDisabled];
    [customButton setTitleColor:[UIColor c3Color]
                       forState:UIControlStateHighlighted];
    
    customButton.adjustsImageWhenDisabled = YES;
    
    switch(backType){
        case NPBarButtonBackTypeBack:{
            if(backButtonImageView != nil){
                [customButton setBackgroundImage:backButtonImageView
                                        forState:UIControlStateNormal];
            } else {

            }
            
            if(BARBUTTON_BACK_IMAGE_BACK_PRESSED != nil){
                [customButton setBackgroundImage:[UIImage imageNamed:BARBUTTON_BACK_IMAGE_BACK_PRESSED]
                                        forState:UIControlStateHighlighted];
                stateForResizing |= UIControlStateHighlighted;
            }
            
            [customButton applyResizableImage:UIEdgeInsetsMake(15.0f, 13.0f, 15.0f, 9.0f)
                                     forState:stateForResizing];
            
            break;
        }
        case NPBarButtonBackTypeAppDefault:
        default:
            // Background image
            [customButton setBackgroundImage:buttonImageView
                                    forState:UIControlStateNormal];
            if(BARBUTTON_BACK_IMAGE_PRESSED != nil){
                [customButton setBackgroundImage:[UIImage imageNamed:BARBUTTON_BACK_IMAGE_PRESSED]
                                        forState:UIControlStateHighlighted];
                
                stateForResizing |= UIControlStateHighlighted;
            }
            
            [customButton applyResizableImageFromCenterForState:stateForResizing];
            break;
    }
//    customButton.contentEdgeInsets = UIEdgeInsetsMake(customButton.contentEdgeInsets.top - 1.0f,
//                                                      customButton.contentEdgeInsets.left,
//                                                      customButton.contentEdgeInsets.bottom,
//                                                      customButton.contentEdgeInsets.right);
    customButton.titleEdgeInsets = UIEdgeInsetsMake(1.0f, 0.0f, 0.0f, 0.0f);
    [customButton addTarget:target
                     action:action
           forControlEvents:UIControlEventTouchUpInside];
//    customButton.autoresizingMask = UIViewAutoresizingNone;
//
//    UIView *adjustingContainerView = [[UIView alloc] initWithFrame:CGRectMake(0,
//                                                                              0,
//                                                                              customButton.bounds.size.width + 4.0f,
//                                                                              customButton.bounds.size.height + 2.0f)];
//    [adjustingContainerView addSubview:customButton];
    return [[UIBarButtonItem alloc] initWithCustomView:customButton];
}

+ (UIBarButtonItem *)barButtonItemWithImage:(UIImage *)normalImage
                                     target:(id)target
                                     action:(SEL)action
                                   backType:(NPBarButtonBackType)backType
{
    NSString *backgroundImage = nil;
    NSString *backgroundImagePressed = nil;
    if(backType != NPBarButtonBackTypeClear){
        backgroundImage = BARBUTTON_BACK_IMAGE;
        backgroundImagePressed = BARBUTTON_BACK_IMAGE_PRESSED;
    }
    
       
    return [self barButtonItemWithImage:normalImage
                        backgroundImage:backgroundImage
             highlightedBackgroundImage:backgroundImagePressed
                                 target:target
                                 action:action
                               backType:backType];
}

+ (UIBarButtonItem *)barButtonItemWithImage:(UIImage *)normalImage
                            backgroundImage:(NSString *)backImageName
                 highlightedBackgroundImage:(NSString *)highlightedBackImageName
                                     target:(id)target
                                     action:(SEL)action
                                   backType:(NPBarButtonBackType)backType
{
    CGFloat buttonWidth = MAX(normalImage.size.width + ((MARGIN_HORIZONTAL_BAR_BUTTON_ITEM - 5.0f) * 2), MINIMUM_BUTTON_WIDTH);
    
    if([NSStringFromSelector(action) isEqual:@"searchButtonTouched:"]){
        buttonWidth = 33.0f;
    }
    
    UIButton *customButton = [UIButton buttonWithType:UIButtonTypeCustom];
    customButton.frame = CGRectMake(0.0f,
                                    0.0f,
                                    buttonWidth,
                                    BUTTON_HEIGHT);
    
    UIControlState stateForResizing = UIControlStateNormal;
    customButton.adjustsImageWhenDisabled = YES;
//    [customButton drawBorder];
    
    [customButton setImage:normalImage
                  forState:UIControlStateNormal];
    [customButton setBackgroundImage:[UIImage imageNamed:backImageName]
                            forState:UIControlStateNormal];
    if(highlightedBackImageName != nil){
        [customButton setBackgroundImage:[UIImage imageNamed:highlightedBackImageName]
                                forState:UIControlStateHighlighted];
        stateForResizing |= UIControlStateHighlighted;
        customButton.adjustsImageWhenHighlighted = NO;
    } else {
        customButton.adjustsImageWhenHighlighted = YES;
    }
    
    [customButton applyResizableImageFromCenterForState:stateForResizing];
    
    
    [customButton addTarget:target
                     action:action
           forControlEvents:UIControlEventTouchUpInside];

    return [[UIBarButtonItem alloc] initWithCustomView:customButton];
}

+ (UIBarButtonItem *)barButtonItemWithImage:(UIImage *)normalImage
                           highlightedImage:(UIImage *)highlightedImage
                                     target:(id)target
                                     action:(SEL)action
                                   backType:(NPBarButtonBackType)backType
{
    UIBarButtonItem *mediumBarButtonItem =
    [self barButtonItemWithImage:normalImage
                          target:target
                          action:action
                        backType:backType];
    
    UIButton *customButton = (UIButton *)mediumBarButtonItem.customView;
    [customButton setImage:highlightedImage forState:UIControlStateHighlighted];
    
    return mediumBarButtonItem;
}

+ (UIBarButtonItem *)barButtonSystemItem:(UIBarButtonSystemItem)systemItem
{
    return [self barButtonSystemItem:systemItem
                              target:nil
                              action:NULL];
}

+ (UIBarButtonItem *)barButtonSystemItem:(UIBarButtonSystemItem)systemItem
                                  target:(id)target
                                  action:(SEL)action
{
    return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:systemItem
                                                          target:target
                                                          action:action];
}

+ (UIBarButtonItem *)barButtonItemWithCustomView:(UIView *)customView
{
    return [[UIBarButtonItem alloc] initWithCustomView:customView];
}

+ (UIBarButtonItem *)dummyBarButtonItem
{
    UIBarButtonItem *mediumBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:
                                            [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                                     0,
                                                                                     MINIMUM_BUTTON_WIDTH,
                                                                                      BUTTON_HEIGHT)]];
    return mediumBarButtonItem;
}

+ (UIBarButtonItem *)barButtonNovaItem:(NPBarButtonItem)NPBarButtonItem
                                target:(id)target
                                action:(SEL)action
{
    UIImage *customImage = nil;
    UIImage *customHighlightedImage = nil;
    NSString *customTitle = nil;
    
//    NPBarButtonBackType backType = NPBarButtonBackTypeDefault;
    NPBarButtonBackType backType = NPBarButtonBackTypeAppDefault;
    
    switch(NPBarButtonItem){
        case NPBarButtonItemBackOrange:
            customImage = [UIImage imageNamed:@"button_back_orange"];
            backType = NPBarButtonBackTypeClear;
            break;
        case NPBarButtonItemBack:
            customImage = [UIImage imageNamed:@"button_back"];
            customHighlightedImage = [UIImage imageNamed:@"button_back_h"];
            backType = NPBarButtonBackTypeClear;
            break;
        case NPBarButtonItemHome:
            customImage = [UIImage imageNamed:@"button_home"];
            customHighlightedImage = [UIImage imageNamed:@"button_home_h"];
            backType = NPBarButtonBackTypeClear;
            break;
        case NPBarButtonItemSetting:
            customImage = [UIImage imageNamed:@"icon_setting_gear"];
            break;            
        case NPBarButtonItemLeftMenu:
            customImage = [UIImage imageNamed:@"button_slidemenu"];
            customHighlightedImage = [UIImage imageNamed:@"button_slidemenu_h"];
            backType = NPBarButtonBackTypeClear;
            break;
        case NPBarButtonItemMap:
            customImage = [UIImage imageNamed:@"button_map"];
            customHighlightedImage = [UIImage imageNamed:@"button_map_h"];
            backType = NPBarButtonBackTypeClear;
            break;
        case NPBarButtonItemSearch:
            customImage = [UIImage imageNamed:@"button_search"];
            customHighlightedImage = [UIImage imageNamed:@"button_search_h"];
            backType = NPBarButtonBackTypeClear;
            break;
        case NPBarButtonItemCart:
            customImage = [UIImage imageNamed:@"button_cart"];
            customHighlightedImage = [UIImage imageNamed:@"button_cart_h"];
            backType = NPBarButtonBackTypeClear;
            break;
        case NPBarButtonItemPlus:
            customImage = [UIImage imageNamed:@"button_plus_white"];
            customHighlightedImage = [UIImage imageNamed:@"button_plus_white_h"];
            backType = NPBarButtonBackTypeClear;
            break;
        case NPBarButtonItemReceipt:
            customImage = [UIImage imageNamed:@"button_icon_receipt"];
            customHighlightedImage = [UIImage imageNamed:@"button_icon_receipt_h"];
            backType = NPBarButtonBackTypeClear;
            break;
        case NPBarButtonEmail:
            customImage = [UIImage imageNamed:@"button_email"];
            customHighlightedImage = [UIImage imageNamed:@"button_email_h"];
            backType = NPBarButtonBackTypeClear;
            break;
        case NPBarButtonItemWrite:
            customImage = [UIImage imageNamed:@"button_item_write"];
            break;
        case NPBarButtonItemDoneStressed:
        case NPBarButtonItemDone:
            customTitle = NSLocalizedString(@"Done",nil);
            break;
        case NPBarButtonItemNextStressed:
        case NPBarButtonItemNext:
            customTitle = NSLocalizedString(@"Next",nil);
            break;
        case NPBarButtonItemCancel:
            customTitle = NSLocalizedString(@"Cancel",nil);
            break;
        case NPBarButtonItemEdit:
            customTitle = NSLocalizedString(@"Edit",nil);
            break;
        case NPBarButtonItemAdd:
            customTitle = NSLocalizedString(@"Add",nil);
            break;
        case NPBarButtonItemClose:
            customTitle = NSLocalizedString(@"Close",nil);
            break;
        case NPBarButtonItemSave:
            customTitle = NSLocalizedString(@"Save",nil);
            break;
        case NPBarButtonItemConfirm:
            customTitle = NSLocalizedString(@"Confirm",nil);
            break;
        case NPBarButtonItemRegistration:
            customTitle = NSLocalizedString(@"Regist",nil);
            break;
        default:
            PCError(@"Cannot find appropriate NPBarButtonItem");
            return nil;
            break;
    }
    
    if(customImage == nil){
        return [self barButtonItemWithTitle:customTitle
                                     target:target
                                     action:action
                                   backType:backType];
    } else {
        if(customHighlightedImage == nil){
            return [self barButtonItemWithImage:customImage
                                         target:target
                                         action:action
                                       backType:backType];
        } else {
            return [self barButtonItemWithImage:customImage
                               highlightedImage:customHighlightedImage
                                         target:target
                                         action:action
                                       backType:backType];
        }
    }
}

- (UIButton *)button
{
    // self.customView is for fix positioning issue.
    if([self.customView isKindOfClass:[UIButton class]]){
        return (UIButton *)self.customView;
    } else {
        if([[self.customView subviews] count] > 0){
            UIButton *aButton = (UIButton *)[self.customView.subviews objectAtIndex:0];
            if([aButton isKindOfClass:[UIButton class]]){
                return aButton;
            } else {
                return nil;
            }
        } else {
            return nil;
        }
    }
}
@end


