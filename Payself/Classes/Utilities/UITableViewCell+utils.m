//
//  UITableViewCell+utils.m
//  RushOrder
//
//  Created by Conan on 2/22/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "UITableViewCell+utils.h"

@implementation UITableViewCell (utils)

+ (UITableViewCell *)cell
{
    return [self cellWithNibName:NSStringFromClass(self)];
}

+ (UITableViewCell *)cellWithNibName:(NSString *)nibName
{
    return [self cellWithNibName:nibName atIndex:0];
}

+ (UITableViewCell *)cellWithNibName:(NSString *)nibName atIndex:(NSUInteger)objectIndex
{
    NSArray *topLevelObject = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    UITableViewCell *rtnTableViewCell = (UITableViewCell *)[topLevelObject objectAtIndex:objectIndex];
#if DEBUG
    if(rtnTableViewCell.reuseIdentifier == nil){
        PCWarning(@"TableViewCell has no identifer!!!! with Nib(Xib)File!! in called tableViewCellWithNibName:atIndex:. nibName is %@",nibName);
    }
#endif
    return rtnTableViewCell;
}

- (IBAction)anyButtonTouched:(UIButton *)button
{
    UITableView *tableView = self.tableView;
    
    if(OVER_IOS7){
        tableView = (UITableView *)self.superview.superview;
    } else {
        tableView = (UITableView *)self.superview;
    }
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:self];
    
    if([tableView.delegate respondsToSelector:@selector(tableView:didTouchAnyButton:atIndexPath:)]){
        [((id <UITableViewButtonDelegate>)tableView.delegate) tableView:tableView
                                                      didTouchAnyButton:button
                                                            atIndexPath:indexPath];
    }
}

- (UITableView *)tableView
{
    UITableView *tableView = (UITableView *)self.superview;
    
    if([tableView isKindOfClass:[UITableView class]]){
        return tableView;
    } else {
        tableView = (UITableView *)self.superview.superview;
        if([tableView isKindOfClass:[UITableView class]]){
            return tableView;
        } else {
            return nil;
        }
    }
}

- (NSIndexPath *)indexPath
{
    // Find current position
    return [self.tableView indexPathForCell:self];
}

@end
