//
//  NSArray+utils.h
//  RushOrder
//
//  Created by Conan on 4/1/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (utils)

- (NSArray *)listData;
- (NSArray *)data;
- (NSInteger)errorCode;

- (NSString *)firstName;
- (NSString *)lastName;

@end
