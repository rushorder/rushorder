//
//  NPValidation.m
//  RushOrder
//
//  Created by Conan on 11/23/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import "NPValidation.h"
@interface NPValidation()
{
    __strong NSMutableArray *validatedObjectStack;
}
@end

@implementation NPValidation

+ (NPValidation *)sharedValidator
{
    static NPValidation *sharedValidator = nil;
    
    @synchronized(self){
        if(sharedValidator == nil){
            sharedValidator = [[NPValidation alloc] init];
        }
    }
    
    return sharedValidator;
}

- (id)init
{
    self = [super init];
    if(self != nil){
        validatedObjectStack = [NSMutableArray array];
    }
    return self;
}

- (BOOL)validate:(NSObject *)validatedObject
         message:(NSString *)message
{
    return [self validate:validatedObject
                condition:([((UITextField *)validatedObject).text length] > 0)
                    title:UNDEFINED_ALERT_TITLE
                  message:message];
}

- (BOOL)validate:(NSObject *)validatedObject
           title:(NSString *)title
         message:(NSString *)message
{
    // Current textView, textField
    return [self validate:validatedObject
                condition:([((UITextField *)validatedObject).text length] > 0)
                    title:title
                  message:message];

    return NO;
}

- (BOOL)validateEmail:(NSObject *)validatedObject
              message:(NSString *)message
{
    return [self validateEmail:validatedObject
                         title:UNDEFINED_ALERT_TITLE
                       message:message];
}

- (BOOL)validateEmail:(NSObject *)validatedObject
                title:(NSString *)title
              message:(NSString *)message
{
    // Current textView, textField
    return [self validate:validatedObject
                condition:(([((UITextField *)validatedObject).text length] > 0) && [((UITextField *)validatedObject).text isValidEmail])
                    title:title
                  message:message];
    
    return NO;
}

- (BOOL)validate:(NSObject *)validatedObject
       condition:(BOOL)condition
         message:(NSString *)message
{
    return [self validate:validatedObject
                condition:condition
                    title:UNDEFINED_ALERT_TITLE
                  message:message];
}

- (BOOL)validate:(NSObject *)validatedObject
       condition:(BOOL)condition
           title:(NSString *)title
         message:(NSString *)message
{
    if(!condition){
        [validatedObjectStack addObject:validatedObject];
        [UIAlertView alertWithTitle:title
                            message:message
                           delegate:self];
        return NO;
    }
    return YES;
}

- (BOOL)validate:(NSObject *)obj
{
    return [self validate:obj logMessage:nil];
}

- (BOOL)validate:(NSObject *)obj logMessage:(NSString *)logMessage
{
    BOOL conditionResult = NO;
    
    if([obj isKindOfClass:[NSString class]]){
        conditionResult = [((NSString *)obj) length] > 0;
    } else {
        conditionResult = obj != nil;
    }
    
    return [self validate:obj
                condition:conditionResult
               logMessage:logMessage];
}

- (BOOL)validate:(NSObject *)obj
       condition:(BOOL)condition
      logMessage:(NSString *)logMessage
{
    if(!condition){
        PCError(@"NPValidation Error with message : %@", logMessage);
        return NO;
    }
    return YES;
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex; // before animation and hiding view
{
    NSObject *lastValidationObject = [validatedObjectStack lastObject];
    if([lastValidationObject respondsToSelector:@selector(becomeFirstResponder)])
        [(UIResponder *)lastValidationObject becomeFirstResponder];
    [validatedObjectStack removeLastObject];
}

@end
