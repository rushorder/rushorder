//
//  NSDate+utils.h
//  RushOrder
//
//  Created by Conan on 2/24/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (utils)

- (NSString *)secondsString;
- (NSString *)shortDateMinutesString;
- (NSString *)minutesString;
- (NSString *)timeString;
- (NSString *)hourMinuteString;
- (NSString *)stringForHumanReadableFormat;
- (NSString *)dateStringForHumanReadableFormat;
- (NSString *)gmtDateStringForHumanReadableFormat;

- (NSString *)stringWithDotFormat;
- (NSString *)stringWithDashedFormat;
- (NSString *)dateStringWithDashedFormat;
- (NSString *)gmtDateStringWithDashedFormat;
- (NSString *)dateHourStringWithDashedFormat;
- (NSString *)dateStringWithDashedFormatGMT;
- (NSString *)dateStringWithDashedFormatGMTWithZone;
- (NSString *)dateStringWithDotFormat;
- (NSString *)weekString;
- (NSString *)monthString;
- (NSString *)localizedTimeFormat;
- (NSDate *)dateWithZero;
- (NSDate *)dateInHour:(NSInteger)hour;
- (NSDate *)gmtDateInHour:(NSInteger)hour;
- (instancetype)rount5dateByAddingTimeInterval:(NSTimeInterval)ti;
@end
