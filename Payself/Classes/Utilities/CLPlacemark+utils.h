//
//  CLPlacemark+utils.h
//  RushOrder
//
//  Created by Conan on 12/16/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@interface CLPlacemark (utils)

@property (nonatomic, readonly) NSString *address;

@end
