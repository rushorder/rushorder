//
//  NSDictionary+utils.m
//  RushOrder
//
//  Created by Conan on 11/23/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import "NSDictionary+utils.h"

NSString * const ResponseErrorCodeKey = @"error_code";

@implementation NSDictionary (utils)

- (NSMutableString *)urlString
{
    NSMutableString *tempUrlString = [NSMutableString string];
    [self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if([tempUrlString length] > 0) [tempUrlString appendString:@"&"];
        [tempUrlString appendString:key];
        [tempUrlString appendString:@"="];
        [tempUrlString appendString:[obj urlEncodedString]];
    }];
    
    return tempUrlString;
}

// This is for only one key and value pairs
- (NSString *)key
{
    NSArray *keys = [self allKeys];
    if([keys count] > 0)
        return [keys objectAtIndex:0];
    else {
        PCWarning(@"No key in dictionary : %@", self);
        return nil;
    }
}

// This is for only one key and value pairs
- (NSString *)value
{
    NSArray *values = [self allValues];
    if([values count] > 0)
        return [values objectAtIndex:0];
    else {
        PCWarning(@"No value in dictionary : %@", self);
        return nil;
    }
}
//
- (NSData *)HTTPFormatDataWithBoundary:(NSString *)boundary
{
    NSMutableData *mutableData = [NSMutableData data];
    for(NSString *key in [self allKeys]) {
        
        NSString *formatString = [NSString stringWithFormat:@"\r\n--%@\r\nContent-Disposition: form-data; name=\"%@\"\r\n\r\n%@",
                                  boundary,
                                  key,
                                  [self objectForKey:key]];
//        PCLog(@"formatString %@", formatString);
        [mutableData appendData:[formatString dataUsingEncoding:NSUTF8StringEncoding]];
    }
    return mutableData;
}

- (NSInteger)errorCode
{
    return [[self objectForKey:ResponseErrorCodeKey] integerValue];
}

//- (NSDictionary *)messageDict
//{
//    return [self objectForKey:@"messsage"];
//}
//

- (NSString *)message
{
    return [self objectForKey:@"message"];
}

- (NSDictionary *)data
{
    if(self.errorCode == 0){
        return self;
    } else {
        return nil;
    }
}

- (NSArray *)listData
{
    if(self.errorCode == 0){
        if([self isNSArray]){
            return (NSArray *)self;
        } else {
            return [NSArray arrayWithObject:self];
        }
    } else {
        return nil;
    }
}

#pragma mark - convenient
- (PCSerial)serialForKey:(NSString *)key
{
    return [[self objectForKey:key] longLongValue];
}

- (BOOL)boolForKey:(NSString *)key
{
    return [[self objectForKey:key] boolValue];
}

- (NSDate *)dateForKey:(NSString *)key
{
    return [[self objectForKey:key] date];
}

- (NSInteger)integerForKey:(NSString *)key
{
    return [[self objectForKey:key] integerValue];
}

- (float)floatForKey:(NSString *)key
{
    return [[self objectForKey:key] floatValue];
}

- (double)doubleForKey:(NSString *)key
{
    return [[self objectForKey:key] doubleValue];
}

- (PCCurrency)currencyForKey:(NSString *)key
{
    return [[self objectForKey:key] integerValue];
}

- (CLLocationDistance)distanceForKey:(NSString *)key
{
    return [[self objectForKey:key] doubleValue];
}

- (id) safeObjectForKey:(id)aKey
{
    id obj = [self objectForKey:aKey];
    if([obj isKindOfClass:[NSNull class]]){
        return nil;
    }
    return obj;
}

@end
