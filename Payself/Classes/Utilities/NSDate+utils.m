//
//  NSDate+utils.m
//  RushOrder
//
//  Created by Conan on 2/24/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "NSDate+utils.h"

@implementation NSDate (utils)

- (NSString *)secondsString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    //    [dateFormatter setDoesRelativeDateFormatting:YES];
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

- (NSString *)shortDateMinutesString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

- (NSString *)minutesString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

- (NSString *)hourMinuteString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    //    [dateFormatter setDoesRelativeDateFormatting:YES];
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}


- (NSString *)timeString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    //    [dateFormatter setDoesRelativeDateFormatting:YES];
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

- (NSString *)stringForHumanReadableFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setDoesRelativeDateFormatting:YES];
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

- (NSString *)dateStringForHumanReadableFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    //    [dateFormatter setDoesRelativeDateFormatting:YES];
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

- (NSString *)gmtDateStringForHumanReadableFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    //    [dateFormatter setDoesRelativeDateFormatting:YES];
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

- (NSString *)stringWithDotFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy.MM.dd HH:mm";
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

- (NSString *)stringWithDashedFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm";
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

- (NSString *)dateStringWithDashedFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

- (NSString *)gmtDateStringWithDashedFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

- (NSString *)dateHourStringWithDashedFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm";
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

- (NSString *)dateStringWithDashedFormatGMT
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm";
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

- (NSString *)dateStringWithDashedFormatGMTWithZone
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss+00:00";
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

- (NSString *)dateStringWithDotFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy.MM.dd";
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

- (NSString *)weekString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"W'week of' MMM yyyy";
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    if([dateString hasPrefix:@"1"]){
        dateString = [dateString stringByReplacingOccurrencesOfString:@"week of"
                                                           withString:@"st week of"];
    } else if([dateString hasPrefix:@"2"]){
        dateString = [dateString stringByReplacingOccurrencesOfString:@"week of"
                                                           withString:@"nd week of"];
    } else if([dateString hasPrefix:@"3"]){
        dateString = [dateString stringByReplacingOccurrencesOfString:@"week of"
                                                           withString:@"rd week of"];
    } else {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"week of"
                                                           withString:@"th week of"];
    }
    
    return dateString;
}

- (NSString *)monthString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MMM yyyy";
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}


- (NSString *)localizedTimeFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"a hh:mm";
    return [dateFormatter stringFromDate:self];
}

- (NSDate *)dateWithZero
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *comp = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond
                                   fromDate:self];
    comp.hour = 0;
    comp.minute = 0;
    comp.second = 0;
    
    return [cal dateFromComponents:comp];
}

- (NSDate *)dateInHour:(NSInteger)hour
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *comp = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond
                                    fromDate:self];
    comp.hour = hour;
    comp.minute = 0;
    comp.second = 0;
    
    return [cal dateFromComponents:comp];
}

- (NSDate *)gmtDateInHour:(NSInteger)hour
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    cal.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDateComponents *comp = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond
                                    fromDate:self];
    comp.hour = hour;
    comp.minute = 0;
    comp.second = 0;
    
    return [cal dateFromComponents:comp];
}

- (instancetype)rount5dateByAddingTimeInterval:(NSTimeInterval)ti
{
    NSDate *addedDate = [self dateByAddingTimeInterval:ti];
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *minuiteSecondComponent =
    [gregorian components:(NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:addedDate];
    NSInteger minute = [minuiteSecondComponent minute];
    NSInteger second = [minuiteSecondComponent second];
    
    NSInteger mod5 = minute % 5;
    mod5 = 5 - mod5;

    NSTimeInterval adjInterval = (mod5 * 60) - second;
    
    return [addedDate dateByAddingTimeInterval:adjInterval];

    
}

@end
