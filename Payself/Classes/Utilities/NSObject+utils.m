//
//  NSObject+utils.m
//  RushOrder
//
//  Created by Conan on 2/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "NSObject+utils.h"

@implementation NSObject (utils)


- (BOOL)isUITextField
{
    return [self isKindOfClass:[UITextField class]];
}

- (BOOL)isUIButton
{
    return [self isKindOfClass:[UIButton class]];
}

- (BOOL)isNSArray
{
    return [self isKindOfClass:[NSArray class]];
}

- (BOOL)isNSDictionary
{
    return [self isKindOfClass:[NSDictionary class]];
}

@end