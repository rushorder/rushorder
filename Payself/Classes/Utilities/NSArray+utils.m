//
//  NSArray+utils.m
//  RushOrder
//
//  Created by Conan on 4/1/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "NSArray+utils.h"

@implementation NSArray (utils)

- (NSArray *)listData
{
    return self;
}

- (NSArray *)data
{
    return self;
}

- (NSInteger)errorCode
{
    return 0;
}

- (NSString *)firstName
{
    NSString *firstName = nil;
    
    if([self count] > 0){
        firstName = [self objectAtIndex:0];
    }
    return firstName;
}

- (NSString *)lastName
{
    NSMutableString *lastName = nil;
    
    if([self count] > 1){
        lastName = [NSMutableString string];
        
        for(int i = 1 ; i < [self count] ; i++){
            if([lastName length] > 0) [lastName appendString:@" "];
            [lastName appendString:[self objectAtIndex:i]];
        }
    }
    
    return lastName;
}
@end
