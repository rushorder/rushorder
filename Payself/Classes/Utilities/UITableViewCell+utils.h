//
//  UITableViewCell+utils.h
//  RushOrder
//
//  Created by Conan on 2/22/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (utils)

// !!!: Don't trust these tableView and indexPath properties.
// These property work well only under some specific condition
// 1. Cell's direct superview is UITableView
// 2. Cell is in the visible state (Not in the reusable queue)
@property (nonatomic, readonly) UITableView *tableView;
@property (nonatomic, readonly) NSIndexPath *indexPath;
///////////////////////////////////////////////////////////////

+ (id)cell;
+ (id)cellWithNibName:(NSString *)nibName;
+ (id)cellWithNibName:(NSString *)nibName atIndex:(NSUInteger)objectIndex;

- (IBAction)anyButtonTouched:(UIButton *)button;

@end

@protocol UITableViewButtonDelegate <UITableViewDelegate>
- (void)tableView:(UITableView *)tableView didTouchAnyButton:(UIButton *)button atIndexPath:(NSIndexPath *)indexPath;
@end