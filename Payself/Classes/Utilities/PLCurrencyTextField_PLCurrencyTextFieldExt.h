//
//  PLCurrencyTextField_PLCurrencyTextFieldExt.h
//  RushOrder
//
//  Created by Conan Kim on 5/25/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

#import "FormatterManager.h"
#import <PLCurrencyTextField/PLCurrencyTextField.h>

@interface PLCurrencyTextField (Ext)
- (void)setAmount:(PCCurrency)value;
- (PCCurrency)amount;
@end


@implementation PLCurrencyTextField (Ext)


- (void)setAmount:(PCCurrency)value
{
    if(value == 0){
        self.text = nil;
    } else {
//        self.text = [[NSNumber numberWithFloat:(value / 100.f)] stringValue];
        NSNumber *amountNumber = [NSNumber numberWithFloat:(value / 100.0f)];
        self.text = [FORMATTER.currencyFormatter stringFromNumber:amountNumber];
    }
}

- (PCCurrency)amount
{
    NSNumber* number = self.numberValue;
    return lroundf([number floatValue] * 100);
}

@end
