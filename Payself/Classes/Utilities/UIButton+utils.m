//
//  UIButton+utils.m
//  RushOrder
//
//  Created by Conan on 11/19/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import "UIButton+utils.h"
#import "NPToggleButton.h"


#define BADGE_MARGIN -4.0f
#define BADGE_VIEW_TAG 39281
@implementation UIButton (utils)
@dynamic fontName;

#pragma mark - Accessors
//- (void)setBadgeValue:(NSInteger)value
//{
//    NPBadgeView *badgeView = (NPBadgeView *)[self viewWithTag:BADGE_VIEW_TAG];
//    if(badgeView == nil){
//        if(value != 0){
//            badgeView = [[NPBadgeView alloc] initWithFrame:CGRectMake((BADGE_MARGIN * 3), //self.bounds.size.width - MIN_BADGE_SIZE - BADGE_MARGIN,
//                                                                      BADGE_MARGIN,
//                                                                      MIN_BADGE_SIZE,
//                                                                      MIN_BADGE_SIZE)];
//            badgeView.tag = BADGE_VIEW_TAG;
//            [self addSubview:badgeView];
//        }
//    } else {
//        if(value == 0)
//            [badgeView removeFromSuperview];
//    }
//    
//    if(value != 0)
//        badgeView.badgeValue = value;
//}
//
//- (NSInteger)badgeValue
//{
//    NPBadgeView *badgeView = (NPBadgeView *)[self viewWithTag:BADGE_VIEW_TAG];
//    if(badgeView != nil){
//        return badgeView.badgeValue;
//    }
//    return NSNotFound;
//}

- (void)setBadgeValue:(NSString *)value
{
    NPBadgeView *aBadgeView = self.badgeView;
    if(aBadgeView == nil){
        if(value != 0){
            aBadgeView = [[NPBadgeView alloc] initWithFrame:CGRectMake(0.0f,
                                                                      BADGE_MARGIN,
                                                                      MIN_BADGE_SIZE,
                                                                      MIN_BADGE_SIZE)];
            aBadgeView.target = self;
            aBadgeView.action = @selector(badgeTouched:);

            
            aBadgeView.tag = BADGE_VIEW_TAG;
            [self addSubview:aBadgeView];
        }
    } else {
        if(value == nil)
            [aBadgeView removeFromSuperview];
    }
    
    if(value != nil)
        aBadgeView.badgeStringValue = value;
}

- (void)badgeTouched:(id)sender
{
    [self sendActionsForControlEvents:UIControlEventTouchUpInside];
}

- (NSString *)badgeValue
{
    NPBadgeView *aBadgeView = self.badgeView;
    if(aBadgeView != nil){
        return aBadgeView.badgeStringValue;
    }
    return nil;
}

- (NPBadgeView *)badgeView
{
    NPBadgeView *aBadgeView = (NPBadgeView *)[self viewWithTag:BADGE_VIEW_TAG];
    return aBadgeView;
}


+ (UIButton *) buttonWithFrame:(CGRect)frame
                         title:(NSString *)title
                          font:(UIFont *)font
                     textColor:(UIColor *)textColor
                        target:(id)target
                        action:(SEL)action
{
    
    UIButton *button = [[UIButton alloc] initWithFrame:frame];
    
    button.titleLabel.font = font;
    button.titleLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:textColor forState:UIControlStateNormal];
    
    button.showsTouchWhenHighlighted = YES;
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}



+ (UIButton *) buttonWithNormalImage:(UIImage *)normalImage
                    highlightedImage:(UIImage *)highlightedImage
                              target:(id)target
                              action:(SEL)action
{
    return [UIButton buttonWithNormalImage:normalImage
                          highlightedImage:highlightedImage
                                     frame:CGRectMake(0, 0, normalImage.size.width, normalImage.size.height)
                                    target:target
                                    action:action];
}


+ (UIButton *) buttonWithNormalImage:(UIImage *)normalImage
                    highlightedImage:(UIImage *)highlightedImage
                               frame:(CGRect)frame
                              target:(id)target
                              action:(SEL)action
{
    return [UIButton buttonWithNormalImage:normalImage
                          highlightedImage:highlightedImage
                                     frame:frame
                                    target:target
                                    action:action
                              controlEvent:UIControlEventTouchUpInside];
}


+ (UIButton *) buttonWithNormalImage:(UIImage *)normalImage
                    highlightedImage:(UIImage *)highlightedImage
                               frame:(CGRect)frame
                              target:(id)target
                              action:(SEL)action
                        controlEvent:(UIControlEvents)controlEvent
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:frame];
    button.backgroundColor = [UIColor clearColor];
    [button setBackgroundImage:normalImage forState:UIControlStateNormal];
    [button setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
    
    
    if(target != nil)
    {
        [button addTarget:target action:action forControlEvents:controlEvent];
    }
    else
    {
        [button setAdjustsImageWhenHighlighted:NO];
    }
    
    return button;
}


- (void)changeNormalImage:(UIImage *)normalImage highlightedImage:(UIImage *)highlightedImage
{
    [self setBackgroundImage:normalImage forState:UIControlStateNormal];
    [self setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
}

- (void)applyResizableImageFromCenter
{
    [self applyResizableImageFromCenterForState:UIControlStateNormal];
}

- (void)applyResizableImageFromCenterForAllState
{
    [self applyResizableImageFromCenterForState:(UIControlStateNormal | UIControlStateHighlighted | UIControlStateDisabled | UIControlStateSelected)];
}

- (void)applyResizableImageFromCenterForState:(UIControlState)ctrlState
{
    // Just for calculating size.
    UIImage *anImage = [self backgroundImageForState:UIControlStateNormal];
    CGSize imgSize = anImage.size;
    CGFloat centerX = floorf(imgSize.width / 2);
    CGFloat centerY = floorf(imgSize.height / 2);
    
    [self applyResizableImage:UIEdgeInsetsMake(centerY, centerX, centerY-1, centerX-1) forState:ctrlState];
}

- (void)applyResizableImage:(UIEdgeInsets)CapInsets
{
    [self applyResizableImage:CapInsets forState:UIControlStateNormal];
}

- (void)applyResizableImageForAllState:(UIEdgeInsets)CapInsets
{
    [self applyResizableImage:CapInsets forState:(UIControlStateNormal | UIControlStateHighlighted | UIControlStateDisabled | UIControlStateSelected)];
}

- (void)applyResizableImage:(UIEdgeInsets)CapInsets forState:(UIControlState)ctrlState
{
    // UIControlStateNormal is 0
    [self applyResizableImage:CapInsets forEachState:UIControlStateNormal];
    
    if((ctrlState & UIControlStateHighlighted) == UIControlStateHighlighted){
        [self applyResizableImage:CapInsets forEachState:UIControlStateHighlighted];
    }
    if((ctrlState & UIControlStateDisabled) == UIControlStateDisabled){
        [self applyResizableImage:CapInsets forEachState:UIControlStateDisabled];
    }
    if((ctrlState & UIControlStateSelected) == UIControlStateSelected){
        [self applyResizableImage:CapInsets forEachState:UIControlStateSelected];
    }
}

- (void)applyResizableImage:(UIEdgeInsets)CapInsets forEachState:(UIControlState)ctrlState
{
    UIImage *anImage = [self backgroundImageForState:ctrlState];
    if(anImage != nil) {
        if([anImage respondsToSelector:@selector(resizableImageWithCapInsets:)]){
            UIImage *resizableImage = [anImage resizableImageWithCapInsets:CapInsets];
            [self setBackgroundImage:resizableImage forState:ctrlState];
            if(ctrlState == UIControlStateNormal){
                if([self isKindOfClass:[NPToggleButton class]]){
                    ((NPToggleButton *)self).normalBackgroundImage = resizableImage;
                }
            }
        } else {
            UIImage *stretchableImage = [anImage stretchableImageWithLeftCapWidth:CapInsets.left topCapHeight:CapInsets.top];
            [self setBackgroundImage:stretchableImage forState:ctrlState];
            if((ctrlState & UIControlStateNormal) == UIControlStateNormal){
                if([self isKindOfClass:[NPToggleButton class]]){
                    ((NPToggleButton *)self).normalBackgroundImage = stretchableImage;
                }
            }
        }
    }
}

- (void)sizeToFitReasonably
{
    CGRect btnFrame = self.frame;
    CGFloat horizontalMargin = 6.0f;
    CGFloat minimumWidth = 20.0f;
    
    NSString *title = [self titleForState:UIControlStateNormal];
    CGSize titleSize = [title sizeWithFontOrAttributes:self.titleLabel.font];
    CGFloat width = titleSize.width + (horizontalMargin * 2);
    width = MAX(minimumWidth, width);
    
    CGRect newFrame = CGRectMake(CGRectGetMaxX(btnFrame) - width,
                                 btnFrame.origin.y,
                                 width,
                                 btnFrame.size.height);
    self.frame = newFrame;
}


- (void)setButtonTitle:(NSString *)title
{
    [self setTitle:title forState:UIControlStateNormal];
}

- (NSString *)buttonTitle
{
    return [self titleForState:UIControlStateNormal];
}

//- (NSString *)fontName {
//    return self.titleLabel.font.fontName;
//}
//
//- (void)setFontName:(NSString *)fontName {
//    self.titleLabel.font = [UIFont fontWithName:fontName size:self.titleLabel.font.pointSize];
//}

@end
