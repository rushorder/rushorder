//
//  PCLogger.m
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCLogger.h"

void PCWarning (NSString *format, ...) {
    va_list args;
    va_start(args, format);
    [[PCLogger shared] logLevel:LevelWarning format:format args:args];
    va_end(args);
}

void PCError (NSString *format, ...) {
    va_list args;
    va_start(args, format);
    [[PCLogger shared] logLevel:LevelError format:format args:args];
    va_end(args);
}

void PCLog (NSString *format, ...) {
    va_list args;
    va_start(args, format);
    [[PCLogger shared] logLevel:LevelLog format:format args:args];
    va_end(args);
}

void PCLogl (LogLevel logLevel, NSString *format, ...) {
    va_list args;
    va_start(args, format);
    [[PCLogger shared] logLevel:logLevel format:format args:args];
    va_end(args);
}


@implementation PCLogger

+ (PCLogger *)shared
{
    static PCLogger *_log = nil;
    
    @synchronized(self){
        if(_log == nil){
            _log = [[self alloc] init];
        }
    }
    
    return _log;
}

- (void)start
{
    //Do nothing, just create shared instnace.
}

- (void)logLevel:(LogLevel)level format:(NSString *)format args:(va_list)args;
{
    NSString *decoratedFormat = nil;
    if(self.logLevel <= level){
        switch(level){
            case LevelWarning:
                decoratedFormat = [@"[RO WARNING] " stringByAppendingString:format];
                break;
            case LevelError:
                decoratedFormat = [@"[RO ERROR] " stringByAppendingString:format];
#ifdef FLURRY_ENABLED
                va_list argsCopy;
                va_copy(argsCopy, args);
                [Flurry logError:[NSString stringWithFormat:@"%@", decoratedFormat]
                         message:[[NSString alloc] initWithFormat:decoratedFormat arguments:argsCopy]
                           error:nil];
                va_end(argsCopy);
#endif
                break;
            default:
                decoratedFormat = format;
                break;
        }
        
        NSLogv(decoratedFormat, args);
        
        
    }
}
@end