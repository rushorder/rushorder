//
//  CardNumberFormatter.m
//  RushOrder
//
//  Created by Conan on 2/27/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

// Refer to
// http://en.wikipedia.org/wiki/Bank_card_number#cite_note-GenCardFeatures-3
// http://en.wikipedia.org/wiki/List_of_Issuer_Identification_Numbers

/*
 
 
 Issuing network	IIN ranges	Active	Length	Validation
 American Express	34, 37[3][dead link]	Yes	15[4]	Luhn algorithm
 Bankcard[5]	5610, 560221-560225	No	16	Luhn algorithm
 China UnionPay	62[6]	Yes	16-19	no validation
 Diners Club Carte Blanche	300-305	Yes	14	Luhn algorithm
 Diners Club enRoute	2014, 2149	No	15	no validation
 Diners Club International[7]	36	Yes	14	Luhn algorithm
 Diners Club United States & Canada[8]	54, 55	Yes	16	Luhn algorithm
 Discover Card[9]	6011, 622126-622925, 644-649, 65	Yes	16	Luhn algorithm
 InstaPayment	637-639[citation needed]	Yes	16	Luhn algorithm
 JCB	3528-3589[10]	Yes	16	Luhn algorithm
 Laser	6304, 6706, 6771, 6709	Yes	16-19	Luhn algorithm
 Maestro	5018, 5020, 5038, 5893, 6304, 6759, 6761, 6762, 6763, 0604	Yes	12-19	Luhn algorithm
 MasterCard	51-55	Yes	16	Luhn algorithm
 Solo	6334, 6767	No	16, 18, 19	Luhn algorithm
 Switch	4903, 4905, 4911, 4936, 564182, 633110, 6333, 6759	No	16, 18, 19	Luhn algorithm
 Visa	4	Yes	13, 16[11]	Luhn algorithm
 Visa Electron	4026, 417500, 4405, 4508, 4844, 4913, 4917	Yes	16	Luhn algorithm

 */


#import "CardNumberFormatter.h"

@interface CardNumberFormatter()
{
    BOOL canValudate;
}

@end

@implementation CardNumberFormatter



- (id)init {
    
    self = [super init];
    if(self != nil){
        cardNumberFormats = [NSArray arrayWithObjects:
                             @"####-####-####-####",
                             nil];
    }
    
    
    return self;
    
}



- (Issuer)acquireIssuer:(NSString *)cardNumber
{
    // Refer http://www.regular-expressions.info/creditcard.html
    // But I
    canValudate = YES;
    NSString *s = [cardNumber stringByReplacingOccurrencesOfString:@"-"
                                                        withString:@""];
    if([s length] == 0){
        return IssuerUnknown;
    }
    
    char firstChar = [s characterAtIndex:0];
    
    switch(firstChar){
        case '0':
            if([s hasPrefixes:[NSArray arrayWithObjects:@"0604", nil]]){
                return IssuerMaestro;
            }
            break;
        case '1':
            
            break;
        case '2':
            if([s hasPrefixes:[NSArray arrayWithObjects:@"2014", @"2149", nil]]){
                canValudate = NO;
                return IssuerDinersClubEnRoute;
            }
            break;
        case '3':
            if([s hasPrefixes:[NSArray arrayWithObjects:@"34", @"37", nil]]){
                return IssuerAmericanExpress;
            } else if ([s hasPrefixes:[NSArray arrayWithObjects:@"36",@"38", nil]]){
                return IssuerDinersClubInternational;
            } else if ([s hasPrefixes:[NSArray arrayWithObjects:@"30", nil]]){
                NSInteger ssi = [s integerWithLength:3];
                if(300 <= ssi && ssi <= 305){
                    return IssuerDinersClubCarteBlanche;
                }
            } else if ([s hasPrefixes:[NSArray arrayWithObjects:@"35", nil]]){
                NSInteger ssi = [s integerWithLength:4];
                if(3528 <= ssi && ssi <= 3589){
                    return IssuerJCB;
                }
            }
            break;
        case '4':
            if([s length] == 1){
                return IssuerVisa;
            } else {
                if([s hasPrefixes:[NSArray arrayWithObjects:@"4026", @"417500", @"4405", @"4508", @"4844", @"4913", @"4917", nil]]){
                    return IssuerVisaElectron;
                } else if ([s hasPrefixes:[NSArray arrayWithObjects:@"4903",@"4905",@"4911",@"4936", nil]]){
                    return IssuerSwitch;
                } else {
                    return IssuerVisa;
                }
            }
            break;
        case '5':            
            if([s hasPrefixes:[NSArray arrayWithObjects:@"5610", nil]]){
                return IssuerBankcard;
            } else if ([s hasPrefixes:[NSArray arrayWithObjects:@"5018",@"5020",@"5038",@"5893", nil]]){
                return IssuerMaestro;
                
            } else if ([s hasPrefixes:[NSArray arrayWithObjects:@"564182", nil]]){
                return IssuerSwitch;
            } else {
                NSInteger ssi = [s integerWithLength:2];
                if(51 <= ssi && ssi <= 55){
                    return IssuerMasterCard;
                } else if(ssi == 56){
                    NSInteger ssi = [s integerWithLength:6];
                    if(560221 <= ssi && ssi <= 560225){
                        return IssuerBankcard;
                    }
                }
            }
            
            /*
             !!!:
             Diners Club: There are Diners Club cards that begin with 5. These are a joint venture between Diners Club and MasterCard, and are processed like a MasterCard.
             IssuerDinersClubUnitedStatesNCanada; 54,55
             if ([s hasPrefixes:[NSArray arrayWithObjects:@"54",@"55", nil]]){
                return IssuerDinersClubUnitedStatesNCanada;
             }
             http://en.wikipedia.org/wiki/List_of_Issuer_Identification_Numbers#Overview
             
             //
             // Duplicated
             6304~ Laser bank / Maestro
             6759** - VARIOUS BANKS (UK) - Maestro (formerly Switch) debit cards
             */
            
            break;
        case '6':
            if([s hasPrefixes:[NSArray arrayWithObjects:@"62", nil]]){
                NSInteger ssi = [s integerWithLength:6];
                if(622126 <= ssi && ssi <= 622925){
                    return IssuerDiscover;
                } else {
                    return IssuerChinaUnionPay;
                }                
            } else if([s hasPrefixes:[NSArray arrayWithObjects:@"6011",@"64",@"65", nil]]){
                NSInteger ssi = [s integerWithLength:3];
                if(644 <= ssi && ssi <= 649){
                    return IssuerDiscover;
                } else if(ssi < 640 || 650 <= ssi){
                    return IssuerDiscover;
                }                
            } else if([s hasPrefixes:[NSArray arrayWithObjects:@"637",@"638",@"639", nil]]){
                return IssuerInstaPayment;
            } else if([s hasPrefixes:[NSArray arrayWithObjects:@"6334",@"6767", nil]]){
                return IssuerSolo;
            } else if([s hasPrefixes:[NSArray arrayWithObjects:@"6304",@"6759",@"6761",@"6762",@"6763", nil]]){
                return IssuerMaestro;
            } else if([s hasPrefixes:[NSArray arrayWithObjects:@"6304",@"6706",@"6771",@"6709", nil]]){
                return IssuerLaser;
            } else if([s hasPrefixes:[NSArray arrayWithObjects:@"633110",@"6333",@"6759", nil]]){
                return IssuerSwitch;
            } else{
                NSInteger ssi = [s integerWithLength:2];
                if(51 <= ssi && ssi <= 55){
                    return IssuerMasterCard;
                } else if(ssi == 56){
                    NSInteger ssi = [s integerWithLength:6];
                    if(560221 <= ssi && ssi <= 560225){
                        return IssuerBankcard;
                    }
                }
            }
            break;
        case '7':
            break;
        case '8':
            break;
        case '9':
            break;
        default:            
            break;
    }

    return IssuerUnknown;
}

- (void)setFormatByIssuer:(Issuer)issuer
{
    switch(issuer){
            // 14
        case IssuerDinersClubCarteBlanche:
        case IssuerDinersClubInternational:
            cardNumberFormats = [NSArray arrayWithObjects:
                                 @"####-######-#########",
                                 nil];
            break;
            // 15
        case IssuerAmericanExpress:
        case IssuerDinersClubEnRoute:
            cardNumberFormats = [NSArray arrayWithObjects:
                                 @"####-######-########",
                                 nil];
            break;
            // 16 ~ 19
        case IssuerChinaUnionPay:
        case IssuerLaser:
            cardNumberFormats = [NSArray arrayWithObjects:
                                 @"####-####-####-####-###",
                                 nil];
            break;
            // 12 ~ 19
        case IssuerMaestro:
            cardNumberFormats = [NSArray arrayWithObjects:
                                 @"####-####-####-####-###",
                                 nil];
            break;
            // 16, 18, 19
        case IssuerSolo:
        case IssuerSwitch:
            cardNumberFormats = [NSArray arrayWithObjects:
                                 @"####-####-####-####-###",
                                 nil];
            break;
            // 13, 16
        case IssuerVisa:
            cardNumberFormats = [NSArray arrayWithObjects:
                                 @"####-####-####-####",
                                 nil];
            break;
            //16
        case IssuerBankcard:
        case IssuerDinersClubUnitedStatesNCanada:
        case IssuerDiscover:
        case IssuerInstaPayment:
        case IssuerJCB:
        case IssuerMasterCard:
        case IssuerUnknown:
        case IssuerVisaElectron:
        case IssuerTest:
        default:
            cardNumberFormats = [NSArray arrayWithObjects:
                                 @"####-####-####-####",
                                 nil];
            break;
    }
    
}


- (NSString *)format:(NSString *)cardNumber withIssuer:(Issuer)issuer
{
    return [self format:cardNumber withIssuer:issuer securely:NO];
}

- (NSString *)format:(NSString *)cardNumber withIssuer:(Issuer)issuer securely:(BOOL)securely
{
    return [self format:cardNumber
             withIssuer:issuer
               securely:securely
           secureString:@"*"];
}

- (NSString *)format:(NSString *)cardNumber withIssuer:(Issuer)issuer securely:(BOOL)securely secureString:(NSString *)secureString
{
    [self setFormatByIssuer:issuer];
    
    if(cardNumberFormats == nil) return cardNumber;
    
    NSString *input = [self strip:cardNumber];
    
    for(NSString *cardFormat in cardNumberFormats) {
        
        int i = 0;
        
        NSMutableString *temp = [[NSMutableString alloc] init];
        
        for(int p = 0; temp != nil && i < [input length] && p < [cardFormat length]; p++) {
            
            char c = [cardFormat characterAtIndex:p];
            
            BOOL required = [self canBeInputByPhonePad:c];
            
            char next = [input characterAtIndex:i];
            
            switch(c) {
                    
                case '$':
                    
                    p--;
                    
                    [temp appendFormat:@"%c", next]; i++;
                    
                    break;
                    
                case '#':
                    
                    if(next < '0' || next > '9') {
                        
                        temp = nil;
                        
                        break;
                        
                    }
                    
//                    if(securely && i >= 4 && i < ([input length] - 4)){
                    if(securely && i < ([input length] - 4)){
                        [temp appendString:secureString]; i++;
                    } else {
                        [temp appendFormat:@"%c", next]; i++;
                    }
                    
                    break;
                    
                default:
                    
                    if(required) {
                        
                        if(next != c) {
                            
                            temp = nil;
                            
                            break;
                            
                        }
                        
                        [temp appendFormat:@"%c", next]; i++;
                        
                    } else {
                        
                        [temp appendFormat:@"%c", c];
                        
                        if(next == c) i++;
                        
                    }
                    
                    break;
                    
            }
            
        }
        
        if(i == [input length]) {
            
            return temp;
            
        }
        
    }
    
    return input;
    
}



- (NSString *)strip:(NSString *)cardNumber {
    
    NSMutableString *res = [[NSMutableString alloc] init];
    
    for(int i = 0; i < [cardNumber length]; i++) {
        
        char next = [cardNumber characterAtIndex:i];
        
        if([self canBeInputByPhonePad:next])
            
            [res appendFormat:@"%c", next];
        
    }
    
    return res;
    
}



- (BOOL)canBeInputByPhonePad:(char)c {
    
    if(c == '+' || c == '*' || c == '#') return YES;
    
    if(c >= '0' && c <= '9') return YES;
    
    return NO;
    
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

@end