//
//  M13ProgressHUD.h
//  RushOrder2
//
//  Created by Conan on 9/15/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface M13ProgressHUD (utils)

+ (M13ProgressHUD *)sharedHUD;

+ (void)show;
+ (void)hide;
+ (void)showError:(NSString *)message;
@end
