//
//  CLPlacemark+utils.m
//  RushOrder
//
//  Created by Conan on 12/16/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import "CLPlacemark+utils.h"
#import <AddressBookUI/AddressBookUI.h>

@implementation CLPlacemark (utils)

- (NSString *)address
{
    NSMutableDictionary *muDict = [self.addressDictionary mutableCopy];
    [muDict removeObjectForKey:@"Country"];
    NSString *addrForReturn = ABCreateStringWithAddressDictionary(self.addressDictionary, NO);
    NSString *noLineBreakAddress = [addrForReturn stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    
    return noLineBreakAddress;
}

- (void)appendString:(NSMutableString *)first string:(NSString *)second
{
    if(second != nil){
        if([first length] > 0) [first appendString:@" "];
        [first appendString:second];
    }
}


@end
