//
//  UIStoryboard+utils.h
//  RushOrder2
//
//  Created by Conan on 8/25/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (utils)
+ (id)initialViewControllerFrom:(NSString *)storyboardName;

+ (id)viewController:(NSString *)identifier
                                from:(NSString *)storyboardName;
@end
