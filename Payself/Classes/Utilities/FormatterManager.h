//
//  FormatterManager.h
//  RushOrder
//
//  Created by Conan on 2/24/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CardNumberFormatter.h"

#define FORMATTER [FormatterManager sharedManager] 

@interface FormatterManager : NSObject <UITextFieldDelegate>

+ (FormatterManager *)sharedManager;

@property (strong, nonatomic) NSNumberFormatter* currencyFormatter;
@property (strong, nonatomic) NSNumberFormatter* currencyFormatterWithoutCent;
@property (strong, nonatomic) NSNumberFormatter* numberFormatter;
@property (strong, nonatomic) NSNumberFormatter* decimalFormatter;
@property (strong, nonatomic) NSNumberFormatter* percentFormatter;
@property (strong, nonatomic) CardNumberFormatter* cardNumberFormatter;
@property (strong, nonatomic) NSCharacterSet *nonNumberSet;

@end
