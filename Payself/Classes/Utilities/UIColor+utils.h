//
//  UIColor+utils.h
//  RushOrder
//
//  Created by Conan on 11/19/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (utils)
+ (UIColor *)colorWithR:(CGFloat)red G:(CGFloat)green B:(CGFloat)blue;
+ (UIColor *)colorWithR:(CGFloat)red G:(CGFloat)green B:(CGFloat)blue A:(CGFloat)alpha;
+ (UIColor *)lightBlueColor;
+ (UIColor *)moreLightGrayColor;
+ (UIColor *)cellHighlightedColor;
+ (UIColor *)alertCellTextLabelColorDarkGray;
+ (UIColor *)cellTextLabelColorDarkGray;
+ (UIColor *)cellTextLabelColorLightBlue;
+ (UIColor *)warningTextColor;
+ (UIColor *)safeTextColor;
+ (UIColor *)infoTextColor;
+ (UIColor *)defaultTintColor;
+ (UIColor *)randomColor;
+ (UIColor *)darkRandomColor;
+ (UIColor *)randomColor:(NSInteger)colorDepth;

+ (UIColor *)disabledColor;

+ (UIColor *)c1Color;
+ (UIColor *)c2Color;
+ (UIColor *)c3Color;
+ (UIColor *)c4Color;
+ (UIColor *)c5Color;
+ (UIColor *)c6Color;
+ (UIColor *)c7Color;
+ (UIColor *)c8Color;
+ (UIColor *)c9Color;
+ (UIColor *)c10Color;
+ (UIColor *)c11Color;
+ (UIColor *)c12Color;
+ (UIColor *)c13Color;
+ (UIColor *)c14Color;
@end
