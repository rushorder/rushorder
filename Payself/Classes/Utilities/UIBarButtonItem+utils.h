//
//  UIBarButtonItem+utils.h
//  RushOrder
//
//  Created by Conan on 19/11/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>


#define BARBUTTON_BACK_IMAGE                @"button_bg_navigationbar"
#define BARBUTTON_BACK_IMAGE_PRESSED        @"button_bg_navigationbar_h"
//#define BARBUTTON_BACK_IMAGE_PRESSED        nil
//
//
//#define BARBUTTON_BACK_IMAGE_BACK            @"UINavigationBarDefaultBack"
//#define BARBUTTON_BACK_IMAGE_BACK_PRESSED    @"UINavigationBarDefaultBackPressed"

#define BARBUTTON_BACK_IMAGE_BACK            nil
#define BARBUTTON_BACK_IMAGE_BACK_PRESSED    nil

typedef enum {
    NPBarButtonItemBackOrange,
    NPBarButtonItemBack,
    NPBarButtonItemHome,
    NPBarButtonItemSetting,
    NPBarButtonItemLeftMenu,
    NPBarButtonItemWrite,
    NPBarButtonItemEdit,
    NPBarButtonItemDone,
    NPBarButtonItemCart,
    NPBarButtonItemPlus,
    NPBarButtonItemReceipt,
    NPBarButtonItemDoneStressed,
    NPBarButtonItemNext,
    NPBarButtonItemNextStressed,
    NPBarButtonItemCancel,
    NPBarButtonItemAdd,
    NPBarButtonItemClose,
    NPBarButtonItemSave,
    NPBarButtonItemConfirm,
    NPBarButtonItemRegistration,
    NPBarButtonItemMap,
    NPBarButtonItemSearch,
    NPBarButtonEmail
} NPBarButtonItem;

typedef enum {
    NPBarButtonBackTypeDefault,
    NPBarButtonBackTypeDefaultArrow,
    NPBarButtonBackTypeAppDefault,
    NPBarButtonBackTypeBack,
    NPBarButtonBackTypeClear
} NPBarButtonBackType;

@interface UIBarButtonItem (utils)

@property (readonly) UIButton *button;

+ (UIBarButtonItem *)barButtonItemWithTitle:(NSString *)title
                                     target:(id)target
                                     action:(SEL)action;

+ (UIBarButtonItem *)barButtonItemWithTitle:(NSString *)title
                                     target:(id)target
                                     action:(SEL)action
                                   backType:(NPBarButtonBackType)backType;

+ (UIBarButtonItem *)barButtonItemWithImage:(UIImage *)anImage
                                     target:(id)target
                                     action:(SEL)action
                                   backType:(NPBarButtonBackType)backType;

+ (UIBarButtonItem *)barButtonItemWithImage:(UIImage *)normalImage
                           highlightedImage:(UIImage *)highlightedImage
                                     target:(id)target
                                     action:(SEL)action
                                   backType:(NPBarButtonBackType)backType;

+ (UIBarButtonItem *)barButtonItemWithImage:(UIImage *)normalImage
                            backgroundImage:(NSString *)backImageName
                 highlightedBackgroundImage:(NSString *)highlightedBackImageName
                                     target:(id)target
                                     action:(SEL)action
                                   backType:(NPBarButtonBackType)backType;

+ (UIBarButtonItem *)barButtonSystemItem:(UIBarButtonSystemItem)systemItem;

+ (UIBarButtonItem *)barButtonSystemItem:(UIBarButtonSystemItem)systemItem
                                  target:(id)target
                                  action:(SEL)action;

+ (UIBarButtonItem *)barButtonItemWithCustomView:(UIView *)customView;

+ (UIBarButtonItem *)dummyBarButtonItem;

+ (UIBarButtonItem *)barButtonNovaItem:(NPBarButtonItem)NPBarButtonItem
                                target:(id)target
                                action:(SEL)action;
@end

