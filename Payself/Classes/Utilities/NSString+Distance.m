//
//  NSString+Distance.m
//  RushOrder
//
//  Created by Conan on 3/25/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "NSString+Distance.h"

@implementation NSString (Distance)

+ (NSString *)stringWithDistanceInMile:(double)distance
{
    BOOL isMetric = [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue];
    
    NSString *format;
    
    if (isMetric) {
        distance = (distance * FEET_IN_MILES) / METERS_TO_FEET;
        if (distance < METERS_CUTOFF) {
            format = @"%@ m";
        } else {
            format = @"%@ km";
            distance = distance / 1000;
        }
    } else { // assume Imperial / U.S.
        double FeetDistance = distance * FEET_IN_MILES;
        if (FeetDistance < FEET_CUTOFF) {
            distance = FeetDistance;
            format = @"%@ ft";
        } else {
            format = @"%@ mi";
        }
    }
    
    return [NSString stringWithFormat:format, [self stringWithDouble:distance]];
}

+ (NSString *)stringWithDistance:(double)distance
{
    BOOL isMetric = [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue];
    
    NSString *format;
    
    if (isMetric) {
        if (distance < METERS_CUTOFF) {
            format = @"%@ m";
        } else {
            format = @"%@ km";
            distance = distance / 1000;
        }
    } else { // assume Imperial / U.S.
        distance = distance * METERS_TO_FEET;
        if (distance < FEET_CUTOFF) {
            format = @"%@ ft";
        } else {
            format = @"%@ mi";
            distance = distance / FEET_IN_MILES;
        }
    }
    
    return [NSString stringWithFormat:format, [self stringWithDouble:distance]];
}

// Return a string of the number to one decimal place and with commas & periods based on the locale.
+ (NSString *)stringWithDouble:(double)value {
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setLocale:[NSLocale currentLocale]];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    return [numberFormatter stringFromNumber:[NSNumber numberWithDouble:value]];
}

@end
