//
//  NSNumberFormatter+utils.m
//  RushOrder
//
//  Created by Conan on 2/24/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "NSNumberFormatter+utils.h"

@implementation NSNumberFormatter (utils)

+ (NSNumberFormatter *)currencyFormatter
{
    return [self currencyFormatterWithoutFraction:NO];
}

+ (NSNumberFormatter *)currencyFormatterWithoutFraction:(BOOL)withoutFraction
{
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [currencyFormatter setMaximumFractionDigits:withoutFraction ? 0 : 2];
    
    
    return currencyFormatter;
}

@end
