//
//  M13ProgressHUD.m
//  RushOrder2
//
//  Created by Conan on 9/15/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import "M13ProgressHUD.h"
#import "M13ProgressViewRing.h"

@implementation M13ProgressHUD (utils)

+ (M13ProgressHUD *)sharedHUD
{
    static dispatch_once_t once;
    static M13ProgressHUD* sharedHUD = nil;
    if(sharedHUD == nil){
        dispatch_once(&once, ^ { sharedHUD = [[M13ProgressHUD alloc] initWithProgressView:[[M13ProgressViewRing alloc] init]]; });
        
        sharedHUD.progressViewSize = CGSizeMake(60.0, 60.0);
        sharedHUD.animationPoint = CGPointMake([UIScreen mainScreen].bounds.size.width / 2, [UIScreen mainScreen].bounds.size.height / 2);
        UIWindow *window = APP.window;
        
        [window addSubview:sharedHUD];
    }
    return sharedHUD;
}

+ (void)show
{
    [[M13ProgressHUD sharedHUD] show:YES];
    [[M13ProgressHUD sharedHUD].progressView setIndeterminate:YES];
}

+ (void)hide
{
    [[M13ProgressHUD sharedHUD].progressView setIndeterminate:NO];
    [[M13ProgressHUD sharedHUD] hide:YES];
}

+ (void)showError:(NSString *)message
{
    [M13ProgressHUD sharedHUD].status = message;
    [[M13ProgressHUD sharedHUD] show:YES];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:[M13ProgressHUD sharedHUD]
                                             selector:@selector(hide)
                                               object:nil];
    [[M13ProgressHUD sharedHUD] performSelector:@selector(hide)
                                     withObject:nil
                                     afterDelay:2.0f];
}
@end
