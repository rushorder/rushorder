//
//  UIAlertView+utils.m
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "UIAlertView+utils.h"

@implementation UIAlertView (utils)

+ (void)alert:(NSString *)message defaultMessage:(NSString *)defaultMessage
{
    NSString *paramMessage = message;
    if(paramMessage == nil) paramMessage = defaultMessage;
    
    [self alertWithTitle:UNDEFINED_ALERT_TITLE
                 message:paramMessage];
}

+ (void)alert:(NSString *)message
{
    [self alertWithTitle:UNDEFINED_ALERT_TITLE
                 message:message];
}

+ (void)alertWithTitle:(NSString *)title
               message:(NSString *)message
{
    [self alertWithTitle:title
                 message:message
                delegate:nil];
}

+ (void)alertWithTitle:(NSString *)title
               message:(NSString *)message
     cancleButtonTitle:(NSString *)cancelButtonTitle
{
    [self alertWithTitle:title
                 message:message
       cancelButtonTitle:cancelButtonTitle
                delegate:nil
                     tag:0];
}

+ (void)alertWithTitle:(NSString *)title
               message:(NSString *)message
              delegate:(id<UIAlertViewDelegate>)delegate
{
    [self alertWithTitle:title
                 message:message
                delegate:delegate
                     tag:0];
}

+ (void)alertWithTitle:(NSString *)title
               message:(NSString *)message
              delegate:(id<UIAlertViewDelegate>)delegate
                   tag:(NSInteger)tag
{
    [self alertWithTitle:title
                 message:message
       cancelButtonTitle:NSLocalizedString(@"OK", nil)
                delegate:delegate
                     tag:tag];
}

+ (void)alertWithTitle:(NSString *)title
               message:(NSString *)message
     cancelButtonTitle:(NSString *)cancelButtonTitle
              delegate:(id<UIAlertViewDelegate>)delegate
                   tag:(NSInteger)tag
{
    UIAlertView  *alertView = [[self alloc] initWithTitle:title
                                                  message:message
                                                 delegate:delegate
                                        cancelButtonTitle:cancelButtonTitle
                                        otherButtonTitles:nil];
    alertView.tag = tag;
    [alertView show];
}

+ (void)askWithMessage:(NSString *)message
              delegate:(id<UIAlertViewDelegate>)delegate
{
    [self askWithTitle:UNDEFINED_ALERT_TITLE
               message:message
              delegate:(id<UIAlertViewDelegate>)delegate];
}

+ (void)askWithMessage:(NSString *)message
              delegate:(id<UIAlertViewDelegate>)delegate
                   tag:(NSInteger)tag
{
    [self askWithTitle:UNDEFINED_ALERT_TITLE
               message:message
              delegate:delegate
                   tag:tag];
}

+ (void)askWithTitle:(NSString *)title
             message:(NSString *)message
            delegate:(id<UIAlertViewDelegate>)delegate
{
    [self askWithTitle:title
               message:message
              delegate:delegate
                   tag:0];
}

+ (void)askWithTitle:(NSString *)title
             message:(NSString *)message
            delegate:(id<UIAlertViewDelegate>)delegate
                 tag:(NSInteger)tag
{
    UIAlertView  *alertView = [[self alloc] initWithTitle:title
                                                  message:message
                                                 delegate:delegate
                                        cancelButtonTitle:NSLocalizedString(@"NO", nil)
                                        otherButtonTitles:NSLocalizedString(@"YES", nil), nil];
    alertView.tag = tag;
    [alertView show];
}

@end
