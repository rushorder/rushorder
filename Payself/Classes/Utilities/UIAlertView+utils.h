//
//  UIAlertView+utils.h
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

#define UNDEFINED_ALERT_TITLE   NSLocalizedString(@"RushOrder", nil)

enum {
    AlertButtonIndexNO = 0,
    AlertButtonIndexYES,
};

@interface UIAlertView (utils)

+ (void)alert:(NSString *)message defaultMessage:(NSString *)defaultMessage;

+ (void)alert:(NSString *)message;

+ (void)alertWithTitle:(NSString *)title
               message:(NSString *)message;

+ (void)alertWithTitle:(NSString *)title
               message:(NSString *)message
     cancleButtonTitle:(NSString *)cancelButtonTitle;

+ (void)alertWithTitle:(NSString *)title
               message:(NSString *)message
              delegate:(id)delegate;

+ (void)alertWithTitle:(NSString *)title
               message:(NSString *)message
              delegate:(id)delegate
                   tag:(NSInteger)tag;

+ (void)askWithMessage:(NSString *)message
              delegate:(id)delegate;

+ (void)askWithMessage:(NSString *)message
              delegate:(id)delegate
                   tag:(NSInteger)tag;

+ (void)askWithTitle:(NSString *)title
             message:(NSString *)message
            delegate:(id)delegate;

+ (void)askWithTitle:(NSString *)title
             message:(NSString *)message
            delegate:(id)delegate
                 tag:(NSInteger)tag;

+ (void)alertWithTitle:(NSString *)title
               message:(NSString *)message
     cancelButtonTitle:(NSString *)cancelButtonTitle
              delegate:(id<UIAlertViewDelegate>)delegate
                   tag:(NSInteger)tag;
@end