//
//  NSDictionary+utils.h
//  RushOrder
//
//  Created by Conan on 11/23/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const ResponseErrorCodeKey;

@interface NSDictionary (utils)

@property (nonatomic, readonly) NSString *key; // This is for only one key and value pairs
@property (nonatomic, readonly) NSString *value; // This is for only one key and value pairs
@property (nonatomic, readonly) NSMutableString *urlString;

- (NSInteger)errorCode;
//- (NSDictionary *)messageDict;
- (NSString *)message;
- (NSDictionary *)data;
- (NSArray *)listData;

- (NSData *)HTTPFormatDataWithBoundary:(NSString *)boundary;


#pragma mark - convenient
- (PCSerial)serialForKey:(NSString *)key;
- (BOOL)boolForKey:(NSString *)key;
- (NSDate *)dateForKey:(NSString *)key;
- (NSInteger)integerForKey:(NSString *)key;
- (float)floatForKey:(NSString *)key;
- (double)doubleForKey:(NSString *)key;
- (PCCurrency)currencyForKey:(NSString *)key;
- (CLLocationDistance)distanceForKey:(NSString *)key;

- (id) safeObjectForKey:(id)aKey;
@end
