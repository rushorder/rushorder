//
//  UINavigationController+utils.h
//  RushOrder
//
//  Created by Conan on 10/7/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (utils)

@property (readonly) UIViewController *rootViewController;

- (void)popToViewControllerClass:(Class)viewControllerClass
                        animated:(BOOL)animated;
@end
