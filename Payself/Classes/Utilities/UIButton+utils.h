//
//  UIButton+utils.h
//  RushOrder
//
//  Created by Conan on 11/19/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NPBadgeView.h"

@interface UIButton (utils)

@property NSString *buttonTitle;
//@property NSInteger badgeValue;
@property NSString *badgeValue;
@property (readonly) NPBadgeView *badgeView;

@property (nonatomic, copy) NSString *fontName;

// text title를 가진 버튼
+ (UIButton *) buttonWithFrame:(CGRect)frame
                         title:(NSString *)title
                          font:(UIFont *)font
                     textColor:(UIColor *)textColor
                        target:(id)target
                        action:(SEL)action;

// 일반 image를 가진 버튼
// frame은 기본적으로 (0, 0, normalImage.size.width, normalImage.size.height)
+ (UIButton *) buttonWithNormalImage:(UIImage *)normalImage
                    highlightedImage:(UIImage *)highlightedImage
                              target:(id)target
                              action:(SEL)action;

+ (UIButton *) buttonWithNormalImage:(UIImage *)normalImage
                    highlightedImage:(UIImage *)highlightedImage
                               frame:(CGRect)frame
                              target:(id)target
                              action:(SEL)action;

+ (UIButton *) buttonWithNormalImage:(UIImage *)normalImage
                    highlightedImage:(UIImage *)highlightedImage
                               frame:(CGRect)frame
                              target:(id)target
                              action:(SEL)action
                        controlEvent:(UIControlEvents)controlEvent;

// 버튼의 이미지 변경
- (void) changeNormalImage:(UIImage *)normalImage
          highlightedImage:(UIImage *)highlightedImage;


- (void)applyResizableImageFromCenter;
- (void)applyResizableImageFromCenterForState:(UIControlState)ctrlState;
- (void)applyResizableImage:(UIEdgeInsets)CapInsets;
- (void)applyResizableImage:(UIEdgeInsets)CapInsets forState:(UIControlState)ctrlState;
- (void)applyResizableImage:(UIEdgeInsets)CapInsets forEachState:(UIControlState)ctrlState;

- (void)applyResizableImageFromCenterForAllState;
- (void)applyResizableImageForAllState:(UIEdgeInsets)CapInsets;

- (void)sizeToFitReasonably;

@end
