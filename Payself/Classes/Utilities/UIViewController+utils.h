//
//  UIViewController+utils.h
//  RushOrder
//
//  Created by Conan on 2/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (utils)

@property (readonly) BOOL isRootViewController;

+ (id)viewControllerFromNib;
- (void)presentViewControllerInNavigation:(UIViewController *)viewControllerToPresent
                                 animated:(BOOL)flag
                               completion:(void (^)(void))completion;
@end