//
//  NSString+utils.h
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TableInfo.h"

@interface NSString (utils)

+ (NSString *)sqlStringWithBool:(BOOL)value;
+ (NSString *)stringWithBool:(BOOL)value;
+ (NSString *)stringWithEmptyBool:(BOOL)value;
+ (NSString *)stringWithInteger:(NSInteger)value;
+ (NSString *)stringWithInteger:(NSInteger)value allowZero:(BOOL)allowZero;
+ (NSString *)stringWithLongLong:(int64_t)value;
+ (NSString *)stringWithDouble:(double)value;
+ (NSString *)stringWithFloat:(float)value;

+ (NSString *)issuerLogoImageName:(Issuer)issuer;
+ (NSString *)issuerName:(Issuer)issuer;

- (NSDate *)dateWithYMD;
- (NSDate *)date;
- (NSDate *)dateFromFormatString:(NSString *)dateFormat;
- (CGSize)sizeWithFontOrAttributes:(UIFont *)font;


- (BOOL)hasPrefixes:(NSArray *)prefixList;
// Return substring from 0 to length
- (NSString *)substringWithLength:(NSInteger)length;
- (NSInteger)integerWithLength:(NSInteger)length;

- (NSString *)urlEncodedString;

- (Issuer)cardIssuer;

- (BOOL)isCaseInsensitiveEqual:(NSString *)other;

#pragma mark - validation
- (BOOL)isValidEmail;

- (OrderType)orderType;

@end
