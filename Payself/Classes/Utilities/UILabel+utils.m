//
//  UILabel+utils.m
//  RushOrder
//
//  Created by Conan on 2/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "UILabel+utils.h"


@implementation UILabel (utils)
@dynamic fontName;

- (void)sizeToHeightFit
{
    [self sizeToHeightFitLimitHeight:FLT_MAX];
}

- (void)sizeToHeightFitLimitHeight:(CGFloat)limitHeight
{
    NSString *text = self.text;
    CGSize constraintSize = CGSizeMake(self.frame.size.width,
                                       limitHeight);
    
    CGRect fitRect = [text boundingRectWithSize:constraintSize
                                        options:0
                                     attributes:@{NSFontAttributeName:self.font}
                                        context:nil];
    
    fitRect.size.width = self.frame.size.width;

    CGRect selfFrame = self.frame;
    selfFrame.size = fitRect.size;
    self.frame = selfFrame;
}

//- (NSString *)fontName
//{
//    return self.font.fontName;
//}
//
//- (void)setFontName:(NSString *)fontName
//{
//    self.font = [UIFont fontWithName:fontName size:self.font.pointSize];
//}
//
//- (void)setAttrText:(NSString *)attrText
//{
//    if(attrText == nil){
//        self.text = nil;
//        self.attributedText = nil;
//        return;
//    }    
//    
//    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];//[[NSParagraphStyle defaultParagraphStyle] mutableCopy];
//    style.lineSpacing = 4.0f;
//    style.lineBreakMode = NSLineBreakByTruncatingTail;
//    style.alignment = self.textAlignment;
//    NSDictionary *attrDictionary = @{
//                                     NSFontAttributeName : self.font,
//                                     NSParagraphStyleAttributeName : style
//                                     };
//    self.attributedText = [[NSAttributedString alloc] initWithString:attrText
//                                                          attributes:attrDictionary];
//    [self updateConstraints];
//}
//
//- (NSString *)attrText
//{
//    return self.text;
//}

@end
