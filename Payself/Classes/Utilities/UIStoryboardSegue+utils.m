//
//  UIStoryboardSegue+utils.m
//  RushOrder
//
//  Created by Conan on 9/16/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "UIStoryboardSegue+utils.h"

@implementation UIStoryboardSegue (utils)


- (id)isDestClass:(Class)aClass
{
    if([self.destinationViewController isKindOfClass:[UINavigationController class]]){
        UINavigationController *naviController = (UINavigationController *)self.destinationViewController;
        if([naviController.rootViewController isKindOfClass:aClass]){
            return naviController.rootViewController;
        }
    } else {
        if([self.destinationViewController isKindOfClass:aClass]){
            return self.destinationViewController;
        }
    }
    return nil;
}

@end
