//
//  NSUserDefaults+utils.m
//  RushOrder
//
//  Created by Conan on 11/20/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import "NSUserDefaults+utils.h"

NSString * const deviceTokenUserDefaultsKey = @"device_token";
NSString * const autoLoginUserDefaultsKey = @"auto_login";
NSString * const lastTrieduserIdUserDefaultsKey = @"last_tried_user_id";
NSString * const testDriveModeDefaultsKey = @"test_drive_mode";
NSString * const demoOnDefaultsKey = @"demo_restaurant_on";
NSString * const testOnDefaultsKey = @"test_restaurant_on";
NSString * const HideCoinKey = @"hide_coin";
NSString * const adoptLastTipRate = @"adoptLastTipRate";
NSString * const adoptLastDeliveryTipRate = @"adoptLastDeliveryTipRate";
NSString * const adoptLastTakeoutTipRate = @"adoptLastTakeoutTipRate";
NSString * const tipRateLastUsed = @"tipRateLastUsed";
NSString * const tipRateLastUsedDelivery = @"tipRateLastUsedDelivery";
NSString * const tipRateLastUsedTakeout = @"tipRateLastUsedTakeout";
NSString * const customRequestSynced = @"customRequestSynced";

NSString * const DemoRestaurantOnChangedNotification = @"DemoRestaurantOnChangedNotification";
NSString * const TestRestaurantOnChangedNotification = @"TestRestaurantOnChangedNotification";


@implementation NSUserDefaults (utils)


- (NSString *)deviceToken
{
    return [self valueForKey:deviceTokenUserDefaultsKey];
}

- (void)setDeviceToken:(NSString *)deviceToken
{
    [self setObject:deviceToken forKey:deviceTokenUserDefaultsKey];
}

- (BOOL)autoLogin
{
    return [self boolForKey:autoLoginUserDefaultsKey];
}

- (void)setAutoLogin:(BOOL)autoLogin
{
    [self setBool:autoLogin forKey:autoLoginUserDefaultsKey];
}

- (NSString *)lastTriedUserId
{
    return [self valueForKey:lastTrieduserIdUserDefaultsKey];
}

- (void)setLastTriedUserId:(NSString *)userId
{
    [self setObject:userId forKey:lastTrieduserIdUserDefaultsKey];
}


- (BOOL)isDemoOn
{
    return [self boolForKey:demoOnDefaultsKey];
}

- (void)setDemoOn:(BOOL)demoOn
{
    [self setBool:demoOn forKey:demoOnDefaultsKey];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:DemoRestaurantOnChangedNotification
                                                        object:self
                                                      userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:demoOn]
                                                                                           forKey:@"isDemoOn"]];
}

- (BOOL)isTestOn
{
    return [self boolForKey:testOnDefaultsKey];
}

- (void)setTestOn:(BOOL)testOn
{
    [self setBool:testOn forKey:testOnDefaultsKey];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:TestRestaurantOnChangedNotification
                                                        object:self
                                                      userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:testOn]
                                                                                           forKey:@"isTestOn"]];
}

- (BOOL)isHideCoin
{
    return [self boolForKey:HideCoinKey];
}

- (void)setHideCoin:(BOOL)hideCoin
{
    [self setBool:hideCoin forKey:HideCoinKey];
}




- (void)resetDatabase
{
    
}

- (NSDate *)dateQuerySettingDate
{
    return [self objectForKey:@"dateQuerySettingDate"];
}

- (NSDate *)startDate
{
    return [self objectForKey:@"startDate"];
}

- (NSDate *)endDate
{
    return [self objectForKey:@"endDate"];
}

- (void)setDateQuerySettingDate:(NSDate *)date
{
    [self setObject:date forKey:@"dateQuerySettingDate"];
}

- (void)setStartDate:(NSDate *)date
{
    [self setObject:date forKey:@"startDate"];
}

- (void)setEndDate:(NSDate *)date
{
    [self setObject:date forKey:@"endDate"];
}

- (NSDate *)lastNotificationTime
{
    return [self objectForKey:@"lastNotificationTime"];
}

- (void)setLastNotificationTime:(NSDate *)date
{
    [self setObject:date forKey:@"lastNotificationTime"];
}

- (BOOL)isPinSet
{
    return [self boolForKey:@"isPinSet"];
}

- (void)setPinSet:(BOOL)val
{
    [self setBool:val forKey:@"isPinSet"];
}

- (void)setVirtualAdded:(BOOL)val
{
    [self setBool:val forKey:@"isVirtualMerchantAdded"];
}

- (BOOL)isVirtualAdded
{
    return [self boolForKey:@"isVirtualMerchantAdded"];
}

- (void)setLastAccessedNoticeId:(PCSerial)noticeId
{
    [self setInteger:noticeId
              forKey:@"lastAccessedNoticeId"];
}

//- (PCSerial)lastLogedInMerchantId
//{
//    return [self integerForKey:@"lastLogedInMerchantId"];
//}
//
//- (void)setLastLogedInMerchantId:(PCSerial)lastLogedInMerchantId
//{
//    [self setInteger:lastLogedInMerchantId
//              forKey:@"lastLogedInMerchantId"];
//}

- (PCSerial)lastLogedInMerchantIdWithId:(NSString *)email
{
    NSDictionary *dict = [self dictionaryForKey:@"lastLogedInMerchantIds"];
 
    if(dict != nil){
        PCSerial merchantId = [dict currencyForKey:email];
        
        return merchantId;
    } else {
        return 0;
    }
}

- (void)setLastLogedInMerchantId:(PCSerial)lastLogedInMerchantId withId:(NSString *)email
{
    NSMutableDictionary *dict = [[self dictionaryForKey:@"lastLogedInMerchantIds"] mutableCopy];
    
    if(dict == nil){
        dict = [NSMutableDictionary dictionary];
    }
    
    [dict setValue:[NSNumber numberWithLongLong:lastLogedInMerchantId]
            forKey:email];
    
    [self setObject:dict
             forKey:@"lastLogedInMerchantIds"];
}


- (PCSerial)lastAccessedNoticeId
{
    return [self integerForKey:@"lastAccessedNoticeId"];
}

- (float)tipRate
{
    return [self floatForKey:tipRateLastUsed];
}

- (void)setTipRate:(float)tipRate
{
    [self setFloat:tipRate
            forKey:tipRateLastUsed];
}

- (float)tipTakeoutRate
{
    return [self floatForKey:tipRateLastUsedTakeout];
}

- (void)setTipTakeoutRate:(float)tipRate
{
    [self setFloat:tipRate
            forKey:tipRateLastUsedTakeout];
}

- (BOOL)customRequestSynced
{
    return [self boolForKey:customRequestSynced];
}

- (void)setCustomRequestSynced:(BOOL)value
{
    [self setBool:value
           forKey:customRequestSynced];
}


- (float)tipDeliveryRate
{
    return [self floatForKey:tipRateLastUsedDelivery];
}

- (void)setTipDeliveryRate:(float)tipRate
{
    [self setFloat:tipRate
            forKey:tipRateLastUsedDelivery];
}


- (BOOL)adoptLastTipRate
{
    return [self boolForKey:adoptLastTipRate];
}

- (void)setAdoptLastTipRate:(BOOL)val
{
    [self setBool:val
           forKey:adoptLastTipRate];
}

- (BOOL)adoptLastTakeoutTipRate
{
    return [self boolForKey:adoptLastTakeoutTipRate];
}

- (void)setAdoptLastTakeoutTipRate:(BOOL)val
{
    [self setBool:val
           forKey:adoptLastTakeoutTipRate];
}

- (BOOL)adoptLastDeliveryTipRate
{
    return [self boolForKey:adoptLastDeliveryTipRate];
}

- (void)setAdoptLastDeliveryTipRate:(BOOL)val
{
    [self setBool:val
           forKey:adoptLastDeliveryTipRate];
}

- (BOOL)didAlertPushNotification
{
    return [self boolForKey:@"didAlertPushNotification"];
}

- (void)setDidAlertPushNotification:(BOOL)val
{
    [self setBool:val
           forKey:@"didAlertPushNotification"];
}

- (BOOL)didUploadRecentAddresses
{
    return [self boolForKey:@"didUploadRecentAddresses"];
}

- (void)setDidUploadRecentAddresses:(BOOL)val
{
    [self setBool:val
           forKey:@"didUploadRecentAddresses"];
}

- (BOOL)autoKitchenPrint
{
    return [self boolForKey:@"autoKitchenPrint"];
}

- (void)setAutoKitchenPrint:(BOOL)value
{
    [self setBool:value
           forKey:@"autoKitchenPrint"];
}

- (BOOL)warnedServiceFee
{
    return [self boolForKey:@"warnedServiceFee"];
}

- (void)setWarnedServiceFee:(BOOL)value
{
    [self setBool:value
           forKey:@"warnedServiceFee"];
}

- (BOOL)autoBillPrint
{
    return [self boolForKey:@"autoBillPrint"];
}

- (void)setAutoBillPrint:(BOOL)value
{
    [self setBool:value
           forKey:@"autoBillPrint"];
}

- (BOOL)autoReceiptPrint
{
    return [self boolForKey:@"autoReceiptPrint"];
}

- (void)setAutoReceiptPrint:(BOOL)value
{
    [self setBool:value
           forKey:@"autoReceiptPrint"];
}

- (BOOL)walletCommercialClosed
{
    return [self boolForKey:@"walletCommercialClosed"];
}

- (void)setWalletCommercialClosed:(BOOL)value
{
    [self setBool:value
           forKey:@"walletCommercialClosed"];
}

- (NSInteger)lastUsedInterval
{
    return [self integerForKey:@"lastUsedInterval"];
}

- (void)setLastUsedInterval:(NSInteger)value
{
    [self setInteger:value
              forKey:@"lastUsedInterval"];
}


- (NSInteger)lastUsedDeliveryInterval
{
    return [self integerForKey:@"lastUsedDeliveryInterval"];
}

- (void)setLastUsedDeliveryInterval:(NSInteger)value
{
    [self setInteger:value
              forKey:@"lastUsedDeliveryInterval"];
}

- (void)setEmailAddress:(NSString *)emailAddress
{
    [self setObject:emailAddress
             forKey:@"emailAddressForReceipt"];
}

- (NSString *)emailAddress
{
    return [self objectForKey:@"emailAddressForReceipt"];
}

- (NSString *)custName
{
    return [self objectForKey:@"customerNamePlaceholder"];
}

- (void)setCustName:(NSString *)value
{
    [self setObject:value
             forKey:@"customerNamePlaceholder"];
}


- (void)setCustPhone:(NSString *)value
{
    [self setObject:value
             forKey:@"customerPhonePlaceholder"];
}

- (NSString *)custPhone
{
    return [self objectForKey:@"customerPhonePlaceholder"];
}

- (void)setAutoMailingMe:(BOOL)value
{
    [self setBool:value
           forKey:@"autoReceiptMailMe"];
}

- (BOOL)autoMailingMe
{
    return [self boolForKey:@"autoReceiptMailMe"];
}

- (void)setEmergencySignedOut:(BOOL)value
{
    [self setBool:value
           forKey:@"emergencySignedOut"];
}

- (BOOL)emergencySignedOut
{
    return [self boolForKey:@"emergencySignedOut"];
}

- (void)setNearbyCollapsed:(BOOL)value
{
    [self setBool:value
           forKey:@"nearbyCollapsed"];
}

- (BOOL)nearbyCollapsed
{
    return [self boolForKey:@"nearbyCollapsed"];
}

- (void)setVisitedCollapsed:(BOOL)value
{
    [self setBool:value
           forKey:@"visitedCollapsed"];
}

- (BOOL)visitedCollapsed
{
    return [self boolForKey:@"visitedCollapsed"];
}

- (void)setFavoraiteCollapsed:(BOOL)value
{
    [self setBool:value
           forKey:@"favoraiteCollapsed"];
}

- (BOOL)favoraiteCollapsed
{
    return [self boolForKey:@"favoraiteCollapsed"];
}

- (NSArray *)receiptPrinters
{
    return [self objectForKey:@"receiptPrinterList"];
}

- (void)setReceiptPrinters:(NSArray *)printers
{
    [self setObject:printers
             forKey:@"receiptPrinterList"];
}

- (BOOL)blinkWithNewOrder
{
    return [self boolForKey:@"blinkWithNewOrder"];
}

- (void)setBlinkWithNewOrder:(BOOL)value
{
    [self setBool:value
           forKey:@"blinkWithNewOrder"];
}

- (BOOL)directSwitchMenu
{
    return [self boolForKey:@"directSwitchMenu"];
}

- (void)setDirectSwitchMenu:(BOOL)value
{
    [self setBool:value
           forKey:@"directSwitchMenu"];
}


- (BOOL)oneTouchLocationUpdate
{
    return [self boolForKey:@"oneTouchLocationUpdate"];
}

- (void)setOneTouchLocationUpdate:(BOOL)value
{
    [self setBool:value
           forKey:@"oneTouchLocationUpdate"];
}

- (BOOL)showedIntroView
{
    return [self boolForKey:@"showedIntroView"];
}

- (void)setShowedIntroView:(BOOL)value
{
    [self setBool:value
           forKey:@"showedIntroView"];
}

- (BOOL)printWhenNewOrder
{
    return [self boolForKey:@"printWhenNewOrder"];
}

- (void)setPrintWhenNewOrder:(BOOL)value
{
    [self setBool:value
           forKey:@"printWhenNewOrder"];
}


- (PCSerial)lastNewPrintedNumber
{
    return [((NSNumber *)[self objectForKey:@"lastNewPrintedNumber"]) serialValue];
}

- (void)setLastNewPrintedNumber:(PCSerial)value
{
    [self setObject:[NSNumber numberWithSerial:value]
                                        forKey:@"lastNewPrintedNumber"];
}

//lastNewPrintedCartNumber
- (PCSerial)lastNewPrintedCartNumber
{
    return [((NSNumber *)[self objectForKey:@"lastNewPrintedCartNumber"]) serialValue];
}

- (void)setLastNewPrintedCartNumber:(PCSerial)value
{
    [self setObject:[NSNumber numberWithSerial:value]
             forKey:@"lastNewPrintedCartNumber"];
}

- (PCSerial)lastPaymentPrintedNumber
{
    return [((NSNumber *)[self objectForKey:@"lastPaymentPrintedNumber"]) serialValue];
}

- (void)setLastPaymentPrintedNumber:(PCSerial)value
{
    [self setObject:[NSNumber numberWithSerial:value]
             forKey:@"lastPaymentPrintedNumber"];
}

@end
