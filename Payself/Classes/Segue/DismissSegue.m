//
//  DismissSegue.m
//  mintmate
//
//  Created by Conan on 8/11/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import "DismissSegue.h"

@implementation DismissSegue

- (void)perform
{
    UIViewController *sourceViewController = self.sourceViewController;
    [sourceViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end