//
//  BackSegue.m
//  mintmate
//
//  Created by Conan on 8/11/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import "BackSegue.h"

@implementation BackSegue

- (void)perform {
    UIViewController *sourceViewController = self.sourceViewController;
    [sourceViewController.navigationController popViewControllerAnimated:YES];
}

@end
