//
//  MenuOptionGroup.m
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MenuOptionGroup.h"
#import "MenuOption.h"

@implementation MenuOptionGroup

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [self init];
    
    if(self != nil){
        self.optionGroupNo = [dict serialForKey:@"id"];
        self.name = [dict objectForKey:@"name"];
        if([[dict objectForKey:@"menu_option_group_type"] isEqual:@"additional"]){
            self.optionType = MenuOptionTypeAdd;
        } else if([[dict objectForKey:@"menu_option_group_type"] isEqual:@"choice"]){
            self.optionType = MenuOptionTypeChoose;
        }
        
        self.options = [NSMutableArray array];
        
        NSArray *menuOptions = [dict objectForKey:@"menu_options"];
        
        self.maxSelect = [dict integerForKey:@"max_select"];
        
        for(NSDictionary *dict in menuOptions){
            MenuOption *menuOption = [[MenuOption alloc] initWithDictionary:dict];
            if(menuOption.price > 0){
                self.priceSensitive = YES;
            }
            [self.options addObject:menuOption];
        }
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    MenuOptionGroup *copy = [[[self class] alloc] init];
    
    if (copy) {
        copy.optionGroupNo = self.optionGroupNo;
        copy.name = self.name;
        copy.desc = self.desc;
        copy.optionType = self.optionType;
    }
    
    return copy;
}

- (NSString *)optionTypeString
{
    switch(self.optionType){
        case MenuOptionTypeAdd:
            return @"additional";
            break;
        case MenuOptionTypeChoose:
            return @"choice";
            break;
    }
    return nil;
}

@end
