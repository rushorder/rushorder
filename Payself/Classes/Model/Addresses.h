//
//  Addresses.h
//  RushOrder
//
//  Created by Conan on 3/3/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import "PCModel.h"
#import "Order.h"

@interface Addresses : PCModel <PCModelProtocol>

@property (nonatomic) PCSerial addressNo;
@property (nonatomic, copy) NSString *receiverName;
@property (nonatomic, copy) NSString *address1;
@property (nonatomic, copy) NSString *address2;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *zip;
@property (nonatomic, copy) NSString *phoneNumber;
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic) CLLocationCoordinate2D takenCoordinate;
@property (strong, nonatomic) NSDate *lastUsedDate;
@property (nonatomic, getter = isNeedToBeUpdatedToDefault) BOOL needToBeUpdatedToDefault;

@property (readonly) NSString *address;
@property (readonly) NSString *fullAddress;


- (id)initWithDictionary:(NSDictionary *)dict;
- (id)initWithOrder:(Order *)order;
- (void)updateWithDictionary:(NSDictionary *)dict;
- (void)updateWithOrder:(Order *)order;

- (Addresses *)duplicatedOne;
- (NSDictionary *)dictRep;


@end
