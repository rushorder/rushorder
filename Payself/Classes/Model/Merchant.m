//
//  Merchant.m
//  RushOrder
//
//  Created by Conan on 2/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "Merchant.h"
#import "DZonePolygon.h"
#import "DZoneRadius.h"


#define INSERT_QRY @"insert into 'Merchant'(\
merchantNo\
,name\
,desc\
,address\
,geoAddress\
,displayGeoAddress\
,numberOfTables\
,latitude\
,longitude\
,imageURL\
,logoURL\
,menuOrder\
,unableDirectPay\
,tableBase\
,lastVisitedDate\
,demo\
,ableTakeout\
,ableDelivery\
,servicedBy\
,roService\
,roServiceRate\
,roServiceMinAmount\
,taxRate\
,deliveryFee\
,deliveryMinOrderAmount\
,dineinMinOrderAmount\
,takeoutMinOrderAmount\
,deliveryDistanceLimit\
,allowTip\
,lastMenuModifiedDate\
,cachedType)\
values \
(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

#define UPDATE_QRY @"update 'Merchant' set \
name = ?\
,desc = ?\
,address = ?\
,geoAddress = ?\
,displayGeoAddress = ?\
,numberOfTables = ?\
,latitude = ?\
,longitude = ?\
,imageURL = ?\
,logoURL = ?\
,menuOrder = ?\
,unableDirectPay = ?\
,tableBase = ?\
,lastVisitedDate = ?\
,demo = ?\
,ableTakeout = ?\
,ableDelivery = ?\
,servicedBy = ?\
,roService = ?\
,roServiceRate = ?\
,roServiceMinAmount = ?\
,taxRate = ?\
,deliveryFee = ?\
,deliveryMinOrderAmount = ?\
,dineinMinOrderAmount = ?\
,takeoutMinOrderAmount = ?\
,deliveryDistanceLimit = ?\
,allowTip = ?\
,lastMenuModifiedDate = ?\
,cachedType = ?\
 where merchantNo = ?"


@implementation Merchant

+ (BOOL)changeToUnknownMerchant
{
    //DELETE FROM MobileCard where rowId=3
    NSString *fmtQry = @"Update Merchant set cachedType=0";
    
    if([DB.db open]){
        return [DB.db executeUpdate:fmtQry];
    } else {
        PCError(@"changeToUnknownMerchant error");
        return NO;
    }
}

- (id)init
{
    self = [super init];
    if(self != nil){
        self.tableBase = YES;
        self.menuOrder = YES;
        self.ablePayment = YES;
        self.testMode = YES;
        self.open = YES;
        self.cellHeight = FLT_MAX;
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    Merchant *copy = [[[self class] alloc] init];
    
    if (copy) {
        copy.allPropertySet = self.isAllPropertySet;
        copy.merchantNo = self.merchantNo;
        copy.name = self.name;
        copy.address = self.address;
        copy.geoAddress = self.geoAddress;
        copy.displayGeoAddress = self.displayGeoAddress;
        copy.desc = self.desc;
        copy.numberOfTables = self.numberOfTables;
        copy.ownerName = self.ownerName;
        copy.phoneNumber = self.phoneNumber;
        copy.roPhoneNumber = self.roPhoneNumber;
        copy.location = self.location;
        copy.distance = self.distance;
        copy.distanceFromServer = self.distanceFromServer;
        copy.deliveryDistance = self.deliveryDistance;
        copy.lastVisitedDate = self.lastVisitedDate;
        copy.imageURL = self.imageURL;
        copy.photoURLs = self.photoURLs;
        copy.logoURL = self.logoURL;
        copy.taxRate = self.taxRate;
        copy.deliveryFee = self.deliveryFee;
        copy.deliveryMinOrderAmount = self.deliveryMinOrderAmount;
        copy.dineinMinOrderAmount = self.dineinMinOrderAmount;
        copy.takeoutMinOrderAmount = self.takeoutMinOrderAmount;
        copy.deliveryDistanceLimit = self.deliveryDistanceLimit;
        copy.allowTip = self.isAllowTip;
        copy.lastMenuModifiedDate = self.lastMenuModifiedDate;
        copy.authenticated = self.isAuthenticated;
        copy.menuOrder = self.isMenuOrder;
        copy.ablePayment = self.isAblePayment;
        copy.tableBase = self.isTableBase;
        copy.open = self.isOpen;
        copy.takeoutOpen = self.isTakeoutOpen;
        copy.published = self.isPublished;
        copy.testMode = self.isTestMode;
        copy.demoMode = self.isDemoMode;
        copy.creditPolicyName = self.creditPolicyName;
        copy.promotions = self.promotions;
        copy.ein = self.ein;
        copy.bankAccountOwnerName = self.bankAccountOwnerName;
        copy.bankAccountNumber = self.bankAccountNumber;
        copy.bankRoutingNumber = self.bankRoutingNumber;
        copy.servicedBy = self.servicedBy;
        copy.roService = self.isRoService;
        copy.roServiceRate = self.roServiceRate;
        copy.ableTakeout = self.isAbleTakeout;
        copy.ableDelivery = self.isAbleDelivery;
        copy.takeoutRemark = self.takeoutRemark;
        copy.hours = self.hours;
        copy.homepage = self.homepage;
        copy.cellHeight = self.cellHeight;
        copy.nonInteractive = self.isNonInteractive;
        copy.multilineAddress = self.multilineAddress;
        
        copy.deliveryFeePlan = self.deliveryFeePlan;
        copy.deliveryFeeZone = self.deliveryFeeZone;
        copy.deliveryFeeDiscount = self.deliveryFeeDiscount;
        
        copy.operationHours = self.operationHours;
        copy.deliveryHours = self.deliveryHours;
        copy.intersectHours = self.intersectHours;
        copy.lastOperationDate = self.lastOperationDate;
        copy.pickType = self.pickType;
        
        copy.autoOpenClose = self.autoOpenClose;
        copy.autoDeliveryHours = self.autoDeliveryHours;
        copy.deliveryFeeAmountForBigOrder = self.deliveryFeeAmountForBigOrder;
        copy.deliveryBigOrderAmount = self.deliveryBigOrderAmount;
        copy.tableList = self.tableList;
        
        copy.openStatus = self.openStatus;
        copy.deliveryOpenStatus = self.deliveryOpenStatus;
        
        copy.timeZone = self.timeZone;
        
    }
    
    return copy;
}

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [self init];
    if(self != nil){
        [self updateWithDictionary:dict];
    }
    return self;
}

- (void)updateWithDictionary:(NSDictionary *)dict
{
    self.allPropertySet = ([dict objectForKey:@"open"] != nil);
    self.merchantNo = [dict serialForKey:@"id"];
    self.name = [dict objectForKey:@"name"];
    self.ownerName = [dict objectForKey:@"ownername"];
    self.address = [dict objectForKey:@"address"];
    self.geoAddress = [dict objectForKey:@"geo_address"];
    self.displayGeoAddress = [dict boolForKey:@"display_geo_address"];
    self.desc = [dict objectForKey:@"description"];
    self.location = [[CLLocation alloc] initWithLatitude:[[dict objectForKey:@"latitude"] doubleValue]
                                               longitude:[[dict objectForKey:@"longitude"] doubleValue]];
    self.distanceFromServer = [[dict objectForKey:@"distance"] doubleValue];
    self.testMode = [[dict objectForKey:@"mode"] isEqualToString:@"demo"];
    self.phoneNumber = [dict objectForKey:@"phone"];
    self.roPhoneNumber = [dict objectForKey:@"ro_service_actual_phone"];
    self.numberOfTables = [[dict objectForKey:@"numoftable"] integerValue];
    
    self.open = [[dict objectForKey:@"open"] boolValue];
    self.takeoutOpen = [[dict objectForKey:@"takeout_open"] boolValue];
    self.published = [[dict objectForKey:@"published"] boolValue];
    
    self.lastVisitedDate = [NSDate date];
    
    self.bankAccountNumber = [dict objectForKey:@"bank_account_number"];
    self.bankAccountOwnerName = [dict objectForKey:@"bank_account_owner_name"];
    self.bankRoutingNumber = [dict objectForKey:@"bank_routing_number"];
    self.ein = [dict objectForKey:@"ein"];
    
    self.price = [dict integerForKey:@"price"];
    
    self.authenticated = [dict boolForKey:@"authenticated"];
    //
    NSDictionary *currentCreditPolicyDict = [dict objectForKey:@"current_credit_policy"];
    self.creditPolicyName = [currentCreditPolicyDict objectForKey:@"name"];
    
    NSArray *promotionsArray = [dict objectForKey:@"current_promotions"];
    
    if([promotionsArray count] > 0){
        self.promotions = [NSMutableArray array];
        
        for(NSDictionary *promotionDict in promotionsArray){
            Promotion *promotion = [[Promotion alloc] initWithDictionary:promotionDict];
            [self.promotions addObject:promotion];
        }
    }
    self.demoMode = [dict boolForKey:@"virtual"];
    
    self.taxRate = [dict floatForKey:@"tax_rate"];
    self.deliveryFee = [dict floatForKey:@"delivery_fee_flat"];
    self.deliveryMinOrderAmount = [dict floatForKey:@"delivery_min_order_amount"];
    self.dineinMinOrderAmount = [dict floatForKey:@"dinein_min_order_amount"];
    self.takeoutMinOrderAmount = [dict floatForKey:@"takeout_min_order_amount"];
    self.deliveryDistanceLimit = [dict floatForKey:@"delivery_distance"];
    self.allowTip = [dict boolForKey:@"tip_enable"];
    
    self.openStatus = [dict boolForKey:@"open_status"];
    self.deliveryOpenStatus = [dict boolForKey:@"delivery_open_status"];

    self.lastMenuModifiedDate = [dict dateForKey:@"menu_last_modified_at"];
    
    NSString *storeURLString = [[[dict objectForKey:@"store_image"] objectForKey:@"thumb"] objectForKey:@"url"];
    
    if([storeURLString length] == 0){
        storeURLString = [[dict objectForKey:@"store_image"] objectForKey:@"url"];
    }
    
    self.imageURL = [NSURL URLWithString:storeURLString];
    
    NSArray *restaurantPhotos = [dict objectForKey:@"restaurant_photos"];
    
    if(restaurantPhotos != nil){
        self.photoURLs = [NSMutableArray array];
        
        for(NSDictionary *photoDict in restaurantPhotos){
            NSString *storeURLString = [[[photoDict objectForKey:@"store_image"] objectForKey:@"thumb"] objectForKey:@"url"];
            
            if([storeURLString length] == 0){
                storeURLString = [[dict objectForKey:@"store_image"] objectForKey:@"url"];
            }
            
            if(storeURLString != nil){
                [self.photoURLs addObject:[NSURL URLWithString:storeURLString]];
            }
        }
    }
    
    
    NSString *logoURLString = [[[dict objectForKey:@"logo"] objectForKey:@"thumb"] objectForKey:@"url"];
    
    if([logoURLString length] == 0){
        logoURLString = [[dict objectForKey:@"logo"] objectForKey:@"url"];
    }
    
    if([logoURLString length] > 0){
        self.logoURL = [NSURL URLWithString:logoURLString];
    }
    
    self.tableBase = ![dict boolForKey:@"accept_to_go"];
    
    if(self.isTableBase){
        self.menuOrder = [dict boolForKey:@"accept_menu_pay"]; //Waiter-Service 없음
        self.ablePayment = [dict boolForKey:@"accept_pay_only"];
    } else {
        self.menuOrder = YES;
        self.ablePayment = YES;
    }
    
    self.ableTakeout = [dict boolForKey:@"accept_takeout"];
    self.ableDelivery = [dict boolForKey:@"accept_delivery"];
    
    NSString *servicedByString = [dict objectForKey:@"served_by"];
    
    if([servicedByString isEqualToString:@"server"]){
        self.servicedBy = ServicedByStaff;
    } else if([servicedByString isEqualToString:@"self"]){
        self.servicedBy = ServicedBySelf;
    } else if([servicedByString isEqualToString:@"none"]){
        self.servicedBy = ServicedByNone;
    } else {
        self.servicedBy = ServicedByUnknown;
    }
    
    self.roService = [dict boolForKey:@"is_ro_service"];
    self.roServiceRate = [dict floatForKey:@"ro_service_fee_rate"];
    self.roServiceMinAmount = [dict currencyForKey:@"min_ro_service_fee"];
    
    self.takeoutRemark = [dict objectForKey:@"takeout_remark"];
    self.hours = [dict objectForKey:@"hours"];
    self.homepage = [dict objectForKey:@"homepage"];
    self.email = [dict objectForKey:@"email"];
    
    self.country = [dict objectForKey:@"address_country"];
    self.address1 = [dict objectForKey:@"address_street_1"];
    self.address2 = [dict objectForKey:@"address_street_2"];
    self.city = [dict objectForKey:@"address_city"];
    self.state = [dict objectForKey:@"address_state"];
    self.zip = [dict objectForKey:@"address_zipcode"];
    
    self.nonInteractive = [dict boolForKey:@"non_interactive"];
    
    self.averageDeliveryTime = [dict integerForKey:@"avg_delivery_time"];
    
//    PCLog(@"averageDeliveryTime %d", self.averageDeliveryTime);
    
    NSString *deliveryFeeStrategy = [dict objectForKey:@"delivery_strategy"];
        
    if([deliveryFeeStrategy isEqual:@"flat"]){
        self.deliveryFeePlan = DeliveryFeePlanFlat;
    } else if([deliveryFeeStrategy isEqual:@"polygon"]){
        self.deliveryFeePlan = DeliveryFeePlanZone;
    } else if([deliveryFeeStrategy isEqual:@"radius"]){
        self.deliveryFeePlan = DeliveryFeePlanRadius;
    } else {
        self.deliveryFeePlan = DeliveryFeePlanUnknown;
    }
    
    NSString *timeZoneName = [dict objectForKey:@"time_zone"];
    if([timeZoneName length] > 0){
        self.timeZone = [NSTimeZone timeZoneWithName:timeZoneName];
    }
    
    self.cellHeight = FLT_MAX;
    self.multilineAddress = [dict objectForKey:@"address_multiline"];
    
    NSArray *compTableList = [dict objectForKey:@"table_list"];
    if(compTableList != nil){
        // TODO: Implemented
        
        self.tableList = [NSMutableArray array];
        for(NSString *tableLabel in compTableList){
            TableInfo *tableInfo = [[TableInfo alloc] initWithTableLabel:tableLabel
                                                              merchantNo:self.merchantNo];
            [self.tableList addObject:tableInfo];
        }
    }
    
    self.autoOpenClose = [dict boolForKey:@"auto_open_close"];
    self.autoDeliveryHours = ![dict boolForKey:@"is_always_deliverable"];
    
    //Operation Hours
    NSArray *operationHourArray = [dict objectForKey:@"operation_hours"];
    if([operationHourArray count] > 0){
        self.operationHours = [NSMutableArray array];
        for(NSDictionary *hoursDict in operationHourArray){
            OperationHour *hour = [[OperationHour alloc] init];
            hour.eventTime = [hoursDict integerForKey:@"open_time"];
            hour.operationEvent = OperationEventOpen;
            hour.openSpanDict = hoursDict;
            [self addOperationEvent:hour inArray:self.operationHours];
            
            hour = [[OperationHour alloc] init];
            hour.eventTime = [hoursDict integerForKey:@"close_time"];
            hour.operationEvent = OperationEventClose;
            hour.openSpanDict = hoursDict;
            [self addOperationEvent:hour inArray:self.operationHours];
        }
    }
    
    NSArray *deliveryHourArray = [dict objectForKey:@"delivery_hours"];
    if([deliveryHourArray count] > 0){
        self.deliveryHours = [NSMutableArray array];
        for(NSDictionary *hoursDict in deliveryHourArray){
            OperationHour *hour = [[OperationHour alloc] init];
            hour.eventTime = [hoursDict integerForKey:@"start_time"];
            hour.operationEvent = OperationEventOpen;
            hour.openSpanDict = hoursDict;
            [self addOperationEvent:hour inArray:self.deliveryHours];
            
            hour = [[OperationHour alloc] init];
            hour.eventTime = [hoursDict integerForKey:@"end_time"];
            hour.operationEvent = OperationEventClose;
            hour.openSpanDict = hoursDict;
            [self addOperationEvent:hour inArray:self.deliveryHours];
        }
    }
    
    self.intersectHours = [self intersectOperationHours];
    
    self.lastOperationDate = [dict dateForKey:@"last_modified_open_close_at"];
    self.pickType = (PickType)[dict integerForKey:@"pick_type"];
    
//    NSDate *lastOperationDate = [dict dateForKey:@"last_modified_open_close_at"];
////    lastOperationDate =
//    
//    PCLog(@"LOD : %@, %@ (%@)", [dict dateForKey:@"last_modified_open_close_at"], lastOperationDate, self.name);
//    
//    NSTimeInterval interval = [lastOperationDate timeIntervalSinceNow];
//    
//    if(interval < ((60 * 60 * 24) * 7)){
//        OperationHour *hour = [[OperationHour alloc] init];
//        
//        NSCalendar *calendar = [NSCalendar currentCalendar];
//        calendar.timeZone = self.timeZone;
//        NSDateComponents *components = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekday
//                                                   fromDate:lastOperationDate];
////        components.timeZone = self.timeZone;
//        NSInteger weekday = (components.weekday - 1);
//        if(weekday == 0) weekday = 7;
//        
//        hour.eventTime = [[NSString stringWithFormat:@"%d%02d%02d",
//                           weekday,
//                           components.hour,
//                           components.minute] integerValue];
//        
//        hour.operationEvent = self.isOpen ? OperationEventOpen : OperationEventClose;
//        [self addOperationEvent:hour];
//    }
//    
//    PCLog(@"(%@, %@) OperationHours : %@\n LastModified:%@, %d", self.name, self.timeZone, self.operationHours, self.lastOperationDate, self.isOpen);
    
    //Delivery Zone
    // TODO: Sorting by priority (if Polygon), by distance (if Radius)
    if(self.deliveryFeePlan == DeliveryFeePlanZone || self.deliveryFeePlan == DeliveryFeePlanRadius){
        PCLog(@"Delivery Zone for %@\n Zones:%@\n DF St:%d", self, [dict objectForKey:@"delivery_zones"], self.deliveryFeePlan);
        
        NSArray *zonesArray = [dict objectForKey:@"delivery_zones"];
        if([zonesArray count] > 0){
            self.deliveryFeeZone = [NSMutableArray array];
            for(NSDictionary *zoneDict in zonesArray){
                switch(self.deliveryFeePlan){
                    case DeliveryFeePlanZone:
                    {
                        DZonePolygon *zonePolygon = [[DZonePolygon alloc] initWithDictionary:zoneDict];
                        NSUInteger index = [self.deliveryFeeZone indexOfObject:zonePolygon
                                                                 inSortedRange:NSMakeRange(0, [self.deliveryFeeZone count])
                                                                       options:NSBinarySearchingFirstEqual | NSBinarySearchingInsertionIndex
                                                               usingComparator:^NSComparisonResult(DZonePolygon * obj1, DZonePolygon * obj2) {
                                                                   if(obj1.priority > obj2.priority){
                                                                       return NSOrderedDescending;
                                                                   } else if(obj1.priority < obj2.priority){
                                                                       return NSOrderedAscending;
                                                                   } else {
                                                                       return NSOrderedSame;
                                                                   }
                                                               }];
                        
                        [self.deliveryFeeZone insertObject:zonePolygon
                                                   atIndex:index];
                    }
                        break;
                    case DeliveryFeePlanRadius:
                    {
                        DZoneRadius *zoneRadius = [[DZoneRadius alloc] initWithDictionary:zoneDict];
                        
                        NSUInteger index = [self.deliveryFeeZone indexOfObject:zoneRadius
                                                                 inSortedRange:NSMakeRange(0, [self.deliveryFeeZone count])
                                                                       options:NSBinarySearchingFirstEqual | NSBinarySearchingInsertionIndex
                                                               usingComparator:^NSComparisonResult(DZoneRadius * obj1, DZoneRadius * obj2) {
                                                                   if(obj1.radius > obj2.radius){
                                                                       return NSOrderedDescending;
                                                                   } else if(obj1.radius < obj2.radius){
                                                                       return NSOrderedAscending;
                                                                   } else {
                                                                       return NSOrderedSame;
                                                                   }
                                                               }];
                        
                        [self.deliveryFeeZone insertObject:zoneRadius
                                                   atIndex:index];
                        break;
                    }
                    default:
                        PCWarning(@"delivery plan is %d, ignoring zones", self.deliveryFeePlan);
                        break;\
                }
            }
            
    //        PCLog(@"Delivery Zone!!! : %@", self.deliveryFeeZone);
        } else {
            self.deliveryFeeZone = nil;
        }
    } else {
        self.deliveryFeeZone = nil;
    }
    
    self.deliveryFeeDiscount = [dict objectForKey:@"delivery_fee_discounts"];
    
    self.deliveryFeeAmountForBigOrder = [dict currencyForKey:@"delivery_fee_amount_for_big_order"];
    self.deliveryBigOrderAmount = [dict currencyForKey:@"delivery_big_order_amount"];
}


- (BOOL)isOpenHour
{
#if ALL_PASS
    return YES;
#else
    return [self isOpenHourWithDate:[NSDate date]];
#endif
}

- (BOOL)isOpenHourWithDate:(NSDate *)date
{
#if ALL_PASS
    return YES;
#else
    if([self.operationHours count] > 0 && self.autoOpenClose){
        NSCalendar *calendar = [NSCalendar currentCalendar];
        calendar.timeZone = self.timeZone;
        
        NSDateComponents *currentComponents = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekday
                                                   fromDate:date];
        NSInteger weekday = (currentComponents.weekday - 1);
        if(weekday == 0) weekday = 7;
        
        OperationHour *currentHour = [[OperationHour alloc] init];
        currentHour.eventTime = [[NSString stringWithFormat:@"%ld%02ld%02ld",
                              (long)weekday,
                              (long)currentComponents.hour,
                              (long)currentComponents.minute] integerValue];
        
        NSComparator operationHourComparison = ^NSComparisonResult(OperationHour *obj1, OperationHour *obj2){
//            PCLog(@"o1e:%d / o2e:%d", obj1.eventTime, obj2.eventTime);
            if(obj1.eventTime > obj2.eventTime){
                return NSOrderedDescending;
            } else if(obj1.eventTime < obj2.eventTime){
                return NSOrderedAscending;
            } else {
                if(obj1 == currentHour) return NSOrderedDescending;
                else if(obj2 == currentHour) return NSOrderedAscending;
                else{
                    if(obj1.openSpanDict == nil) {
                        return NSOrderedDescending;
                    } else if(obj2.openSpanDict == nil){
                        return NSOrderedAscending;
                    } else {
                        if(obj1.operationEvent == OperationEventOpen){
                            return NSOrderedDescending;
                        } else if (obj1.operationEvent == OperationEventClose){
                            return NSOrderedAscending;
                        } else {
                            return NSOrderedSame;
                        }
                    }
                }
            }
        };
        
        OperationHour *lastOperationHour = nil;
        
        NSTimeInterval interval = 100000000;
        if(self.lastOperationDate != nil){
            interval = [self.lastOperationDate timeIntervalSinceNow];
            interval *= -1; //Make positive value
        }
        
        if(interval < ((60 * 60 * 24) * 7)){
            lastOperationHour = [[OperationHour alloc] init];
            NSDateComponents *lastOperationComponents = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekday
                                                                    fromDate:self.lastOperationDate];
            
            NSInteger weekday = (lastOperationComponents.weekday - 1);
            if(weekday == 0) weekday = 7;
            
            lastOperationHour.eventTime = [[NSString stringWithFormat:@"%ld%02ld%02ld",
                                            (long)weekday,
                                            (long)lastOperationComponents.hour,
                                            (long)lastOperationComponents.minute] integerValue];
            
            lastOperationHour.operationEvent = self.isOpen ? OperationEventOpen : OperationEventClose;
            
        } else {
            lastOperationHour = nil;
        }
        
        NSMutableArray *tempOperationHours = [NSMutableArray arrayWithArray:self.operationHours];
        
        NSUInteger firstMatchingIndex = NSNotFound;
        
        if(lastOperationHour != nil){
            firstMatchingIndex = [tempOperationHours indexOfObject:lastOperationHour
                                                     inSortedRange:NSMakeRange(0, [tempOperationHours count])
                                                           options:NSBinarySearchingInsertionIndex | NSBinarySearchingFirstEqual
                                                   usingComparator:operationHourComparison];
            if(firstMatchingIndex != NSNotFound){
                [tempOperationHours insertObject:lastOperationHour
                                         atIndex:firstMatchingIndex];
            }
        }
        
        firstMatchingIndex = [tempOperationHours indexOfObject:currentHour
                                                  inSortedRange:NSMakeRange(0, [tempOperationHours count])
                                                        options:NSBinarySearchingInsertionIndex | NSBinarySearchingFirstEqual
                                                usingComparator:operationHourComparison];
        
        if(firstMatchingIndex == 0){
            firstMatchingIndex = ([tempOperationHours count] - 1);
        } else {
            firstMatchingIndex -= 1;
        }
        
        
        if(firstMatchingIndex >= ([tempOperationHours count] - 1)){
            self.nearestAvailableTimeIndex = 0;
        } else {
            self.nearestAvailableTimeIndex = firstMatchingIndex + 1;
        }
        
        
        ///////////////////////////////////////////////////////////////////////
        
        OperationHour *nextHour = [tempOperationHours objectAtIndex:self.nearestAvailableTimeIndex];
        
        self.nearestOperationHour = nextHour;
        
        if(nextHour == nil){
            self.nextAvailableTime = nil;
        } else {
        
            NSNumber *startTimeNumber = [nextHour.openSpanDict objectForKey:@"open_time"];
            NSNumber *endTimeNumber = [nextHour.openSpanDict objectForKey:@"close_time"];
            NSString *startTime = [startTimeNumber stringValue];
            NSString *endTime = [endTimeNumber stringValue];
            NSString *startWeekString = [startTime substringToIndex:1];
            NSString *endWeekString = [endTime substringToIndex:1];
            NSString *startHourString = [startTime substringWithRange:NSMakeRange(1, 2)];
            NSString *endHourString = [endTime substringWithRange:NSMakeRange(1, 2)];
            NSString *startMinString = [startTime substringWithRange:NSMakeRange(3, 2)];
//            NSString *endMinString = [endTime substringWithRange:NSMakeRange(3, 2)];
            
            if([startHourString integerValue] == 24){
                NSInteger weekVal = [startWeekString integerValue];
                weekVal++;
                if(weekVal > 7) weekVal = 1;
                startWeekString = [NSString stringWithInteger:weekVal];
            }
            
            if([endHourString integerValue] == 24){
                NSInteger weekVal = [endWeekString integerValue];
                weekVal++;
                if(weekVal > 7) weekVal = 1;
                endWeekString = [NSString stringWithInteger:weekVal];
            }
            
            
            NSString *startAmPmHour = [self amPmStringWithHour:[startHourString integerValue]
                                                        minute:[startMinString integerValue]];
            
//            NSString *endAmPmHour = [self amPmStringWithHour:[endHourString integerValue]
//                                                      minute:[endMinString integerValue]];
            
//            self.nextAvailableTime = [NSString stringWithFormat:@"%@ %@ - %@ %@",
//                    [self weekStringAtIndex:[startWeekString integerValue]],
//                    startAmPmHour,
//                    [self weekStringAtIndex:[endWeekString integerValue]],
//                    endAmPmHour];
            
            self.nextAvailableTime = [NSString stringWithFormat:@"Next Available Time\n%@ at %@",
                                      [self weekStringAtIndexWithHumanReadable:[startWeekString integerValue] withCurrentWeekay:weekday
                                                                     timeValue:startTimeNumber.integerValue],
                                      startAmPmHour];
        }

        ///////////////////////////////////////////////////////////////////////
        
        
        OperationHour *prevHour = [tempOperationHours objectAtIndex:firstMatchingIndex];
        
        return (prevHour.operationEvent == OperationEventOpen);
                               
    } else {
        if(self.isAllPropertySet){
            return self.isOpen;
        } else {
            return self.openStatus;
        }
    }
#endif
}

- (BOOL)isDeliveryHour
{
#if ALL_PASS
    return YES;
#else
    return [self isDeliveryHourWithDate:[NSDate date]];
#endif
}

- (BOOL)isDeliveryHourWithDate:(NSDate *)date
{
#if ALL_PASS
    return YES;
#else
    if(!self.autoDeliveryHours){
        return YES;
    }
    
    if([self.deliveryHours count] > 0){
        NSCalendar *calendar = [NSCalendar currentCalendar];
        calendar.timeZone = self.timeZone;
        
        NSDateComponents *currentComponents = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekday
                                                          fromDate:date];
        NSInteger weekday = (currentComponents.weekday - 1);
        if(weekday == 0) weekday = 7;
        
        OperationHour *currentHour = [[OperationHour alloc] init];
        currentHour.eventTime = [[NSString stringWithFormat:@"%ld%02ld%02ld",
                                  (long)weekday,
                                  (long)currentComponents.hour,
                                  (long)currentComponents.minute] integerValue];
        
        NSComparator operationHourComparison = ^NSComparisonResult(OperationHour *obj1, OperationHour *obj2){
            if(obj1.eventTime > obj2.eventTime){
                return NSOrderedDescending;
            } else if(obj1.eventTime < obj2.eventTime){
                return NSOrderedAscending;
            } else {
                if(obj1 == currentHour) return NSOrderedDescending;
                else if(obj2 == currentHour) return NSOrderedAscending;
                else{
                    if(obj1.openSpanDict == nil) return NSOrderedDescending;
                    else if(obj2.openSpanDict == nil) return NSOrderedAscending;
                    else {
                        if(obj1.operationEvent == OperationEventOpen){
                            return NSOrderedDescending;
                        } else if (obj1.operationEvent == OperationEventClose){
                            return NSOrderedAscending;
                        } else {
                            return NSOrderedSame;
                        }
                    }
                }
            }
        };
        
        NSUInteger firstMatchingIndex = NSNotFound;
        
        firstMatchingIndex = [self.deliveryHours indexOfObject:currentHour
                                                 inSortedRange:NSMakeRange(0, [self.deliveryHours count])
                                                       options:NSBinarySearchingInsertionIndex | NSBinarySearchingFirstEqual
                                               usingComparator:operationHourComparison];
        
        if(firstMatchingIndex == 0){
            firstMatchingIndex = ([self.deliveryHours count] - 1);
        } else {
            firstMatchingIndex -= 1;
        }
        
        if(firstMatchingIndex >= ([self.deliveryHours count] - 1)){
            self.nearestDeliveryAvailableTimeIndex = 0;
        } else {
            self.nearestDeliveryAvailableTimeIndex = firstMatchingIndex + 1;
        }
        
        self.nearestDeliveryOperationHour = [self.deliveryHours objectAtIndex:self.nearestDeliveryAvailableTimeIndex];
        OperationHour *prevHour = [self.deliveryHours objectAtIndex:firstMatchingIndex];
        
        return (prevHour.operationEvent == OperationEventOpen);
        
    } else {
        return NO;
    }
#endif
}

- (NSString *)nextDeliveryAvailableTime
{
    OperationHour *nextHour = [self.deliveryHours objectAtIndex:self.nearestDeliveryAvailableTimeIndex];
    
    if(nextHour == nil) return @"";
    
    NSNumber *startTimeNumber = [nextHour.openSpanDict objectForKey:@"start_time"];
    NSNumber *endTimeNumber = [nextHour.openSpanDict objectForKey:@"end_time"];
    NSString *startTime = [startTimeNumber stringValue];
    NSString *endTime = [endTimeNumber stringValue];
    NSString *startWeekString = [startTime substringToIndex:1];
    NSString *endWeekString = [endTime substringToIndex:1];
    NSString *startHourString = [startTime substringWithRange:NSMakeRange(1, 2)];
    NSString *endHourString = [endTime substringWithRange:NSMakeRange(1, 2)];
    NSString *startMinString = [startTime substringWithRange:NSMakeRange(3, 2)];
    NSString *endMinString = [endTime substringWithRange:NSMakeRange(3, 2)];
    
    if([startHourString integerValue] == 24){
        NSInteger weekVal = [startWeekString integerValue];
        weekVal++;
        if(weekVal > 7) weekVal = 1;
        startWeekString = [NSString stringWithInteger:weekVal];
    }
    
    if([endHourString integerValue] == 24){
        NSInteger weekVal = [endWeekString integerValue];
        weekVal++;
        if(weekVal > 7) weekVal = 1;
        endWeekString = [NSString stringWithInteger:weekVal];
    }
    
    
    NSString *startAmPmHour = [self amPmStringWithHour:[startHourString integerValue]
                                                minute:[startMinString integerValue]];
    
    NSString *endAmPmHour = [self amPmStringWithHour:[endHourString integerValue]
                                              minute:[endMinString integerValue]];
    
    return [NSString stringWithFormat:@"%@ %@ - %@ %@",
            [self weekStringAtIndex:[startWeekString integerValue]],
            startAmPmHour,
            [self weekStringAtIndex:[endWeekString integerValue]],
            endAmPmHour];
}

- (NSString *)nextDeliveryAvailableHumanReadableTime
{
    OperationHour *nextHour = [self.deliveryHours objectAtIndex:self.nearestDeliveryAvailableTimeIndex];
    
    if(nextHour == nil) return @"";
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    calendar.timeZone = self.timeZone;
    
    NSDateComponents *currentComponents = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekday
                                                      fromDate:[NSDate date]];
    NSInteger weekday = (currentComponents.weekday - 1);
    if(weekday == 0) weekday = 7;
    
    NSNumber *startTimeNumber = [nextHour.openSpanDict objectForKey:@"start_time"];
//    NSNumber *endTimeNumber = [nextHour.openSpanDict objectForKey:@"end_time"];
    NSString *startTime = [startTimeNumber stringValue];
//    NSString *endTime = [endTimeNumber stringValue];
    NSString *startWeekString = [startTime substringToIndex:1];
//    NSString *endWeekString = [endTime substringToIndex:1];
    NSString *startHourString = [startTime substringWithRange:NSMakeRange(1, 2)];
//    NSString *endHourString = [endTime substringWithRange:NSMakeRange(1, 2)];
    NSString *startMinString = [startTime substringWithRange:NSMakeRange(3, 2)];
//    NSString *endMinString = [endTime substringWithRange:NSMakeRange(3, 2)];
    
    if([startHourString integerValue] == 24){
        NSInteger weekVal = [startWeekString integerValue];
        weekVal++;
        if(weekVal > 7) weekVal = 1;
        startWeekString = [NSString stringWithInteger:weekVal];
    }
    
    NSString *startAmPmHour = [self amPmStringWithHour:[startHourString integerValue]
                                                minute:[startMinString integerValue]];
    
    
    
    return [NSString stringWithFormat:@"Next Available Time\n%@ at %@",
            [self weekStringAtIndexWithHumanReadable:[startWeekString integerValue]
                                   withCurrentWeekay:weekday
                                           timeValue:startTimeNumber.integerValue],
            startAmPmHour];;
}


- (NSString *)amPmStringWithHour:(NSInteger)hour minute:(NSInteger)minute
{
    NSString *amPmString = @"AM";
    
    if(hour == 0){
        hour = 12;
    } else if (hour >= 12) {
        if(hour >= 13){
            hour -= 12;
            if(hour == 12){
                amPmString = @"AM";
            } else {
                amPmString = @"PM";
            }
        } else {
            amPmString = @"PM";
        }
    }
    
    return [NSString stringWithFormat:@"%ld:%02ld %@", (long)hour, (long)minute, amPmString];
}

- (NSString *)weekStringAtIndex:(NSInteger)weekIndex
{
    switch(weekIndex){
        case 1: return NSLocalizedString(@"Mon", nil);
            break;
        case 2: return NSLocalizedString(@"Tue", nil);
            break;
        case 3: return NSLocalizedString(@"Wed", nil);
            break;
        case 4: return NSLocalizedString(@"Thu", nil);
            break;
        case 5: return NSLocalizedString(@"Fri", nil);
            break;
        case 6: return NSLocalizedString(@"Sat", nil);
            break;
        case 7: return NSLocalizedString(@"Sun", nil);
            break;
    }
    return @"Unknown";
}

- (NSString *)weekStringAtIndexWithHumanReadable:(NSInteger)weekIndex
                               withCurrentWeekay:(NSInteger)weekday
                                      timeValue:(NSInteger)timeValue
{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    calendar.timeZone = self.timeZone;
    
    NSDateComponents *currentComponents = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekday
                                                      fromDate:[NSDate date]];
    NSInteger nowWeekday = (currentComponents.weekday - 1);
    if(weekday == 0) weekday = 7;
    
    NSInteger nowTime = [[NSString stringWithFormat:@"%ld%02ld%02ld",
                              (long)nowWeekday,
                              (long)currentComponents.hour,
                              (long)currentComponents.minute] integerValue];
    if(nowTime < timeValue){
        if(weekIndex - weekday == 0) return NSLocalizedString(@"Today", nil);
        else if(weekIndex - weekday == 1) return NSLocalizedString(@"Tomorrow", nil);
        else if(weekday == 7 && weekIndex == 1) return  NSLocalizedString(@"Tomorrow", nil);
    } else {
        
    }
    
    switch(weekIndex){
        case 1: return NSLocalizedString(@"Mon", nil);
            break;
        case 2: return NSLocalizedString(@"Tue", nil);
            break;
        case 3: return NSLocalizedString(@"Wed", nil);
            break;
        case 4: return NSLocalizedString(@"Thu", nil);
            break;
        case 5: return NSLocalizedString(@"Fri", nil);
            break;
        case 6: return NSLocalizedString(@"Sat", nil);
            break;
        case 7: return NSLocalizedString(@"Sun", nil);
            break;
    }
    return @"Unknown";
}

- (NSMutableArray *)intersectOperationHours
{
    NSMutableArray *result = [NSMutableArray array];
    NSArray *tempOperationHours = self.operationHours;
    NSArray *tempDeliveryHours = self.deliveryHours;
    
    if(!self.autoOpenClose) tempOperationHours = nil;
    if(!self.autoDeliveryHours) tempDeliveryHours = nil;
    
    if(tempOperationHours > 0 && tempDeliveryHours > 0){
        NSInteger i = 0, j = 0;
        BOOL os = NO, ds = NO, is = NO;
        while(i < [tempOperationHours count] || j < [tempDeliveryHours count]){
            OperationHour *oh = nil, *dh = nil, *ph = nil;
            
            if(i < [tempOperationHours count]){
                oh = tempOperationHours[i];
            }
            if(j < [tempDeliveryHours count]){
                dh = tempDeliveryHours[j];
            }
            
            if(dh == nil || (oh != nil && oh.eventTime < dh.eventTime)){
                ph = oh;
                os = (oh.operationEvent == OperationEventOpen);
                i++;
            } else if(oh == nil || (dh != nil && dh.eventTime < oh.eventTime)){
                ph = dh;
                ds = (dh.operationEvent == OperationEventOpen);
                j++;
            } else {
                ph = oh; // No matter oh or dh
                os = (oh.operationEvent == OperationEventOpen);
                ds = (dh.operationEvent == OperationEventOpen);
                i++; j++;
            }
            
            OperationHour *neoOh = nil;
            if((os && ds) && !is){
                is = YES; //Generate Open event
                neoOh = [OperationHour alloc];
                neoOh.eventTime = ph.eventTime;
                neoOh.operationEvent = OperationEventOpen;
            } else if((!os || !ds) && is){
                is = NO; //Generate Close event
                neoOh = [OperationHour alloc];
                neoOh.eventTime = ph.eventTime;
                neoOh.operationEvent = OperationEventClose;
            }
            
            if(neoOh != nil){
                [result addObject:neoOh];
            }
        }
        return result;
    } else if([tempOperationHours count] > 0){
        return self.operationHours;
    } else if([tempDeliveryHours count] > 0){
        return self.deliveryHours;
    }
    
    return nil;
}

- (NSString *)nextAvailableIntersectTime
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *currentComponents = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekday
                                                      fromDate:[NSDate date]];
    NSInteger weekday = (currentComponents.weekday - 1);
    if(weekday == 0) weekday = 7;
    NSInteger aTime = [[NSString stringWithFormat:@"%ld%02ld%02ld",
                              (long)weekday,
                              (long)currentComponents.hour,
                              (long)currentComponents.minute] integerValue];
    OperationHour *nearestHour = nil;
    OperationHour *firstOpenHour = nil;
    for(OperationHour *anHour in self.intersectHours){
        if(firstOpenHour == nil && anHour.operationEvent == OperationEventOpen){
            firstOpenHour = anHour;
        }
        if(aTime <= anHour.eventTime && anHour.operationEvent == OperationEventOpen){
            nearestHour = anHour;
            break;
        }
    }
    
    if(nearestHour == nil) nearestHour = firstOpenHour;
    NSInteger week = (nearestHour.eventTime / 10000);
    NSInteger hour = (nearestHour.eventTime / 100) - ((nearestHour.eventTime / 10000) * 100);
    NSInteger mins = nearestHour.eventTime - ((nearestHour.eventTime / 100) * 100);
    NSString *startAmPmHour = [self amPmStringWithHour:hour
                                                minute:mins];
    
    return [NSString stringWithFormat:@"Next Available Time\n%@ at %@",
            [self weekStringAtIndexWithHumanReadable:week
                                   withCurrentWeekay:weekday
                                           timeValue:nearestHour.eventTime],
            startAmPmHour];;
    
}

- (void)addOperationEvent:(OperationHour *)hour inArray:(NSMutableArray *)inArray
{
    NSUInteger index = [inArray indexOfObject:hour
                                inSortedRange:NSMakeRange(0, [inArray count])
                                      options:NSBinarySearchingInsertionIndex | NSBinarySearchingFirstEqual
                              usingComparator:^NSComparisonResult(OperationHour *obj1, OperationHour *obj2) {
                                  if(obj1.eventTime > obj2.eventTime){
                                      return NSOrderedDescending;
                                  } else if(obj1.eventTime < obj2.eventTime){
                                      return NSOrderedAscending;
                                  } else {
                                      if(obj1.operationEvent == OperationEventOpen){
                                          return NSOrderedDescending;
                                      } else if (obj1.operationEvent == OperationEventClose){
                                          return NSOrderedAscending;
                                      } else {
                                          return NSOrderedSame;
                                      }

                                  }
                              }];
    [inArray insertObject:hour atIndex:index];
}

- (PCCurrency)deliveryFeeAt:(CLLocation *)location
{
    return [self deliveryFeeAt:location
                   andSubtotal:0];
}

- (PCCurrency)deliveryFeeAt:(CLLocation *)location andSubtotal:(PCCurrency)subtotal
{
    PCCurrency deliveryFee = NSNotFound;
    switch(self.deliveryFeePlan){
        case DeliveryFeePlanFlat:
            deliveryFee = [self flatDeliveryFeeAt:location];
            break;
        case DeliveryFeePlanZone:
            deliveryFee = [self polygonDeliveryFeeAt:location];
            break;
        case DeliveryFeePlanRadius:
            deliveryFee = [self radiusDeliveryFeeAt:location];
            break;
        default:
            
            break;
    }
    
    if([self.deliveryFeeDiscount count] && deliveryFee != NSNotFound){
        NSDictionary *properDict = nil;
        for(NSDictionary *discDict in self.deliveryFeeDiscount){
            if(subtotal < [discDict currencyForKey:@"at_least_amount"]){
                break;
            }
            properDict = discDict;
        }
        if(properDict != nil){
            deliveryFee -= [properDict currencyForKey:@"discount_amount"];
        }
    }
    
    deliveryFee = MAX(deliveryFee, 0);
    
    return deliveryFee;
}

- (PCCurrency)flatDeliveryFeeAt:(CLLocation *)location
{
    CLLocationDistance distance = [location distanceFromLocation:self.location];
    double distanceInMile = distance * METERS_TO_MILES;
    
    if(distanceInMile <= self.deliveryDistanceLimit){
        return self.deliveryFee;
    } else {
        return NSNotFound;
    }
}

- (PCCurrency)polygonDeliveryFeeAt:(CLLocation *)location
{
    for(DZonePolygon *zonePolygon in self.deliveryFeeZone){
        if([zonePolygon isContainLocation:location]){
            return zonePolygon.feeAmount;
        }
    }
    return NSNotFound;
}

- (PCCurrency)radiusDeliveryFeeAt:(CLLocation *)location
{
    CLLocationDistance distance = [location distanceFromLocation:self.location];
    double distanceInMile = distance * METERS_TO_MILES;
    
    for(DZoneRadius *zoneRadius in self.deliveryFeeZone){
        if(distanceInMile <= zoneRadius.radius){
            return zoneRadius.feeAmount;
        }
    }
    return NSNotFound;
}

- (void)refreshWithMerchant:(Merchant *)merchant
{
    self.allPropertySet = merchant.isAllPropertySet;
    self.name = merchant.name;
    self.address = merchant.address;
    self.geoAddress = merchant.geoAddress;
    self.displayGeoAddress = merchant.displayGeoAddress;
    self.desc = merchant.desc;
    self.location = merchant.location;
    self.favorite = merchant.isFavorite;
    
    self.phoneNumber = merchant.phoneNumber;
    self.roPhoneNumber = merchant.roPhoneNumber;
    self.numberOfTables = merchant.numberOfTables;
    self.taxRate = merchant.taxRate;
    self.deliveryFee = merchant.deliveryFee;
    self.deliveryMinOrderAmount = merchant.deliveryMinOrderAmount;
    self.dineinMinOrderAmount = merchant.dineinMinOrderAmount;
    self.takeoutMinOrderAmount = merchant.takeoutMinOrderAmount;
    self.deliveryDistanceLimit = merchant.deliveryDistanceLimit;
    self.allowTip = merchant.isAllowTip;
    self.lastMenuModifiedDate = merchant.lastMenuModifiedDate;
    self.creditPolicyName = merchant.creditPolicyName;
    self.promotions = merchant.promotions;
    self.imageURL = merchant.imageURL;
    self.photoURLs = merchant.photoURLs;
    self.logoURL = merchant.logoURL;
    
    self.demoMode = merchant.demoMode;
    self.open = merchant.isOpen;
    self.takeoutOpen = merchant.isTakeoutOpen;
    self.testMode = merchant.isTestMode;
    self.published = merchant.isPublished;
    
    self.menuOrder = merchant.isMenuOrder;
    self.ablePayment = merchant.isAblePayment;
    self.tableBase = merchant.isTableBase;
    
    self.ableTakeout = merchant.isAbleTakeout;
    self.ableDelivery = merchant.isAbleDelivery;
    self.servicedBy = merchant.servicedBy;
    
    self.roServiceRate = merchant.roServiceRate;
    self.roService = merchant.isRoService;
    self.roServiceMinAmount = merchant.roServiceMinAmount;
    
    self.takeoutRemark = merchant.takeoutRemark;
    self.hours = merchant.hours;
    self.nonInteractive = merchant.isNonInteractive;
    self.homepage = merchant.homepage;
    self.email = merchant.email;
    self.distance = merchant.distance;
//    self.distanceFromServer = merchant.distanceFromServer // Don't refresh distance
//    self.deliveryDistance = merchant.deliveryDistance; // Don't refresh delivery distance
    self.averageDeliveryTime = merchant.averageDeliveryTime;
    self.country = merchant.country;
    self.city = merchant.city;
    self.address1 = merchant.address1;
    self.address2 = merchant.address2;
    self.state = merchant.state;
    self.zip = merchant.zip;
    
    self.multilineAddress = merchant.multilineAddress;
    self.deliveryFeePlan = merchant.deliveryFeePlan;
    self.deliveryFeeZone = merchant.deliveryFeeZone;
    self.deliveryFeeDiscount = merchant.deliveryFeeDiscount;
    
    self.autoOpenClose = merchant.autoOpenClose;
    self.autoDeliveryHours = merchant.autoDeliveryHours;
    self.deliveryFeeAmountForBigOrder = merchant.deliveryFeeAmountForBigOrder;
    self.deliveryBigOrderAmount = merchant.deliveryBigOrderAmount;
    self.tableList = merchant.tableList;
    
    self.operationHours = merchant.operationHours;
    self.deliveryHours = merchant.deliveryHours;
    self.lastOperationDate = merchant.lastOperationDate;
    
    self.pickType = merchant.pickType;
    
    self.openStatus = merchant.openStatus;
    self.deliveryOpenStatus = merchant.deliveryOpenStatus;
    
    self.timeZone = merchant.timeZone;
    
    self.cellHeight = FLT_MAX;
}

#pragma mark - MKAnnotation
- (CLLocationCoordinate2D)coordinate
{
    return self.location.coordinate;
}

- (NSString *)title
{
    return self.displayName;
}

//- (void)setTempImage
//{
//    if(self.merchantNo % 2 == 1){
//        self.image = [UIImage imageNamed:@"sample_restaurant.jpg"];
//    } else {
//        self.image = [UIImage imageNamed:@"alitos.jpg"];
//    }
//}

#pragma mark - PCModelProtocol
- (id)initWithFMResult:(FMResultSet *)result
{
    self = [super init];
    if(self != nil){
        self.merchantNo = [result longLongIntForColumn:@"merchantNo"];
        self.name = [result stringForColumn:@"name"];
        self.desc = [result stringForColumn:@"desc"];
        
        NSString *storeURLString = [result stringForColumn:@"imageURL"];
        
        self.imageURL = [NSURL URLWithString:storeURLString];
        
        NSString *logoURLString = [result stringForColumn:@"logoURL"];

        self.logoURL = [NSURL URLWithString:logoURLString];
        
        self.menuOrder = [result boolForColumn:@"menuOrder"];
        self.ablePayment = [result boolForColumn:@"unableDirectPay"];
        self.tableBase = [result boolForColumn:@"tableBase"];
        
        self.address = [result stringForColumn:@"address"];
        self.geoAddress = [result stringForColumn:@"geoAddress"];
        self.displayGeoAddress = [result boolForColumn:@"displayGeoAddress"];
        self.numberOfTables = [result intForColumn:@"numberOfTables"];
        
        self.taxRate = [result doubleForColumn:@"taxRate"];
        self.deliveryFee = [result intForColumn:@"deliveryFee"];
        self.deliveryMinOrderAmount = [result intForColumn:@"deliveryMinOrderAmount"];
        self.dineinMinOrderAmount = [result intForColumn:@"dineinMinOrderAmount"];
        self.takeoutMinOrderAmount = [result intForColumn:@"takeoutMinOrderAmount"];
        self.deliveryDistanceLimit = [result doubleForColumn:@"deliveryDistanceLimit"];
        self.allowTip = [result boolForColumn:@"allowTip"];
        self.lastMenuModifiedDate = [result dateForColumn:@"lastMenuModifiedDate"];
        
        CLLocationDegrees latitude = [result doubleForColumn:@"latitude"];
        CLLocationDegrees longitude = [result doubleForColumn:@"longitude"];
        self.location = [[CLLocation alloc] initWithLatitude:latitude
                                                   longitude:longitude];
        self.testMode = [result boolForColumn:@"demo"];
        self.lastVisitedDate = [result dateForColumn:@"lastVisitedDate"];
        
        self.ableTakeout = [result boolForColumn:@"ableTakeout"];
        self.ableDelivery = [result boolForColumn:@"ableDelivery"];
        self.servicedBy = [result intForColumn:@"servicedBy"];
        self.roService = [result boolForColumn:@"roService"];
        self.roServiceRate = [result doubleForColumn:@"roServiceRate"];
        self.roServiceMinAmount = [result intForColumn:@"roServiceMinAmount"];
        self.cachedType = [result intForColumn:@"cachedType"];
        
        self.open = YES;
        
        self.cellHeight = FLT_MAX;
    }
    
    return self;
}

- (BOOL)insert
{
    if([self openDB]){
        
        return [DB.db executeUpdate:INSERT_QRY,
                [NSNumber numberWithLongLong:self.merchantNo],
                self.name,
                self.desc,
                self.address,
                self.geoAddress,
                [NSString sqlStringWithBool:self.displayGeoAddress],
                [NSNumber numberWithInteger:self.numberOfTables],
                [NSNumber numberWithDouble:self.location.coordinate.latitude],
                [NSNumber numberWithDouble:self.location.coordinate.longitude],
                [self.imageURL absoluteString],
                [self.logoURL absoluteString],
                [NSString sqlStringWithBool:self.isMenuOrder],
                [NSString sqlStringWithBool:self.isAblePayment],
                [NSString sqlStringWithBool:self.isTableBase],
                [NSDate date],
                [NSString sqlStringWithBool:self.isTestMode],
                [NSString sqlStringWithBool:self.isAbleTakeout],
                [NSString sqlStringWithBool:self.isAbleDelivery],
                [NSNumber numberWithInteger:self.servicedBy],
                [NSString sqlStringWithBool:self.isRoService],
                [NSNumber numberWithFloat:self.roServiceRate],
                [NSNumber numberWithCurrency:self.roServiceMinAmount],
                [NSNumber numberWithFloat:self.taxRate],
                [NSNumber numberWithCurrency:self.deliveryFee],
                [NSNumber numberWithCurrency:self.deliveryMinOrderAmount],
                [NSNumber numberWithCurrency:self.dineinMinOrderAmount],
                [NSNumber numberWithCurrency:self.takeoutMinOrderAmount],
                [NSNumber numberWithDouble:self.deliveryDistanceLimit],
                [NSString sqlStringWithBool:self.isAllowTip],
                self.lastMenuModifiedDate,
                [NSNumber numberWithInteger:self.cachedType]];
        
    } else {
        PCError(@"Merchant insert error");
        return NO;
    }
    
    return [self closeDB];
}

- (BOOL)update
{
    if([self openDB]){
        
            return [DB.db executeUpdate:UPDATE_QRY,
                    self.name,
                    self.desc,
                    self.address,
                    self.geoAddress,
                    [NSString sqlStringWithBool:self.displayGeoAddress],
                    [NSNumber numberWithInteger:self.numberOfTables],
                    [NSNumber numberWithDouble:self.location.coordinate.latitude],
                    [NSNumber numberWithDouble:self.location.coordinate.longitude],
                    [self.imageURL absoluteString],
                    [self.logoURL absoluteString],
                    [NSString sqlStringWithBool:self.isMenuOrder],
                    [NSString sqlStringWithBool:self.isAblePayment],
                    [NSString sqlStringWithBool:self.isTableBase],
                    self.lastVisitedDate,
                    [NSString sqlStringWithBool:self.isTestMode],
                    [NSString sqlStringWithBool:self.isAbleTakeout],
                    [NSString sqlStringWithBool:self.isAbleDelivery],
                    [NSNumber numberWithInteger:self.servicedBy],
                    [NSString sqlStringWithBool:self.isRoService],
                    [NSNumber numberWithFloat:self.roServiceRate],
                    [NSNumber numberWithCurrency:self.roServiceMinAmount],
                    [NSNumber numberWithFloat:self.taxRate],
                    [NSNumber numberWithCurrency:self.deliveryFee],
                    [NSNumber numberWithCurrency:self.deliveryMinOrderAmount],
                    [NSNumber numberWithCurrency:self.dineinMinOrderAmount],
                    [NSNumber numberWithCurrency:self.takeoutMinOrderAmount],
                    [NSNumber numberWithDouble:self.deliveryDistanceLimit],
                    [NSString sqlStringWithBool:self.isAllowTip],
                    self.lastMenuModifiedDate,
                    [NSNumber numberWithInteger:self.cachedType],
                    [NSNumber numberWithInteger:self.merchantNo]];

        
    } else {
        PCError(@"Merchant update error");
        return NO;
    }
    
    return [self closeDB];
}

+ (NSString *)orderByQuery
{
    return @"order by lastVisitedDate desc";
}

- (NSString *)description
{
    /*
     @property (nonatomic) PCSerial merchantNo;
     @property (copy, nonatomic) NSString *name;
     @property (copy, nonatomic) NSString *address;
     @property (copy, nonatomic) NSString *desc;
     @property (nonatomic) NSInteger numberOfTables;
     @property (copy, nonatomic) NSString *ownerName;
     @property (copy, nonatomic) NSString *phoneNumber;
     @property (strong, nonatomic) CLLocation *location;
     @property (strong, nonatomic) NSDate *lastVisitedDate;

     */
    return [NSString stringWithFormat:@"%@{ merchantNo:%lld \n\t name:%@ \n\t address:%@ \n\t numberOfTables:%ld \n\t location:%@ \n\t isOpen:%d \n\t isPublished:%d \n\t isAbleTakeout:%d \n\t isAbleDelivery:%d \n\t servicedBy:%d \n\t cachedType:%d \n\t distance:%f \n\t distanceFromServer:%f \n\t strategy:%d}",
            [super description],
            self.merchantNo,
            self.name,
            self.address,
            (long)self.numberOfTables,
            self.location,
            self.isOpenHour,
            self.isPublished,
            self.isAbleTakeout,
            self.isAbleDelivery,
            self.servicedBy,
            self.cachedType,
            self.distance,
            self.distanceFromServer,
            self.deliveryFeePlan
            ];
}

- (NSString *)displayName
{
    return self.name;
}

- (NSString *)servicedByString
{
    switch(self.servicedBy){
        case ServicedByNone:
            return @"none";
        case ServicedBySelf:
            return @"self";
        case ServicedByStaff:
            return @"server";
        default:
            return @"";
    }
}

- (BOOL)isServicedByStaff
{
    return (self.servicedBy == ServicedByStaff);
}

- (BOOL)isAbleDinein
{
    return (self.servicedBy != ServicedByNone);
}

- (NSString *)restaurantTypeString
{
    if(self.isTableBase){
        return @"Waiter-Service";
    } else {
        return @"Counter-Service";
    }
    
//    switch(self.servicedBy){
//        case ServicedByNone:
//            return @"";
//        case ServicedBySelf:
//            return @"Self-Service";
//        case ServicedByStaff:
//            if(self.isTableBase){
//                return @"Waiter-Service";
//            } else {
//                return @"Counter-Service";
//            }
//        default:
//            return @"";
//    }
}


- (BOOL)isAbleTakeoutOrDelivery
{
    return (self.isAbleDelivery || self.isAbleTakeout);
}

- (BOOL)isAbleTakeoutAndDelivery
{
    return (self.isAbleDelivery && self.isAbleTakeout);
}

- (BOOL)isOnlyDelivery
{
    return (self.isAbleDelivery && !self.isAbleTakeout && !self.isAbleDinein);
}

- (NSString *)displayAddress
{
    if(self.displayGeoAddress){
        if([self.geoAddress length] > 0){
            return self.geoAddress;
        } else {
            return self.address;
        }
    } else {
        return self.address;
    }
}

- (NSString *)displayMultilineAddress
{
    if(self.displayGeoAddress){
        if([self.geoAddress length] > 0){
            return self.geoAddress;
        } else {
            return self.multilineAddress;
        }
    } else {
        return self.multilineAddress;
    }
}

- (NSString *)multilineAddress
{
    if(_multilineAddress == nil){
        return self.address;
    } else {
        return _multilineAddress;
    }
}

- (Promotion *)promotion
{
    if([self.promotions count] > 0){
        return [self.promotions objectAtIndex:0];
    }
    return nil;
}

- (Promotion *)promotionOnCartType:(CartType)cartType
{
    NSArray *promotions = [self promotionsOnCartType:cartType];
    if([promotions count] > 0){
        return [promotions objectAtIndex:0];
    }
    return nil;
}

- (NSArray *)promotionsOnCartType:(CartType)cartType
{
    PromotionOrderType promotionOrdertype = PromotionOrderTypeNone;
    switch(cartType){
        case CartTypeCart:
        case CartTypePickup:
        case CartTypePickupServiced:
            promotionOrdertype = PromotionOrderTypeDineIn;
            break;
        case CartTypeTakeout:
            promotionOrdertype = PromotionOrderTypeTakeout;
            break;
        case CartTypeDelivery:
            promotionOrdertype = PromotionOrderTypeDelivery;
            break;
        case CartTypeUnknown:
        default:
            // Do nothing
            break;
    }
    return [self promotionsOnPromotionOrderType:promotionOrdertype];
}

- (Promotion *)promotionOnOrderType:(OrderType)orderType
{
    NSArray *promotions = [self promotionsOnOrderType:orderType];
    if([promotions count] > 0){
        return [promotions objectAtIndex:0];
    }
    return nil;
}


- (NSArray *)promotionsOnOrderType:(OrderType)orderType;
{
    PromotionOrderType promotionOrdertype = PromotionOrderTypeNone;
    switch(orderType){
        case OrderTypeBill:
        case OrderTypeCart:
        case OrderTypePickup:
            promotionOrdertype = PromotionOrderTypeDineIn;
            break;
        case OrderTypeTakeout:
            promotionOrdertype = PromotionOrderTypeTakeout;
            break;
        case OrderTypeDelivery:
            promotionOrdertype = PromotionOrderTypeDelivery;
            break;
        case OrderTypeUnknown:
        case OrderTypeOrderOnly:
        default:
            // Do nothing
            break;
    }
    return [self promotionsOnPromotionOrderType:promotionOrdertype];
}


- (NSArray *)promotionsOnPromotionOrderType:(PromotionOrderType)promotionOrderType;
{
    NSMutableArray *promotions = [NSMutableArray array];
    
    if([self.promotions count] > 0){
        for(Promotion *promotion in self.promotions){
            if((promotion.promotionOrderType & promotionOrderType) == promotionOrderType){
                [promotions addObject:promotion];
            }
        }
    }
    return promotions;
}

- (BOOL)hasRoServiceFee
{
//    return
    return (self.roServiceRate > 0);
}

- (NSString *)distanceString
{
    return [NSString stringWithDistance:self.distance];
}

- (NSString *)distanceFromServerString
{
    return [NSString stringWithDistance:self.distanceFromServer];
}

- (NSString *)deliveryDistanceString
{
    return [NSString stringWithDistance:self.deliveryDistance];
}
@end
