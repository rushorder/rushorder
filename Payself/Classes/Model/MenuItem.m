//
//  TableItem.m
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MenuItem.h"
#import "MenuOptionGroup.h"
#import "MenuOption.h"


@implementation MenuItem

+ (BOOL)isRetina
{
    return ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale >= 2.0))?1:0;
}

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [self init];
    
    if(self != nil){
        
        self.menuItemNo = [dict serialForKey:@"id"];
        self.name = [dict objectForKey:@"name"];
        self.desc = [dict objectForKey:@"description"];

        NSDictionary *imageDict = [dict objectForKey:@"image"];

        NSString *imageURLString = [imageDict objectForKey:@"url"];
//        NSString *imageURLStringWithoutExt = [imageURLString stringByDeletingPathExtension];
        NSString *imageURLThumb = nil;

        if([MenuItem isRetina]){
            imageURLThumb = [[imageDict objectForKey:@"retina"] objectForKey:@"url"];
        } else {
            imageURLThumb = [[imageDict objectForKey:@"thumb"] objectForKey:@"url"];
        }

        if(imageURLString.length > 0){
            self.imageURL = [NSURL URLWithString:imageURLString];
        }
        
        if(imageURLThumb.length > 0){
            self.thumbnailURL = [NSURL URLWithString:imageURLThumb];
        }
//        self.imageURL = [NSURL URLWithString:@"http://www.styleweekly.com/imager/rockfish-with-mushrooms-and-leeks-is-memorable-at-the-museum-districts-new/b/original/1787119/793a/food48_belmont.jpg"];
//        self.thumbnailURL = [NSURL URLWithString:@"http://www.styleweekly.com/imager/rockfish-with-mushrooms-and-leeks-is-memorable-at-the-museum-districts-new/b/original/1787119/793a/food48_belmont.jpg"];
        
        NSArray *menuPrices = [dict objectForKey:@"menu_choices"];
        
        NSMutableArray *priceArray = [NSMutableArray array];
        BOOL isThereDefaultPrice = NO;
        for(NSDictionary *dict in menuPrices){
            MenuPrice *price = [[MenuPrice alloc] initWithDictionary:dict];
            if(price.selected) isThereDefaultPrice = YES;
            [priceArray addObject:price];
        }
        
        if(!isThereDefaultPrice){
            if([priceArray count] > 0){
                MenuPrice *price = [priceArray objectAtIndex:0];
                price.selected = YES;
            }
        }

        self.prices = priceArray;
        self.taxable = [dict boolForKey:@"taxable"];

        self.forDineIn = [dict boolForKey:@"dine_in"];
        self.forTakeout = [dict boolForKey:@"take_out"];
        self.forDelivery = [dict boolForKey:@"delivery"];
        self.alcohol = [dict boolForKey:@"alcohol"];

        NSArray *menuOptionGroups = [dict objectForKey:@"menu_option_groups"];
        NSMutableArray *optionGroupArray = [NSMutableArray array];
        for(NSDictionary *dict in menuOptionGroups){
            MenuOptionGroup *group = [[MenuOptionGroup alloc] initWithDictionary:dict];
            [optionGroupArray addObject:group];
        }
        self.optionGroups = optionGroupArray;
        
        if([dict objectForKey:@"merchant"] != nil){
            self.merchant = [[Merchant alloc] initWithDictionary:[dict objectForKey:@"merchant"]];
        }
    }
    return self;
}

- (void)resetAllOptions
{
    for(MenuPrice *price in self.prices){
        price.selected = price.isDefaultPrice;
    }
    
    for(MenuOptionGroup *group in self.optionGroups){
        for(MenuOption *option in group.options){
            option.selected = NO;
        }
    }
}

- (PCCurrency)price
{
    return [self basePrice] + [self optionPrices];
}

- (PCCurrency)basePrice
{
    return [self selectedMenuPrice].price;
}

- (MenuPrice *)defaultMenuPrice
{
    for(MenuPrice *price in self.prices){
        if(price.isDefaultPrice){
            return price;
        }
    }
    
    if([self.prices count] > 0){
        return ((MenuPrice *)[self.prices objectAtIndex:0]);
    }
    
    return nil;
}

- (MenuPrice *)selectedMenuPrice
{
    for(MenuPrice *price in self.prices){
        if(price.isSelected){
            return price;
        }
    }
    
    if([self.prices count] > 0){
        return ((MenuPrice *)[self.prices objectAtIndex:0]);
    }
    
    return nil;
}

- (NSMutableArray *)selectedOptions
{
    NSMutableArray *array = [NSMutableArray array];
    
    for(MenuOptionGroup *aMenuGroup in self.optionGroups){
        for(MenuOption *option in aMenuGroup.options){
            if(option.isSelected){
                [array addObject:option];
            }
        }
    }
    
    return array;
}

- (PCCurrency)optionPrices
{
    PCCurrency additionalPrice = 0;
    for(MenuOptionGroup *aMenuGroup in self.optionGroups){
        for(MenuOption *option in aMenuGroup.options){
            if(option.isSelected){
                additionalPrice += option.price;
            }
        }
    }
    return additionalPrice;
}

- (void)linkMenuPan:(MenuPan *)menuPan
{
    self.menuPan = menuPan;
}
/*
 
 @property (nonatomic) PCSerial menuItemNo;
 @property (copy, nonatomic) NSString *name;
 @property (copy, nonatomic) NSString *desc;
 @property (nonatomic, readonly) PCCurrency price;
 @property (copy, nonatomic) NSURL *imageURL;
 @property (copy, nonatomic) NSURL *thumbnailURL;
 @property (strong, nonatomic) NSArray *prices;
 @property (strong, nonatomic) NSArray *optionGroups;
 */

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@{ \n\t no:%lld \n\t name:%@ \n\t price:%ld \n\t imageURL:%@ \n\t thumbnailURL:%@ \n\t taxRate:%f \n\t}",
            [super description],
            self.menuItemNo,
            self.name,
            (long)self.price,
            self.imageURL,
            self.thumbnailURL,
            self.taxRate];
}

@end
