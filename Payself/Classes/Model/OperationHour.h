//
//  OperationHour.h
//  RushOrder
//
//  Created by Conan on 7/5/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "PCModel.h"

typedef enum operationEvent_{
    OperationEventNone = 0,
    OperationEventOpen,
    OperationEventClose,
} OperationEvent;

@interface OperationHour : PCModel

@property (nonatomic) NSInteger eventTime;
@property (nonatomic) OperationEvent operationEvent;
@property (strong, nonatomic) NSDictionary *openSpanDict;

- (NSComparisonResult)compare:(OperationHour *)anotherHour;
@end
