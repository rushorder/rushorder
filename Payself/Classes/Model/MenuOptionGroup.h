//
//  MenuOptionGroup.h
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCModel.h"

typedef enum menuOptionType_{
    MenuOptionTypeChoose,
    MenuOptionTypeAdd,
} MenuOptionType;

@interface MenuOptionGroup : PCModel

@property (nonatomic) PCSerial optionGroupNo;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *desc;
@property (nonatomic) MenuOptionType optionType;
@property (nonatomic) NSInteger maxSelect;

@property (strong, nonatomic) NSMutableArray *options;

@property (readonly) NSString *optionTypeString;

@property (nonatomic, getter = isPriceSensitive) BOOL priceSensitive;

- (id)initWithDictionary:(NSDictionary *)dict;

@end