//
//  CustomRequest.h
//  RushOrder
//
//  Created by Conan Kim on 6/18/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import "PCModel.h"

@interface CustomRequest : PCModel<PCModelProtocol>

@property (nonatomic) PCSerial requestNo;
@property (nonatomic) CustomRequestType type;
@property (copy, nonatomic) NSString *request;
@property (nonatomic) PCSerial merchantNo;
@property (nonatomic) PCSerial menuNo;
@property (nonatomic) NSUInteger count;
@property (copy, nonatomic) NSDate *lastUsedDate;

+ (CustomRequest *)updateCustomRequest:(NSString *)request
                       type:(CustomRequestType)type;

+ (CustomRequest *)updateCustomRequest:(NSString *)request
                       type:(CustomRequestType)type
                 merchantNo:(PCSerial)merchantNo;

+ (CustomRequest *)updateCustomRequest:(NSString *)request
                       type:(CustomRequestType)type
                 merchantNo:(PCSerial)merchantNo
                     menuNo:(PCSerial)menuNo;

+ (NSMutableArray *)suggestingListWithKeyword:(NSString *)keyword
                                         type:(CustomRequestType)type
                                   merchantNo:(PCSerial)merchantNo;

@end
