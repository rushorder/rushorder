//
//  LineItem.h
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCModel.h"
#import "MenuItem.h"
#import "MenuPrice.h"
#import "JSONKit.h"

typedef enum lineItemStatus_{
    LineItemStatusOrdering = 0,
    LineItemStatusSubmitted,
    LineItemStatusFixed
} LineItemStatus;

typedef enum menuValidStatus_{
    MenuValidStatusValid = 0,
    MenuValidStatusUnknown = (1 << 0),
    MenuValidStatusOutOfHours = (1 << 1),
    MenuValidStatusPriceChanged = (1 << 2),
    MenuValidStatusOptionChanged = (1 << 3),
    MenuValidStatusMenuRemoved = (1 << 4),
    MenuValidStatusNotAvailableOrderType = (1 << 5),
} MenuValidStatus;

@interface LineItem : PCModel<PCModelProtocol>

//// Persistence ///////////////////////////////////////////////////////////////
@property (nonatomic) PCSerial lineItemNo;
@property (nonatomic) PCSerial paymentNo;
@property (nonatomic) PCSerial receiptNo;
@property (nonatomic) PCSerial orderNo;
@property (nonatomic) PCSerial cartNo;
@property (nonatomic) PCSerial merchantNo;
@property (nonatomic) PCSerial menuItemNo;
@property (nonatomic) PCSerial customerNo;
@property (nonatomic) PCCurrency unitPrice;
@property (nonatomic) PCCurrency basePrice;
@property (nonatomic) NSInteger quantity;
@property (nonatomic) PCCurrency lineAmount;
//@property (nonatomic) PCCurrency lineTaxAmount;
@property (copy, nonatomic) NSString *menuName;
@property (copy, nonatomic) NSString *menuChoiceName;
@property (copy, nonatomic) NSString *menuOptionNames; //JSON Format by array
@property (strong, nonatomic) NSArray *menuOptionNamesObj;
@property (nonatomic) PCSerial menuChoiceId;
@property (copy, nonatomic) NSString *optionIds;
@property (copy, nonatomic) NSString *specialInstruction;
@property (copy, nonatomic) NSString *udid;
////////////////////////////////////////////////////////////////////////////////

@property (strong, nonatomic) MenuItem *menuItem;
@property (strong, nonatomic) MenuItem *substitutableMenuItem;
@property (copy, nonatomic) NSString *deviceToken;
@property (strong, nonatomic) MenuPrice *selectedPrice;
@property (strong, nonatomic) NSMutableArray *selectedOptions;
@property (strong, nonatomic) NSMutableArray *selectedOptionGroups;
@property (nonatomic) LineItemStatus status;
@property (nonatomic, getter = isLocal) BOOL local;

@property (readonly) PCCurrency liveAmount;
//@property (readonly) PCCurrency totalAmount;
@property (readonly) NSString *liveAmountString;
@property (readonly) NSString *lineAmountString;
//@property (readonly) NSString *totalAmountString;
@property (readonly) NSString *displayMenuName;
@property (readonly) NSString *quantityString;
@property (readonly) NSString *serializedOptionNames;
@property (nonatomic) BOOL taxable;

@property (readonly) BOOL isMine;
@property (readonly) BOOL isPaid;

// For Near Me feature - temp
@property (copy, nonatomic) NSURL *imageURL;
@property (copy, nonatomic) NSURL *thumbnailURL;


@property (nonatomic, getter = isSplitExcept) BOOL splitExcept;

@property (nonatomic) MenuValidStatus menuValidStatus;

- (id)initWithDictionary:(NSDictionary *)dict;
- (id)initWithDictionary:(NSDictionary *)dict menuPan:(NSArray *)menuPan;
- (void)linkMenuItemInMenuPan:(NSArray *)menuPan;
- (void)linkMenuItemInMenuList:(NSArray *)menuList;

- (NSDictionary *)dictRepWithDeviceToken:(NSString *)deviceToken;
- (NSDictionary *)updateDictRepWithDeviceToken:(NSString *)deviceToken;

- (void)updatePriceAndOptions;
- (void)applyLiveAmount;

- (void)configureChoiceAndOptions;

- (NSString *)displayMenuNameWithPrice:(BOOL)withPrice;
@end
