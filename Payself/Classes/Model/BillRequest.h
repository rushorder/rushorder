//
//  BillRequest.h
//  RushOrder
//
//  Created by Conan on 3/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCModel.h"

@interface BillRequest : PCModel

@property (nonatomic) PCSerial billRequestNo;
@property (nonatomic) PCSerial merchantNo;
@property (nonatomic) PCSerial tableNo;
@property (nonatomic) BOOL active;
@property (strong, nonatomic) NSDate *requestTime;

@property (copy, nonatomic) NSString *tableNumber;

@property (strong, nonatomic) NSDate *lastAccessedTime;

- (id)initWithDictionary:(NSDictionary *)dict;
@end
