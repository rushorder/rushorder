//
//  MenuSubCategory.m
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MenuSubCategory.h"

@implementation MenuSubCategory

- (id)init
{
    self = [super init];
    
    if(self != nil){
        self.subCategoryNo = NSNotFound;
        self.items = [NSMutableArray array];
        self.cellHeight = NSNotFound;
    }
    
    return self;
    
}

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [self init];
    
    if(self != nil){
        self.subCategoryNo = [dict serialForKey:@"id"];
        self.name = [dict objectForKey:@"name"];
        self.desc = [dict objectForKey:@"description"];
    }
    
    return self;
}


- (NSString *)description
{
    return [NSString stringWithFormat:@"%@{ \n\t no:%lld \n\t name:%@ \n\t items:%@ \n\t}",
            [super description],
            self.subCategoryNo,
            self.name,
            self.items];
}

@end
