//
//  TableInfo.h
//  RushOrder
//
//  Created by Conan on 3/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCModel.h"
#import "Order.h"
#import "BillRequest.h"
#import "Cart.h"

typedef enum tableStatus_{
    TableStatusUnknown                  = 0,
    TableStatusEmpty                    = 1 << 0,
    TableStatusBillReqeusted            = 1 << 1,
    TableStatusOrdering                 = 1 << 2,
    TableStatusBillIssued               = 1 << 3,
    TableStatusOrderSubmitted           = 1 << 4,
    TableStatusPartialPaid              = 1 << 5,
    TableStatusCompleted                = 1 << 6,
    
    TableStatusFixed                    = 1 << 10,
    
    TableStatusNoBill                   = (TableStatusEmpty | TableStatusBillReqeusted | TableStatusUnknown),
    TableStatusNotEmpty                 = 0xFFFFFFFE,
    TableStatusAll                      = 0xFFFFFFFF
} TableStatus;

@interface TableInfo : PCModel

@property (nonatomic) PCSerial merchantNo;
//@property (nonatomic) PCSerial tableNo;
@property (copy, nonatomic) NSString *tableNumber;
@property (nonatomic) TableStatus status;
@property (nonatomic) TableStatus bfStatus;

@property (strong, nonatomic) Order *order;
@property (strong, nonatomic) BillRequest *billRequest;
@property (strong, nonatomic) Cart *cart;

@property (nonatomic, getter = isWrappingMode) BOOL wrappingMode; // Means this TableInfo is just wrapper

#pragma mark - readonly
@property (readonly) NSString *statusString;
@property (readonly) UIColor *statusColor;
@property (readonly) NSDate *actionDate;

- (id)initWithDictionary:(NSDictionary *)dict;
- (id)initWithModelDictionary:(NSDictionary *)dict;

- (id)initWithTableLabel:(NSString *)tableLabel merchantNo:(PCSerial)merchantNo;

- (NSComparisonResult)compareTimeWith:(TableInfo *)anotherTableInfo;

- (void)configureTableStatus;
@end