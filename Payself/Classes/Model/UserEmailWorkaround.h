//
//  UserEmailWorkaround.h
//  RushOrder
//
//  Created by Conan on 11/29/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserEmailWorkaround : NSObject
@property(nonatomic, readonly) NSString *userEmail;
@end
