//
//  DZoneRadius.h
//  RushOrder
//
//  Created by Conan on 7/1/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "PCModel.h"

@interface DZoneRadius : PCModel

@property (copy, nonatomic) NSString *label;
@property (nonatomic) PCCurrency feeAmount;
@property (nonatomic) CLLocationDistance radius;

@property (readonly) NSString *feeAmountString;
- (id)initWithDictionary:(NSDictionary *)dict;
@end