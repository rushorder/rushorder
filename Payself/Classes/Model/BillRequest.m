//
//  BillRequest.m
//  RushOrder
//
//  Created by Conan on 3/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "BillRequest.h"

@implementation BillRequest


- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self != nil){
        self.billRequestNo = [dict serialForKey:@"id"];
        self.merchantNo = [dict serialForKey:@"merchant_id"];
        self.active = [dict boolForKey:@"active"];
        self.requestTime = [dict dateForKey:@"created_at"];
        self.tableNumber = [dict objectForKey:@"table_label"];
        self.tableNo = [dict serialForKey:@"table_id"];
        self.lastAccessedTime = [[dict objectForKey:@"updated_at"] date];
    }
    return self;
}


@end