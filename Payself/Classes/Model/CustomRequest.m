//
//  CustomRequest.m
//  RushOrder
//
//  Created by Conan Kim on 6/18/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import "CustomRequest.h"

#define INSERT_QRY @"insert into 'CustomRequest'(\
type\
,request\
,merchantNo\
,menuNo\
,count\
,lastUsedDate) \
values \
(?,?,?,?,?,?)"

#define UPDATE_QRY @"update 'CustomRequest' set \
type = ?\
,request = ?\
,merchantNo = ?\
,menuNo = ?\
,count = ?\
,lastUsedDate = ?\
where requestNo = ?"

@implementation CustomRequest

+ (CustomRequest *)updateCustomRequest:(NSString *)request
                                  type:(CustomRequestType)type
{
    return [self updateCustomRequest:request
                                type:type
                          merchantNo:0];
}

+ (CustomRequest *)updateCustomRequest:(NSString *)request
                                  type:(CustomRequestType)type
                            merchantNo:(PCSerial)merchantNo
{
    return [self updateCustomRequest:request
                                type:type
                          merchantNo:merchantNo
                              menuNo:0];
}

+ (CustomRequest *)updateCustomRequest:(NSString *)request
                       type:(CustomRequestType)type
                 merchantNo:(PCSerial)merchantNo
                     menuNo:(PCSerial)menuNo
{
    
    CustomRequest *customRequest = [self instanceWithCondition:[NSString stringWithFormat:@"where request='%@'",
                                                                [request stringByReplacingOccurrencesOfString:@"'" withString:@"''"]]];
    if(customRequest == nil){
        customRequest = [[CustomRequest alloc] init];
        customRequest.request = request;
        customRequest.type = type;
        customRequest.merchantNo = merchantNo;
        customRequest.menuNo = menuNo;
        customRequest.count = 1;
        customRequest.lastUsedDate = [NSDate date];
    } else {
        customRequest.type = type;
        customRequest.merchantNo = merchantNo;
        customRequest.menuNo = menuNo;
        customRequest.count++;
        customRequest.lastUsedDate = [NSDate date];
    }
    
    if([customRequest save]){
        return customRequest;
    } else {
        PCError(@"Could not save Custom Request to Database");
        return nil;
    }
}

+ (NSMutableArray *)suggestingListWithKeyword:(NSString *)keyword
                                         type:(CustomRequestType)type
                                   merchantNo:(PCSerial)merchantNo
{
    NSMutableString *condition = [NSMutableString string];
    
    [condition appendFormat:@"where request like '%@%%' and type=%d and merchantNo=%lld COLLATE NOCASE",
     [keyword stringByReplacingOccurrencesOfString:@"'" withString:@"''"],
     type, merchantNo];
    [condition appendString:@" order by lastUsedDate desc, count desc, request asc"];
    
    NSMutableArray *list = [super listWithCondition:condition];
    
    if([list count] > 0){
        return list;
    }
    
    condition = [NSMutableString string];
    [condition appendFormat:@"where request like '%@%%' and type=%d COLLATE NOCASE",
     [keyword stringByReplacingOccurrencesOfString:@"'" withString:@"''"],
     type];
    [condition appendString:@" order by lastUsedDate desc, count desc, request asc"];
    
    list = [super listWithCondition:condition];
    
    if([list count] > 0){
        return list;
    }
    
    condition = [NSMutableString string];
    [condition appendFormat:@"where request like '%@%%' COLLATE NOCASE",
     [keyword stringByReplacingOccurrencesOfString:@"'" withString:@"''"]];
    [condition appendString:@" order by lastUsedDate desc, count desc, request asc"];
    
    list = [super listWithCondition:condition];
    
    if([list count] > 0){
        return list;
    }
    
    return nil;
}

- (id)initWithFMResult:(FMResultSet *)result
{
    self = [super init];
    if(self != nil){
        self.requestNo = [result longLongIntForColumn:@"requestNo"];
        self.type = [result intForColumn:@"type"];
        self.request = [result stringForColumn:@"request"];
        self.merchantNo = [result longLongIntForColumn:@"merchantNo"];
        self.menuNo = [result longLongIntForColumn:@"menuNo"];
        self.count = [result intForColumn:@"count"];
        self.lastUsedDate = [result dateForColumn:@"lastUsedDate"];
    }
    return self;
}

+ (NSString *)orderByQuery
{
 return @"";
}
     
- (BOOL)insert
{
    if([self openDB]){
        
        return [DB.db executeUpdate:INSERT_QRY,
                [NSNumber numberWithCurrency:self.type],
                self.request,
                [NSNumber numberWithLongLong:self.merchantNo],
                [NSNumber numberWithLongLong:self.menuNo],
                [NSNumber numberWithUnsignedInteger:self.count],
                self.lastUsedDate];
    } else {
        PCError(@"CustomRequest Insertion Error");
        return NO;
    }
}

- (BOOL)update
{
    if([self openDB]){
        return [DB.db executeUpdate:UPDATE_QRY,
                [NSNumber numberWithCurrency:self.type],
                self.request,
                [NSNumber numberWithLongLong:self.merchantNo],
                [NSNumber numberWithLongLong:self.menuNo],
                [NSNumber numberWithUnsignedInteger:self.count],
                self.lastUsedDate,
                [NSNumber numberWithLongLong:self.requestNo]];
    } else {
        PCError(@"Order update error");
        return NO;
    }
}
@end
