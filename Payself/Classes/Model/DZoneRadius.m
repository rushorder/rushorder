//
//  DZoneRadius.m
//  RushOrder
//
//  Created by Conan on 7/1/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

/*
 
 "delivery_zones" =         (
 {
 "fee_amount" = 100;
 label = 1;
 "merchant_id" = 1;
 points =                 (
 {
 latitude = "34.035966";
 longitude = "-118.444963";
 },
 {
 latitude = "34.036504";
 longitude = "-118.444094";
 },
 {
 latitude = "34.035033";
 longitude = "-118.442790";
 },
 {
 latitude = "34.034495";
 longitude = "-118.443697";
 },
 {
 latitude = "34.035966";
 longitude = "-118.444963";
 }
 );
 "zone_type" = polygon;
 },
 {
 "fee_amount" = 200;
 label = 2;
 "merchant_id" = 1;
 points =                 (
 {
 latitude = "34.036762";
 longitude = "-118.447119";
 },
 {
 latitude = "34.038407";
 longitude = "-118.444362";
 },
 {
 latitude = "34.034237";
 longitude = "-118.440682";
 },
 {
 latitude = "34.033232";
 longitude = "-118.444153";
 },
 {
 latitude = "34.036762";
 longitude = "-118.447119";
 }
 );
 "zone_type" = polygon;
 }
 
 */

#import "DZoneRadius.h"


@implementation DZoneRadius

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self != nil){
        [self updateWithDictionary:dict];
    }
    
    return self;
}

- (void)updateWithDictionary:(NSDictionary *)dict
{
    self.label = [dict objectForKey:@"label"];
    self.feeAmount = [dict currencyForKey:@"fee_amount"];
    self.radius = [dict distanceForKey:@"radius"];
}

- (NSString *)feeAmountString
{
    return [[NSNumber numberWithCurrency:self.feeAmount] currencyString];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ / %f",[super description], self.radius];
}

@end
