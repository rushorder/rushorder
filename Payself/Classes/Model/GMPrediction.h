//
//  GMPrediction.h
//  RushOrder
//
//  Created by Conan on 11/24/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GMPrediction : NSObject
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *desc;
@property (copy, nonatomic) NSString *placeId;
@property (copy, nonatomic) NSString *reference;
@property (nonatomic) CLLocationCoordinate2D coordinate;

@property (copy, nonatomic) NSString *street;
@property (copy, nonatomic) NSString *zip;
@property (copy, nonatomic) NSString *city;
@property (copy, nonatomic) NSString *state;
@property (copy, nonatomic) NSString *country;


- (id)initWithDictionary:(NSDictionary *)dict;
- (void)updateWithDetailDictionary:(NSDictionary *)dict;
@end
