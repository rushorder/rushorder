//
//  MenuCategory.m
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MenuCategory.h"

@implementation MenuCategory

- (id)init
{
    self = [super init];
    
    if(self != nil){
        self.categoryNo = NSNotFound;
        self.subCategories = [NSMutableArray array];
        self.items = [NSMutableArray array];
    }
    
    return self;

}

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [self init];
    
    if(self != nil){
        self.categoryNo = [dict serialForKey:@"id"];
        self.name = [dict objectForKey:@"name"];
        self.desc = [dict objectForKey:@"description"];
    }
    
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@{ \n\t no:%lld \n\t name:%@ \n\t subcategories:%@ \n\t}",
            [super description],
            self.categoryNo,
            self.name,
            self.subCategories];
}


@end