//
//  Promotion.m
//  RushOrder
//
//  Created by Conan on 4/12/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "Promotion.h"

@implementation Promotion

+ (id)smallCutPromotion
{
    Promotion *aPromotion = [[self alloc] init];
    aPromotion.promotionOrigin = PromotionOriginRO;
    return aPromotion;
}

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    if(self != nil){
        [self updateWithDictionary:dict];
    }
    return self;
}

- (void)updateWithDictionary:(NSDictionary *)dict
{
    self.promotionNo = [dict serialForKey:@"id"];
    self.merchantNo = [dict serialForKey:@"merchant_id"];
    self.flatDiscount = [dict boolForKey:@"flat_discount"];
    self.requiredCode = [dict boolForKey:@"require_code"];
    self.combinable = [dict boolForKey:@"combinable"];
    self.couponCode = [dict objectForKey:@"code"];
    self.discountRate = [dict floatForKey:@"discount_rate"];
    self.discountAmount = [dict currencyForKey:@"discount_amount"];
    self.startDate = [dict dateForKey:@"startdate"];
    self.endDate = [dict dateForKey:@"enddate"];
    self.title = [dict objectForKey:@"title"];
    self.desc = [dict objectForKey:@"description"];
    self.oneTimeUse = [dict boolForKey:@"one_time_use"];
    self.firstTimeUse = [dict boolForKey:@"first_time_use"];
    self.minOrderAmount = [dict currencyForKey:@"min_order_amount"];
    self.limitDiscountAmount = [dict currencyForKey:@"limit_discount_amount"];
    self.active = [dict boolForKey:@"active"];
    
    PromotionOrderType tempPromotionOrderType = PromotionOrderTypeNone;
    if([dict boolForKey:@"is_dinein"]) tempPromotionOrderType |= PromotionOrderTypeDineIn;
    if([dict boolForKey:@"is_takeout"]) tempPromotionOrderType |= PromotionOrderTypeTakeout;
    if([dict boolForKey:@"is_delivery"]) tempPromotionOrderType |= PromotionOrderTypeDelivery;
    
    self.promotionOrderType = tempPromotionOrderType;
    
    if([dict boolForKey:@"by_merchant"]){
        self.promotionOrigin = PromotionOriginMerchant;
    } else {
        self.promotionOrigin = PromotionOriginRO;
    }
}

- (NSString *)discountString
{
    if(self.isFlatDiscount){
        return [[NSNumber numberWithCurrency:self.discountAmount] currencyString];
    } else {
        return [[NSNumber numberWithCurrency:self.discountRate] percentString];
    }
}

- (NSString *)minOrderAmountString
{
    return [[NSNumber numberWithCurrency:self.minOrderAmount] currencyString];
}

- (NSString *)appliedDiscountAmountString
{
    return [[NSNumber numberWithCurrency:(-self.appliedDiscountAmount)] currencyString];
}

- (BOOL)byMerchant
{
    return (self.promotionOrigin == PromotionOriginMerchant);
}

@end
