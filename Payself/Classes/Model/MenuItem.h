//
//  TableItem.h
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCModel.h"
#import "MenuPrice.h"
#import "MenuPan.h"
#import "Merchant.h"

@interface MenuItem : PCModel

@property (nonatomic) PCSerial menuItemNo;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *desc;
@property (nonatomic, readonly) PCCurrency price;
@property (nonatomic, readonly) PCCurrency basePrice;
@property (copy, nonatomic) NSURL *imageURL;
@property (copy, nonatomic) NSURL *thumbnailURL;
@property (strong, nonatomic) NSArray *prices;
@property (strong, nonatomic) NSArray *optionGroups;
@property (strong, nonatomic) MenuPan *menuPan;
@property (strong, nonatomic) Merchant *merchant;
@property (nonatomic) BOOL taxable;
@property (nonatomic) float taxRate;

@property (nonatomic) BOOL forDineIn;
@property (nonatomic) BOOL forTakeout;
@property (nonatomic) BOOL forDelivery;
@property (nonatomic, getter = isAlcohol) BOOL alcohol;

// UI State property
@property (nonatomic, getter = isFlipped) BOOL flipped;
@property (nonatomic) CGFloat cellHeight;

- (MenuPrice *)defaultMenuPrice;
- (MenuPrice *)selectedMenuPrice;
- (NSMutableArray *)selectedOptions;

- (id)initWithDictionary:(NSDictionary *)dict;

- (void)resetAllOptions;

- (void)linkMenuPan:(MenuPan *)menuPan;
@end
