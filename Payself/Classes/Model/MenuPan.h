//
//  TableItem.h
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCModel.h"
#import "MenuPrice.h"

@interface MenuPan : PCModel

@property (nonatomic) PCSerial menuPanNo;
@property (copy, nonatomic) NSString *name;
@property (nonatomic, getter = isActive) BOOL active;

@property (nonatomic, strong) NSMutableArray *menus;
@property (strong, nonatomic) NSMutableArray *menuHours;
@property (nonatomic, getter = isAlwaysOn) BOOL alwaysOn;
@property (readonly) BOOL isMenuHour;
@property (nonatomic, strong) NSTimeZone *timeZone;
@property (nonatomic) NSInteger nearestAvailableTimeIndex;


- (id)initWithDictionary:(NSDictionary *)dict withTimeZone:(NSTimeZone *)timeZone;

- (NSString *)nextAvailableTime;
@end
