//
//  CustomerInfo.m
//  RushOrder
//
//  Created by Conan on 3/2/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "CustomerInfo.h"


#define INSERT_QRY @"insert into 'CustomerInfo'(\
customerNo\
,email\
,name\
,firstName\
,lastName\
,photoURL\
,phoneNumber\
,birthday\
,verifiedDate) \
values \
(?,?,?,?,?,?,?,?,?)"

#define UPDATE_QRY @"update 'CustomerInfo' set \
customerNo = ?\
,email = ?\
,name = ?\
,firstName = ?\
,lastName = ?\
,photoURL = ?\
,phoneNumber = ?\
,birthday = ?\
,verifiedDate = ?"


@implementation CustomerInfo

- (id)init
{
    self = [super init];
    if(self != nil){
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    CustomerInfo *copy = [[[self class] alloc] init];
    
    if (copy) {
        copy.customerNo = self.customerNo;
        copy.email = self.email;
        copy.phoneNumber = self.phoneNumber;
        copy.name = self.name;
        copy.firstName = self.firstName;
        copy.lastName = self.lastName;
        copy.photoURL = self.photoURL;
        copy.authToken = self.authToken;
        copy.verifiedDate = self.verifiedDate;
        copy.birthday = self.birthday;
        copy.credit = self.credit;
        copy.rewardedCredit = self.rewardedCredit;
        copy.usedCredit = self.usedCredit;
        copy.referralBadgeCount = self.referralBadgeCount;
    }
    
    return copy;
}

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [self init];
    if(self != nil){
        [self updateWithDictionary:dict];
    }
    return self;
}

- (void)updateWithDictionary:(NSDictionary *)dict
{
    if(dict == nil) return;
    
    self.customerNo = [dict serialForKey:@"id"];
    self.email = [dict objectForKey:@"email"];
    self.name = [dict objectForKey:@"name"];
    
    self.firstName = [dict objectForKey:@"first_name"];
    self.lastName = [dict objectForKey:@"last_name"];
    self.credit = [dict integerForKey:@"credit_balance"];
    
    NSDictionary *photoDict = [dict objectForKey:@"photo"];
    
    NSString *photoURLString = [photoDict objectForKey:@"url"];
    
    if(photoURLString != nil && ![photoURLString hasPrefix:@"http"]){
        
#ifdef SSL_ENABLED
        NSString *urlProtocol = @"https://";
#else
        NSString *urlProtocol = @"http://";
#endif
        NSString *serverAPIURL = [urlProtocol stringByAppendingString:SERVER_URL_ADDRESS];
        photoURLString = [serverAPIURL stringByAppendingString:photoURLString];
        
    }
    
    if([photoURLString length] > 0){
        self.photoURL = [NSURL URLWithString:photoURLString];
    } else {
        self.photoURL = nil;
    }
    
    self.phoneNumber = [dict objectForKey:@"phone_number"];
    self.birthday = [dict objectForKey:@"birthday"];
    self.verifiedDate = [[dict objectForKey:@"verified_at"] date];
    self.referralBadgeCount = [dict integerForKey:@"referral_badge_count"];
}

#pragma mark - PCModelProtocol
- (id)initWithFMResult:(FMResultSet *)result
{
    self = [super init];
    if(self != nil){
        self.customerNo = [result longLongIntForColumn:@"customerNo"];
        self.email = [result stringForColumn:@"email"];
        self.name = [result stringForColumn:@"name"];
        self.firstName = [result stringForColumn:@"firstName"];
        self.lastName = [result stringForColumn:@"lastName"];
        NSString *photoURLString = [result stringForColumn:@"photoURL"];
        self.photoURL = [NSURL URLWithString:photoURLString];
        self.phoneNumber = [result stringForColumn:@"phoneNumber"];
        self.birthday = [result stringForColumn:@"birthday"];
        self.verifiedDate = [result dateForColumn:@"verifiedDate"];
        
        //TODO :get Profile Photo if saved
    }
    
    return self;
}

- (BOOL)insert
{
    
    if([self openDB]){
        
        return [DB.db executeUpdate:INSERT_QRY,
                [NSNumber numberWithLongLong:self.customerNo],
                self.email,
                self.name,
                self.firstName,
                self.lastName,
                [self.photoURL absoluteString],
                self.phoneNumber,
                self.birthday,
                self.verifiedDate];
    } else {
        PCError(@"CustomerInfo insert error");
        return NO;
    }
    
    return [self closeDB];
}

- (BOOL)update
{
    if([self openDB]){
        
        return [DB.db executeUpdate:UPDATE_QRY,
                [NSNumber numberWithLongLong:self.customerNo],
                self.email,
                self.name,
                self.firstName,
                self.lastName,
                [self.photoURL absoluteString],
                self.phoneNumber,
                self.birthday,
                self.verifiedDate];
        
    } else {
        PCError(@"CustomerInfo update error");
        return NO;
    }
    
    return [self closeDB];
}

- (NSString *)totalEarnedCreditString
{
    return [[NSNumber numberWithCurrency:self.totalEarnedCredit] pointString];
}

- (NSNumber *)customerNoNumber
{
    return [NSNumber numberWithLongLong:self.customerNo];
}

@end