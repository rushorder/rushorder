//
//  Payment.h
//  RushOrder
//
//  Created by Conan on 2/22/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCModel.h"
#import "MobileCard.h"
#import "Order.h"
#import "Promotion.h"
#import "LineItem.h"
#import "Payment.h"

typedef enum receiptType_{
    ReceiptTypeUnknown = 0,
    ReceiptTypePay = 1,
    ReceiptTypeRefund,
    ReceiptTypeManual
} ReceiptType;

@interface Receipt : PCModel <PCModelProtocol>

@property (nonatomic) PCSerial receiptNo;
@property (nonatomic) PCSerial paymentNo;
@property (nonatomic) PCSerial orderNo;
@property (nonatomic) PCSerial merchantNo;
@property (nonatomic) PCSerial customerNo;
@property (nonatomic) ReceiptType receiptType;

//Merchant
@property (copy, nonatomic) NSString *merchantName;
@property (copy, nonatomic) NSString *merchantDesc;
@property (copy, nonatomic) NSString *merchantPhone;
@property (copy, nonatomic) NSString *merchantImageURLString;
@property (copy, nonatomic) NSString *merchantLogoURLString;

//Order
@property (nonatomic, copy) NSDate *orderDate;
@property (nonatomic, copy) NSDate *expectedDate;
@property (nonatomic, copy) NSDate *confirmDate;
@property (nonatomic, copy) NSString *staffName;
@property (nonatomic) OrderType orderType;
@property (copy, nonatomic) NSString *ticketNumber;
@property (copy, nonatomic) NSString *tableNumber;

//hirearchy
@property (strong, nonatomic) Payment *payment;


//temp
@property (nonatomic, getter = isTestMode) BOOL testMode;
// Calculated
@property (readonly) NSURL *merchantImageURL;
@property (readonly) NSURL *merchantLogoURL;
@property (readonly) NSString *typeString;

@property (nonatomic) PCCurrency subTotal;
@property (nonatomic) PCCurrency subTotalTaxable;
@property (nonatomic) PCCurrency taxes;
@property (nonatomic) PCCurrency grandTotal;

@property (readonly) NSString *subTotalString;
@property (readonly) NSString *subTotalTaxableString;
@property (readonly) NSString *taxesString;
@property (readonly) NSString *taxNroServiceFeeAmountString;
@property (readonly) NSString *grandTotalString;



@property (strong, nonatomic) NSMutableArray *lineItems;
@property (strong, nonatomic) NSMutableArray *allItems;

- (id)initWithDictionary:(NSDictionary *)dict;

- (void)summation;
- (void)summationWithTaxCalc:(BOOL)withTaxCalc merchant:(Merchant *)merchant;

@end
