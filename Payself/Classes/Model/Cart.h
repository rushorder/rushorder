//
//  Cart.h
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuItem.h"
#import "LineItem.h"
#import "Merchant.h"
#import "FavoriteOrder.h"



extern NSString * const LineItemChangedNotification;
extern NSString * const CartUnsolicitChangedNotification;

@interface Cart : PCModel

// Persistent
@property (nonatomic) PCSerial cartNo;
//@property (nonatomic) PCSerial tableNo;
@property (nonatomic) PCSerial merchantNo;
@property (copy, nonatomic) NSString *tableLabel;
@property (nonatomic) CartStatus status;
@property (nonatomic) CartType cartType;
@property (copy, nonatomic) NSString *flagNumber;

// Object Graph
@property (strong, nonatomic) Merchant *merchant;
@property (strong, nonatomic) NSMutableArray *lineItems;

// Volatile
@property (copy, nonatomic) NSString *deviceToken;
@property (copy, nonatomic) NSString *udid;
@property (nonatomic) PCSerial orderNo;
@property (nonatomic) PCCurrency subtotalAmountTaxable;



// Calculated properties
@property (nonatomic) NSInteger quantities;
@property (nonatomic) PCCurrency amount;
@property (nonatomic) PCCurrency taxAmount;

@property (copy, nonatomic) NSString *lineItemNames;

// Readonly properties
@property (readonly) NSString *amountString;
@property (readonly) NSString *taxAmountString;
@property (readonly) PCCurrency subtotalAmount;
@property (readonly) NSString *subtotalAmountTaxableString;
@property (readonly) NSString *subtotalAmountString;
@property (readonly) NSString *statusString;
@property (readonly) BOOL isLocal;
@property (readonly) BOOL isValid;
@property (readonly) BOOL hasAlcohol;
@property (readonly) BOOL hasCorruptedItem;

// Temp
@property (nonatomic, getter = isAdditionalCart) BOOL additionalCart;
@property (copy, nonatomic) NSString *merchantName;
@property (readonly) BOOL isMine;
@property (readonly) BOOL isMineIncluded;
@property (readonly) BOOL shouldBeRemoteCart;

@property (strong, nonatomic) NSDate *createdDate;
@property (strong, nonatomic) NSDate *lastAccessedTime;
@property (strong, nonatomic) NSDate *placedDate;

@property (readonly) NSString *statusGuideString;

@property (nonatomic, copy) NSString *staffName;
@property (nonatomic) PCSerial staffNo;
@property (readonly) NSString *typeString;
@property (readonly) UIImage *cartTypeIcon;

// Temp Object Graph
@property (strong, nonatomic) NSMutableArray *lineItemsSection;

@property (strong, nonatomic) NSMutableArray *customers;

@property (nonatomic, getter = isNonInteractive) BOOL nonInteractive;

+ (NSString *)typeStringForCartType:(CartType)cartType;

//- (id)initWithDictionary:(NSDictionary *)dict menuPan:(NSArray *)menuPan;
- (void)updateWithDictionary:(NSDictionary *)dict menuPan:(NSArray *)menuPan;
- (void)updateMerchantWithDict:(NSDictionary *)merchantDict;

- (id)initWithFavroiteOrder:(FavoriteOrder *)favoriteOrder menuPan:(NSArray *)menuPan;

- (void)addMenuItem:(MenuItem *)menuItem;
- (void)addMenuItem:(MenuItem *)menuItem
           quantity:(NSInteger)quantity
specialInstructions:(NSString *)specialInstructions
            success:(void (^)(BOOL isOnlyLocal))successBlock
            failure:(void (^)())failureBlock;

- (void)updateLineItem:(LineItem *)lineItem;

- (void)updateLineItem:(LineItem *)lineItem
               success:(void (^)(BOOL isOnlyLocal))successBlock
               failure:(void (^)())failureBlock;

- (void)updateLineItem:(LineItem *)lineItem
overidePriceAndOptions:(BOOL)overidePriceAndOptions
               success:(void (^)(BOOL isOnlyLocal))successBlock
               failure:(void (^)())failureBlock;

- (void)subtractLineItem:(LineItem *)lineItem
                 success:(void (^)(BOOL isOnlyLocal))successBlock
                 failure:(void (^)())failureBlock;

- (void)removeLineItem:(LineItem *)lineItem
               success:(void (^)(BOOL isOnlyLocal))successBlock
               failure:(void (^)())failureBlock;

- (void)subtractLineItem:(LineItem *)lineItem
                      by:(NSInteger)quantity
                 success:(void (^)(BOOL isOnlyLocal))successBlock
                 failure:(void (^)())failureBlock;

- (void)removeAllFromCartSuccess:(void (^)(BOOL isOnlyLocal))successBlock
                         failure:(void (^)())failureBlock;

- (void)updateStatus:(CartStatus)cartStatus
             success:(void (^)(BOOL isOnlyLocal))successBlock
             failure:(void (^)())failureBlock;

#ifdef RUSHORDER_MERCHANT
- (void)updateOrderingWithSuccess:(void (^)(BOOL isOnlyLocal))successBlock
                          failure:(void (^)())failureBlock;
#else
- (void)updateSubmitted:(BOOL)isSubmitted
                success:(void (^)(BOOL isOnlyLocal))successBlock
                failure:(void (^)())failureBlock;
#endif

- (void)summation;

- (BOOL)isAllItemsInHour;
- (BOOL)isAllItemsInHour:(BOOL)forciblyReLinkMenu;

@end