//
//  DeliveryAddress.m
//  RushOrder
//
//  Created by Conan on 2/20/17.
//  Copyright (c) 2017 RushOrder. All rights reserved.
//

#import "DeliveryAddress.h"
#import "PCPaymentService.h"
#import "RemoteDataManager.h"
#import "PCCredentialService.h"

@implementation DeliveryAddress

+ (NSString *)orderByQuery
{
    return @"order by lastUsedDate desc";
}

+ (void)newDeliveryAddressWithAddress:(Addresses *)address
{
    RequestResult result = RRFail;
    
    DeliveryAddress *deliveryAddress = [[DeliveryAddress alloc] init];
    
    [deliveryAddress updateWithAddresses:address];
    
    result = [PAY requestAddDeliveryAddress:deliveryAddress
                            completionBlock:^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode) {
                                if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                                    switch(response.errorCode){
                                        case ResponseSuccess:{
                                            [REMOTE handleDeliveryAddressesResponse:(NSMutableArray *)response
                                                                   withNotification:YES];
                                        }
                                            break;
                                        default:
                                        {
                                            NSString *message = [response objectForKey:@"message"];
                                            NSString *title = [response objectForKey:@"title"];
                                            if(title == nil){
                                                [UIAlertView alert:message];
                                            } else {
                                                [UIAlertView alertWithTitle:title
                                                                    message:message];
                                            }
                                        }
                                            break;
                                    }
                                } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                                    [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                                    [CRED resetUserAccount];
                                } else {
                                    if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                                        [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                                            object:nil];
                                    } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                                        [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                                            ,statusCode]];
                                    }
                                }
                                [SVProgressHUD dismiss];
                            }];
    
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            [SVProgressHUD dismiss];
            break;
        default:
            [SVProgressHUD dismiss];
            break;
    }
}

+ (void)updateDeliveryAddress:(DeliveryAddress *)deliveryAddress withAddress:(Addresses *)address
{
    RequestResult result = RRFail;
    
    if(address != nil){
        [deliveryAddress updateWithAddresses:address];
    } else {
        deliveryAddress.defaultAddress = YES;
    }
    
    result = [PAY requestUpdateDeliveryAddress:deliveryAddress
                               completionBlock:^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode) {
                                   if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                                       switch(response.errorCode){
                                           case ResponseSuccess:
                                               
//                                               [self.navigationController popViewControllerAnimated:YES];
                                               
                                               [REMOTE handleDeliveryAddressesResponse:(NSMutableArray *)response
                                                                      withNotification:YES];
                                               
                                               break;
                                           default:
                                           {
                                               NSString *message = [response objectForKey:@"message"];
                                               NSString *title = [response objectForKey:@"title"];
                                               if(title == nil){
                                                   [UIAlertView alert:message];
                                               } else {
                                                   [UIAlertView alertWithTitle:title
                                                                       message:message];
                                               }
                                           }
                                               break;
                                       }
                                   } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                                       [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                                       [CRED resetUserAccount];
                                   } else {
                                       if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                                           [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                                               object:nil];
                                       } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                                           [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                                               ,statusCode]];
                                       }
                                   }
                                   [SVProgressHUD dismiss];
                               }];
    
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            [SVProgressHUD dismiss];
            break;
        default:
            [SVProgressHUD dismiss];
            break;
    }
}

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self != nil){
        [self updateWithDictionary:dict];
    }
    return self;
}

- (void)updateWithDictionary:(NSDictionary *)dict
{
    if(dict != nil){
        self.no = [dict integerForKey:@"id"];
        self.name = [dict objectForKey:@"name"];
        self.phoneNumber = [dict objectForKey:@"phone_number"];
        self.street = [dict objectForKey:@"street"];
        self.street2 = [dict objectForKey:@"street2"];
        self.city = [dict objectForKey:@"city"];
        self.state = [dict objectForKey:@"state"];
        self.zip = [dict objectForKey:@"zip"];
        self.deliveryInstruction = [dict objectForKey:@"delivery_instruction"];
        
        CLLocationDegrees latitude = [[dict objectForKey:@"latitude"] doubleValue];
        CLLocationDegrees longitude = [[dict objectForKey:@"longitude"] doubleValue];
        self.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
        
        self.defaultAddress = [dict boolForKey:@"default"];
    }
}

- (NSString *)fullAddress
{
    NSMutableString *address = [NSMutableString string];
    
    if([self.name length] > 0){
        [address appendString:self.name];
    }
    
    if([self.street length] > 0){
        if([address length] > 0) [address appendString:@"\n"];
        [address appendString:self.street];
    }
    
    if([self.street2 length] > 0){
        if([address length] > 0) [address appendString:@", "];
        [address appendString:self.street2];
    }
    
    if([self.city length] > 0){
        if([address length] > 0) [address appendString:@"\n"];
        [address appendString:self.city];
    }
    
    if([self.state length] > 0){
        if([address length] > 0) [address appendString:@", "];
        [address appendString:self.state];
    }
    
    if([self.zip length] > 0){
        if([address length] > 0) [address appendString:@" "];
        [address appendString:self.zip];
    }
    
    
    if([self.phoneNumber length] > 0){
        if([address length] > 0) [address appendString:@"\n"];
        [address appendString:self.phoneNumber];
    }
    
    return address;
}

- (NSString *)address
{
    NSMutableString *address = [NSMutableString string];
    
    [address appendString:self.street];
    
    if([self.street2 length] > 0){
        [address appendString:@", "];
        [address appendString:self.street2];
    }
    
    if([self.city length] > 0){
        [address appendString:@"\n"];
        [address appendString:self.city];
    }
    
    if([self.state length] > 0){
        [address appendString:@", "];
        [address appendString:self.state];
    }
    
    if([self.zip length] > 0){
        [address appendString:@" "];
        [address appendString:self.zip];
    }
    
    return address;
}

- (NSString *)shortAddress
{
    NSMutableString *address = [NSMutableString string];
    
    [address appendString:self.street];
    
    if([self.street2 length] > 0){
        [address appendString:@", "];
        [address appendString:self.street2];
    }
    
    return address;
}

- (NSString *)shortAddressWithName
{
    NSMutableString *address = [NSMutableString string];
    
    [address appendString:self.name];
    
    if([self.street length] > 0){
        if([address length] > 0) [address appendString:@"\n"];
        [address appendString:self.street];
    }
    
    if([self.street2 length] > 0){
        if([address length] > 0) [address appendString:@", "];
        [address appendString:self.street2];
    }
    
    return address;
}

- (DeliveryAddressSimilarity)sameWithStreet:(NSString *)street street2:(NSString *)street2 city:(NSString *)city state:(NSString *)state zip:(NSString *)zip phoneNumber:(NSString *)phoneNumber name:(NSString *)name
{
    DeliveryAddressSimilarity rtn = DeliveryAddressSimilarityDifferent;
    
    if(![street isEqualToString:self.street]){ return rtn; }
    if(![city isEqualToString:self.city]){ return rtn; }
    if(![state isEqualToString:self.state]){ return rtn; }
    if(![zip isEqualToString:self.zip]){ return rtn; }
    
    rtn = DeliveryAddressSimilaritySimilar;
    
    if(![street2 isEqualToString:self.street2]){ return rtn; }
    if(![name isEqualToString:self.name]){ return rtn; }
    if(![phoneNumber isEqualToString:self.phoneNumber]){ return rtn; }
    
    rtn = DeliveryAddressSimilaritySame;
    
    return rtn;
}

- (DeliveryAddressSimilarity)sameWithAddresses:(Addresses *)address
{
    DeliveryAddressSimilarity rtn = DeliveryAddressSimilarityDifferent;
    
    if(![address.address1 isEqualToString:self.street]){ return rtn; }
    if(![address.city isEqualToString:self.city]){ return rtn; }
    if(![address.state isEqualToString:self.state]){ return rtn; }
    if(![address.zip isEqualToString:self.zip]){ return rtn; }
    
    rtn = DeliveryAddressSimilaritySimilar;
    
    if(![address.receiverName isEqualToString:self.name]){ return rtn; }
    if(![address.phoneNumber isEqualToString:self.phoneNumber]){ return rtn; }
    if(![address.address2 isEqualToString:self.street2]){ return rtn; }
    
    rtn = DeliveryAddressSimilaritySame;
    
    return rtn;
}

- (void)updateWithAddresses:(Addresses *)address
{
    self.name = address.receiverName;
    self.phoneNumber = address.phoneNumber;
    self.street = address.address1;
    self.street2 = address.address2;
    self.city = address.city;
    self.state = address.state;
    self.zip = address.zip;
    
    self.defaultAddress = address.needToBeUpdatedToDefault;
}



@end
