//
//  MenuPrice.m
//  RushOrder
//
//  Created by Conan on 5/24/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MenuPrice.h"

@implementation MenuPrice

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [self init];
    
    if(self != nil){
        self.menuPriceNo = [dict serialForKey:@"id"];
        self.name = [dict objectForKey:@"name"];
        self.price = [dict currencyForKey:@"price"];
        self.defaultPrice = [dict boolForKey:@"default"];
    }
    return self;
}

- (NSString *)priceString
{
    return [[NSNumber numberWithCurrency:self.price] currencyString];
}

- (void)setDefaultPrice:(BOOL)defaultPrice
{
    _defaultPrice = defaultPrice;
    _selected = defaultPrice;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@{ \n\t no:%lld \n\t name:%@ \n\t price:%ld\n\t}",
            [super description],
            self.menuPriceNo,
            self.name,
            (long)self.price];
}

@end