//
//  FavoriteOrder.m
//  RushOrder
//
//  Created by Conan on 1/7/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import "FavoriteOrder.h"

@implementation FavoriteOrder

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self){
        [self updateWithDictionary:dict];
    }
    
    return self;
}

- (void)updateWithDictionary:(NSDictionary *)dict
{
    self.favoriteOrderNo = [dict serialForKey:@"id"];
    self.title = [dict objectForKey:@"title"];
    self.merchantName = [dict objectForKey:@"merchant_name"];
    self.merchantAddr = [dict objectForKey:@"merchant_address"];
    
    self.merchant = [[Merchant alloc] initWithDictionary:[dict objectForKey:@"merchant"]];
    self.merchantNo = self.merchant.merchantNo;
    
    NSString *imageUrlString = [dict objectForKey:@"image"];
    if(imageUrlString != nil)
        self.imageURL = [[NSURL alloc] initWithString:imageUrlString];
    
    self.orderId = [dict serialForKey:@"order_id"];
    
    self.merchantTaxRate = [dict floatForKey:@"merchant_tax_rate"];
    self.roFeeRate = [dict floatForKey:@"ro_fee_rate"];
    self.orderType = [[dict objectForKey:@"order_type"] orderType];
    self.lineItemsJSON = [dict objectForKey:@"line_items"];
    
    NSMutableArray *lineItems = [NSMutableArray array];
    NSMutableString *lineItemNames = [NSMutableString string];
    
    for(NSDictionary *lineItemDict in self.lineItemsJSON){
        LineItem *lineItem = [[LineItem alloc] initWithDictionary:lineItemDict
                                                          menuPan:nil];
        lineItem.merchantNo = self.merchantNo;
        [lineItems addObject:lineItem];
        if([lineItemNames length] > 0){
            [lineItemNames appendString:@", "];
        }
        [lineItemNames appendString:lineItem.menuName];
        if(lineItem.quantity > 1){
            [lineItemNames appendFormat:@"(%ld)", (long)lineItem.quantity];
        }
    }
    
    self.lineItemNames = lineItemNames;
    self.lineItems = lineItems;
        self.subTotal = [dict currencyForKey:@"sub_total_amount"];
    if([dict objectForKey:@"sub_total_amount_taxable"] == nil){
        self.subTotalTaxable = [dict currencyForKey:@"sub_total_amount"];
    } else {
        self.subTotalTaxable = [dict currencyForKey:@"sub_total_amount_taxable"];
    }
    self.taxes = [dict currencyForKey:@"tax_amount"];
    self.tips = [dict currencyForKey:@"tip_amount"];
    self.deliveryFee = [dict currencyForKey:@"delivery_fee_amount"];
    self.roServiceFee = [dict currencyForKey:@"ro_service_fee_amount"];
    
    self.deviceToken = [dict objectForKey:@"customer_device_token"];
    self.pickupAfter = [dict integerForKey:@"pickup_after"];
    self.customerRequest = [dict objectForKey:@"customer_request"];
    self.city = [dict objectForKey:@"delivery_address_city"];
    self.state = [dict objectForKey:@"delivery_address_state"];
    self.address1 = [dict objectForKey:@"delivery_address_street_1"];
    self.address2 = [dict objectForKey:@"delivery_address_street_2"];
    self.zip = [dict objectForKey:@"delivery_address_zipcode"];
    
    CLLocationDegrees latitude = [dict doubleForKey:@"delivery_latitude"];
    CLLocationDegrees longitude = [dict doubleForKey:@"delivery_longitude"];
    
    self.location = [[CLLocation alloc] initWithLatitude:latitude
                                               longitude:longitude];
    
    self.phoneNumber = [dict objectForKey:@"delivery_phone_number"];
    self.receiverName = [dict objectForKey:@"delivery_customer_name"];
    
    self.tableLabel = [dict objectForKey:@"table_label"];
    self.customerId = [dict serialForKey:@"customer_id"];
    self.uuid = [dict objectForKey:@"uuid"];
    
    self.card = [[MobileCard alloc] initWithDictionary:[dict objectForKey:@"card"]];
}

- (id)copyWithZone:(NSZone *)zone
{
    FavoriteOrder *copy = [[FavoriteOrder alloc] init];
    
    if(copy){
        copy.favoriteOrderNo = self.favoriteOrderNo;
        copy.image = self.image;
        copy.title = self.title;
        copy.customerRequest = self.customerRequest;
        
        copy.location = self.location;
    }
    
    return copy;
}

- (PCCurrency)total
{
    return self.subTotal + self.taxes + self.deliveryFee + self.roServiceFee;
}

- (PCCurrency)grandTotal
{
    // !!!: Bill 발행으로 결제하면서 나누어 지불하는 경우(스플릿)
    // tips 값은 초기 tip값만 DB에 저장되고 업데이트 되지 않으므로 사용에 주의 할 것.
    // 현재는 참고용으로만 사용하고 payment의 tip을 사용하므로 현상 유지
    // -> 안전을 위해 수정될 필요가 있어보임
    return self.total + self.tips;
}

- (PCCurrency)dueTotal
{
    return self.grandTotal;
}

- (NSString *)subTotalString
{
    return [[NSNumber numberWithCurrency:self.subTotal] currencyString];
}

- (NSString *)subTotalTaxableString
{
    return [[NSNumber numberWithCurrency:self.subTotalTaxable] currencyString];
}

- (NSString *)taxesString
{
    return [[NSNumber numberWithCurrency:self.taxes] currencyString];
}

- (NSString *)taxNroServiceFeeAmountString
{
    return [[NSNumber numberWithCurrency:(self.taxes + self.roServiceFee)] currencyString];
}

- (NSString *)deliveryFeeString
{
    return [[NSNumber numberWithCurrency:self.deliveryFee] currencyString];
}


- (NSString *)roServiceFeeString
{
    return [[NSNumber numberWithCurrency:self.roServiceFee] currencyString];
}


- (NSString *)tipsString
{
    return [[NSNumber numberWithCurrency:self.tips] currencyString];
}

- (NSString *)grandTotalString
{
    return [[NSNumber numberWithCurrency:self.grandTotal] currencyString];
}

- (NSString *)dueTotalString
{
    return [[NSNumber numberWithCurrency:self.dueTotal] currencyString];
}

- (NSString *)totalString
{
    return [[NSNumber numberWithCurrency:self.total] currencyString];
}

- (NSString *)address
{
    NSMutableString *address = [NSMutableString string];
    
    if([self.address1 length] > 0){
        [address appendString:self.address1];
    }
    
    if([self.address2 length] > 0){
        [address appendString:@", "];
        [address appendString:self.address2];
    }
    
    if([self.city length] > 0){
        [address appendString:@"\n"];
        [address appendString:self.city];
    }
    
    if([self.state length] > 0){
        [address appendString:@", "];
        [address appendString:self.state];
    }
    
    if([self.zip length] > 0){
        [address appendString:@" "];
        [address appendString:self.zip];
    }
    
    return address;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@{ \n\t FavoriteOrder No:%lld \n\t subTotal:%ld \n\t subTotalTaxable:%ld \n\t Taxes:%ld\n\t}",
            [super description],
            self.favoriteOrderNo,
            (long)self.subTotal,
            (long)self.subTotalTaxable,
            (long)self.taxes];
}

- (NSString *)typeString
{
    return [Order typeStringForOrderType:self.orderType];
}

+ (NSString *)typeStringForOrderType:(OrderType)orderType
{
    switch(orderType){
        case OrderTypeBill:
        case OrderTypeCart:
        case OrderTypeOrderOnly:
        case OrderTypePickup:
            return @"Dine-in";
            break;
        case OrderTypeTakeout:
            return @"Take-out";
            break;
        case OrderTypeDelivery:
            return @"Delivery";
            break;
        default:
            return @"Unknown";
    }
}

- (UIImage *)orderTypeIcon
{
    switch(self.orderType){
        case OrderTypeBill:
        case OrderTypeCart:
        case OrderTypeOrderOnly:
        case OrderTypePickup:
            return [UIImage imageNamed:@"button_icon_dinein"];
            break;
        case OrderTypeTakeout:
            return [UIImage imageNamed:@"button_icon_takeout"];
            break;
        case OrderTypeDelivery:
            return [UIImage imageNamed:@"button_icon_delivery"];
            break;
        default:
            return [UIImage imageNamed:@""];
    }
}

@end
