//
//  FavoriteOrder.h
//  RushOrder
//
//  Created by Conan on 1/7/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import "PCModel.h"
#import "Order.h"
#import "Merchant.h"
#import "MobileCard.h"

@interface FavoriteOrder : PCModel <NSCopying>

@property (nonatomic) PCSerial favoriteOrderNo;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *merchantName;
@property (copy, nonatomic) NSString *merchantAddr;
@property (strong, nonatomic) NSURL *imageURL;
@property (strong, nonatomic) UIImage *image;
@property (nonatomic) PCSerial orderId;
@property (nonatomic) PCSerial merchantNo;

@property (strong, nonatomic) MobileCard *card;

@property (strong, nonatomic) Merchant *merchant;

@property (nonatomic) float merchantTaxRate;
@property (nonatomic) float roFeeRate;
@property (nonatomic) OrderType orderType;
@property (strong, nonatomic) NSArray *lineItemsJSON;
@property (strong, nonatomic) NSArray *lineItems;
@property (copy, nonatomic) NSString *lineItemNames;
@property (nonatomic) PCCurrency subTotal;
@property (nonatomic) PCCurrency subTotalTaxable;
@property (nonatomic) PCCurrency taxes;
@property (nonatomic) PCCurrency tips;
@property (nonatomic) PCCurrency deliveryFee;
@property (nonatomic) PCCurrency roServiceFee;

@property (copy, nonatomic) NSString *deviceToken;
@property (nonatomic) NSInteger pickupAfter;
@property (copy, nonatomic) NSString *customerRequest;
@property (copy, nonatomic) NSString *city;
@property (copy, nonatomic) NSString *state;
@property (copy, nonatomic) NSString *address1;
@property (copy, nonatomic) NSString *address2;
@property (copy, nonatomic) NSString *zip;
@property (strong, nonatomic) CLLocation *location;
@property (copy, nonatomic) NSString *phoneNumber;
@property (copy, nonatomic) NSString *receiverName;
@property (copy, nonatomic) NSString *tableLabel;
@property (nonatomic) PCSerial customerId;
@property (copy, nonatomic) NSString *uuid;

// Readonly
@property (readonly) NSString *address;

@property (readonly) PCCurrency total;
@property (readonly) PCCurrency grandTotal;
@property (readonly) PCCurrency dueTotal;


@property (readonly) NSString *typeString;
@property (readonly) UIImage *orderTypeIcon;

@property (readonly) NSString *subTotalString;
@property (readonly) NSString *subTotalTaxableString;
@property (readonly) NSString *taxesString;
@property (readonly) NSString *taxNroServiceFeeAmountString;
@property (readonly) NSString *deliveryFeeString;
@property (readonly) NSString *roServiceFeeString;
@property (readonly) NSString *totalPaidDeliveryFeeAmountsString;
@property (readonly) NSString *totalPaidRoServiceFeeAmountsString;
@property (readonly) NSString *discountedAmountString;
@property (readonly) NSString *roDiscountedAmountString;
@property (readonly) NSString *meDiscountedAmountString;
@property (readonly) NSString *tipsString;
@property (readonly) NSString *totalString;
@property (readonly) NSString *grandTotalString;
@property (readonly) NSString *dueTotalString;

- (id)initWithDictionary:(NSDictionary *)dict;
- (void)updateWithDictionary:(NSDictionary *)dict;

@end
