//
//  UserInfo.h
//  RushOrder
//
//  Created by Conan on 3/2/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

/*
 {"result":0,"data":{"address":null,"admin":false,"birth":null,"created_at":"2013-03-04T08:00:58Z","device_token":"iOS simulator fake device token","email":"kmaku@novaple.com","gender":null,"id":3,"level":null,"name":null,"phone":null,"point":null,"provider":null,"role":"owner","uid":null,"updated_at":"2013-03-13T12:36:57Z"}}
 */
#import "PCModel.h"
#import "Merchant.h"

@interface UserInfo : PCModel

@property (nonatomic) PCSerial userNo;
@property (copy, nonatomic) NSString *email;
@property (nonatomic) UserRole role;
@property (copy, nonatomic) NSString *authToken;
@property (copy, nonatomic) NSString *deviceToken;
@property (copy, nonatomic) NSString *name;
@property (nonatomic) NotificationScope notificationScope;
@property (strong, nonatomic) Merchant *merchant;

- (id)initWithDictionary:(NSDictionary *)dict;
@end

@interface NSString (UserInfo)
+ (NSString *)stringWithUserRole:(UserRole)userRole;
+ (NSString *)stringWithNotificationScope:(NotificationScope)notificationScope;
- (UserRole)userRoleEnum;
- (NotificationScope)notificationScopeEnum;
@end