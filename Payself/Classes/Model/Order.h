//
//  Order.h
//  RushOrder
//
//  Created by Conan on 2/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MenuItem.h"
#import "LineItem.h"
#import "PCModel.h"
#import "Merchant.h"
#import "MenuPan.h"
#import <CoreLocation/CoreLocation.h>

@class Merchant;

@interface Order : PCModel <PCModelProtocol, NSCopying>

@property (nonatomic) PCSerial orderNo;
@property (nonatomic) PCSerial tableNo;
@property (nonatomic) PCCurrency subTotal;
@property (nonatomic) PCCurrency subTotalTaxable;
@property (nonatomic) PCCurrency taxes;
@property (nonatomic) PCCurrency deliveryFee;
@property (nonatomic) PCCurrency roServiceFee;
// !!!: Bill 발행으로 결제하면서 나누어 지불하는 경우(스플릿)
// tips 값은 초기 tip값만 DB에 저장되고 업데이트 되지 않으므로 사용에 주의 할 것.
// 현재는 참고용으로만 사용하고 payment의 tip을 사용하므로 현상 유지
// -> 안전을 위해 수정될 필요가 있어보임
@property (nonatomic) PCCurrency tips;
@property (copy, nonatomic) NSString *merchantName;
@property (copy, nonatomic) NSString *merchantDesc;
@property (copy, nonatomic) NSString *merchantImageURLString;
@property (copy, nonatomic) NSString *merchantLogoURLString;
@property (nonatomic) CLLocationDegrees latitude;
@property (nonatomic) CLLocationDegrees longitude;
@property (nonatomic) PCSerial merchantNo;
@property (copy, nonatomic) NSString *tableNumber;
@property (nonatomic) OrderType orderType;
@property (copy, nonatomic) NSString *customerRequest;
@property (copy, nonatomic) NSString *rejectReason;
@property (nonatomic) BOOL needToAdjustment;
@property (strong, nonatomic) Merchant *merchant;

@property (nonatomic, copy) NSDate *orderDate;
@property (nonatomic, copy) NSDate *createdDate;
@property (nonatomic, copy) NSDate *updatedDate;
@property (nonatomic, copy) NSDate *expectedDate;
@property (nonatomic, copy) NSDate *expectedPreparingDate;
@property (nonatomic, copy) NSDate *acceptedDate;
@property (nonatomic, copy) NSDate *preparedDate;
@property (nonatomic, copy) NSString *completeReasonJson; //Keys : "cancelled", "cash", "card", "other", "memo"
@property (nonatomic, getter = isCustomerClose) BOOL customerClose;

//Delivering
@property (nonatomic, copy) NSString *receiverName;
@property (nonatomic, copy) NSString *address1;
@property (nonatomic, copy) NSString *address2;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *zip;
@property (nonatomic, copy) NSString *phoneNumber;
@property (nonatomic) CLLocationCoordinate2D takenCoordinate;
// Readonly
@property (readonly) NSString *address;

#pragma mark -
@property (nonatomic) BOOL paperReceipt;
@property (nonatomic) PCCurrency payAmounts;
@property (nonatomic) PCCurrency cardAmounts;
@property (nonatomic) PCCurrency creditAmounts;
@property (nonatomic) PCCurrency rewardedAmount;
@property (nonatomic) PCCurrency totalPaidDeliveryFeeAmounts;
@property (nonatomic) PCCurrency totalPaidRoServiceFeeAmounts;
@property (nonatomic) PCCurrency discountedAmount;
@property (nonatomic) PCCurrency roDiscountedAmount;
@property (nonatomic) PCCurrency meDiscountedAmount;

@property (strong, nonatomic) NSDate *lastAccessedTime;
@property (nonatomic) NSInteger pickupAfter;

@property (strong, nonatomic) NSArray *lineItems;
@property (strong, nonatomic) NSArray *paymentList;

@property (copy, nonatomic) NSString *lineItemNames;

@property (nonatomic) OrderStatus status;
@property (nonatomic) OrderStatus bfStatus;

@property (nonatomic, getter = isTestMode) BOOL testMode;

@property (nonatomic, copy) NSString *staffName;
@property (nonatomic) PCSerial staffNo;

@property (strong, nonatomic) NSMutableArray *customers;

@property (nonatomic, getter = isTwoStepProcess) BOOL twoStepProcess;

@property (nonatomic, getter = isRoService) BOOL roService;

@property (nonatomic) NSInteger quantities;

/// Readonly - not saved to persistent store
#pragma mark - Readonly

@property (readonly, nonatomic) UIImage *image;
@property (readonly) NSURL *merchantImageURL;
@property (readonly) NSURL *merchantLogoURL;
@property (readonly) UIColor *statusColor;

@property (readonly) NSString *typeString;
@property (readonly) UIImage *orderTypeIcon;
@property (readonly) NSString *statusString;
@property (readonly) NSString *hrStatusPickupString;
@property (readonly) NSString *hrCustStatusPickupString;

@property (readonly) PCCurrency total;
@property (readonly) PCCurrency grandTotal;
@property (readonly) PCCurrency dueTotal;
@property (readonly) PCCurrency remainingToPay;
@property (readonly) PCCurrency netPayAmounts;
@property (readonly) float payProgress;

@property (readonly) NSString *subTotalString;
@property (readonly) NSString *subTotalTaxableString;
@property (readonly) NSString *taxesString;
@property (readonly) NSString *deliveryFeeString;
@property (readonly) NSString *roServiceFeeString;
@property (readonly) NSString *totalPaidDeliveryFeeAmountsString;
@property (readonly) NSString *totalPaidRoServiceFeeAmountsString;
@property (readonly) NSString *discountedAmountString;
@property (readonly) NSString *roDiscountedAmountString;
@property (readonly) NSString *meDiscountedAmountString;
@property (readonly) NSString *tipsString;
@property (readonly) NSString *totalString;
@property (readonly) NSString *grandTotalString;
@property (readonly) NSString *dueTotalString;
@property (readonly) NSString *payAmountString;
@property (readonly) NSString *creditAmountString;
@property (readonly) NSString *cardAmountString;
@property (readonly) NSString *taxNroServiceFeeAmountString;

@property (readonly) BOOL isCompletelyPaid;
@property (readonly) NSString *completeReason;
@property (readonly) BOOL isTakeout;
@property (readonly) BOOL isDelivery;
@property (readonly) BOOL isTicketBase;
@property (readonly) BOOL didTimedOut;
@property (readonly) BOOL hasAlcohol;
@property (readonly) BOOL isEditable;

// For temporary near you
@property (copy, nonatomic) NSURL *customerPhotoURL;


+ (NSString *)typeStringForOrderType:(OrderType)orderType;

- (id)initWithDictionary:(NSDictionary *)dict;

- (id)initWithDictionary:(NSDictionary *)dict
                merchant:(Merchant *)mechant;

- (id)initWithDictionary:(NSDictionary *)dict
                merchant:(Merchant *)merchant
                 menuPan:(NSArray *)menuPan;

- (void)updateWithDictionary:(NSDictionary *)dict
                    merchant:(Merchant *)merchant;

- (void)updateWithDictionary:(NSDictionary *)dict
                    merchant:(Merchant *)merchant
                     menuPan:(NSArray *)menuPan;

- (void)updateWithDictionary:(NSDictionary *)dict
                    merchant:(Merchant *)merchant
                     menuPan:(NSArray *)menuPan
                withBfStatus:(BOOL)withBfStatus;

- (void)updateWithMerchant:(Merchant *)merchant;

- (void)updateSumWithPayments:(NSArray *)payments;

- (BOOL)dateChanged:(NSDate *)date;
- (BOOL)weekChanged:(NSDate *)date;
- (BOOL)monthChanged:(NSDate *)date;

- (void)sum:(Order *)order;

- (NSString *)statusGuideStringWithMerchant:(Merchant *)merchant;
- (NSString *)statusGuideStringWithMerchant:(Merchant *)merchant fully:(BOOL)fully;

- (NSComparisonResult)compareTimeWith:(Order *)anotherOrder;
- (BOOL)isEqual:(Order *)anotherOrder;

#if RUSHORDER_CUSTOMER

- (void)addMenuItem:(MenuItem *)menuItem
           quantity:(NSInteger)quantity
specialInstructions:(NSString *)specialInstructions
            success:(void (^)())successBlock
            failure:(void (^)())failureBlock;

- (void)updateLineItem:(LineItem *)lineItem
               success:(void (^)())successBlock
               failure:(void (^)())failureBlock;

- (void)updateLineItem:(LineItem *)lineItem
overidePriceAndOptions:(BOOL)overidePriceAndOptions
               success:(void (^)())successBlock
               failure:(void (^)())failureBlock;

- (void)removeLineItem:(LineItem *)lineItem
               success:(void (^)())successBlock
               failure:(void (^)())failureBlock;

- (void)subtractLineItem:(LineItem *)lineItem
                 success:(void (^)())successBlock
                 failure:(void (^)())failureBlock;

- (void)subtractLineItem:(LineItem *)lineItem
                      by:(NSInteger)quantity
                 success:(void (^)())successBlock
                 failure:(void (^)())failureBlock;

- (void)removeAllFromOrderSuccess:(void (^)(BOOL isOnlyLocal))successBlock
                          failure:(void (^)())failureBlock;

- (BOOL)isAllItemsInHour;

#endif //Payself Customer

@end
