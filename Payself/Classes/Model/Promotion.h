//
//  Promotion.h
//  RushOrder
//
//  Created by Conan on 4/12/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCModel.h"

typedef enum promotionOrigin_{
    PromotionOriginRO = 0,
    PromotionOriginMerchant = 1,
} PromotionOrigin;

typedef enum promotionOrderType_{
    PromotionOrderTypeNone = 0,
    PromotionOrderTypeDineIn = 1 << 0,
    PromotionOrderTypeTakeout = 1 << 1,
    PromotionOrderTypeDelivery = 1 << 2,
    PromotionOrdertypeAll = PromotionOrderTypeDineIn | PromotionOrderTypeTakeout | PromotionOrderTypeDelivery
} PromotionOrderType;

@interface Promotion : PCModel

@property (nonatomic) PCSerial promotionNo;
@property (nonatomic) PCSerial merchantNo;
@property (nonatomic, getter = isFlatDiscount) BOOL flatDiscount;
@property (nonatomic, getter = isRequiredCode) BOOL requiredCode;
@property (nonatomic, getter = isCombinable) BOOL combinable;
@property (copy, nonatomic) NSString *couponCode;
@property (nonatomic) float discountRate;
@property (nonatomic) PCCurrency discountAmount;
@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *endDate;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *desc;
@property (nonatomic, getter = isOneTimeUse) BOOL oneTimeUse;
@property (nonatomic, getter = isFirstTimeUse) BOOL firstTimeUse;
@property (nonatomic) PCCurrency minOrderAmount;
@property (nonatomic) PCCurrency limitDiscountAmount;
@property (nonatomic, getter = isActive) BOOL active;
@property (nonatomic) PromotionOrigin promotionOrigin;
@property (readonly) BOOL byMerchant;

@property (nonatomic) PromotionOrderType promotionOrderType;

////
@property (readonly) NSString *discountString;
@property (readonly) NSString *minOrderAmountString;

@property (nonatomic) PCCurrency appliedDiscountAmount;
@property (readonly) NSString *appliedDiscountAmountString;

+ (id)smallCutPromotion;
- (id)initWithDictionary:(NSDictionary *)dict;
- (void)updateWithDictionary:(NSDictionary *)dict;
@end
