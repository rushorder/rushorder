//
//  MobileCard.m
//  RushOrder
//
//  Created by Conan on 2/22/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MobileCard.h"
#import "FormatterManager.h"

#define INSERT_QRY @"insert into 'MobileCard'(\
cardFingerPrint\
,last4\
,cardIssuer\
,cardExpireMonth\
,cardExpireYear\
,bankName\
,pgType) \
values \
(?,?,?,?,?,?,?)"

#define UPDATE_QRY @"update 'MobileCard' set \
cardFingerPrint = ?\
,last4 = ?\
,cardIssuer = ?\
,cardExpireMonth = ?\
,cardExpireYear = ?\
,bankName = ?\
,pgType = ? where rowid = ?"

#define SELECT_QRY @"select * from MobileCard"

//#define FAKE_CARD

@interface MobileCard()

@end

@implementation MobileCard

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    if(self != nil){
        [self updateWithCardDictionary:dict];
    }
    
    return self;
}


- (id)initWithFMResult:(FMResultSet *)result
{
    self = [super init];
    
    if(self != nil){
        self.cardFingerPrint = [result stringForColumn:@"cardFingerPrint"];
        self.last4 = [result intForColumn:@"last4"];
        self.cardIssuer = [result intForColumn:@"cardIssuer"];
        self.bankName = [result stringForColumn:@"bankName"];
        self.pgType = [result intForColumn:@"pgType"];
        self.cardExpireMonth = [result intForColumn:@"cardExpireMonth"];
        self.cardExpireYear = [result intForColumn:@"cardExpireYear"];
    }
    
    return self;
}

- (BOOL)insert
{
    if([self openDB]){
        
        return [DB.db executeUpdate:INSERT_QRY,
                self.cardFingerPrint,
                [NSNumber numberWithInteger:self.last4],
                [NSNumber numberWithInteger:self.cardIssuer],
                [NSNumber numberWithInteger:self.cardExpireMonth],
                [NSNumber numberWithInteger:self.cardExpireYear],
                self.bankName,
                [NSNumber numberWithInteger:self.pgType]];
        
    } else {
        PCError(@"Mobile card inserting error");
        return NO;
    }
    
    return [self closeDB];
}

- (BOOL)update
{
    if([self openDB]){
        
        return [DB.db executeUpdate:UPDATE_QRY,
                self.cardFingerPrint,
                [NSNumber numberWithInteger:self.last4],
                [NSNumber numberWithInteger:self.cardIssuer],
                [NSNumber numberWithInteger:self.cardExpireMonth],
                [NSNumber numberWithInteger:self.cardExpireYear],
                self.bankName,
                [NSNumber numberWithInteger:self.pgType],
                [NSNumber numberWithInteger:self.rowId]];
        
    } else {
        PCError(@"Mobile card updating error");
        return NO;
    }
    
    return [self closeDB];
}

- (void)updateWithPaymentDictionary:(NSDictionary *)dict
{  
    self.cardFingerPrint = [dict objectForKey:@"card_fingerprint"];
    self.last4 = [[dict objectForKey:@"card_last4"] integerValue];
//    self.cardIssuer = [[dict objectForKey:@"card_type"] cardIssuer];
    self.cardExpireMonth = [[dict objectForKey:@"card_exp_month"] integerValue];
    self.cardExpireYear = [[dict objectForKey:@"card_exp_year"] integerValue];
    self.pgType = (PgType)[dict integerForKey:@"pg_type"];
}

- (void)updateWithCardDictionary:(NSDictionary *)dict
{
    
    self.cardId = [dict serialForKey:@"id"];
    self.cardFingerPrint = [dict objectForKey:@"stripe_fingerprint"];
    self.last4 = [dict integerForKey:@"last4"];
    self.cardExpireMonth = [dict integerForKey:@"exp_month"];
    self.cardExpireYear = [dict integerForKey:@"exp_year"];
    
    NSString *cardTypeStr = [dict objectForKey:@"card_type"];
    self.bankName = cardTypeStr;
    self.pgType = (PgType)[dict integerForKey:@"pg_type"];
    self.cardIssuer = [cardTypeStr cardIssuer];
}

- (void)setCardNumber:(NSString *)value
{
    if(![_cardNumber isEqualToString:value]){
        if(self.cardIssuer == IssuerUnknown){
            self.cardIssuer = [FORMATTER.cardNumberFormatter acquireIssuer:value];
            if(self.bankName == nil){
                self.bankName = [NSString issuerName:self.cardIssuer];
            }
        }
//        if([value length] > 4){
//            self.last4 = [[value substringFromIndex:[value length] - 4] integerValue];
//        } else {
//            self.last4 = [value integerValue];
//        }
        
        _cardNumber = value;
    }
}

- (void)setLast4:(NSInteger)last4
{
    NSNumber *last4Number = [NSNumber numberWithInteger:last4];
    NSString *cardNumberString = [last4Number stringValue];
    if(cardNumberString != nil){
        self.cardNumber = [@"000000000000" stringByAppendingString:cardNumberString];
    } else {
        self.cardNumber = [@"000000000000" stringByAppendingString:@"0000"];
    }
    
    _last4 = last4;
}



#pragma mark - readonly
- (NSString *)formattedCardNumber
{   
    return [self formattedCardNumber:YES];
}

- (NSString *)formattedCardNumber:(BOOL)securely
{
    return [FORMATTER.cardNumberFormatter format:self.cardNumber
                                      withIssuer:self.cardIssuer
                                        securely:securely];
}

- (UIImage *)cardLogo
{
    return [UIImage logoImageByIssuer:self.cardIssuer];
}


@end
