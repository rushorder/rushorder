//
//  Receipt.m
//  RushOrder
//
//  Created by Conan on 2/22/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "Receipt.h"
#import "FormatterManager.h"

#define INSERT_QRY @"insert into 'Receipt'(\
receiptNo,\
paymentNo,\
orderNo,\
merchantNo,\
customerNo,\
receiptType,\
merchantName,\
merchantDesc,\
merchantPhone,\
merchantImageURLString,\
merchantLogoURLString,\
orderDate,\
expectedDate,\
confirmDate,\
staffName,\
orderType,\
ticketNumber,\
tableNumber)\
values \
(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

#define UPDATE_QRY @"update 'Receipt' set \
paymentNo=?,\
orderNo=?,\
merchantNo=?,\
customerNo=?,\
receiptType=?,\
merchantName=?,\
merchantDesc=?,\
merchantPhone=?,\
merchantImageURLString=?,\
merchantLogoURLString=?,\
orderDate=?,\
expectedDate=?,\
confirmDate=?,\
staffName=?,\
orderType=?,\
ticketNumber=?,\
tableNumber=?)\
where ReceiptNo = ?"

@interface Receipt()

@end

@implementation Receipt

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    if(self != nil){
        [self updateReceipt:dict];
    }
    return self;
}

- (void)updateReceipt:(NSDictionary *)dict
{
    self.receiptNo = [dict serialForKey:@"id"];
    self.paymentNo = [dict serialForKey:@"payment_id"];
    self.orderNo = [dict serialForKey:@"order_id"];
    self.merchantNo = [dict serialForKey:@"merchant_id"];
    self.customerNo = [dict serialForKey:@"customer_id"];
    
    NSString *receiptType = [dict objectForKey:@"receipt_type"];
    if([receiptType isEqualToString:@"payment"]){
        self.receiptType = ReceiptTypePay;
    } else if([receiptType isEqualToString:@"refund"]){
        self.receiptType = ReceiptTypeRefund;
    } else if([receiptType isEqualToString:@"pos_payment"]){
        self.receiptType = ReceiptTypeManual;
    }
    
    //Merchant
    self.merchantName = [dict objectForKey:@"merchant_name"];
    self.merchantDesc = [dict objectForKey:@"merchant_address_multiline"];
    if([self.merchantDesc length] == 0){
        self.merchantDesc = [dict objectForKey:@"merchant_address"];
    }
    self.merchantPhone = [dict objectForKey:@"merchant_phone"];
//    self.merchantImageURLString = [dict objectForKey:@"merchant_img_url"];
    self.merchantLogoURLString = [dict objectForKey:@"merchant_logo_url"];
    
    //Order
    self.orderDate = [dict dateForKey:@"ordered_at"];
    self.expectedDate = [dict dateForKey:@"expected_at"];
    self.confirmDate = [dict dateForKey:@"confirmed_at"];
    self.staffName = [dict objectForKey:@"staff_name"];
    
    NSString *mode = [dict objectForKey:@"mode"];
    if([mode isEqualToString:@"demo"]){
        self.testMode = YES;
    } else {
        self.testMode = NO;
    }
    
    NSString *orderTypeString = [dict objectForKey:@"order_type"];
    
    if([orderTypeString isEqualToString:@"cart"]){
        self.orderType = OrderTypeCart;
    } else if([orderTypeString isEqualToString:@"togo"]){
        self.orderType = OrderTypePickup;
    } else if([orderTypeString isEqualToString:@"takeout"]){
        self.orderType = OrderTypeTakeout;
    } else if([orderTypeString isEqualToString:@"menu"]){
        self.orderType = OrderTypeOrderOnly;
    } else if([orderTypeString isEqualToString:@"delivery"]){
        self.orderType = OrderTypeDelivery;
    } else { // pay
        self.orderType = OrderTypeBill;
    }

    self.ticketNumber = [dict objectForKey:@"ticket_number"];
    self.tableNumber = [dict objectForKey:@"table_label"];
    
    NSDictionary *paymentDict = [dict objectForKey:@"payment"];
    if(paymentDict != nil){
        self.payment = [[Payment alloc] initWithDictionary:paymentDict];
    } else {
        self.payment = nil;
    }
    
    NSArray *lineItems = [dict objectForKey:@"line_items"];
    self.allItems = nil;
    if([lineItems count] > 0){
        NSMutableArray *items = [NSMutableArray array];
        for(NSDictionary *lineItemDict in lineItems){
            LineItem *lineItem = [[LineItem alloc] initWithDictionary:lineItemDict];
            lineItem.orderNo = self.orderNo;
            lineItem.merchantNo = self.merchantNo;
            lineItem.receiptNo = self.receiptNo;
            lineItem.customerNo = self.customerNo;
            lineItem.merchantNo = self.merchantNo;
            [items addObject:lineItem];
        }
        self.lineItems = items;
    }
    
    [self summation];
}

- (NSMutableArray *)allItems
{
    if(_allItems == nil){
        _allItems = [NSMutableArray array];
        for(LineItem *lineItem in self.lineItems){
            [_allItems addObject:lineItem];
            for(NSDictionary *groupDict in lineItem.menuOptionNamesObj){
                NSArray *selectedOptions = [groupDict objectForKey:@"menu_option_names"];
                for(NSDictionary *optionDict in selectedOptions){
                    NSString *optionName = [optionDict objectForKey:@"option_name"]; //option_price
                    PCCurrency optionPrice = [optionDict currencyForKey:@"option_price"];
                    NSString *optionPriceString = nil;
                    if(optionPrice != 0){
                        optionPriceString = [[NSNumber numberWithCurrency:(optionPrice * lineItem.quantity)] currencyString];
                    } else {
                        optionPriceString = @"";
                    }
                    
                    NSDictionary *optionDict = @{@"option_name" : optionName,
                                                 @"option_unit_price" : [NSNumber numberWithCurrency:optionPrice],
                                                 @"option_price" : optionPriceString};
                    
                    [_allItems addObject:optionDict];
                }
            }
        }
    }
    return _allItems;
}

- (void)summation
{
    [self summationWithTaxCalc:NO merchant:nil];
}

- (void)summationWithTaxCalc:(BOOL)withTaxCalc merchant:(Merchant *)merchant
{
    if([self.lineItems count] > 0){
        PCCurrency sum = 0;
        PCCurrency sumTaxable = 0;
        for(LineItem *lineItem in self.lineItems){
            sum += lineItem.lineAmount;
            if(lineItem.taxable){
                sumTaxable += lineItem.lineAmount;
            }
        }
        
        self.subTotal = sum;
        self.subTotalTaxable = sumTaxable;
        
        if(withTaxCalc){
            self.taxes = roundf(((self.subTotalTaxable * merchant.taxRate) / 100.0f));
        } else {
            self.taxes = self.payment.taxAmount;
        }
        self.grandTotal = sum + self.taxes + self.payment.tipAmount + self.payment.deliveryFeeAmount + self.payment.roServiceFeeAmount - self.payment.discountAmount;
    }
}

- (NSString *)subTotalString
{
    return [[NSNumber numberWithCurrency:self.subTotal] currencyString];
}

- (NSString *)grandTotalString
{
    return [[NSNumber numberWithCurrency:self.grandTotal] currencyString];
}

- (NSString *)taxesString
{
    return [[NSNumber numberWithCurrency:self.taxes] currencyString];
}

- (NSString *)taxNroServiceFeeAmountString
{
    return [[NSNumber numberWithCurrency:(self.taxes + self.payment.roServiceFeeAmount)] currencyString];
}


#pragma mark - PCModelProtocol
- (id)initWithFMResult:(FMResultSet *)result;
{
    self = [super init];
    if(self != nil){
        self.receiptNo = [result longLongIntForColumn:@"receiptNo"];
        self.paymentNo = [result longLongIntForColumn:@"paymentNo"];
        self.orderNo = [result longLongIntForColumn:@"orderNo"];
        self.merchantNo = [result longLongIntForColumn:@"merchantNo"];
        self.customerNo = [result longLongIntForColumn:@"customerNo"];
        
        self.receiptType = [result intForColumn:@"receiptType"];
        //Merchant
        self.merchantName = [result stringForColumn:@"merchantName"];
        self.merchantDesc = [result stringForColumn:@"merchantDesc"];
        self.merchantPhone = [result stringForColumn:@"merchantPhone"];
        self.merchantImageURLString = [result stringForColumn:@"merchantImageURLString"];
        self.merchantLogoURLString = [result stringForColumn:@"merchantLogoURLString"];
        
        //Order
        self.orderDate = [result dateForColumn:@"orderDate"];
        self.expectedDate = [result dateForColumn:@"expectedDate"];
        self.confirmDate = [result dateForColumn:@"confirmDate"];
        self.staffName = [result stringForColumn:@"staffName"];

        self.orderType = [result intForColumn:@"orderType"];
        
        self.ticketNumber = [result stringForColumn:@"ticketNumber"];
        self.tableNumber = [result stringForColumn:@"tableNumber"];
    }
    return self;
}

- (BOOL)save
{
    if(self.isExisted){
        return [self update];
    } else {
        BOOL rtn = [self insert];
        if(rtn){
            self.rowId = [DB.db lastInsertRowId];
            
            [self closeDB];
            
            if([self.lineItems count] > 0){
                for(LineItem *lineItem in self.lineItems){
                    [lineItem save];
                }
            }
            
        } else {
            if(self.isTestMode){
                PCLog(@"Payment not saved because this payment is demo");
            } else {
                if(DB.db.lastErrorCode == 19){
                    rtn = [self update];
                    if(!rtn){
                        PCError(@"Receipt DB Update error : %d", DB.db.lastErrorCode);
                    }
                } else {
                    PCError(@"Receipt DB Insert error : %d", DB.db.lastErrorCode);
                }
            }
        }
        return rtn;
    }
}

- (BOOL)insert
{
    if(self.isTestMode){
        PCLog(@"Receipt not saved because this Receipt is demo");
        return NO;
    }
    
    if([self openDB]){
        
        return [DB.db executeUpdate:INSERT_QRY,
                [NSNumber numberWithLongLong:self.receiptNo],
                [NSNumber numberWithLongLong:self.paymentNo],
                [NSNumber numberWithLongLong:self.orderNo],
                [NSNumber numberWithLongLong:self.merchantNo],
                [NSNumber numberWithLongLong:self.customerNo],
                [NSNumber numberWithInteger:self.receiptType],
                self.merchantName,
                self.merchantDesc,
                self.merchantPhone,
                self.merchantImageURLString,
                self.merchantLogoURLString,
                self.orderDate,
                self.expectedDate,
                self.confirmDate,
                self.staffName,
                [NSNumber numberWithInteger:self.orderType],
                self.ticketNumber,
                self.tableNumber
                ];
    } else {
        PCError(@"Receipt insert error");
        return NO;
    }
    
}

- (BOOL)update
{

    if([self openDB]){
        return [DB.db executeUpdate:UPDATE_QRY,
                [NSNumber numberWithLongLong:self.paymentNo],
                [NSNumber numberWithLongLong:self.orderNo],
                [NSNumber numberWithLongLong:self.merchantNo],
                [NSNumber numberWithLongLong:self.customerNo],
                [NSNumber numberWithInteger:self.receiptType],
                self.merchantName,
                self.merchantDesc,
                self.merchantPhone,
                self.merchantImageURLString,
                self.merchantLogoURLString,
                self.orderDate,
                self.expectedDate,
                self.confirmDate,
                self.staffName,
                [NSNumber numberWithInteger:self.orderType],
                self.ticketNumber,
                self.tableNumber,
                [NSNumber numberWithLongLong:self.receiptNo]
                ];
        
    } else {
        PCError(@"Receipt update error");
        return NO;
    }

}

- (NSURL *)merchantImageURL
{
    return [NSURL URLWithString:self.merchantImageURLString];
}

- (NSURL *)merchantLogoURL
{
    return [NSURL URLWithString:self.merchantLogoURLString];
}

- (NSString *)typeString
{
    switch(self.orderType){
        case OrderTypeBill:
        case OrderTypeCart:
        case OrderTypeOrderOnly:
            return @"Dine-in";
            break;
        case OrderTypePickup:
            return @"Dine-in";
            break;
        case OrderTypeTakeout:
            return @"Take-out";
            break;
        case OrderTypeDelivery:
            return @"Delivery";
            break;
        default:
            return @"Unknown";
    }
}


+ (NSString *)orderByQuery
{
    return @"order by paymentNo desc";
}



@end
