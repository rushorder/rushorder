//
//  Payment.h
//  RushOrder
//
//  Created by Conan on 2/22/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCModel.h"
#import "MobileCard.h"
#import "Order.h"
#import "Promotion.h"
#import "LineItem.h"

typedef enum _paymentMethod{
    PaymentMethodUnknown = 0,
    PaymentMethodPayself,
    PaymentMethodPlasticCard,
    PaymentMethodCash
} PaymentMethod;

typedef enum _paymentStatus{
    PaymentStatusUnknown = 0,
    PaymentStatusSuccess,
    PaymentStatusRefunded
} PaymentStatus;

typedef enum _promotionErrorCode{
    PromotionErrorSuccess = 0,
    PromotionErrorInActive = 1,
    PromotionErrorNotStarted,
    PromotionErrorExpired,
    PromotionErrorFullUse,
    PromotionErrorLackOrderAmount,
    PromotionErrorOthers,
    PromotionErrorNotCombinable,
    PromotionErrorNotCombinableExists,
} PromotionErrorCode;

@interface Payment : PCModel <PCModelProtocol>

@property (nonatomic) PCSerial paymentNo;
@property (nonatomic) PCSerial orderNo;
@property (nonatomic) PCSerial merchantNo;
@property (nonatomic) PCSerial promotionNo;
@property (nonatomic) PCCurrency payAmount;
@property (nonatomic) PCCurrency creditAmount;
@property (nonatomic) PCCurrency tipAmount;
@property (nonatomic) PCCurrency taxAmount;
@property (nonatomic) PCCurrency rewardedAmount;
@property (nonatomic) PCCurrency discountAmountByRO;
@property (nonatomic) PCCurrency discountAmountByMerchant;
@property (nonatomic) PCCurrency pinPointBalance;
@property (nonatomic) PCCurrency deliveryFeeAmount;
@property (nonatomic) PCCurrency roServiceFeeAmount;
@property (nonatomic) PCSerial creditPolicyNo;
@property (copy, nonatomic) NSString *creditPolicyName;
@property (nonatomic) PCSerial globalCreditPolicyNo;
@property (copy, nonatomic) NSString *globalCreditPolicyName;
@property (copy, nonatomic) NSString *currency;
@property (copy, nonatomic) NSDate *payDate;
@property (copy, nonatomic) NSDate *refundedDate;
@property (copy, nonatomic) NSString *chargeId;

@property (copy, nonatomic) NSString *cardFingerPrint;
@property (copy, nonatomic) NSString *cardNumber;
@property (copy, nonatomic) NSString *cardType;
@property (nonatomic) NSInteger cardExpireMonth;
@property (nonatomic) NSInteger cardExpireYear;
@property (copy, nonatomic) NSString *cardCvc;
@property (copy, nonatomic) NSString *zipCode;

@property (nonatomic) PaymentStatus status;

@property (strong, nonatomic) Order *order;
@property (strong, nonatomic) Promotion *promotion;

@property (nonatomic, getter = isTestMode) BOOL testMode;

@property (nonatomic) PaymentMethod method;
@property (readonly) NSString *methodString;

// Local Temp
@property (nonatomic) PCCurrency orderAmount;
@property (nonatomic) PCCurrency actualAmount;

// readonly
@property (readonly) PCCurrency netAmount;
@property (readonly) UIImage *cardLogo;
@property (readonly) NSString *formattedCardNumber;

// Only for lineitems
@property (nonatomic) PCCurrency subTotal;
@property (nonatomic) PCCurrency subTotalTaxable;
@property (nonatomic) PCCurrency taxes;
@property (nonatomic) PCCurrency grandTotal;

@property (nonatomic, readonly) PCCurrency total;

@property (readonly) NSString *payAmountString;
@property (readonly) NSString *actualAmountString;
@property (readonly) NSString *netAmountString;
@property (readonly) NSString *creditAmountString;
@property (readonly) NSString *creditConvertedAmountString;
@property (readonly) NSString *creditConvertedAmountStringAsDiscount;
@property (readonly) NSString *creditConvertedAmountStringInParenthesis;
@property (readonly) NSString *rewardedAmountString;
@property (readonly) NSString *discountAmountString;
@property (readonly) NSString *pinPointBalanceString;
@property (readonly) NSString *tipAmountString;
@property (readonly) NSString *deliveryFeeAmountString;
@property (readonly) NSString *taxAmountString;
@property (readonly) NSString *roServiceFeeAmountString;
@property (readonly) NSString *taxNroServiceFeeAmountString;
@property (readonly) NSString *subTotalString;
@property (readonly) NSString *subTotalTaxableString;
@property (readonly) NSString *taxesString;
@property (readonly) NSString *grandTotalString;
@property (nonatomic) PgType pgType;

@property (readonly) PCCurrency discountAmount;

@property (readonly) PCCurrency payAmountWithoutCents;

@property (nonatomic) Issuer issuer;

@property (nonatomic) BOOL tipSetByUser;
@property (nonatomic, getter = isRoService) BOOL roService;

// Object graph
@property (strong, nonatomic) NSMutableArray *lineItems;
@property (strong, nonatomic) NSMutableArray *appliedPromotions;

+ (Payment *)paymentWithPaymentNo:(PCSerial)paymentNo;
+ (NSArray *)paymentWithOrderNo:(PCSerial)orderNo;

- (id)initWithDictionary:(NSDictionary *)dict;

- (void)updateCardInfo:(MobileCard *)card;
- (void)updatePayment:(NSDictionary *)dict;
- (void)summation;
- (void)summationWithTaxCalc:(BOOL)withTaxCalc merchant:(Merchant *)merchant;
- (NSString *)formattedCardNumberWithSecureString:(NSString *)secureString;

- (PromotionErrorCode)removePromotion:(Promotion *)promotion;
- (PromotionErrorCode)applyPromotion:(Promotion *)promotion;

- (NSArray *)appliedDiscountsDicts;
- (NSArray *)appliedDiscountsDictsWithSmallCutAmount:(PCCurrency)smallCutAmount;

- (void)applyRoServiceFee:(Merchant *)merchant;

@end
