//
//  TableInfo.m
//  RushOrder
//
//  Created by Conan on 3/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "TableInfo.h"

@implementation TableInfo

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    if(self != nil){
//        self.tableNo = [dict serialForKey:@"id"];
        self.tableNumber = [dict objectForKey:@"table_label"];
        self.merchantNo = [dict serialForKey:@"merchant_id"];
        [self configureTableStatus];
    }
    
    return self;
}

- (id)initWithModelDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    if(self != nil){
//        self.tableNo = [dict serialForKey:@"id"];
        self.tableNumber = [dict objectForKey:@"label"];
        self.merchantNo = [dict serialForKey:@"merchant_id"];
        [self configureTableStatus];
    }
    
    return self;
}

- (id)initWithTableLabel:(NSString *)tableLabel merchantNo:(PCSerial)merchantNo
{
    self = [super init];
    
    if(self != nil){
        self.tableNumber = tableLabel;
        self.merchantNo = merchantNo;
        [self configureTableStatus];
    }
    
    return self;
}

- (void)setOrder:(Order *)order
{
    _order = order;
    [self configureTableStatus];
}

- (void)setBillRequest:(BillRequest *)billRequest
{
    _billRequest = billRequest;
    [self configureTableStatus];
}

//- (void)configureTableStatus
//{
//    if(self.order != nil){
//        if(self.order.status == OrderStatusFixed || self.order.status == OrderStatusCanceled){
//            if(!self.isWrappingMode){
//                self.status = TableStatusEmpty;
//                self.order = nil;
//            } else {
//                self.status = TableStatusFixed;
//            }
//        } else {
//            if(self.order.payAmounts == 0){
//                self.status = TableStatusBillIssued;
//            } else if(self.order.netPayAmounts < self.order.total) {
//                self.status = TableStatusPartialPaid;
//            } else {
//                self.status = TableStatusCompleted;
//            }
//        }
//    } else {
//        if(self.billRequest != nil){
//            self.status = self.billRequest.active ? TableStatusBillReqeusted : TableStatusEmpty;
//        } else {
//            self.status = TableStatusEmpty;
//        }
//    }
//}

- (void)configureTableStatus
{
    self.bfStatus = self.status;
    
    if(self.order != nil){
        if(self.order.status == OrderStatusFixed || self.order.status == OrderStatusCanceled || self.order.status == OrderStatusDeclined || self.order.status == OrderStatusRefused){
            if(!self.isWrappingMode){
                self.order = nil;
            }
        }
    }
    
    if(self.cart.status == CartStatusOrdering){
        self.status = TableStatusOrdering;
    } else if(self.cart.status == CartStatusSubmitted){
        self.status = TableStatusOrderSubmitted;
    } else {
        if(self.order != nil){
            if(self.order.status == OrderStatusFixed || self.order.status == OrderStatusCanceled || self.order.status == OrderStatusDeclined || self.order.status == OrderStatusRefused){
                if(!self.isWrappingMode){
                    self.status = TableStatusEmpty;
                } else {
                    self.status = TableStatusFixed;
                }
            } else {
                if((self.order.netPayAmounts + self.order.discountedAmount) == 0){
                    self.status = TableStatusBillIssued;
                } else if((self.order.netPayAmounts + self.order.discountedAmount) < self.order.total) {
                    self.status = TableStatusPartialPaid;
                } else {
                    self.status = TableStatusCompleted;
                }
            }
        } else {
            if(self.billRequest != nil){
                self.status = self.billRequest.active ? TableStatusBillReqeusted : TableStatusEmpty;
            } else {
                self.status = TableStatusEmpty;
            }
        }
    }
}


- (NSString *)statusString
{
    switch(self.status){
        case TableStatusEmpty:
            return NSLocalizedString(@"Empty", nil);
            break;
        case TableStatusBillReqeusted:
            return NSLocalizedString(@"Bill Requested", nil);
            break;
        case TableStatusOrdering:
            return NSLocalizedString(@"Ordering", nil);
            break;
        case TableStatusBillIssued:
            return NSLocalizedString(@"Waiting Payment", nil);
            break;
        case TableStatusOrderSubmitted:
            return NSLocalizedString(@"New Order", nil);
            break;
        case TableStatusPartialPaid:
            return NSLocalizedString(@"Partially Paid ", nil);
            break;
        case TableStatusCompleted:
            return NSLocalizedString(@"Payment Completed", nil);
            break;
        case TableStatusFixed:
            return NSLocalizedString(@"Completed", nil);
            break;
        case TableStatusUnknown:
        default:
            return @"Unknown";
            break;
    }
}

- (UIColor *)statusColor
{
    switch(self.status){
        case TableStatusEmpty:
            return [UIColor colorWithWhite:0.0f alpha:0.35f];
            break;
        case TableStatusOrdering:
            return [UIColor colorWithR:226.0f G:83.0f B:91.0f];
            break;
        case TableStatusBillReqeusted:
        case TableStatusOrderSubmitted:
//            return [UIColor colorWithR:255 G:153 B:51];
            return [UIColor colorWithR:206.0f G:63.0f B:71.0f];
            break;
        case TableStatusBillIssued:
        case TableStatusPartialPaid:
//            return [UIColor colorWithR:235 G:78 B:32];
            return [UIColor colorWithR:240.0f G:147.0f B:0.0f];
            break;
        case TableStatusCompleted:
//            return [UIColor colorWithR:67 G:138 B:207];
            return [UIColor colorWithR:0.0f G:141.0f B:193.0f];
            break;
        case TableStatusFixed:
            return [UIColor darkGrayColor];
            break;
        case TableStatusUnknown:
        default:
            return [UIColor lightGrayColor];
            break;
    }
}

- (NSDate *)actionDate
{
    if(self.order != nil){
        return self.order.orderDate;
    }
    
    if(self.cart != nil){
        if(self.cart.placedDate != nil){
            return self.cart.placedDate;
        } else {
            return self.cart.createdDate;
        }
    }
    
    if(self.billRequest != nil){
        return self.billRequest.lastAccessedTime;
    }
    
    return nil;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ { \n\t table Label:%@ \n\t statusString:%@ \n\t order:%@ \n\t billRequest:%@\n}",
            [super description],
//            self.tableNo,
            self.tableNumber,
            self.statusString,
            self.order,
            self.billRequest];
}

- (NSComparisonResult)compareTimeWith:(TableInfo *)anotherTableInfo
{
//    if(self.tableNo > anotherTableInfo.tableNo){
//        return NSOrderedDescending;
//    } else if(self.tableNo < anotherTableInfo.tableNo){
//        return NSOrderedAscending;
//    } else {
//        return NSOrderedSame;
//    }
    return [self.tableNumber compare:anotherTableInfo.tableNumber
                             options:NSCaseInsensitiveSearch];
}

- (BOOL)isEqual:(TableInfo *)anotherTableInfo
{
    if([anotherTableInfo isKindOfClass:[TableInfo class]]){
        return [self.tableNumber isCaseInsensitiveEqual:anotherTableInfo.tableNumber];
    } else {
        if([anotherTableInfo isKindOfClass:[Order class]]){
            return [self.order isEqual:(Order *)anotherTableInfo];
        } else {
            return NO;
        }
    }
}



@end
