//
//  OperationHour.m
//  RushOrder
//
//  Created by Conan on 7/5/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "OperationHour.h"

@implementation OperationHour

- (NSComparisonResult)compare:(OperationHour *)anotherHour
{
    if(anotherHour == nil) return NSOrderedDescending;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];

    NSDateComponents *currentComponents = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekday
                                                      fromDate:[NSDate date]];
    NSInteger weekday = (currentComponents.weekday - 1);
    if(weekday == 0) weekday = 7;
    
    OperationHour *currentHour = [[OperationHour alloc] init];
    currentHour.eventTime = [[NSString stringWithFormat:@"%ld%02ld%02ld",
                              (long)weekday,
                              (long)currentComponents.hour,
                              (long)currentComponents.minute] integerValue];
    
    NSNumber *selfStartTimeNumber = [self.openSpanDict objectForKey:@"open_time"];
    NSNumber *anotherStartTimeNumber = [anotherHour.openSpanDict objectForKey:@"start_time"];
    
    
    if((currentHour.eventTime < [selfStartTimeNumber integerValue] && currentHour.eventTime < [anotherStartTimeNumber integerValue]) ||
       (currentHour.eventTime > [selfStartTimeNumber integerValue] && currentHour.eventTime > [anotherStartTimeNumber integerValue])){
        if([selfStartTimeNumber integerValue] < [anotherStartTimeNumber integerValue]){
            return NSOrderedAscending;
        } else if([selfStartTimeNumber integerValue] > [anotherStartTimeNumber integerValue]){
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    } else if(currentHour.eventTime < [selfStartTimeNumber integerValue]){
        return NSOrderedAscending;
    } else if(currentHour.eventTime < [anotherStartTimeNumber integerValue]){
        return NSOrderedDescending;
    } else {
        return NSOrderedSame;
    }
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@, Type : %d, Hour : %ld", [super description], self.operationEvent, (long)self.eventTime];
}
@end
