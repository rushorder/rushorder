//
//  MenuCategory.h
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCModel.h"

@interface MenuCategory : PCModel

@property (nonatomic) PCSerial categoryNo;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *desc;
@property (strong, nonatomic) NSMutableArray *subCategories; //Object hierachy


@property (strong, nonatomic) NSMutableArray *items; //sub categories + items at same level.

- (id)initWithDictionary:(NSDictionary *)dict;

@end