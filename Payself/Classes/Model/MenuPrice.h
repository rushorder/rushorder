//
//  MenuPrice.h
//  RushOrder
//
//  Created by Conan on 5/24/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCModel.h"

@interface MenuPrice : PCModel

@property (nonatomic) PCSerial menuPriceNo;
@property (copy, nonatomic) NSString *name;
@property (nonatomic) PCCurrency price;

@property (nonatomic, getter = isDefaultPrice) BOOL defaultPrice;

@property (nonatomic, readonly) NSString *priceString;
@property (nonatomic, getter = isSelected) BOOL selected;

- (id)initWithDictionary:(NSDictionary *)dict;
@end
