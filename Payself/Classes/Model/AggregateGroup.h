//
//  AggregateGroup.h
//  RushOrder
//
//  Created by Conan on 3/14/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AggregateGroup : NSObject

@property (copy, atomic) NSString *sectionTitle;
@property (strong, atomic) Order *sumOrder;
@property (strong, atomic) NSArray *list;
@end

