//
//  LineItem.m
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "LineItem.h"
#import "MenuOptionGroup.h"
#import "MenuOption.h"
#import "MenuCategory.h"
#import "MenuSubCategory.h"
#import "JSONKit.h"
#import "PCCredentialService.h"

#define INSERT_QRY @"insert into 'LineItem'(\
lineItemNo\
,menuItemNo\
,paymentNo\
,receiptNo\
,orderNo\
,cartNo\
,merchantNo\
,unitPrice\
,basePrice\
,quantity\
,lineAmount\
,menuName\
,menuChoiceName\
,menuOptionNames\
,menuChoiceId\
,optionIds\
,udid\
,taxable\
,specialInstruction) \
values \
(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

#define UPDATE_QRY @"update 'LineItem' set \
menuItemNo = ?\
,paymentNo = ?\
,receiptNo = ?\
,orderNo = ?\
,cartNo = ?\
,merchantNo = ?\
,unitPrice = ?\
,basePrice = ?\
,quantity = ?\
,lineAmount = ?\
,menuName = ?\
,menuChoiceName = ?\
,menuOptionNames = ?\
,menuChoiceId = ?\
,optionIds = ?\
,udid = ?\
,taxable = ?\
,specialInstruction = ?\
where lineItemNo = ?"

@implementation LineItem

+ (BOOL)isRetina
{
    return ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale >= 2.0))?1:0;
}

+ (NSString *)orderByQuery
{
    return @"order by rowid asc";
}

- (id)init
{
    self = [super init];
    if(self != nil){
        //Initialization
    }
    return self;
}

/* 
 {
 "cart_id" = 3;
 "created_at" = "2013-06-03T09:47:12Z";
 "customer_device_token" = "";
 id = 6;
 "line_item_amount" = 100;
 "menu_choice_id" = 1;
 "menu_id" = 2;
 "menu_option_ids" = "[]";
 price = 100;
 quantity = 1;
 "tax_amount" = 17;
 "updated_at" = "2013-06-03T09:47:12Z";
 }

 */

- (id)initWithDictionary:(NSDictionary *)dict
{
    return [self initWithDictionary:dict
                            menuPan:nil];
}

- (id)initWithDictionary:(NSDictionary *)dict menuPan:(NSArray *)menuPan
{
    self = [self init];
    if(self != nil){
        
        PCSerial menuNo = [dict serialForKey:@"menu_id"];
        
        if(menuNo <= 0){
            PCError(@"There is no menu id in dictionary of lineitem");
            return self;
        }
        
        self.lineItemNo = [dict serialForKey:@"id"];
        self.paymentNo = [dict serialForKey:@"payment_id"];
        self.orderNo = [dict serialForKey:@"order_id"];
        self.menuItemNo = [dict serialForKey:@"menu_id"];
        self.customerNo = [dict serialForKey:@"customer_id"];
        self.cartNo = [dict serialForKey:@"cart_id"];
        
        self.lineAmount = [dict currencyForKey:@"line_item_amount"];
        self.quantity = [dict integerForKey:@"quantity"];
        if(self.quantity > 0) self.unitPrice = self.lineAmount / self.quantity;
        self.basePrice = [dict currencyForKey:@"price"]; // Server price is menu choice's price , but here, price is including option prices
        
        self.menuName = [dict objectForKey:@"menu_name"];
        self.menuOptionNames = [dict objectForKey:@"menu_option_names"];
        self.menuChoiceName = [dict objectForKey:@"menu_choice_name"];
        
        self.deviceToken = [dict objectForKey:@"customer_device_token"];
        
        self.menuChoiceId = [dict serialForKey:@"menu_choice_id"];
        self.optionIds = [dict objectForKey:@"menu_option_ids"];
        self.udid = [dict objectForKey:@"uuid"];
        self.taxable = [dict boolForKey:@"taxable"];
        self.specialInstruction = [dict objectForKey:@"memo"];
        
        NSDictionary *imageDict = [[dict objectForKey:@"menu_image"] objectForKey:@"image"];
        
        if(imageDict != nil){
            NSString *imageURLString = [imageDict objectForKey:@"url"];
            //        NSString *imageURLStringWithoutExt = [imageURLString stringByDeletingPathExtension];
            NSString *imageURLThumb = nil;
            
            if([LineItem isRetina]){
                imageURLThumb = [[imageDict objectForKey:@"retina"] objectForKey:@"url"];
            } else {
                imageURLThumb = [[imageDict objectForKey:@"thumb"] objectForKey:@"url"];
            }
            
            if(imageURLString.length > 0){
                self.imageURL = [NSURL URLWithString:imageURLString];
            }
            
            if(imageURLThumb.length > 0){
                self.thumbnailURL = [NSURL URLWithString:imageURLThumb];
            }
        }
        
        [self linkMenuItemInMenuList:menuPan];
    }
    return self;
}

- (void)linkMenuItemInMenuPan:(NSArray *)menuPan
{
    self.menuItem = nil;
    self.menuValidStatus = MenuValidStatusValid;
    
    for(MenuCategory *category in menuPan){
        for(MenuSubCategory *subCategory in category.subCategories){
            for(MenuItem *menu in subCategory.items){
                if(menu.menuItemNo == self.menuItemNo){
                    self.menuItem = menu;
                    break;
                }
            }
            if(self.menuItem != nil) break;
        }
        if(self.menuItem != nil) break;
    }
    
    if(self.menuItem == nil){
        if(menuPan != nil){
            PCWarning(@"Cannot find menu item in menuPan menu id is %lld", self.menuItemNo);
        }
    } else {
        [self configureChoiceAndOptions];
    }
}

- (void)linkMenuItemInMenuList:(NSArray *)menuList
{
    self.menuItem = nil;
    self.substitutableMenuItem = nil;
    self.menuValidStatus = MenuValidStatusValid;
    
    for(MenuItem *menu in menuList){
//        PCLog(@"Name %@, self Name %@", menu.name, self.menuName);
        if(menu.menuItemNo == self.menuItemNo){
            self.menuItem = menu;
            if(menu.menuPan != nil && menu.menuPan.isActive){ // menuPan은 OrderType이 일치할 때만 할당 되므로...
                break;
            } else if(self.substitutableMenuItem != nil){
                break;
            }
        } else if([menu.name isEqualToString:self.menuName]){
            if(menu.menuPan != nil && menu.menuPan.isActive){
                self.substitutableMenuItem = menu;
                if(self.menuItem != nil){
                    break;
                }
            }
        }
    }
    
    self.menuValidStatus &= ~MenuValidStatusMenuRemoved;
    
    if(self.menuItem == nil){
        if(menuList != nil){
            PCWarning(@"Cannot find menu item in menuPan menu id is %lld", self.menuItemNo);
            self.menuValidStatus |= MenuValidStatusMenuRemoved;
        }
    } else {
        [self configureChoiceAndOptions];
    }
}

- (void)configureChoiceAndOptions
{
    if(self.menuChoiceId == 0){
        self.menuChoiceId = self.selectedPrice.menuPriceNo;
    }
    
    
    for(MenuPrice *price in self.menuItem.prices){
        if(price.menuPriceNo == self.menuChoiceId){
            self.selectedPrice = price;
            price.selected = YES;
//            break;
        } else {
            price.selected = NO;
        }
    }
    
    
    if(self.selectedPrice == nil){
        if([self.menuItem.prices count] > 0){
            MenuPrice *price = [self.menuItem.prices objectAtIndex:0];
            price.selected = YES;
        }
    }
    
    //menu_option_ids
    NSArray *optionIds = [self.optionIds objectFromJSONString];
    
    NSMutableArray *optionIdArray = nil;
    
    if(optionIds != nil){
        optionIdArray = [NSMutableArray arrayWithArray:optionIds];
        
        self.selectedOptions = [NSMutableArray array];
        self.selectedOptionGroups = [NSMutableArray array];
        
    } else {
        optionIdArray = [NSMutableArray array];
        for(MenuOption *option in self.selectedOptions){
            [optionIdArray addObject:[NSNumber numberWithSerial:option.menuOptionNo]];
        }
        if([optionIdArray count] == 0){
            optionIdArray = nil;
        }
    }
    
    
    
    // TODO: neeed to be refactoring for performance issue
    
//    for(NSNumber *optionId in optionIdArray){
//        BOOL found = NO;
//        for(MenuOptionGroup *group in self.menuItem.optionGroups){
//            for(MenuOption *option in group.options){
//                if(option.menuOptionNo == [optionId serialValue]){
//                    [self.selectedOptions addObject:option];
//                    found = YES;
//                    break;
//                }
//            }
//            if(found) break;
//        }
//    }
    
    for(MenuOptionGroup *group in self.menuItem.optionGroups){
        MenuOptionGroup *selectedGroup = [group copy];
        selectedGroup.options = [NSMutableArray array];
        [self.selectedOptionGroups addObject:selectedGroup];
        for(MenuOption *option in group.options){
            BOOL found = NO;
            NSInteger i = 0;
            for(i = 0 ; i < [optionIdArray count] ; i++){
                NSNumber *optionId = [optionIdArray objectAtIndex:i];
                if(option.menuOptionNo == [optionId serialValue]){
                    if(optionIds != nil){
                        [self.selectedOptions addObject:option];
                        [selectedGroup.options addObject:option];
                    }
                    option.selected = YES;
                    found = YES;
                    break;
                } else {
                    option.selected = NO;
                }
            }
            
            if(found){
                [optionIdArray removeObjectAtIndex:i];
            } else {
                option.selected = NO;
            }
            
            // To make all options selected NO, please run through the end
//            if([optionIdArray count] == 0){
//                break;
//            }
        }
        
        if(optionIds != nil){
            if([selectedGroup.options count] == 0){
                [self.selectedOptionGroups removeLastObject];
            }
        }

        // To make all options selected NO, please run through the end
//        if([optionIdArray count] == 0){
//            break;
//        }
    }
    
    if([optionIdArray count] > 0){
        PCError(@"There might be removed options in the option lists");
        self.menuValidStatus |= MenuValidStatusOptionChanged;
    } else {
        self.menuValidStatus &= ~MenuValidStatusOptionChanged;
    }
    
    // This should be conducted at the last stage of option configuring
    // Because option price is being calculated based on 'selected' flags
    // So all proper 'selected' flag should be set first
    if(self.unitPrice != self.menuItem.price){
        self.menuValidStatus |= MenuValidStatusPriceChanged;
    } else {
        self.menuValidStatus &= ~MenuValidStatusPriceChanged;
    }
}

- (void)updatePriceAndOptions
{
    if(self.menuItem == nil){
        PCWarning(@"Menu item is not defined. For -(void)updatePriceAndOptions, you should first set menuItem");
        return;
    }
    
    self.menuValidStatus = MenuValidStatusValid;
    self.selectedPrice = self.menuItem.selectedMenuPrice;
    self.selectedOptions = self.menuItem.selectedOptions;
    
    self.optionIds = nil;
    [self updatePriceAndOptionNames];
}

- (void)updatePriceAndOptionNames
{
    self.menuChoiceName = self.selectedPrice.name;
    
    if([self.selectedOptions count] > 0){
        NSMutableArray *menuOptionNames = [NSMutableArray array];
        for(MenuOptionGroup *optionGroup in self.menuItem.optionGroups){
            NSMutableDictionary *optionGroupDict = [NSMutableDictionary dictionary];
            NSMutableArray *selectedOptions = [NSMutableArray array];
            for(MenuOption *option in optionGroup.options){
                if(option.isSelected){
                    NSMutableDictionary *optionDict = [NSMutableDictionary dictionary];
                    [optionDict setObject:option.name ? option.name : @"" forKey:@"option_name"];
                    [optionDict setObject:[NSNumber numberWithCurrency:option.price] forKey:@"option_price"];
                    [selectedOptions addObject:optionDict];
                }
            }
            
            if([selectedOptions count] > 0){
                [optionGroupDict setObject:selectedOptions forKey:@"menu_option_names"];
                [optionGroupDict setObject:optionGroup.name ? optionGroup.name : @"" forKey:@"name"];
                [optionGroupDict setObject:optionGroup.optionTypeString ? optionGroup.optionTypeString : @"" forKey:@"menu_option_group_type"];
                [optionGroupDict setObject:[NSNumber numberWithSerial:optionGroup.optionGroupNo] forKey:@"id"];
                [menuOptionNames addObject:optionGroupDict];
            }
        }
        self.menuOptionNames = [menuOptionNames JSONString];
    } else {
        self.menuOptionNames = nil;
    }
}

- (void)applyLiveAmount
{
    self.lineAmount = self.liveAmount;
    self.unitPrice = self.menuItem.price;
    self.basePrice = self.menuItem.basePrice;
}

- (BOOL)isMine
{
    if(self.isLocal){
        return YES;
    } else if(self.udid == nil || [self.udid length] == 0){
        return YES;
    } else {
#if RUSHORDER_CUSTOMER
        if(CRED.isSignedIn){
            return (self.customerNo == CRED.customerInfo.customerNo);
        } else {
            return ([self.udid isEqualToString:CRED.udid] && self.customerNo == 0);
        }
#else
        return [self.udid isEqualToString:CRED.udid];
#endif
    }
}

- (PCCurrency)optionPrices
{
    PCCurrency sum = 0;
    for(MenuOption *option in self.selectedOptions){
        sum += option.price;
    }
    return sum;
}

- (NSString *)lineAmountString
{
    return [[NSNumber numberWithCurrency:self.lineAmount] currencyString];
}

- (NSString *)liveAmountString
{
    return [[NSNumber numberWithCurrency:self.liveAmount] currencyString];
}

//- (NSString *)totalAmountString
//{
//    return [[NSNumber numberWithCurrency:self.totalAmount] currencyString];
//}

- (NSString *)quantityString
{
    return [[[NSNumber numberWithCurrency:self.quantity] decimalString] stringByAppendingString:@"x"];
}

- (NSString *)displayMenuName
{
#if RUSHORDER_CUSTOMER
    return [self displayMenuNameWithPrice:NO];
#else
    return [self displayMenuNameWithPrice:YES];
#endif
}

- (NSString *)displayMenuNameWithPrice:(BOOL)withPrice
{
    NSString *menuName = nil;
    if(self.menuItem != nil){
        menuName = self.menuItem.name;
    } else {
        menuName = self.menuName;
    }
    
    if(self.quantity > 1){
        if([self.menuChoiceName length] > 0 && withPrice){
            return [menuName stringByAppendingFormat:@" %@(x%ld)",self.menuChoiceName, (long)self.quantity];
        } else {
            return [menuName stringByAppendingFormat:@" (x%ld)",(long)self.quantity];
        }
    } else {

        if([self.menuChoiceName length] > 0){
            return [menuName stringByAppendingFormat:@" %@",self.menuChoiceName];
        } else {
            return menuName;
        }
    }
}

- (NSDictionary *)dictRepWithDeviceToken:(NSString *)deviceToken
{
    NSMutableArray *optionsArray = [NSMutableArray array];
    
    for(MenuOption *option in self.selectedOptions){
        [optionsArray addObject:[NSNumber numberWithSerial:option.menuOptionNo]];
    }
    
    if(self.menuItem == nil){
        return [NSDictionary dictionaryWithObjectsAndKeys:
                [NSNumber numberWithSerial:self.menuItemNo], @"menu_id",
                [NSNumber numberWithCurrency:self.basePrice], @"price",
                [NSNumber numberWithInteger:self.quantity], @"quantity",
                deviceToken ? deviceToken : @"", @"customer_device_token",
                [NSNumber numberWithSerial:self.menuChoiceId], @"menu_choice_id",
                self.optionIds ? self.optionIds : @"", @"menu_option_ids",
                self.specialInstruction ? self.specialInstruction : @"", @"memo",  //don't url encoding - because caller do it
                [NSNumber numberWithCurrency:self.lineAmount], @"line_item_amount",
                nil];
    } else {
        return [NSDictionary dictionaryWithObjectsAndKeys:
                [NSNumber numberWithSerial:self.menuItemNo], @"menu_id",
                [NSNumber numberWithCurrency:self.basePrice], @"price",
                [NSNumber numberWithInteger:self.quantity], @"quantity",
                deviceToken ? deviceToken : @"", @"customer_device_token",
                [NSNumber numberWithSerial:self.selectedPrice.menuPriceNo], @"menu_choice_id",
                [optionsArray JSONString], @"menu_option_ids",
                self.specialInstruction ? self.specialInstruction : @"", @"memo",  //don't url encoding - because caller do it
                [NSNumber numberWithCurrency:self.lineAmount], @"line_item_amount",
                nil];
    }
}

- (NSDictionary *)updateDictRepWithDeviceToken:(NSString *)deviceToken
{
    NSMutableArray *optionsArray = [NSMutableArray array];
    
    if(self.menuItem == nil){
        return [NSDictionary dictionaryWithObjectsAndKeys:
                [NSNumber numberWithSerial:self.lineItemNo], @"id",
                [NSNumber numberWithSerial:self.menuItemNo], @"menu_id",
                [NSNumber numberWithCurrency:self.unitPrice], @"price",
                [NSNumber numberWithInteger:self.quantity], @"quantity",
                deviceToken ? deviceToken : @"", @"customer_device_token",
                [NSNumber numberWithSerial:self.menuChoiceId], @"menu_choice_id",
                self.optionIds ? self.optionIds : @"", @"menu_option_ids",
                self.specialInstruction ? self.specialInstruction : @"", @"memo", //don't url encoding - because caller do it
                [NSNumber numberWithCurrency:self.lineAmount], @"line_item_amount",
                nil];
    } else {
        
        for(MenuOption *option in self.selectedOptions){
            [optionsArray addObject:[NSNumber numberWithSerial:option.menuOptionNo]];
        }
        
        return [NSDictionary dictionaryWithObjectsAndKeys:
                [NSNumber numberWithSerial:self.lineItemNo], @"id",
                [NSNumber numberWithSerial:self.menuItemNo], @"menu_id",
                [NSNumber numberWithCurrency:self.menuItem.basePrice], @"price",
                [NSNumber numberWithInteger:self.quantity], @"quantity",
                deviceToken ? deviceToken : @"", @"customer_device_token",
                [NSNumber numberWithSerial:self.selectedPrice.menuPriceNo], @"menu_choice_id",
                [optionsArray JSONString], @"menu_option_ids",
                self.specialInstruction ? self.specialInstruction : @"", @"memo",  //don't url encoding - because caller do it
                [NSNumber numberWithCurrency:self.lineAmount], @"line_item_amount",
                nil];
    }
}


- (PCCurrency)liveAmount
{
    PCCurrency sum = 0;
    sum = self.selectedPrice.price;
    sum += [self optionPrices];
    
    sum *= self.quantity;
    
    return sum;
}

- (BOOL)isPaid
{
    return (self.paymentNo > 0);
}

#pragma mark - PCModelProtocol
- (id)initWithFMResult:(FMResultSet *)result
{
    self = [super init];
    if(self != nil){
        
        self.lineItemNo = [result longLongIntForColumn:@"lineItemNo"];
        self.menuItemNo = [result longLongIntForColumn:@"menuItemNo"];
        self.paymentNo = [result longLongIntForColumn:@"paymentNo"];
        self.receiptNo = [result longLongIntForColumn:@"receiptNo"];
        self.orderNo = [result longLongIntForColumn:@"orderNo"];
        self.cartNo = [result longLongIntForColumn:@"cartNo"];
        self.merchantNo = [result longLongIntForColumn:@"merchantNo"];
        self.unitPrice = [result intForColumn:@"unitPrice"];
        self.basePrice = [result intForColumn:@"basePrice"];
        self.quantity = [result intForColumn:@"quantity"];
        self.lineAmount = [result intForColumn:@"lineAmount"];
        self.menuName = [result stringForColumn:@"menuName"];
        self.menuChoiceName = [result stringForColumn:@"menuChoiceName"];
        self.menuOptionNames = [result stringForColumn:@"menuOptionNames"];
        self.menuChoiceId = [result longLongIntForColumn:@"menuChoiceId"];
        self.optionIds = [result stringForColumn:@"optionIds"];
        self.udid = [result stringForColumn:@"udid"];
        self.taxable = [result boolForColumn:@"taxable"];
        self.specialInstruction = [result stringForColumn:@"specialInstruction"];
        self.local = YES;
    }
    
    return self;
}

- (BOOL)insert
{
    if([self openDB]){
        
        NSMutableArray *optionsArray = [NSMutableArray array];
        
        for(MenuOption *option in self.selectedOptions){
            [optionsArray addObject:[NSNumber numberWithSerial:option.menuOptionNo]];
        }
        
        BOOL rtn = [DB.db executeUpdate:INSERT_QRY,
                    [NSNumber numberWithLongLong:self.lineItemNo],
                    [NSNumber numberWithLongLong:self.menuItemNo],
                    [NSNumber numberWithLongLong:self.paymentNo],
                    [NSNumber numberWithLongLong:self.receiptNo],
                    [NSNumber numberWithLongLong:self.orderNo],
                    [NSNumber numberWithLongLong:self.cartNo],
                    [NSNumber numberWithLongLong:self.merchantNo],
                    [NSNumber numberWithLongLong:self.unitPrice],
                    [NSNumber numberWithLongLong:self.basePrice],
                    [NSNumber numberWithLongLong:self.quantity],
                    [NSNumber numberWithCurrency:self.lineAmount],
                    self.menuName,
                    self.menuChoiceName,
                    self.menuOptionNames,
                    [NSNumber numberWithSerial:self.selectedPrice.menuPriceNo],
                    [optionsArray JSONString],
                    self.udid,
                    [NSNumber numberWithBool:self.taxable],
                    self.specialInstruction
                    ];
        
        self.rowId = [DB.db lastInsertRowId];
        
        return rtn;

    } else {
        PCError(@"LineItem insert error");
        return NO;
    }

    return [self closeDB];
}

- (BOOL)update
{
    if([self openDB]){
        
        NSMutableArray *optionsArray = [NSMutableArray array];
        
        for(MenuOption *option in self.selectedOptions){
            [optionsArray addObject:[NSNumber numberWithSerial:option.menuOptionNo]];
        }
        
        return [DB.db executeUpdate:UPDATE_QRY,
                [NSNumber numberWithLongLong:self.menuItemNo],
                [NSNumber numberWithLongLong:self.paymentNo],
                [NSNumber numberWithLongLong:self.receiptNo],
                [NSNumber numberWithLongLong:self.orderNo],
                [NSNumber numberWithLongLong:self.cartNo],
                [NSNumber numberWithLongLong:self.merchantNo],
                [NSNumber numberWithLongLong:self.unitPrice],
                [NSNumber numberWithLongLong:self.basePrice],
                [NSNumber numberWithLongLong:self.quantity],
                [NSNumber numberWithCurrency:self.lineAmount],
                self.menuName,
                self.menuChoiceName,
                self.menuOptionNames,
                [NSNumber numberWithSerial:self.selectedPrice.menuPriceNo],
                [optionsArray JSONString],
                self.specialInstruction,
                self.udid,
                [NSNumber numberWithBool:self.taxable],
                [NSNumber numberWithLongLong:self.lineItemNo]];

    } else {
        PCError(@"LineItem update error");
        return NO;
    }

    return [self closeDB];
}

- (NSString *)serializedOptionNames
{
    NSString *optionStrings = nil;
    
    if(self.quantity > 1){
        optionStrings = [[NSNumber numberWithCurrency:round(self.lineAmount / self.quantity)] currencyString];
    } else {
        optionStrings = @"";
    }
    
    if([self.menuChoiceName length] > 0){
        if([optionStrings length] > 0){
            optionStrings = [optionStrings stringByAppendingString:@", "];
        }
        optionStrings = [optionStrings stringByAppendingString:self.menuChoiceName];
    }
    
    if([self.menuOptionNames length] > 0){
        NSArray *array = [self.menuOptionNames objectFromJSONString];
        
        NSMutableString *optionNames = [NSMutableString string];
        
        for(NSDictionary *optionDict in array){
            NSArray *optionNamesArray = [optionDict objectForKey:@"menu_option_names"];
            for(NSDictionary *nameDict in optionNamesArray){
                if([optionNames length] > 0){
                    [optionNames appendString:@", "];
                }
                NSString *optionName = [nameDict objectForKey:@"option_name"];
                [optionNames appendString:optionName ? optionName : @""];
            }
        }
        
        if([optionNames length] > 0){
            if([optionStrings length] > 0){
                optionStrings = [optionStrings stringByAppendingString:@", "];
            }
            
            optionStrings = [optionStrings stringByAppendingString:optionNames];
        }
    }
    return optionStrings;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@{ \n\t no:%lld \n\t orderNo:%lld \n\t cartNo:%lld \n\t merchantNo:%lld \n\t menuItemNo:%lld \n\t menuName:%@ \n\t menuOptions:%@ \n\t menuChoice:%@ \n\t \n\t menu:%@ \n\t lineAmount:%ld \n\t quantity:%ld \n\t selected Price:%@ \n\t selected Options:%@ \n\t Taxable:%d \n\t}",
            [super description],
            self.lineItemNo,
            self.orderNo,
            self.cartNo,
            self.merchantNo,
            self.menuItemNo,
            self.menuName,
            self.menuOptionNames,
            self.menuChoiceName,
            self.menuItem,
            (long)self.lineAmount,
            (long)self.quantity,
            self.selectedPrice,
            self.selectedOptions,
            self.taxable];
}

- (void)setMenuItem:(MenuItem *)menuItem
{
    //Because there maybe not exist menuItem, but has menuItemNo.
    if(menuItem != nil){
        self.menuItemNo = menuItem.menuItemNo;
        self.menuName = menuItem.name;
        self.taxable = menuItem.taxable;
    } else {
        // !!!:Don't assign 0 to menuItemNo and menuName to nil
//        self.menuItemNo = 0;
//        self.menuName = nil;
    }
    
    _menuItem = menuItem;
}

- (NSArray *)menuOptionNamesObj
{
    if(_menuOptionNamesObj == nil){
        if([self.menuOptionNames length] > 0){
            self.menuOptionNamesObj = [self.menuOptionNames objectFromJSONString];
        }
    }
    return _menuOptionNamesObj;
}

@end
