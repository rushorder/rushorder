//
//  PCModel.m
//  RushOrder
//
//  Created by Conan on 2/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCModel.h"

@interface VersionUpdate : NSObject
@property (nonatomic) int version;
@property (copy, nonatomic) NSString *updateQuery;
@end

@implementation VersionUpdate
@end

#pragma mark - SQLLiteDatabase

@interface SQLLiteDatabase(){
    __strong FMDatabase *_db;
}
@end

@implementation SQLLiteDatabase : NSObject

+ (SQLLiteDatabase *)sharedDatabase
{
    static SQLLiteDatabase *_saredDataBase = nil;
    
    @synchronized(self){
        if(_saredDataBase == nil){
            _saredDataBase = [[SQLLiteDatabase alloc] init];
        }
    }
    
    return _saredDataBase;
}

- (id)init
{
    self = [super init];
    if(self != nil){
        [self resolveDbName];
    }
    return self;
}

- (void)testDriveModeChanged:(NSNotification *)aNoti
{
    [self resolveDbName];
}

- (void) resolveDbName
{
    self.dbName = DB_NAME;
    
    [_db close];
    _db = nil;
}

- (FMDatabase *)db
{
    if(_db == nil){
        NSURL *storeURL = [[APP applicationDocumentsDirectory] URLByAppendingPathComponent:self.dbName];
        
        BOOL needToCheckMigration = NO;
        
        NSFileManager *manager = [NSFileManager defaultManager];
        if([manager fileExistsAtPath:[storeURL path]]){
            needToCheckMigration = YES;
        } else {
            NSString *dbPath = [[NSBundle mainBundle] pathForResource:[DB_NAME stringByDeletingPathExtension]
                                                               ofType:[DB_NAME pathExtension]];
            NSError *error = nil;
            if([manager copyItemAtPath:dbPath
                                toPath:[storeURL path]
                                 error:&error]){
                PCLog(@"Databased copied successfully to %@", storeURL);
            } else {
                PCError(@"Error while copying DB from %@ to %@, error %@", dbPath, storeURL, error);
            }
        }
        
        NSString *dbPath = [storeURL path];
        _db = [FMDatabase databaseWithPath:dbPath];
        
        if(needToCheckMigration){
            if([self checkDatabase]){ //migrated
//                _db = [FMDatabase databaseWithPath:dbPath];
                PCLog(@"Migrated");
            }
        }
    }
    
    return _db;
}

- (BOOL)checkDatabase //return migrated
{
    int currentVersion = 0;
    BOOL didMigrated = NO;
    
    FMResultSet *result = nil;
    
    if([_db open]){
        
        BOOL isVersionTable = NO;
        result = [_db executeQuery:@"SELECT name FROM sqlite_master WHERE type='table' AND name='Version';"];
        
        while ([result next]) {
            NSString *name = [result stringForColumn:@"name"];
            if([name isEqualToString:@"Version"]){
                isVersionTable = YES;
                break;
            }
        }
        
        if(!isVersionTable){
            BOOL success = [_db executeUpdate:@"CREATE  TABLE Version (version INTEGER PRIMARY KEY  NOT NULL , update_query VARCHAR)"];
            if(!success){
                PCError(_db.lastErrorMessage);
            }
        }
        
        result = [_db executeQuery:@"select * from Version order by rowid desc"];
        
        
        while ([result next]) {
            currentVersion = [result intForColumn:@"version"];
            break;
        }

    } else {
        PCError(@"%@ - DB opening error in list all", NSStringFromClass([self class]));
    }
    
    if(![_db close]){
        PCError(@"%@ - DB closing error in list all", NSStringFromClass([self class]));
    }
    
    
    NSMutableArray *updateQueries = [NSMutableArray array];
    NSString *sourcePath = [[NSBundle mainBundle] pathForResource:[DB_NAME stringByDeletingPathExtension]
                                                           ofType:[DB_NAME pathExtension]];
    FMDatabase *newDB = [FMDatabase databaseWithPath:sourcePath];
    
    if([newDB open]){
        // !!!:Sort Should be desc because of performance issue, handed by reverseObjectEnumerator below
        result = [newDB executeQuery:@"select * from Version order by version desc"];
        
        
        while ([result next]) {
            int aVersion = [result intForColumn:@"version"];
            
            if(aVersion > currentVersion){
                VersionUpdate *aVersionUpdate = [[VersionUpdate alloc] init];
                aVersionUpdate.version = [result intForColumn:@"version"];
                aVersionUpdate.updateQuery = [result stringForColumn:@"update_query"];
                [updateQueries addObject:aVersionUpdate];
            }
        }
    } else {
        PCError(@"%@ - Source DB opening error in list all", NSStringFromClass([self class]));
    }
    
    if(![newDB close]){
        PCError(@"%@ - Source DB closing error in list all", NSStringFromClass([self class]));
    }
    
    if([updateQueries count] > 0){
        if([_db open]){
            
            for(VersionUpdate *aVersionUpdate in [updateQueries reverseObjectEnumerator]){
                if([_db executeUpdate:aVersionUpdate.updateQuery]){
                    [_db executeUpdateWithFormat:@"Insert into Version (version, update_query) values (%d, %@)",
                     aVersionUpdate.version,
                     aVersionUpdate.updateQuery];
                    didMigrated = YES;
                } else {
                    PCError(@"DB migration error SQL:%@",aVersionUpdate.updateQuery);
                }
            }
            
        } else {
            PCError(@"%@ - DB opening error in updating Queries", NSStringFromClass([self class]));
        }
        
        if(![_db close]){
            PCError(@"%@ - DB closing error in updating Queries", NSStringFromClass([self class]));
        }
    }
    
    return didMigrated;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end

@interface PCModel(){

}
@end

@implementation PCModel
+ (NSMutableArray *)listAll
{
    return [self listWithCondition:nil];
}

+ (NSMutableArray *)listWithCondition:(NSString *)condition
{
    NSMutableArray *listArray = nil;
    if([DB.db open]){
        
        NSMutableString *fmtQry = nil;
        
        if([self respondsToSelector:@selector(selectQuery)]){
            fmtQry = [NSMutableString stringWithString:[self selectQuery]];
        } else {
            fmtQry = [NSMutableString stringWithFormat:@"select rowid 'rowid', * from %@ "
                                   , NSStringFromClass(self)];
        }
        
        NSString *whereQry = nil;
        if(condition == nil){
            if([self respondsToSelector:@selector(whereQuery)]){
                whereQry = [@" " stringByAppendingString:[self whereQuery]];
            } else {
                whereQry = @"";
            }
        } else {
            whereQry = [@" " stringByAppendingString:condition];
        }
        
        if(whereQry != nil){
            [fmtQry appendString:whereQry];
        }
        
        
        NSString *orderByQry = nil;
        if([self respondsToSelector:@selector(orderByQuery)]){
            orderByQry = [@" " stringByAppendingString:[self orderByQuery]];
        } else {
            orderByQry = @" order by rowid desc";
        }
        
        if(orderByQry != nil){
            [fmtQry appendString:orderByQry];
        }
        
//        PCLog(@"Query: %@", fmtQry);
        FMResultSet *result = [DB.db executeQuery:fmtQry];
        
        listArray = [NSMutableArray array];
        
        while ([result next]) {
            PCModel *instance = [[self alloc] initWithFMResult:result];
            instance.rowId = [result intForColumn:@"rowid"];
            [listArray addObject:instance];
        }
        
    } else {
        PCError(@"%@ - DB opening error in list all", NSStringFromClass([self class]));
    }
    
    if(![DB.db close]){
        PCError(@"%@ - DB closing error in list all", NSStringFromClass([self class]));
    }
    
    return listArray;
}

+ (id)instanceWithCondition:(NSString *)condition
{
    NSArray *list = [self listWithCondition:condition];
    if([list count] > 0){
        return [list objectAtIndex:0];
    } else {
        return nil;
    }
}

+ (BOOL)deleteWithCondition:(NSString *)condition
{
    //DELETE FROM MobileCard where rowId=3
    NSString *fmtQry = [NSString stringWithFormat:@"delete from %@ where %@",
                        NSStringFromClass(self),
                        condition];
    
    if([DB.db open]){
        return [DB.db executeUpdate:fmtQry];
    } else {
        PCError(@"PCModel deleting error");
        return NO;
    }
}

- (id)init
{
    self = [super init];
    if(self != nil){
        self.rowId = NSNotFound;
    }
    return self;
}

- (BOOL)openDB
{
    if([DB.db open]){
        return YES;
    } else {
        PCError(@"%@ - Error has occurred while opening database", NSStringFromClass([self class]));
        return NO;
    }
}
- (BOOL)closeDB
{
    if([DB.db close]){
        return YES;
    } else {
        PCError(@"%@ - Error has occurred while opening database", NSStringFromClass([self class]));
        return NO;
    }
}

#pragma mark - PCModelProtocol

- (id)initWithFMResult:(FMResultSet *)result
{
    PCWarning(@"- (id)initWithFMResult:(FMResultSet *)result method should be overrided");
    return nil;
}

- (BOOL)save
{
    if(self.isExisted){
        return [self update];
    } else {
        BOOL rtn = [self insert];
        if(rtn){
            self.rowId = [DB.db lastInsertRowId];
            
             [self closeDB];
        } else {
            if(DB.db.lastErrorCode == 19){
                rtn = [self update];
                if(!rtn){
                    PCError(@"%@ - DB Update error : %d", NSStringFromClass([self class]), DB.db.lastErrorCode);
                }
            } else {
                PCError(@"%@ - DB Insert error : %d", NSStringFromClass([self class]), DB.db.lastErrorCode);
            }
        }
        return rtn;
    }
}

- (BOOL)delete
{
    //DELETE FROM MobileCard where rowId=3
    NSString *fmtQry = [NSString stringWithFormat:@"delete from %@ where rowid= ?"
                               , NSStringFromClass([self class])];
    
    if([self openDB]){
        if([DB.db executeUpdate:fmtQry,
            [NSNumber numberWithInteger:self.rowId]]){
            self.rowId = NSNotFound;
            return YES;
        } else {
            return NO;
        }
    } else {
        PCError(@"%@ - PCModel deleting error", NSStringFromClass([self class]));
        return NO;
    }
    
    return [self closeDB];
}

- (BOOL)insert
{
    PCWarning(@"- (BOOL)insert method should be overrided");
    return NO;
}

- (BOOL)update
{
    PCWarning(@"- (BOOL)update method should be overrided");
    return NO;
}

- (BOOL)isExisted
{
    return (self.rowId != NSNotFound);
}


@end
