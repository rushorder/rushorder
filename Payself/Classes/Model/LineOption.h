//
//  LineOption.h
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCModel.h"
#import "MenuOptionGroup.h"

@interface LineOption : PCModel

@property (strong, nonatomic) MenuOptionGroup *optionGroup;
@property (strong, nonatomic) NSMutableArray *chosenOptions;

@end