//
//  Cart.m
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "Cart.h"
#import "MenuOptionGroup.h"
#import "MenuOption.h"
#import "MenuManager.h"
#import "TableSectionItem.h"
#import "PCMenuService.h"
#import "PCCredentialService.h"
#import "CustomerInfo.h"

#if RUSHORDER_MERCHANT
#import "PCMerchantService.h"
#else
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#endif

#define INSERT_QRY @"insert into 'Cart'(\
cartNo\
,merchantNo\
,tableLabel\
,status\
,createdDateTime\
,cartType\
,lastAccessedTime\
,placedDate\
,createdDate\
,flagNumber\
,nonInteractive) \
values \
(?,?,?,?,?,?,?,?,?,?,?)"

#define UPDATE_QRY @"update 'Cart' set \
merchantNo = ?\
,tableLabel = ?\
,status = ?\
,cartType = ?\
,lastAccessedTime = ?\
,flagNumber = ?\
,nonInteractive = ?\
where rowid = ?"


NSString * const LineItemChangedNotification = @"LineItemChangedNotification";
NSString * const CartUnsolicitChangedNotification = @"CartUnsolicitChangedNotification";

@interface Cart()
@end

@implementation Cart

+ (NSString *)orderByQuery
{
    return @"order by rowid asc";
}

- (id)init
{
    self = [super init];
    if(self != nil){
        _lineItems = [NSMutableArray array];
        _status = CartStatusOrdering;
        _createdDate = [NSDate date];
    }
    return self;
}

- (id)initWithFavroiteOrder:(FavoriteOrder *)favoriteOrder menuPan:(NSArray *)menuPan
{
    self = [self init];
    if(self){
        self.merchantNo = favoriteOrder.merchantNo;
        self.merchant = favoriteOrder.merchant;
        self.status = CartStatusOrdering;
        self.deviceToken = favoriteOrder.deviceToken;
        self.udid = favoriteOrder.uuid;
        self.flagNumber = favoriteOrder.tableLabel;
        
        switch(favoriteOrder.orderType){
            case OrderTypePickup:
                if([favoriteOrder.tableLabel length] > 0){
                    self.cartType = CartTypePickupServiced;
                } else {
                    self.cartType = CartTypePickup;
                }
                break;
            case OrderTypeDelivery:
                self.cartType = CartTypeDelivery;
                break;
            case OrderTypeTakeout:
                self.cartType = CartTypeTakeout;
                break;
            default:
                self.cartType = CartTypeCart;
                break;
        }
    
        
        [self updateLineItems:favoriteOrder.lineItemsJSON menuPan:menuPan];

    }
    return self;
}

- (void)updateLineItems:(NSArray *)lineItemArray menuPan:(NSArray *)menuPan
{
    NSMutableString *lineItemNames = [NSMutableString string];
    
    if(lineItemArray != nil){
        NSMutableArray *lineItems = [NSMutableArray array];
        for(NSDictionary *lineItemDict in lineItemArray){
            LineItem *lineItem = [[LineItem alloc] initWithDictionary:lineItemDict menuPan:menuPan];
            lineItem.merchantNo = self.merchantNo;
            [lineItems addObject:lineItem];
            
            if([lineItemNames length] > 0){
                [lineItemNames appendString:@", "];
            }
            [lineItemNames appendString:lineItem.menuName];
            if(lineItem.quantity > 1){
                [lineItemNames appendFormat:@"(%ld)", (long)lineItem.quantity];
            }
            
        }
        self.lineItemNames = lineItemNames;
        self.lineItems = lineItems;
        
        [self isAllItemsInHour];
        [self summation];
    }
}

- (void)updateWithDictionary:(NSDictionary *)dict menuPan:(NSArray *)menuPan
{
    self.cartNo = [dict serialForKey:@"id"];
    self.merchantNo = [dict serialForKey:@"merchant_id"];
    
    if([[dict objectForKey:@"status"] isEqual:@"ordering"]){
        self.status = CartStatusOrdering;
    } else if([[dict objectForKey:@"status"] isEqual:@"submitted"]){
        self.status = CartStatusSubmitted;
    } else if([[dict objectForKey:@"status"] isEqual:@"fixed"]){
        self.status = CartStatusFixed;
    } else {
        self.status = CartStatusUnknown;
    }
    
    self.deviceToken = [dict objectForKey:@"customer_device_token"];
    self.udid = [dict objectForKey:@"uuid"];
    
    
    NSArray *lineItemArray = [dict objectForKey:@"line_items"];
    
    if(lineItemArray != nil){
        [self updateLineItems:lineItemArray menuPan:menuPan];
    } else {
        self.amount = [[dict objectForKey:@"sub_total_amount"] integerValue];
        self.taxAmount = [[dict objectForKey:@"tax_amount"] integerValue];
    }
    
    self.staffNo = [dict serialForKey:@"staff_id"];
    self.staffName = [dict objectForKey:@"staff_name"];
    
    // In Server - Only cart type exsit - assume the dictionary comes from server only
    self.cartType = CartTypeCart;
    
    self.createdDate = [dict dateForKey:@"created_at"];
    self.lastAccessedTime = [dict dateForKey:@"updated_at"];
    self.placedDate = [dict dateForKey:@"submitted_at"];
    
    NSArray *customers = [dict objectForKey:@"customer_info"];
    
    if(customers != nil){
        NSMutableArray *customerInfos = [NSMutableArray array];
        for(NSDictionary *customer in customers){
            CustomerInfo *customerInfo = [[CustomerInfo alloc] initWithDictionary:customer];
            [customerInfos addObject:customerInfo];
        }
        self.customers = customerInfos;
    }
}



- (void)updateMerchantWithDict:(NSDictionary *)merchantDict
{
    self.merchantName = [merchantDict objectForKey:@"name"];
}


- (void)addMenuItem:(MenuItem *)menuItem
{
    [self addMenuItem:menuItem
             quantity:0
              success:^(BOOL isOnlyLocal){
        
    } failure:^{
        
    }];
}

- (void)addMenuItem:(MenuItem *)menuItem
           quantity:(NSInteger)quantity
            success:(void (^)(BOOL isOnlyLocal))successBlock
            failure:(void (^)())failureBlock
{
    [self addMenuItem:menuItem
             quantity:quantity
  specialInstructions:nil
              success:successBlock
              failure:failureBlock];
}

- (void)addMenuItem:(MenuItem *)menuItem
           quantity:(NSInteger)quantity
specialInstructions:(NSString *)specialInstructions
            success:(void (^)(BOOL isOnlyLocal))successBlock
            failure:(void (^)())failureBlock
{
    if(self.status != CartStatusOrdering){
        failureBlock();
        return;
    }
    
    LineItem *lineItem = nil;
    
    BOOL existLineItem = NO;
    
    for(LineItem *obj in self.lineItems){
        if(obj.isMine || obj.isLocal || self.isMine){
            if(obj.menuItemNo == menuItem.menuItemNo){
                if(obj.menuItem == nil){
                    obj.menuItem = menuItem;
                    [obj configureChoiceAndOptions];
                }
                
                // Don't use menuChoiceId of LineItem -> that value si from the server. If local data it cause serious problem
                if(obj.selectedPrice.menuPriceNo == menuItem.selectedMenuPrice.menuPriceNo){
                    NSComparator optionComparator = ^NSComparisonResult(MenuOption *obj1, MenuOption *obj2){
                        if(obj1.menuOptionNo > obj2.menuOptionNo){
                            return NSOrderedDescending;
                        } else if(obj1.menuOptionNo < obj2.menuOptionNo){
                            return NSOrderedAscending;
                        } else {
                            return NSOrderedSame;
                        }
                    };
                    
                    NSArray *newSelectedOptions = [menuItem.selectedOptions sortedArrayUsingComparator:optionComparator];
                    NSArray *selectedOptions = [obj.selectedOptions sortedArrayUsingComparator:optionComparator];
                    
                    BOOL matchedOption = YES;
                    
                    if([newSelectedOptions count] == [selectedOptions count]){
                        NSInteger i = 0;
                        for( i = 0 ; i < [newSelectedOptions count] ; i++){
                            MenuOption *newOption = [newSelectedOptions objectAtIndex:i];
                            MenuOption *oldOption = [selectedOptions objectAtIndex:i];
                            
                            if(newOption.menuOptionNo != oldOption.menuOptionNo){
                                matchedOption = NO;
                                break;
                            }
                        }
                    } else {
                        matchedOption = NO;
                    }
             
                    
                    if(matchedOption){
                        if((obj.specialInstruction == nil && specialInstructions == nil)
                           || [obj.specialInstruction isEqualToString:specialInstructions]){
                            lineItem = obj;
                            existLineItem = YES;
                            break;
                        }
                    }
                } else {
                    
                }
            } else {
            }
        } else {
        }
    }
    
    if(!existLineItem){
        lineItem = [[LineItem alloc] init];
        lineItem.deviceToken = [NSUserDefaults standardUserDefaults].deviceToken;
        lineItem.udid = CRED.udid;
        lineItem.quantity = 0;
    }
    
    if(specialInstructions != nil){
//        if([lineItem.specialInstruction length] > 0){
//            lineItem.specialInstruction =
//            [lineItem.specialInstruction stringByAppendingString:
//             [@"\n---------------------------\n" stringByAppendingString:specialInstructions]];
//        } else {
            lineItem.specialInstruction = specialInstructions;
//        }
    }
    
    lineItem.menuItem = menuItem;
    
    [lineItem updatePriceAndOptions];
    
    if(quantity == 0)
        lineItem.quantity += 1;
    else
        lineItem.quantity += quantity;
    
    [lineItem applyLiveAmount];
    
    if(!existLineItem){
        if(self.shouldBeRemoteCart){
            RequestResult result = RRFail;
            
            CompletionBlock addCompletionBlock = ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                    switch(response.errorCode){
                        case ResponseSuccess:{
                            
                            NSDictionary *cartDict = [response data];
                            
                            if([cartDict objectForKey:@"id"] != nil){
                                [self updateWithDictionary:cartDict menuPan:MENUPAN.wholeSerialMenuList];
                            }
                            
                            [self summation];
                            
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:LineItemChangedNotification
                                                                                object:self
                                                                              userInfo:nil];
                            successBlock(NO);
                            break;
                        }
                        case ResponseErrorCartUnchangable:
                            [UIAlertView alertWithTitle:NSLocalizedString(@"Order list is not editable", nil)
                                                message:NSLocalizedString(@"Order list has been placed. To add/modify/remove orders, you have to postpone the confirmation of your order by \"Change Placed Order\" button.", nil)
                                               delegate:self
                                                    tag:100];
                            [[NSNotificationCenter defaultCenter] postNotificationName:CartUnsolicitChangedNotification
                                                                                object:self
                                                                              userInfo:nil];
                            failureBlock();
                            break;
                        case ResponseErrorGeneral:{
                            failureBlock();
                            NSString *message = [response objectForKey:@"message"];
                            if([message isKindOfClass:[NSString class]]){
                                [UIAlertView alert:message];
                            }
                            break;
                        }
                        default:
                        {
                            failureBlock();
                            NSString *message = [response objectForKey:@"message"];
                            if([message isKindOfClass:[NSString class]]){
                                [UIAlertView alert:message];
                            } else {
                                [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while adding menu itmes to order list.", nil)];
                            }
                        }
                            break;
                    }
                } else {
                    failureBlock();
                    if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                        [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                    } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                        [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d.", nil)
                                            ,statusCode]];
                    }
                }
                [SVProgressHUD dismiss];
            };
            
#if RUSHORDER_CUSTOMER
            result = [MENU requestAddItems:[NSArray arrayWithObject:lineItem]
                                merchantNo:self.merchantNo
                                tableLabel:self.tableLabel
                               deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                                 authToken:CRED.customerInfo.authToken
                           completionBlock:addCompletionBlock];
            
            
#else
            result = [MERCHANT requestPOSAddItems:[NSArray arrayWithObject:lineItem]
                                       tableLabel:self.tableLabel
                                  completionBlock:addCompletionBlock];
#endif
            
            switch(result){
                case RRSuccess:
                    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                    break;
                case RRParameterError:
                    failureBlock();
                    [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                    break;
                default:
                    break;
            }
            
        } else {
            //Pickup, Takeout, Delivery
            lineItem.local = YES;
            [self.lineItems addObject:lineItem];
            
            self.lineItemsSection = nil;
            
            [self summation];
            
            [self saveContext];
#if RUSHORDER_CUSTOMER
#if FBAPPEVENT_ENABLED
            [FBSDKAppEvents logEvent:FBSDKAppEventNameAddedToCart
                          valueToSum:(lineItem.lineAmount / 100.0f)
                          parameters:@{ FBSDKAppEventParameterNameCurrency    : @"USD",
                                        FBSDKAppEventParameterNameContentType : (lineItem.menuName ? lineItem.menuName : @"Unknown"),
                                        FBSDKAppEventParameterNameContentID   : [NSNumber numberWithSerial:lineItem.menuItemNo]}];
#endif
#endif
            
            [[NSNotificationCenter defaultCenter] postNotificationName:LineItemChangedNotification
                                                                object:self
                                                              userInfo:nil];
            successBlock(YES);
        }
    } else {
        if(self.shouldBeRemoteCart){
            [self updateLineItem:lineItem
                         success:successBlock
                         failure:failureBlock];
        } else {
            
            //Pickup, Takeout, Delivery
            lineItem.local = YES;
            [self summation];
            
            [self saveContext];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:LineItemChangedNotification
                                                                object:self
                                                              userInfo:nil];
            
            successBlock(YES);
        }
        
    }
}

- (BOOL)shouldBeRemoteCart
{
    return ((self.cartType == CartTypeUnknown && self.merchant.isTableBase)
            || self.cartType == CartTypeCart);
}

- (void)saveContext
{
#if RUSHORDER_CUSTOMER
    [self save];
    [self.merchant save];
#endif
}

- (NSString *)typeString
{
    return [Cart typeStringForCartType:self.cartType];
}

+ (NSString *)typeStringForCartType:(CartType)cartType
{
    switch(cartType){
        case CartTypePickup:
        case CartTypeCart:
        case CartTypePickupServiced:
            return @"Dine-in";
            break;
        case CartTypeTakeout:
            return @"Take-out";
            break;
        case CartTypeDelivery:
            return @"Delivery";
            break;
        default:
            return @"Unknown";
    }
}

- (UIImage *)cartTypeIcon
{
    switch(self.cartType){
        case CartTypePickup:
        case CartTypeCart:
        case CartTypePickupServiced:
            return [UIImage imageNamed:@"button_icon_dinein"];
            break;
        case CartTypeTakeout:
            return [UIImage imageNamed:@"button_icon_takeout"];
            break;
        case CartTypeDelivery:
            return [UIImage imageNamed:@"button_icon_delivery"];
            break;
        default:
            return nil;
    }
}

- (void)updateLineItem:(LineItem *)lineItem
{
    [self updateLineItem:lineItem
                 success:^(BOOL isOnlyLocal){
                     
                 } failure:^{
                     
                 }];
}

- (void)updateLineItem:(LineItem *)lineItem
               success:(void (^)(BOOL isOnlyLocal))successBlock
               failure:(void (^)())failureBlock
{
    
    [self updateLineItem:lineItem
  overidePriceAndOptions:YES
                 success:successBlock
                 failure:failureBlock];
}

- (void)updateLineItem:(LineItem *)lineItem
overidePriceAndOptions:(BOOL)overidePriceAndOptions
               success:(void (^)(BOOL isOnlyLocal))successBlock
               failure:(void (^)())failureBlock
{
    [lineItem applyLiveAmount];
    
    if(overidePriceAndOptions){
        [lineItem updatePriceAndOptions];
    }
    
    if(self.shouldBeRemoteCart){
        RequestResult result = RRFail;
        
        CompletionBlock updateCompletionBlock = ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
            if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                switch(response.errorCode){
                    case ResponseSuccess:{
                        
                        
                        NSDictionary *cartDict = [response data];
                        
                        if([cartDict objectForKey:@"id"] != nil){
                            [self updateWithDictionary:cartDict menuPan:MENUPAN.wholeSerialMenuList];
                        } else {
                            self.cartNo = 0;
                            self.status = CartStatusOrdering;
                            self.lineItems = nil;
                            self.lineItemsSection = nil;
                        }
                        
                        [self summationWithForce:YES];
                        [[NSNotificationCenter defaultCenter] postNotificationName:LineItemChangedNotification
                                                                            object:self
                                                                          userInfo:nil];
                        successBlock(NO);
                    }
                        break;
                    case ResponseErrorNoLineItem:
                        [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Cannot find the item to modify. Line item number is %d.", nil), lineItem.lineItemNo]];
                        failureBlock();
                        break;
                    case ResponseErrorCartUnchangable:
                        [UIAlertView alertWithTitle:NSLocalizedString(@"Order list is not editable", nil)
                                            message:NSLocalizedString(@"Order list has been placed. To add/modify/remove orders, you have to postpone the confirmation of your order by \"Change Placed Order\" button.", nil)
                                           delegate:self
                                                tag:100];
                        [[NSNotificationCenter defaultCenter] postNotificationName:CartUnsolicitChangedNotification
                                                                            object:self
                                                                          userInfo:nil];
                        failureBlock();
                        break;
                    case ResponseErrorGeneral:{
                        failureBlock();
                        NSString *message = [response objectForKey:@"message"];
                        if([message isKindOfClass:[NSString class]]){
                            [UIAlertView alert:message];
                        }
                        break;
                    }
                    default:{
                        failureBlock();
                        NSString *message = [response objectForKey:@"message"];
                        if([message isKindOfClass:[NSString class]]){
                            [UIAlertView alert:message];
                        } else {
                            [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while updating line itmes from the cart.", nil)];
                        }
                    }
                        break;
                }
            } else {
                failureBlock();
                if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                    [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                    [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d.", nil)
                                        ,statusCode]];
                }
            }
            [SVProgressHUD dismiss];
        };
        
#if RUSHORDER_CUSTOMER
        result = [MENU requestUpdateItems:self
                                lineItems:[NSArray arrayWithObject:lineItem]
                              deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                          completionBlock:updateCompletionBlock];
#else
        result = [MERCHANT requestPOSUpdateItems:self
                                       lineItems:[NSArray arrayWithObject:lineItem]
                                 completionBlock:updateCompletionBlock];
#endif
        
        switch(result){
            case RRSuccess:
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                break;
            case RRParameterError:
                failureBlock();
                [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                break;
            default:
                break;
        }
    } else {
        //Pickup
        [self saveContext];
        [self summation];
#if RUSHORDER_CUSTOMER
        [FBSDKAppEvents logEvent:FBSDKAppEventNameAddedToCart
                   valueToSum:(lineItem.lineAmount / 100.0f)
                   parameters:@{ FBSDKAppEventParameterNameCurrency    : @"USD",
                                 FBSDKAppEventParameterNameContentType : @"food",
                                 FBSDKAppEventParameterNameDescription : (lineItem.menuName ? lineItem.menuName : @"Unknown"),
                                 FBSDKAppEventParameterNameContentID   : [NSNumber numberWithSerial:lineItem.menuItemNo],
                                 @"UUID" : (CRED.udid ? CRED.udid : @"Unknown")}];
#endif
        
        [[NSNotificationCenter defaultCenter] postNotificationName:LineItemChangedNotification
                                                            object:self
                                                          userInfo:nil];
        successBlock(YES);
    }
}


- (void)removeLineItem:(LineItem *)lineItem
               success:(void (^)(BOOL isOnlyLocal))successBlock
               failure:(void (^)())failureBlock
{
    [self subtractLineItem:lineItem
                        by:lineItem.quantity
                   success:successBlock
                   failure:failureBlock];
}

- (void)subtractLineItem:(LineItem *)lineItem
                 success:(void (^)(BOOL isOnlyLocal))successBlock
                 failure:(void (^)())failureBlock
{
    [self subtractLineItem:lineItem
                        by:1
                   success:successBlock
                   failure:failureBlock];
}

- (void)subtractLineItem:(LineItem *)lineItem
                      by:(NSInteger)quantity
                 success:(void (^)(BOOL isOnlyLocal))successBlock
                 failure:(void (^)())failureBlock
{
    lineItem.quantity -= quantity;
    
    lineItem.quantity = MAX(lineItem.quantity, 0);
    
    if(lineItem.quantity == 0){
        
        if(self.shouldBeRemoteCart){
            RequestResult result = RRFail;
            
            CompletionBlock deleteCompletionBlock = ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                
                if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                    switch(response.errorCode){
                        case ResponseSuccess:{
                            NSDictionary *cartDict = [response data];
                            
                            if([cartDict objectForKey:@"id"] != nil){
                                [self updateWithDictionary:cartDict menuPan:MENUPAN.wholeSerialMenuList];
                            } else {
                                self.cartNo = 0;
                                self.status = CartStatusOrdering;
                                self.lineItems = nil;
                                self.lineItemsSection = nil;
                            }
                            
                            [self summationWithForce:YES];
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:LineItemChangedNotification
                                                                                object:self
                                                                              userInfo:nil];
                            successBlock(NO);
                        }
                            break;
                        case ResponseErrorNoCart:{
                            [UIAlertView alert:NSLocalizedString(@"This order list does not exist or has been removed.", nil)];
                            self.lineItems = nil;
                            
                            
                        }
                            break;
                        case ResponseErrorNoLineItem:
                            [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Cannot find the item to remove. Line item number is %d", nil), lineItem.lineItemNo]];
                            failureBlock();
                            break;
                        case ResponseErrorCartUnchangable:
                            [UIAlertView alertWithTitle:NSLocalizedString(@"Order list is not editable", nil)
                                                message:NSLocalizedString(@"Order list has been placed. To add/modify/remove orders, you have to postpone the confirmation of your order by \"Change Placed Order\" button", nil)
                                               delegate:self
                                                    tag:100];
                            [[NSNotificationCenter defaultCenter] postNotificationName:CartUnsolicitChangedNotification
                                                                                object:self
                                                                              userInfo:nil];
                            failureBlock();
                            break;
                        case ResponseErrorGeneral:{
                            failureBlock();
                            NSString *message = [response objectForKey:@"message"];
                            if([message isKindOfClass:[NSString class]]){
                                [UIAlertView alert:message];
                            }
                            break;
                        }
                        default:{
                            failureBlock();
                            NSString *message = [response objectForKey:@"message"];
                            if([message isKindOfClass:[NSString class]]){
                                [UIAlertView alert:message];
                            } else {
                                [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while deleting line itmes from the cart", nil)];
                            }
                        }
                            break;
                    }
                } else {
                    failureBlock();
                    if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                        [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                    } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                        [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                            ,statusCode]];
                    }
                }
                [SVProgressHUD dismiss];
            };
            
#if RUSHORDER_CUSTOMER
            result = [MENU requestDeleteItems:self
                                    lineItems:[NSArray arrayWithObject:lineItem]
                                  deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                              completionBlock:deleteCompletionBlock];
#else
            result = [MERCHANT requestPOSDeleteItems:self
                                           lineItems:[NSArray arrayWithObject:lineItem]
                                     completionBlock:deleteCompletionBlock];
#endif
            
            switch(result){
                case RRSuccess:
                    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                    break;
                case RRParameterError:
                    failureBlock();
                    [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                    break;
                default:
                    break;
            }
        } else {
            //Pickup
            [self.lineItems removeObject:lineItem];
            self.lineItemsSection = nil;
            [self saveContext];
            [self summation];
            [[NSNotificationCenter defaultCenter] postNotificationName:LineItemChangedNotification
                                                                object:self
                                                              userInfo:nil];
            successBlock(YES);
        }
        
    } else {
        
        if(lineItem.menuItem == nil){
            [lineItem linkMenuItemInMenuList:MENUPAN.wholeSerialMenuList];
        }
        
        [lineItem applyLiveAmount];
        
        if(self.shouldBeRemoteCart){
            
            [self updateLineItem:lineItem
          overidePriceAndOptions:NO
                         success:successBlock
                         failure:failureBlock];
        } else {
            //Pickup
            [self saveContext];
            [self summation];
            [[NSNotificationCenter defaultCenter] postNotificationName:LineItemChangedNotification
                                                                object:self
                                                              userInfo:nil];
            successBlock(YES);
        }
    }
}

- (void)removeAllFromCartSuccess:(void (^)(BOOL isOnlyLocal))successBlock
                         failure:(void (^)())failureBlock
{
    if(self.shouldBeRemoteCart){
        NSMutableArray *myOrderList = [NSMutableArray array];
        
        for(LineItem *lineItem in self.lineItems){
            if(lineItem.isMine){
                [myOrderList addObject:lineItem];
            }
        }
        
        if([myOrderList count] == 0){
            failureBlock();
            return;
        }
        
        RequestResult result = RRFail;
        
        CompletionBlock deleteCompletionBlock = ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
            
            if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                switch(response.errorCode){
                    case ResponseSuccess:{
                        NSDictionary *cartDict = [response data];
                        
                        if([cartDict objectForKey:@"id"] != nil){
                            [self updateWithDictionary:cartDict menuPan:MENUPAN.wholeSerialMenuList];
                        } else {
                            self.cartNo = 0;
                            self.status = CartStatusOrdering;
                            self.lineItems = nil;
                            self.lineItemsSection = nil;
                        }
                        
                        [self summationWithForce:YES];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:LineItemChangedNotification
                                                                            object:self
                                                                          userInfo:nil];
                        successBlock(NO);
                    }
                        break;
                    case ResponseErrorNoCart:{
                        [UIAlertView alert:NSLocalizedString(@"This order list does not exist or has been removed", nil)];
                        self.lineItems = nil;
                        
                        
                    }
                        break;
                    case ResponseErrorNoLineItem:
                        [UIAlertView alert:NSLocalizedString(@"Cannot find the items to remove", nil)];
                        failureBlock();
                        break;
                    case ResponseErrorCartUnchangable:
                        [UIAlertView alertWithTitle:NSLocalizedString(@"Order list is not editable", nil)
                                            message:NSLocalizedString(@"Order list has been placed. To add/modify/remove order, you have to postpone the confirmation of your order by \"Change Placed Order\" button", nil)
                                           delegate:self
                                                tag:100];
                        [[NSNotificationCenter defaultCenter] postNotificationName:CartUnsolicitChangedNotification
                                                                            object:self
                                                                          userInfo:nil];
                        failureBlock();
                        break;
                    case ResponseErrorGeneral:{
                        failureBlock();
                        NSString *message = [response objectForKey:@"message"];
                        if([message isKindOfClass:[NSString class]]){
                            [UIAlertView alert:message];
                        }
                        break;
                    }
                    default:{
                        failureBlock();
                        NSString *message = [response objectForKey:@"message"];
                        if([message isKindOfClass:[NSString class]]){
                            [UIAlertView alert:message];
                        } else {
                            [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while deleting line itmes from the cart", nil)];
                        }
                    }
                        break;
                }
            } else {
                failureBlock();
                if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                    [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                    [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                        ,statusCode]];
                }
            }
            [SVProgressHUD dismiss];
        };
        
#if RUSHORDER_CUSTOMER
        result = [MENU requestDeleteItems:self
                                lineItems:myOrderList
                              deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                          completionBlock:deleteCompletionBlock];
#else
        result = [MERCHANT requestPOSDeleteItems:self
                                       lineItems:myOrderList
                                 completionBlock:deleteCompletionBlock];
#endif
        
        switch(result){
            case RRSuccess:
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                break;
            case RRParameterError:
                failureBlock();
                [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                break;
            default:
                break;
        }
        
    }
}

- (void)updateStatus:(CartStatus)cartStatus
             success:(void (^)(BOOL isOnlyLocal))successBlock
             failure:(void (^)())failureBlock
{
    if(cartStatus == CartStatusSubmitted && self.status != CartStatusOrdering){
        PCError(@"Placing order should be status in CartStatusOrdering");
        return;
    } else if(cartStatus == CartStatusOrdering && self.status != CartStatusSubmitted){
        PCError(@"Placing order should be status in CartStatusOrdering");
        return;
    }
    
    self.status = cartStatus;
    
    
    NSString *deviceToken = nil;
    
#if RUSHORDER_MERCHANT
    
#else
    deviceToken = [NSUserDefaults standardUserDefaults].deviceToken;
#endif
    
    RequestResult result = RRFail;
    result = [MENU requestUpdateCart:self
                         deviceToken:deviceToken
                     completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              NSDictionary *cartDict = [response data];
                              
                              if([cartDict objectForKey:@"id"] != nil){
                                  [self updateWithDictionary:cartDict menuPan:MENUPAN.wholeSerialMenuList];
                              }
                              
                              [self summation];
                              [[NSNotificationCenter defaultCenter] postNotificationName:LineItemChangedNotification
                                                                                  object:self
                                                                                userInfo:nil];
                              successBlock(NO);
                              
                              break;
                          }
                          case ResponseErrorNoCart:{
                              failureBlock();
                              [UIAlertView alertWithTitle:NSLocalizedString(@"This order list removed", nil)
                                                  message:NSLocalizedString(@"Your order list may be removed by restaurant at any reason. Ask restaurant for the reason.", nil)];
                              [[NSNotificationCenter defaultCenter] postNotificationName:CartUnsolicitChangedNotification
                                                                                  object:self
                                                                                userInfo:nil];
                              break;
                          }
                          case ResponseErrorCartFixed:{
                              failureBlock();
                              [[NSNotificationCenter defaultCenter] postNotificationName:CartUnsolicitChangedNotification
                                                                                  object:self
                                                                                userInfo:nil];
                              break;
                          }
                          case ResponseErrorGeneral:{
                              failureBlock();
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:{
                              failureBlock();
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while update order list status", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      failureBlock();
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            failureBlock();
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

#if RUSHORDER_MERCHANT
- (void)updateOrderingWithSuccess:(void (^)(BOOL isOnlyLocal))successBlock
                          failure:(void (^)())failureBlock
{
    self.status = CartStatusOrdering;
    
    RequestResult result = RRFail;
    result = [MERCHANT requestUpdateOrderingForCart:self
                                    completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              NSDictionary *cartDict = [response data];
                              
                              if([cartDict objectForKey:@"id"] != nil){
                                  [self updateWithDictionary:cartDict menuPan:MENUPAN.wholeSerialMenuList];
                              }
                              
                              [self summation];
                              [[NSNotificationCenter defaultCenter] postNotificationName:LineItemChangedNotification
                                                                                  object:self
                                                                                userInfo:nil];
                              successBlock(NO);
                              
                              break;
                          }
                          case ResponseErrorNoCart:{
                              failureBlock();
                              [UIAlertView alertWithTitle:NSLocalizedString(@"This order list removed", nil)
                                                  message:NSLocalizedString(@"Your order list may be removed by restaurant at any reason. Ask restaurant for the reason.", nil)];
                              [[NSNotificationCenter defaultCenter] postNotificationName:CartUnsolicitChangedNotification
                                                                                  object:self
                                                                                userInfo:nil];
                              break;
                          }
                          case ResponseErrorCartFixed:{
                              failureBlock();
                              [[NSNotificationCenter defaultCenter] postNotificationName:CartUnsolicitChangedNotification
                                                                                  object:self
                                                                                userInfo:nil];
                              break;
                          }
                          case ResponseErrorGeneral:{
                              failureBlock();
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:{
                              failureBlock();
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while update order list status", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      failureBlock();
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            failureBlock();
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
    
}
#else
- (void)updateSubmitted:(BOOL)isSubmitted
                success:(void (^)(BOOL isOnlyLocal))successBlock
                failure:(void (^)())failureBlock
{
    self.status = isSubmitted ? CartStatusSubmitted : CartStatusOrdering;
    
    RequestResult result = RRFail;
    result = [MENU requestUpdateSubmittedForCart:self
                                 completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              NSDictionary *cartDict = [response data];
                              
                              if([cartDict objectForKey:@"id"] != nil){
                                  [self updateWithDictionary:cartDict menuPan:MENUPAN.wholeSerialMenuList];
                              }
                              
                              [self summation];
                              [[NSNotificationCenter defaultCenter] postNotificationName:LineItemChangedNotification
                                                                                  object:self
                                                                                userInfo:nil];
                              successBlock(NO);
                              
                              break;
                          }
                          case ResponseErrorNoCart:{
                              failureBlock();
                              [UIAlertView alertWithTitle:NSLocalizedString(@"This order list removed", nil)
                                                  message:NSLocalizedString(@"Your order list may be removed by restaurant at any reason. Ask restaurant for the reason.", nil)];
                              [[NSNotificationCenter defaultCenter] postNotificationName:CartUnsolicitChangedNotification
                                                                                  object:self
                                                                                userInfo:nil];
                              break;
                          }
                          case ResponseErrorCartFixed:{
                              failureBlock();
                              [[NSNotificationCenter defaultCenter] postNotificationName:CartUnsolicitChangedNotification
                                                                                  object:self
                                                                                userInfo:nil];
                              break;
                          }
                          case ResponseErrorGeneral:{
                              failureBlock();
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:{
                              failureBlock();
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while update order list status", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      failureBlock();
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            failureBlock();
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
    
}
#endif


- (void)summation
{
    [self summationWithForce:NO];
}

- (void)summationWithForce:(BOOL)force
{
    if(self.lineItems == nil && !force){
        // !!!: line items 없이 amount, taxAmount, quantities 값이 지정되어 있는 경우도 있기 때문에
        // 일반적인 경우는 amount, taxAmount, quantities를 보존하기위해 값을 계산하지 않고 그냥 리턴한다.
        PCWarning(@"Summation needs line items");
        return;
    }
    
    PCCurrency subTotal = 0;
    PCCurrency subTotalTaxable = 0;
    NSInteger count = 0;
    
    NSMutableString *lineItemNames = [NSMutableString string];
    
    for(LineItem *lineItem in self.lineItems){
        subTotal += lineItem.lineAmount;
        if(lineItem.taxable){
            subTotalTaxable += lineItem.lineAmount;
        }
        count += lineItem.quantity;
        
        if([lineItemNames length] > 0){
            [lineItemNames appendString:@", "];
        }
        [lineItemNames appendString:lineItem.menuName];
        if(lineItem.quantity > 1){
            [lineItemNames appendFormat:@"(%ld)", (long)lineItem.quantity];
        }
    }
    
    self.lineItemNames = lineItemNames;
    
    self.amount = subTotal;
    self.subtotalAmountTaxable = subTotalTaxable;
    self.taxAmount = roundf(self.subtotalAmountTaxable * (self.merchant.taxRate / 100.0f));
    self.quantities = count;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@{ \n\t no:%lld \n\t status:%d \n\t amount:%ld \n\t quantity:%ld \n\t line items:%@ \n\t rowID:%ld}",
            [super description],
            self.cartNo,
            self.status,
            (long)self.amount,
            (long)self.quantities,
            self.lineItems,
            (long)self.rowId];
}

- (NSString *)statusString
{
    switch(self.status){
        case CartStatusOrdering:
            return @"ordering";
            break;
        case CartStatusSubmitted:
            return @"submitted";
            break;
        case CartStatusFixed:
            return @"fixed";
            break;
        case CartStatusUnknown:
        default:
            return @"unknown";
            break;
    }
}


- (NSString *)amountString
{
    return [[NSNumber numberWithCurrency:self.amount] currencyString];
}

- (NSString *)taxAmountString
{
    return [[NSNumber numberWithCurrency:self.taxAmount] currencyString];
}

- (PCCurrency)subtotalAmount
{
    return self.amount + self.taxAmount;
}

- (NSString *)subtotalAmountString
{
    return [[NSNumber numberWithCurrency:self.subtotalAmount] currencyString];
}

- (NSString *)subtotalAmountTaxableString
{
    return [[NSNumber numberWithCurrency:self.subtotalAmountTaxable] currencyString];
}


- (void)setLineItems:(NSMutableArray *)lineItems
{
    self.lineItemsSection = nil;
    _lineItems = lineItems;
}

- (NSMutableArray *)lineItemsSection
{
    if(_lineItemsSection == nil){
        _lineItemsSection = [NSMutableArray array];
        
        NSArray *sortedArray = [self.lineItems sortedArrayWithOptions:0
                                                      usingComparator:
                                ^NSComparisonResult(LineItem *obj1, LineItem *obj2) {
                                    if(obj1.isMine && !obj2.isMine){
                                        return NSOrderedDescending;
                                    } else if(!obj1.isMine && obj2.isMine){
                                        return NSOrderedAscending;
                                    } else {
                                        //All is Mine or not Mine
                                        
                                        if(obj1.customerNo != 0 && obj2.customerNo == 0){
                                            return NSOrderedAscending;
                                        } else if(obj1.customerNo == 0 && obj2.customerNo != 0){
                                            return NSOrderedDescending;
                                        } else if(obj1.customerNo != 0 && obj2.customerNo != 0){
                                            // All member
                                            if(obj1.customerNo > obj2.customerNo){
                                                return NSOrderedDescending;
                                            } else if(obj1.customerNo < obj2.customerNo){
                                                return NSOrderedAscending;
                                            } else {
                                                if(obj1.lineItemNo > obj2.lineItemNo){
                                                    return NSOrderedDescending;
                                                } else if(obj1.lineItemNo < obj2.lineItemNo){
                                                    return NSOrderedAscending;
                                                } else {
                                                    if(obj1.rowId > obj2.rowId){
                                                        return NSOrderedDescending;
                                                    } else if(obj1.rowId < obj2.rowId){
                                                        return NSOrderedAscending;
                                                    } else {
                                                        return NSOrderedSame;
                                                    }
                                                }
                                            }
                                        } else {
                                            // All guest
                                            NSComparisonResult result = [obj1.udid compare:obj2.udid];
                                            if(result == NSOrderedSame){
                                                if(obj1.lineItemNo > obj2.lineItemNo){
                                                    return NSOrderedDescending;
                                                } else if(obj1.lineItemNo < obj2.lineItemNo){
                                                    return NSOrderedAscending;
                                                } else {
                                                    if(obj1.rowId > obj2.rowId){
                                                        return NSOrderedDescending;
                                                    } else if(obj1.rowId < obj2.rowId){
                                                        return NSOrderedAscending;
                                                    } else {
                                                        return NSOrderedSame;
                                                    }
                                                }
                                            } else {
                                                return result;
                                            }
                                        }
                                    }
                                }];
        
        TableSection *section = nil;
        NSMutableArray *subArray = nil;
        NSString *bfUDID = nil;
        PCSerial bfCustomerNo = NSNotFound;
        
        NSInteger companyCount = 1;
        
        for(LineItem *lineItem in sortedArray){
            if(
               ((lineItem.udid != nil) &&
                (![lineItem.udid isEqual:bfUDID]) && ((lineItem.customerNo != bfCustomerNo) || ((lineItem.customerNo == 0) && (bfCustomerNo == 0)))) ||
               (lineItem.customerNo != bfCustomerNo) ||
               (lineItem.udid == nil && bfUDID == nil)){
                
                section = [[TableSection alloc] init];
                
                if(lineItem.isMine || self.isMine){
                    
                    section.title = NSLocalizedString(@"My Order", nil);
                    
                } else {
                    
                    if([self.staffName length] > 0){
                        section.title = [NSString stringWithFormat:NSLocalizedString(@"%@'s Order", nil),self.staffName];
                    } else {
#if RUSHORDER_CUSTOMER
                        section.title = [NSString stringWithFormat:NSLocalizedString(@"Guest %d's Order", nil), companyCount++];
#else
                        section.title = [NSString stringWithFormat:NSLocalizedString(@"Customer %d's Order", nil), companyCount++];
#endif
                    }
                }
                
                subArray = [NSMutableArray array];
                section.menus = subArray;
                [_lineItemsSection addObject:section];
            }
            
            [subArray addObject:lineItem];
            
            bfCustomerNo = lineItem.customerNo;
            
            if(lineItem.udid == nil){
                bfUDID = @"<NIL>";
            } else {
                bfUDID = lineItem.udid;
            }
        }
    }
    
    return _lineItemsSection;
    
}

/*
 @property (nonatomic) PCSerial cartNo;
 @property (nonatomic) PCSerial tableNo;
 @property (nonatomic) PCSerial merchantNo;
 @property (copy, nonatomic) NSString *tableLabel;
 @property (nonatomic) CartStatus status;
 
 @property (strong, nonatomic) NSMutableArray *lineItems;
 @property (strong, nonatomic) NSMutableArray *lineItemsSection;
 */
#pragma mark - PCModelProtocol
- (id)initWithFMResult:(FMResultSet *)result
{
    self = [super init];
    if(self != nil){
        self.cartNo = [result longLongIntForColumn:@"cartNo"];
//        self.tableNo = [result longLongIntForColumn:@"tableNo"];
        self.merchantNo = [result longLongIntForColumn:@"merchantNo"];
        
        self.tableLabel = [result stringForColumn:@"tableLabel"];
        
        self.status = [result intForColumn:@"status"];
        
        self.cartType = [result intForColumn:@"cartType"];
        self.flagNumber = [result stringForColumn:@"flagNumber"];
        self.nonInteractive = [result boolForColumn:@"nonInteractive"];
        self.lastAccessedTime = [result dateForColumn:@"lastAccessedTime"];
        self.placedDate = [result dateForColumn:@"placedDate"];
        self.createdDate = [result dateForColumn:@"createdDate"];
    }
    
    return self;
}

- (BOOL)save
{
    BOOL rtn = NO;
    
    if(self.isExisted){
        rtn = [self update];
    } else {
        
        // !!!: Remove loca cart if there is same local cart for this merchant.
        [Cart deleteWithCondition:[NSString stringWithFormat:@"cartNo=0 and merchantNo=%lld",
                                   self.merchantNo]];
        
        rtn = [self insert];
        if(rtn){
            self.rowId = [DB.db lastInsertRowId];
            
            [self closeDB];
            
        } else {
            if(DB.db.lastErrorCode == 19){
                rtn = [self update];
                if(rtn){
                    PCError(@"Cart DB Update error : %d", DB.db.lastErrorCode);
                }
            } else {
                PCError(@"Cart DB Insert error : %d", DB.db.lastErrorCode);
            }
        }
    }
    
    if(rtn){
        
        if([LineItem deleteWithCondition:[NSString stringWithFormat:@"cartNo = %lld and merchantNo = %lld and lineItemNo = 0",
                                          self.cartNo,
                                          self.merchantNo]]){
            
            if([self.lineItems count] > 0){
                for(LineItem *lineItem in self.lineItems){
                    
                    if(lineItem.menuItem == nil){
                        [lineItem linkMenuItemInMenuList:MENUPAN.wholeSerialMenuList];
                    }
                    
                    lineItem.cartNo = self.cartNo;
                    lineItem.merchantNo = self.merchantNo;
                    lineItem.lineItemNo = 0; //This is local Line Item, Because we remove lineitems only lineitem no is 0
                    [lineItem insert]; //Don't use save, because LineItem's already removed from above deleteWithCondition
                }
            }
        } else {
            rtn = NO;
            PCError(@"Error to delete all lineitems related with cart number %lld and merchantNo %lld", self.cartNo, self.merchantNo);
        }
    }
    
    return rtn;
}

- (BOOL)insert
{
    if([self openDB]){
        
        return [DB.db executeUpdate:INSERT_QRY,
                [NSNumber numberWithLongLong:self.cartNo],
                [NSNumber numberWithLongLong:self.merchantNo],
                self.tableLabel,
                [NSNumber numberWithInt:self.status],
                [NSDate date],
                [NSNumber numberWithInteger:self.cartType],
                [NSDate date],
                [NSDate date],
                [NSDate date],
                self.flagNumber,
                [NSNumber numberWithBool:self.isNonInteractive]];
    } else {
        PCError(@"Cart insert error");
        return NO;
    }
    return [self closeDB];
}

- (BOOL)update
{
    if([self openDB]){
        
        return [DB.db executeUpdate:UPDATE_QRY,
                [NSNumber numberWithLongLong:self.merchantNo],
                self.tableLabel,
                [NSNumber numberWithInt:self.status],
                [NSNumber numberWithInteger:self.cartType],
                [NSDate date],
                self.flagNumber,
                [NSNumber numberWithBool:self.isNonInteractive],
                [NSNumber numberWithInteger:self.rowId]
                ];
        
    } else {
        PCError(@"Cart update error");
        return NO;
    }
    
    return [self closeDB];
}

- (BOOL)delete
{
    BOOL rtn = [super delete];
    
    if(rtn){
        if([LineItem deleteWithCondition:[NSString stringWithFormat:@"cartNo = %lld and merchantNo = %lld and lineItemNo = 0",
                                          self.cartNo,
                                          self.merchantNo]]){
            PCLog(@"Line Item deleted");
            self.lineItems = nil;
            self.lineItemsSection = nil;
            [self summationWithForce:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:LineItemChangedNotification
                                                                object:self
                                                              userInfo:nil];
            
        } else {
            PCError(@"Error to delete all lineitems related with cart number %lld and merchantNo %lld", self.cartNo, self.merchantNo);
        }
    }
    return rtn;
}

#pragma mark - Accessors
- (void)setMerchant:(Merchant *)merchant
{
    self.merchantName = merchant.name;
    
    self.merchantNo = merchant.merchantNo;
    
    _merchant = merchant;
}

- (BOOL)isLocal
{
    return (self.cartNo == 0 && self.isExisted);
}

- (BOOL)isValid
{
    return (self.status != CartStatusUnknown
            && self.status != CartStatusFixed);
}

- (NSString *)statusGuideString
{
    switch(self.status){
        case CartStatusOrdering:
            if(self.isLocal){
                return NSLocalizedString(@"Done ordering?\nPlease check out to place your order.", nil);
            } else {
                return NSLocalizedString(@"If you are ready to submit the order at this restaurant, enter and touch 'Submit Order' button.", nil);
            }
            break;
        case CartStatusSubmitted:
            return NSLocalizedString(@"Your order has been placed.\nYou will receive a confirmation notice shortly.", nil);
            break;
        default:
            return NSLocalizedString(@"Ordering...", nil);
            break;
    }
}

- (BOOL)isMineIncluded
{
    if(self.isMine){
        return YES;
    } else {
        for(LineItem *lineItem in self.lineItems){
            if(lineItem.isMine){
                return YES;
            }
        }
    }
    return NO;
}

- (BOOL)isAllItemsInHour
{
    return [self isAllItemsInHour:NO];
}

- (BOOL)isAllItemsInHour:(BOOL)forciblyReLinkMenu
{
    BOOL allInHour = YES;
    
    for(LineItem *lineItem in self.lineItems){
        if(lineItem.menuItem == nil || forciblyReLinkMenu){
            [lineItem linkMenuItemInMenuList:MENUPAN.wholeSerialMenuList];
        }
    }
    
    for(LineItem *lineItem in self.lineItems){
//        PCLog(@"PAN : %@", lineItem.menuItem.menuPan);
        lineItem.menuValidStatus &= ~MenuValidStatusNotAvailableOrderType;
        lineItem.menuValidStatus &= ~MenuValidStatusOutOfHours;
        
        if(lineItem.menuItem != nil){
            if(lineItem.menuItem.menuPan == nil){
                lineItem.menuValidStatus |= MenuValidStatusNotAvailableOrderType;
                allInHour = NO;
            } else {
                if(!lineItem.menuItem.menuPan.isMenuHour){
                    lineItem.menuValidStatus |= MenuValidStatusOutOfHours;
                    allInHour = NO;
                }
            }
        } else {
//            PCError(@"Menu item is null!!!! on line Item No:%d / menu ID:%d", lineItem.lineItemNo, lineItem.menuItemNo);
        }
    }
    
    return allInHour;
}

- (BOOL)isMine
{
#if RUSHORDER_CUSTOMER
    //                          NSString *curDeviceToken = [NSUserDefaults standardUserDefaults].deviceToken;
    return self.isLocal;
#else
    return self.staffNo == CRED.userInfo.userNo;
#endif
}

- (BOOL)hasAlcohol
{
    for(LineItem *lineItem in self.lineItems){
        if(lineItem.menuItem == nil){
            [lineItem linkMenuItemInMenuList:MENUPAN.wholeSerialMenuList];
        }
        
        if(lineItem.menuItem.isAlcohol){
            return YES;
            break;
        }
    }
    
    return NO;
}

- (BOOL)hasCorruptedItem
{
    for(LineItem *lineItem in self.lineItems){
        if(lineItem.menuValidStatus != MenuValidStatusValid) return YES;
    }
    
    return NO;
}

@end
