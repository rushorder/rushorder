//
//  TableSectionItem.h
//  RushOrder
//
//  Created by Conan on 3/28/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>

////////////////////////////////////////////////////////////////////////////////

@interface TableSection : NSObject
@property (copy, nonatomic) NSString *title;
@property (strong, nonatomic) NSMutableArray *menus;
@property (nonatomic) NSInteger tag;
@end



////////////////////////////////////////////////////////////////////////////////

@interface TableItem : NSObject
@property (nonatomic) NSInteger tag;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *subTitle;
@property (nonatomic) SEL action;
@property (strong, nonatomic) id object;
@property (nonatomic, getter = isSelected) BOOL selected;
@end



////////////////////////////////////////////////////////////////////////////////