//
//  CustomerInfo.h
//  RushOrder
//
//  Created by Conan on 3/2/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

/*
 {"result":0,"data":{"address":null,"admin":false,"birth":null,"created_at":"2013-03-04T08:00:58Z","device_token":"iOS simulator fake device token","email":"kmaku@novaple.com","gender":null,"id":3,"level":null,"name":null,"phone":null,"point":null,"provider":null,"role":"owner","uid":null,"updated_at":"2013-03-13T12:36:57Z"}}
 */
#import "PCModel.h"

@interface CustomerInfo : PCModel

@property (nonatomic) PCSerial customerNo;
@property (copy, nonatomic) NSString *authToken;
@property (copy, nonatomic) NSString *email;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *firstName;
@property (copy, nonatomic) NSString *lastName;
@property (copy, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) UIImage *photo;
@property (copy, nonatomic) NSURL *photoURL;
@property (copy, nonatomic) NSString *birthday;
@property (copy, nonatomic) NSDate *verifiedDate;

@property (nonatomic) PCCurrency credit;
@property (nonatomic) PCCurrency rewardedCredit;
@property (nonatomic) PCCurrency usedCredit;

@property (copy, nonatomic) NSString *referralCode;
@property (copy, nonatomic) NSString *referredCode;
@property (nonatomic) BOOL isReferredCodeRewarded;
@property (nonatomic) PCCurrency totalEarnedCredit;
@property (nonatomic) NSInteger referralBadgeCount;

@property (readonly) NSString *totalEarnedCreditString;
@property (readonly) NSNumber *customerNoNumber;


- (id)initWithDictionary:(NSDictionary *)dict;
- (void)updateWithDictionary:(NSDictionary *)dict;

@end