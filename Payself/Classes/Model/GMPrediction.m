//
//  GMPrediction.m
//  RushOrder
//
//  Created by Conan on 11/24/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "GMPrediction.h"

@implementation GMPrediction

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    if(self != nil){
        self.desc = [dict objectForKey:@"description"];
        if(self.desc == nil){
            self.desc = [dict objectForKey:@"formatted_address"];
        }
        self.placeId = [dict objectForKey:@"place_id"];
        self.reference = [dict objectForKey:@"reference"];
    }
    
    return self;
}

- (void)updateWithDetailDictionary:(NSDictionary *)dict
{
    self.name = [dict objectForKey:@"name"];
    
    NSDictionary *locationDict = [[dict objectForKey:@"geometry"] objectForKey:@"location"];
    self.coordinate = CLLocationCoordinate2DMake([locationDict doubleForKey:@"lat"],
                                                 [locationDict doubleForKey:@"lng"]);
    
    NSMutableString *streeAddress = [NSMutableString string];
    
    for(NSDictionary *compDict in [dict objectForKey:@"address_components"]){
        NSArray *types = [compDict objectForKey:@"types"];
        NSString *type = nil;
        
        if([types count] > 0){
            type = types[0];
        }
        
        if([type isEqual:@"street_number"]){
            if([streeAddress length] > 0) [streeAddress appendString:@" "];
            [streeAddress appendString:[compDict objectForKey:@"long_name"]];
        }
        
        if([type isEqual:@"route"]){
            if([streeAddress length] > 0) [streeAddress appendString:@" "];
            [streeAddress appendString:[compDict objectForKey:@"long_name"]];
        }
        
        if([type isEqual:@"locality"]){
            self.city = [compDict objectForKey:@"long_name"];
        }
        
        if([type isEqual:@"administrative_area_level_1"]){
            self.state = [compDict objectForKey:@"long_name"];
        }
        
        if([type isEqual:@"country"]){
            self.country = [compDict objectForKey:@"long_name"];
        }
        
        if([type isEqual:@"postal_code"]){
            self.zip = [compDict objectForKey:@"long_name"];
        }
    }
    
    self.street = streeAddress;
    
    if(self.name == nil){
        self.name = self.street;
    }
}

@end

/*
 CLLocationDegrees lat = [poiDict doubleForKey:@""];
 CLLocationDegrees lon = [poiDict doubleForKey:@""];
 
 NSMutableDictionary *addrDict = [NSMutableDictionary dictionary];
 CLLocationCoordinate2D coords = CLLocationCoordinate2DMake(lat, lon);
 MKPlacemark *placeMark = [[MKPlacemark alloc] initWithCoordinate:coords
 addressDictionary:<#(NSDictionary *)#>]
 MKMapItem *item = [[MKMapItem alloc] initWithPlacemark:<#(MKPlacemark *)#>];
*/