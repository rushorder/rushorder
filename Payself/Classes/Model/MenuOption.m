//
//  MenuOption.m
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MenuOption.h"

@implementation MenuOption

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [self init];
    
    if(self != nil){
        self.menuOptionNo = [dict serialForKey:@"id"];
        self.name = [dict objectForKey:@"name"];
        self.price = [dict currencyForKey:@"price"];
    }
    return self;
}

- (NSString *)priceString
{
    return [[NSNumber numberWithCurrency:self.price] currencyString];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@{ \n\t no:%lld \n\t name:%@ \n\t price:%ld\n\t}",
            [super description],
            self.menuOptionNo,
            self.name,
            (long)self.price];
}
@end