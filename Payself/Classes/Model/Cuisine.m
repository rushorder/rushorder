//
//  Cuisine.m
//  RushOrder
//
//  Created by Conan on 10/27/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "Cuisine.h"

@implementation Cuisine

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self){
        self.no = [dict serialForKey:@"id"];
        self.name = [dict objectForKey:@"name"];
    }
    
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ \n\tid : %lld\n\t name:%@",
            [super description],
            self.no,
            self.name];
}

@end
