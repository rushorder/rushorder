//
//  DeliveryAddress.h
//  RushOrder
//
//  Created by Conan on 2/20/17.
//  Copyright (c) 2017 RushOrder. All rights reserved.
//

#import "Addresses.h"

typedef enum deliveryAddressSimilarity_{
    DeliveryAddressSimilarityDifferent = 0,
    DeliveryAddressSimilaritySimilar,
    DeliveryAddressSimilaritySame,
} DeliveryAddressSimilarity;

@interface DeliveryAddress : NSObject

@property (nonatomic) PCSerial no;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *phoneNumber;
@property (nonatomic, copy) NSString *street;
@property (nonatomic, copy) NSString *street2;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *zip;
@property (nonatomic, copy) NSString *deliveryInstruction;
@property (nonatomic, getter = isDefaultAddress) BOOL defaultAddress;
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic) CLLocationCoordinate2D takenCoordinate;

#pragma mark - readonly
@property (readonly) NSString *fullAddress;
@property (readonly) NSString *address;
@property (readonly) NSString *shortAddress;
@property (readonly) NSString *shortAddressWithName;

+ (void)newDeliveryAddressWithAddress:(Addresses *)address;
+ (void)updateDeliveryAddress:(DeliveryAddress *)deliveryAddress withAddress:(Addresses *)address;

- (id)initWithDictionary:(NSDictionary *)dict;
- (void)updateWithDictionary:(NSDictionary *)dict;

- (DeliveryAddressSimilarity)sameWithStreet:(NSString *)street street2:(NSString *)street2 city:(NSString *)city state:(NSString *)state zip:(NSString *)zip phoneNumber:(NSString *)phoneNumber name:(NSString *)name;
- (DeliveryAddressSimilarity)sameWithAddresses:(Addresses *)address;
- (void)updateWithAddresses:(Addresses *)address;
@end
