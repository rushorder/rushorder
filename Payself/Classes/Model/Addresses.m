//
//  Addresses.m
//  RushOrder
//
//  Created by Conan on 3/3/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import "Addresses.h"

#define INSERT_QRY @"insert into 'Addresses'(\
receiverName\
,address1\
,address2\
,city\
,state\
,zip\
,phoneNumber\
,takenLongitude\
,takenLatitude\
,longitude\
,latitude\
,lastUsedDate) \
values \
(?,?,?,?,?,?,?,?,?,?,?,?)"

#define UPDATE_QRY @"update 'Addresses' set \
receiverName = ?\
,address1 = ?\
,address2 = ?\
,city = ?\
,state = ?\
,zip = ?\
,phoneNumber = ?\
,takenLongitude = ?\
,takenLatitude = ?\
,latitude = ?\
,longitude = ?\
,lastUsedDate = ?\
where addressNo = ?"

@implementation Addresses

+ (NSString *)orderByQuery
{
    return @"order by lastUsedDate desc";
}

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self != nil){
        [self updateWithDictionary:dict];
    }
    return self;
}

- (id)initWithOrder:(Order *)order
{
    self = [super init];
    if(self != nil){
        [self updateWithOrder:order];
    }
    return self;
}

- (void)updateWithDictionary:(NSDictionary *)dict
{
    if(dict != nil){
        self.receiverName = [dict objectForKey:@"delivery_customer_name"];
        self.address1 = [dict objectForKey:@"delivery_address_street_1"];
        self.address2 = [dict objectForKey:@"delivery_address_street_2"];
        self.city = [dict objectForKey:@"delivery_address_city"];
        self.state = [dict objectForKey:@"delivery_address_state"];
        self.zip = [dict objectForKey:@"delivery_address_zipcode"];
        self.phoneNumber = [dict objectForKey:@"delivery_phone_number"];
    }
}

- (void)updateWithOrder:(Order *)order
{
    self.receiverName = order.receiverName;
    self.address1 = order.address1;
    self.address2 = order.address2;
    self.city = order.city;
    self.state = order.state;
    self.zip = order.zip;
    self.phoneNumber = order.phoneNumber;
    self.takenCoordinate = order.takenCoordinate;
}

#pragma mark - PCModelProtocol
- (id)initWithFMResult:(FMResultSet *)result
{
    self = [super init];
    if(self != nil){
        self.addressNo = [result intForColumn:@"addressNo"];
        self.receiverName = [result stringForColumn:@"receiverName"];
        self.address1 = [result stringForColumn:@"address1"];
        self.address2 = [result stringForColumn:@"address2"];
        self.city = [result stringForColumn:@"city"];
        self.state = [result stringForColumn:@"state"];
        self.zip = [result stringForColumn:@"zip"];
        self.phoneNumber = [result stringForColumn:@"phoneNumber"];
        
        self.lastUsedDate = [result dateForColumn:@"lastUsedDate"];

        CLLocationDegrees latitude = [result doubleForColumn:@"latitude"];
        CLLocationDegrees longitude = [result doubleForColumn:@"longitude"];
        self.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
        
        CLLocationDegrees takenLatitude = [result doubleForColumn:@"takenLatitude"];
        CLLocationDegrees takenLongitude = [result doubleForColumn:@"takenLongitude"];
        self.takenCoordinate = CLLocationCoordinate2DMake(takenLatitude, takenLongitude);

    }
    return self;
}

- (BOOL)save
{
    Addresses *exOne = [self duplicatedOne];
    if(exOne != nil){
        return [exOne updateLastDate];
    } else {
        if(self.isExisted){
            return [self update];
        } else {
            BOOL rtn = [self insert];
            if(rtn){
                self.rowId = [DB.db lastInsertRowId];
                
                [self closeDB];
            } else {
                if(DB.db.lastErrorCode == 19){
                    rtn = [self update];
                    if(!rtn){
                        PCError(@"Address DB Update error : %d", DB.db.lastErrorCode);
                    }
                } else {
                    PCError(@"Address DB Insert error : %d", DB.db.lastErrorCode);
                }
            }
            return rtn;
        }
    }
}

- (BOOL)insert
{
    if([self openDB]){
        self.lastUsedDate = [NSDate date];
        return [DB.db executeUpdate:INSERT_QRY,
                self.receiverName,
                self.address1,
                self.address2 ? self.address2 : @"",
                self.city,
                self.state,
                self.zip,
                self.phoneNumber,
                [NSNumber numberWithDouble:self.takenCoordinate.longitude],
                [NSNumber numberWithDouble:self.takenCoordinate.latitude],
                [NSNumber numberWithDouble:self.coordinate.longitude],
                [NSNumber numberWithDouble:self.coordinate.latitude],
                self.lastUsedDate];
    } else {
        PCError(@"Order insert error");
        return NO;
    }
}

- (BOOL)update
{
    if([self openDB]){
        return [DB.db executeUpdate:UPDATE_QRY,
                self.receiverName,
                self.address1,
                self.address2 ? self.address2 : @"",
                self.city,
                self.state,
                self.zip,
                self.phoneNumber,
                [NSNumber numberWithDouble:self.takenCoordinate.longitude],
                [NSNumber numberWithDouble:self.takenCoordinate.latitude],
                [NSNumber numberWithDouble:self.coordinate.longitude],
                [NSNumber numberWithDouble:self.coordinate.latitude],
                self.lastUsedDate,
                [NSNumber numberWithSerial:self.addressNo]];
        
        
    } else {
        PCError(@"Order update error");
        return NO;
    }
}

- (BOOL)updateLastDate
{
    if([self openDB]){
        self.lastUsedDate = [NSDate date];
        return [DB.db executeUpdate:@"update 'Addresses' set lastUsedDate=? where addressNo = ?",
                self.lastUsedDate,
                [NSNumber numberWithSerial:self.addressNo]];
        
        
    } else {
        PCError(@"Order update error");
        return NO;
    }
}

- (NSString *)fullAddress
{
    NSMutableString *address = [NSMutableString string];
    
    if([self.receiverName length] > 0){
        [address appendString:self.receiverName];
    }
    
    if([self.address1 length] > 0){
        if([address length] > 0) [address appendString:@"\n"];
        [address appendString:self.address1];
    }
    
    if([self.address2 length] > 0){
        if([address length] > 0) [address appendString:@", "];
        [address appendString:self.address2];
    }
    
    if([self.city length] > 0){
        if([address length] > 0) [address appendString:@"\n"];
        [address appendString:self.city];
    }
    
    if([self.state length] > 0){
        if([address length] > 0) [address appendString:@", "];
        [address appendString:self.state];
    }
    
    if([self.zip length] > 0){
        if([address length] > 0) [address appendString:@" "];
        [address appendString:self.zip];
    }
    
    
    if([self.phoneNumber length] > 0){
        if([address length] > 0) [address appendString:@"\n"];
        [address appendString:self.phoneNumber];
    }
    return address;
}

- (NSString *)address
{
    NSMutableString *address = [NSMutableString string];
    
    [address appendString:self.address1];
    
    if([self.address2 length] > 0){
        [address appendString:@", "];
        [address appendString:self.address2];
    }
    
    if([self.city length] > 0){
        [address appendString:@"\n"];
        [address appendString:self.city];
    }
    
    if([self.state length] > 0){
        [address appendString:@", "];
        [address appendString:self.state];
    }
    
    if([self.zip length] > 0){
        [address appendString:@" "];
        [address appendString:self.zip];
    }
    
    return address;
}

- (Addresses *)duplicatedOne;
{
    NSString *condition = [NSString stringWithFormat:@"where address1='%@' and address2='%@' and city='%@' and state='%@' and zip='%@'",
                           self.address1 ? self.address1 : @"",
                           self.address2 ? self.address2 : @"",
                           self.city ? self.city : @"",
                           self.state ? self.state : @"",
                           self.zip ? self.zip : @""];
    
    NSArray *exList = [Addresses listWithCondition:condition];
    
    if([exList count] > 0){
        Addresses *exOne = [exList objectAtIndex:0];
        PCWarning(@"Address is duplicated %@", self, exList);
        return exOne;
    } else {
        return nil;
    }
}

- (NSDictionary *)dictRep
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
            self.address1, @"street",
            self.address2, @"street2",
            self.city, @"city",
            self.state, @"state",
            self.zip, @"zip",
            nil];
}

@end
