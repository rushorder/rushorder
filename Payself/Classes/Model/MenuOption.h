//
//  MenuOption.h
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCModel.h"

@interface MenuOption : PCModel

@property (nonatomic) PCSerial menuOptionNo;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *desc;
@property (nonatomic) PCCurrency price;

@property (nonatomic, readonly) NSString *priceString;
@property (nonatomic, getter = isSelected) BOOL selected;

- (id)initWithDictionary:(NSDictionary *)dict;
@end