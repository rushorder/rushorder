//
//  MenuSubCategory.h
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCModel.h"

@interface MenuSubCategory : PCModel

@property (nonatomic) PCSerial subCategoryNo;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *desc;

@property (strong, nonatomic) NSMutableArray *items;
@property (nonatomic) CGFloat cellHeight;

- (id)initWithDictionary:(NSDictionary *)dict;
@end
