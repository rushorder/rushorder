//
//  MobileCard.h
//  RushOrder
//
//  Created by Conan on 2/22/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCModel.h"

typedef enum pgType_{
    PgTypeStripe = 0,
    PgTypeBraintree
} PgType;

@interface MobileCard : PCModel <PCModelProtocol>

@property (nonatomic) PCSerial cardId;
@property (copy, nonatomic) NSString *cardNumber;
@property (nonatomic) NSInteger last4;
@property (nonatomic) Issuer cardIssuer;
@property (copy, nonatomic) NSString *cardFingerPrint;
@property (nonatomic) NSInteger cardExpireMonth;
@property (nonatomic) NSInteger cardExpireYear;
@property (copy, nonatomic) NSString *cardCvc;
@property (copy, nonatomic) NSString *zipCode;
@property (copy, nonatomic) NSString *bankName; //calculated
@property (nonatomic) PgType pgType;

@property (nonatomic, readonly) UIImage *cardLogo;
@property (nonatomic, readonly) NSString *formattedCardNumber;

- (id)initWithDictionary:(NSDictionary *)dict;

- (NSString *)formattedCardNumber:(BOOL)securely;
- (void)updateWithPaymentDictionary:(NSDictionary *)dict;
- (void)updateWithCardDictionary:(NSDictionary *)dict;

@end