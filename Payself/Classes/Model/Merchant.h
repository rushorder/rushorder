//
//  Merchant.h
//  RushOrder
//
//  Created by Conan on 2/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "PCModel.h"
#import "Promotion.h"
#import "OperationHour.h"

typedef enum merchantCachedType_{
    MerchantCachedTypeUnknown = 0,
    MerchantCachedTypeVisited,
    MerchantCachedTypeFavorite
} MerchantCachedType;

typedef enum servicedBy_{
    ServicedByUnknown = 0,
    ServicedBySelf,
    ServicedByStaff,
    ServicedByNone
} ServicedBy;

typedef enum restaurantType_{
    RestaurantTypeSitdown = 1,
    RestaurantTypeFastfood,
    RestaurantTypeTakeoutOnly
} RestaurantType;

typedef enum deliveryFeePlan_{
    DeliveryFeePlanUnknown = 0,
    DeliveryFeePlanFlat,
    DeliveryFeePlanZone,
    DeliveryFeePlanRadius
} DeliveryFeePlan;

typedef enum pickType_{
    PickTypeNone            = 0,
    PickTypeNew             = 1 << 0,
    PickTypeEditor          = 1 << 1,
    PickTypeAwesome         = 1 << 2,
} PickType;

@interface Merchant : PCModel<PCModelProtocol, MKAnnotation>

@property (nonatomic) PCSerial merchantNo;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *address;
@property (copy, nonatomic) NSString *geoAddress;
@property (copy, nonatomic) NSString *desc;
@property (nonatomic) NSInteger numberOfTables;
@property (copy, nonatomic) NSString *ownerName;
@property (copy, nonatomic) NSString *phoneNumber;
@property (copy, nonatomic) NSString *roPhoneNumber;
@property (strong, nonatomic) CLLocation *location;
@property (strong, nonatomic) NSDate *lastVisitedDate;
@property (strong, nonatomic) NSURL *imageURL;
@property (strong, nonatomic) NSURL *logoURL;
@property (strong, nonatomic) NSMutableArray *photoURLs;
@property (nonatomic, copy) NSString *takeoutRemark;
@property (nonatomic) float taxRate;
@property (nonatomic) NSInteger price;

@property (nonatomic, getter = isAuthenticated) BOOL authenticated;

@property (nonatomic, getter = isMenuOrder) BOOL menuOrder;
@property (nonatomic, getter = isAblePayment) BOOL ablePayment;
@property (nonatomic, getter = isTableBase) BOOL tableBase;

@property (nonatomic, getter = isAbleTakeout) BOOL ableTakeout;
@property (nonatomic, getter = isAbleDelivery) BOOL ableDelivery;
@property (readonly) BOOL isOnlyDelivery;
@property (readonly) BOOL isAbleTakeoutOrDelivery;
@property (readonly) BOOL isAbleTakeoutAndDelivery;
@property (nonatomic) ServicedBy servicedBy;

@property (nonatomic, getter = isOpen) BOOL open;
@property (nonatomic, getter = isTakeoutOpen) BOOL takeoutOpen;
@property (nonatomic, getter = isPublished) BOOL published;
@property (nonatomic) BOOL displayGeoAddress;

@property (nonatomic) MerchantCachedType cachedType;

@property (nonatomic) DeliveryFeePlan deliveryFeePlan;
@property (strong, nonatomic) NSMutableArray *deliveryFeeZone;
@property (strong, nonatomic) NSArray *deliveryFeeDiscount;
@property (strong, nonatomic) NSMutableArray *operationHours;
@property (strong, nonatomic) NSMutableArray *deliveryHours;
@property (strong, nonatomic) NSMutableArray *intersectHours;

//Delivery optional information
@property (nonatomic) PCCurrency deliveryFee;
@property (nonatomic) PCCurrency deliveryMinOrderAmount;
@property (nonatomic) PCCurrency takeoutMinOrderAmount;
@property (nonatomic) PCCurrency dineinMinOrderAmount;

@property (nonatomic) NSInteger averageDeliveryTime;

@property (nonatomic) CLLocationDistance deliveryDistanceLimit; //mile

@property (nonatomic, getter = isAllowTip) BOOL allowTip;
@property (readonly) Promotion *promotion;
@property (nonatomic, strong) NSMutableArray *promotions; // will be stored promotion

@property (readonly) NSString *displayAddress;
@property (readonly) NSString *displayMultilineAddress;
@property (nonatomic, copy) NSString *multilineAddress;

#pragma mark - Not in persistent
@property (nonatomic) CLLocationDistance distance; //meters
@property (nonatomic) CLLocationDistance deliveryDistance; //meters
@property (nonatomic) CLLocationDistance distanceFromServer;

@property (nonatomic, readonly) NSString *restaurantTypeString;

@property (nonatomic, getter = isTestMode) BOOL testMode;
@property (nonatomic, getter = isDemoMode) BOOL demoMode;
@property (nonatomic) PickType pickType;
@property (nonatomic, strong) NSString *creditPolicyName;
@property (nonatomic, readonly) NSString *displayName;
@property (copy, nonatomic) NSString *hours;
@property (copy, nonatomic) NSString *homepage;
@property (copy, nonatomic) NSString *email;
@property (nonatomic, readonly) NSString *servicedByString;

@property (nonatomic, readonly) BOOL isServicedByStaff;
@property (nonatomic, readonly) BOOL isAbleDinein;

@property (nonatomic, getter = isFavorite) BOOL favorite;

@property (nonatomic, copy) NSString *code;

@property (strong, nonatomic) NSMutableArray *tableList;

// merchant app exclusivly
@property (nonatomic, strong) UIImage *logoImage;

@property (nonatomic, getter = isNonInteractive) BOOL nonInteractive;

@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *address1;
@property (nonatomic, copy) NSString *address2;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *zip;

@property (nonatomic) BOOL autoOpenClose;
@property (nonatomic) BOOL autoDeliveryHours;
@property (nonatomic, copy) NSDate *lastMenuModifiedDate;
@property (nonatomic, strong) NSTimeZone *timeZone;

@property (nonatomic) PCCurrency deliveryBigOrderAmount;
@property (nonatomic) PCCurrency deliveryFeeAmountForBigOrder;

@property (readonly) BOOL isOpenHour;
@property (readonly) BOOL isDeliveryHour;
@property (nonatomic) BOOL openStatus;
@property (nonatomic) BOOL deliveryOpenStatus;

@property (nonatomic, getter = isAllPropertySet) BOOL allPropertySet;

#pragma mark - Banking - Not in persistent too
@property (copy, nonatomic) NSString *ein;
@property (copy, nonatomic) NSString *ssn;
@property (copy, nonatomic) NSString *bankAccountOwnerName;
@property (copy, nonatomic) NSString *bankAccountNumber;
@property (copy, nonatomic) NSString *bankRoutingNumber;
@property (strong, nonatomic) CLLocation *relativeLocation;

#pragma mark - MKAnnotation
@property (readonly, nonatomic) CLLocationCoordinate2D coordinate;
@property (readonly, copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *nextAvailableTime;
@property (nonatomic, strong) NSDate *lastOperationDate;

@property (nonatomic) NSInteger nearestDeliveryAvailableTimeIndex;
@property (nonatomic) NSInteger nearestAvailableTimeIndex;
@property (strong, nonatomic) OperationHour *nearestOperationHour;
@property (strong, nonatomic) OperationHour *nearestDeliveryOperationHour;

@property (nonatomic, getter = isRoService) BOOL roService;
@property (nonatomic) float roServiceRate;
@property (nonatomic) PCCurrency roServiceMinAmount;

@property (readonly) BOOL hasRoServiceFee;

@property (readonly) NSString *distanceString;
@property (readonly) NSString *deliveryDistanceString;
@property (readonly) NSString *distanceFromServerString;

#pragma mark - Cached height
@property (nonatomic) CGFloat cellHeight;


+ (BOOL)changeToUnknownMerchant;

- (id)initWithDictionary:(NSDictionary *)dict;

- (void)refreshWithMerchant:(Merchant *)merchant;

- (void)updateWithDictionary:(NSDictionary *)dict;

- (PCCurrency)deliveryFeeAt:(CLLocation *)location;
- (PCCurrency)deliveryFeeAt:(CLLocation *)location andSubtotal:(PCCurrency)subtotal;

- (NSString *)nextDeliveryAvailableTime;
- (NSString *)nextDeliveryAvailableHumanReadableTime;


- (NSArray *)promotionsOnCartType:(CartType)cartType;

- (Promotion *)promotionOnCartType:(CartType)cartType;

- (Promotion *)promotionOnOrderType:(OrderType)orderType;

- (NSArray *)promotionsOnOrderType:(OrderType)orderType;

- (NSArray *)promotionsOnPromotionOrderType:(PromotionOrderType)promotionOrderType;

- (PCCurrency)polygonDeliveryFeeAt:(CLLocation *)location;

- (BOOL)isDeliveryHourWithDate:(NSDate *)date;
- (BOOL)isOpenHourWithDate:(NSDate *)date;
- (NSMutableArray *)intersectOperationHours;
- (NSString *)nextAvailableIntersectTime;

@end
