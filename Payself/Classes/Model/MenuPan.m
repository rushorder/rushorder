//
//  TableItem.m
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MenuPan.h"
#import "OperationHour.h"

@implementation MenuPan


- (id)initWithDictionary:(NSDictionary *)dict withTimeZone:(NSTimeZone *)timeZone
{
    self = [self init];
    
    if(self != nil){
        if(timeZone == nil){
            timeZone = [NSTimeZone timeZoneWithName:@"America/Los_Angeles"];
        }
        
        self.timeZone = timeZone;
        self.menuPanNo = [dict serialForKey:@"id"];
        self.name = [dict objectForKey:@"name"];
        self.active = [dict boolForKey:@"active"];
        
        id obj = [dict objectForKey:@"is_always_on"];
        if(obj != nil){
            self.alwaysOn = [dict boolForKey:@"is_always_on"];
        } else {
            self.alwaysOn = YES;
        }
    
        NSArray *menuHourArray = [dict objectForKey:@"menu_hours"];
        if([menuHourArray count] > 0){
            self.menuHours = [NSMutableArray array];
            for(NSDictionary *hoursDict in menuHourArray){
                OperationHour *hour = [[OperationHour alloc] init];
                hour.eventTime = [hoursDict integerForKey:@"start_time"];
                hour.operationEvent = OperationEventOpen;
                hour.openSpanDict = hoursDict;
                [self addOperationEvent:hour];
                
                hour = [[OperationHour alloc] init];
                hour.eventTime = [hoursDict integerForKey:@"end_time"];
                hour.operationEvent = OperationEventClose;
                hour.openSpanDict = hoursDict;
                [self addOperationEvent:hour];
            }
        } else {
            self.menuHours = nil;
        }

    }
    return self;
}

- (void)addOperationEvent:(OperationHour *)hour
{
    NSUInteger index = [self.menuHours indexOfObject:hour
                                            inSortedRange:NSMakeRange(0, [self.menuHours count])
                                                  options:NSBinarySearchingInsertionIndex | NSBinarySearchingFirstEqual
                                          usingComparator:^NSComparisonResult(OperationHour *obj1, OperationHour *obj2) {
                                              if(obj1.eventTime > obj2.eventTime){
                                                  return NSOrderedDescending;
                                              } else if(obj1.eventTime < obj2.eventTime){
                                                  return NSOrderedAscending;
                                              } else {
                                                  if(obj1.operationEvent == OperationEventOpen){
                                                      return NSOrderedDescending;
                                                  } else if (obj1.operationEvent == OperationEventClose){
                                                      return NSOrderedAscending;
                                                  } else {
                                                      return NSOrderedSame;
                                                  }

                                              }
                                          }];
    [self.menuHours insertObject:hour atIndex:index];
}

- (BOOL)isMenuHour
{
    if([self.menuHours count] > 0 && !self.isAlwaysOn){
        NSCalendar *calendar = [NSCalendar currentCalendar];
        calendar.timeZone = self.timeZone;
        
        NSDateComponents *currentComponents = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitWeekday
                                                          fromDate:[NSDate date]];
        NSInteger weekday = (currentComponents.weekday - 1);
        if(weekday == 0) weekday = 7;
        
        OperationHour *currentHour = [[OperationHour alloc] init];
        
        currentHour.eventTime = [[NSString stringWithFormat:@"%ld%02ld%02ld",
                                  (long)weekday,
                                  (long)currentComponents.hour,
                                  (long)currentComponents.minute] integerValue];
        PCLog(@"self.menuHours : %@", self.menuHours);
        PCLog(@"Current Hour : %@", currentHour);
        
        NSUInteger firstMatchingIndex = [self.menuHours indexOfObject:currentHour
                                                             inSortedRange:NSMakeRange(0, [self.menuHours count])
                                                                   options:NSBinarySearchingInsertionIndex | NSBinarySearchingFirstEqual
                                                           usingComparator:^NSComparisonResult(OperationHour *obj1, OperationHour *obj2) {
                                                               if(obj1.eventTime > obj2.eventTime){
                                                                   return NSOrderedDescending;
                                                               } else if(obj1.eventTime < obj2.eventTime){
                                                                   return NSOrderedAscending;
                                                               } else {
                                                                   if(obj1 == currentHour) return NSOrderedDescending;
                                                                   else if(obj2 == currentHour) return NSOrderedAscending;
                                                                   else {
                                                                       if(obj1.operationEvent == OperationEventOpen){
                                                                           return NSOrderedDescending;
                                                                       } else if (obj1.operationEvent == OperationEventClose){
                                                                           return NSOrderedAscending;
                                                                       } else {
                                                                           return NSOrderedSame;
                                                                       }
                                                                   }
                                                               }
                                                           }];
    
        if(firstMatchingIndex == 0){
            firstMatchingIndex = ([self.menuHours count] - 1);
        } else {
            firstMatchingIndex -= 1;
        }
        
        if(firstMatchingIndex >= ([self.menuHours count] - 1)){
            self.nearestAvailableTimeIndex = 0;
        } else {
            self.nearestAvailableTimeIndex = firstMatchingIndex + 1;
        }
        
        OperationHour *prevHour = [self.menuHours objectAtIndex:firstMatchingIndex];
        
        return (prevHour.operationEvent == OperationEventOpen);
        
    } else {
        return self.isAlwaysOn;
    }
}

- (NSString *)nextAvailableTime
{
    OperationHour *nextHour = [self.menuHours objectAtIndex:self.nearestAvailableTimeIndex];
    
    if(nextHour == nil) return @"";
    
    NSNumber *startTimeNumber = [nextHour.openSpanDict objectForKey:@"start_time"];
    NSNumber *endTimeNumber = [nextHour.openSpanDict objectForKey:@"end_time"];
    NSString *startTime = [startTimeNumber stringValue];
    NSString *endTime = [endTimeNumber stringValue];
    NSString *startWeekString = [startTime substringToIndex:1];
    NSString *endWeekString = [endTime substringToIndex:1];
    NSString *startHourString = [startTime substringWithRange:NSMakeRange(1, 2)];
    NSString *endHourString = [endTime substringWithRange:NSMakeRange(1, 2)];
    NSString *startMinString = [startTime substringWithRange:NSMakeRange(3, 2)];
    NSString *endMinString = [endTime substringWithRange:NSMakeRange(3, 2)];
    
    if([startHourString integerValue] == 24){
        NSInteger weekVal = [startWeekString integerValue];
        weekVal++;
        if(weekVal > 7) weekVal = 1;
        startWeekString = [NSString stringWithInteger:weekVal];
    }
    
    if([endHourString integerValue] == 24){
        NSInteger weekVal = [endWeekString integerValue];
        weekVal++;
        if(weekVal > 7) weekVal = 1;
        endWeekString = [NSString stringWithInteger:weekVal];
    }
    
    NSString *startAmPmHour = [self amPmStringWithHour:[startHourString integerValue]
                                               minute:[startMinString integerValue]];
    
    NSString *endAmPmHour = [self amPmStringWithHour:[endHourString integerValue]
                                               minute:[endMinString integerValue]];
    
    return [NSString stringWithFormat:@"%@ %@ - %@ %@",
            [self weekStringAtIndex:[startWeekString integerValue]],
            startAmPmHour,
            [self weekStringAtIndex:[endWeekString integerValue]],
            endAmPmHour];
}

- (NSString *)amPmStringWithHour:(NSInteger)hour minute:(NSInteger)minute
{
    NSString *amPmString = @"AM";
    
    if(hour == 0){
        hour = 12;
    } else if (hour >= 12) {
        if(hour >= 13){
            hour -= 12;
            if(hour == 12){
                amPmString = @"AM";
            } else {
                amPmString = @"PM";
            }
        } else {
            amPmString = @"PM";
        }
    }
    
    return [NSString stringWithFormat:@"%ld:%02ld %@", (long)hour, (long)minute, amPmString];
}

- (NSString *)weekStringAtIndex:(NSInteger)weekIndex
{
    switch(weekIndex){
        case 1: return NSLocalizedString(@"Mon", nil);
            break;
        case 2: return NSLocalizedString(@"Tue", nil);
            break;
        case 3: return NSLocalizedString(@"Wed", nil);
            break;
        case 4: return NSLocalizedString(@"Thu", nil);
            break;
        case 5: return NSLocalizedString(@"Fri", nil);
            break;
        case 6: return NSLocalizedString(@"Sat", nil);
            break;
        case 7: return NSLocalizedString(@"Sun", nil);
            break;
    }
    return @"Unknown";
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@{ \n\t no:%lld \n\t name:%@}",
            [super description],
            self.menuPanNo,
            self.name];
}

@end
