//
//  DZonePolygon.m
//  RushOrder
//
//  Created by Conan on 7/1/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

/*
 
 "delivery_zones" =         (
 {
 "fee_amount" = 100;
 label = 1;
 "merchant_id" = 1;
 points =                 (
 {
 latitude = "34.035966";
 longitude = "-118.444963";
 },
 {
 latitude = "34.036504";
 longitude = "-118.444094";
 },
 {
 latitude = "34.035033";
 longitude = "-118.442790";
 },
 {
 latitude = "34.034495";
 longitude = "-118.443697";
 },
 {
 latitude = "34.035966";
 longitude = "-118.444963";
 }
 );
 "zone_type" = polygon;
 },
 {
 "fee_amount" = 200;
 label = 2;
 "merchant_id" = 1;
 points =                 (
 {
 latitude = "34.036762";
 longitude = "-118.447119";
 },
 {
 latitude = "34.038407";
 longitude = "-118.444362";
 },
 {
 latitude = "34.034237";
 longitude = "-118.440682";
 },
 {
 latitude = "34.033232";
 longitude = "-118.444153";
 },
 {
 latitude = "34.036762";
 longitude = "-118.447119";
 }
 );
 "zone_type" = polygon;
 }
 
 */

#import "DZonePolygon.h"

@implementation DZonePoint
- (id)initWithLatitude:(NSNumber *)latitude longitude:(NSNumber *)longitude
{
    self = [super init];
    
    if(self != nil){
        self.latitude = latitude;
        self.longitude = longitude;
    }
    
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@,%@", self.latitude, self.longitude];
}


@end

@implementation DZonePolygon

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self != nil){
        [self updateWithDictionary:dict];
    }
    
    return self;
}

- (void)updateWithDictionary:(NSDictionary *)dict
{
    self.label = [dict objectForKey:@"label"];
    self.feeAmount = [dict currencyForKey:@"fee_amount"];
    self.priority = [dict integerForKey:@"priority"];
    NSArray *points = [dict objectForKey:@"points"];
    
    self.polygonPoints = [NSMutableArray array];
    
    for(NSDictionary *dict in points){
        NSNumber *latNumber = [NSNumber numberWithDouble:[dict doubleForKey:@"latitude"]];
        NSNumber *longNumber = [NSNumber numberWithDouble:[dict doubleForKey:@"longitude"]];
        
        DZonePoint *dzPoint = [[DZonePoint alloc] initWithLatitude:latNumber
                                                         longitude:longNumber];
        [self.polygonPoints addObject:dzPoint];
    }
}

- (BOOL)isContainLocation:(CLLocation *)location
{
    if(location == nil){
        return NO;
    }
    
    DZonePoint *fromPoint = [[DZonePoint alloc] initWithLatitude:[NSNumber numberWithDouble:location.coordinate.latitude]
                                                       longitude:[NSNumber numberWithDouble:location.coordinate.longitude]];
    
    NSInteger crossingCount = 0;
    
    NSInteger i = 0;
    
    for(i = 0 ; i < [self.polygonPoints count] ; i++){
        DZonePoint *sPoint = [self.polygonPoints objectAtIndex:i];
        NSInteger j = i + 1;
        if(j >= [self.polygonPoints count]){
            
            DZonePoint *firstPoint = [self.polygonPoints objectAtIndex:0];
            
            if([firstPoint.latitude isEqualToNumber:sPoint.latitude] &&
               [firstPoint.longitude isEqualToNumber:sPoint.longitude]){
                break;
            }
            
            j = 0;
        }
        DZonePoint *ePoint = [self.polygonPoints objectAtIndex:j];
        
//        PCLog(@"p:%@ s:%@ e:%@", fromPoint, sPoint, ePoint);
        if([self rayCrossesFromPoint:fromPoint
                              sPoint:sPoint
                              ePoint:ePoint]){
            crossingCount++;
        }
        PCLog(@"crossingCount %d", crossingCount);
    }
    
//    PCLog(@"Final Crossing is %d %d", crossingCount, (crossingCount % 2 == 1));
    return (crossingCount % 2 == 1);
}

// 남반구에서는 안되는 로직?
// http://rosettacode.org/wiki/Ray-casting_algorithm

- (BOOL)rayCrossesFromPoint:(DZonePoint *)fromPoint
                     sPoint:(DZonePoint *)sPoint
                     ePoint:(DZonePoint *)ePoint
{
    CLLocationDegrees fromX = [fromPoint.longitude doubleValue];
    CLLocationDegrees fromY = [fromPoint.latitude doubleValue];
    CLLocationDegrees sX = [sPoint.longitude doubleValue];
    CLLocationDegrees sY = [sPoint.latitude doubleValue];
    CLLocationDegrees eX = [ePoint.longitude doubleValue];
    CLLocationDegrees eY = [ePoint.latitude doubleValue];
    
    if(sY > eY){
        // Swapping
        PCLog(@"Swapping");
        sX = [ePoint.longitude doubleValue];
        sY = [ePoint.latitude doubleValue];
        eX = [sPoint.longitude doubleValue];
        eY = [sPoint.latitude doubleValue];
    }
    
    if(fromX < 0) fromX += 360;
    if(sX < 0) sX += 360;
    if(eX < 0) eX += 360;
    
    PCLog(@"p:%f,%f s:%f,%f e:%f,%f", fromX, fromY, sX, sY, eX, eY);
    
    if (fromY == sY || fromY == eY) fromY += 0.00000001;
    
    if ((fromY < sY || fromY > eY) || (fromX > MAX(sX, eX))){
        return NO;
    }
    
    if (fromX < MIN(sX, eX)){
        return YES;
    }
    
    double red = (sX != eX) ? ((eY - sY) / (eX - sX)) : DBL_MAX;
    double blue = (sX != fromX) ? ((fromY - sY) / (fromX - sX)) : DBL_MAX;
    
    return (blue >= red);
}


- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ %@, (%ld)", [super description], self.polygonPoints, (long)self.priority];
}


@end
