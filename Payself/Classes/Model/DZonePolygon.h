//
//  DZonePolygon.h
//  RushOrder
//
//  Created by Conan on 7/1/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "PCModel.h"

@interface DZonePoint : PCModel
@property (strong, nonatomic) NSNumber *latitude;
@property (strong, nonatomic) NSNumber *longitude;
@end

// ==============================================

@interface DZonePolygon : PCModel

@property (copy, nonatomic) NSString *label;
@property (nonatomic) PCCurrency feeAmount;
@property (strong, nonatomic) NSMutableArray *polygonPoints;
@property (nonatomic) NSInteger priority;

- (id)initWithDictionary:(NSDictionary *)dict;

- (BOOL)isContainLocation:(CLLocation *)location;

@end