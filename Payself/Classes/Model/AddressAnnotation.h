//
//  AddressAnnotation.h
//  RushOrder
//
//  Created by Conan on 10/29/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressAnnotation : NSObject<MKAnnotation>
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@end

