//
//  Order.m
//  RushOrder
//
//  Created by Conan on 2/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "Order.h"
#import "Payment.h"
#import "JSONKit.h"
#import "CustomerInfo.h"
#import "MenuOptionGroup.h"
#import "MenuOption.h"
#import "MenuManager.h"
#if RUSHORDER_CUSTOMER
#import "PCCredentialService.h"
#endif

#if RUSHORDER_CUSTOMER
#import "RemoteDataManager.h"
#endif

#define INSERT_QRY @"insert into 'Order'(\
orderNo\
,subTotal\
,subTotalTaxable\
,taxes\
,tips\
,merchantName\
,merchantDesc\
,merchantImageURLString\
,merchantLogoURLString\
,latitude\
,longitude\
,merchantNo\
,tableNumber\
,rejectReason\
,customerRequest\
,address1\
,address2\
,city\
,state\
,zip\
,receiverName\
,phoneNumber\
,deliveryFee\
,roService\
,roServiceFee\
,takenLongitude\
,takenLatitude\
,orderDate\
,createdDate\
,orderType) \
values \
(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

#define UPDATE_QRY @"update 'Order' set \
subTotal = ?\
,subTotalTaxable = ?\
,taxes = ?\
,tips = ?\
,merchantName = ?\
,merchantDesc = ?\
,merchantImageURLString = ?\
,merchantLogoURLString = ?\
,latitude = ?\
,longitude = ?\
,merchantNo = ?\
,tableNumber = ?\
,rejectReason = ?\
,customerRequest = ?\
,address1 = ?\
,address2 = ?\
,city = ?\
,state = ?\
,zip = ?\
,receiverName = ?\
,phoneNumber = ?\
,deliveryFee = ?\
,roService = ?\
,roServiceFee = ?\
,takenLongitude = ?\
,takenLatitude = ?\
,orderDate = ?\
,createdDate = ?\
,orderType = ?\
where orderNo = ?"

@implementation Order

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if(self != nil){
        [self updateWithDictionary:dict
                          merchant:nil];
    }
    return self;
}


- (id)initWithDictionary:(NSDictionary *)dict
                merchant:(Merchant *)merchant
{
    self = [super init];
    if(self != nil){
        [self updateWithDictionary:dict
                          merchant:merchant];
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary *)dict
                merchant:(Merchant *)merchant
                 menuPan:(NSArray *)menuPan
{
    self = [super init];
    if(self != nil){
        [self updateWithDictionary:dict
                          merchant:merchant
                           menuPan:menuPan];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    Order *copy = [[[self class] alloc] init];
    
    if (copy) {
        copy.orderNo = self.orderNo;
        copy.tableNo = self.tableNo;
        copy.subTotal = self.subTotal;
        copy.subTotalTaxable = self.subTotalTaxable;
        copy.taxes = self.taxes;
        copy.tips = self.tips;
        copy.merchantName = [self.merchantName copyWithZone:zone];
        copy.merchantDesc = [self.merchantDesc copyWithZone:zone];
        copy.merchantImageURLString = [self.merchantImageURLString copyWithZone:zone];
        copy.merchantLogoURLString = [self.merchantLogoURLString copyWithZone:zone];
        copy.latitude = self.latitude;
        copy.longitude = self.longitude;
        copy.merchantNo = self.merchantNo;
        copy.tableNumber = self.tableNumber;
        copy.customerRequest = self.customerRequest;
        copy.address1 = self.address1;
        copy.address2 = self.address2;
        copy.city = self.city;
        copy.state = self.state;
        copy.zip = self.zip;
        copy.phoneNumber = self.phoneNumber;
        copy.receiverName = self.receiverName;
        copy.deliveryFee = self.deliveryFee;
        copy.roService = self.isRoService;
        copy.roServiceFee = self.roServiceFee;
        copy.takenCoordinate = self.takenCoordinate;
        copy.rejectReason = self.rejectReason;
        copy.needToAdjustment = self.needToAdjustment;
        copy.paperReceipt = self.paperReceipt;
        copy.payAmounts = self.payAmounts;
        copy.creditAmounts = self.creditAmounts;
        copy.cardAmounts = self.cardAmounts;
        copy.totalPaidDeliveryFeeAmounts = self.totalPaidDeliveryFeeAmounts;
        copy.totalPaidRoServiceFeeAmounts = self.totalPaidRoServiceFeeAmounts;
        copy.rewardedAmount = self.rewardedAmount;
        copy.lastAccessedTime = [self.lastAccessedTime copyWithZone:zone];
        copy.orderDate = self.orderDate;
        copy.createdDate = self.createdDate;
        copy.orderType = self.orderType;
        copy.updatedDate = self.updatedDate;
        copy.expectedDate = self.expectedDate;
        copy.expectedPreparingDate = self.expectedPreparingDate;
        copy.preparedDate = self.preparedDate;
        copy.acceptedDate = self.acceptedDate;
        copy.status = self.status;
        copy.bfStatus = self.status;
        copy.staffName = self.staffName;
        copy.staffNo = self.staffNo;
        copy.customerClose = self.isCustomerClose;
        copy.twoStepProcess = self.isTwoStepProcess;
    }
    
    return copy;
}

- (void)updateWithDictionary:(NSDictionary *)dict
                    merchant:(Merchant *)merchant
{
    [self updateWithDictionary:dict
                      merchant:merchant
                       menuPan:nil
                  withBfStatus:YES];
}

- (void)updateWithDictionary:(NSDictionary *)dict
                    merchant:(Merchant *)merchant
                     menuPan:(NSArray *)menuPan
{
    [self updateWithDictionary:dict
                      merchant:merchant
                       menuPan:menuPan
                  withBfStatus:YES];
}

- (void)updateWithDictionary:(NSDictionary *)dict
                    merchant:(Merchant *)merchant
                     menuPan:(NSArray *)menuPan
                withBfStatus:(BOOL)withBfStatus
{
    Merchant *aMerchant = nil;
    
    if(merchant == nil){
        NSDictionary *merchantDict = [dict objectForKey:@"merchant"];
        if(merchantDict != nil){
            aMerchant = [[Merchant alloc] initWithDictionary:merchantDict];
#if RUSHORDER_CUSTOMER
            aMerchant.favorite = [REMOTE isFavoriteMerchant:aMerchant];
#endif
            self.merchant = aMerchant;
        }
    } else {
        aMerchant = merchant;
    }
    
    if(aMerchant != nil){
        [self updateWithMerchant:aMerchant];
    }
    
    if(merchant){
        
    }
    
    NSArray *payments = [dict objectForKey:@"payments"];
    
    if(payments != nil){
        NSMutableArray *paymentArray = [NSMutableArray array];
        for(NSDictionary *paymentDict in payments){
            if([paymentDict isKindOfClass:[NSDictionary class]]){
                Payment *payment = [[Payment alloc] initWithDictionary:paymentDict];
                [paymentArray addObject:payment];
            }
        }
        if([paymentArray count] > 0){
            self.paymentList = [NSArray arrayWithArray:paymentArray];
        }
    }
    
    if(dict != nil){
        self.orderNo = [[dict objectForKey:@"id"] longLongValue];
        self.subTotal = [dict currencyForKey:@"sub_total_amount"];
        
        if([dict objectForKey:@"sub_total_amount_taxable"] == nil){
            self.subTotalTaxable = [dict currencyForKey:@"sub_total_amount"];
        } else {
            self.subTotalTaxable = [dict currencyForKey:@"sub_total_amount_taxable"];
        }
        
        self.taxes = [dict currencyForKey:@"tax_amount"];
        self.tableNumber = [dict objectForKey:@"table_label"];
        self.customerRequest = [dict objectForKey:@"customer_request"];
        self.address1 = [dict objectForKey:@"delivery_address_street_1"];
        self.address2 = [dict objectForKey:@"delivery_address_street_2"];
        self.city = [dict objectForKey:@"delivery_address_city"];
        self.state = [dict objectForKey:@"delivery_address_state"];
        self.zip = [dict objectForKey:@"delivery_address_zipcode"];
        self.phoneNumber = [dict objectForKey:@"delivery_phone_number"];
        self.receiverName = [dict objectForKey:@"delivery_customer_name"];
        self.deliveryFee = [dict currencyForKey:@"delivery_fee_amount"];
        self.roServiceFee = [dict currencyForKey:@"ro_service_fee_amount"];
        self.roService = [dict boolForKey:@"is_ro_service"];
        self.rejectReason = [dict objectForKey:@"reject_reason"];
        self.needToAdjustment = [dict boolForKey:@"need_to_adjustment"];
        self.tableNo = [dict serialForKey:@"table_id"];
        self.createdDate = [[dict objectForKey:@"created_at"] date];
        self.orderDate = [[dict objectForKey:@"ordered_at"] date];
        self.updatedDate = [[dict objectForKey:@"updated_at"] date];
        self.expectedDate = [[dict objectForKey:@"expected_at"] date];
        self.expectedPreparingDate = [[dict objectForKey:@"expected_preparing_at"] date];
        self.acceptedDate = [[dict objectForKey:@"accepted_at"] date];
        self.preparedDate = [[dict objectForKey:@"prepared_at"] date];
        self.completeReasonJson = [dict objectForKey:@"complete_reason"];
        self.pickupAfter = [dict integerForKey:@"pickup_after"]; //minute
        self.staffName = [dict objectForKey:@"staff_name"];
        self.staffNo = [dict serialForKey:@"staff_id"];
        self.twoStepProcess = [dict boolForKey:@"two_step_process"];
        
        
        if([dict objectForKey:@"total_paid_amount"] != nil){
            self.payAmounts = [dict currencyForKey:@"total_paid_amount"];
        }
        
        if([dict objectForKey:@"total_paid_credit_amount"] != nil){
            self.creditAmounts = [dict currencyForKey:@"total_paid_credit_amount"];
        }
        
        self.cardAmounts = self.payAmounts - self.creditAmounts;
        
        if([dict objectForKey:@"total_paid_delivery_fee_amount"] != nil){
            self.totalPaidDeliveryFeeAmounts = [dict currencyForKey:@"total_paid_delivery_fee_amount"];
        }
        
        if([dict objectForKey:@"total_paid_ro_service_fee_amount"] != nil){
            self.totalPaidRoServiceFeeAmounts = [dict currencyForKey:@"total_paid_ro_service_fee_amount"];
        }
        
        if([dict objectForKey:@"total_discount_amount"] != nil){
            self.discountedAmount = [dict currencyForKey:@"total_discount_amount"];
        }
        
        if([dict objectForKey:@"total_discount_amount_by_ro"] != nil){
            self.roDiscountedAmount = [dict currencyForKey:@"total_discount_amount_by_ro"];
        }
        
        if([dict objectForKey:@"total_discount_amount_by_merchant"] != nil){
            self.meDiscountedAmount = [dict currencyForKey:@"total_discount_amount_by_merchant"];
        }
        
        // !!!: Bill 발행으로 결제하면서 나누어 지불하는 경우(스플릿)
        // tips 값은 초기 tip값만 DB에 저장되고 업데이트 되지 않으므로 사용에 주의 할 것.
        // 현재는 참고용으로만 사용하고 payment의 tip을 사용하므로 현상 유지
        // -> 안전을 위해 수정될 필요가 있어보임
        if([dict objectForKey:@"total_tip_amount"] != nil){
            self.tips = [dict currencyForKey:@"total_tip_amount"];
        }
        
        if([dict objectForKey:@"total_rewarded_credit"] != nil){
            self.rewardedAmount = [dict currencyForKey:@"total_rewarded_credit"];
        }
        
        self.testMode = [[dict objectForKey:@"mode"] isEqualToString:@"demo"];
        
        self.lastAccessedTime = [[dict objectForKey:@"updated_at"] date];
        
        self.orderType = [[dict objectForKey:@"order_type"] orderType];
        
        if(withBfStatus)
            self.bfStatus = self.status;
        
        if([[dict objectForKey:@"status"] isEqual:@"draft"]){
            self.status = OrderStatusDraft;
        } else if([[dict objectForKey:@"status"] isEqual:@"paid"]){
            self.status = OrderStatusPaid;
        } else if([[dict objectForKey:@"status"] isEqual:@"ready"]){
            self.status = OrderStatusReady;
        } else if([[dict objectForKey:@"status"] isEqual:@"confirmed"]){
            if(!self.isTicketBase){
                if(self.total <= (self.netPayAmounts + self.discountedAmount)){
                    self.status = OrderStatusCompleted;
                } else {
                    self.status = OrderStatusConfirmed;
                }
            } else {
                self.status = OrderStatusConfirmed;
            }
        } else if([[dict objectForKey:@"status"] isEqual:@"completed"]){
            self.status = OrderStatusFixed;
        } else if([[dict objectForKey:@"status"] isEqual:@"canceled"]){
            self.status = OrderStatusCanceled;
        } else if([[dict objectForKey:@"status"] isEqual:@"rejected"]){
            self.status = OrderStatusDeclined;
        } else if([[dict objectForKey:@"status"] isEqual:@"accepted"]){
            self.status = OrderStatusAccepted;
        } else if([[dict objectForKey:@"status"] isEqual:@"prepared"]){
            self.status = OrderStatusPrepared;
        } else if([[dict objectForKey:@"status"] isEqual:@"refused"]){
            self.status = OrderStatusRefused;
        } else {
            self.status = OrderStatusCanceled;
        }
        
        NSArray *lineItemArray = [dict objectForKey:@"line_items"];
        
        NSMutableArray *lineItems = [NSMutableArray array];
        
        NSMutableString *lineItemNames = [NSMutableString string];
        
        //Remove all line items in payment before newly Linking To Payment
        for(Payment *payment in self.paymentList){
            [payment.lineItems removeAllObjects];
        }
        
        for(NSDictionary *lineItemDict in lineItemArray){
            LineItem *lineItem = [[LineItem alloc] initWithDictionary:lineItemDict
                                                              menuPan:menuPan];
            lineItem.merchantNo = self.merchantNo;
            [lineItems addObject:lineItem];
            if([lineItemNames length] > 0){
                [lineItemNames appendString:@", "];
            }
            if([lineItem.menuName length] > 0){
                [lineItemNames appendString:lineItem.menuName];
            }
            if(lineItem.quantity > 1){
                [lineItemNames appendFormat:@"(%ld)", (long)lineItem.quantity];
            }
            
            //Link To Payment too
            for(Payment *payment in self.paymentList){                
                if(lineItem.paymentNo == payment.paymentNo){
                    [payment.lineItems addObject:lineItem];
                    break;
                }
            }
        }
        
        for(Payment *payment in self.paymentList){
            [payment summation];
        }
        
        self.lineItemNames = lineItemNames;
        
        self.lineItems = lineItems;
        
        NSArray *customers = [dict objectForKey:@"customer_info"];
        
        if(customers != nil){
            NSMutableArray *customerInfos = [NSMutableArray array];
            for(NSDictionary *customer in customers){
                CustomerInfo *customerInfo = [[CustomerInfo alloc] initWithDictionary:customer];
                [customerInfos addObject:customerInfo];
            }
            
            self.customers = customerInfos;
        }
        
        ///////////////////////////////////////////////////////////////
        NSString *photoStr = [dict objectForKey:@"customer_photo"];
        self.customerPhotoURL = [NSURL URLWithString:photoStr];
        ///////////////////////////////////////////////////////////////
        
//        NSDictionary *photoDict = [[dict objectForKey:@"customer_photo"] objectForKey:@"photo"];
//        
//        if(photoDict != nil){
//            NSString *photoURLString = [photoDict objectForKey:@"url"];
//            
//            if(photoURLString != nil && ![photoURLString hasPrefix:@"http"]){
//                
//    #ifdef SSL_ENABLED
//                NSString *urlProtocol = @"https://";
//    #else
//                NSString *urlProtocol = @"http://";
//    #endif
//                NSString *serverAPIURL = [urlProtocol stringByAppendingString:SERVER_URL_ADDRESS];
//                photoURLString = [serverAPIURL stringByAppendingString:photoURLString];
//                
//            }
//            
//            if([photoURLString length] > 0){
//                self.customerPhotoURL = [NSURL URLWithString:photoURLString];
//            } else {
//                self.customerPhotoURL = nil;
//            }
//        }
        
    } else {
        if(withBfStatus)
            self.bfStatus = self.status;
        self.status = OrderStatusUnknown;
    }
    
    [self summation];
}

- (void)summation
{
    [self summationWithForce:NO];
}

- (void)summationWithForce:(BOOL)force
{
    if(self.lineItems == nil && !force){
        // !!!: line items 없이 amount, taxAmount, quantities 값이 지정되어 있는 경우도 있기 때문에
        // 일반적인 경우는 amount, taxAmount, quantities를 보존하기위해 값을 계산하지 않고 그냥 리턴한다.
        PCWarning(@"Summation needs line items - Orders");
        return;
    }
    
//    PCCurrency subTotal = 0;
    NSInteger count = 0;
    
//    NSMutableString *lineItemNames = [NSMutableString string];
    
    for(LineItem *lineItem in self.lineItems){
        
        count += lineItem.quantity;
        
//        subTotal += lineItem.lineAmount;
//        
//        if([lineItemNames length] > 0){
//            [lineItemNames appendString:@", "];
//        }
//        [lineItemNames appendString:lineItem.menuName];
//        if(lineItem.quantity > 1){
//            [lineItemNames appendFormat:@"(%ld)", (long)lineItem.quantity];
//        }
    }
    
//    self.lineItemNames = lineItemNames;
//    
//    self.subTotal = subTotal;
//    self.taxes = roundf(((subTotal * self.merchant.taxRate) / 100.0f));
    self.quantities = count;
}

- (void)updateWithMerchant:(Merchant *)merchant
{
    if(merchant != nil){
        self.merchantNo = merchant.merchantNo;
        self.merchantName = merchant.name;
        self.merchantDesc = merchant.displayAddress;
        self.merchantImageURLString = merchant.imageURL.absoluteString;
        self.merchantLogoURLString = merchant.logoURL.absoluteString;
        self.latitude = merchant.coordinate.latitude;
        self.longitude = merchant.coordinate.longitude;
    }
}

- (void)sum:(Order *)order
{
    self.subTotal += order.subTotal;
    self.subTotalTaxable += order.subTotalTaxable;
    self.taxes += order.taxes;
    self.payAmounts += order.payAmounts;
    self.creditAmounts += order.creditAmounts;
    self.cardAmounts += order.cardAmounts;
    self.totalPaidDeliveryFeeAmounts += order.totalPaidDeliveryFeeAmounts;
    self.totalPaidRoServiceFeeAmounts += order.totalPaidRoServiceFeeAmounts;
    self.rewardedAmount += order.rewardedAmount;
    self.tips += order.tips;
}


- (void)updateSumWithPayments:(NSArray *)payments
{
    PCCurrency tipSum = 0;
    PCCurrency paymentSum = 0;
    
    for(Payment *payment in payments){
        if(payment.status == PaymentStatusSuccess){
            tipSum += payment.tipAmount;
            if(payment.payAmount == 0){
                paymentSum += payment.actualAmount;
            } else {
                paymentSum += payment.payAmount;
            }
        }
    }
    
    self.tips = tipSum;
    self.payAmounts = paymentSum;
    
    // !!!Don't add this code - bfStatus decided in updateWithDic...
    // self.bfStatus = self.status;
    
    if(self.status == OrderStatusConfirmed
       || self.status == OrderStatusCompleted){
        if(!self.isTicketBase){
            if(self.total <= (self.netPayAmounts + self.discountedAmount)){
                self.status = OrderStatusCompleted;
            } else {
                self.status = OrderStatusConfirmed;
            }
        } else {
            self.status = OrderStatusConfirmed;
        }
    }
}

#pragma mark - PCModelProtocol
- (id)initWithFMResult:(FMResultSet *)result
{
    self = [super init];
    if(self != nil){
        self.orderNo = [result longLongIntForColumn:@"orderNo"];
        self.subTotal = [result longLongIntForColumn:@"subTotal"];
        self.subTotalTaxable = [result longLongIntForColumn:@"subTotalTaxable"];
        self.taxes = [result doubleForColumn:@"taxes"];
        // !!!: Bill 발행으로 결제하면서 나누어 지불하는 경우(스플릿)
        // tips 값은 초기 tip값만 DB에 저장되고 업데이트 되지 않으므로 사용에 주의 할 것.
        // 현재는 참고용으로만 사용하고 payment의 tip을 사용하므로 현상 유지
        // -> 안전을 위해 수정될 필요가 있어보임
        self.tips = [result doubleForColumn:@"tips"];
        self.merchantName = [result stringForColumn:@"merchantName"];
        self.merchantDesc = [result stringForColumn:@"merchantDesc"];
        self.merchantImageURLString = [result stringForColumn:@"merchantImageURLString"];
        self.merchantLogoURLString = [result stringForColumn:@"merchantLogoURLString"];
        self.latitude = [result doubleForColumn:@"latitude"];
        self.longitude = [result doubleForColumn:@"longitude"];
        self.merchantNo = [result longLongIntForColumn:@"merchantNo"];
        self.tableNumber = [result stringForColumn:@"tableNumber"];
        self.customerRequest = [result stringForColumn:@"customerRequest"];
        self.address1 = [result stringForColumn:@"address1"];
        self.address2 = [result stringForColumn:@"address2"];
        self.city = [result stringForColumn:@"city"];
        self.state = [result stringForColumn:@"state"];
        self.zip = [result stringForColumn:@"zip"];
        self.receiverName = [result stringForColumn:@"receiverName"];
        self.phoneNumber = [result stringForColumn:@"phoneNumber"];
        self.deliveryFee = [result intForColumn:@"deliveryFee"];
        self.roServiceFee = [result intForColumn:@"roServiceFee"];
        self.roService = [result boolForColumn:@"roService"];
        CLLocationDegrees takenLatitude = [result doubleForColumn:@"takenLatitude"];
        CLLocationDegrees takenLongitude = [result doubleForColumn:@"takenLongitude"];
        self.takenCoordinate = CLLocationCoordinate2DMake(takenLatitude, takenLongitude);
        self.rejectReason = [result stringForColumn:@"rejectReason"];
        self.orderDate = [result dateForColumn:@"orderDate"];
        self.createdDate = [result dateForColumn:@"createdDate"];
        self.orderType = [result intForColumn:@"orderType"];
    }
    return self;
}


- (BOOL)insert
{
    if(self.isTestMode){
        PCLog(@"Order not saved because this Order is demo");
        return NO;
    }
    
    if([self openDB]){
        
        return [DB.db executeUpdate:INSERT_QRY,
                [NSNumber numberWithLongLong:self.orderNo],
                [NSNumber numberWithCurrency:self.subTotal],
                [NSNumber numberWithCurrency:self.subTotalTaxable],
                [NSNumber numberWithCurrency:self.taxes],
                [NSNumber numberWithCurrency:self.tips],
                self.merchantName,
                self.merchantDesc,
                self.merchantImageURLString,
                self.merchantLogoURLString,
                [NSNumber numberWithDouble:self.latitude],
                [NSNumber numberWithDouble:self.longitude],
                [NSNumber numberWithLongLong:self.merchantNo],
                self.tableNumber,
                self.rejectReason,
                self.customerRequest,
                self.address1,
                self.address2,
                self.city,
                self.state,
                self.zip,
                self.receiverName,
                self.phoneNumber,
                [NSNumber numberWithCurrency:self.deliveryFee],
                [NSString sqlStringWithBool:self.isRoService],
                [NSNumber numberWithCurrency:self.roServiceFee],
                [NSNumber numberWithDouble:self.takenCoordinate.longitude],
                [NSNumber numberWithDouble:self.takenCoordinate.latitude],
                self.orderDate,
                self.createdDate,
                [NSNumber numberWithInteger:self.orderType]];
    } else {
        PCError(@"Order insert error");
        return NO;
    }
}

- (BOOL)update
{
    if(self.isTestMode){
        return NO;
    }
    
    if([self openDB]){
        return [DB.db executeUpdate:UPDATE_QRY,
                [NSNumber numberWithCurrency:self.subTotal],
                [NSNumber numberWithCurrency:self.subTotalTaxable],
                [NSNumber numberWithCurrency:self.taxes],
                [NSNumber numberWithCurrency:self.tips],
                self.merchantName,
                self.merchantDesc,
                self.merchantImageURLString,
                self.merchantLogoURLString,
                [NSNumber numberWithDouble:self.latitude],
                [NSNumber numberWithDouble:self.longitude],
                [NSNumber numberWithLongLong:self.merchantNo],
                self.tableNumber,
                self.rejectReason,
                self.customerRequest,
                self.address1,
                self.address2,
                self.city,
                self.state,
                self.zip,
                self.receiverName,
                self.phoneNumber,
                [NSNumber numberWithCurrency:self.deliveryFee],
                [NSString sqlStringWithBool:self.isRoService],
                [NSNumber numberWithCurrency:self.roServiceFee],
                [NSNumber numberWithDouble:self.takenCoordinate.longitude],
                [NSNumber numberWithDouble:self.takenCoordinate.latitude],
                self.orderDate,
                self.createdDate,
                [NSNumber numberWithInteger:self.orderType],
                [NSNumber numberWithLongLong:self.orderNo]];
        
        
    } else {
        PCError(@"Order update error");
        return NO;
    }
}

- (BOOL)dateChanged:(NSDate *)date
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *orderDateComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                                                        fromDate:self.orderDate];
    
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                                                   fromDate:date];
    if(orderDateComponents.year == dateComponents.year
       && orderDateComponents.month == dateComponents.month
       && orderDateComponents.day == dateComponents.day){
        return NO;
    } else {
        return YES;
    }
}

- (BOOL)isCompletelyPaid
{
    if(self.grandTotal == 0){
        return self.payAmounts > 0;
    } else {
        return (self.total <= self.netPayAmounts);
    }
}

- (BOOL)weekChanged:(NSDate *)date
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *orderDateComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitWeekOfYear
                                                        fromDate:self.orderDate];
    
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitWeekOfYear
                                                   fromDate:date];
    if(orderDateComponents.year == dateComponents.year
       && orderDateComponents.weekOfYear == dateComponents.weekOfYear){
        return NO;
    } else {
        return YES;
    }
}

- (BOOL)monthChanged:(NSDate *)date
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *orderDateComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth
                                                        fromDate:self.orderDate];
    
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth
                                                   fromDate:date];
    if(orderDateComponents.year == dateComponents.year
       && orderDateComponents.month == dateComponents.month){
        return NO;
    } else {
        return YES;
    }
}

#pragma mark - Readonly

- (NSString *)typeString
{
    return [Order typeStringForOrderType:self.orderType];
}

+ (NSString *)typeStringForOrderType:(OrderType)orderType
{
    switch(orderType){
        case OrderTypeBill:
        case OrderTypeCart:
        case OrderTypeOrderOnly:
        case OrderTypePickup:
            return @"Dine-in";
            break;
        case OrderTypeTakeout:
            return @"Take-out";
            break;
        case OrderTypeDelivery:
            return @"Delivery";
            break;
        default:
            return @"Unknown";
    }
}

- (UIImage *)orderTypeIcon
{
    switch(self.orderType){
        case OrderTypeBill:
        case OrderTypeCart:
        case OrderTypeOrderOnly:
        case OrderTypePickup:
            return [UIImage imageNamed:@"button_icon_dinein"];
            break;
        case OrderTypeTakeout:
            return [UIImage imageNamed:@"button_icon_takeout"];
            break;
        case OrderTypeDelivery:
            return [UIImage imageNamed:@"button_icon_delivery"];
            break;
        default:
            return [UIImage imageNamed:@""];
    }
}

- (NSString *)statusString
{
    switch(self.status){
        case OrderStatusDraft:
            return @"draft";
            break;
        case OrderStatusPaid:
            return @"paid";
        case OrderStatusConfirmed:
        case OrderStatusCompleted:
            return @"confirmed";
            break;
        case OrderStatusAccepted:
            return @"accepted";
            break;
        case OrderStatusReady:
            return @"ready";
            break;
        case OrderStatusPrepared:
            return @"prepared";
            break;
        case OrderStatusFixed:
            return @"completed";
            break;
        case OrderStatusCanceled:
            return @"canceled";
            break;
        case OrderStatusDeclined:
            return @"rejected";
            break;
        case OrderStatusRefused:
            return @"refused";
            break;
        default:
            return @"";
    }
}


// Used in merchant app
- (NSString *)hrStatusPickupString
{
    switch(self.status){
        case OrderStatusDraft:
            return NSLocalizedString(@"Ordering", nil);
            break;
        case OrderStatusPaid:
            return NSLocalizedString(@"New Order", nil);
            break;
        case OrderStatusConfirmed:
        case OrderStatusAccepted:
            return NSLocalizedString(@"Confirmed", nil);
            break;
        case OrderStatusReady:
        case OrderStatusPrepared:
            if(self.isDelivery){
                return NSLocalizedString(@"Delivering", nil);
            } else if(self.isTakeout || self.merchant.servicedBy != ServicedByStaff){
                return NSLocalizedString(@"Waiting for Pickup", nil);
            } else {
                return NSLocalizedString(@"Serving", nil);
            }
            break;
        case OrderStatusFixed:
            return NSLocalizedString(@"Completed", nil);
            break;
        case OrderStatusCanceled:
            return NSLocalizedString(@"Removed", nil);
            break;
        case OrderStatusDeclined:
        case OrderStatusRefused:
            return NSLocalizedString(@"Declined", nil);
            break;
        default:
            return @"";
    }
}
// Used in merchant app
- (NSString *)hrCustStatusPickupString
{
    switch(self.status){
        case OrderStatusDraft:
            return NSLocalizedString(@"Ordering", nil);
            break;
        case OrderStatusPaid:
            return NSLocalizedString(@"Waiting For Confirmation", nil);
            break;
        case OrderStatusConfirmed:
        case OrderStatusAccepted:
            return NSLocalizedString(@"Preparing Your Order", nil);
            break;
        case OrderStatusReady:
        case OrderStatusPrepared:
            if(self.isDelivery){
                return NSLocalizedString(@"Delivering", nil);
            } else if(self.isTakeout || self.merchant.servicedBy != ServicedByStaff){
                return NSLocalizedString(@"Ready For Pickup", nil);
            } else {
                return NSLocalizedString(@"Serving", nil);
            }
            break;
        case OrderStatusDeclined:
        case OrderStatusRefused:
            return NSLocalizedString(@"Declined", nil);
            break;
        case OrderStatusFixed:
        case OrderStatusCanceled:
            return @"";
            
            break;
        default:
            return @"";
    }
}


#if RUSHORDER_CUSTOMER
# pragma mark - CRUD for Order (Remote Only)
- (void)addMenuItem:(MenuItem *)menuItem
           quantity:(NSInteger)quantity
specialInstructions:(NSString *)specialInstructions
            success:(void (^)())successBlock
            failure:(void (^)())failureBlock
{
    if(!self.isEditable){
        failureBlock();
        return;
    }
    
    LineItem *lineItem = nil;
    
    BOOL existLineItem = NO;
    
    for(LineItem *obj in self.lineItems){
        if(obj.menuItemNo == menuItem.menuItemNo){
            if(obj.menuItem == nil){
                obj.menuItem = menuItem;
                [obj configureChoiceAndOptions];
            }
            
            // Don't use menuChoiceId of LineItem -> that value is from the server. If local data it cause serious problem
            if(obj.selectedPrice.menuPriceNo == menuItem.selectedMenuPrice.menuPriceNo){
                
                NSComparator optionComparator = ^NSComparisonResult(MenuOption *obj1, MenuOption *obj2){
                    if(obj1.menuOptionNo > obj2.menuOptionNo){
                        return NSOrderedDescending;
                    } else if(obj1.menuOptionNo < obj2.menuOptionNo){
                        return NSOrderedAscending;
                    } else {
                        return NSOrderedSame;
                    }
                };
                
                NSArray *newSelectedOptions = [menuItem.selectedOptions sortedArrayUsingComparator:optionComparator];
                NSArray *selectedOptions = [obj.selectedOptions sortedArrayUsingComparator:optionComparator];
                
                BOOL matchedOption = YES;
                
                if([newSelectedOptions count] == [selectedOptions count]){
                    NSInteger i = 0;
                    for( i = 0 ; i < [newSelectedOptions count] ; i++){
                        MenuOption *newOption = [newSelectedOptions objectAtIndex:i];
                        MenuOption *oldOption = [selectedOptions objectAtIndex:i];
                        
                        if(newOption.menuOptionNo != oldOption.menuOptionNo){
                            matchedOption = NO;
                            break;
                        }
                    }
                } else {
                    matchedOption = NO;
                }
                
                if(matchedOption){
                    if((obj.specialInstruction == nil && specialInstructions == nil)
                       || [obj.specialInstruction isEqualToString:specialInstructions]){
                        lineItem = obj;
                        existLineItem = YES;
                        break;
                    }
                }
            } else {
                
            }
        } else {
        }
    }
    
    if(!existLineItem){
        lineItem = [[LineItem alloc] init];
        lineItem.deviceToken = [NSUserDefaults standardUserDefaults].deviceToken;
        lineItem.udid = CRED.udid;
        lineItem.quantity = 0;
    }
    
    if(specialInstructions != nil){
//        if([lineItem.specialInstruction length] > 0){
//            lineItem.specialInstruction =
//            [lineItem.specialInstruction stringByAppendingString:
//             [@"\n---------------------------\n" stringByAppendingString:specialInstructions]];
//        } else {
            lineItem.specialInstruction = specialInstructions;
//        }
    }
    
    lineItem.menuItem = menuItem;
    [lineItem updatePriceAndOptions];
    
    if(quantity == 0)
        lineItem.quantity += 1;
    else
        lineItem.quantity += quantity;
    
    [lineItem applyLiveAmount];
    
    if(!existLineItem){
        
        RequestResult result = RRFail;
        
        CompletionBlock addCompletionBlock = ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
            if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                switch(response.errorCode){
                    case ResponseSuccess:{
                        
                        NSDictionary *orderDict = [response data];
                        
                        if([orderDict objectForKey:@"id"] != nil){
                            [self updateWithDictionary:orderDict
                                              merchant:self.merchant
                                               menuPan:MENUPAN.wholeSerialMenuList];
                             
                        }
                        
                        [self summation];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:LineItemChangedNotification
                                                                            object:self
                                                                          userInfo:nil];
                        successBlock();
                        break;
                    }
                    case ResponseErrorCartUnchangable:
                        [UIAlertView alertWithTitle:NSLocalizedString(@"Order list is not editable", nil)
                                            message:NSLocalizedString(@"Order list has been placed. To add/modify/remove orders, you have to postpone the confirmation of your order by \"Change Placed Order\" button", nil)
                                           delegate:self
                                                tag:100];
                        [[NSNotificationCenter defaultCenter] postNotificationName:CartUnsolicitChangedNotification
                                                                            object:self
                                                                          userInfo:nil];
                        failureBlock();
                        break;
                    case ResponseErrorGeneral:{
                        failureBlock();
                        NSString *message = [response objectForKey:@"message"];
                        if([message isKindOfClass:[NSString class]]){
                            [UIAlertView alert:message];
                        }
                        break;
                    }
                    default:
                    {
                        failureBlock();
                        NSString *message = [response objectForKey:@"message"];
                        if([message isKindOfClass:[NSString class]]){
                            [UIAlertView alert:message];
                        } else {
                            [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while adding menu itmes to order list", nil)];
                        }
                    }
                        break;
                }
            } else {
                failureBlock();
                if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                    [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                        object:nil];
                } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                    [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                        ,statusCode]];
                }
            }
            [SVProgressHUD dismiss];
        };
        
        result = [MENU requestAddItemsToOrder:self
                                    lineItems:[NSArray arrayWithObject:lineItem]
                                  deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                                    authToken:CRED.customerInfo.authToken
                              completionBlock:addCompletionBlock];
        
        switch(result){
            case RRSuccess:
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                break;
            case RRParameterError:
                failureBlock();
                [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                break;
            default:
                break;
        }
        
        
    } else {
        [self updateLineItem:lineItem
                     success:successBlock
                     failure:failureBlock];
    }
}

- (void)updateLineItem:(LineItem *)lineItem
               success:(void (^)())successBlock
               failure:(void (^)())failureBlock
{
    
    [self updateLineItem:lineItem
  overidePriceAndOptions:YES
                 success:successBlock
                 failure:failureBlock];
}

- (void)updateLineItem:(LineItem *)lineItem
overidePriceAndOptions:(BOOL)overidePriceAndOptions
               success:(void (^)())successBlock
               failure:(void (^)())failureBlock
{
    [lineItem applyLiveAmount];
    
    if(overidePriceAndOptions){
        [lineItem updatePriceAndOptions];
    }
    
    
    RequestResult result = RRFail;
    
    CompletionBlock updateCompletionBlock = ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
        if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
            switch(response.errorCode){
                case ResponseSuccess:{
                    
                    NSDictionary *orderDict = [response data];
                    
                    if([orderDict objectForKey:@"id"] != nil){
                        [self updateWithDictionary:orderDict
                                          merchant:self.merchant
                                           menuPan:MENUPAN.wholeSerialMenuList];
                        
                    } else {
                        self.lineItems = nil;
                    }
                    
                    [self summationWithForce:YES];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:LineItemChangedNotification
                                                                        object:self
                                                                      userInfo:nil];
                    successBlock();
                }
                    break;
                case ResponseErrorNoLineItem:
                    [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Cannot find the item to modify. Line item number is %d", nil), lineItem.lineItemNo]];
                    failureBlock();
                    break;
                case ResponseErrorCartUnchangable:
                    [UIAlertView alertWithTitle:NSLocalizedString(@"Order list is not editable", nil)
                                        message:NSLocalizedString(@"Order list has been placed. To add/modify/remove orders, you have to postpone the confirmation of your order by \"Change Placed Order\" button", nil)
                                       delegate:self
                                            tag:100];
                    [[NSNotificationCenter defaultCenter] postNotificationName:CartUnsolicitChangedNotification
                                                                        object:self
                                                                      userInfo:nil];
                    failureBlock();
                    break;
                case ResponseErrorGeneral:{
                    failureBlock();
                    NSString *message = [response objectForKey:@"message"];
                    if([message isKindOfClass:[NSString class]]){
                        [UIAlertView alert:message];
                    }
                    break;
                }
                default:{
                    failureBlock();
                    NSString *message = [response objectForKey:@"message"];
                    if([message isKindOfClass:[NSString class]]){
                        [UIAlertView alert:message];
                    } else {
                        [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while updating line itmes from the cart", nil)];
                    }
                }
                    break;
            }
        } else {
            failureBlock();
            if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                    object:nil];
            } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                    ,statusCode]];
            }
        }
        [SVProgressHUD dismiss];
    };
    
    result = [MENU requestUpdateItemsToOrder:self
                                   lineItems:[NSArray arrayWithObject:lineItem]
                                 deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                             completionBlock:updateCompletionBlock];
    
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            failureBlock();
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)removeLineItem:(LineItem *)lineItem
               success:(void (^)())successBlock
               failure:(void (^)())failureBlock
{
    [self subtractLineItem:lineItem
                        by:lineItem.quantity
                   success:successBlock
                   failure:failureBlock];
}

- (void)subtractLineItem:(LineItem *)lineItem
                 success:(void (^)())successBlock
                 failure:(void (^)())failureBlock
{
    [self subtractLineItem:lineItem
                        by:1
                   success:successBlock
                   failure:failureBlock];
}

- (void)subtractLineItem:(LineItem *)lineItem
                      by:(NSInteger)quantity
                 success:(void (^)())successBlock
                 failure:(void (^)())failureBlock
{
    lineItem.quantity -= quantity;
    
    lineItem.quantity = MAX(lineItem.quantity, 0);
    
    if(lineItem.quantity == 0){
        
        RequestResult result = RRFail;
        
        CompletionBlock deleteCompletionBlock = ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
            
            if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                switch(response.errorCode){
                    case ResponseSuccess:{
                        
                        NSDictionary *orderDict = [response data];
                        
                        if([orderDict objectForKey:@"id"] != nil){
                            [self updateWithDictionary:orderDict
                                              merchant:self.merchant
                                               menuPan:MENUPAN.wholeSerialMenuList];
                            
                        } else {
                            self.lineItems = nil;
                        }
                        
                        [self summationWithForce:YES];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:LineItemChangedNotification
                                                                            object:self
                                                                          userInfo:nil];
                        successBlock(NO);
                    }
                        break;
                    case ResponseErrorNoCart:{
                        [UIAlertView alert:NSLocalizedString(@"This order list does not exist or has been removed.", nil)];
                        self.lineItems = nil;
                        
                        
                    }
                        break;
                    case ResponseErrorNoLineItem:
                        [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Cannot find the item to remove. Line item number is %d", nil), lineItem.lineItemNo]];
                        failureBlock();
                        break;
                    case ResponseErrorCartUnchangable:
                        [UIAlertView alertWithTitle:NSLocalizedString(@"Order list is not editable", nil)
                                            message:NSLocalizedString(@"Order list has been placed. To add/modify/remove orders, you have to postpone the confirmation of your order by \"Change Placed Order\" button", nil)
                                           delegate:self
                                                tag:100];
                        [[NSNotificationCenter defaultCenter] postNotificationName:CartUnsolicitChangedNotification
                                                                            object:self
                                                                          userInfo:nil];
                        failureBlock();
                        break;
                    case ResponseErrorGeneral:{
                        failureBlock();
                        NSString *message = [response objectForKey:@"message"];
                        if([message isKindOfClass:[NSString class]]){
                            [UIAlertView alert:message];
                        }
                        break;
                    }
                    default:{
                        failureBlock();
                        NSString *message = [response objectForKey:@"message"];
                        if([message isKindOfClass:[NSString class]]){
                            [UIAlertView alert:message];
                        } else {
                            [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while deleting line itmes from the cart", nil)];
                        }
                    }
                        break;
                }
            } else {
                failureBlock();
                if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                    [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                        object:nil];
                } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                    [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                        ,statusCode]];
                }
            }
            [SVProgressHUD dismiss];
        };
        
        result = [MENU requestDeleteItemsFromOrder:self
                                         lineItems:[NSArray arrayWithObject:lineItem]
                                       deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                                         authToken:CRED.customerInfo.authToken
                                   completionBlock:deleteCompletionBlock];
        
        switch(result){
            case RRSuccess:
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                break;
            case RRParameterError:
                failureBlock();
                [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                break;
            default:
                break;
        }
        
        
    } else {
        
        if(lineItem.menuItem == nil){
            [lineItem linkMenuItemInMenuList:MENUPAN.wholeSerialMenuList];
        }
        
        [lineItem applyLiveAmount];
        
        [self updateLineItem:lineItem
      overidePriceAndOptions:NO
                     success:successBlock
                     failure:failureBlock];
    }
}


- (void)removeAllFromOrderSuccess:(void (^)(BOOL isOnlyLocal))successBlock
                          failure:(void (^)())failureBlock
{
    NSMutableArray *myOrderList = [NSMutableArray array];
    
    for(LineItem *lineItem in self.lineItems){
        if(lineItem.isMine){
            [myOrderList addObject:lineItem];
        }
    }
    
    RequestResult result = RRFail;
    
    CompletionBlock deleteCompletionBlock = ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
        
        if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
            switch(response.errorCode){
                case ResponseSuccess:{
                    NSDictionary *orderDict = [response data];
                    
                    if([orderDict objectForKey:@"id"] != nil){
                        [self updateWithDictionary:orderDict
                                          merchant:self.merchant
                                           menuPan:MENUPAN.wholeSerialMenuList];
                        
                    } else {
                        self.lineItems = nil;
                    }
                    
                    [self summationWithForce:YES];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:LineItemChangedNotification
                                                                        object:self
                                                                      userInfo:nil];
                    successBlock(NO);
                }
                    break;
                case ResponseErrorNoCart:{
                    [UIAlertView alert:NSLocalizedString(@"This order list does not exist or has been removed", nil)];
                    self.lineItems = nil;
                    
                    
                }
                    break;
                case ResponseErrorNoLineItem:
                    [UIAlertView alert:NSLocalizedString(@"Cannot find the items to remove", nil)];
                    failureBlock();
                    break;
                case ResponseErrorCartUnchangable:
                    [UIAlertView alertWithTitle:NSLocalizedString(@"Order list is not editable", nil)
                                        message:NSLocalizedString(@"Order list has been placed. To add/modify/remove order, you have to postpone the confirmation of your order by \"Change Placed Order\" button", nil)
                                       delegate:self
                                            tag:100];
                    [[NSNotificationCenter defaultCenter] postNotificationName:CartUnsolicitChangedNotification
                                                                        object:self
                                                                      userInfo:nil];
                    failureBlock();
                    break;
                case ResponseErrorGeneral:{
                    failureBlock();
                    NSString *message = [response objectForKey:@"message"];
                    if([message isKindOfClass:[NSString class]]){
                        [UIAlertView alert:message];
                    }
                    break;
                }
                default:{
                    failureBlock();
                    NSString *message = [response objectForKey:@"message"];
                    if([message isKindOfClass:[NSString class]]){
                        [UIAlertView alert:message];
                    } else {
                        [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while deleting line itmes from the cart", nil)];
                    }
                }
                    break;
            }
        } else {
            failureBlock();
            if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                    object:nil];
            } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                    ,statusCode]];
            }
        }
        [SVProgressHUD dismiss];
    };
    
    result = [MENU requestDeleteItemsFromOrder:self
                                     lineItems:myOrderList
                             withCustomerClose:YES
                                   deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                                     authToken:CRED.customerInfo.authToken
                               completionBlock:deleteCompletionBlock];
    
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            failureBlock();
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
    

}

#endif //Payself Customer

- (PCCurrency)total
{
    return self.subTotal + self.taxes + self.deliveryFee + self.roServiceFee;
}

- (PCCurrency)grandTotal
{
    // !!!: Bill 발행으로 결제하면서 나누어 지불하는 경우(스플릿)
    // tips 값은 초기 tip값만 DB에 저장되고 업데이트 되지 않으므로 사용에 주의 할 것.
    // 현재는 참고용으로만 사용하고 payment의 tip을 사용하므로 현상 유지
    // -> 안전을 위해 수정될 필요가 있어보임
    return self.total + self.tips;
}

- (PCCurrency)dueTotal
{
    return self.grandTotal - self.discountedAmount;
}

- (PCCurrency)remainingToPay
{
    return self.total - self.netPayAmounts - self.discountedAmount;
}

- (PCCurrency)netPayAmounts
{
    // !!!: Bill 발행으로 결제하면서 나누어 지불하는 경우(스플릿)
    // tips 값은 초기 tip값만 DB에 저장되고 업데이트 되지 않으므로 사용에 주의 할 것.
    // 현재는 참고용으로만 사용하고 payment의 tip을 사용하므로 현상 유지
    // -> 안전을 위해 수정될 필요가 있어보임
    return self.payAmounts - self.tips;
}

- (float)payProgress
{
    if(self.total != 0){
        return (float)self.netPayAmounts / (float)self.total;
    } else {
        if(self.netPayAmounts > 0){
            return 1.1f;
        } else {
            return 0.0f;
        }
    }
}

- (UIImage *)image
{
    if(self.merchantNo % 2 == 1){
        return [UIImage imageNamed:@"sample_restaurant.jpg"];
    } else {
        return [UIImage imageNamed:@"alitos.jpg"];
    }
}

//Keys : "canceled", "cash", "card", "other", "memo"
- (NSString *)completeReason
{
    NSDictionary *reasonDict = [self.completeReasonJson objectFromJSONString];
    
    if(reasonDict == nil){
        return nil;
    } else {
        NSMutableString *rtnString = [NSMutableString string];
        if([reasonDict boolForKey:@"cancel"]){
            [rtnString appendString:NSLocalizedString(@"canceled", nil)];
        } else {
        
            if([reasonDict boolForKey:@"cash"]){
                [rtnString appendString:NSLocalizedString(@"+cash ", nil)];
            }
            
            if([reasonDict boolForKey:@"card"]){
                [rtnString appendString:NSLocalizedString(@"+card ", nil)];
            }
            
            if([reasonDict boolForKey:@"other"]){
                [rtnString appendString:NSLocalizedString(@"+other ", nil)];
            }
            
            if([[reasonDict objectForKey:@"memo"] length] > 0){
                [rtnString appendString:[reasonDict objectForKey:@"memo"]];
            }
        }
        
        return rtnString;
    }
}

- (NSString *)merchantName
{
    return _merchantName;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@{ \n\t Order No:%lld \n\t status:%d \n\t subTotal:%ld \n\t subTotalTaxable:%ld \n\t tableNumber:%@ \n\t}",
            [super description],
            self.orderNo,
            self.status,
            (long)self.subTotal,
            (long)self.subTotalTaxable,
            self.tableNumber];
}

- (NSURL *)merchantImageURL
{
    return [NSURL URLWithString:self.merchantImageURLString];
}

- (NSURL *)merchantLogoURL
{
    return [NSURL URLWithString:self.merchantLogoURLString];
}

- (UIColor *)statusColor
{
    switch(self.status){ 
        case OrderStatusDraft:
            return [UIColor colorWithR:226.0f G:83.0f B:91.0f];
            break;
        case OrderStatusPaid:
            return [UIColor colorWithR:206.0f G:63.0f B:71.0f];
            break;
        case OrderStatusConfirmed:
        case OrderStatusAccepted:
            return [UIColor colorWithR:240.0f G:147.0f B:0.0f];
            break;
        case OrderStatusReady:
        case OrderStatusPrepared:
            return [UIColor colorWithR:0.0f G:141.0f B:193.0f];
            break;
        default:
            return [UIColor lightGrayColor];
            break;
    }
}

+ (NSString *)selectQuery
{
    return @"select p.rowid 'rowid', o.* from  'Order' as o left outer join 'Payment' as p on p.orderNo = o.orderNo";
}

- (NSComparisonResult)compareTimeWith:(Order *)anotherOrder
{
    if([anotherOrder isKindOfClass:[TableInfo class]]){
//        return [self.lastAccessedTime compare:((TableInfo *)anotherOrder).actionDate];
        TableInfo *castedTableInfo = (TableInfo *)anotherOrder;
        anotherOrder = castedTableInfo.order;
    }
    
    if(self.orderNo > anotherOrder.orderNo){
        return NSOrderedAscending;
    } else if(self.orderNo < anotherOrder.orderNo){
        return NSOrderedDescending;
    } else {
        return NSOrderedSame;
    }
}

- (BOOL)isEqual:(Order *)anotherOrder
{
    if([anotherOrder isKindOfClass:[Order class]]){
        return self.orderNo == anotherOrder.orderNo;
    } else {
        if([anotherOrder isKindOfClass:[TableInfo class]]){
            TableInfo *castedTableInfo = (TableInfo *)anotherOrder;
            return self.orderNo == castedTableInfo.order.orderNo;
        } else {
            return NO;
        }
    }
}


- (NSString *)subTotalString
{
    return [[NSNumber numberWithCurrency:self.subTotal] currencyString];
}

- (NSString *)payAmountString
{
    return [[NSNumber numberWithCurrency:self.payAmounts] currencyString];
}

- (NSString *)creditAmountString
{
    return [[NSNumber numberWithCurrency:self.creditAmounts] currencyString];
}

- (NSString *)cardAmountString
{
    return [[NSNumber numberWithCurrency:self.cardAmounts] currencyString];
}

- (NSString *)subTotalStringTaxable
{
    return [[NSNumber numberWithCurrency:self.subTotalTaxable] currencyString];
}

- (NSString *)taxesString
{
    return [[NSNumber numberWithCurrency:self.taxes] currencyString];
}

- (NSString *)taxNroServiceFeeAmountString
{
    return [[NSNumber numberWithCurrency:(self.taxes + self.roServiceFee)] currencyString];
}

- (NSString *)deliveryFeeString
{
    return [[NSNumber numberWithCurrency:self.deliveryFee] currencyString];
}

- (NSString *)totalPaidDeliveryFeeAmountsString
{
    return [[NSNumber numberWithCurrency:self.totalPaidDeliveryFeeAmounts] currencyString];
}

- (NSString *)roServiceFeeString
{
    return [[NSNumber numberWithCurrency:self.roServiceFee] currencyString];
}

- (NSString *)totalPaidRoServiceFeeAmountsString
{
    return [[NSNumber numberWithCurrency:self.totalPaidRoServiceFeeAmounts] currencyString];
}

- (NSString *)discountedAmountString
{
    return [[NSNumber numberWithCurrency:-self.discountedAmount] currencyString];
}

- (NSString *)roDiscountedAmountString
{
    return [[NSNumber numberWithCurrency:-self.roDiscountedAmount] currencyString];
}

- (NSString *)meDiscountedAmountString
{
    return [[NSNumber numberWithCurrency:-self.meDiscountedAmount] currencyString];
}

- (NSString *)tipsString
{
    return [[NSNumber numberWithCurrency:self.tips] currencyString];
}

- (NSString *)grandTotalString
{
    return [[NSNumber numberWithCurrency:self.grandTotal] currencyString];
}

- (NSString *)dueTotalString
{
    return [[NSNumber numberWithCurrency:self.dueTotal] currencyString];
}

- (NSString *)totalString
{
    return [[NSNumber numberWithCurrency:self.total] currencyString];
}

- (BOOL)isTakeout
{
    return (self.orderType == OrderTypeTakeout);
}

- (BOOL)isDelivery
{
    return (self.orderType == OrderTypeDelivery);
}

- (BOOL)isTicketBase
{
    return (self.orderType == OrderTypePickup
            || self.orderType == OrderTypeTakeout
            || self.orderType == OrderTypeDelivery);
}


- (NSString *)statusGuideStringWithMerchant:(Merchant *)merchant
{
    return [self statusGuideStringWithMerchant:merchant fully:NO];
}

- (NSString *)statusGuideStringWithMerchant:(Merchant *)merchant fully:(BOOL)fully
{
    if(self.isTwoStepProcess){
        switch(self.status){
            case OrderStatusConfirmed:
            case OrderStatusReady:
            case OrderStatusPaid:
            case OrderStatusFixed:
            case OrderStatusCompleted:
            case OrderStatusAccepted:
                return NSLocalizedString(@"Your order has been placed", nil);
                break;
            default:
                // Do nothing
                break;
        }
    }
    
    switch(self.status){
        case OrderStatusDraft:
            if(fully){
                return NSLocalizedString(@"Done ordering?\nPlease check out to place your order.", nil);
            } else {
                return NSLocalizedString(@"Done ordering?\nPlease check out to place your order.", nil);
            }
            break;
        case OrderStatusPaid:
            return NSLocalizedString(@"You have placed and paid for your order.\nYour food will be prepared soon.", nil);
            break;
        case OrderStatusConfirmed:
        case OrderStatusAccepted:
            if(self.isTicketBase){
                if(self.isDelivery) {
                    return NSLocalizedString(@ "Your food is being prepared.\nWe will notify you when your order\nis ready to be delivered.", nil);
                } if(self.isTakeout){
                    return NSLocalizedString(@"Your food is being prepared.\nWe will notify you when your order\nis ready for pickup.", nil);
                } else {
                    if(merchant.servicedBy == ServicedByStaff ){
                        return NSLocalizedString(@"Your food is being prepared.\nWe will notify you when your order\nis ready to be served.", nil);
                    } else {
                        return NSLocalizedString(@"Your food is being prepared.\nWe will notify you when your order\nis ready for pickup.", nil);
                    }
                }
            } else {
                switch(self.orderType){
                    case OrderTypeBill:
                        return [NSString stringWithFormat:NSLocalizedString(@"Your bill has been issued.\nPlease feel free to checkout and pay whenever you are ready.", nil), merchant.name];
                        break;
                    case OrderTypeOrderOnly:
                        return [NSString stringWithFormat:NSLocalizedString(@"Your food is being prepared.\nThank you for your order.", nil), merchant.name];
                        break;
                    default:
                        return [NSString stringWithFormat:NSLocalizedString(@"Your food is being prepared.\nPlease feel free to checkout and pay whenever you are ready.", nil), merchant.name];
                        break;
                }
            }
            break;
        case OrderStatusReady:
        case OrderStatusPrepared:
        case OrderStatusCompleted:
            if(self.isTicketBase){
                if(self.isDelivery) {
                    return NSLocalizedString(@"Your food is on its way.", nil);
                } if(self.isTakeout){
                    return NSLocalizedString(@"Your food is ready.\nPlease come and pick up your order.", nil);
                } else {
                    if(merchant.servicedBy == ServicedByStaff){
                        return NSLocalizedString(@"Your food is ready.\nOur server will bring your order soon.", nil);
                    } else {
                        return NSLocalizedString(@"Your food is ready.\nPlease come and pick up your order.", nil);
                    }
                }
            } else {
                return NSLocalizedString(@"Your order is complete!\nFeel free to leave at any time.", nil);
            }
            break;
        case OrderStatusFixed:
            return NSLocalizedString(@"Your order process is done.", nil);
            break;
        case OrderStatusDeclined:
        case OrderStatusRefused:
            if(fully){
                if([self.rejectReason length] > 0){
                    return NSLocalizedString(@"Your order was declined.\nRestaurant's reason for declining order is below.\nPlease re-enter your order.", nil);
                } else {
                    return NSLocalizedString(@"Your order was declined.", nil);
                }
            } else {
                if([self.rejectReason length] > 0){
                    return NSLocalizedString(@"Your order was declined.\nCheck for more information.", nil);
                } else {
                    return NSLocalizedString(@"Your order was declined.", nil);
                }
            }
            break;
        case OrderStatusCanceled:
            return NSLocalizedString(@"Your order was removed by restaurant.", nil);
            break;
        default:
            return NSLocalizedString(@"Error to process.", nil);
            break;
    }
}

- (NSString *)address
{
    NSMutableString *address = [NSMutableString string];
    
    if([self.address1 length] > 0){
        [address appendString:self.address1];
    }
    
    if([self.address2 length] > 0){
        [address appendString:@", "];
        [address appendString:self.address2];
    }
    
    if([self.city length] > 0){
        [address appendString:@"\n"];
        [address appendString:self.city];
    }
    
    if([self.state length] > 0){
        [address appendString:@", "];
        [address appendString:self.state];
    }
    
    if([self.zip length] > 0){
        [address appendString:@" "];
        [address appendString:self.zip];
    }
    
    return address;
}

- (BOOL)hasAlcohol
{
    for(LineItem *lineItem in self.lineItems){
        if(lineItem.menuItem.isAlcohol){
            return YES;
            break;
        }
    }
    
    return NO;
}

- (BOOL)didTimedOut
{
//#if DEBUG
//    return NO;
//#else
    NSTimeInterval interval = [self.orderDate timeIntervalSinceNow];
    
    if(interval != 0) interval *= -1;

    if(interval > (60 * 60 * 2)){
        return YES;
    } else {
        return NO;
    }
//#endif
    
}

//- (MenuPan *)isAllItemsInHour
//{
//    for(LineItem *lineItem in self.lineItems){
//        PCLog(@"PAN : %@", lineItem.menuItem.menuPan);
//        if(!lineItem.menuItem.menuPan.isMenuHour){
//            return lineItem.menuItem.menuPan;
//        }
//    }
//    return nil;
//}

- (BOOL)isAllItemsInHour
{
    return [self isAllItemsInHour:NO];
}

- (BOOL)isAllItemsInHour:(BOOL)forciblyReLinkMenu
{
    BOOL allInHour = YES;
    
    for(LineItem *lineItem in self.lineItems){
        if(lineItem.menuItem == nil || forciblyReLinkMenu){
            [lineItem linkMenuItemInMenuList:MENUPAN.wholeSerialMenuList];
        }
    }
    
    
    for(LineItem *lineItem in self.lineItems){
//        PCLog(@"PAN : %@", lineItem.menuItem.menuPan);
        if(lineItem.menuItem != nil){
            if(!lineItem.menuItem.menuPan.isMenuHour){
                lineItem.menuValidStatus |= MenuValidStatusOutOfHours;
                allInHour = NO;
            } else {
                lineItem.menuValidStatus &= ~MenuValidStatusOutOfHours;
            }
        } else {
//            PCError(@"Menu item is null!!!! on line Item No:%d / menu ID:%d", lineItem.lineItemNo, lineItem.menuItemNo);
        }
    }
    
    return allInHour;
}

- (BOOL)isEditable
{
    return (self.status == OrderStatusDeclined);
}

@end
