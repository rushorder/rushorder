//
//  Payment.m
//  RushOrder
//
//  Created by Conan on 2/22/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "Payment.h"
#import "FormatterManager.h"

#define INSERT_QRY @"insert into 'Payment'(\
paymentNo\
,orderNo\
,merchantNo\
,promotionNo\
,payAmount\
,tipAmount\
,taxAmount\
,deliveryFeeAmount\
,roService\
,roServiceFeeAmount\
,creditAmount\
,rewardedAmount\
,discountAmountByRO\
,discountAmountByMerchant\
,pinPointBalance\
,creditPolicyNo\
,globalCreditPolicyNo\
,creditPolicyName\
,globalCreditPolicyName\
,currency\
,payDate\
,refundedDate\
,cardNumber\
,cardType\
,cardFingerPrint\
,cardExpireMonth\
,cardExpireYear\
,status\
,cardCvc) \
values \
(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

#define UPDATE_QRY @"update 'Payment' set \
orderNo = ?\
,merchantNo = ?\
,promotionNo = ?\
,payAmount = ?\
,tipAmount = ?\
,taxAmount = ?\
,deliveryFeeAmount = ?\
,roService = ?\
,roServiceFeeAmount = ?\
,creditAmount = ?\
,rewardedAmount = ?\
,discountAmount = ?\
,discountAmountByRO = ?\
,discountAmountByMerchant = ?\
,pinPointBalance = ?\
,creditPolicyNo = ?\
,globalCreditPolicyNo = ?\
,creditPolicyName = ?\
,globalCreditPolicyName = ?\
,currency = ?\
,payDate = ?\
,refundedDate = ?\
,cardNumber = ?\
,cardType = ?\
,cardFingerPrint = ?\
,cardExpireMonth = ?\
,cardExpireYear = ?\
,cardCvc = ?\
,status = ?\
where paymentNo = ?"

@interface Payment()

@end

@implementation Payment

+ (Payment *)paymentWithPaymentNo:(PCSerial)paymentNo
{
    NSArray *payments = [self listWithCondition:[NSString stringWithFormat:@"where paymentNo=%lld", paymentNo]];
    if([payments count] > 0){
        return [payments objectAtIndex:0];
    } else {
        return nil;
    }
}

+ (NSArray *)paymentWithOrderNo:(PCSerial)orderNo
{
    return [self listWithCondition:[NSString stringWithFormat:@"where p.orderNo=%lld", orderNo]];
}

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    if(self != nil){
        [self updateAddition:dict];
        [self updatePayment:dict];
        
        // TODO:Temporary
//        self.cardNumber = @"6011111111111117";
        /////////////////
    }
    return self;
}

- (void)updateCardInfo:(MobileCard *)card
{
    self.cardNumber = card.cardNumber;
    self.cardType = card.bankName;
    self.cardExpireMonth = card.cardExpireMonth;
    self.cardExpireYear = card.cardExpireYear;
    self.cardCvc = card.cardCvc;
    self.zipCode = card.zipCode;
    self.cardFingerPrint = card.cardFingerPrint;
    self.pgType = card.pgType;
}

- (void)updateAddition:(NSDictionary *)dict
{
    self.chargeId = [dict objectForKey:@"stripe_charge_id"];
    self.orderNo = [dict serialForKey:@"order_id"];
    self.merchantNo = [dict serialForKey:@"merchant_id"];
    self.currency = [dict objectForKey:@"currency"];
    
    self.testMode = [[dict objectForKey:@"mode"] isEqualToString:@"demo"];
}

- (void)updatePayment:(NSDictionary *)dict
{
    self.paymentNo = [dict serialForKey:@"id"];
    self.payAmount = [dict currencyForKey:@"pay_amount"];
    self.actualAmount = [dict currencyForKey:@"actual_amount"];
    self.tipAmount = [dict currencyForKey:@"tip_amount"];
    self.taxAmount = [dict currencyForKey:@"tax_amount"];
    self.deliveryFeeAmount = [dict currencyForKey:@"delivery_fee_amount"];
    self.roServiceFeeAmount = [dict currencyForKey:@"ro_service_fee_amount"];
    self.roService = [dict boolForKey:@"is_ro_service"];
    self.creditAmount = [dict currencyForKey:@"credit_amount"];
    self.rewardedAmount = [dict currencyForKey:@"rewarded_credit"];
    self.discountAmountByRO = [dict currencyForKey:@"discount_amount_by_ro"];
    self.discountAmountByMerchant = [dict currencyForKey:@"discount_amount_by_merchant"];
    self.pinPointBalance = [dict currencyForKey:@"pin_point_credit"]; //Not Yet implemented on Server Side
    self.payDate = [[dict objectForKey:@"created_at"] date];
    self.refundedDate = [[dict objectForKey:@"refunded_at"] date];
    
    self.creditPolicyNo = [[dict objectForKey:@"credit_policy"] serialForKey:@"id"];
    self.creditPolicyName = [[dict objectForKey:@"credit_policy"] objectForKey:@"name"];
    
    self.globalCreditPolicyNo = [[dict objectForKey:@"global_credit_policy"] serialForKey:@"id"];
    self.globalCreditPolicyName = [[dict objectForKey:@"global_credit_policy"] objectForKey:@"name"];
    
    NSDictionary *promotionDict = [dict objectForKey:@"promotion"];
    if(promotionDict != nil){
        self.promotion = [[Promotion alloc] initWithDictionary:promotionDict];
    } else {
        self.promotion = nil;
    }
    
    self.promotionNo = [dict serialForKey:@"promotion_id"];
    
    id last4Number = [dict objectForKey:@"card_last4"];
    
    if(last4Number != nil){
        if([last4Number isKindOfClass:[NSNumber class]]){
            self.cardNumber = [@"000000000000" stringByAppendingString:[last4Number stringValue]];
        } else if([last4Number isKindOfClass:[NSString class]]){
            self.cardNumber = [@"000000000000" stringByAppendingString:last4Number];
        }
    }
    
    self.cardExpireMonth = [[dict objectForKey:@"card_exp_month"] integerValue];
    self.cardExpireYear = [[dict objectForKey:@"card_exp_year"] integerValue];
    self.cardFingerPrint = [dict objectForKey:@"card_fingerprint"];
    
    NSString *cardTypeStr = [dict objectForKey:@"card_type"];
    self.cardType = cardTypeStr;
    self.issuer = [cardTypeStr cardIssuer];
    
    self.testMode = [[dict objectForKey:@"mode"] isEqualToString:@"demo"];
    
    NSString *paymentStatus = [dict objectForKey:@"status"];
    
    if([paymentStatus isEqualToString:@"success"]){
        self.status = PaymentStatusSuccess;
    } else if([paymentStatus isEqualToString:@"refunded"]){
        self.status = PaymentStatusRefunded;
    } else {
        self.status = PaymentStatusUnknown;
    }
    
    NSString *payMethod = [dict objectForKey:@"payment_method"];
    
    if([payMethod isEqualToString:@"cash"]){
        self.method = PaymentMethodCash;
    } else if([payMethod isEqualToString:@"payself"]){
        self.method = PaymentMethodPayself;
    } else if([payMethod isEqualToString:@"plastic_card"]){
        self.method = PaymentMethodPlasticCard;
    } else {
        self.method = PaymentMethodUnknown;
    }
    
    NSArray *lineItems = [dict objectForKey:@"line_items"];
    
    if([lineItems count] > 0){
        NSMutableArray *items = [NSMutableArray array];
        for(NSDictionary *lineItemDict in lineItems){
            LineItem *lineItem = [[LineItem alloc] initWithDictionary:lineItemDict];
            lineItem.merchantNo = self.merchantNo;
            [items addObject:lineItem];
        }
        self.lineItems = items;
    }
    
    [self summation];
}

/*
 @property (nonatomic) PCSerial paymentNo;
 @property (nonatomic) PCSerial orderNo;
 @property (nonatomic) PCSerial merchantNo;
 @property (nonatomic) PCCurrency payAmount;
 @property (nonatomic) PCCurrency tipAmount;
 @property (copy, nonatomic) NSString *currency;
 @property (copy, nonatomic) NSDate *payDate;
 
 @property (copy, nonatomic) NSString *cardNumber;
 @property (nonatomic) NSInteger cardExpireMonth;
 @property (nonatomic) NSInteger cardExpireYear;
 @property (copy, nonatomic) NSString *cardCvc;
 */

#pragma mark - PCModelProtocol
- (id)initWithFMResult:(FMResultSet *)result;
{
    /*
     
     paymentNo\
     ,orderNo\
     ,merchantNo\
     ,promotionNo\
     ,payAmount\
     ,tipAmount\
     ,taxAmount\
     ,deliveryFeeAmount\
     ,creditAmount\
     ,rewardedAmount\
     ,discountAmount\
     ,pinPointBalance\
     ,creditPolicyNo\
     ,globalCreditPolicyNo\
     ,creditPolicyName\
     ,globalCreditPolicyName\
     ,currency\
     ,payDate\
     ,refundedDate\
     ,cardNumber\
     ,cardType\
     ,cardFingerPrint\
     ,cardExpireMonth\
     ,cardExpireYear\
     ,status\
     ,cardCvc) \
     */
     
    self = [super init];
    if(self != nil){
        self.paymentNo = [result longLongIntForColumn:@"paymentNo"];
        self.orderNo = [result longLongIntForColumn:@"orderNo"];
        self.merchantNo = [result longLongIntForColumn:@"merchantNo"];
        self.promotionNo = [result longLongIntForColumn:@"promotionNo"];
        self.payAmount = [result intForColumn:@"payAmount"];
        self.tipAmount = [result intForColumn:@"tipAmount"];
        self.taxAmount = [result intForColumn:@"taxAmount"];
        self.deliveryFeeAmount = [result intForColumn:@"deliveryFeeAmount"];
        self.roService = [result boolForColumn:@"roService"];
        self.roServiceFeeAmount = [result intForColumn:@"roServiceFeeAmount"];
        self.creditAmount = [result intForColumn:@"creditAmount"];
        self.rewardedAmount = [result intForColumn:@"rewardedAmount"];
        self.discountAmountByRO = [result intForColumn:@"discountAmountByRO"];
        self.discountAmountByMerchant = [result intForColumn:@"discountAmountByMerchant"];
        self.pinPointBalance = [result intForColumn:@"pinPointBalance"];
        self.creditPolicyNo = [result longLongIntForColumn:@"creditPolicyNo"];
        self.creditPolicyName = [result stringForColumn:@"creditPolicyName"];
        self.globalCreditPolicyNo = [result longLongIntForColumn:@"globalCreditPolicyNo"];
        self.globalCreditPolicyName = [result stringForColumn:@"globalCreditPolicyName"];
        self.currency = [result stringForColumn:@"currency"];
        self.payDate = [result dateForColumn:@"payDate"];
        self.refundedDate = [result dateForColumn:@"refundedDate"];
        self.status = [result intForColumn:@"status"];
        self.cardNumber = [result stringForColumn:@"cardNumber"];
        self.cardType = [result stringForColumn:@"cardType"];
        self.cardFingerPrint = [result stringForColumn:@"cardFingerPrint"];
        self.cardExpireMonth = [result intForColumn:@"cardExpireMonth"];
        self.cardExpireYear = [result intForColumn:@"cardExpireYear"];
        self.status = [result intForColumn:@"status"];
        self.cardCvc = [result stringForColumn:@"cardCvc"];
        
        self.order = [[Order alloc] initWithFMResult:result]; //Joined Query Result
    }
    return self;
}

- (BOOL)save
{
    if(self.isExisted){
        return [self update];
    } else {
        BOOL rtn = [self insert];
        if(rtn){
            self.rowId = [DB.db lastInsertRowId];
            
            [self closeDB];
            
            if([self.lineItems count] > 0){
                for(LineItem *lineItem in self.lineItems){
                    [lineItem save];
                }
            }
        } else {
            if(self.isTestMode){
                PCLog(@"Payment not saved because this payment is demo");
            } else {
                if(DB.db.lastErrorCode == 19){
                    rtn = [self update];
                    if(!rtn){
                        PCError(@"Payment DB Update error : %d", DB.db.lastErrorCode);
                    }
                } else {
                    PCError(@"Payment DB Insert error : %d", DB.db.lastErrorCode);
                }
            }
        }
        return rtn;
    }
}

- (BOOL)insert
{
    if(self.isTestMode){
        return NO;
    }    
    
    if([self openDB]){
        
        if([self.cardType length] == 0){
            self.cardType = [NSString issuerName:self.issuer];
        }
        
        return [DB.db executeUpdate:INSERT_QRY,
                [NSNumber numberWithLongLong:self.paymentNo],
                [NSNumber numberWithLongLong:self.orderNo],
                [NSNumber numberWithLongLong:self.merchantNo],
                [NSNumber numberWithLongLong:self.promotionNo],
                [NSNumber numberWithCurrency:self.payAmount],
                [NSNumber numberWithCurrency:self.tipAmount],
                [NSNumber numberWithCurrency:self.taxAmount],
                [NSNumber numberWithCurrency:self.deliveryFeeAmount],
                [NSString sqlStringWithBool:self.isRoService],
                [NSNumber numberWithCurrency:self.roServiceFeeAmount],
                [NSNumber numberWithCurrency:self.creditAmount],
                [NSNumber numberWithCurrency:self.rewardedAmount],
                [NSNumber numberWithCurrency:self.discountAmountByRO],
                [NSNumber numberWithCurrency:self.discountAmountByMerchant],
                [NSNumber numberWithCurrency:self.pinPointBalance],
                [NSNumber numberWithCurrency:self.creditPolicyNo],
                [NSNumber numberWithCurrency:self.globalCreditPolicyNo],
                self.creditPolicyName,
                self.globalCreditPolicyName,
                self.currency,
                self.payDate,
                self.refundedDate,
                self.cardNumber,
                self.cardType,
                self.cardFingerPrint,
                [NSNumber numberWithInteger:self.cardExpireMonth],
                [NSNumber numberWithInteger:self.cardExpireYear],
                [NSNumber numberWithInteger:self.status],
                self.cardCvc];
    } else {
        PCError(@"Payment insert error");
        return NO;
    }
}

- (BOOL)update
{
    if(self.isTestMode){
        return NO;
    }
    
    if([self.cardType length] == 0){
        self.cardType = [NSString issuerName:self.issuer];
    }
    
    if([self openDB]){
        
        PCWarning(@"Line items of Payment will not be updated!!!!!!!!");
        
        return [DB.db executeUpdate:UPDATE_QRY,
                [NSNumber numberWithLongLong:self.orderNo],
                [NSNumber numberWithLongLong:self.merchantNo],
                [NSNumber numberWithLongLong:self.promotionNo],
                [NSNumber numberWithCurrency:self.payAmount],
                [NSNumber numberWithCurrency:self.tipAmount],
                [NSNumber numberWithCurrency:self.taxAmount],
                [NSNumber numberWithCurrency:self.deliveryFeeAmount],
                [NSString sqlStringWithBool:self.isRoService],
                [NSNumber numberWithCurrency:self.roServiceFeeAmount],
                [NSNumber numberWithCurrency:self.creditAmount],
                [NSNumber numberWithCurrency:self.rewardedAmount],
                [NSNumber numberWithCurrency:self.discountAmountByRO],
                [NSNumber numberWithCurrency:self.discountAmountByMerchant],
                [NSNumber numberWithCurrency:self.pinPointBalance],
                [NSNumber numberWithCurrency:self.creditPolicyNo],
                [NSNumber numberWithCurrency:self.globalCreditPolicyNo],
                self.creditPolicyName,
                self.globalCreditPolicyName,
                self.currency,
                self.payDate,
                self.refundedDate,
                self.cardNumber,
                self.cardType,
                self.cardFingerPrint,
                [NSNumber numberWithInteger:self.cardExpireMonth],
                [NSNumber numberWithInteger:self.cardExpireYear],
                self.cardCvc,
                [NSNumber numberWithInteger:self.status],
                [NSNumber numberWithLongLong:self.paymentNo]];
    } else {
        PCError(@"Payment update error");
        return NO;
    }

}

+ (NSString *)selectQuery
{
    return @"select p.rowid 'rowid', * from 'Payment' as p left outer join 'Order' as o on p.orderNo = o.orderNo";
}

+ (NSString *)orderByQuery
{
    return @"order by paymentNo desc";
}

- (Issuer)issuer
{
    if(_issuer == IssuerUnknown){
        _issuer = [FORMATTER.cardNumberFormatter acquireIssuer:self.cardNumber];
    }
    return _issuer;
}

#pragma mark - readonly

- (PCCurrency)netAmount
{
    return self.payAmount - self.creditAmount;
}

//- (PCCurrency)orderAmount
//{
//    return self.payAmount - self.tipAmount;
//}

- (NSString *)formattedCardNumber
{
    return [self formattedCardNumberWithSecureString:@"●"];
}

- (NSString *)formattedCardNumberWithSecureString:(NSString *)secureString
{
    switch(self.method){
        case PaymentMethodCash:
            return NSLocalizedString(@"Cash - Manually", nil);
            break;
        case PaymentMethodPayself:
            if(self.cardNumber == nil){
                return @"●●●●";
            } else {
                return [FORMATTER.cardNumberFormatter format:self.cardNumber
                                                  withIssuer:self.issuer
                                                    securely:YES
                                                secureString:secureString];
            }
            break;
        case PaymentMethodPlasticCard:
            return NSLocalizedString(@"Plastic Card - Manually", nil);
            break;
        default:
            return NSLocalizedString(@"Unknown", nil);
            break;
    }
}

- (UIImage *)cardLogo
{
    switch(self.method){
        case PaymentMethodCash:
            return [UIImage imageNamed:@"cash.png"];
            break;
        case PaymentMethodPayself:
            return [UIImage logoImageByIssuer:self.issuer];
            break;
        default:
            return [UIImage imageNamed:@"icon_card.png"];
            break;
    }
}

- (void)summation
{
    [self summationWithTaxCalc:NO merchant:nil];
}

- (void)summationWithTaxCalc:(BOOL)withTaxCalc merchant:(Merchant *)merchant
{
    if([self.lineItems count] > 0){
        PCCurrency sum = 0;
        PCCurrency sumTaxable = 0;
        for(LineItem *lineItem in self.lineItems){
            sum += lineItem.lineAmount;
            if(lineItem.taxable){
                sumTaxable += lineItem.lineAmount;
            }
        }
        
        self.subTotal = sum;
        self.subTotalTaxable = sumTaxable;
        if(withTaxCalc){
            self.taxes = roundf(((self.subTotalTaxable * merchant.taxRate) / 100.0f));
        } else {
            self.taxes = self.taxAmount;
        }

        self.grandTotal = sum + self.taxes + self.tipAmount + self.deliveryFeeAmount + self.roServiceFeeAmount - self.discountAmount;
    } else {
        self.subTotal = self.order.subTotal;
        self.taxes = self.order.taxes;
        self.grandTotal = self.order.total + self.tipAmount + self.deliveryFeeAmount + self.roServiceFeeAmount - self.discountAmount;
    }
}

- (NSString *)tipAmountString
{
    return [[NSNumber numberWithCurrency:self.tipAmount] currencyString];
}

- (NSString *)taxAmountString
{
    return [[NSNumber numberWithCurrency:self.taxAmount] currencyString];
}

- (NSString *)taxNroServiceFeeAmountString
{
    return [[NSNumber numberWithCurrency:(self.taxAmount + self.roServiceFeeAmount)] currencyString];
}

- (NSString *)deliveryFeeAmountString
{
    return [[NSNumber numberWithCurrency:self.deliveryFeeAmount] currencyString];
}

- (NSString *)roServiceFeeAmountString
{
    return [[NSNumber numberWithCurrency:self.roServiceFeeAmount] currencyString];
}

- (NSString *)subTotalString
{
    return [[NSNumber numberWithCurrency:self.subTotal] currencyString];
}

- (NSString *)subTotalTaxableString
{
    return [[NSNumber numberWithCurrency:self.subTotalTaxable] currencyString];
}

- (NSString *)grandTotalString
{
    return [[NSNumber numberWithCurrency:self.grandTotal] currencyString];
}

- (NSString *)taxesString
{
    return [[NSNumber numberWithCurrency:self.taxes] currencyString];
}

- (NSString *)payAmountString
{
    return [[NSNumber numberWithInteger:self.payAmount] currencyString];
}

- (NSString *)actualAmountString
{
    return [[NSNumber numberWithInteger:self.actualAmount] currencyString];
}

- (NSString *)netAmountString
{
    return [[NSNumber numberWithInteger:self.netAmount] currencyString];
}


- (NSString *)creditAmountString
{
    return [[NSNumber numberWithInteger:self.creditAmount] pointString];
}

- (NSString *)creditConvertedAmountString
{
    return [[NSNumber numberWithInteger:self.creditAmount] currencyString];
}

- (NSString *)creditConvertedAmountStringAsDiscount
{
    return [[NSNumber numberWithInteger:-self.creditAmount] currencyString];
}

- (NSString *)creditConvertedAmountStringInParenthesis
{
    return [NSString stringWithFormat:@"(%@)",[[NSNumber numberWithInteger:self.creditAmount] currencyString]];
}

- (NSString *)rewardedAmountString
{
    return [[NSNumber numberWithInteger:self.rewardedAmount] pointString];
}

- (NSString *)discountAmountString
{
    return [[NSNumber numberWithInteger:(-self.discountAmount)] currencyString];
}

- (NSString *)pinPointBalanceString
{
    return [[NSNumber numberWithInteger:self.pinPointBalance] pointString];
}

- (PCCurrency)payAmountWithoutCents
{
    return (self.payAmount - (self.payAmount % 100));
}

- (NSMutableArray *)lineItems
{
    if(_lineItems == nil){
        _lineItems = [NSMutableArray array];
    }
    
    return _lineItems;
}

- (NSString *)methodString
{
    switch(self.method){
        case PaymentMethodCash:
            return @"cash";
            break;
        case PaymentMethodPlasticCard:
            return @"plastic_card";
            break;
        case PaymentMethodPayself:
            return @"payself";
            break;
        default:
            return @"unknown";
            break;
    }
}

- (PromotionErrorCode)removePromotion:(Promotion *)promotion
{
    return [self operatePromotion:promotion adding:NO];
}

- (PromotionErrorCode)applyPromotion:(Promotion *)promotion
{
    return [self operatePromotion:promotion adding:YES];
}

- (PromotionErrorCode)operatePromotion:(Promotion *)promotion adding:(BOOL)adding
{
    NSInteger i = 0;
    
    if(adding && !promotion.isCombinable){
        
        BOOL isExistAnyCoupon = NO;
        for(i = 0; i < [self.appliedPromotions count]; i++){
            Promotion *exPromotion = [self.appliedPromotions objectAtIndex:i];
            if(exPromotion.isRequiredCode){
                isExistAnyCoupon = YES;
            }
        }
        
        if(isExistAnyCoupon){
            return PromotionErrorNotCombinable;
        }
    }
    
    if(promotion.merchantNo > 0 && promotion.merchantNo != self.merchantNo){
        return PromotionErrorOthers;
    }
    
    self.discountAmountByRO = 0;
    self.discountAmountByMerchant = 0;
    
    PCCurrency totalAmount = self.subTotal;
    PCCurrency totalDiscountAmountByRO = 0;
    PCCurrency totalDiscountAmountByMerchant = 0;
    
    
    NSInteger foundIndex = NSNotFound;
    PromotionErrorCode rtnCode = PromotionErrorSuccess;
    
    BOOL duplicatedPromotion = NO;
    BOOL notCombinableExists = NO;
    
    for(i = 0; i < [self.appliedPromotions count]; i++){
        
        Promotion *exPromotion = [self.appliedPromotions objectAtIndex:i];
        
        if(adding && promotion.isRequiredCode){
            if(!exPromotion.isCombinable){
                notCombinableExists = YES;
            }
        } else {
            if(promotion.promotionNo == exPromotion.promotionNo){
                foundIndex = i;
                continue;
            }
        }
        
        if(promotion.promotionNo == exPromotion.promotionNo){
            duplicatedPromotion = YES;
        }
        
        if(!exPromotion.isActive){
            [self.appliedPromotions removeObjectAtIndex:i];
            i--;
            continue;
        }
        
        if([exPromotion.endDate timeIntervalSinceDate:[NSDate date]] < 0){
            [self.appliedPromotions removeObjectAtIndex:i];
            i--;
            continue;
        }
        
        if([exPromotion.startDate timeIntervalSinceDate:[NSDate date]] > 0){
            [self.appliedPromotions removeObjectAtIndex:i];
            i--;
            continue;
        }
        
        if(exPromotion.minOrderAmount > self.subTotal){
            [self.appliedPromotions removeObjectAtIndex:i];
            i--;
            continue;
        }
        
        if(exPromotion.isFlatDiscount){
            exPromotion.appliedDiscountAmount = MIN(exPromotion.discountAmount, (totalAmount - (totalDiscountAmountByMerchant + totalDiscountAmountByRO)));
        } else {
            PCCurrency limitedDiscountAmount = roundf(totalAmount * (exPromotion.discountRate / 100.0f));

            limitedDiscountAmount = MIN(exPromotion.limitDiscountAmount, limitedDiscountAmount);

            
            exPromotion.appliedDiscountAmount = MIN(limitedDiscountAmount, (totalAmount - (totalDiscountAmountByMerchant + totalDiscountAmountByRO)));
        }
        
        if(exPromotion.byMerchant){
            totalDiscountAmountByMerchant += exPromotion.appliedDiscountAmount;
        } else {
            totalDiscountAmountByRO += exPromotion.appliedDiscountAmount;
        }
        
        if(totalAmount <= (totalDiscountAmountByMerchant + totalDiscountAmountByRO)){
            break;
        }
    }
    
    if(totalAmount <= (totalDiscountAmountByMerchant + totalDiscountAmountByRO)){
        goto sumup;
    }
    
    if(foundIndex != NSNotFound){
        [self.appliedPromotions removeObjectAtIndex:foundIndex];
    }
    
    if(adding && !duplicatedPromotion){
        if(notCombinableExists){
            rtnCode = PromotionErrorNotCombinableExists;
            goto sumup;
        }
        
        if(!promotion.isActive){
            rtnCode = PromotionErrorInActive;
            goto sumup;
        }
        
        if([promotion.endDate timeIntervalSinceDate:[NSDate date]] < 0){
            rtnCode = PromotionErrorExpired;
            goto sumup;
        }
        
        if([promotion.startDate timeIntervalSinceDate:[NSDate date]] > 0){
            rtnCode = PromotionErrorNotStarted;
            goto sumup;
        }
        
        if(promotion.minOrderAmount > self.subTotal){
            rtnCode = PromotionErrorLackOrderAmount;
            goto sumup;
        }
        
        if(promotion.isFlatDiscount){
            promotion.appliedDiscountAmount = MIN(promotion.discountAmount, (totalAmount - (totalDiscountAmountByMerchant + totalDiscountAmountByRO)));
        } else {
            PCCurrency limitedDiscountAmount = roundf(totalAmount * (promotion.discountRate / 100.0f));
//            if(promotion.limitDiscountAmount > 0){
                limitedDiscountAmount = MIN(promotion.limitDiscountAmount, limitedDiscountAmount);
//            }
            promotion.appliedDiscountAmount = MIN(limitedDiscountAmount, (totalAmount - (totalDiscountAmountByMerchant + totalDiscountAmountByRO)));
        }
        
        if(promotion.byMerchant){
            totalDiscountAmountByMerchant += promotion.appliedDiscountAmount;
        } else {
            totalDiscountAmountByRO += promotion.appliedDiscountAmount;
        }
        
        [self.appliedPromotions addObject:promotion];
    }
    
sumup:

    self.discountAmountByMerchant = totalDiscountAmountByMerchant;
    self.discountAmountByRO = totalDiscountAmountByRO;
    
    PCLog(@"Credit Amount %d", self.creditAmount);
    
    if(rtnCode == PromotionErrorSuccess){
        if(self.discountAmount == totalAmount){
            rtnCode = PromotionErrorFullUse;
        } else {
            rtnCode = PromotionErrorSuccess;
        }
    }
    
    return rtnCode;
}

- (NSMutableArray *)appliedPromotions
{
    if(_appliedPromotions == nil){
        _appliedPromotions = [NSMutableArray array];
    }
    return _appliedPromotions;
}

- (void)applyRoServiceFee:(Merchant *)merchant
{
    if(merchant.hasRoServiceFee){
        self.roService = YES;
        self.roServiceFeeAmount = roundf(self.subTotal * (merchant.roServiceRate / 100.0f));
        self.roServiceFeeAmount = MAX(self.roServiceFeeAmount, merchant.roServiceMinAmount);
    } else {
        self.roService = NO;
    }
}

- (NSArray *)appliedDiscountsDicts
{
    return [self appliedDiscountsDictsWithSmallCutAmount:0];
}

- (NSArray *)appliedDiscountsDictsWithSmallCutAmount:(PCCurrency)smallCutAmount
{
    NSMutableArray *discounts = [NSMutableArray array];
    
    for(Promotion *promotion in self.appliedPromotions){
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                              [NSNumber numberWithSerial:promotion.promotionNo], @"promotion_id",
                              [NSNumber numberWithCurrency:promotion.appliedDiscountAmount], @"discount_amount",
                              promotion.couponCode, @"code",
                              @"promotion",@"discount_type",
                              (promotion.byMerchant ? @"true":@"false"),@"by_merchant",
                              promotion.title,@"title",
                              nil];
        [discounts addObject:dict];
    }
    
    if(smallCutAmount > 0){
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                              [NSNumber numberWithCurrency:smallCutAmount], @"discount_amount",
                              @"small_cut",@"discount_type",
                              @"false",@"by_merchant",
                              @"RushOrder Pays Difference",@"title",
                              nil];
        [discounts addObject:dict];
    }
    
    if([discounts count] > 0){
        return discounts;
    } else {
        return nil;
    }
}

- (PCCurrency)total
{
    return self.subTotal + self.taxes;
}

- (PCCurrency)discountAmount
{
    return self.discountAmountByRO + self.discountAmountByMerchant;
}


@end
