//
//  PCModel.h
//  RushOrder
//
//  Created by Conan on 2/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"

#define DB [SQLLiteDatabase sharedDatabase]

@interface SQLLiteDatabase : NSObject
@property (copy, nonatomic) NSString *dbName;
@property (strong, readonly) FMDatabase *db;

+ (SQLLiteDatabase *)sharedDatabase;
@end

@protocol PCModelProtocol<NSObject>
@required
- (id)initWithFMResult:(FMResultSet *)result;
- (BOOL)insert;
- (BOOL)update;
@optional
+ (NSString *)selectQuery;
+ (NSString *)orderByQuery;
+ (NSString *)whereQuery;
@end

@interface PCModel : NSObject <PCModelProtocol>

@property (nonatomic) NSInteger rowId;
@property (readonly) BOOL isExisted;
- (BOOL)openDB;
- (BOOL)closeDB;

+ (NSMutableArray *)listAll;
+ (NSMutableArray *)listWithCondition:(NSString *)condition;
+ (id)instanceWithCondition:(NSString *)condition;
+ (BOOL)deleteWithCondition:(NSString *)condition;

#pragma mark - PCModelProtocol
- (id)initWithFMResult:(FMResultSet *)result;
- (BOOL)save;
- (BOOL)delete;
- (BOOL)insert;
- (BOOL)update;

@end