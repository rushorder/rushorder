//
//  UserInfo.m
//  RushOrder
//
//  Created by Conan on 3/2/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "UserInfo.h"

@implementation UserInfo

- (id)init
{
    self = [super init];
    if(self != nil){
    }
    return self;
}

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    if(self != nil){
        [self updateWithOwnerDict:dict];
    }
    
    return self;
}

- (void)updateWithOwnerDict:(NSDictionary *)owner
{
    self.userNo = [owner serialForKey:@"id"];
    self.email = [owner objectForKey:@"email"];
    self.name = [owner objectForKey:@"name"];
    self.notificationScope = [[owner objectForKey:@"receive_notification_scope"] notificationScopeEnum];
    self.role = [[owner objectForKey:@"role"] userRoleEnum];
}

@end


@implementation NSString (UserInfo)

+ (NSString *)stringWithUserRole:(UserRole)userRole
{
    switch(userRole){
        case UserRoleUnknown:
            return @"";
            break;
        case UserRoleOwner:
            return @"owner";
            break;
        case UserRoleStaff:
            return @"staff";
            break;
    }
}

+ (NSString *)stringWithNotificationScope:(NotificationScope)notificationScope
{
    switch(notificationScope){
        case NotificationScopeNone:
            return @"";
            break;
        case NotificationScopeMyOrders:
            return @"my_orders";
            break;
        case NotificationScopeMyTables:
            return @"my_tables";
            break;
        case NotificationScopeAll:
            return @"all";
            break;
    }
}

- (UserRole)userRoleEnum
{
    if([self isEqualToString:@"owner"]){
        return UserRoleOwner;
    } else if([self isEqualToString:@"staff"]){
        return UserRoleStaff;
    } else {
        return UserRoleUnknown;
    }
}

- (NotificationScope)notificationScopeEnum
{
    if([self isEqualToString:@"my_orders"]){
        return NotificationScopeMyOrders;
    } else if([self isEqualToString:@"my_tables"]){
        return NotificationScopeMyTables;
    } else if([self isEqualToString:@"all"]){
        return NotificationScopeAll;
    } else {
        return NotificationScopeNone;
    }
}

@end