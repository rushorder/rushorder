//
//  Cuisine.h
//  RushOrder
//
//  Created by Conan on 10/27/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "PCModel.h"

@interface Cuisine : PCModel

@property (nonatomic) PCSerial no; //If no == 0 => All Cuisine
@property (copy, nonatomic) NSString *name;
@property (nonatomic, getter = isSelected) BOOL selected;

- (id)initWithDictionary:(NSDictionary *)dict;
@end
