//
//  AccountSwitchingManager.h
//  RushOrder
//
//  Created by Conan on 6/10/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Receipt.h"

#define SWITCH [AccountSwitchingManager sharedManager]

extern NSString * const AccountDataSwitchedNotification;

@interface AccountSwitchingManager : NSObject
@property (nonatomic, strong) NSMutableString *syncRangeString;

+ (AccountSwitchingManager *)sharedManager;

- (BOOL)initSyncData;
- (BOOL)startSync;
- (BOOL)removeLocalData;
- (BOOL)removeLocalDataIncludeCard:(BOOL)includeCard;
@end