//
//  MenuManager.h
//  RushOrder
//
//  Created by Conan on 6/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

//#import "PCServiceBase.h"
#import "PCMenuService.h"

#define MENUPAN [MenuManager sharedManager]

extern NSString * const MenuListChangedNotification;
typedef void (^SuccessBlock)(id response);
typedef void (^FailBlock)(NSUInteger errorCode);


@interface MenuManager : PCServiceBase

@property (nonatomic, getter = isDirty) BOOL dirty;
@property (strong, nonatomic) Merchant *merchant;
@property (strong, nonatomic) NSMutableArray *menupanList;
@property (strong, nonatomic) NSMutableArray *wholeSerialMenuList;
@property (strong, nonatomic) NSMutableArray *wholeMenuList;
@property (strong, nonatomic) NSMutableArray *activeMenuList;
@property (nonatomic, getter = isMenuFetched) BOOL menuFetched;
@property (nonatomic) MenuListType selectedMenuListType;
@property (nonatomic, copy) NSDate *modifiedDate;
@property (nonatomic, readonly) NSString *menuListTypeString;


+ (MenuManager *)sharedManager;

- (void)requestMenuPan;

- (void)requestMenus;
- (void)requestMenusWithType:(MenuListType)menuListType;
- (void)requestMenus:(BOOL)forcibly;
- (BOOL)resetGraphWithMerchant:(Merchant *)merchant; //return same merchant. so does not refresh menu list
- (BOOL)resetGraphWithMerchant:(Merchant *)merchant withType:(MenuListType)menuListType;
- (void)resetGraph;
- (void)resetGraphWithType:(MenuListType)menuListType;
- (void)requestMenus:(BOOL)fircibly
             success:(SuccessBlock)successBlock
             andFail:(FailBlock)failBlock;
- (void)requestMenusWithSuccess:(SuccessBlock)successBlock
                        andFail:(FailBlock)failBlock;
- (NSMutableArray *)menuListAt:(NSUInteger)index;
@end
