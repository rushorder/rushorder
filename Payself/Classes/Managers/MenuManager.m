//
//  MenuManager.m
//  RushOrder
//
//  Created by Conan on 6/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MenuManager.h"
#import "MenuPan.h"
#import "MenuCategory.h"
#import "MenuSubCategory.h"
#import "MenuOptionGroup.h"
#import "MenuOption.h"


NSString * const MenuListChangedNotification = @"MenuListChangedNotification";

@interface MenuManager()
@property (nonatomic, getter = inProgressing) BOOL progressing;

@end


@implementation MenuManager

+ (MenuManager *)sharedManager
{
    static MenuManager *aManager = nil;
    
    @synchronized(self){
        if(aManager == nil){
            aManager = [[self alloc] init];            
        }
    }
    
    return aManager;
}

- (id)init
{
    self = [super init];
    if(self != nil){
        self.progressing = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didEnterBackground:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(willEnterForeground:)
                                                     name:UIApplicationWillEnterForegroundNotification
                                                   object:nil];
        self.dirty = YES;

    }
    
    return self;
}

- (void)dealloc
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didEnterBackground:(NSNotification *)aNoti
{
    self.dirty = YES;
    self.modifiedDate = nil;
    self.wholeMenuList = nil;
    self.activeMenuList = nil;
    self.wholeSerialMenuList = nil;
    self.menuFetched = NO;
}

- (void)willEnterForeground:(NSNotification *)aNoti
{
    
}

- (void)requestMenus
{
    [self requestMenus:NO menuListType:MenuListTypeAll];
}

- (void)requestMenus:(BOOL)forcibly
{
    [self requestMenus:forcibly menuListType:MenuListTypeAll];
}

- (void)requestMenusWithType:(MenuListType)menuListType
{
    [self requestMenus:NO menuListType:menuListType];
}

- (void)requestMenus:(BOOL)forcibly
        menuListType:(MenuListType)menuListType
{
    [self requestMenus:forcibly
          menuListType:menuListType
               success:nil
                  fail:nil];
}

- (void)requestMenusWithSuccess:(SuccessBlock)successBlock
                        andFail:(FailBlock)failBlock
{
    [self requestMenus:NO
          menuListType:self.selectedMenuListType
               success:successBlock
                  fail:failBlock];
}

- (void)requestMenus:(BOOL)fircibly
             success:(SuccessBlock)successBlock
             andFail:(FailBlock)failBlock
{
    [self requestMenus:fircibly
          menuListType:self.selectedMenuListType
               success:successBlock
                  fail:failBlock];
}

- (void)requestMenus:(BOOL)forcibly
        menuListType:(MenuListType)menuListType
             success:(SuccessBlock)successBlock
                fail:(FailBlock)failBlock
{
    if(!self.isDirty && !forcibly){
        if(failBlock != nil){
            failBlock(0);
        }
        return;
    }
    
    if(self.progressing){
        if(failBlock != nil){
            failBlock(0);
        }
        return;
    }
    
    RequestResult result = RRFail;
    result = [MENU requestMenu:self.merchant
                          type:menuListType
               completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode) {
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      self.menuFetched = YES;
                      switch(response.errorCode){
                          case ResponseSuccess:{
                            
                              self.modifiedDate = self.merchant.lastMenuModifiedDate;
                              
                              self.wholeMenuList = [response objectForKey:@"wholeMenuList"];
                              self.activeMenuList = [response objectForKey:@"activeMenuList"];
                              self.wholeSerialMenuList = [response objectForKey:@"wholeSerialMenuList"];
                              
                              [[NSNotificationCenter defaultCenter]
                               postNotificationName:MenuListChangedNotification
                               object:nil
                               userInfo:nil];
                              
                              self.dirty = NO;
                              
                              if(successBlock != nil){
                                  successBlock(response);
                              }
                          }
                              break;
                          case ResponseErrorNoMerchant:
                              // Silent because this case will be treated in - (void)requestMerchantInfo
                              if(failBlock != nil){
                                  failBlock(response.errorCode);
                              }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              if(failBlock != nil){
                                  failBlock(response.errorCode);
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting menu", nil)];
                              }
                              if(failBlock != nil){
                                  failBlock(response.errorCode);
                              }
                          }
                              break;
                                  
                      }
                      
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      if(failBlock != nil){
                          failBlock(response.errorCode);
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                      if(failBlock != nil){
                          failBlock(response.errorCode);
                      }
                  }
                  self.progressing = NO;
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            self.progressing = YES;
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            self.progressing = NO;
            if(failBlock != nil){
                failBlock(0);
            }
            break;
        default:
            break;
    }
}


- (NSMutableArray *)menuListAt:(NSUInteger)index
{
    if(_activeMenuList == nil){
        [self requestMenus:YES menuListType:self.selectedMenuListType];
        return nil;
    } else {
        if(index < [_activeMenuList count]){
            return ((MenuPan *)[_activeMenuList objectAtIndex:index]).menus;
        } else {
            PCError(@"Try to access out of bounds menu pan index %d, count %d", index, [_activeMenuList count]);
            return nil;
        }
    }
}

- (NSMutableArray *)menupanList
{
    if(_menupanList == nil){
        [self requestMenuPan];
        return nil;
    }
    return _menupanList;
}
         
- (void)requestMenuPan
{
    RequestResult result = RRFail;
    result = [MENU requestMenuPan:self.merchant
               completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode) {
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      self.menuFetched = YES;
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              self.menupanList = [NSMutableArray array];
                              
                              for(NSDictionary *menuPanDict in response){
                                  MenuPan *menuPan = [[MenuPan alloc] initWithDictionary:menuPanDict
                                                                            withTimeZone:nil];
                                  [self.menupanList addObject:menuPan];
                              }
                          }
                              break;
                          case ResponseErrorNoMerchant:
                              // Silent because this case will be treated in - (void)requestMerchantInfo
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting menu", nil)];
                              }
                          }
                              break;
                              
                      }
                      
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  self.progressing = NO;
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            self.progressing = YES;
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            self.progressing = NO;
            break;
        default:
            break;
    }
}

- (NSMutableArray *)wholeMenuList
{
    if(_wholeMenuList == nil){
        [self requestMenus:YES menuListType:self.selectedMenuListType];
        return nil;
    }
    return _wholeMenuList;
}

- (NSMutableArray *)activeMenuList
{
    if(_activeMenuList == nil){
        [self requestMenus:YES menuListType:self.selectedMenuListType];
        return nil;
    }
    return _activeMenuList;
}

- (BOOL)resetGraphWithMerchant:(Merchant *)merchant
{
    return [self resetGraphWithMerchant:merchant withType:MenuListTypeAll];
}

- (BOOL)resetGraphWithMerchant:(Merchant *)merchant withType:(MenuListType)menuListType
{
    if(self.merchant.merchantNo == merchant.merchantNo && self.selectedMenuListType == menuListType){
        if((merchant.lastMenuModifiedDate != nil && self.modifiedDate == nil) || [merchant.lastMenuModifiedDate compare:self.modifiedDate] != NSOrderedSame || self.isDirty){
            [self resetGraphWithType:menuListType];
            return YES;
        } else {
            return NO;
        }
    } else {
        self.merchant = merchant;
        [self resetGraphWithType:menuListType];
        return YES;
    }
}

- (void)resetGraph
{
    [self resetGraphWithType:MenuListTypeAll];
}

- (void)resetGraphWithType:(MenuListType)menuListType
{
    self.selectedMenuListType = menuListType;
    self.modifiedDate = nil;
    self.wholeSerialMenuList = nil;
    self.wholeMenuList = nil;
    self.activeMenuList = nil;
    self.menuFetched = NO;
    self.dirty = YES;
}

- (NSString *)menuListTypeString
{
    switch(self.selectedMenuListType){
        case MenuListTypeDinein:
            return NSLocalizedString(@"Dine-in", nil);
            break;
        case MenuListTypeTakeout:
            return NSLocalizedString(@"Take-out", nil);
            break;
        case MenuListTypeDelivery:
            return NSLocalizedString(@"Delivery", nil);
            break;
        default:
            return NSLocalizedString(@"Order", nil);
    }
}

@end
