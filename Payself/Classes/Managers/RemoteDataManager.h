//
//  RemoteDataManager.h
//  RushOrder
//
//  Created by Conan on 6/10/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DeliveryAddress.h"

#define REMOTE [RemoteDataManager sharedManager]

extern NSString * const CardListChangeNotification;
extern NSString * const OrdersListChangeNotification;
extern NSString * const FavoritedRestaurantsListChangeNotification;
extern NSString * const DeliveryAddressChangeNotification;

@interface RemoteDataManager : NSObject

@property(strong, nonatomic) NSMutableArray *orders;
@property(strong, nonatomic) NSMutableArray *cards;
@property(strong, nonatomic) NSMutableArray *favoriteRestaurants;
@property(strong, nonatomic) NSMutableArray *deliveryAddresses;


@property(nonatomic, getter = isOrdersDirty) BOOL ordersDirty;
@property(nonatomic, getter = isCardDirty) BOOL cardDirty;
@property(nonatomic, getter = isFavoritedDirty) BOOL favoritedDirty;
@property(nonatomic, getter = isDeliveryAddressDirty) BOOL deliveryAddressDirty;

@property(nonatomic, getter = isOrdersFetched) BOOL ordersFetched;
@property(nonatomic, getter = isCardFetched) BOOL cardFetched;
@property(nonatomic, getter = isFavoritedFetched) BOOL favoritedFetched;
@property(nonatomic, getter = isDeliveryAddressFetched) BOOL deliveryAddressFetched;

@property(strong, nonatomic) DeliveryAddress *defaultDeliveryAddress;

//For Only Orders
@property (nonatomic) NSUInteger currentPage;
@property (nonatomic, getter = isMoredata) BOOL moredata;


+ (RemoteDataManager *)sharedManager;

- (void)handleCardsResponse:(NSMutableArray *)cardList
           withNotification:(BOOL)withNotification;
- (void)handleFavoriteResponse:(NSMutableArray *)restaurantList;
- (void)handleFavoriteResponse:(NSMutableArray *)restaurantList
              withNotification:(BOOL)withNotification;
- (void)handleDeliveryAddressesResponse:(NSMutableArray *)deliveryAddresses
                       withNotification:(BOOL)withNotification;
- (BOOL)requestOrdersList;
- (BOOL)requestDeliveryAddresses;

- (BOOL)isFavoriteMerchant:(Merchant *)aMerchant;
@end
