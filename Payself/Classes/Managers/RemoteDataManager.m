//
//  RemoteDataManager.m
//  RushOrder
//
//  Created by Conan on 6/10/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "RemoteDataManager.h"
#import "PCPaymentService.h"
#import "PCCredentialService.h"
#import "ReachabilityService.h"
#import "CustomRequest.h"

NSString * const CardListChangeNotification = @"CardListChangeNotification";
NSString * const OrdersListChangeNotification = @"OrdersListChangeNotification";
NSString * const FavoritedRestaurantsListChangeNotification = @"FavoritedRestaurantsListChangeNotification";
NSString * const DeliveryAddressChangeNotification = @"DeliveryAddressChangeNotification";

@interface RemoteDataManager()
@property(nonatomic, getter = isOrdersProgressing) BOOL ordersProgressing;
@property(nonatomic, getter = isCardProgressing) BOOL cardProgressing;
@property(nonatomic, getter = isFavoritedProgressing) BOOL favoritedProgressing;
@property(nonatomic, getter = isDeliveryAddressProgressing) BOOL deliveryAddressProgressing;

@end

@implementation RemoteDataManager

+ (RemoteDataManager *)sharedManager
{
    static RemoteDataManager *aManager = nil;
    
    @synchronized(self){
        if(aManager == nil){
            aManager = [[self alloc] init];
        }
    }
    
    return aManager;
}

- (id)init
{
    self = [super init];
    if(self != nil){
        self.cardDirty = YES;
        self.ordersDirty = YES;
        self.favoritedDirty = YES;
        
        self.ordersFetched = NO;
         self.favoritedFetched = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(signedIn:)
                                                     name:SignedInNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(signedOut:)
                                                     name:SignedOutNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didEnterBackground:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
    }
    
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)signedIn:(NSNotification *)aNoti
{
    self.cardDirty = YES;
    self.ordersDirty = YES;
    self.favoritedDirty = YES;
    self.deliveryAddressDirty = YES;
    
    self.cards = nil;
    self.orders = nil;
    self.favoriteRestaurants = nil;
    self.deliveryAddresses = nil;
    self.defaultDeliveryAddress = nil;
    
    self.deliveryAddressFetched = NO;
    
    [self favoriteRestaurants];
    
    self.currentPage = 0;
    
    [NSUserDefaults standardUserDefaults].customRequestSynced = NO;
}

- (void)signedOut:(NSNotification *)aNoti
{
    self.cardDirty = YES;
    self.ordersDirty = YES;
    self.favoritedDirty = YES;
    self.deliveryAddressDirty = YES;
    
    self.cards = nil;
    self.orders = nil;
    self.favoriteRestaurants = nil;
    self.deliveryAddresses = nil;
    self.defaultDeliveryAddress = nil;
    
    self.deliveryAddressFetched = NO;
    
    self.currentPage = 0;
}

- (void)didEnterBackground:(NSNotification *)aNoti
{
    self.deliveryAddressDirty = YES;
}

- (NSMutableArray *)cards
{
    if(_cards == nil){
        _cards = [NSMutableArray array];
    }
    
    if(self.isCardDirty){
        [self requestCardList];
    }
    
    return _cards;
}


- (NSMutableArray *)orders
{
    if(_orders == nil){
        _orders = [NSMutableArray array];
    }

    if(self.isOrdersDirty || !self.isOrdersFetched){
        [self requestOrdersList];
    }
    
    return _orders;
}

- (NSMutableArray *)favoriteRestaurants
{
    if(_favoriteRestaurants == nil){
        _favoriteRestaurants = [NSMutableArray array];
    }
    
    if(self.isFavoritedDirty || !self.isFavoritedFetched){
        if(CRED.isSignedIn){
            [self requestFavoriteRestaurant];
        }
    }
    
    return _favoriteRestaurants;
}

- (NSMutableArray *)deliveryAddresses
{
    if(_deliveryAddresses == nil){
        _deliveryAddresses = [NSMutableArray array];
    }
    
    if(self.isDeliveryAddressDirty || !self.isDeliveryAddressFetched){
        [self requestDeliveryAddresses];
    }
    
    return _deliveryAddresses;
}

- (BOOL)requestCardList
{
    if(self.isCardProgressing){
        PCWarning(@"Requesting visited restaurant is on the progress");
        return NO;
    }
    
    self.cardProgressing = YES;
    
    RequestResult result = RRFail;
    result = [PAY requestCardListWithCompletionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              [self handleCardsResponse:(NSMutableArray *)response
                                       withNotification:YES];
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting cards", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
                  self.cardProgressing = NO;
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            self.cardProgressing = NO;
            break;
        default:
            break;
    }
    
    return YES;
}

- (void)createCustomRequestByOrder:(Order *)order
{
    if([order.customerRequest length] > 0){
        CustomRequestType type = CustomRequestTypeUnknown;
        switch(order.orderType){
            case OrderTypeDelivery: type = CustomRequestTypeDelivery; break;
            case OrderTypeTakeout: type = CustomRequestTypeTakeout; break;
            case OrderTypePickup: type = CustomRequestTypeDineIn; break;
            default: type = CustomRequestTypeUnknown;
        }
        [CustomRequest updateCustomRequest:order.customerRequest
                                      type:type
                                merchantNo:order.merchantNo];
    }
}

- (BOOL)requestOrdersList
{
    if(REACH.status == NotReachable){
        PCWarning(@"!!!!======== Network not reachable ========= !!!!!");
        return NO;
    }
    
    if(self.isOrdersProgressing){
        PCWarning(@"Requesting orders is on the progress");
        return NO;
    }
    
    self.ordersProgressing = YES;
    
    RequestResult result = RRFail;
     result = [PAY requestOrdersAt:(self.currentPage + 1)
                   completionBlock:
               ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                   
                   if(isSuccess && 200 <= statusCode && statusCode < 300){
                       switch(response.errorCode){
                           case ResponseSuccess:
                           {
                               self.ordersDirty = NO;
                               if(self.currentPage == 0){
                                   self.orders = nil;
                               }
                               
                               NSArray *completedOrders = [response objectForKey:@"completed"];
                               
                               [self.orders addObjectsFromArray:completedOrders];
                               
                               self.ordersFetched = YES;
                               self.currentPage++;
                               
                               if([completedOrders count] < RUSHORDER_PAGE_COUNT){
                                   self.moredata = NO;
                               } else {
                                   self.moredata = YES;
                               }
                               
                               if(![NSUserDefaults standardUserDefaults].customRequestSynced){
                                   
                                   NSArray *comOrders = [response objectForKey:@"completed"];
                                   NSArray *actOrders = [response objectForKey:@"active"];
                                   
                                   for(Order *order in comOrders){
                                       if(![order isKindOfClass:[Order class]]) continue;
                                       
                                       [self createCustomRequestByOrder:order];
                                   }
                                   
                                   for(Order *order in actOrders){
                                       if(![order isKindOfClass:[Order class]]) continue;
                                       [self createCustomRequestByOrder:order];
                                   }
                                   
                                   [NSUserDefaults standardUserDefaults].customRequestSynced = YES;
                               }
                               [[NSNotificationCenter defaultCenter] postNotificationName:OrdersListChangeNotification
                                                                                   object:self
                                                                                 userInfo:nil];
                               
                           }
                               break;
                           default:{
                               NSString *message = [response objectForKey:@"message"];
                               if([message isKindOfClass:[NSString class]]){
                                   [UIAlertView alert:message];
                               } else {
                                   [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting orders", nil)];
                               }
                           }
                               break;
                       }
                   }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                       [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                       //Signed out in requestMyCreditWithCompletionBlock
                   } else {
                       if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                           [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                               object:nil];
                       } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                           [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                               ,statusCode]];
                       }
                   }

                   self.ordersProgressing = NO;
                   [SVProgressHUD dismiss];
                   
               }];
     switch(result){
         case RRSuccess:
             [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
             return YES;
             break;
         case RRParameterError:
             [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
             self.ordersProgressing = NO;
             return NO;
             break;
         default:
             self.ordersProgressing = NO;
             return NO;
             break;
     }
}

- (BOOL)requestDeliveryAddresses
{
    if(self.isCardProgressing){
        PCWarning(@"Requesting visited restaurant is on the progress");
        return NO;
    }
    
    self.cardProgressing = YES;
    
    RequestResult result = RRFail;
    result = [PAY requestDeliveryAddressesWithCompletionBlock:^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              [self handleDeliveryAddressesResponse:(NSMutableArray *)response
                                                   withNotification:YES];
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              NSString *title = [response objectForKey:@"title"];
                              if(title == nil){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alertWithTitle:title
                                                      message:message];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
                  self.cardProgressing = NO;
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            self.cardProgressing = NO;
            break;
        default:
            break;
    }
    return (result == RRSuccess);
}

- (BOOL)requestFavoriteRestaurant
{
    if(self.isDeliveryAddressProgressing){
        PCWarning(@"Requesting delivery addresses is on the progress");
        return NO;
    }
    
    self.deliveryAddressProgressing = YES;
    
    RequestResult result = RRFail;
    result = [PAY requestFavoriteMerchantsWithCurrentLocation:APP.location
                                              completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              [self handleFavoriteResponse:(NSMutableArray *)response
                                          withNotification:YES];
                              self.favoritedFetched = YES;
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting favorites", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
                  self.favoritedProgressing = NO;
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            self.favoritedProgressing = NO;
            break;
        default:
            self.favoritedProgressing = NO;
            break;
    }
    
    return YES;
}

- (void)handleFavoriteResponse:(NSMutableArray *)restaurantList
{
    [self handleFavoriteResponse:restaurantList
                withNotification:NO];
}

- (void)handleFavoriteResponse:(NSMutableArray *)restaurantList
              withNotification:(BOOL)withNotification
{
    self.favoriteRestaurants = restaurantList;
    self.favoritedDirty = NO;
    self.favoritedProgressing = NO;
    self.favoritedFetched = YES;
    
    if(withNotification){
        [[NSNotificationCenter defaultCenter] postNotificationName:FavoritedRestaurantsListChangeNotification
                                                            object:self];
    }
}

- (void)handleCardsResponse:(NSMutableArray *)cardList
              withNotification:(BOOL)withNotification
{
    self.cards = cardList;
    self.cardDirty = NO;
    self.cardProgressing = NO;
    self.cardFetched = YES;
    
    if(withNotification){
        [[NSNotificationCenter defaultCenter] postNotificationName:CardListChangeNotification
                                                            object:self];
    }
}

- (void)handleDeliveryAddressesResponse:(NSMutableArray *)deliveryAddresses
                       withNotification:(BOOL)withNotification
{
    self.deliveryAddresses = deliveryAddresses;
    self.deliveryAddressDirty = NO;
    self.deliveryAddressProgressing = NO;
    self.deliveryAddressFetched = YES;
    
    self.defaultDeliveryAddress = nil;
    
    for(DeliveryAddress *deliveryAddress in self.deliveryAddresses){
        if(deliveryAddress.isDefaultAddress){
            self.defaultDeliveryAddress = deliveryAddress;
            break;
        }
    }
    
    if(withNotification){
        [[NSNotificationCenter defaultCenter] postNotificationName:DeliveryAddressChangeNotification
                                                            object:self];
    }
}

- (BOOL)isFavoriteMerchant:(Merchant *)aMerchant
{
    for(Merchant *favoriteMerchant in REMOTE.favoriteRestaurants){
        if(aMerchant.merchantNo == favoriteMerchant.merchantNo){
            return YES;
        }
    }
    return NO;
}

@end
