//
//  AccountSwitchingManager.m
//  RushOrder
//
//  Created by Conan on 6/10/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "AccountSwitchingManager.h"
#import "RemoteDataManager.h"
#import "PCMenuService.h"
#import "PCPaymentService.h"

NSString * const AccountDataSwitchedNotification = @"AccountDataSwitchedNotification";

typedef enum syncStatus_{
    SyncStatusUnknown = 0,
    SyncStatusDirty = 1 << 0,
    SyncStatusClean = 1 << 1,
    SyncStatusSyncing = 1 << 2,
    SyncStatusSynced = 1 << 3,
    SyncStatusFailed = 1 << 4,
    SyncStatusDone = SyncStatusSynced | SyncStatusFailed | SyncStatusClean,
    SyncStatusSuccess = SyncStatusSynced | SyncStatusClean,
} SyncStatus;

@interface AccountSwitchingManager()

@property (nonatomic, strong) NSArray *favoriteMerchants;
@property (nonatomic, strong) NSArray *visitedMerchants;
@property (nonatomic, strong) NSArray *receipts;

@property (nonatomic) SyncStatus favoriteStatus;
@property (nonatomic) SyncStatus visitedStatus;
@property (nonatomic) SyncStatus receiptStatus;

@end

@implementation AccountSwitchingManager

+ (AccountSwitchingManager *)sharedManager
{
    static AccountSwitchingManager *aManager = nil;
    
    @synchronized(self){
        if(aManager == nil){
            aManager = [[self alloc] init];
        }
    }
    
    return aManager;
}

- (id)init
{
    self = [super init];
    if(self != nil){
        
    }
    
    return self;
}

- (BOOL)initSyncData
{
    self.favoriteStatus = SyncStatusDirty;
    self.visitedStatus = SyncStatusDirty;
    self.receiptStatus = SyncStatusDirty;
    
//    self.favoriteMerchants = [Merchant listWithCondition:
//                                  [NSString stringWithFormat:@"where cachedType = %d", MerchantCachedTypeFavorite]];
//    
//    self.visitedMerchants = [Merchant listWithCondition:
//                                 [NSString stringWithFormat:@"where cachedType = %d", MerchantCachedTypeVisited]];
    
//    self.receipts = [Receipt listWithCondition:@"where receiptNo != 0"];
    
    self.syncRangeString = nil;
    
    return [self.favoriteMerchants count] > 0 || [self.visitedMerchants count] > 0 || [self.receipts count] > 0;
}

- (NSMutableString *)syncRangeString
{
    if(_syncRangeString == nil){
        _syncRangeString = [NSMutableString string];
        
        if([self.favoriteMerchants count] > 0){
            [_syncRangeString appendString:@"favorites, "];
        }
        
        if([self.visitedMerchants count] > 0){
            [_syncRangeString appendString:@"visited stores, "];
        }
        
        if([self.receipts count] > 0){
            [_syncRangeString appendString:@"receipts, "];
        }
        
        [_syncRangeString deleteCharactersInRange:NSMakeRange([_syncRangeString length] - 2, 2)];
    }
    
    return _syncRangeString;
}

- (BOOL)removeLocalData
{
    return [self removeLocalDataIncludeCard:YES];
}

- (BOOL)removeLocalDataIncludeCard:(BOOL)includeCard
{
//    [Merchant deleteWithCondition:
//     [NSString stringWithFormat:@"cachedType = %d", MerchantCachedTypeFavorite]];
//    
//    [Merchant deleteWithCondition:
//     [NSString stringWithFormat:@"cachedType = %d", MerchantCachedTypeVisited]];
    
    [Merchant changeToUnknownMerchant];
    
    [Receipt deleteWithCondition:@"1"];
    
    if(includeCard){
        [MobileCard deleteWithCondition:@"1"];
    }
    return YES;
}

- (BOOL)startSync
{
    [MobileCard deleteWithCondition:@"1"];
    
    BOOL atLeastOneStarted = NO;
    if([self.favoriteMerchants count] > 0 || self.favoriteStatus == SyncStatusDirty){
        if([self requestAddFavorites]){
            atLeastOneStarted = YES;
        }
    } else {
        self.favoriteStatus = SyncStatusClean;
    }
    
    if([self.visitedMerchants count] > 0 || !self.visitedStatus == SyncStatusDirty){
        if([self requestAddVisited]){
            atLeastOneStarted = YES;
        }
    } else {
        self.visitedStatus = SyncStatusClean;
    }
    
    if([self.receipts count] > 0 || !self.receiptStatus == SyncStatusDirty){
        if([self requestAddReceipts]){
            atLeastOneStarted = YES;
        }
    } else {
        self.receiptStatus = SyncStatusClean;
    }
    
    if(atLeastOneStarted){
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    }
    
    [Merchant changeToUnknownMerchant];
    
    return YES;
}

- (BOOL)requestAddFavorites
{
    NSMutableArray *array = [NSMutableArray array];
    
    for(Merchant *favorite in self.favoriteMerchants){
        [array addObject:[NSNumber numberWithSerial:favorite.merchantNo]];
    }
    
    RequestResult result = RRFail;
    result = [PAY requestAddFavorites:array
                      currentLocation:APP.location
                      completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              [REMOTE handleFavoriteResponse:(NSMutableArray *)response
                                            withNotification:YES];
                            
                              self.favoriteStatus = SyncStatusSynced;
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while adding favoriets", nil)];
                              }
                              self.favoriteStatus = SyncStatusFailed;
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                      self.favoriteStatus = SyncStatusFailed;
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                      self.favoriteStatus = SyncStatusFailed;
                  }
                  [self checkDoneAndNotice];
              }];
    switch(result){
        case RRSuccess:
            
            return YES;
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            self.favoriteStatus = SyncStatusFailed;
            [self checkDoneAndNotice];
            return NO;
            break;
        default:
            break;
    }
    return NO;
}

- (BOOL)requestAddVisited
{
    NSMutableArray *array = [NSMutableArray array];
    
    for(Merchant *visited in self.visitedMerchants){
        [array addObject:[NSNumber numberWithSerial:visited.merchantNo]];
    }
    
    RequestResult result = RRFail;
    result = [PAY requestAddVisited:array
                      currentLocation:APP.location
                      completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
//
//                              [REMOTE handleVisitedResponse:(NSMutableArray *)response
//                                            withNotification:YES];
                              
//                              [Merchant deleteWithCondition:
//                               [NSString stringWithFormat:@"cachedType = %d", MerchantCachedTypeVisited]];
                              
                              self.visitedStatus = SyncStatusSynced;
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while adding visited", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                      self.visitedStatus = SyncStatusFailed;
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                      self.visitedStatus = SyncStatusFailed;
                  }
                  [self checkDoneAndNotice];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            return YES;
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            self.visitedStatus = SyncStatusFailed;
            [self checkDoneAndNotice];
            return NO;
            break;
        default:
            break;
    }
    return NO;
}

- (BOOL)requestAddReceipts
{
    NSMutableArray *array = [NSMutableArray array];
    
    for(Receipt *receipt in self.receipts){
        [array addObject:[NSNumber numberWithSerial:receipt.receiptNo]];
    }
    
    RequestResult result = RRFail;
    result = [PAY requestAddReceipts:array
                    completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
//                              
//                              [REMOTE handleReceiptsResponse:(NSMutableArray *)response
//                                           withNotification:YES];
                              
                              
                              [Receipt deleteWithCondition:@"1"];
                              
                              self.receiptStatus = SyncStatusSynced;
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while updating receipts", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                      self.receiptStatus = SyncStatusFailed;
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                      self.receiptStatus = SyncStatusFailed;
                  }
                  [self checkDoneAndNotice];
              }];
    switch(result){
        case RRSuccess:
            
            return YES;
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            self.receiptStatus = SyncStatusFailed;
            [self checkDoneAndNotice];
            return NO;
            break;
        default:
            break;
    }
    return NO;
}

- (void)checkDoneAndNotice
{
    if(((self.favoriteStatus & SyncStatusDone) == self.favoriteStatus) &&
       ((self.visitedStatus & SyncStatusDone) == self.visitedStatus) &&
       ((self.receiptStatus & SyncStatusDone) == self.receiptStatus)
       ){
        
        [SVProgressHUD dismiss];
        
        if(((self.favoriteStatus & SyncStatusSuccess) == self.favoriteStatus) &&
           ((self.visitedStatus & SyncStatusSuccess) == self.visitedStatus) &&
           ((self.receiptStatus & SyncStatusSuccess) == self.receiptStatus)
           ){
            // Succeedeed all
            
            [[NSNotificationCenter defaultCenter] postNotificationName:AccountDataSwitchedNotification
                                                                object:self
                                                              userInfo:@{@"Result":@YES}];
            
        } else {
            // Something Failed
            
            [[NSNotificationCenter defaultCenter] postNotificationName:AccountDataSwitchedNotification
                                                                object:self
                                                              userInfo:@{@"Result":@NO}];
        }
        
    } else {
        // Wait..
        
    }
}

@end