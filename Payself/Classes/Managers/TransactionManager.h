//
//  TransactionManager.h
//  RushOrder
//
//  Created by Conan on 6/10/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TRANS [TransactionManager sharedManager]

extern NSString * const TransactionListChangedNotification;

@interface TransactionManager : NSObject

@property(strong, nonatomic) NSMutableArray *transactions;
@property(strong, nonatomic) NSMutableArray *activeOrders;
@property(nonatomic, getter = isDirty) BOOL dirty;
@property(nonatomic, getter = isLocalDirty) BOOL localDirty;
@property(nonatomic, getter = isActiveOrderDirty) BOOL activeOrderDirty;

+ (TransactionManager *)sharedManager;

- (BOOL)requestMyOrders;
- (BOOL)requestMyOrder:(Order *)order;
- (BOOL)requestUploadMyOrders;

- (Order *)orderInTransactionsByOrderNo:(PCSerial)orderNo;
- (id)activeOrderAtMerchantNo:(PCSerial)merchantNo;
- (id)activeLocalCartAtMerchantNo:(PCSerial)merchantNo;

@end