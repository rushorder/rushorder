//
//  TransactionManager.m
//  RushOrder
//
//  Created by Conan on 6/10/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "TransactionManager.h"
#import "PCMenuService.h"
#import "BlockUtil.h"
#import "PCCredentialService.h"

NSString * const TransactionListChangedNotification = @"TransactionListChangedNotification";

@interface TransactionManager()
@property (nonatomic, getter = isProgressing) BOOL progressing;
@end

@implementation TransactionManager

+ (TransactionManager *)sharedManager
{
    static TransactionManager *aManager = nil;
    
    @synchronized(self){
        if(aManager == nil){
            aManager = [[self alloc] init];
        }
    }
    
    return aManager;
}

- (id)init
{
    self = [super init];
    if(self != nil){
        self.dirty = YES;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(lineItemUpdated:)
                                                     name:LineItemChangedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(signedIn:)
                                                     name:SignedInNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(signedOut:)
                                                     name:SignedOutNotification
                                                   object:nil];
    }
    return self;
}

- (void)signedIn:(NSNotification *)notification
{
    [TRANS requestUploadMyOrders];
}

- (void)signedOut:(NSNotification *)notification
{
    self.dirty = YES;
    [self requestMyOrders];
}

- (void)lineItemUpdated:(NSNotification *)aNoti
{
    if([aNoti.object isKindOfClass:[TableInfo class]]){
        
        TableInfo *tableInfo = (TableInfo *)aNoti.object;
        Order *order = [aNoti.userInfo objectForKey:@"order"];
        Cart *cart = [aNoti.userInfo objectForKey:@"cart"];
        
        Order *foundOrder = nil;
        Cart *foundCart = nil;
        
        NSInteger idx = 0;
        
        BOOL isChanged = NO;
        
        for(id obj in self.transactions){
            if([obj isKindOfClass:[Cart class]]){
                Cart *castedCart = (Cart *)obj;

                if([tableInfo.tableNumber isCaseInsensitiveEqual:castedCart.tableLabel] && tableInfo.merchantNo == castedCart.merchantNo){
                        foundCart = castedCart;
                    break;
                }
            } else if([obj isKindOfClass:[Order class]]){
                Order *castedOrder = (Order *)obj;

                if([tableInfo.tableNumber isCaseInsensitiveEqual:castedOrder.tableNumber] && tableInfo.merchantNo == castedOrder.merchantNo){
                        foundOrder = castedOrder;
                    break;
                }
            }
            idx++;
        }
        
        
        if(order == nil && cart == nil){
            if(foundOrder != nil || foundCart != nil){
                self.dirty = YES; // Removed or completed case
                
                // Transaction Manager에서 Alert를 할 필요가 있을까? 일단 이상동작하여 주석처리
//                [UIAlertView alertWithTitle:NSLocalizedString(@"This Order(list) is not valid", nil)
//                                    message:NSLocalizedString(@"This Order(list) may be removed or completed with some reasons. Please ask the restaurant what to do.", nil)
//                                   delegate:self
//                                        tag:101];
            } else {
                foundCart.status = cart.status;
                foundOrder.status = order.status;
                isChanged = YES;
            }
        } else if(order != nil && cart != nil){ //추가주문상태라면
            if(foundCart.cartNo == cart.cartNo){ // 이미 cart가 있는 경우는 1. 추가주문이거나, 2. 그냥 주문일 경우인데 카트번호가 같다면 추가주문이므로 상태만 업데이트하고 아닌 경우 다시 받아와야함
                foundCart.status = cart.status;
                isChanged = YES;
            } else {
                self.dirty = YES;
            }
        } else if(order != nil) { // Impose cart is nil by above condition
            if(foundOrder != nil){
                foundOrder.status = order.status;
                isChanged = YES;
            } else {
                self.dirty = YES;
            }
        } else if (cart != nil) { // Impose order is nil by above condition
            if(foundCart != nil && cart.cartNo == foundCart.cartNo){
                foundCart.status = cart.status;
                isChanged = YES;
            } else {
                self.dirty = YES;
            }
        }
        
        if(isChanged){
            
            NSDictionary *userInfo = nil;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:TransactionListChangedNotification
                                                                object:self
                                                              userInfo:userInfo];
        }
    }
}


- (Order *)orderInTransactionsByOrderNo:(PCSerial)orderNo
{
    for(id orderOrCart in self.transactions){
        if([orderOrCart isKindOfClass:[Order class]]){
            Order *castedOrder = (Order *)orderOrCart;
            if(castedOrder.orderNo == orderNo){
                return castedOrder;
            }
        }
}
    return nil;
}

- (id)activeOrderAtMerchantNo:(PCSerial)merchantNo
{
    for(id orderOrCart in self.transactions){
        if([orderOrCart isKindOfClass:[Order class]]){
            Order *castedOrder = (Order *)orderOrCart;
            if(castedOrder.merchant.merchantNo == merchantNo){
                return orderOrCart;
            }
        } else if([orderOrCart isKindOfClass:[Cart class]]){
            Cart *castedCart = (Cart *)orderOrCart;
            if(castedCart.merchant.merchantNo == merchantNo){
                return orderOrCart;
            }
        }
    }
    return nil;
}

- (id)activeLocalCartAtMerchantNo:(PCSerial)merchantNo
{
    for(id orderOrCart in self.transactions){
        if([orderOrCart isKindOfClass:[Cart class]]){
            Cart *castedCart = (Cart *)orderOrCart;
            if(castedCart.merchant.merchantNo == merchantNo
               && castedCart.isLocal){
                return orderOrCart;
            }
        }
    }
    return nil;
}

- (BOOL)requestMyOrder:(Order *)order
{
    RequestResult result = RRFail;
    result = [MENU requestMyOrderId:order.orderNo
                          authToken:CRED.customerInfo.authToken
                    completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              [order updateWithDictionary:response
                                                 merchant:nil];
                              
                              NSArray *before = self.transactions;
                              
                              NSDictionary *userInfo = [self changedListFrom:before
                                                                          to:self.transactions];
                              
                              [[NSNotificationCenter defaultCenter] postNotificationName:TransactionListChangedNotification
                                                                                  object:self
                                                                                userInfo:userInfo];
                          }
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting order detail", nil)];
                              }
                          }
                              break;
                      }
                  }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      //Signed out in requestMyCreditWithCompletionBlock
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            return YES;
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            return NO;
            break;
        default:
            return NO;
            break;
    }
}

// Return if the loading mark should be stopped - NO (stop), YES (will be notified - guarantee)
- (BOOL)requestMyOrders
{
    // !!!:addLocalCart should be called at any cases,
    // !!!:force parameter should be set NO when transaction is not changed by network
    // !!!:Other YES (remove existing, append)
    
    if(!self.isDirty){
        [self addLocalCart:NO];
        return YES;
    }
    
    if(self.isProgressing)
        return NO;
    
    self.progressing = YES;
    
    RequestResult result = RRFail;
    result = [MENU requestMyOrderListAuthToken:CRED.customerInfo.authToken
                               completionBlock:
              ^(BOOL isSuccess, NSMutableDictionary *response, NSInteger statusCode){
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              self.dirty = NO;

                              NSArray *before = self.transactions;
                              
                              NSMutableArray *orderCartList = [response objectForKey:@"OrderCartList"];
                              self.transactions = orderCartList;
                              self.activeOrderDirty = YES;
                              [self addLocalCart:YES];

                              NSDictionary *userInfo = [self changedListFrom:before
                                                                          to:self.transactions];
                              
                              if([self.transactions count] > 0){
                                  ((UITabBarItem *)APP.tabBarController.tabBar.items[3]).badgeValue = [NSString stringWithFormat:@"%lu",[self.transactions count]];
                              } else {
                                  ((UITabBarItem *)APP.tabBarController.tabBar.items[3]).badgeValue = nil;
                              }
                              
                              if(CRED.isSignedIn){
                                  CRED.customerInfo.referralBadgeCount = [response integerForKey:@"referral_badge_count"];
                                  
                              }
                              
                              if(CRED.customerInfo.referralBadgeCount > 0){
                                  ((UITabBarItem *)APP.tabBarController.tabBar.items[4]).badgeValue = [NSString stringWithFormat:@"%lu",CRED.customerInfo.referralBadgeCount];
                              } else {
                                  ((UITabBarItem *)APP.tabBarController.tabBar.items[4]).badgeValue = nil;
                              }
                              
                              
                              [[NSNotificationCenter defaultCenter] postNotificationName:TransactionListChangedNotification
                                                                                  object:self
                                                                                userInfo:userInfo];
                              break;
                          }
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              [self addLocalCart:NO]; //Push notification will be sent in this method
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting my orders.", nil)];
                              }
                              [self addLocalCart:NO];
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      [CRED resetUserAccount];
                      [self addLocalCart:NO];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                      [self addLocalCart:NO];
                  }
                  
                  self.progressing = NO;
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            
            return YES;
            
            break;
        case RRParameterError:
            self.progressing = NO;
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            [self addLocalCart:NO];
            
            return YES;
            
            break;
        case RRNotReachable:
            // Wait...
            self.progressing = NO;
            [self addLocalCart:NO];
            return YES;
            break;
        default:
            
            self.progressing = NO;
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Error %d", result]];
            [self addLocalCart:NO];
            
            return YES;
            
            break;
    }
}

- (BOOL)requestUploadMyOrders
{
    self.progressing = YES;
    
    RequestResult result = RRFail;
    result = [MENU requestAddOrdersAuthToken:CRED.customerInfo.authToken
                             completionBlock:
              ^(BOOL isSuccess, NSMutableDictionary *response, NSInteger statusCode){
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              self.progressing = NO;
                              self.dirty = YES;
                              [self requestMyOrders];
                              
//                              self.dirty = NO;
//
//                              NSArray *before = self.transactions;
//                              
//                              NSMutableArray *orderCartList = [response objectForKey:@"active"];
//                              self.transactions = orderCartList;
//                              self.activeOrderDirty = YES;
//
//                              [self addLocalCart:YES];
//                              
//                              NSDictionary *userInfo = [self changedListFrom:before
//                                                                          to:self.transactions];
//                              
//                              [[NSNotificationCenter defaultCenter] postNotificationName:TransactionListChangedNotification
//                                                                                  object:self
//                                                                                userInfo:userInfo];
                              break;
                          }
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              [self addLocalCart:NO]; //Push notification will be sent in this method
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting my orders.", nil)];
                              }
                              [self addLocalCart:NO];
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                      [self addLocalCart:NO];
                  }
                  
                  self.progressing = NO;
              }];
    switch(result){
        case RRSuccess:
            return YES;
            break;
        case RRParameterError:
            self.progressing = NO;
            [self addLocalCart:NO];
            return YES;
            break;
        case RRNotReachable:
            // Wait...
            self.progressing = NO;
            [self addLocalCart:NO];
            return YES;
            break;
        default:
            self.progressing = NO;
            [self addLocalCart:NO];
            return YES;
            break;
    }
}

- (void)addLocalCart:(BOOL)force
{
//    NSMutableArray *inserted = [NSMutableArray array];
//    NSMutableArray *removed = [NSMutableArray array];
    
    NSArray *bfArray = nil;
    
    if(!force){
        
        if(!self.isLocalDirty){
            [[NSNotificationCenter defaultCenter] postNotificationName:TransactionListChangedNotification
                                                                object:self
                                                              userInfo:nil];
            return;
        }
        
        bfArray = [NSArray arrayWithArray:self.transactions];
        
        for(NSInteger i = [self.transactions count] - 1; i >= 0; i--){
            id orderOrCart = [self.transactions objectAtIndex:i];
            if([orderOrCart isKindOfClass:[Cart class]]){
                Cart *castedCart = (Cart *)orderOrCart;
                if(castedCart.cartNo == 0){
                    [self.transactions removeObjectAtIndex:i];
                }
            }
        }
    }
    
    NSArray *activePickupCart = [Cart listWithCondition:@" where cartNo = 0"];
    
    if(self.transactions == nil){
        self.transactions = [NSMutableArray array];
        self.activeOrderDirty = YES;
    }
    
    for(Cart *aCart in activePickupCart){
        aCart.merchant = [Merchant instanceWithCondition:[NSString stringWithFormat:@" where merchantNo = %lld", aCart.merchantNo]];
        NSMutableArray *lineItems = [LineItem listWithCondition:[NSString stringWithFormat:@"where merchantNo = %lld and cartNo = 0 and lineItemNo = 0", aCart.merchantNo]];
        aCart.lineItems = lineItems;
        [aCart summation];
        
        NSInteger insertIndex = [self.transactions indexOfObject:aCart
                                                   inSortedRange:NSMakeRange(0, [self.transactions count])
                                                         options:NSBinarySearchingInsertionIndex
                                                 usingComparator:orderOrCartComparator];
        [self.transactions insertObject:aCart
                                atIndex:insertIndex];
    }
    
    if(!force){
        
        if([self.transactions count] > 0){
            ((UITabBarItem *)APP.tabBarController.tabBar.items[3]).badgeValue = [NSString stringWithFormat:@"%lu",[self.transactions count]];
        } else {
            ((UITabBarItem *)APP.tabBarController.tabBar.items[3]).badgeValue = nil;
        }
        
        NSDictionary *userInfo = [self changedListFrom:bfArray
                                                    to:self.transactions];
        [[NSNotificationCenter defaultCenter] postNotificationName:TransactionListChangedNotification
                                                            object:self
                                                          userInfo:userInfo];
    }
    
    self.localDirty = NO;
}

- (NSDictionary *)changedListFrom:(NSArray *)from to:(NSArray *)to
{
    NSMutableArray *inserted = [NSMutableArray array];
    NSMutableArray *removed = [NSMutableArray array];
    NSMutableArray *updated = [NSMutableArray array];
    
    NSInteger i = 0;
    NSInteger j = 0;
    
    while(YES){
     
        id fromObj = nil;
        id toObj = nil;
        
        BOOL done = YES;
        if([from count] > i){
            done = NO;
            fromObj = [from objectAtIndex:i];
        }
        
        if([to count] > j){
            done = NO;
            toObj = [to objectAtIndex:j];
        }
        
        if(done) break; //exit
        
        if(fromObj != nil && toObj != nil){

            NSComparisonResult rtn = orderOrCartComparator(fromObj, toObj);

            switch(rtn){
                case NSOrderedAscending:
                    [removed addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                    i++;
                    break;
                case NSOrderedDescending:
                    [inserted addObject:[NSIndexPath indexPathForRow:j inSection:0]];
                    j++;
                    break;
                case NSOrderedSame:
                {
                    if([fromObj isKindOfClass:[Cart class]]){
                        Cart *fromCart = (Cart *)fromObj;
                        Cart *toCart = (Cart *)toObj;
                        if(fromCart.status != toCart.status){
                            [updated addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                        }
                    } else if ([fromObj isKindOfClass:[Order class]]){
                        Order *fromOrder = (Order *)fromObj;
                        Order *toOrder = (Order *)toObj;
                        if(fromOrder.status != toOrder.status){
                            [updated addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                        }
                    }
                }
                    i++; j++;
                    break;
            }
        } else if(fromObj == nil && toObj != nil){
            [inserted addObject:[NSIndexPath indexPathForRow:j inSection:0]];
            j++;
        } else { // toObj == nil
            [removed addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            i++;
        }
    }
    
    return @{@"insertedKey" : inserted,
             @"removedKey" : removed,
             @"updatedKey" : updated};
}

- (void)setDirty:(BOOL)value
{
    _dirty = value;
}

- (NSMutableArray *)activeOrders
{
    if(_activeOrders == nil || self.isActiveOrderDirty){
        _activeOrders = [NSMutableArray array];
        for(id obj in self.transactions){
            if([obj isKindOfClass:[Order class]]){
                [_activeOrders addObject:obj];
            }
        }
    }
    return _activeOrders;
}


@end
