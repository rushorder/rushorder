//
//  OrderManager.m
//  RushOrder
//
//  Created by Conan on 5/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "OrderManager.h"
#import "MenuPrice.h"
#import "MenuCategory.h"
#import "MenuSubCategory.h"
#import "MenuOptionGroup.h"
#import "MenuOption.h"
#import "PCMenuService.h"
#import "Merchant.h"
#import "MenuManager.h"
#import "PCPaymentService.h"
#import "Addresses.h"
#import "PCCredentialService.h"
#import "RemoteDataManager.h"

NSString * const TableListChangedNotification = @"TableListChangedNotification";
NSString * const MerchantInformationUpdatedNotification = @"MerchantInformationUpdatedNotification";
NSString * const OrderTypeSwitchedNotification = @"OrderTypeSwitchedNotification";

@interface OrderManager(){
    PCCurrency _tempDiscount;
}
@property (nonatomic, getter = isDirty) BOOL dirty;
@property (nonatomic, getter = isTableLablesAsking) BOOL tableLablesAsking;
@end

@implementation OrderManager
+ (OrderManager *)sharedManager
{
    static OrderManager *aManager = nil;
    
    @synchronized(self){
        if(aManager == nil){
            aManager = [[self alloc] init];
        }
    }
    
    return aManager;
}

- (id)init
{
    self = [super init];
    if(self != nil){
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didBecomeActive:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didEnterBackground:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(orderConfirmed:)
                                                     name:OrderConfirmedNotificationKey
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(signedIn:)
                                                     name:SignedInNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(signedOut:)
                                                     name:SignedOutNotification
                                                   object:nil];
        self.tableLablesAsking = NO;
    }
    
    return self;
}

- (void)dealloc
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)signedIn:(NSNotification *)aNoti
{
    self.lastUsedDeliveryAddress = nil;
}

- (void)signedOut:(NSNotification *)aNoti
{
    self.lastUsedDeliveryAddress = nil;
}

- (void)didBecomeActive:(NSNotification *)aNoti
{
    if(self.isDirty){

    }
}

- (void)didEnterBackground:(NSNotification *)aNoti
{
    self.dirty = YES;
}

- (void)orderConfirmed:(NSNotification *)aNoti
{
    PCSerial merchantNo = [aNoti.userInfo serialForKey:@"merchant_id"];
    if(self.merchant.merchantNo == merchantNo){
        if(self.tableInfo != nil){
            [self checkTable:self.tableInfo];
        } else {

        }
    } else {
        // Other restaurant's push notification
    }
}

- (void)resetGraphWithMerchant:(Merchant *)merchant
{
    self.merchant = merchant;
    [self resetGraph];
}

- (void)resetGraph
{
    self.tableInfo = nil;
    self.tableList = nil;
    self.order = nil;
    self.cart = nil;
    self.tempDeliveryAddress = nil;
    self.lastUsedDeliveryAddress = nil;
    self.tempAfterMinute = 0;
    self.tempCustomerRequest = nil;
    self.flagNumber = nil;
    self.lastUpdatedDate = [NSDate date];
}

- (Cart *)cart
{
    if(_cart == nil){
        _cart = [[Cart alloc] init];
        _cart.merchant = self.merchant;
        _cart.nonInteractive = self.merchant.isNonInteractive;
        _cart.merchantNo = self.merchant.merchantNo;
        _cart.tableLabel = self.tableInfo.tableNumber;
    }
    return _cart;
}

- (PCCurrency)tempSubtotal
{
    return self.cart.amount + self.order.subTotal;
}

- (NSString *)tempSubtotalString
{
    return [[NSNumber numberWithCurrency:self.tempSubtotal] currencyString];
}

- (PCCurrency)tempTaxes
{
    return self.cart.taxAmount + self.order.taxes;
}

- (NSString *)tempTaxesString
{
    return [[NSNumber numberWithCurrency:self.tempTaxes] currencyString];
}

- (PCCurrency)tempDiscount
{
    PCCurrency total = self.tempSubtotal;
    
    if(self.merchant.promotion != nil){
        if(CRED.isSignedIn){
            
            for(Promotion *promotion in [self.merchant promotionsOnCartType:self.cart.cartType]){
                
                if(!promotion.isActive){
                    continue;
                }
                
                if([promotion.endDate timeIntervalSinceDate:[NSDate date]] < 0){
                    continue;
                }
                
                if([promotion.startDate timeIntervalSinceDate:[NSDate date]] > 0){
                    continue;
                }
                
                if(promotion.minOrderAmount > total){
                    continue;
                }
                
                PCCurrency appliedDiscountAmount = 0;
                if(promotion.isFlatDiscount){
                    appliedDiscountAmount = MIN(promotion.discountAmount, (total - appliedDiscountAmount));
                } else {
                    PCCurrency limitedDiscountAmount = roundf(total * (promotion.discountRate / 100.0f));
//                    if(promotion.limitDiscountAmount > 0){
                        limitedDiscountAmount = MIN(promotion.limitDiscountAmount, limitedDiscountAmount);
//                    }
                    appliedDiscountAmount = MIN(limitedDiscountAmount, (total - appliedDiscountAmount));
                }
                _tempDiscount += appliedDiscountAmount;
                
                if(_tempDiscount >= total){
                    break;
                }
            }
        } else {

        }
    } else {
        
    }
    
    return _tempDiscount;
}

- (void)setTempDiscount:(PCCurrency)value
{
    _tempDiscount = value;
}

- (NSString *)tempDiscountString
{
    return [[NSNumber numberWithCurrency:-self.tempDiscount] currencyString];
}

- (PCCurrency)tempTotal
{
    return self.tempTaxes + self.tempSubtotal - _tempDiscount;
}

- (NSString *)tempTotalString
{
    return [[NSNumber numberWithCurrency:self.tempTotal] currencyString];
}

- (BOOL)canCheckOut
{
    return ([self.order.lineItems count] > 0 && [self.cart.lineItems count] == 0);
}

- (NSArray *)tableList
{
//    if(_tableList == nil){
//        [self requestTableLables];
//    }
//    return _tableList;
    return self.merchant.tableList;
}

//- (void)requestTableLables
//{
//    if(self.tableLablesAsking){
//        return;
//    }
//
//    RequestResult result = RRFail;
//    
//    result = [PAY requestTableLabels:self.merchant.merchantNo
//                     completionBlock:
//              ^(BOOL isSuccess, NSMutableArray *tableList, NSInteger statusCode){
//                  
//                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
//                      self.tableList = tableList;
//                      
//                      [[NSNotificationCenter defaultCenter]
//                       postNotificationName:TableListChangedNotification
//                       object:nil
//                       userInfo:nil];
//                      
//                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
//                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
//                  } else {
//                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
//                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
//                                                                              object:nil];
//                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
//                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
//                                              ,statusCode]];
//                      }
//                  }
//                  self.tableLablesAsking = NO;
//                  [SVProgressHUD dismiss];
//              }];
//    switch(result){
//        case RRSuccess:
//            self.tableLablesAsking = YES;
//            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
//            break;
//        case RRParameterError:
//            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
//            break;
//        default:
//            break;
//    }
//}

- (BOOL)checkTable:(TableInfo *)tableInfo
{
    return [self checkTable:tableInfo
                 assertMine:YES];
}

- (BOOL)checkTable:(TableInfo *)tableInfo
        assertMine:(BOOL)assertMine
{
    return [self checkTable:tableInfo
                 assertMine:assertMine
                    success:^{
                        
                    } failure:^(OrderStatus status){
                        
                    }];
}

- (BOOL)checkTable:(TableInfo *)tableInfo
           success:(void (^)())successBlock
           failure:(void (^)(OrderStatus status))failureBlock
{
    return [self checkTable:tableInfo
                 assertMine:YES
                    success:successBlock
                    failure:failureBlock];
}


- (BOOL)checkTable:(TableInfo *)tableInfo
        assertMine:(BOOL)assertMine
           success:(void (^)())successBlock
           failure:(void (^)(OrderStatus status))failureBlock
{
    if(tableInfo == nil){
        PCError(@"TableInfo should not be nil");
        failureBlock(OrderStatusUnknown);
        return NO;
    }
    
    if(self.merchant == nil){
        PCError(@"Merchant is not specified");
        failureBlock(OrderStatusUnknown);
        return NO;
    }
    
    self.tableInfo = tableInfo;
//    self.cart.tableNo = tableInfo.tableNo;
    self.cart.tableLabel = tableInfo.tableNumber;
    
    RequestResult result = RRFail;
    
    result = [MENU requestOrderAndCart:self.merchant.merchantNo
                             tableInfo:tableInfo
                       completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode) {
                  self.loading = NO;
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              self.lastUpdatedDate = [NSDate date];
                              
                              self.tableInfo = tableInfo;
                              
                              NSDictionary *cartDict = [response objectForKey:@"cart"];
                              
                              if([cartDict objectForKey:@"id"] != nil){
                                  [self.cart updateWithDictionary:cartDict menuPan:MENUPAN.wholeSerialMenuList];
                              } else {
                                  self.cart = nil;
                              }
                              
                              NSDictionary *orderDict = [response objectForKey:@"order"];
                              
                              if([orderDict objectForKey:@"id"] != nil){
                                  self.order = [[Order alloc] initWithDictionary:orderDict
                                                                        merchant:self.merchant
                                                                         menuPan:MENUPAN.wholeSerialMenuList];
                                  
                                  if(self.order.orderType == OrderTypeOrderOnly
                                     && self.order.status == OrderStatusConfirmed){
                                      [self.merchant save];
                                      if([self.order save]){
                                          if(self.order.orderType == OrderTypeDelivery){
                                              Addresses *address = [[Addresses alloc] initWithDictionary:orderDict];
                                              [address save];
                                          }
                                          
                                          if([LineItem deleteWithCondition:[NSString stringWithFormat:@"orderNo = %lld and merchantNo = %lld and lineItemNo != 0",
                                                                            self.order.orderNo,
                                                                            self.merchant.merchantNo]]){
                                              
                                              if([self.order.lineItems count] > 0){
                                                  for(LineItem *lineItem in self.order.lineItems){
                                                      
                                                      if(lineItem.menuItem == nil){
                                                          [lineItem linkMenuItemInMenuList:MENUPAN.wholeSerialMenuList];
                                                      }
                                                    [lineItem insert]; //Don't use save, because LineItem's already removed from above deleteWithCondition
                                                  }
                                              }
                                          } else {
                                              PCError(@"Error to delete all lineitems related with order number %lld and merchantNo %lld", self.order.orderNo, self.merchant.merchantNo);
                                          }
                                      }
//                                      
//                                      if([self.order.lineItems count] > 0){
//                                          for(LineItem *lineItem in self.order.lineItems){
//                                              [lineItem save];
//                                          }
//                                      }
                                  }
                                  
                              } else {
                                  self.order = nil;
                              }
                              
                              if(self.order.status == OrderStatusCompleted && !assertMine){
                                  self.tableInfo = nil;
                                  self.order = nil;
                                  failureBlock(OrderStatusCompleted);
                              } else {
   
                                  NSMutableDictionary *newUserInfo = [NSMutableDictionary dictionary];
                                  
                                  if(_order != nil)
                                      [newUserInfo setObject:_order forKey:@"order"];
                                  
                                  if(_cart != nil)
                                      [newUserInfo setObject:_cart forKey:@"cart"];
                                  
                                  [[NSNotificationCenter defaultCenter] postNotificationName:LineItemChangedNotification
                                                                                      object:tableInfo
                                                                                    userInfo:newUserInfo];
                                  
                                  successBlock();
                              }
                              break;
                          }
                          case ResponseErrorInvalid:{
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Table Number is Invalid", nil)
                                                  message:NSLocalizedString(@"Try to reorder after canceling this order", nil)];
                              break;
                          }
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting menu", nil)];
                                  
                                  failureBlock(OrderStatusUnknown);
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      failureBlock(OrderStatusUnknown);
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                      failureBlock(OrderStatusUnknown);
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            self.loading = YES;
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            return YES;
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            failureBlock(OrderStatusUnknown);
            return NO;
            break;
        default:
            return NO;
            break;
    }
}

#ifndef DEBUG
    #define RESTRICT_DISTANCE   1
#endif

- (BOOL)isNearMerchant
{
#if RESTRICT_DISTANCE
    if(self.merchant.isDemoMode || self.merchant.isTestMode){
        return YES;
    } else {
        if(APP.location == nil){
            if(self.merchant.isTableBase){
                return NO;
            } else {
                return YES;
            }
        } else {
            CLLocationDistance distance = [APP.location distanceFromLocation:self.merchant.location];

            if(self.merchant.isTableBase){
                return distance < AMILE;
            } else {
                return YES; // Douglas wants (19 Aug 2013)
            }
        }
    }
#else
    return YES;
#endif
}

- (BOOL)shouldOpenRightSlide
{
    if(self.cart.status == CartStatusSubmitted
       || (self.order != nil && ([self.cart.lineItems count] == 0))
       || (self.cart.isLocal && ([self.cart.lineItems count] > 0))){
        return YES;
    } else {
        return NO;
    }
}

- (void)requestMerchantInfo
{
    RequestResult result = RRFail;
    result = [PAY requestMerchant:self.merchant.merchantNo
                  completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              
                              NSDictionary *merchantDict = response.data;
                              
                              Merchant *aMerchant = nil;
                              if(merchantDict != nil){
                                  aMerchant = [[Merchant alloc] initWithDictionary:merchantDict];
                                  aMerchant.favorite = [REMOTE isFavoriteMerchant:aMerchant];
                                  if(APP.location != nil){
                                      aMerchant.distance = [APP.location distanceFromLocation:aMerchant.location];
                                  }
                              }
                              
                              [self.merchant refreshWithMerchant:aMerchant];
                              
                              if(_cart != nil && _cart.merchant.merchantNo == self.merchant.merchantNo){
                                  _cart.merchant = self.merchant;
                                  _cart.merchantName = self.merchant.name;
                              }
                              
                              [self.merchant update];

                              [[NSNotificationCenter defaultCenter] postNotificationName:MerchantInformationUpdatedNotification
                                                                                  object:self.merchant];
                          }
                              break;
                          case ResponseErrorNoMerchant:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"This restaurant does not exist", nil)
                                                  message:NSLocalizedString(@"Please try again and if this error is continuing, please contact us.", nil)];
                              [self.merchant delete];
                              
                              [[NSNotificationCenter defaultCenter] postNotificationName:MerchantInformationUpdatedNotification
                                                                                  object:self.merchant
                                                                                userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES]
                                                                                                                     forKey:@"InvalidMerchant"]];
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting restaurant information", nil)];
                              }
                          }
                              break;
                      }
                      
                      
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
//                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
//            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (Addresses *)tempDeliveryAddress
{
    if(_tempDeliveryAddress == nil){
        self.tempDeliveryAddressFilled = 0;
        _tempDeliveryAddress = [[Addresses alloc] init];
    }
    
    return _tempDeliveryAddress;
}

- (BOOL)isSetTempDelivery
{
    return (_tempDeliveryAddress != nil);
}

@end
