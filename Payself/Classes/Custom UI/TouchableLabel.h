//
//  TouchableLabel.h
//  RushOrder
//
//  Created by Conan on 11/28/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TouchableLabelAction;

@interface TouchableLabel : UILabel
@property (nonatomic, weak) id<TouchableLabelAction> target;
@end

@protocol TouchableLabelAction <NSObject>
- (void)touchableLabelActionTriggerred:(id)sender;
@end