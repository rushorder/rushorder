//
//  NPImageButton.m
//  RushOrder
//
//  Created by Conan Kim on 5/13/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import "NPImageButton.h"

@implementation NPImageButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self commonInit:self.frame];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.alpha = 0.7;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.alpha = 1.0;
}

- (void)touchesEnded:(NSSet *)touches
           withEvent:(UIEvent *)event
{
    self.alpha = 1.0;

    if([self.target respondsToSelector:self.action]){
        [self.target performSelector:self.action
                          withObject:self];
    }
}

- (void)commonInit:(CGRect)rect
{
    self.layer.cornerRadius = self.cornerRadius.floatValue;
    self.clipsToBounds = YES;
    self.userInteractionEnabled = YES;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    self.layer.cornerRadius = self.cornerRadius.floatValue;

}

@end