//
//  RestaurantAnnotationView.h
//  RushOrder
//
//  Created by Conan on 12/2/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface RestaurantAnnotationView : MKAnnotationView

@property (nonatomic, strong) Merchant *merchant;

- (void)drawRestaurant;

@end
