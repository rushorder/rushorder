//
//  FavoriteOrderSignIn.h
//  RushOrder
//
//  Created by Conan on 1/22/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoriteOrderSignIn : UIView

@property (weak, nonatomic) IBOutlet NPStretchableButton *signInButton;
@end
