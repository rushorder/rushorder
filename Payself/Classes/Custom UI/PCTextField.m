//
//  PCTextField.m
//  RushOrder
//
//  Created by Conan on 2/15/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCTextField.h"

@implementation PCTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self) {
        
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UIImage *image = [self.background resizableImageFromCenter];
    self.background = image;
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    CGRect superRect = [super editingRectForBounds:bounds];
    return [self textRectForBounds:superRect];
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    if(!self.underLine && self.sideMargin == 0.0f){
        self.sideMargin = 10.0f;
    }
    bounds.origin.x += self.sideMargin;
    bounds.size.width -= (self.sideMargin * 2) + 8.0f;
    
    return bounds;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if(self.underLine){
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetStrokeColorWithColor(context, [UIColor colorWithR:204.0f G:204.0f B:204.0f].CGColor);
        
        // Draw them with a 2.0 stroke width so they are a bit more visible.
        CGContextSetLineWidth(context, 0.5f);
        
        CGContextMoveToPoint(context, 0,rect.size.height - 1.0); //start at this point
        
        CGContextAddLineToPoint(context, rect.size.width, rect.size.height - 1.0); //draw to this point
        
        // and now draw the Path!
        CGContextStrokePath(context);
    }
}


@end
