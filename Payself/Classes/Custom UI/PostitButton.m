//
//  PostitButton.m
//  RushOrderMerchant
//
//  Created by Conan on 7/3/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//


#import "PostitButton.h"
#import <QuartzCore/QuartzCore.h>

#define _TABLENUMBER_LABEL_MARGIN 6.0f

@interface PostitButton()


@end

@implementation PostitButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UISwipeGestureRecognizer *swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                              action:@selector(buttonSwiped:)];
        swipeRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
        [self addGestureRecognizer:swipeRecognizer];
    }
    return self;
}

- (void)buttonSwiped:(id)sender
{
    [self.swipeTarget performSelector:self.swipeSelector
                           withObject:self];
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];

//    self.layer.cornerRadius = 8.0f;
    self.layer.masksToBounds = YES;
    
//    self.tableNumberLabel.text = self.orderNumber;
    self.tableNumberLabel.adjustsFontSizeToFitWidth = YES;
    self.takeoutIcon.frame = CGRectMake(_TABLENUMBER_LABEL_MARGIN + 5.0f,
                                        _TABLENUMBER_LABEL_MARGIN + 8.0f,
                                        42.0f,
                                        38.0f);
    self.takeoutIcon.contentMode = UIViewContentModeRight;
    if(self.takeoutIcon.hidden){
        self.tableNumberLabel.frame = CGRectMake(_TABLENUMBER_LABEL_MARGIN,
                                                 _TABLENUMBER_LABEL_MARGIN,
                                                 rect.size.width - _TABLENUMBER_LABEL_MARGIN,
                                                 self.tableNumberLabel.font.pointSize + 2.0f);
    } else {
        self.tableNumberLabel.frame = CGRectMake(CGRectGetMaxX(self.takeoutIcon.frame) + 6.0f,
                                                 _TABLENUMBER_LABEL_MARGIN,
                                                 rect.size.width - _TABLENUMBER_LABEL_MARGIN,
                                                 self.tableNumberLabel.font.pointSize + 2.0f);
    }
    
    CGFloat memoOriginY = _TABLENUMBER_LABEL_MARGIN + self.tableNumberLabel.height + _TABLENUMBER_LABEL_MARGIN;
    self.memoLabel.frame = CGRectMake(_TABLENUMBER_LABEL_MARGIN,
                                      memoOriginY,
                                      rect.size.width - (_TABLENUMBER_LABEL_MARGIN * 2),
                                      rect.size.height - memoOriginY - self.titleLabel.height - (_TABLENUMBER_LABEL_MARGIN * 2));
}

- (UILabel *)tableNumberLabel
{
    if(_tableNumberLabel == nil){
        _tableNumberLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _tableNumberLabel.font = [UIFont boldSystemFontOfSize:43.0f];
        _tableNumberLabel.textColor = [self titleColorForState:UIControlStateNormal];
        _tableNumberLabel.highlightedTextColor = [self titleColorForState:UIControlStateHighlighted];
        _tableNumberLabel.backgroundColor = [UIColor clearColor];
        _tableNumberLabel.adjustsFontSizeToFitWidth = YES;
        _tableNumberLabel.minimumScaleFactor = 0.5f;
        [self addSubview:_tableNumberLabel];
    }
//    [_tableNumberLabel drawBorder];
    return _tableNumberLabel;
}

- (UILabel *)memoLabel
{
    if(_memoLabel == nil){
        _memoLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _memoLabel.font = self.titleLabel.font;
        _memoLabel.textColor = [self titleColorForState:UIControlStateNormal];
        _memoLabel.highlightedTextColor = [self titleColorForState:UIControlStateHighlighted];
        _memoLabel.backgroundColor = [UIColor clearColor];
        _memoLabel.adjustsFontSizeToFitWidth = YES;
        _memoLabel.minimumScaleFactor = 0.5f;
        _memoLabel.textAlignment = NSTextAlignmentCenter;
        _memoLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _memoLabel.numberOfLines = 0;
        
        [self addSubview:_memoLabel];
    }
//    [_memoLabel drawBorder];
    return _memoLabel;
}

- (UIImageView *)takeoutIcon
{
    if(_takeoutIcon == nil){
        _takeoutIcon = [[UIImageView alloc] initWithFrame:CGRectZero];
        _takeoutIcon.backgroundColor = [UIColor clearColor];
        
        [self addSubview:_takeoutIcon];
    }
    
    return _takeoutIcon;
}

- (void)setShapeType:(NSUInteger)shapeTypeNumber
{
    _shapeType = shapeTypeNumber;
    BOOL isWhiteFont = YES;
    
    switch(_shapeType)
    {
        case 1:
            [self setBackgroundImage:[[UIImage imageNamed:@"card_bg_red"] resizableImageFromCenter]
                            forState:UIControlStateNormal];
            break;
        case 2:
            [self setBackgroundImage:[[UIImage imageNamed:@"card_bg_yellow"] resizableImageFromCenter]
                            forState:UIControlStateNormal];
            isWhiteFont = NO;
            break;
        case 3:
        default:
            [self setBackgroundImage:[[UIImage imageNamed:@"card_bg_blue"] resizableImageFromCenter]
                            forState:UIControlStateNormal];
            break;
    }
    
    UIColor *color = [UIColor colorWithR:54.0f G:54.0f B:54.0f];
    if(isWhiteFont){
        color = [UIColor whiteColor];
    }
    [self setTitleColor:color forState:UIControlStateNormal];
    self.tableNumberLabel.textColor = color;
    self.memoLabel.textColor = color;
    [self iconAdoptation];
}

- (void)setIconType:(NSUInteger)iconTypeNumber
{
    _iconType = iconTypeNumber;
    [self iconAdoptation];
}

- (void)iconAdoptation
{
    switch(self.iconType)
    {
        case 1:
            if(self.shapeType == 2){
                self.takeoutIcon.image = [UIImage imageNamed:@"icon_table_54"];
            } else {
                self.takeoutIcon.image = [UIImage imageNamed:@"icon_table"];
            }
            break;
        case 2:
            if(self.shapeType == 2){
                self.takeoutIcon.image = [UIImage imageNamed:@"icon_takeout_54"];
            } else {
                self.takeoutIcon.image = [UIImage imageNamed:@"icon_takeout"];
            }
            break;
        case 3:
        default:
            if(self.shapeType == 2){
                self.takeoutIcon.image = [UIImage imageNamed:@"icon_delivery_54"];
            } else {
                self.takeoutIcon.image = [UIImage imageNamed:@"icon_delivery"];
            }
            break;
    }
}


@end
