//
//  BadgeBarButtonItem.h
//  RushOrder
//
//  Created by Conan on 11/10/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BadgeBarButtonItem : UIBarButtonItem

@property (nonatomic, getter = isBadgeOn) BOOL badgeOn;

@end