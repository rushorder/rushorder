//
//  PCCurrencyTextField.h
//  RushOrder
//
//  Created by Conan on 2/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PCCurrencyTextFieldDelegate;

@interface PCCurrencyTextField : UITextField 

@property (weak, nonatomic) id<PCCurrencyTextFieldDelegate> forwardDelegate;
@property (nonatomic) PCCurrency amount;

@property (nonatomic, readonly) NSString *currencyCode;

- (void)reset;
- (void)sendMessageToTargets;

@end

@protocol PCCurrencyTextFieldDelegate <UITextFieldDelegate>
@end