//
//  PCNumberTextField.h
//  RushOrder
//
//  Created by Conan on 2/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PCNumberTextFieldDelegate;

@interface PCNumberTextField : UITextField

@property (weak, nonatomic) id<PCNumberTextFieldDelegate> forwardDelegate;
@property (readonly) NSNumber *value;


- (void)reset;
- (void)sendMessageToTargets;

@end

@protocol PCNumberTextFieldDelegate <UITextFieldDelegate>
@end