//
//  TouchableLabel.m
//  RushOrder
//
//  Created by Conan on 11/28/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "TouchableLabel.h"

@interface TouchableLabel()
@property (strong, nonatomic) UIColor *initialColor;
@end

@implementation TouchableLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.userInteractionEnabled = YES;
    self.initialColor = self.textColor;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.textColor = [UIColor blackColor];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.textColor = self.initialColor;
}

- (void)touchesEnded:(NSSet *)touches
           withEvent:(UIEvent *)event
{
    self.textColor = self.initialColor;
    [self.target touchableLabelActionTriggerred:self];
}

@end