//
//  MenuHeaderDetailView.h
//  RushOrder
//
//  Created by Conan Kim on 4/20/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuHeaderDetailView : UIView

@property (nonatomic, strong) IBOutlet UILabel *multilineLabel;
@end
