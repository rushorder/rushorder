//
//  PCDynamicGridView.h
//  RushOrder
//
//  Created by Conan on 3/6/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PCDynamicGridView;

@protocol PCDynamicGridViewDataSource <NSObject>
@required
- (NSInteger)numberOfItemsInGridView:(PCDynamicGridView *)gridView;
- (UIView *)gridView:(PCDynamicGridView *)gridView cellForItemIndex:(NSInteger)index;
- (void)gridView:(PCDynamicGridView *)gridView updateCell:(UIView *)cell atIndex:(NSInteger)index;
@end

@protocol PCDynamicGridViewDelegate <UIScrollViewDelegate>
@end


//==============================================================================
@interface PCDynamicGridView : UIScrollView

@property (nonatomic) UIEdgeInsets outerMargin;
@property (nonatomic) CGFloat itemSpan;

@property (weak, nonatomic) id < PCDynamicGridViewDataSource> dataSrouce;
@property (weak, nonatomic) id < PCDynamicGridViewDelegate> delegate;

@property (nonatomic) BOOL fixedSize;

- (void)reloadData;

- (void)updateCellAtIndex:(NSUInteger)index;
@end


