//
//  NPImageButton.h
//  RushOrder
//
//  Created by Conan Kim on 5/13/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface NPImageButton : UIImageView
@property (nonatomic, weak) IBOutlet id target;
@property (nonatomic) SEL action;
@property (strong, nonatomic) NSNumber *cornerRadius;
@end