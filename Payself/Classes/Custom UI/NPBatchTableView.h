//
//  NPBatchTableView.h
//  RushOrder
//
//  Created by Conan on 12/7/12.
//  Copyright (c) 2012 Novaple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"

@interface NPBatchTableView : UITableView
<
UIScrollViewDelegate,
EGORefreshTableHeaderDelegate
>

@property (retain, nonatomic) EGORefreshTableHeaderView *refreshHeaderView;

//@property (strong, nonatomic) MoreLoadTableFooterView *moreLoadFooterView;
@property (nonatomic) BOOL hidesAtLast;

@property (nonatomic) CGFloat adjustingOffsetY;

//- (void)dereferDelegates;

- (void)scrollToBottom:(BOOL)animated;
@end

@protocol PullToRefreshing <UITableViewDelegate>
@required
- (BOOL)refresh; // Pull to refresh가 트리거 되었을 때 수행할 작업을 구현합니다. Return: Success to start refresh
- (BOOL)inNewLoading; // -(void)refresh 작업중일 때 YES를 return합니다.
@optional
- (BOOL)isThereRefreshableData; //위로 데이터가 더 없어 Pull to refresh 하여도 데이터가 더 이상 없을 것이 확실할 때 사용합니다.
- (NSDate *)lastUpdatedDate;
- (BOOL)isRedType;
@end