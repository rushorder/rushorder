//
//  LogoCircleImageView.m
//  RushOrder
//
//  Created by Conan on 10/30/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "LogoCircleImageView.h"
#import "MerchantViewController.h"

@implementation LogoCircleImageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.userInteractionEnabled = YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.alpha = 0.8;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.alpha = 1.0;
}

- (void)touchesEnded:(NSSet *)touches
           withEvent:(UIEvent *)event
{
    self.alpha = 1.0;
    [self.target circleImageViewActionTriggerred:self];
}
@end
