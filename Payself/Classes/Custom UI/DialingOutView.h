//
//  DialingOutView.h
//  RushOrder
//
//  Created by Conan Kim on 4/10/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DialingOutView : UIView

@property (copy, nonatomic) NSString *merchantPhoneNumber;
@property (weak, nonatomic) IBOutlet NPStretchableButton *merchantCallButton;
@end
