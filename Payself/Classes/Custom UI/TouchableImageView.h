//
//  TouchableImageView.h
//  RushOrder
//
//  Created by Conan Kim on 8/16/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TouchableImageViewAction;

@interface TouchableImageView : UIImageView
@property (nonatomic, weak) id<TouchableImageViewAction> target;
@end

@protocol TouchableImageViewAction <NSObject>
- (void)touchableImageViewActionTriggerred:(TouchableImageView *)sender;
@end
