//
//  PCDynamicRowGridView.m
//  RushOrder
//
//  Created by Conan on 3/6/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCDynamicRowGridView.h"


@interface PCDynamicRowGridView()
{
    int tileHeight_;
    CGSize containerSize_;
    int column_;
    int row_;
}

@property (nonatomic) NSInteger drawnItemCount;
@end

@implementation PCDynamicRowGridView

@dynamic delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.drawnItemCount = 0;
    
    self.outerMargin = UIEdgeInsetsZero;
    self.itemSpan = 2.0f;
    
    containerSize_ = self.bounds.size;
    containerSize_.width -= (self.outerMargin.left + self.outerMargin.right);
    containerSize_.height -= (self.outerMargin.top + self.outerMargin.bottom);
    
    tileHeight_ = containerSize_.height;
    
//    [self drawBorderWithColor:[UIColor lightGrayColor]];
}

- (void)reloadData
{
    NSInteger itemCount = [self.dataSrouce numberOfItemsInGridView:self];
    // TODO:re alive
//    if(self.drawnItemCount != itemCount){
        self.drawnItemCount = itemCount;
        [self arrangeItems];
//    }
    
    [self updateCells];
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    
    containerSize_ = bounds.size;
    
    containerSize_.width -= (self.outerMargin.left + self.outerMargin.right);
    containerSize_.height -= (self.outerMargin.top + self.outerMargin.bottom);
    
    tileHeight_ = containerSize_.height;
}

- (void)setOuterMargin:(UIEdgeInsets)outerMargin
{
    _outerMargin = outerMargin;
    containerSize_ = self.bounds.size;
    containerSize_.width -= (outerMargin.left + outerMargin.right);
    containerSize_.height -= (outerMargin.top + outerMargin.bottom);
    
    tileHeight_ = containerSize_.height;
}

- (void)updateCells
{
    for(PostitButton *subview in self.subviews){
        if([subview isKindOfClass:[PostitButton class]]){
            [self.dataSrouce gridView:self
                           updateCell:subview
                              atIndex:subview.index];
        }
    }
}

- (void)arrangeItems
{
    [self removeAllSubviews];
    
    
    NSInteger i = 0;
    
    CGFloat lastItemMaxX = 0;
    
    for(i = 0 ; i < self.drawnItemCount ; i++){
        PostitButton *cell = (PostitButton *)[self.dataSrouce gridView:self
                                                      cellForItemIndex:i];
        
        CGRect frame = [self frameAtIndex:i];
        cell.frame = frame;
        cell.index = i;
        cell.section = self.tag;
        
        [self addSubview:cell];
        
        lastItemMaxX = CGRectGetMaxX(cell.frame) + self.outerMargin.right;
    }
    
    self.contentSize = CGSizeMake(lastItemMaxX
                                  , self.bounds.size.height);
}

- (CGRect)frameAtIndex:(NSInteger)index
{
    CGRect rtnRect = CGRectZero;
    
    rtnRect.origin.x = self.outerMargin.left + (index * (self.tileWidth + (index == 0 ? 0.0f : self.itemSpan)));
    rtnRect.origin.y = self.outerMargin.top;
    rtnRect.size.width = self.tileWidth;
    rtnRect.size.height = tileHeight_;
    
    return rtnRect;
}

- (PostitButton *)buttonAtIndex:(NSInteger)index
{
    if(index < [self.subviews count]){
        return [self.subviews objectAtIndex:index];
    } else {
        return nil;
    }
}

- (void)shiftLeftFromIndex:(NSInteger)index
{
    NSInteger i = 0;
    CGFloat lastItemMaxX = 0;
    
    for(i = index; i < [self.subviews count]; i++){
        PostitButton *button = [self.subviews objectAtIndex:i];
        CGRect btnFrame = button.frame;
        btnFrame.origin.x -= (self.tileWidth + self.itemSpan);
        button.frame = btnFrame;
        button.index--;
        
        if(([self.subviews count]-1) == i){
            lastItemMaxX = CGRectGetMaxX(button.frame) + self.outerMargin.right;
        }
    }
    
    if([self.subviews count]== index && ([self.subviews count] > 0)){
        PostitButton *button = [self.subviews objectAtIndex:(index-1)];
        lastItemMaxX = CGRectGetMaxX(button.frame) + self.outerMargin.right;
    }
    
    self.contentSize = CGSizeMake(lastItemMaxX
                                  , self.bounds.size.height);
}

- (void)shiftRightFromIndex:(NSInteger)index
{
    NSInteger i = 0;
    CGFloat lastItemMaxX = 0;
    
    for(i = index; i < [self.subviews count]; i++){
        PostitButton *button = [self.subviews objectAtIndex:i];
        CGRect btnFrame = button.frame;
        btnFrame.origin.x += (self.tileWidth + self.itemSpan);
        button.frame = btnFrame;
        button.index++;
        if(([self.subviews count]-1) == i){
            lastItemMaxX = CGRectGetMaxX(button.frame) + self.outerMargin.right;
        }
    }
    
    if([self.subviews count] == index){
        CGRect tobeRect = [self frameAtIndex:index];
        lastItemMaxX = CGRectGetMaxX(tobeRect) + self.outerMargin.right;
    }
    
    self.contentSize = CGSizeMake(lastItemMaxX
                                  , self.bounds.size.height);
}

- (void)addSubview:(UIView *)view
{
    [super addSubview:view];
}


@end
