//
//  RestaurantSummaryView.h
//  RushOrder
//
//  Created by Conan on 11/25/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantSummaryView : UIView
<
LogoCircleImageViewAction,
TouchableLabelAction
>

@property (nonatomic, strong) Merchant *merchant;
@property (weak, nonatomic) IBOutlet LogoCircleImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *distanceWarningImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *serviceFeeWidthConstraint;


- (void)drawMerchant;
- (void)drawMerchantForceHeight:(BOOL)forceHeight;
- (void)drawMerchant:(Merchant *)merchant;
- (void)drawMerchant:(Merchant *)merchant
         forceHeight:(BOOL)forceHeight
          isDelivery:(BOOL)isDelivery
           showPromo:(BOOL)showPromo
       showRoService:(BOOL)showRoService;
- (void)drawMerchant:(Merchant *)merchant
         forceHeight:(BOOL)forceHeight
          isDelivery:(BOOL)isDelivery
           showPromo:(BOOL)showPromo
       showRoService:(BOOL)showRoService
            cartType:(CartType)cartType;

@end
