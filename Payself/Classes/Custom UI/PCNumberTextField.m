//
//  PCNumberTextField.m
//  RushOrder
//
//  Created by Conan on 2/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCNumberTextField.h"
#import "FormatterManager.h"

@interface PCNumberTextField()
{

}


@property (strong, nonatomic) UIColor *defaultTextColor;
@end

@implementation PCNumberTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self != nil){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    // Initialization code
    self.keyboardType = UIKeyboardTypeNumberPad;
    
//    self.numberFormatter = [NSNumberFormatter numberFormatter];
    
    [self reset];
    
//    [self addTarget:self
//             action:@selector(eidtingValueChanged:)
//   forControlEvents:UIControlEventEditingChanged];
    
    self.delegate = FORMATTER;
}

//- (void)eidtingValueChanged:(UITextField *)sender
//{
//    NSNumber *number = [FORMATTER.numberFormatter numberFromString:sender.text];
//    
//    self.text = [FORMATTER.numberFormatter stringFromNumber:number];
//}

- (void)reset
{
    [self.delegate textField:self
shouldChangeCharactersInRange:NSMakeRange(0, [self.text length])
  replacementString:@"0"];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.defaultTextColor = self.textColor;
    UIImage *image = [self.background resizableImageFromCenter];
    self.background = image;
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    CGRect superRect = [super editingRectForBounds:bounds];
    return [self textRectForBounds:superRect];
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    CGFloat sideMargin = 8.0f;

    bounds.size.width -= sideMargin * 2;
    bounds.origin.x += sideMargin;
    bounds.origin.y += 2.0f;
    
    return bounds;
}

- (void)sendMessageToTargets
{
    NSSet *targets = [self allTargets];
    
    for(id target in targets){
        NSArray *selectors = [self actionsForTarget:target forControlEvent:UIControlEventValueChanged];
        for(NSString *selectorName in selectors){
            SEL selector = NSSelectorFromString(selectorName);
            [target performSelector:selector
                         withObject:self];
        }
    }
}

- (void)setValue:(NSNumber *)value
{    
    self.text = [FORMATTER.numberFormatter stringFromNumber:value];
    
    [self sendMessageToTargets];
}

- (void)setEnabled:(BOOL)value
{
    if(value){
        self.textColor = self.defaultTextColor;
    } else {
        self.textColor = [UIColor lightGrayColor];
    }
    
    [super setEnabled:value];
}

- (NSNumber *)value
{
    NSNumber* number = [FORMATTER.numberFormatter numberFromString:self.text];
    return number;
}

@end
