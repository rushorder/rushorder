//
//  RestaurantMiniSummaryView.h
//  RushOrder
//
//  Created by Conan on 4/24/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Merchant.h"

@protocol RestaurantMiniSummaryViewDelegate;

@interface RestaurantMiniSummaryView : UIView

@property (weak, nonatomic) IBOutlet TouchableLabel *titleLabel;
@property (weak, nonatomic) IBOutlet TouchableLabel *addressLabel;

@property (weak, nonatomic) id<RestaurantMiniSummaryViewDelegate> delegate;
@property (strong, nonatomic) Merchant *merchant;
@property (weak, nonatomic) IBOutlet LogoCircleImageView *logoImage;

- (void)drawRestaurant:(BOOL)forHeight;
@end

@protocol RestaurantMiniSummaryViewDelegate <NSObject>
- (void)restaurantMiniSummaryView:(RestaurantMiniSummaryView *)viewController
             didTouchActionButton:(id)sender;
@end
