//
//  PaymentSummaryView.h
//  RushOrder
//
//  Created by Conan on 12/23/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"
#import "Payment.h"
#import "Merchant.h"

typedef enum _promotionValidStatus{
    PromotionValidStatusNone = 0,
    PromotionValidStatusChecking,
    PromotionValidStatusInvalid,
    PromotionValidStatusValid
} PromotionValidStatus;
    
@protocol PaymentSummaryViewDelegate;

@interface PaymentSummaryView : UIView
<
PCCurrencyTextFieldDelegate,
LogoCircleImageViewAction,
TouchableLabelAction,
UITextFieldDelegate
>

@property (weak, nonatomic) id<PaymentSummaryViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *pointAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointConvertedLabel;
@property (weak, nonatomic) IBOutlet UILabel *cardAmountLabel;
@property (weak, nonatomic) IBOutlet PLCurrencyTextField *tipTextField;
@property (weak, nonatomic) IBOutlet PCTextField *couponCodeTextField;
@property (weak, nonatomic) IBOutlet UILabel *totalPayAmountLabel;

@property (weak, nonatomic) IBOutlet UISwitch *orangeUseSwitch;
@property (weak, nonatomic) IBOutlet UILabel *orangePointTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *orangePointLabel;

@property (nonatomic, strong) Order *order;
@property (nonatomic, strong) Payment *payment;
@property (nonatomic, strong) Merchant *merchant;

@property (strong, nonatomic) Promotion *smallCutPromotion;

@property (nonatomic, getter = isAutoTipApplied) BOOL autoTipApplied;
@property (nonatomic, getter = isAutoTipSetOnce) BOOL autoTipSetOnce;

- (void)drawPayment;
@end


@protocol PaymentSummaryViewDelegate<NSObject>
- (void)paymentSummaryView:(PaymentSummaryView *)summaryView
 didTouchApplyCouponButton:(id)sender;

- (void)paymentSummaryView:(PaymentSummaryView *)summaryView
      didTouchSignInButton:(id)sender;

- (void)paymentSummaryView:(PaymentSummaryView *)summaryView
    didTouchDiscountAmount:(Promotion *)promotion;

- (void)paymentSummaryView:(PaymentSummaryView *)summaryView
    didTouchFeesAmount:(Promotion *)promotion;

- (void)paymentSummaryViewTipAmountChanged:(PaymentSummaryView *)summaryView;

- (void)paymentSummaryViewOrangeSwitchChanged:(UISwitch *)sender;

@optional
- (void)paymentSummaryViewWillViewHeightChagned:(PaymentSummaryView *)summaryView;
- (void)paymentSummaryViewDidViewHeightChagned:(PaymentSummaryView *)summaryView;

@end
