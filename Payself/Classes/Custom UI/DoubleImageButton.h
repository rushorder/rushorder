//
//  DoubleImageButton.h
//  RushOrder
//
//  Created by Conan on 9/4/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "NPStretchableButton.h"

@interface DoubleImageButton : NPStretchableButton

@property (strong, nonatomic) UIImage *subImage;
@property (nonatomic, getter = isSubImageOn) BOOL subImageOn;
@end
