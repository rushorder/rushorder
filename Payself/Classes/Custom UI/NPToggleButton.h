//
//  NPToggleButton.h
//  RushOrder
//
//  Created by Conan on 11/19/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NPToggleButton : UIButton

@property (nonatomic) BOOL manualSelection;
@property (nonatomic,strong) UIImage *normalImage;
@property (nonatomic,strong) UIImage *normalBackgroundImage;
@property (nonatomic,copy) NSString *normalTitle;
@property (nonatomic,strong) UIColor *normalTitleColor;

@end
