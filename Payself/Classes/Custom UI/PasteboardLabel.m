//
//  PasteboardLabel.m
//  RushOrder
//
//  Created by Conan Kim on 2/4/16.
//  Copyright © 2016 Paycorn. All rights reserved.
//

#import "PasteboardLabel.h"

@implementation PasteboardLabel

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    return (action == @selector(copy:));
}

#pragma mark - UIResponderStandardEditActions
- (void)copy:(id)sender
{
    UIPasteboard *generalPastebaord = [UIPasteboard generalPasteboard];
    generalPastebaord.string = self.text;
}

@end
