//
//  PCTextField.h
//  RushOrder
//
//  Created by Conan on 2/15/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCTextField : UITextField

@property (nonatomic, readwrite, assign) IBOutlet UIResponder *nextField;
@property (nonatomic) BOOL underLine;
@property (nonatomic) CGFloat sideMargin;

@end
