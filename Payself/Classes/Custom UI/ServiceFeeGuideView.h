//
//  ServiceFeeGuideView.h
//  RushOrder
//
//  Created by Conan on 11/11/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceFeeGuideView : UIView

@property (weak, nonatomic) IBOutlet UILabel *guideLabel;

- (void)startAnimation;
@end
