//
//  SearchBarContainerView.m
//  RushOrder
//
//  Created by Conan Kim on 12/2/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

#import "SearchBarContainerView.h"

@implementation SearchBarContainerView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)layoutSubviews
{
    self.searchBar.frame = self.bounds;
}

//- (CGSize)sizeThatFits:(CGSize)size {
//    return size;
//}
@end
