//
//  RatingView.m
//  RushOrder
//
//  Created by Conan Kim on 12/3/15.
//  Copyright © 2015 Paycorn. All rights reserved.
//

#import "RatingView.h"

@implementation RatingView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
//    self.selectedRating = 3;
    return self;
}

- (IBAction)ratingButtonTouched:(UIButton *)sender {
    
    sender.selected = YES;
    
    if(sender != self.badButton) self.badButton.selected = NO;
    if(sender != self.neutralButton) self.neutralButton.selected = NO;
    if(sender != self.goodButton) self.goodButton.selected = NO;
    
    self.selectedRating = sender.tag;
}

@end

@implementation FeedbackView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
