//
//  PostitButton.h
//  RushOrderMerchant
//
//  Created by Conan on 7/3/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostitButton : UIButton

@property (strong, nonatomic) UILabel *tableNumberLabel;
@property (strong, nonatomic) UILabel *memoLabel;
@property (strong, nonatomic) UIImageView *takeoutIcon;
//@property (nonatomic) BOOL enLarged;
@property (copy, nonatomic) NSString *orderNumber;

@property (nonatomic)NSInteger index;
@property (nonatomic)NSInteger section;

@property (weak, nonatomic) id swipeTarget;
@property (nonatomic) SEL swipeSelector;

@property (nonatomic) NSUInteger shapeType;
@property (nonatomic) NSUInteger iconType;

@end