//
//  NPBatchTableView.m
//  Toast
//
//  Created by Conan on 5/30/12.
//  Copyright (c) 2012 Novaple. All rights reserved.
//

#import "NPBatchTableView.h"

@interface NPBatchTableView()
{
    BOOL    _didEndDragging;
}
- (void)commonInit;
@end


@implementation NPBatchTableView

- (id)initWithFrame:(CGRect)frame
{
    return [self initWithFrame:frame style:UITableViewStylePlain];
}

- (id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    self = [super initWithFrame:frame style:style];
    if (self) {
        // Initialization code
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if (self) {
        // Initialization code
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    self.adjustingOffsetY = 0.0f;
}


- (void)dealloc
{
    _refreshHeaderView.delegate = nil;

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)keyWillShow:(NSNotification *)aNoti
{
//    TSLog(@"aNoti : %@", aNoti);
    [self setContentInsetsWithNotification:aNoti keyboardShown:YES];
}

- (void)keyWillHide:(NSNotification *)aNoti
{
//    TSLog(@"aNoti : %@", aNoti);
    [self setContentInsetsWithNotification:aNoti keyboardShown:NO];
}

- (void)setContentInsetsWithNotification:(NSNotification *)aNoti
                           keyboardShown:(BOOL)keyboardShown
{
    UIView *firstResponder = [self findFirstResponder];
    if(firstResponder == nil){
        return;
    }
    
    NSTimeInterval animationDuration = [[aNoti.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGRect endFrame = [[aNoti.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect convertedFrame = [self convertRect:endFrame fromView:self.window];
    convertedFrame.origin.y -= self.contentOffset.y;
    CGFloat hiddenHeight = self.bounds.size.height - convertedFrame.origin.y;
    hiddenHeight = MAX(hiddenHeight, 0);
    
    [UIView animateWithDuration:animationDuration
                     animations:^(){
                         if(keyboardShown){
                             
                             self.contentInset = UIEdgeInsetsMake(0.0f,
                                                                  0.0f,
                                                                  hiddenHeight,
                                                                  0.0f);
                             
                             CGRect toScrollRect = [firstResponder.superview convertRect:firstResponder.frame
                                                                                  toView:self];
                             toScrollRect.origin.y -= 24.0f;
                             
                             toScrollRect.origin.y += self.adjustingOffsetY;
                             
                             toScrollRect.origin.y = MIN(toScrollRect.origin.y,
                                                         self.contentSize.height - (self.height - hiddenHeight));
                             toScrollRect.origin.y = MAX(toScrollRect.origin.y,
                                                         0.0f);
                             
                             
                             [self setContentOffset:CGPointMake(0.0f, toScrollRect.origin.y)
                                           animated:NO];
                         } else {
                             
                             self.contentInset = UIEdgeInsetsZero;
                             
                             CGFloat bottomInsets = self.contentInset.bottom;
                             
                             CGPoint offset = [self contentOffset];
                             
                             offset.y -= bottomInsets;
                             offset.y = MIN(offset.y,
                                            self.contentSize.height - self.height);
                             offset.y = MAX(offset.y,
                                            0.0f);
                             [self setContentOffset:offset
                                           animated:NO];
                             
                         }
                         self.scrollIndicatorInsets = self.contentInset;
                     }];
}

#pragma mark - EGORefreshTableHeaderView delegate
- (BOOL)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
    return [(id <PullToRefreshing>)self.delegate refresh];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
    return [(id <PullToRefreshing>)self.delegate inNewLoading];
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view
{
    if([self.delegate respondsToSelector:@selector(lastUpdatedDate)]){
        return [(id <PullToRefreshing>)self.delegate lastUpdatedDate];
    }
    return nil;
}

- (BOOL)hasMoreLoadableData
{
    if([self.delegate respondsToSelector:@selector(isThereRefreshableData)]){
        return [(id <PullToRefreshing>)self.delegate isThereRefreshableData];
    } else {
        return YES;
    }
}

- (void)setContentOffset:(CGPoint)contentOffset
{
    [_refreshHeaderView egoRefreshScrollViewDidScroll:self];
    
//    PCLog(@" Dragging:%d, Tracking:%d, Decelerating:%d", self.isDragging, self.isTracking, self.isDecelerating);
    
    if(!_didEndDragging && !self.isDragging && self.isDecelerating){
        _didEndDragging = YES;
        [self didEndScroll];
    } else {
        _didEndDragging = !self.isDragging;
    }
    
//    PCLog(@" Dragging:%d, Tracking:%d, Decelerating:%d", self.isDragging, self.isTracking, self.isDecelerating);

    [super setContentOffset:contentOffset];
}

- (void)didEndScroll;
{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:self];
}

- (void)setDelegate:(id<PullToRefreshing>) delegate
{
    [super setDelegate:delegate];
    
    if([delegate conformsToProtocol:@protocol(PullToRefreshing)]){
        //Refresh header View
        
        if([delegate respondsToSelector:@selector(isRedType)]){
            if([delegate isRedType]){
                _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f,
                                                                                                 0.0f - self.bounds.size.height,
                                                                                                 self.bounds.size.width,
                                                                                                 self.bounds.size.height)
                                                                       arrowImageName:@"whiteArrow"
                                                                            textColor:[UIColor whiteColor]];
                _refreshHeaderView.backgroundColor = [UIColor colorWithR:251.0f G:168.0f B:8.0f];//[UIColor colorWithR:203.0 G:35.0 B:58.0];
            } else {
                _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(40.0f,
                                                                                                 0.0f - self.bounds.size.height,
                                                                                                 self.bounds.size.width,
                                                                                                 self.bounds.size.height)
                                                                       arrowImageName:@"whiteArrow"
                                                                            textColor:[UIColor whiteColor]];
            }
        } else {
            _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(40.0f,
                                                                                             0.0f - self.bounds.size.height,
                                                                                             self.bounds.size.width,
                                                                                             self.bounds.size.height)
                                                                   arrowImageName:@"whiteArrow"
                                                                        textColor:[UIColor whiteColor]];
        }

        _refreshHeaderView.delegate = self;
        _refreshHeaderView.tag = 805;
        [self addSubview:_refreshHeaderView];
        [_refreshHeaderView refreshLastUpdatedDate];
    }
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self endEditing:YES];
//    [self.superview.superview.superview endEditing:YES];
}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if ([self.delegate respondsToSelector:@selector(scrollViewDidEndDragging:willDecelerate:)]) {
        [self.delegate scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if ([self.delegate respondsToSelector:@selector(scrollViewDidEndDecelerating:)]) {
        [self.delegate scrollViewDidEndDecelerating:scrollView];
    }
}

- (void)scrollToBottom:(BOOL)animated
{
    CGFloat yOffset = self.contentSize.height - self.bounds.size.height;
    if(self.contentOffset.y == 0 && (yOffset > 0)){
        [self setContentOffset:CGPointMake(0.0f,
                                           yOffset)
                      animated:animated];
    }
}

- (void)setRefreshHeaderView:(EGORefreshTableHeaderView *)view
{
    if(view == nil){
        UIView *refreshHeaderView = [self viewWithTag:805];
        [refreshHeaderView removeFromSuperview];
    }
    
    _refreshHeaderView = view;
}

@end

