//
//  NPKeyboardAwareScrollView.m
//  RushOrder
//
//  Created by Conan on 11/23/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import "NPKeyboardAwareScrollView.h"

@implementation NPKeyboardAwareScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    self.adjustingOffsetY = 0.0f;
}

- (void)keyWillShow:(NSNotification *)aNoti
{
//    PCLog(@"aNoti : %@", aNoti);
    [self setContentInsetsWithNotification:aNoti keyboardShown:YES];
}

- (void)keyWillHide:(NSNotification *)aNoti
{
//    PCLog(@"aNoti : %@", aNoti);
    [self setContentInsetsWithNotification:aNoti keyboardShown:NO];
}

- (void)setContentInsetsWithNotification:(NSNotification *)aNoti
                           keyboardShown:(BOOL)keyboardShown
{
    UIView *firstResponder = [self findFirstResponder];
    if(firstResponder == nil){
        return;
    }
    
    NSTimeInterval animationDuration = [[aNoti.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGRect endFrame = [[aNoti.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect convertedFrame = [self convertRect:endFrame fromView:self.window];
    convertedFrame.origin.y -= self.contentOffset.y;
    CGFloat hiddenHeight = self.bounds.size.height - convertedFrame.origin.y;
    hiddenHeight = MAX(hiddenHeight, 0);
                                
    [UIView animateWithDuration:animationDuration
                     animations:^(){
                         if(keyboardShown){
                             
                             self.contentInset = UIEdgeInsetsMake(0.0f,
                                                                  0.0f,
                                                                  hiddenHeight,
                                                                  0.0f);
                             
                             CGRect toScrollRect = [firstResponder.superview convertRect:firstResponder.frame
                                                                                  toView:self];
                             toScrollRect.origin.y -= 10.0f;
                             
                             toScrollRect.origin.y += self.adjustingOffsetY;
                             
                             toScrollRect.origin.y = MIN(toScrollRect.origin.y,
                                                         self.contentSize.height - (self.height - hiddenHeight));
                             toScrollRect.origin.y = MAX(toScrollRect.origin.y,
                                                         0.0f);
                             
                             [self setContentOffset:CGPointMake(0.0f, toScrollRect.origin.y)
                                           animated:NO];
                             
                         } else {
                             
                             self.contentInset = UIEdgeInsetsZero;
                             
                             CGFloat bottomInsets = self.contentInset.bottom;
                             
                             CGPoint offset = [self contentOffset];
                             offset.y -= bottomInsets;
                             offset.y = MIN(offset.y,
                                            self.contentSize.height - self.height);
                             offset.y = MAX(offset.y,
                                            0.0f);
                             [self setContentOffset:offset
                                           animated:NO];
                             
                         }
                         self.scrollIndicatorInsets = self.contentInset;
                     }];
}

- (void)setContentSizeWithBottomViewNoMargin:(UIView *)bottomView
{
    [self setContentSizeWithBottomView:bottomView
                          bottomMargin:0.0f];
}

- (void)setContentSizeWithBottomView:(UIView *)bottomView
{
    [self setContentSizeWithBottomView:bottomView
                          bottomMargin:DEFAULT_BOTTOM_MARGIN];
}

- (void)setContentSizeWithBottomView:(UIView *)bottomView
                        bottomMargin:(CGFloat)bottomMargin
{
    CGRect bounds = self.bounds;
    self.contentSize = CGSizeMake(bounds.size.width,
                                  CGRectGetMaxY(bottomView.frame) + bottomMargin);
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    [self endEditing:YES];
    //    [self.superview.superview.superview endEditing:YES];
}

@end
