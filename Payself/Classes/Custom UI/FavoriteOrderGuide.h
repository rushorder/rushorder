//
//  FavoriteOrderGuide.h
//  RushOrder
//
//  Created by Conan on 1/22/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoriteOrderGuide : UIView

@property (weak, nonatomic) IBOutlet NPStretchableButton *addButton;
@end
