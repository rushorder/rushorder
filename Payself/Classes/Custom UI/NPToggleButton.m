//
//  NPToggleButton.m
//  RushOrder
//
//  Created by Conan on 11/19/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import "NPToggleButton.h"

@implementation NPToggleButton

@synthesize normalImage = _normalImage;
@synthesize normalBackgroundImage = _normalBackgroundImage;
@synthesize normalTitle = _normalTitle;
@synthesize manualSelection = _manualSelection;
@synthesize normalTitleColor = _normalTitleColor;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    if(!self.manualSelection){
        [self addTarget:self action:@selector(selfTouched:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.normalTitle = [self titleForState:UIControlStateNormal];
    self.normalImage = [self imageForState:UIControlStateNormal];
    self.normalBackgroundImage = [self backgroundImageForState:UIControlStateNormal];
    self.normalTitleColor = [self titleColorForState:UIControlStateNormal];
    
    if(self.selected){
        [self setTitle:[self titleForState:UIControlStateSelected] forState:UIControlStateNormal];
        [self setImage:[self imageForState:UIControlStateSelected] forState:UIControlStateNormal];
        [self setBackgroundImage:[self backgroundImageForState:UIControlStateSelected] forState:UIControlStateNormal];
        [self setTitleColor:[self titleColorForState:UIControlStateSelected] forState:UIControlStateNormal];
    }
}

#pragma mark - Method overriding
- (void)setTitle:(NSString *)title forState:(UIControlState)state
{
    if((state == UIControlStateNormal) && _normalTitle == nil)
        self.normalTitle = title;
    [super setTitle:title forState:state];
}

- (void)setImage:(UIImage *)image forState:(UIControlState)state
{
    if((state == UIControlStateNormal) && _normalImage == nil)
        self.normalImage = image;
    [super setImage:image forState:state];
}

- (void)setTitleColor:(UIColor *)color forState:(UIControlState)state
{
    if((state == UIControlStateNormal) && _normalTitleColor == nil)
        self.normalTitleColor = color;
    [super setTitleColor:color forState:state];
}

- (void)setBackgroundImage:(UIImage *)image forState:(UIControlState)state
{
    if((state == UIControlStateNormal) && _normalBackgroundImage == nil)
        self.normalBackgroundImage = image;
    
    [super setBackgroundImage:image forState:state];
}


#pragma mark - logics
- (void)selfTouched:(NPToggleButton *)sender
{
    self.selected = !self.selected;
}

- (void)setSelected:(BOOL)selected
{
    if(selected){
        [self setTitle:[self titleForState:UIControlStateSelected] forState:UIControlStateNormal];
        [self setTitleColor:[self titleColorForState:UIControlStateSelected] forState:UIControlStateNormal];
        [self setImage:[self imageForState:UIControlStateSelected] forState:UIControlStateNormal];
        [self setBackgroundImage:[self backgroundImageForState:UIControlStateSelected] forState:UIControlStateNormal];
    } else {
        [self setTitle:self.normalTitle forState:UIControlStateNormal];
        [self setTitleColor:self.normalTitleColor forState:UIControlStateNormal];
        [self setImage:self.normalImage forState:UIControlStateNormal];
        [self setBackgroundImage:self.normalBackgroundImage forState:UIControlStateNormal];
    }
    
    [super setSelected:selected];
}

- (void)setManualSelection:(BOOL)val
{
    _manualSelection = val;
    [self removeTarget:self action:@selector(selfTouched:) forControlEvents:UIControlEventTouchUpInside];
    if(!_manualSelection){
        [self addTarget:self action:@selector(selfTouched:) forControlEvents:UIControlEventTouchUpInside];
    }
}

#pragma mark - Accessors
- (NSString *)normalTitle
{
    if(_normalTitle == nil){
        self.normalTitle = [super titleForState:UIControlStateNormal];
    }
    return _normalTitle;
}

- (UIColor *)normalTitleColor
{
    if(_normalTitleColor == nil){
        self.normalTitleColor = [super titleColorForState:UIControlStateNormal];
        
    }
    return _normalTitleColor;
}

- (UIImage *)normalImage
{
    if(_normalImage == nil){
        self.normalImage = [super imageForState:UIControlStateNormal];
    }
    
    return _normalImage;
}

- (UIImage *)normalBackgroundImage
{
    if(_normalBackgroundImage == nil){
        self.normalBackgroundImage = [super backgroundImageForState:UIControlStateNormal];
    }
    return _normalBackgroundImage;
}


@end
