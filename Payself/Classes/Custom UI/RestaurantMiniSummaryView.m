//
//  RestaurantMiniSummaryView.m
//  RushOrder
//
//  Created by Conan on 4/24/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import "RestaurantMiniSummaryView.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface RestaurantMiniSummaryView()


@property (weak, nonatomic) IBOutlet UIImageView *eventFlagImageView;
@property (strong, nonatomic) IBOutlet UIImageView *favoriteIcon;
@property (strong, nonatomic) IBOutlet UIImageView *eventIcon;
@property (weak, nonatomic) IBOutlet UILabel *eventTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventLabel;

@property (strong, nonatomic) IBOutlet NPStretchableButton *processButton;
@property (strong, nonatomic) IBOutlet NPStretchableButton *takeoutButton;
@property (strong, nonatomic) IBOutlet DoubleImageButton *deliveryButton;

@property (weak, nonatomic) IBOutlet UIImageView *panelImageView;
@property (weak, nonatomic) IBOutlet UILabel *panelLabel;

@property (weak, nonatomic) IBOutlet UIView *buttonsContainer;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *panelHeightConstraint;

@end

@implementation RestaurantMiniSummaryView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self){

    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
//    self.deliveryButton.subImage = [UIImage imageNamed:@"icon_sf"];
}
- (void)drawRestaurant:(BOOL)forHeight
{
//    self.deliveryButton.subImageOn = self.merchant.hasRoServiceFee;
    
    if(self.merchant.isDemoMode || (APP.location == nil)){
        self.distanceLabel.hidden = YES;
        self.distanceLabel.text = nil;
    } else {
        self.distanceLabel.hidden = NO;
        self.distanceLabel.text = self.merchant.distanceString;
    }
    
    [self.distanceLabel updateConstraints];
    
    self.titleLabel.text = self.merchant.displayName;
//    [self.titleLabel drawBorder];
    [self.titleLabel updateConstraints];
    
//    self.titleLabel.text = @"akdjflakjdsf akljdf allkasjd flkajsdf lkja sldjkf aljfdk alkjfd alfk a end";
    
    self.addressLabel.text = self.merchant.displayAddress;
//    [self.addressLabel drawBorder];
    [self.addressLabel updateConstraints];
    
    if(self.merchant.isOpenHour){
        if(self.merchant.isDemoMode){
            [self turnDemoFlagOn:YES];
        } else {
            [self turnTestFlagOn:self.merchant.isTestMode];
        }
    } else {
        [self turnCloseFlagOn:YES];
    }
    
    BOOL showPromoIcon = NO;
    if(self.merchant.promotion != nil){
        showPromoIcon = YES;
//        self.eventTitleLabel.text = NSLocalizedString(@"Promo", nil);
        
        
        NSString *shortenedTitle = NSLocalizedString(@"Promo", nil);
        if([self.merchant.promotion.title length] > 0){
            shortenedTitle = self.merchant.promotion.title;
        };
        
        /*
        NSRange offRange = [self.merchant.promotion.title rangeOfString:@"off"
                                                                options:NSCaseInsensitiveSearch];
        
        if([self.merchant.promotion.title length] > 10){
            if(offRange.location > 10 || offRange.location == NSNotFound){
                shortenedTitle = [self.merchant.promotion.title substringToIndex:10];
            } else {
                shortenedTitle = [self.merchant.promotion.title substringToIndex:offRange.location + offRange.length];
            }
        } else {
            shortenedTitle = self.merchant.promotion.title;
        }
        */
        
        self.eventTitleLabel.text = shortenedTitle;
        [self.eventTitleLabel updateConstraints];
        self.eventIcon.hidden = NO;
    } else {
        if([self.merchant.creditPolicyName length] > 0){
            showPromoIcon = YES;
            self.eventTitleLabel.text = NSLocalizedString(@"Event", nil);
            self.eventTitleLabel.text = self.merchant.creditPolicyName;
            [self.eventTitleLabel updateConstraints];
            self.eventIcon.hidden = NO;
        } else {
            self.eventTitleLabel.text = nil;
            self.eventTitleLabel.text = nil;
            self.eventIcon.hidden = YES;
        }
    }
    
//    [self.eventTitleLabel drawBorder];
//    [self.eventTitleLabel drawBorder];
    
    if(!forHeight){
        
        NSURL *imgURL = self.merchant.logoURL;
        if(imgURL == nil) imgURL = self.merchant.imageURL;
        
        [self.logoImage sd_setImageWithURL:imgURL
                          placeholderImage:[UIImage imageNamed:@"placeholder_logo"]
                                   options:0
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                     PCLog(@"Logo image %@\n%@", imgURL, image);
                                 }];
        
        [self.favoriteIcon removeFromSuperview];

        
        NSDictionary *views = @{@"favoriteIcon":self.favoriteIcon,
                                @"titleLabel":self.titleLabel};
        
        NSArray *exConstraints = [self constraintsForAttribute:NSLayoutAttributeTrailing
                                                withSecondItem:self.titleLabel];
        [self removeConstraints:exConstraints];
        
        if(self.merchant.isFavorite){
            [self addSubview:self.favoriteIcon];
            NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"[titleLabel]-5-[favoriteIcon]-10-|"
                                                                           options:0
                                                                           metrics:nil
                                                                             views:views];
            [self addConstraints:constraints];
            constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-16-[favoriteIcon]"
                                                                  options:0
                                                                  metrics:nil
                                                                    views:views];
            [self addConstraints:constraints];

        } else {
            NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"[titleLabel]-10-|"
                                                                           options:0
                                                                           metrics:nil
                                                                             views:views];
            [self addConstraints:constraints];
        }
        
        [self.buttonsContainer removeAllSubviews];
        [self.buttonsContainer updateConstraints];
        NSArray *containerConstraints = [self.buttonsContainer constraints];
        [self.buttonsContainer removeConstraints:containerConstraints];
        
        NPStretchableButton *leftButton = nil;
        NPStretchableButton *middleButton = nil;
        NPStretchableButton *rightButton = nil;
        NPStretchableButton *soleButton = nil;
        
        if(self.merchant.menuOrder){
            if(self.merchant.isAbleTakeout && self.merchant.isAbleDelivery){
                if(self.merchant.isAbleDinein){
                    // Dine-in , Takeout, Delivery
                    
                    [self.buttonsContainer addSubview:self.processButton];
                    leftButton = self.processButton;
                    [self.buttonsContainer addSubview:self.takeoutButton];
                    middleButton = self.takeoutButton;
                    [self.buttonsContainer addSubview:self.deliveryButton];
                    rightButton = self.deliveryButton;
                    
                } else {
                    // Takeout, Delivery
                    [self.buttonsContainer addSubview:self.takeoutButton];
                    leftButton = self.takeoutButton;
                    [self.buttonsContainer addSubview:self.deliveryButton];
                    rightButton = self.deliveryButton;
                  
                }
            } else if(self.merchant.isAbleTakeout){
                if(self.merchant.isAbleDinein){
                    // Dine-in , Takeout
                    [self.buttonsContainer addSubview:self.processButton];
                    leftButton = self.processButton;
                    [self.buttonsContainer addSubview:self.takeoutButton];
                    rightButton = self.takeoutButton;
                    
                } else {
                    // Takeout
                    [self.buttonsContainer addSubview:self.takeoutButton];
                    soleButton = self.takeoutButton;
                }
                
            } else if(self.merchant.isAbleDelivery){
                if(self.merchant.isAbleDinein){
                    // Dine-in, Delivery
                    [self.buttonsContainer addSubview:self.processButton];
                    [self.buttonsContainer addSubview:self.deliveryButton];
                    leftButton = self.processButton;
                    rightButton = self.deliveryButton;
                } else {
                    // Delivery
                    [self.buttonsContainer addSubview:self.deliveryButton];
                    soleButton = self.deliveryButton;
                }
            } else {
                if(self.merchant.isAbleDinein){
                    // Dine-in
                    [self.buttonsContainer addSubview:self.processButton];
                    soleButton = self.processButton;
                    
                } else {
                    //Error

                }
            }
            
            if(self.merchant.isTableBase){
                self.processButton.tag = CartTypeCart;
            } else {
                if(self.merchant.isServicedByStaff){
                    self.processButton.tag = CartTypePickupServiced;
                } else {
                    self.processButton.tag = CartTypePickup;
                }
            }
            
        } else {
            // Dine-in
            [self.buttonsContainer addSubview:self.processButton];
            soleButton = self.processButton;
        }
        
        NSMutableDictionary *viewsDictionary = [NSMutableDictionary dictionaryWithObject:self.buttonsContainer
                                                                                  forKey:@"buttonsContainer"];
        if(leftButton) [viewsDictionary setObject:leftButton forKey:@"leftButton"];
        if(middleButton) [viewsDictionary setObject:middleButton forKey:@"middleButton"];
        if(rightButton) [viewsDictionary setObject:rightButton forKey:@"rightButton"];
        if(soleButton) [viewsDictionary setObject:soleButton forKey:@"soleButton"];
        
        containerConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[buttonsContainer(==35@1000)]"
                                                                                 options:0
                                                                                 metrics:nil
                                                                                   views:viewsDictionary];
        [self.buttonsContainer addConstraints:containerConstraints];
        
        NSMutableString *formatString = [NSMutableString string];
        [formatString appendString:@"|"];
        if(soleButton){
            [formatString appendString:@"-0-[soleButton]"];
        } else {
            if(leftButton){
                [formatString appendString:@"-0-[leftButton]"];
            }
            if(middleButton){
                [formatString appendString:@"-0-[middleButton(==leftButton)]"];
            }
            if(rightButton){
                if(middleButton){
                    [formatString appendString:@"-0-"];
                } else {
                    [formatString appendString:@"-(-1)-"];
                }
                [formatString appendString:@"[rightButton(==leftButton)]"];
            }
        }
        [formatString appendString:@"-0-|"];
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:formatString
                                                              options:0
                                                              metrics:nil
                                                                views:viewsDictionary];
        [self.buttonsContainer addConstraints:constraints];
        
        if(soleButton){
            constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[soleButton]|"
                                                                  options:0
                                                                  metrics:nil
                                                                    views:viewsDictionary];
            [self.buttonsContainer addConstraints:constraints];
        } else {
            if(leftButton){
                constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[leftButton]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:viewsDictionary];
                [self.buttonsContainer addConstraints:constraints];
            }
            if(middleButton){
                constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[middleButton]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:viewsDictionary];
                [self.buttonsContainer addConstraints:constraints];
            }
            if(rightButton){
                constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[rightButton]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:viewsDictionary];
                [self.buttonsContainer addConstraints:constraints];
            }
        }
        
        if(soleButton){
            [soleButton setBackgroundImage:[UIImage imageNamed:@"button_order"]
                                  forState:UIControlStateNormal];
            [soleButton setBackgroundImage:[UIImage imageNamed:@"button_order_h"]
                                  forState:UIControlStateHighlighted];
            [soleButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
    
        } else {
            if(leftButton){
                [leftButton setBackgroundImage:[UIImage imageNamed:@"button_order_left"]
                                      forState:UIControlStateNormal];
                [leftButton setBackgroundImage:[UIImage imageNamed:@"button_order_left_h"]
                                      forState:UIControlStateHighlighted];
                [leftButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
            }
            if(middleButton){
                [middleButton setBackgroundImage:[UIImage imageNamed:@"button_order_middle"]
                                      forState:UIControlStateNormal];
                [middleButton setBackgroundImage:[UIImage imageNamed:@"button_order_middle_h"]
                                      forState:UIControlStateHighlighted];
                [middleButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
            }
            if(rightButton){
                [rightButton setBackgroundImage:[UIImage imageNamed:@"button_order_right"]
                                      forState:UIControlStateNormal];
                [rightButton setBackgroundImage:[UIImage imageNamed:@"button_order_right_h"]
                                      forState:UIControlStateHighlighted];
                [rightButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
            }
        }
        
    }
    [self updateConstraints];
}

- (void)turnTestFlagOn:(BOOL)on
{
    self.panelImageView.hidden = !on;
    self.panelLabel.hidden = !on;
    self.panelLabel.text = NSLocalizedString(@"TEST", nil);
    self.panelHeightConstraint.constant = on ? 39.0f : 0.0f;
}

- (void)turnDemoFlagOn:(BOOL)on
{
    self.panelImageView.hidden = !on;
    self.panelLabel.hidden = !on;
    self.panelLabel.text = NSLocalizedString(@"DEMO", nil);
    self.panelHeightConstraint.constant = on ? 39.0f : 0.0f;
}

- (void)turnCloseFlagOn:(BOOL)on
{
    self.panelImageView.hidden = !on;
    self.panelLabel.hidden = !on;
    self.panelLabel.text = NSLocalizedString(@"Closed", nil);
    self.panelHeightConstraint.constant = on ? 39.0f : 0.0f;
}

- (IBAction)actionButtonTouched:(id)sender
{
    if([self.delegate respondsToSelector:@selector(restaurantMiniSummaryView:didTouchActionButton:)]){
        [self.delegate restaurantMiniSummaryView:self
                            didTouchActionButton:sender];
    }
}
@end
