//
//  CircleImageView.m
//  RushOrder
//
//  Created by Conan on 8/12/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import "CircleImageView.h"

@implementation CircleImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {

        
    }
    return self;
}

- (void)commonInit:(CGRect)rect
{
    self.layer.cornerRadius = rect.size.width / 2;
    
    if(self.isBorder){
        self.layer.borderWidth = 2.0f;
        self.layer.borderColor = [UIColor colorWithWhite:1.0f alpha:0.5f].CGColor;
    }

    self.clipsToBounds = YES;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self commonInit:self.frame];
    
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    self.layer.cornerRadius = rect.size.width / 2;
    if(self.isBorder){
        self.layer.borderWidth = 2.0f;
        self.layer.borderColor = [UIColor colorWithWhite:1.0f alpha:0.5f].CGColor;
    }
}




@end
