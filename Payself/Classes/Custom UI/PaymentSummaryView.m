//
//  PaymentSummaryView.m
//  RushOrder
//
//  Created by Conan on 12/23/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PaymentSummaryView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PCCredentialService.h"
#import "DiscountViewController.h"
#import "CustomIOSAlertView.h"
#import "MerchantViewController.h"
#import "OrderManager.h"
#import "ServiceFeeGuideView.h"

#define USE_SIMPLE_TIP

@interface PaymentSummaryView()
{
    CGFloat promoContainerInitialHeight_;
    CGFloat signinContainerInitialHeight_;
}

@property (weak, nonatomic) IBOutlet UILabel *cardAmountTitleLabel;
@property (weak, nonatomic) IBOutlet LogoCircleImageView *logoImageView;
@property (weak, nonatomic) IBOutlet TouchableLabel *addressLabel;
//@property (weak, nonatomic) IBOutlet UILabel *eventLabel;
//@property (weak, nonatomic) IBOutlet UILabel *eventTitleLabel;
//@property (weak, nonatomic) IBOutlet UIImageView *eventFlagImageView;
@property (weak, nonatomic) IBOutlet UILabel *tableNoTitleLabel;
@property (weak, nonatomic) IBOutlet TouchableLabel *merchantNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tableNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtotalAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtotalAmountTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *taxInfoButton;
@property (weak, nonatomic) IBOutlet UILabel *taxAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *tipTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxInfoViewDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxInfoViewServiceFeeInfoLabel;

@property (weak, nonatomic) IBOutlet UILabel *discountTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *smallCutAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *smallCutTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointAmountTitleLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *tipManualButton;
@property (weak, nonatomic) IBOutlet UILabel *deliveryFeeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryFeeLabel;
@property (weak, nonatomic) IBOutlet UIButton *discountButton;
@property (strong, nonatomic) IBOutlet UIView *sfGuideView;
@property (weak, nonatomic) IBOutlet UILabel *sfGuideLabel;

@property (weak, nonatomic) IBOutlet UILabel *roServiceFeeLabel;
@property (weak, nonatomic) IBOutlet UILabel *roServiceFeeTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *roServiceButton;

@property (strong, nonatomic) IBOutlet UIPickerView *tipPickerView;
@property (strong, nonatomic) IBOutlet UIToolbar *tipInputAccessoryView;

@property (nonatomic, getter = isDefaultTipApplied) BOOL defaultTipApplied;
@property (nonatomic, getter = isTipChecked) BOOL tipChecked;

@property (nonatomic) PCCurrency initialOrderAmount;


@property (weak, nonatomic) IBOutlet UIView *cardPointContainer;
@property (weak, nonatomic) IBOutlet UIView *signInContainer;
@property (weak, nonatomic) IBOutlet UIView *promoContainer;
@property (weak, nonatomic) IBOutlet UILabel *tipUsageLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deliveryFeeVerticalSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *serviceFeeVerticalSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cardContainerTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cardContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *promoContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *signinContainerConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tipTextFieldHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *smallCutTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discountTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orangePointTopConstraint;
@property (strong, nonatomic) IBOutlet UIView *tipButtonsView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *keyboardTitleButton;
@property (strong, nonatomic) IBOutlet UIView *taxGuideView;



@property (nonatomic) NSUInteger selecdtedTipPercent;

@end

@implementation PaymentSummaryView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.tipTextField.inputView = self.tipButtonsView;
    self.tipTextField.inputAccessoryView = self.tipInputAccessoryView;
    
    self.tipTextField.text = nil;
    
    
//    self.tipTextField.forwardDelegate = self;
    self.tipTextField.delegate = self;
    
    promoContainerInitialHeight_ = self.promoContainerHeightConstraint.constant;
    signinContainerInitialHeight_ = self.signinContainerConstraint.constant;
    self.logoImageView.target = self;
    self.merchantNameLabel.target = self;
    self.addressLabel.target = self;
    
    self.couponCodeTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_coupon"]];
    self.couponCodeTextField.leftViewMode = UITextFieldViewModeAlways;
    
}

- (void)drawPayment
{
    if([self.delegate respondsToSelector:@selector(paymentSummaryViewWillViewHeightChagned:)]){
        [self.delegate paymentSummaryViewWillViewHeightChagned:self];
    }
    
    self.merchantNameLabel.text = self.order.merchantName;
    self.merchantNameLabel.preferredMaxLayoutWidth = self.merchantNameLabel.width;
    [self.merchantNameLabel setNeedsUpdateConstraints];
    [self.merchantNameLabel updateConstraints];
    
    self.addressLabel.text = self.order.merchantDesc;
    
    if(self.order.tableNumber == nil){
        self.tableNumberLabel.hidden = YES;
        self.tableNumberLabel.text = nil;
    } else {
        self.tableNumberLabel.hidden = NO;
        self.tableNumberLabel.text = [NSString stringWithFormat:@"%@", self.order.tableNumber];
    }
    self.tableNoTitleLabel.hidden = self.tableNumberLabel.hidden;
    
    self.subtotalAmountLabel.text = self.payment.subTotalString;
    self.taxAmountLabel.text = self.payment.taxNroServiceFeeAmountString;
    if(self.payment.roServiceFeeAmount > 0){ //Merge Service Fee + Tax in one line
        self.taxTitleLabel.text = NSLocalizedString(@"Tax & Fees", nil);
        self.taxInfoButton.hidden = NO;
        
        self.taxInfoViewDescLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Tax - %@\nService Fee - %@\nTotal - %@", nil)
                                          , self.payment.taxAmountString
                                          , self.payment.roServiceFeeAmountString
                                          , self.payment.taxNroServiceFeeAmountString];
        self.taxInfoViewServiceFeeInfoLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Orders from %@ include a %g%% Service Fee.", nil)
                                                    , self.order.merchantName
                                                    , self.merchant.roServiceRate];
    } else {
        self.taxTitleLabel.text = NSLocalizedString(@"Tax", nil);
        self.taxInfoButton.hidden = YES;
    }
    
    if(self.payment.deliveryFeeAmount > 0 || self.order.orderType == OrderTypeDelivery){
        self.deliveryFeeTitleLabel.hidden = NO;
        self.deliveryFeeLabel.hidden = NO;
        self.deliveryFeeLabel.text = self.payment.deliveryFeeAmountString;
        self.deliveryFeeVerticalSpaceConstraint.constant = 7.0f;
    } else {
        self.deliveryFeeTitleLabel.hidden = YES;
        self.deliveryFeeLabel.hidden = YES;
        self.deliveryFeeLabel.text = nil;
        self.deliveryFeeVerticalSpaceConstraint.constant = 0.0f;
    }
    
    if(self.merchant.promotion != nil){
        if(CRED.isSignedIn){
            [self turnOnSignIn:NO];
            
            for(Promotion *promotion in [self.merchant promotionsOnOrderType:self.order.orderType]){
                [self.payment applyPromotion:promotion];
            }
        } else {
            [self turnOnSignIn:YES];
        }
    } else {
        [self turnOnSignIn:NO];
    }
    
    if(self.payment.discountAmount > 0){
        self.discountAmountLabel.text = self.payment.discountAmountString;
        self.discountTitleLabel.text = NSLocalizedString(@"Discount", nil);
        self.discountTopConstraint.constant = 7.0f;
        self.discountButton.hidden = NO;
    } else {
        self.discountAmountLabel.text = nil;
        self.discountTitleLabel.text = nil;
        self.discountTopConstraint.constant = 0.0f;
        self.discountButton.hidden = YES;
    }
    
    [self.discountAmountLabel updateConstraints];
    
    // !!!:Don't move this code below under [self textFieldValueChanged:self.tipTextField];
//    PCLog(@"Payment String %@", self.payment.payAmountString);
    self.totalPayAmountLabel.text = self.payment.payAmountString;

    if(self.order.orderType == OrderTypeDelivery && ORDER.merchant.isRoService){
        self.tipUsageLabel.hidden = NO;
    } else {
        self.tipUsageLabel.hidden = YES;
    }
    
    if(self.merchant.isAllowTip){
        
        self.tipTitleLabel.hidden = NO;
        self.tipTextField.hidden = NO;
        self.tipTextFieldHeightConstraint.constant = 30.0f;
        
        
                
        if(self.payment.tipSetByUser){
            
            self.tipTextField.amount = MAX(0, self.payment.tipAmount);
            
        } else {
            
            float tipRate = 0.0f;
            
            NSNumber *tipAmountSaved = nil;
            switch(self.order.orderType){
                case OrderTypeDelivery:
//                    tipAmountSaved = [[NSUserDefaults standardUserDefaults] objectForKey:tipRateLastUsedDelivery]; //https://app.asana.com/0/11543106091665/108690098988023
                    tipAmountSaved = nil;
                    if([NSUserDefaults standardUserDefaults].adoptLastDeliveryTipRate
                       && tipAmountSaved != nil){
                        
                        tipRate = MAX([NSUserDefaults standardUserDefaults].tipDeliveryRate, 0.0f);
                        self.autoTipApplied = YES;
                    }
                    
                    break;
                case OrderTypeTakeout:
//                    tipAmountSaved = [[NSUserDefaults standardUserDefaults] objectForKey:tipRateLastUsedTakeout]; //https://app.asana.com/0/11543106091665/108690098988023
                    tipAmountSaved = nil;
                    if([NSUserDefaults standardUserDefaults].adoptLastTakeoutTipRate
                       && tipAmountSaved != nil){
                        tipRate = MAX([NSUserDefaults standardUserDefaults].tipTakeoutRate, 0.0f);
                        self.autoTipApplied = YES;
                    }
                    break;
                case OrderTypeBill:
                case OrderTypeCart:
                case OrderTypePickup:
                default:
//                    tipAmountSaved = [[NSUserDefaults standardUserDefaults] objectForKey:tipRateLastUsed]; //https://app.asana.com/0/11543106091665/108690098988023
                    tipAmountSaved = nil;
                    if([NSUserDefaults standardUserDefaults].adoptLastTipRate
                       && tipAmountSaved != nil){
                        tipRate = MAX([NSUserDefaults standardUserDefaults].tipRate, 0.0f);
                        self.autoTipApplied = YES;
                    }
                    break;
            }
            
            self.autoTipSetOnce = (tipAmountSaved != nil);
            
            if(self.autoTipApplied){
                if(self.payment.subTotal > 0){
                    self.tipTextField.amount = MAX(0, roundf(self.payment.subTotal * ((float)tipRate / 100.0)));
                } else {
                    self.tipTextField.amount = 0;
                }
            } else {
                self.tipTextField.amount = 0;
                self.tipTextField.text = nil;
            }
        }
        
    } else {
        self.tipTitleLabel.hidden = YES;
        self.tipTextField.hidden = YES;
        self.tipTextFieldHeightConstraint.constant = 0.0f;
        self.tipTextField.amount = 0;
    }
    
    [self textFieldValueChanged:nil]; // !!!:Important this code decide the amount of payment
    
//    if(self.payment.roServiceFeeAmount > 0){ //Merge Service Fee + Tax in one line
//        self.roServiceFeeTitleLabel.hidden = NO;
//        self.roServiceFeeLabel.hidden = NO;
//        self.roServiceButton.hidden = NO;
//        self.roServiceFeeLabel.text = self.payment.roServiceFeeAmountString;
//        self.serviceFeeVerticalSpaceConstraint.constant = 7.0f;
//    } else {
        self.roServiceFeeTitleLabel.hidden = YES;
        self.roServiceFeeLabel.hidden = YES;
        self.roServiceButton.hidden = YES;
        self.roServiceFeeLabel.text = nil;
        self.serviceFeeVerticalSpaceConstraint.constant = 0.0f;
//    }
    
#define MARGIN_FOR_BOTTOM 8.0f
    
    if(!self.promoContainer.hidden){
        self.bottomSpaceConstraint.constant = promoContainerInitialHeight_ + MARGIN_FOR_BOTTOM;
    } else if (!self.signInContainer.hidden){
        self.bottomSpaceConstraint.constant = signinContainerInitialHeight_ + MARGIN_FOR_BOTTOM;
    } else {
        self.bottomSpaceConstraint.constant = 0;
    }
    
    NSURL *imgURL = self.merchant.logoURL;
    if(imgURL == nil) imgURL = self.merchant.imageURL;
    
    [self.logoImageView sd_setImageWithURL:imgURL
                          placeholderImage:[UIImage imageNamed:@"placeholder_logo"]
                                   options:0
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                     
                                 }];
//    if(CRED.customerInfo.credit > 0){
    if(/* DISABLES CODE */ (NO)){
        self.cardPointContainer.hidden = NO;
        self.cardContainerTopConstraint.constant = 8.0f;
        self.cardContainerHeightConstraint.constant = 27.0f;
        
        self.pointAmountLabel.hidden = NO;
        self.pointAmountTitleLabel.hidden = NO;
        self.pointAmountTitleLabel.text = NSLocalizedString(@"Orange Points", nil);
        self.cardAmountTitleLabel.hidden = NO;
        self.cardAmountLabel.hidden = NO;
        self.pointAmountLabel.text = self.payment.creditConvertedAmountString;
        self.pointConvertedLabel.text = self.payment.creditConvertedAmountStringInParenthesis;
        self.cardAmountLabel.text = self.payment.netAmountString;
    } else {
        self.cardPointContainer.hidden = YES;
        self.cardContainerTopConstraint.constant = 0.0f;
        self.cardContainerHeightConstraint.constant = 0.0f;
        
        self.pointAmountLabel.hidden = YES;
        self.pointAmountTitleLabel.hidden = YES;
        self.pointAmountTitleLabel.text = nil;
        self.cardAmountTitleLabel.hidden = YES;
        self.pointAmountLabel.text = nil;
        self.pointConvertedLabel.text = nil;
        self.cardAmountLabel.hidden = YES;
        
    }
    
    BOOL showOrange = NO;
#if USE_INLINE_ORANGE
    showOrange = (CRED.customerInfo.credit > 0);
#endif
    
    if(showOrange){
        self.orangePointTopConstraint.constant = 8.0;
        self.orangeUseSwitch.hidden = NO;
        self.orangePointLabel.text = self.payment.creditConvertedAmountStringAsDiscount;
        self.orangePointTitleLabel.text = [NSString stringWithFormat:NSLocalizedString(@"(%@ Orange Points)", nil)
                                           ,[[NSNumber numberWithInteger:CRED.customerInfo.credit] pointString]];
    } else {
        self.orangePointTopConstraint.constant = 0.0;
        self.orangeUseSwitch.hidden = YES;
        self.orangePointLabel.text = nil;
    }
    self.orangePointTitleLabel.hidden = self.orangeUseSwitch.hidden;
    self.orangePointLabel.hidden = self.orangeUseSwitch.hidden;


    
    [self setNeedsUpdateConstraints];
    [self updateConstraintsIfNeeded];
    [self setNeedsLayout];
    [self layoutIfNeeded];
    
    self.height = [self systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    
    if([self.delegate respondsToSelector:@selector(paymentSummaryViewDidViewHeightChagned:)]){
        [self.delegate paymentSummaryViewDidViewHeightChagned:self];
    }
}

- (void)circleImageViewActionTriggerred:(id)sender
{
    MerchantViewController *viewController = [UIStoryboard viewController:@"MerchantViewController"
                                                                     from:@"Restaurants"];
    [APP.tabBarController presentViewControllerInNavigation:viewController
                                                   animated:YES
                                                 completion:^{
                                                     
                                                 }];
}

- (void)touchableLabelActionTriggerred:(id)sender
{
    [self circleImageViewActionTriggerred:sender];
}

- (void)turnOnSignIn:(BOOL)on
{
    self.promoContainer.hidden = on;
    self.promoContainerHeightConstraint.constant = !self.promoContainer.hidden ? promoContainerInitialHeight_ : 0.0f;
    
    self.signInContainer.hidden = !on;
    self.signinContainerConstraint.constant = !self.signInContainer.hidden ? signinContainerInitialHeight_ : 0.0f;
}



- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == self.tipTextField){
        
        if(self.payment.total == 0 && self.tipTextField.inputView != nil){
            [UIAlertView alertWithTitle:NSLocalizedString(@"Total amount is $0.00", nil)
                                message:NSLocalizedString(@"To input tip, total amount should be greater than $0.00", nil)];

            self.tipTextField.amount = 0;
            
            return NO;
            
        } else {
            
            NSInteger tipRate = 0;
            
            switch(self.order.orderType){
                case OrderTypeDelivery:
                    tipRate = roundf([NSUserDefaults standardUserDefaults].tipDeliveryRate);
                    break;
                case OrderTypeTakeout:
                    tipRate = roundf([NSUserDefaults standardUserDefaults].tipTakeoutRate);
                    break;
                case OrderTypeBill:
                case OrderTypeCart:
                case OrderTypePickup:
                default:
                    tipRate = roundf([NSUserDefaults standardUserDefaults].tipRate);
                    break;
            }
            
            if(!self.autoTipApplied){
                tipRate = 15;
            }
            
            tipRate = MAX(0, MIN(20, tipRate));
            
            if(tipRate == 0.0f && ([textField.text length] == 0)){
                
                switch(self.order.orderType){
                    case OrderTypeDelivery:
                        tipRate = DEFAULT_TIP_DELIVERY_RATE;
                        break;
                    case OrderTypeTakeout:
                        tipRate = DEFAULT_TIP_TAKEOUT_RATE;
                        break;
                    case OrderTypeBill:
                    case OrderTypeCart:
                    case OrderTypePickup:
                    default:
                        tipRate = DEFAULT_TIP_RATE;
                        break;
                }
            }
            
            if(self.tipTextField.amount != 0.0f){
                if(self.payment.subTotal > 0){
                    tipRate = roundf(((float)self.tipTextField.amount / (float)self.payment.subTotal) * 100);
                } else {
                    tipRate = 0;
                }
                tipRate = MAX(0, tipRate);
            } else {
                if(self.isDefaultTipApplied){
                    tipRate = 0;
                }
            }
            
            if(self.tipTextField.amount > 0 && tipRate == 0){
                tipRate = 1;
            }
            
            for(UIButton *button in self.tipButtonsView.subviews){
                if([button isKindOfClass:[UIButton class]]){
                    if (button.tag == tipRate) {
                        button.selected = YES;
                        [self tipOptionButtonTouched:button];
                    } else {
                        button.selected = NO;
                    }
                }
            }
            
            self.defaultTipApplied = YES;
        }
    }
    
    return YES;
}


- (IBAction)tipManualButtonTouched:(UIButton *)sender
{
    [self.tipTextField resignFirstResponder];
    [self toggleTipInputView];
    [self.tipTextField becomeFirstResponder];
}

- (void)toggleTipInputView
{
    if(self.tipTextField.inputView == nil){
//        self.tipTextField.inputView = self.tipPickerView;
        self.tipTextField.inputView = self.tipButtonsView;
        self.tipTextField.inputAccessoryView = self.tipInputAccessoryView;
//        self.tipManualButton.title = NSLocalizedString(@"Other", nil);
        self.tipManualButton.title = nil;
        self.keyboardTitleButton.title = NSLocalizedString(@"Percent", nil);
        
#ifndef USE_SIMPLE_TIP
//        self.tipSliderBar.hidden = NO;
#endif
    } else {
        self.tipTextField.inputView = nil;
        self.tipTextField.inputAccessoryView = self.tipInputAccessoryView;
        self.tipManualButton.title = NSLocalizedString(@"Percent", nil);
        self.keyboardTitleButton.title = NSLocalizedString(@"Other", nil);

//        self.keyboardTitleButton.title = nil;
#ifndef USE_SIMPLE_TIP
//        self.tipSliderBar.hidden = YES;
#endif
    }
}

- (IBAction)percentButtonTouched:(id)sender
{
    [self.tipTextField resignFirstResponder];
    [self showTipAmount];
}

- (void)showTipAmount
{
#ifdef USE_SIMPLE_TIP

#else
    NSInteger componentIndex = 1;
#endif
    
    if(self.tipTextField.inputView != nil){
        CGFloat orderAmount = self.payment.subTotal;
        NSInteger tipRate = self.selecdtedTipPercent;
        
        if(orderAmount > 0 && tipRate > 0.0f){
            self.tipTextField.amount = MAX(0, roundf(orderAmount * ((float)tipRate / 100)));
        } else {
            self.tipTextField.amount = 0;
        }
        [self textFieldValueChanged:self.tipTextField];
//        [self textFieldDidBeginEditing:self.tipTextField];
    }
}

- (IBAction)textFieldValueChanged:(UITextField *)textField
{
    if(textField != nil){
        if(self.tipTextField.inputView == nil || ![self.tipTextField isFirstResponder]){
            if([self.delegate respondsToSelector:@selector(paymentSummaryViewWillViewHeightChagned:)]){
                [self.delegate paymentSummaryViewWillViewHeightChagned:self];
            }
        }
    }
    
    self.payment.tipAmount = self.tipTextField.amount;
    
    if([self.tipTextField.text length] > 0){
        self.payment.tipSetByUser = YES;
    }
    
    if(self.order.orderType == OrderTypeDelivery){
        [self.payment applyRoServiceFee:self.merchant];
    }
    
    self.payment.payAmount
    = self.payment.tipAmount
    + self.initialOrderAmount
    + self.payment.deliveryFeeAmount
    + self.payment.roServiceFeeAmount
    - self.payment.discountAmount;
    
    if(CRED.customerInfo.credit > 0){
        if(self.orangeUseSwitch.on){
            NSInteger maxUsableCredit = MIN(self.payment.payAmount, CRED.customerInfo.credit);
            self.payment.creditAmount = maxUsableCredit;
        } else {
            self.payment.creditAmount = 0;
        }
        
        if(!self.orangePointLabel.hidden){
            self.orangePointLabel.text = self.payment.creditConvertedAmountStringAsDiscount;
        } else {
            self.orangePointLabel.text = nil;
        }
    } else {
        self.payment.creditAmount = 0;
    }
    
    self.payment.payAmount -= self.payment.creditAmount;
    
    if(CRED.customerInfo.credit > 0){
        self.pointAmountLabel.text = self.payment.creditConvertedAmountString;
        self.pointConvertedLabel.text = self.payment.creditConvertedAmountStringInParenthesis;
        self.cardAmountLabel.text = self.payment.netAmountString;
    } else {
        self.cardAmountLabel.text = self.payment.netAmountString;
    }
    
    if(self.tipTextField.inputView == nil || ![self.tipTextField isFirstResponder]){
        if(0 < self.payment.payAmount && self.payment.payAmount < 50){
            self.smallCutPromotion = [Promotion smallCutPromotion];
            self.smallCutPromotion.discountAmount = self.payment.payAmount;
            self.smallCutPromotion.appliedDiscountAmount = self.payment.payAmount;
            
            self.smallCutAmountLabel.text = self.smallCutPromotion.appliedDiscountAmountString;
            self.smallCutTitleLabel.text = NSLocalizedString(@"RO Pays Difference",nil);
            self.smallCutTopConstraint.constant = 7.0f;
        } else {
            self.smallCutPromotion = nil;
            
            self.smallCutAmountLabel.text = nil;
            self.smallCutTitleLabel.text = nil;
            self.smallCutTopConstraint.constant = 0.0f;
        }
    }
    
    self.payment.payAmount
    = self.payment.tipAmount
    + self.initialOrderAmount
    + self.payment.deliveryFeeAmount
    + self.payment.roServiceFeeAmount
    - self.payment.discountAmount
    - self.smallCutPromotion.appliedDiscountAmount;
    
    self.totalPayAmountLabel.text = self.payment.netAmountString;
    
    
    if([self.delegate respondsToSelector:@selector(paymentSummaryViewTipAmountChanged:)]){
        [self.delegate paymentSummaryViewTipAmountChanged:self];
    }
    
    if(textField != nil){
        if(self.tipTextField.inputView == nil || ![self.tipTextField isFirstResponder]){
            [self updateConstraints];
            [self setNeedsDisplay];
            [self layoutIfNeeded];
            [self layoutSubviews];
            
            if([self.delegate respondsToSelector:@selector(paymentSummaryViewDidViewHeightChagned:)]){
                [self.delegate paymentSummaryViewDidViewHeightChagned:self];
            }
        }
    }
}

- (IBAction)signInButtonTouched:(id)sender
{
    if([self.delegate respondsToSelector:@selector(paymentSummaryView:didTouchSignInButton:)]){
        [self.delegate paymentSummaryView:self
                     didTouchSignInButton:sender];
    }
}

- (IBAction)applyButtonTouched:(id)sender
{
    [self endEditing:YES];
    
    if(![VALID validate:self.couponCodeTextField
                  title:NSLocalizedString(@"Promo Code Empty", nil)
                message:NSLocalizedString(@"Please input promo code", nil)]){
        return;
    }
    
    if([self.delegate respondsToSelector:@selector(paymentSummaryView:didTouchApplyCouponButton:)]){
        [self.delegate paymentSummaryView:self
                didTouchApplyCouponButton:sender];
    }
}

- (void)setPayment:(Payment *)payment
{
    self.initialOrderAmount = payment.orderAmount;
    _payment = payment;
}

- (IBAction)roServiceButtonTouched:(id)sender
{
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    
    ServiceFeeGuideView *view = [ServiceFeeGuideView view];
    view.guideLabel.text = [NSString stringWithFormat:NSLocalizedString(@"To offer delivery from places that don't normally deliver, we sometimes partner with 3rd parties who charge an added Service Fee. We're working hard to get these fees down, so just hang on tight!", nil),
                              [[NSNumber numberWithFloat:self.merchant.roServiceRate] percentString]];
    [alertView setContainerView:view];
    alertView.buttonTitles = [NSArray arrayWithObject:NSLocalizedString(@"Got it!", nil)];
    [alertView show];
    
    [view startAnimation];
}

- (IBAction)showDiscountView:(id)sender
{
    [self endEditing:YES];
    
    if([self.delegate respondsToSelector:@selector(paymentSummaryView:didTouchDiscountAmount:)]){
        [self.delegate paymentSummaryView:self
                   didTouchDiscountAmount:sender];
    }
}

- (IBAction)showFeesView:(id)sender
{
    [self endEditing:YES];
    
//    if([self.delegate respondsToSelector:@selector(paymentSummaryView:didTouchFeesAmount:)]){
//        [self.delegate paymentSummaryView:self
//                   didTouchDiscountAmount:sender];
//    }
    
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    [alertView setContainerView:self.taxGuideView];
    alertView.buttonTitles = [NSArray arrayWithObject:NSLocalizedString(@"OK", nil)];
    [alertView show];
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if(textField == self.tipTextField){
        [self textFieldValueChanged:textField];
        [self.tipTextField becomeFirstResponder];
        
        for(UIButton *button in self.tipButtonsView.subviews){
            if([button isKindOfClass:[UIButton class]]){
                button.selected = (button.tag == 0);
            }
        }
        
        self.selecdtedTipPercent = 0;
        
        return NO;
    } else {
        return YES;
    }
}

- (IBAction)orangeSwitchChanged:(UISwitch *)sender
{
    [self textFieldValueChanged:self.tipTextField];
//    [self.delegate paymentSummaryViewOrangeSwitchChanged:sender];
}

- (IBAction)tipOptionButtonTouched:(UIButton *)sender
{
    switch(sender.tag){
        case -1:
            self.selecdtedTipPercent = 0;
            self.tipTextField.text = nil;
            [self textFieldValueChanged:self.tipTextField];
            [self tipManualButtonTouched:sender];
            break;
        case 0:
        case 10:
        case 15:
        case 18:
        case 20:
        case 25:
        case 30:
            self.selecdtedTipPercent = sender.tag;
            self.tipChecked = YES;
            [self showTipAmount];
//            [self.tipTextField resignFirstResponder];
            break;
        case 999:
            [self tipManualButtonTouched:sender];
            break;
        default:
            // Do nothing
            break;
    }
    
    for(UIButton *button in self.tipButtonsView.subviews){
        if([button isKindOfClass:[UIButton class]]){
            button.selected = (button.tag == sender.tag);
        }
    }
    
}

//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    NSRange dotRange = [textField.text rangeOfString:@"."];
//    
//    UITextPosition *beginning = textField.beginningOfDocument;
//    UITextPosition *start = [textField positionFromPosition:beginning offset:dotRange.location];
//    UITextPosition *end = [textField positionFromPosition:start offset:0];
//    UITextRange *textRange = [textField textRangeFromPosition:start toPosition:end];
//    
//    [textField setSelectedTextRange:textRange];
//}

@end
