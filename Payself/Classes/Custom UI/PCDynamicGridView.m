//
//  PCDynamicGridView.m
//  RushOrder
//
//  Created by Conan on 3/6/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCDynamicGridView.h"
#import "TableButton.h"
#import "PostitButton.h"

int GetTileSize(int width, int height, int tileCount)
{
    // quick bailout for invalid input
    if (width*height < tileCount) { return 0; }
    
    // come up with an initial guess
    double aspect = (double)height/width;
    double xf = sqrtf(tileCount/aspect);
    double yf = xf*aspect;
    int x = MAX(1.0, floor(xf));
    int y = MAX(1.0, floor(yf));
    int x_size = floor((double)width/x);
    int y_size = floor((double)height/y);
    int tileSize = MIN(x_size, y_size);
    
    // test our guess:
    x = floor((double)width/tileSize);
    y = floor((double)height/tileSize);
    if (x*y < tileCount) // we guessed too high
    {
        if (((x+1)*y < tileCount) && (x*(y+1) < tileCount))
        {
            // case 2: the upper bound is correct
            //         compute the tileSize that will
            //         result in (x+1)*(y+1) tiles
            x_size = floor((double)width/(x+1));
            y_size = floor((double)height/(y+1));
            tileSize = MIN(x_size, y_size);
        }
        else
        {
            // case 3: solve an equation to determine
            //         the final x and y dimensions
            //         and then compute the tileSize
            //         that results in those dimensions
            int test_x = ceil((double)tileCount/y);
            int test_y = ceil((double)tileCount/x);
            x_size = MIN(floor((double)width/test_x), floor((double)height/y));
            y_size = MIN(floor((double)width/x), floor((double)height/test_y));
            tileSize = MAX(x_size, y_size);
        }
    }
    
    return tileSize;
}

@interface PCDynamicGridView()
{
    int tileWidth_;
    int tileHeight_;
    CGSize containerSize_;
    int column_;
    int row_;
}

@property (nonatomic) NSInteger drawnItemCount;
@end

@implementation PCDynamicGridView
@dynamic delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.drawnItemCount = 0;
    
    self.outerMargin = UIEdgeInsetsZero;
    self.itemSpan = 2.0f;
}

- (void)reloadData
{
    NSInteger itemCount = [self.dataSrouce numberOfItemsInGridView:self];
    if(self.drawnItemCount != itemCount){
        self.drawnItemCount = itemCount;
        [self arrangeItems];
    }
    
    [self updateCells];
}

- (void)updateCells
{
    for(UIView *subview in self.subviews){
        if([subview isKindOfClass:[TableButton class]]
           || [subview isKindOfClass:[PostitButton class]]){
            [self.dataSrouce gridView:self
                           updateCell:subview
                              atIndex:subview.tag];
        }
    }
}

- (void)updateCellAtIndex:(NSUInteger)index
{
    for(UIView *subview in self.subviews){
        if([subview isKindOfClass:[TableButton class]]
           || [subview isKindOfClass:[PostitButton class]]){
            if(subview.tag == index){
                [self.dataSrouce gridView:self
                               updateCell:subview
                                  atIndex:subview.tag];
                break;
            }
        }
    }
}

- (void)arrangeItems
{
    [self removeAllSubviews];
    
    containerSize_ = self.bounds.size;
    containerSize_.width -= (self.outerMargin.left + self.outerMargin.right);
    containerSize_.height -= (self.outerMargin.top + self.outerMargin.bottom);
    
    if(self.fixedSize){
        tileHeight_ = 200.0f;
        column_ = 4;
    } else {
        tileHeight_ = GetTileSize(containerSize_.width, containerSize_.height, (int)self.drawnItemCount);
        column_ = containerSize_.width / tileHeight_;
    }
    
    row_ = containerSize_.height / tileHeight_;
    
    if((column_ * (row_- 1)) >= self.drawnItemCount){
        row_--;
    }
    
    CGFloat sumWidth = column_ * tileHeight_;
    CGFloat remainee = containerSize_.width - sumWidth;
    CGFloat perRemainee = remainee / column_;
    tileWidth_ = tileHeight_ + perRemainee;
    
    if(!self.fixedSize){
        CGFloat sumHeight = row_ * tileHeight_;
        remainee = containerSize_.height - sumHeight;
        perRemainee = remainee / row_;
        tileHeight_ = tileHeight_ + perRemainee;
    }
    
    NSInteger i = 0;
    
    CGFloat lastItemMaxY = 0;
    
    for(i = 0 ; i < self.drawnItemCount ; i++){
        UIView *cell = [self.dataSrouce gridView:self
                                cellForItemIndex:i];
        
        CGRect frame = [self frameForIndex:i];
        cell.frame = frame;
        cell.tag = i;
        
        [self addSubview:cell];
        
        lastItemMaxY = MAX(lastItemMaxY, CGRectGetMaxY(cell.frame));
    }
    
    self.contentSize = CGSizeMake(self.bounds.size.width
                                  , lastItemMaxY);
}

- (CGRect)frameForIndex:(NSInteger)index
{
    CGRect rtnRect = CGRectZero;
    
    rtnRect.origin.x = 0.0f + ((index % column_) * tileWidth_);
    rtnRect.origin.y = 0.0f + ((index / column_) * tileHeight_);
    rtnRect.size.width = tileWidth_ - self.itemSpan;
    rtnRect.size.height = tileHeight_ - self.itemSpan;
    
    return rtnRect;
}


@end
