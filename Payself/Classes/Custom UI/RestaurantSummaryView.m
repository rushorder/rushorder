//
//  RestaurantSummaryView.m
//  RushOrder
//
//  Created by Conan on 11/25/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "RestaurantSummaryView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DZoneRadius.h"
#import "OrderManager.h"
#import "MerchantViewController.h"
#import "DZonePolygon.h"
#import "CustomIOSAlertView.h"
#import "ServiceFeeGuideView.h"
#import "RushOrder-Swift.h"

@interface RestaurantSummaryView()

@property (weak, nonatomic) IBOutlet TouchableLabel *restaurantNameLabel;
@property (weak, nonatomic) IBOutlet TouchableLabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventLabel;

@property (weak, nonatomic) IBOutlet UIImageView *eventFlagImageView;
@property (weak, nonatomic) IBOutlet UIImageView *panelImageView;
@property (weak, nonatomic) IBOutlet UILabel *panelLabel;
@property (weak, nonatomic) IBOutlet UIImageView *favoriteIcon;
@property (strong, nonatomic) IBOutlet UIView *promotionContainer;
@property (strong, nonatomic) IBOutlet UIView *deliveryDistanceInfoContainer;
@property (strong, nonatomic) IBOutlet UIView *roServiceContainer;
@property (strong, nonatomic) IBOutlet UILabel *deliveryLimitLabel;
@property (strong, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *minimumOrderAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *roServiceFeeLabel;
@property (weak, nonatomic) IBOutlet UILabel *averageDeliveryTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *averageDeliveryTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *moreDeliveryFeeButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *panelHeightConstraint;

@end

@implementation RestaurantSummaryView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    if([self.logoImageView respondsToSelector:@selector(setTarget:)]){
        self.logoImageView.target = self;
        self.restaurantNameLabel.target = self;
        self.addressLabel.target = self;
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)drawMerchant
{
    [self drawMerchant:nil
           forceHeight:NO
            isDelivery:NO
             showPromo:YES
         showRoService:YES];
}

- (void)drawMerchantForceHeight:(BOOL)forceHeight
{
    [self drawMerchant:nil
           forceHeight:forceHeight
            isDelivery:NO
             showPromo:YES
         showRoService:YES];
}

- (void)drawMerchant:(Merchant *)merchant
{
    [self drawMerchant:merchant
           forceHeight:NO
            isDelivery:NO
             showPromo:YES
         showRoService:YES];
}

- (void)drawMerchant:(Merchant *)merchant
         forceHeight:(BOOL)forceHeight
          isDelivery:(BOOL)isDelivery
           showPromo:(BOOL)showPromo
       showRoService:(BOOL)showRoService
{
    [self drawMerchant:merchant
           forceHeight:forceHeight
            isDelivery:isDelivery
             showPromo:showPromo
         showRoService:showRoService
              cartType:CartTypeUnknown];
}

- (void)drawMerchant:(Merchant *)merchant
         forceHeight:(BOOL)forceHeight
          isDelivery:(BOOL)isDelivery
           showPromo:(BOOL)showPromo
       showRoService:(BOOL)showRoService
            cartType:(CartType)cartType
{

    if(merchant != nil){
        self.merchant = merchant;
    }
    
    self.restaurantNameLabel.text = self.merchant.displayName;

    self.addressLabel.text = self.merchant.displayAddress;
    self.favoriteIcon.hidden = !self.merchant.isFavorite;
    
    NSURL *imgURL = self.merchant.logoURL;
    if(imgURL == nil) imgURL = self.merchant.imageURL;
    
    [self.logoImageView sd_setImageWithURL:imgURL
                       placeholderImage:[UIImage imageNamed:@"placeholder_logo"]
                                options:0
                              completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                  
                              }];
    
    if(self.merchant.isOpenHour){
        if(self.merchant.isDemoMode){
            [self turnDemoFlagOn:YES];
        } else {
            [self turnTestFlagOn:self.merchant.isTestMode];
        }
    } else {
        [self turnCloseFlagOn:YES];
    }
    
    if(showPromo){
        Promotion *selectedPromotion = nil;
        if(CartTypeUnknown == cartType){
            selectedPromotion = self.merchant.promotion;
        } else {
            selectedPromotion = [self.merchant promotionOnCartType:cartType];
        }
        
        if(selectedPromotion != nil){
            [self addPromotionContainer:YES];
//            if([selectedPromotion.desc length] > 0){
//                self.eventLabel.text = selectedPromotion.desc;
//            } else {
//                self.eventLabel.text = selectedPromotion.title;
//            }

            self.eventLabel.text = selectedPromotion.desc;
            if([selectedPromotion.title length] > 0){
                self.eventTitleLabel.text = selectedPromotion.title;
            } else {
                self.eventTitleLabel.text = NSLocalizedString(@"Promotion", nil);
            }
        } else {
            if([self.merchant.creditPolicyName length] > 0){
                [self addPromotionContainer:YES];
                self.eventLabel.text = self.merchant.creditPolicyName;
                self.eventTitleLabel.text = NSLocalizedString(@"Event", nil);
            } else {
                [self addPromotionContainer:NO];
                self.promotionContainer.hidden = YES;
            }
        }
    } else {
        [self addPromotionContainer:NO];
        self.promotionContainer.hidden = YES;
    }
    
    if(isDelivery){
        [self addDeliveryInfoContainer:YES];
        
        PCCurrency maxDiscountAmount = 0;
        
        if ([ORDER.merchant.deliveryFeeDiscount count] > 0){
            for(NSDictionary *discountDict in ORDER.merchant.deliveryFeeDiscount){
                if(maxDiscountAmount < [discountDict currencyForKey:@"discount_amount"]){
                    maxDiscountAmount = [discountDict currencyForKey:@"discount_amount"];
                }
            }
        }
        
        switch(merchant.deliveryFeePlan){
            case DeliveryFeePlanFlat:
            {
                if(ORDER.merchant.deliveryFee == 0){
                    self.deliveryLimitLabel.text = NSLocalizedString(@"Free", nil);
                } else {
                    
                    NSString *firstString = NSLocalizedString(@"Free", nil);
                    
                    if(maxDiscountAmount > 0){
                        
                        if(self.merchant.deliveryFee - maxDiscountAmount > 0){
                            firstString = [[NSNumber numberWithCurrency:self.merchant.deliveryFee - maxDiscountAmount] currencyString];
                        }
                        
                        self.deliveryLimitLabel.text = [NSString stringWithFormat:@"%@ - %@",
                                                            firstString,
                                                            [[NSNumber numberWithCurrency:self.merchant.deliveryFee] currencyString]];
                    } else {
                        self.deliveryLimitLabel.text = [[NSNumber numberWithCurrency:self.merchant.deliveryFee] currencyString];
                    }
                }
            }
                break;
            case DeliveryFeePlanRadius:
            {
                if([ORDER.merchant.deliveryFeeZone count] > 0){
                    DZoneRadius *firstRadius = [ORDER.merchant.deliveryFeeZone objectAtIndex:0];
                    DZoneRadius *lastRadius = [ORDER.merchant.deliveryFeeZone lastObject];
                    
                    if(firstRadius.feeAmount == lastRadius.feeAmount){
                        if(firstRadius.feeAmount == 0){
                            self.deliveryLimitLabel.text = NSLocalizedString(@"Free", nil);
                        } else {
                            
                            NSString *firstString = NSLocalizedString(@"Free", nil);
                            
                            if(maxDiscountAmount > 0){
                                if(firstRadius.feeAmount - maxDiscountAmount > 0){
                                    firstString = [[NSNumber numberWithCurrency:firstRadius.feeAmount - maxDiscountAmount] currencyString];
                                }
                                self.deliveryLimitLabel.text = [NSString stringWithFormat:@"%@ - %@",
                                                                    firstString,
                                                                    firstRadius.feeAmountString];
                            } else {
                                self.deliveryLimitLabel.text = firstRadius.feeAmountString;
                            }
                        }
                    } else {
                        NSString *firstString = NSLocalizedString(@"Free", nil);
                        NSString *lastString = NSLocalizedString(@"Free", nil);
                        
                        if(firstRadius.feeAmount - maxDiscountAmount > 0) {
                            firstString = [[NSNumber numberWithCurrency:firstRadius.feeAmount - maxDiscountAmount] currencyString];
                        }
                        if(lastRadius.feeAmount > 0) lastString = lastRadius.feeAmountString;
                        
                        self.deliveryLimitLabel.text = [NSString stringWithFormat:@"%@ - %@",
                                                            firstString,
                                                            lastString];
                    }
                } else {
                    self.deliveryLimitLabel.text = NSLocalizedString(@"Unknown Delivery Fee", nil);
                }
            }
                break;
            case DeliveryFeePlanZone:
            {
                PCCurrency smallestFee = 100000000;
                PCCurrency largestFee = -100000000;
                
                for(DZonePolygon *polygon in ORDER.merchant.deliveryFeeZone){
                    if(smallestFee > polygon.feeAmount){
                        smallestFee = polygon.feeAmount;
                    }
                    if(largestFee < polygon.feeAmount){
                        largestFee = polygon.feeAmount;
                    }
                }
                if(smallestFee == largestFee){
                    if(smallestFee == 0){
                        self.deliveryLimitLabel.text = NSLocalizedString(@"Free", nil);
                    } else {
                        
                        if(maxDiscountAmount > 0){
                            NSString *firstString = NSLocalizedString(@"Free", nil);
                            
                            if(smallestFee - maxDiscountAmount > 0){
                                firstString = [[NSNumber numberWithCurrency:smallestFee - maxDiscountAmount] currencyString];
                            }
                            self.deliveryLimitLabel.text = [NSString stringWithFormat:@"%@ - %@",
                                                                firstString,
                                                                [[NSNumber numberWithCurrency:smallestFee] currencyString]];
                        } else {
                            self.deliveryLimitLabel.text = [[NSNumber numberWithCurrency:smallestFee] currencyString];
                        }
                    }
                } else {
                    
                    NSString *firstString = NSLocalizedString(@"Free", nil);
                    
                    if(smallestFee - maxDiscountAmount > 0){
                        firstString = [[NSNumber numberWithCurrency:smallestFee - maxDiscountAmount] currencyString];
                    }
                    
                    self.deliveryLimitLabel.text = [NSString stringWithFormat:@"%@ - %@",
                                                    firstString,
                                                    [[NSNumber numberWithCurrency:largestFee] currencyString]];
                }
            }
                break;
            default:
                self.deliveryLimitLabel.text = NSLocalizedString(@"Unknown Delivery Plan", nil);
                break;
        }
        
        switch(merchant.deliveryFeePlan){
            case DeliveryFeePlanFlat:
            case DeliveryFeePlanRadius:
                    if(APP.location == nil){
                        self.distanceWarningImageView.hidden = YES;
                        self.distanceLabel.text = @"N/A";
                    } else {
                        self.distanceWarningImageView.hidden = (((merchant.distance * METERS_TO_FEET)/FEET_IN_MILES) <= merchant.deliveryDistanceLimit);
                        self.distanceLabel.text = merchant.distanceString;
                    }
                break;
            case DeliveryFeePlanZone:
                if(APP.location == nil){
                    self.distanceWarningImageView.hidden = YES;
                    self.distanceLabel.text = @"N/A";
                } else {
                    PCCurrency feeAmount = [merchant deliveryFeeAt:APP.location andSubtotal:NSNotFound];
                    
                    self.distanceWarningImageView.hidden = (feeAmount != NSNotFound);
                    
//                    if(self.distanceWarningImageView.hidden){
//                        self.distanceLabel.text = NSLocalizedString(@"In Deliverable Area", nil);
//                    } else {
//                        self.distanceLabel.text = NSLocalizedString(@"Not in Deliverable Area", nil);
//                    }
                    self.distanceLabel.text = merchant.distanceString;
                }

                break;
            default:
                self.distanceWarningImageView.hidden = NO;
                self.distanceLabel.text = NSLocalizedString(@"N/A", nil);
                break;
        }
        
        self.minimumOrderAmountLabel.text = [[NSNumber numberWithCurrency:merchant.deliveryMinOrderAmount] currencyString];
        if(merchant.averageDeliveryTime > 0){
            self.averageDeliveryTimeLabel.text = [NSString stringWithFormat:@"%ld min",(long)merchant.averageDeliveryTime];
            self.averageDeliveryTitleLabel.text = NSLocalizedString(@"Average Delivery Time", nil);
        } else {
            self.averageDeliveryTimeLabel.text = nil;
            self.averageDeliveryTitleLabel.text = nil;
        }
        
    } else {
        [self addDeliveryInfoContainer:NO];
    }

    if(self.merchant.hasRoServiceFee && showRoService){
//        [self addRoServiceInfoContainer:YES];
//        self.roServiceFeeLabel.text = [NSString stringWithFormat:@"%@ service fee at checkout",
//                                       [[NSNumber numberWithFloat:self.merchant.roServiceRate] percentString]];
    } else {
        
//        [self addRoServiceInfoContainer:NO];
    }
    
    NSDictionary *views = nil;
    
    NSArray *exConstraints = nil;
    
    if(self.addressLabel == nil){
        
        exConstraints = [self constraintsForAttribute:NSLayoutAttributeTop withSecondItem:self.restaurantNameLabel];
        [self removeConstraints:exConstraints];
        
        views = @{@"deliveryDistanceInfoContainer":self.deliveryDistanceInfoContainer,
                  @"promotionContainer":self.promotionContainer,
                  @"addressLabel":self.restaurantNameLabel};
    } else {
        exConstraints = [self constraintsForAttribute:NSLayoutAttributeTop
                                                withSecondItem:self.addressLabel];
        [self removeConstraints:exConstraints];
        
        views = @{@"deliveryDistanceInfoContainer":self.deliveryDistanceInfoContainer,
                  @"promotionContainer":self.promotionContainer,
                  @"addressLabel":self.addressLabel};
    }
    
    NSString *format = nil;
    
    exConstraints = [self constraintsForAttribute:NSLayoutAttributeTop
                                   withSecondItem:self.promotionContainer];
    [self removeConstraints:exConstraints];
    
    exConstraints = [self constraintsForAttribute:NSLayoutAttributeTop
                                   withSecondItem:self.deliveryDistanceInfoContainer];
    [self removeConstraints:exConstraints];
    
    // Remove Ro Service Container, it's replaced with Ro Service Button
    [self.roServiceContainer removeFromSuperview];
    
//    exConstraints = [self constraintsForAttribute:NSLayoutAttributeTop
//                                   withSecondItem:self.roServiceContainer];
//    [self removeConstraints:exConstraints];
    
    // Remove All bttom constraints
    exConstraints = [self constraintsForAttribute:NSLayoutAttributeBottom
                                   withSecondItem:self.roServiceContainer];

    [self removeConstraints:exConstraints];
    
    exConstraints = [self constraintsForAttribute:NSLayoutAttributeBottom
                                   withSecondItem:self.deliveryDistanceInfoContainer];

    [self removeConstraints:exConstraints];
    
    exConstraints = [self constraintsForAttribute:NSLayoutAttributeBottom
                                   withSecondItem:self.promotionContainer];

    [self removeConstraints:exConstraints];
    
    if(self.addressLabel != nil){
        exConstraints = [self constraintsForAttribute:NSLayoutAttributeBottom
                                       withSecondItem:self.addressLabel];

        [self removeConstraints:exConstraints];
    }
    
    exConstraints = [self constraintsForAttribute:NSLayoutAttributeBottom
                                   withSecondItem:self.restaurantNameLabel];

    [self removeConstraints:exConstraints];
    //////////////////////////////////////////
    
    if(!self.deliveryDistanceInfoContainer.hidden
       && !self.promotionContainer.hidden){
        format = @"V:[addressLabel]-7-[promotionContainer]-7-[deliveryDistanceInfoContainer]-10-|";
//    } else if(!self.deliveryDistanceInfoContainer.hidden && !self.promotionContainer.hidden){
//        format = @"V:[addressLabel]-7-[deliveryDistanceInfoContainer]-7-[promotionContainer]-10-|";
//    } else if(!self.promotionContainer.hidden && !self.roServiceContainer.hidden){
//        format = @"V:[addressLabel]-7-[promotionContainer]-7-[roServiceContainer]-10-|";
//    } else if(!self.deliveryDistanceInfoContainer.hidden && !self.roServiceContainer.hidden){
//        format = @"V:[addressLabel]-7-[deliveryDistanceInfoContainer]-7-[roServiceContainer]-10-|";
    } else if(!self.deliveryDistanceInfoContainer.hidden){
        format = @"V:[addressLabel]-7-[deliveryDistanceInfoContainer]-10-|";
    } else if(!self.promotionContainer.hidden){
        format = @"V:[addressLabel]-7-[promotionContainer]-10-|";
//    } else if(!self.roServiceContainer.hidden){
//        format = @"V:[addressLabel]-7-[roServiceContainer]-10-|";
    } else {
        format = @"V:[addressLabel]-(>=10)-|";
    }
    
    if(format != nil){
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:format
                                                                       options:0
                                                                       metrics:nil
                                                                         views:views];
        [self addConstraints:constraints];
    } else {
        
    }
    
    self.moreDeliveryFeeButton.hidden = ([ORDER.merchant.deliveryFeeDiscount count] == 0);
    
    [self updateConstraints];
    
    if(forceHeight){
        self.height = [self systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    }
}

- (void)addPromotionContainer:(BOOL)add
{
    self.promotionContainer.hidden = !add;
    if(add){
        if([self.promotionContainer superview] == nil){
            [self addSubview:self.promotionContainer];
            
            NSDictionary *views = @{@"promotionContainer":self.promotionContainer};
            
            NSString *format = @"|-75-[promotionContainer]-0-|";
            
            NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:format
                                                                           options:0
                                                                           metrics:nil
                                                                             views:views];
            [self addConstraints:constraints];
        }
    } else {
        [self.promotionContainer removeFromSuperview];
    }
}

- (void)addDeliveryInfoContainer:(BOOL)add
{
    self.deliveryDistanceInfoContainer.hidden = !add;
    if(add){
        if([self.deliveryDistanceInfoContainer superview] == nil){
            [self addSubview:self.deliveryDistanceInfoContainer];
            
            NSDictionary *views = @{@"deliveryDistanceInfoContainer":self.deliveryDistanceInfoContainer};

            NSString *format = @"|-75-[deliveryDistanceInfoContainer]-0-|";
            
            
            NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:format
                                                                           options:0
                                                                           metrics:nil
                                                                             views:views];
            [self addConstraints:constraints];

        }
    } else {
        [self.deliveryDistanceInfoContainer removeFromSuperview];
    }
}

- (void)addRoServiceInfoContainer:(BOOL)add
{
    self.roServiceContainer.hidden = !add;
    if(add){
        if([self.roServiceContainer superview] == nil){
            [self addSubview:self.roServiceContainer];
            
            NSDictionary *views = @{@"roServiceContainer":self.roServiceContainer};
            
            NSString *format = @"|-75-[roServiceContainer]-0-|";
            
            
            NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:format
                                                                           options:0
                                                                           metrics:nil
                                                                             views:views];
            [self addConstraints:constraints];
            
        }
    } else {
        [self.roServiceContainer removeFromSuperview];
    }
}

- (void)turnTestFlagOn:(BOOL)on
{
    self.panelImageView.hidden = !on;
    self.panelLabel.hidden = !on;
    self.panelLabel.text = NSLocalizedString(@"TEST", nil);
    self.panelHeightConstraint.constant = on ? 39.0f : 0.0f;
}

- (void)turnDemoFlagOn:(BOOL)on
{
    self.panelImageView.hidden = !on;
    self.panelLabel.hidden = !on;
    self.panelLabel.text = NSLocalizedString(@"DEMO", nil);
    self.panelHeightConstraint.constant = on ? 39.0f : 0.0f;
}

- (void)turnCloseFlagOn:(BOOL)on
{
    
    self.panelImageView.hidden = !on;
    self.panelLabel.hidden = !on;
    self.panelLabel.text = NSLocalizedString(@"Closed", nil);
    self.panelHeightConstraint.constant = on ? 39.0f : 0.0f;
}

- (void)circleImageViewActionTriggerred:(id)sender
{
//    [ORDER resetGraphWithMerchant:self.merchant];
    MerchantViewController *viewController = [UIStoryboard viewController:@"MerchantViewController"
                                                                     from:@"Restaurants"];
    [APP.tabBarController presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^{

                                 }];
}

- (IBAction)deliveryFeeInfoButtonTouched:(id)sender
{
    PCCurrency deliveryFeeBasedOnLocation = [ORDER.merchant deliveryFeeAt:APP.location];
    if(deliveryFeeBasedOnLocation == NSNotFound){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Outside Deliverable Area", nil)
                            message:NSLocalizedString(@"You are currently outside the restaurant's delivery distance limit.", nil)];
        return;
    }
    
    DeliveryFeeViewController *deliveryFeeViewController = [[DeliveryFeeViewController alloc] initWithNibName:@"DeliveryFeeViewController"
                                                                                                       bundle:nil
                                                                                          deliveryFeeDiscount:ORDER.merchant.deliveryFeeDiscount
                                                                                              baseDeliveryFee:deliveryFeeBasedOnLocation];
    
    PopupDialog *popup = [[PopupDialog alloc] initWithViewController:deliveryFeeViewController
                                                     buttonAlignment:UILayoutConstraintAxisHorizontal
                                                     transitionStyle:PopupDialogTransitionStyleBounceUp
                                                    gestureDismissal:YES
                                                          completion:^{
                                                              PCLog(@"Completed!!!! *************************");
                                                          }];
    
    CancelButton *ok = [[CancelButton alloc] initWithTitle:NSLocalizedString(@"OK", nil)
                                                    height:44.f
                                              dismissOnTap:YES
                                                    action:^{
                                                        
                                                    }];
    ok.titleColor = [UIColor colorWithR:66. G:66. B:66.];
    [popup addButtons: @[ok]];
    [APP.tabBarController presentViewController:popup animated:YES completion:nil];
    
}

- (void)touchableLabelActionTriggerred:(id)sender
{
    [self circleImageViewActionTriggerred:sender];
}

- (IBAction)showServiceFeeGuide:(id)sender
{
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    
    ServiceFeeGuideView *view = [ServiceFeeGuideView view];
    view.guideLabel.text = [NSString stringWithFormat:NSLocalizedString(@"To offer delivery from places that don't normally deliver, we sometimes partner with 3rd parties who charge an added Service Fee. We're working hard to get these fees down, so just hang on tight!", nil),
                            [[NSNumber numberWithFloat:self.merchant.roServiceRate] percentString]];
    [alertView setContainerView:view];
    alertView.buttonTitles = [NSArray arrayWithObject:NSLocalizedString(@"Got it!", nil)];
    [alertView show];
    
    [view startAnimation];
}
@end
