//
//  ServiceFeeGuideView.m
//  RushOrder
//
//  Created by Conan on 11/11/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "ServiceFeeGuideView.h"

#define degreesToRadians(x) (M_PI * x / 180.0)
#define P(x,y) CGPointMake(x, y)

#define START_X             10.0f
#define START_Y             25.0f
#define HALF_WAVE_LENGTH    43.0f
#define AMPLITUDE           17.0f
#define POINT_OFFSET        20.0f
#define COUNT 6

@interface ServiceFeeGuideView()

@property (weak, nonatomic) IBOutlet UIImageView *carIcon;
@end

@implementation ServiceFeeGuideView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)startAnimation
{
    UIBezierPath *trackPath = [UIBezierPath bezierPath];
    
    CGFloat curX = START_X;
    CGFloat curY = START_Y;
    
    [trackPath moveToPoint:CGPointMake(curX, curY)];
    
    NSInteger updown = -1;
    
    int signer = 1;
    
    CGFloat newPointX = 0.0f;
    CGFloat newPointY = 0.0f;
    
    for(int i = 0; i < COUNT; i++){
        CGFloat startHalf = ((i == 0 || COUNT == i) ? 0.5 : 1);
        newPointX = curX + HALF_WAVE_LENGTH * startHalf;
        CGFloat amplitude = updown * AMPLITUDE * startHalf;
        
        newPointY = curY + amplitude;
        
        [trackPath addCurveToPoint:P(newPointX, newPointY)
                     controlPoint1:P(curX + (POINT_OFFSET * ((i == 0) ? 0.5 : 1)) , curY)
                     controlPoint2:P(newPointX - POINT_OFFSET, newPointY)];
        
        curX = newPointX;
        curY = newPointY;

        switch(i % 2)
        {
            case 0:
                updown = 1;
                break;
            case 1:
                updown = -1;
                break;
        }
        
        signer = signer * -1;
    }
    
//    CAShapeLayer *racetrack = [CAShapeLayer layer];
//    racetrack.path = trackPath.CGPath;
//    racetrack.strokeColor = [UIColor blackColor].CGColor;
//    racetrack.fillColor = [UIColor clearColor].CGColor;
//    racetrack.lineWidth = 1.0;
//    [self.layer addSublayer:racetrack];
//
//    CAShapeLayer *centerline = [CAShapeLayer layer];
//    centerline.path = trackPath.CGPath;
//    centerline.strokeColor = [UIColor whiteColor].CGColor;
//    centerline.fillColor = [UIColor clearColor].CGColor;
//    centerline.lineWidth = 2.0;
//    centerline.lineDashPattern = [NSArray arrayWithObjects:
//                                  [NSNumber numberWithInt:6],
//                                  [NSNumber numberWithInt:2],
//                                  nil];
//    [self.layer addSublayer:centerline];
    
    CALayer *car = [CALayer layer];
    car.bounds = CGRectMake(newPointX, newPointY, 42.0, 23.0);
    car.position = CGPointMake(newPointX, newPointY);
    car.contents = (id)([UIImage imageNamed:@"icon_truck_animation"].CGImage);
    [self.layer addSublayer:car];
    
    CAKeyframeAnimation *anim = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    anim.path = trackPath.CGPath;
    anim.rotationMode = kCAAnimationRotateAuto;
    anim.repeatCount = 1;
    anim.duration = 5.0;
    [car addAnimation:anim forKey:@"race"];
}


@end