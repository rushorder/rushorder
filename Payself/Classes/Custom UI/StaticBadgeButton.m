//
//  StaticBadgeButton.m
//  RushOrder
//
//  Created by Conan on 11/10/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "StaticBadgeButton.h"

@interface StaticBadgeButton()
@property (strong, nonatomic) UIImageView *badgeImage;
@end

@implementation StaticBadgeButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setBadgeOn:(BOOL)value
{
    _badgeOn = value;
    
    self.badgeImage.hidden = !_badgeOn;
}


- (UIImageView *)badgeImage
{
    if(_badgeImage == nil){
        _badgeImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"filter_badge_on"]];
        CGRect frame = _badgeImage.frame;
        frame.origin.x = -10.0f;
        frame.origin.y = -4.0f;
        _badgeImage.frame = frame;
        [self addSubview:_badgeImage];
    }
    
    return _badgeImage;
}

@end
