//
//  CircleImageView.h
//  RushOrder
//
//  Created by Conan on 8/12/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CircleImageView : UIImageView

@property (nonatomic, getter = isBorder) BOOL border;
@end
