//
//  RestaurantAnnotationView.m
//  RushOrder
//
//  Created by Conan on 12/2/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "RestaurantAnnotationView.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define CENTER_FOR_X    23.0f // 핀 이미지 꼭지점의 X좌표
#define CENTER_FOR_Y    50.0f // 핀 이미지 손잡이의 센터 Y좌표 + 핀 이미지 자체의 Y좌표

@interface RestaurantAnnotationView()
@property (strong, nonatomic) UIImageView *pinImageView;
@property (strong, nonatomic) UIImageView *selectedImageView;
@property (strong, nonatomic) UIImageView *thumbnailImageView;
@property (strong, nonatomic) UIImageView *demoFlagImageView;
@end

@implementation RestaurantAnnotationView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self != nil){
        [self commonInit];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [self drawRestaurant];
}

- (void)commonInit
{
    [self addSubview:self.thumbnailImageView];
    [self addSubview:self.pinImageView];
    [self addSubview:self.selectedImageView];
    [self addSubview:self.demoFlagImageView];
//    Debuging center code
//    UIView *spotView = [[UIView alloc] initWithFrame:CGRectMake(-1.0f,
//                                                                -1.0f,
//                                                                2.0f,
//                                                                2.0f)];
//    spotView.backgroundColor = [UIColor blackColor];
//    [self addSubview:spotView];
    
    self.centerOffset = CGPointMake(0.0f, -30.0f); // 핀 꼭지로 부터 손잡이 중심까지 높이 = y
    
    self.backgroundColor = [UIColor clearColor];
}

- (void)drawRestaurant
{
    if(self.merchant.isOpenHour){
        if(self.merchant.isAbleTakeoutAndDelivery){
            self.pinImageView.image = [UIImage imageNamed:@"pin_open_takeout_delivery"];
        } else if(self.merchant.isAbleTakeout){
            self.pinImageView.image = [UIImage imageNamed:@"pin_open_takeout"];
        } else if(self.merchant.isAbleDelivery){
            self.pinImageView.image = [UIImage imageNamed:@"pin_open_delivery"];
        } else {
            self.pinImageView.image = [UIImage imageNamed:@"pin_open"];
        }
    } else {
        if(self.merchant.isAbleTakeoutAndDelivery){
            self.pinImageView.image = [UIImage imageNamed:@"pin_close_takeout_delivery"];
        } else if(self.merchant.isAbleTakeout){
            self.pinImageView.image = [UIImage imageNamed:@"pin_close_takeout"];
        } else if(self.merchant.isAbleDelivery){
            self.pinImageView.image = [UIImage imageNamed:@"pin_close_delivery"];
        } else {
            self.pinImageView.image = [UIImage imageNamed:@"pin_close"];
        }
    }
    
    NSURL *imgURL = self.merchant.logoURL;
    if(imgURL == nil) imgURL = self.merchant.imageURL;
    
    if(imgURL != nil){
        [self.thumbnailImageView sd_setImageWithURL:imgURL
                                   placeholderImage:[UIImage imageNamed:@"placeholder_logo"]];
    } else {
        self.thumbnailImageView.image = [UIImage imageNamed:@"placeholder_logo"];
    }
    
    self.demoFlagImageView.hidden = !self.merchant.isDemoMode;

    self.selectedImageView.hidden = !self.isSelected;
}

- (UIImageView *)selectedImageView
{
    if(_selectedImageView == nil){
        _selectedImageView = [[UIImageView alloc] initWithFrame:CGRectMake(11.0f - CENTER_FOR_X,
                                                                           0.0f - CENTER_FOR_Y,
                                                                           24.0f,
                                                                           24.0f)];
        _selectedImageView.image = [UIImage imageNamed:@"arrow_select_restaurant"];
    }
    return _selectedImageView;
}

- (UIImageView *)pinImageView
{
    if(_pinImageView == nil){
        _pinImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f - CENTER_FOR_X,
                                                                      27.0f - CENTER_FOR_Y,
                                                                      66.0f,
                                                                      57.0f)];
        _pinImageView.contentMode = UIViewContentModeTopLeft;
        
//        [_pinImageView drawBorder];
    }
    return _pinImageView;
}

- (UIImageView *)thumbnailImageView
{
    if(_thumbnailImageView == nil){
        _thumbnailImageView = [[UIImageView alloc] initWithFrame:CGRectMake(7.0f - CENTER_FOR_X,
                                                                           34.0f - CENTER_FOR_Y,
                                                                           32.0f,
                                                                           32.0f)];
        _thumbnailImageView.contentMode = UIViewContentModeScaleAspectFill;
        _thumbnailImageView.clipsToBounds = YES;
        _thumbnailImageView.backgroundColor = [UIColor whiteColor];
    }
    return _thumbnailImageView;
}

- (UIImageView *)demoFlagImageView
{
    if(_demoFlagImageView == nil){
        _demoFlagImageView = [[UIImageView alloc] initWithFrame:CGRectMake(-2.0f - CENTER_FOR_X,
                                                                            34.0f- CENTER_FOR_Y,
                                                                            56.0f,
                                                                            40.0f)];
        _demoFlagImageView.contentMode = UIViewContentModeScaleAspectFill;
        _demoFlagImageView.clipsToBounds = YES;
        _demoFlagImageView.backgroundColor = [UIColor clearColor];
        _demoFlagImageView.image = [UIImage imageNamed:@"artwork_demo_ribbon"];
    }
    return _demoFlagImageView;
}

@end
