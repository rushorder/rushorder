//
//  PCCardNumberTextField.m
//  RushOrder
//
//  Created by Conan on 2/27/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCCardNumberTextField.h"


@interface PCCardNumberTextField()
{
    int _textFieldSemaphore;
}

@property (strong, nonatomic) NSCharacterSet *nonNumberSet;
@property (strong, nonatomic) UIColor *defaultTextColor;
@end

@implementation PCCardNumberTextField


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self != nil){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    // Initialization code
    self.keyboardType = UIKeyboardTypeNumberPad;
        
    NSMutableCharacterSet *numberSet = [[NSCharacterSet decimalDigitCharacterSet] mutableCopy];
    [numberSet formUnionWithCharacterSet:[NSCharacterSet whitespaceCharacterSet]];
    self.nonNumberSet = [numberSet invertedSet];
    
    [self addTarget:self
             action:@selector(valueChanged:)
   forControlEvents:UIControlEventValueChanged];
    
    [self addTarget:self
             action:@selector(valueChanged:)
   forControlEvents:UIControlEventEditingChanged];
    
    self.issuer = IssuerUnknown;
    
    self.delegate = FORMATTER.cardNumberFormatter;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.defaultTextColor = self.textColor;
    UIImage *image = [self.background resizableImageFromCenter];
    self.background = image;
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    CGFloat sideMargin = 8.0f;
    bounds.size.width -= sideMargin * 2;
    bounds.origin.x += sideMargin;
    
    return bounds;
}


- (void)setEnabled:(BOOL)value
{
    if(value){
        self.textColor = self.defaultTextColor;
    } else {
        self.textColor = [UIColor lightGrayColor];
    }
    
    [super setEnabled:value];
}

- (void)valueChanged:(id)sender
{
    if(_textFieldSemaphore) return;
    _textFieldSemaphore = 1;
    
    self.issuer = [FORMATTER.cardNumberFormatter acquireIssuer:self.text];
    self.text = [FORMATTER.cardNumberFormatter format:self.text
                                           withIssuer:self.issuer];
    
    _textFieldSemaphore = 0;
}

- (void)setIssuer:(Issuer)val
{
    if(_issuer != val){
        if([self.forwardDelegate
            respondsToSelector:@selector(cardNumberTextField:didChangeIssuer:issuerString:)]){
            
            NSString *issuerName = [NSString issuerName:val];
                        
            [self.forwardDelegate cardNumberTextField:self
                                      didChangeIssuer:val
                                         issuerString:issuerName];
        }
    }
    _issuer = val;
}

- (NSString *)cardNumber
{
    NSString *strippedCardNumber = [self.text stringByReplacingOccurrencesOfString:@"-"
                                                                        withString:@""];
    
    return strippedCardNumber;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if(self.underLine){
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetStrokeColorWithColor(context, [UIColor colorWithR:204.0f G:204.0f B:204.0f].CGColor);
        
        // Draw them with a 2.0 stroke width so they are a bit more visible.
        CGContextSetLineWidth(context, 0.5f);
        
        CGContextMoveToPoint(context, 0,rect.size.height - 1.0); //start at this point
        
        CGContextAddLineToPoint(context, rect.size.width, rect.size.height - 1.0); //draw to this point
        
        // and now draw the Path!
        CGContextStrokePath(context);
    }
}


@end
