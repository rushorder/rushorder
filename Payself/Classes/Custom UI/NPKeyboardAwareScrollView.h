//
//  NPKeyboardAwareScrollView.h
//  RushOrder
//
//  Created by Conan on 11/23/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface NPKeyboardAwareScrollView : UIScrollView

@property (nonatomic) CGFloat adjustingOffsetY;

- (void)setContentSizeWithBottomViewNoMargin:(UIView *)bottomView;
- (void)setContentSizeWithBottomView:(UIView *)bottomView;
- (void)setContentSizeWithBottomView:(UIView *)bottomView
                        bottomMargin:(CGFloat)bottomMargin;

@end
