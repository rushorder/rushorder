//
//  PCDynamicRowGridView.h
//  RushOrder
//
//  Created by Conan on 3/6/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostitButton.h"

@class PCDynamicRowGridView;

@protocol PCDynamicRowGridViewDataSource <NSObject>
@required
- (NSInteger)numberOfItemsInGridView:(PCDynamicRowGridView *)gridView;
- (UIView *)gridView:(PCDynamicRowGridView *)gridView cellForItemIndex:(NSInteger)index;
- (void)gridView:(PCDynamicRowGridView *)gridView updateCell:(UIView *)cell atIndex:(NSInteger)index;
@end

@protocol PCDynamicRowGridViewDelegate <UIScrollViewDelegate>
@end


//==============================================================================
@interface PCDynamicRowGridView : UIScrollView

@property (nonatomic) UIEdgeInsets outerMargin;
@property (nonatomic) CGFloat itemSpan;
@property (nonatomic) CGFloat tileWidth;

@property (weak, nonatomic) id < PCDynamicRowGridViewDataSource> dataSrouce;
@property (weak, nonatomic) id < PCDynamicRowGridViewDelegate> delegate;

- (void)reloadData;
- (CGRect)frameAtIndex:(NSInteger)index;
- (PostitButton *)buttonAtIndex:(NSInteger)index;
- (void)shiftLeftFromIndex:(NSInteger)index;
- (void)shiftRightFromIndex:(NSInteger)index;
@end


