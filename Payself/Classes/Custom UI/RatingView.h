//
//  RatingView.h
//  RushOrder
//
//  Created by Conan Kim on 12/3/15.
//  Copyright © 2015 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "BTRatingView.h"

@interface RatingView : UIView

@property (nonatomic) NSInteger selectedRating;

@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet NPToggleButton *badButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *neutralButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *goodButton;


@end



@interface FeedbackView : UIView

@property (weak, nonatomic) IBOutlet UILabel *feedbackLabel;
@property (weak, nonatomic) IBOutlet UITextView *feedbackTextView;
@end
