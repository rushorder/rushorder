//
//  BadgeBarButtonItem.m
//  RushOrder
//
//  Created by Conan on 11/10/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "BadgeBarButtonItem.h"

@interface BadgeBarButtonItem()
@property (strong, nonatomic) UIImageView *badgeImage;
@end

@implementation BadgeBarButtonItem

- (void)setBadgeOn:(BOOL)value
{
    _badgeOn = value;
    
    self.badgeImage.hidden = !_badgeOn;
}


- (UIImageView *)badgeImage
{
    if(_badgeImage == nil){
        _badgeImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"filter_badge_on"]];
    }
    
    
    return _badgeImage;
}



@end