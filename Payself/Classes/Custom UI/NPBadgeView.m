//
//  NPBadgeView.m
//  RushOrder
//
//  Created by Conan on 1/11/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "NPBadgeView.h"

@interface NPBadgeView()

@property (strong, nonatomic) UIImageView *backgroundImageView;
@end

@implementation NPBadgeView

- (id)initWithFrame:(CGRect)frame
{
    CGRect minimumFrame = frame;
    
    minimumFrame.size.width = MAX(minimumFrame.size.width, MIN_BADGE_SIZE);
    minimumFrame.size.height = MAX(minimumFrame.size.height, MIN_BADGE_SIZE);
    
    self = [super initWithFrame:minimumFrame];
    
    if(self){
        self.backgroundColor = [UIColor clearColor];
        
        UIImage *stretchableBackImage = [[UIImage imageNamed:@"round_count_menu"] resizableImageFromCenter];
        self.backgroundImageView = [[UIImageView alloc] initWithImage:stretchableBackImage];
        self.backgroundImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:self.backgroundImageView];
        
        CGRect frameBounds = self.bounds;
        
        self.countButton = [[UIButton alloc] initWithFrame:frameBounds];
        self.countButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [self.countButton setTitleColor:[UIColor whiteColor]
                               forState:UIControlStateNormal];
        self.countButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0f];
        
        self.countButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.countButton.backgroundColor = [UIColor clearColor];
        self.countButton.adjustsImageWhenHighlighted = YES;
        self.countButton.contentEdgeInsets = UIEdgeInsetsMake(0.0f,
                                                              0.0f,
                                                              0.0f,
                                                              0.0f);
        
        [self.countButton addTarget:self.target
                             action:self.action
                   forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:self.countButton];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)setBadgeValue:(NSInteger)value
{
    _badgeValue = value;
    self.badgeStringValue = [NSString stringWithInteger:_badgeValue];
}

- (void)setBadgeStringValue:(NSString *)value
{
    _badgeStringValue = value;
    
    self.countButton.buttonTitle = value;
    CGSize countSize = [self.countButton.buttonTitle sizeWithFontOrAttributes:self.countButton.titleLabel.font];
    CGRect selfFrame = self.frame;
    selfFrame.size.width = MAX(countSize.width + 14.0f, MIN_BADGE_SIZE);
    selfFrame.origin.x = -(selfFrame.size.width - 13.0f);
    self.frame = selfFrame;
}

- (void)setTarget:(id)target
{
    if(self.action != nil){
        [self.countButton addTarget:target
                             action:self.action
                   forControlEvents:UIControlEventTouchUpInside];
    }
    
    _target = target;
}

- (void)setAction:(SEL)action
{
    if(self.target != nil){
        [self.countButton addTarget:self.target
                             action:action
                   forControlEvents:UIControlEventTouchUpInside];
    }
    
    _action = action;
}

@end
