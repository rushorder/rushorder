//
//  SingleScrollPageViewController.h
//  RushOrder
//
//  Created by Conan Kim on 7/20/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleScrollPageViewController : UIPageViewController

@end
