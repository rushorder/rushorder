//
//  MenuHeaderDetailView.m
//  RushOrder
//
//  Created by Conan Kim on 4/20/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

#import "MenuHeaderDetailView.h"

@implementation MenuHeaderDetailView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.multilineLabel.preferredMaxLayoutWidth = self.multilineLabel.bounds.size.width;
}

@end
