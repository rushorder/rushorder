//
//  NPProgressView.m
//  RushOrder
//
//  Created by Conan Kim on 11. 10. 5..
//  Copyright 2013 RushOrder. All rights reserved.
//

#import "NPProgressView.h"

#define PROGRESSBAR_ANI_DURATION    0.001

@interface NPProgressView()
//- (void)progressGauge:(NSNumber *)withAnimation;
@end

@implementation NPProgressView

//@synthesize progress;

- (id)initWithFrame:(CGRect)frame
{
    return [self initWithFrame:frame withStyle:NPProgressViewStyleLarge];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];

    if(self != nil){
        CGRect frame = self.frame;
        frame.size.height = MAX(self.frame.size.height, 10.0f);
        self.frame = frame;
        [self commonInitWithFrame:frame
                        withStyle:NPProgressViewStyleLarge];
    }

    return self;
}


- (id)initWithFrame:(CGRect)frame withStyle:(NPProgressViewStyle)progressViewStyle
{
    frame.size.height = MAX(self.frame.size.height, 10.0f);
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInitWithFrame:frame withStyle:progressViewStyle];
    }
    return self;
}

- (void)commonInitWithFrame:(CGRect)frame withStyle:(NPProgressViewStyle)progressViewStyle
{
    UIImage *backImage = nil;
    
    switch(progressViewStyle){
        case NPProgressViewStyleSmall:
            backImage = [UIImage imageNamed:@"gauge_bg_10_10"];
            break;
        case NPProgressViewStyleLarge:
        default:
            backImage = [UIImage imageNamed:@"gauge_bg_10_10"];
            break;
    }
    
#define _CEN 4.9f
    if ([backImage respondsToSelector:@selector(resizableImageWithCapInsets:)]){
        gaugeBackImage = [backImage
                          resizableImageWithCapInsets:UIEdgeInsetsMake(_CEN ,_CEN, _CEN, _CEN)];
    } else{
        gaugeBackImage = [backImage stretchableImageWithLeftCapWidth:_CEN
                                                        topCapHeight:_CEN];
    }
    
    
    UIImage *anImage = nil;
    
    anImage = [UIImage imageNamed:@"gauge_bar_6_6"];
    
#define _CEN_P 3.0f
    if ([gaugeImage respondsToSelector:@selector(resizableImageWithCapInsets:)]){
        gaugeImage = [anImage
                      resizableImageWithCapInsets:UIEdgeInsetsMake(_CEN_P ,_CEN_P, _CEN_P, _CEN_P)];
    } else{
        gaugeImage = [anImage stretchableImageWithLeftCapWidth:_CEN_P
                                                  topCapHeight:_CEN_P];
    }
    
    validWidth = frame.size.width - (gaugeBackImage.size.width - anImage.size.width) - anImage.size.width;

}


- (void)resetProgress:(CGFloat)val
{
    [super setProgress:val];
}

- (void)drawRect:(CGRect)rect
{
    [gaugeBackImage drawInRect:CGRectMake(0
                                          ,0
                                          ,rect.size.width
                                          ,rect.size.height)];
    
    
    
    CGFloat progressWidth = ceilf(validWidth * [self progress]) + gaugeImage.size.width;

    
    [gaugeImage drawInRect:CGRectMake(2
                                      ,2
                                      , progressWidth
                                      , gaugeImage.size.height)];
}

@end
