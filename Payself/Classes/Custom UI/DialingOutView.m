//
//  DialingOutView.m
//  RushOrder
//
//  Created by Conan Kim on 4/10/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import "DialingOutView.h"

@implementation DialingOutView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (IBAction)roPhoneCallButtonTouched:(id)sender
{
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://424-488-3170"]]){
        if(![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://424-488-3170"]]){
            //            User's cancel
        }
    } else {
        [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to Call",nil)
                            message:NSLocalizedString(@"Try to call manually", nil)];
    }
}

- (IBAction)merchantPhoneCallButtonTouched:(id)sender
{
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[@"tel://" stringByAppendingString:self.merchantPhoneNumber]]]){
        if(![[UIApplication sharedApplication] openURL:[NSURL URLWithString:[@"tel://" stringByAppendingString:self.merchantPhoneNumber]]]){
            //            User's cancel
        }
    } else {
        [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to Call",nil)
                            message:NSLocalizedString(@"Try to call manually or contact us", nil)];
    }
}

@end
