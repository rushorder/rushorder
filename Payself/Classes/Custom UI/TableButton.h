//
//  TableButton.h
//  RushOrder
//
//  Created by Conan on 2/15/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableButton : UIButton

@property (strong, nonatomic) UILabel *tableNumberLabel;
@property (nonatomic) BOOL enLarged;
@property (copy, nonatomic) NSString *tableNumber;

@property (weak, nonatomic) id swipeTarget;
@property (nonatomic) SEL swipeSelector;

@end