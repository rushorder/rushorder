//
//  DoubleImageButton.m
//  RushOrder
//
//  Created by Conan on 9/4/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "DoubleImageButton.h"

@interface DoubleImageButton()
@property (strong, nonatomic) UIImageView *subImageView;
@end

@implementation DoubleImageButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self){
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit
{
    [self addSubImageView];
}

- (void)setSubImage:(UIImage *)subImage
{
    _subImage = subImage;
    self.subImageView.image = subImage;
}

#define IMAGE_SIZE_FOR_IMAGE    17.0f

- (void)addSubImageView
{
    if(self.subImageView == nil){

        self.subImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f,0.0f,IMAGE_SIZE_FOR_IMAGE,IMAGE_SIZE_FOR_IMAGE)];
        self.subImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.subImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.subImageView.image = [UIImage imageNamed:@"icon_sf"];
        
        [self addSubview:self.subImageView];
        
        NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self.subImageView
                                                  attribute:NSLayoutAttributeHeight
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:nil
                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                 multiplier:1
                                                   constant:IMAGE_SIZE_FOR_IMAGE];
        [self.subImageView addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:self.subImageView
                                                  attribute:NSLayoutAttributeWidth
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:nil
                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                 multiplier:1
                                                   constant:IMAGE_SIZE_FOR_IMAGE];
        [self.subImageView addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:self.imageView
                                                  attribute:NSLayoutAttributeCenterY
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self.subImageView
                                                  attribute:NSLayoutAttributeCenterY
                                                 multiplier:1.0f
                                                   constant:-1.0f];
        constraint.priority = UILayoutPriorityFittingSizeLevel;
        [self addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:self.subImageView
                                                  attribute:NSLayoutAttributeTrailing
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self.imageView
                                                  attribute:NSLayoutAttributeLeading
                                                 multiplier:1.0f
                                                   constant:91.0f];
        constraint.priority = UILayoutPriorityFittingSizeLevel;
        [self addConstraint:constraint];
    }
    
//    [self.titleLabel drawBorder];
//    [self.subImageView drawBorder];
    
    self.subImageView.hidden = !self.isSubImageOn;
}


//- (void)drawRect:(CGRect)rect
//{
//    [super drawRect:rect];
//    
//    if(self.isSubImageOn){
//        if(self.subImageView == nil){
//            [self addSubImageView];
//        }
//        CGRect titleRect = [self titleRectForContentRect:rect];
//        
//        CGRect subImageViewRect = self.subImageView.frame;
//        subImageViewRect.origin.x = CGRectGetMaxX(titleRect) + 5.0f;
//        subImageViewRect.origin.y = (self.height - subImageViewRect.size.width) / 2.0f;
//        self.subImageView.frame = subImageViewRect;
//        self.subImageView.hidden = NO;
//    } else {
//        self.subImageView.hidden = YES;
//    }
//}

//- (void)layoutSubviews
//{
//    [super layoutSubviews];
//    if(self.isSubImageOn){
//        if(self.subImageView == nil){
//            [self addSubImageView];
//        }
//        CGRect titleRect = [self titleRectForContentRect:self.frame];
//        
//        CGRect subImageViewRect = self.subImageView.frame;
//        subImageViewRect.origin.x = CGRectGetMaxX(titleRect) + 0.0f;
//        subImageViewRect.origin.y = (self.height - subImageViewRect.size.width) / 2.0f;
//        self.subImageView.frame = subImageViewRect;
//        self.subImageView.hidden = NO;
//    } else {
//        self.subImageView.hidden = YES;
//    }
//}

- (void)setSubImageOn:(BOOL)subImageOn
{
    _subImageOn = subImageOn;
    self.subImageView.hidden = !self.isSubImageOn;
    self.contentEdgeInsets = UIEdgeInsetsMake(0.0f,
                                              _subImageOn ? -20.0f : -8.0f,
                                              0.0f,
                                              0.0f);
}


@end
