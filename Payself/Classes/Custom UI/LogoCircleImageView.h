//
//  LogoCircleImageView.h
//  RushOrder
//
//  Created by Conan on 10/30/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "CircleImageView.h"

@protocol LogoCircleImageViewAction;

@interface LogoCircleImageView : CircleImageView
@property (nonatomic, weak) id<LogoCircleImageViewAction> target;
@end


@protocol LogoCircleImageViewAction <NSObject>
- (void)circleImageViewActionTriggerred:(id)sender;
@end