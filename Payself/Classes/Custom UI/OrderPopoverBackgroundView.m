//
//  OrderPopoverBackgroundView.m
//  RushOrder
//
//  Created by Conan on 2/18/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import "OrderPopoverBackgroundView.h"


#define CONTENT_OUTSET   5.0f
#define CAP_INSET       25.0f
#define ARROW_BASE      16.0f
#define ARROW_HEIGHT    14.0f

@implementation OrderPopoverBackgroundView

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        _borderImageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"popover_bg"]
                                                               resizableImageWithCapInsets:UIEdgeInsetsMake(CAP_INSET,CAP_INSET,CAP_INSET,CAP_INSET)]];
        
        _arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow"]];
        
        [self addSubview:_borderImageView];
        [self addSubview:_arrowView];
        
        _originalSize = frame.size;
    }
    return self;
}

- (CGFloat) arrowOffset {
    return _arrowOffset;
}

- (void) setArrowOffset:(CGFloat)arrowOffset {
    _arrowOffset = arrowOffset;
}

- (UIPopoverArrowDirection)arrowDirection {
    return _arrowDirection;
}

- (void)setArrowDirection:(UIPopoverArrowDirection)arrowDirection {
    _arrowDirection = arrowDirection;
}

+ (UIEdgeInsets)contentViewInsets{
//    return UIEdgeInsetsMake(CONTENT_OUTSET, CONTENT_OUTSET, CONTENT_OUTSET, CONTENT_OUTSET);
    return UIEdgeInsetsZero;
}

+(CGFloat)arrowHeight{
    return ARROW_HEIGHT;
}

+(CGFloat)arrowBase{
    return ARROW_BASE;
}

-  (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat _height = self.frame.size.height;
    CGFloat _width = self.frame.size.width;
    CGFloat _left = -CONTENT_OUTSET;
    CGFloat _top = -CONTENT_OUTSET;
    CGFloat _coordinate = 0.0;
    CGAffineTransform _rotation = CGAffineTransformIdentity;
    
    _width += (CONTENT_OUTSET * 2);
    _height += (CONTENT_OUTSET * 2);
    
    switch (self.arrowDirection) {
        case UIPopoverArrowDirectionUp:
            _top += ARROW_HEIGHT;
            _height -= ARROW_HEIGHT;
            _coordinate = ((_originalSize.width / 2) + self.arrowOffset) - (ARROW_BASE/2);
            _arrowView.frame = CGRectMake(_coordinate, (0 - CONTENT_OUTSET), ARROW_BASE, ARROW_HEIGHT);
            break;
        case UIPopoverArrowDirectionDown:
            _height -= ARROW_HEIGHT;
//            _top -= ARROW_HEIGHT;
            _coordinate = ((_originalSize.width / 2) + self.arrowOffset) - (ARROW_BASE/2);
            _arrowView.frame = CGRectMake(_coordinate, (_height - CONTENT_OUTSET), ARROW_BASE, ARROW_HEIGHT);
            _rotation = CGAffineTransformMakeRotation( M_PI );
            break;
        case UIPopoverArrowDirectionLeft:
            _left += ARROW_HEIGHT;
            _width -= ARROW_HEIGHT;
            _coordinate = ((_originalSize.height / 2) + self.arrowOffset) - (ARROW_HEIGHT/2);
            _arrowView.frame = CGRectMake((0 - CONTENT_OUTSET), _coordinate, ARROW_HEIGHT, ARROW_BASE);
            _rotation = CGAffineTransformMakeRotation( -M_PI_2 );
            break;
        case UIPopoverArrowDirectionRight:
            _width -= ARROW_HEIGHT;
//            _left += ARROW_HEIGHT;
            _coordinate = ((_originalSize.height / 2) + self.arrowOffset) - (ARROW_HEIGHT/2);
            _arrowView.frame = CGRectMake((_width - CONTENT_OUTSET), _coordinate, ARROW_HEIGHT, ARROW_BASE);
            _rotation = CGAffineTransformMakeRotation( M_PI_2 );
            break;
        default:   
            break;
    }
    
//    [_arrowView drawBorder];
    
    _borderImageView.frame =  CGRectMake(_left, _top, _width, _height);
//    [_borderImageView drawBorder];
    
    
    [_arrowView setTransform:_rotation];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
