//
//  OrderPopoverBackgroundView.h
//  RushOrder
//
//  Created by Conan on 2/18/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderPopoverBackgroundView : UIPopoverBackgroundView
{
    __strong UIImageView *_borderImageView;
    __strong UIImageView *_arrowView;
    CGFloat _arrowOffset;
    UIPopoverArrowDirection _arrowDirection;
    CGSize _originalSize;
}

@end
