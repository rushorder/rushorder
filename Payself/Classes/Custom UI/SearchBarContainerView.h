//
//  SearchBarContainerView.h
//  RushOrder
//
//  Created by Conan Kim on 12/2/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchBarContainerView : UIView
@property(strong, nonatomic) UISearchBar *searchBar;
@end
