//
//  PCTableView.m
//  RushOrder
//
//  Created by Conan on 3/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCTableView.h"

@implementation PCTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)keyWillShow:(NSNotification *)aNoti
{
    //    PCLog(@"aNoti : %@", aNoti);
    [self setContentInsetsWithNotification:aNoti keyboardShown:YES];
}

- (void)keyWillHide:(NSNotification *)aNoti
{
    //    PCLog(@"aNoti : %@", aNoti);
    [self setContentInsetsWithNotification:aNoti keyboardShown:NO];
}

- (void)setContentInsetsWithNotification:(NSNotification *)aNoti
                           keyboardShown:(BOOL)keyboardShown
{
    UIView *firstResponder = [self findFirstResponder];
    if(firstResponder == nil){
        return;
    }
    
    NSTimeInterval animationDuration = [[aNoti.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGRect endFrame = [[aNoti.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect convertedFrame = [self convertRect:endFrame fromView:self.window];
    convertedFrame.origin.y -= self.contentOffset.y;
    CGFloat hiddenHeight = self.bounds.size.height - convertedFrame.origin.y;
    hiddenHeight = MAX(hiddenHeight, 0);
    
    [UIView animateWithDuration:animationDuration
                     animations:^(){
                         if(keyboardShown){
                             
                             self.contentInset = UIEdgeInsetsMake(0.0f,
                                                                  0.0f,
                                                                  hiddenHeight,
                                                                  0.0f);
                             
                             CGRect toScrollRect = [firstResponder.superview convertRect:firstResponder.frame
                                                                                  toView:self];
                             toScrollRect.origin.y -= 10.0f;
                             
                             toScrollRect.origin.y = MIN(toScrollRect.origin.y,
                                                         self.contentSize.height - (self.height - hiddenHeight));
                             toScrollRect.origin.y = MAX(toScrollRect.origin.y,
                                                         0.0f);
                             
                             [self setContentOffset:CGPointMake(0.0f, toScrollRect.origin.y)
                                           animated:NO];
                             
                         } else {
                             
                             self.contentInset = UIEdgeInsetsZero;
                             
                             CGFloat bottomInsets = self.contentInset.bottom;
                             
                             CGPoint offset = [self contentOffset];
                             offset.y -= bottomInsets;
                             offset.y = MIN(offset.y,
                                            self.contentSize.height - self.height);
                             offset.y = MAX(offset.y,
                                            0.0f);
                             [self setContentOffset:offset
                                           animated:NO];
                             
                         }
                         self.scrollIndicatorInsets = self.contentInset;
                     }];
}

@end
