//
//  TouchableImageView.m
//  RushOrder
//
//  Created by Conan Kim on 8/16/17.
//  Copyright © 2017 Paycorn. All rights reserved.
//

#import "TouchableImageView.h"

@implementation TouchableImageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.userInteractionEnabled = YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.alpha = 0.8;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.alpha = 1.0;
}

- (void)touchesEnded:(NSSet *)touches
           withEvent:(UIEvent *)event
{
    self.alpha = 1.0;
    [self.target touchableImageViewActionTriggerred:self];
}
@end
