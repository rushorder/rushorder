//
//  PCCurrencyTextField.m
//  RushOrder
//
//  Created by Conan on 2/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCCurrencyTextField.h"
#import "FormatterManager.h"

@interface PCCurrencyTextField()
{
//    PCCurrency _amount;
}

//@property (strong, nonatomic) NSNumberFormatter* currencyFormatter;
@property (strong, nonatomic) UIColor *defaultTextColor;
@end

@implementation PCCurrencyTextField
@synthesize amount = _amount;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self != nil){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    // Initialization code
    self.keyboardType = UIKeyboardTypeDecimalPad;
    
//    self.currencyFormatter = [NSNumberFormatter currencyFormatter];
    
    [self reset];
    
    if(self.delegate != FORMATTER){
        self.forwardDelegate = (id<PCCurrencyTextFieldDelegate>)self.delegate;
        self.delegate = FORMATTER;
    }
}

- (void)reset
{
    [self.delegate textField:self
shouldChangeCharactersInRange:NSMakeRange(0, 0)
  replacementString:@"0"];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.defaultTextColor = self.textColor;
    UIImage *image = [self.background resizableImageFromCenter];
    self.background = image;
    
    if(self.delegate != FORMATTER){
        self.forwardDelegate = (id<PCCurrencyTextFieldDelegate>)self.delegate;
        self.delegate = FORMATTER;
    }
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    CGRect superRect = [super editingRectForBounds:bounds];
    return [self textRectForBounds:superRect];
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    CGFloat sideMargin = 8.0f;

    bounds.size.width -= sideMargin * 2;
    bounds.origin.x += sideMargin;
//    bounds.origin.y += 2.0f;
    
    return bounds;
}

- (void)sendMessageToTargets
{
    NSSet *targets = [self allTargets];
    
    for(id target in targets){
        NSArray *selectors = [self actionsForTarget:target forControlEvent:UIControlEventValueChanged];
        for(NSString *selectorName in selectors){
            SEL selector = NSSelectorFromString(selectorName);
            [target performSelector:selector
                         withObject:self];
        }
    }
}

- (void)setAmount:(PCCurrency)value
{
//    _amount = value;
//    NSMutableString* mstring = [NSMutableString string];
//
//    [mstring appendString:[[NSLocale currentLocale] objectForKey:NSLocaleCurrencySymbol]];
//    
    NSNumber *amountNumber = [NSNumber numberWithFloat:(value / 100.0f)];
    
    self.text = [FORMATTER.currencyFormatter stringFromNumber:amountNumber];
    
    
    [self sendMessageToTargets];
}

- (void)setEnabled:(BOOL)value
{
    if(value){
        self.textColor = self.defaultTextColor;
    } else {
        self.textColor = [UIColor lightGrayColor];
    }
    
    [super setEnabled:value];
}

- (PCCurrency)amount
{
    NSNumber* number = [FORMATTER.currencyFormatter numberFromString:self.text];
    return lroundf([number floatValue] * 100);
}

- (NSString *)currencyCode
{
    return FORMATTER.currencyFormatter.currencyCode;
}


@end
