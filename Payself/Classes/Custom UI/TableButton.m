//
//  TableButton.m
//  RushOrder
//
//  Created by Conan on 2/15/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "TableButton.h"
#import <QuartzCore/QuartzCore.h>

#define _TABLENUMBER_LABEL_MARGIN 5.0f

@interface TableButton()


@end

@implementation TableButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UISwipeGestureRecognizer *swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                              action:@selector(buttonSwiped:)];
        swipeRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
        [self addGestureRecognizer:swipeRecognizer];
    }
    return self;
}

- (void)buttonSwiped:(id)sender
{
    [self.swipeTarget performSelector:self.swipeSelector
                           withObject:self];
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];

    self.layer.cornerRadius = 8.0f;
    self.layer.masksToBounds = YES;
    
    self.tableNumberLabel.text = self.tableNumber;
    self.tableNumberLabel.adjustsFontSizeToFitWidth = YES;
    
//    [self.tableNumberLabel drawBorderWithColor:[UIColor blueColor]];
    
    
    CGFloat height = rect.size.height - (_TABLENUMBER_LABEL_MARGIN * 2) - 48.0f;
    height = MAX(32.0f, height);
    
    self.tableNumberLabel.frame = CGRectMake(_TABLENUMBER_LABEL_MARGIN,
                                             _TABLENUMBER_LABEL_MARGIN,
                                             rect.size.width - (_TABLENUMBER_LABEL_MARGIN * 2),
                                             height);
    
//    if([[self titleForState:UIControlStateNormal] length] > 0){
//        self.tableNumberLabel.font = [UIFont boldSystemFontOfSize:40.0f];
//        self.tableNumberLabel.textAlignment = NSTextAlignmentCenter;
//    } else {
//        //Full size
//        
//    }

    self.tableNumberLabel.font = [UIFont boldSystemFontOfSize:(self.tableNumberLabel.width / 2.0f)];
    self.tableNumberLabel.textAlignment = NSTextAlignmentCenter;
    
//    PCLog(@"Title Label %@", self.titleLabel);
    
//    if(self.enLarged){
//        self.tableNumberLabel.frame = CGRectMake(_TABLENUMBER_LABEL_MARGIN,
//                                                 _TABLENUMBER_LABEL_MARGIN,
//                                                 rect.size.width - (_TABLENUMBER_LABEL_MARGIN * 2),
//                                                 rect.size.height - (_TABLENUMBER_LABEL_MARGIN * 2));
//    } else {
//        self.tableNumberLabel.frame = CGRectMake(_TABLENUMBER_LABEL_MARGIN,
//                                                 _TABLENUMBER_LABEL_MARGIN,
//                                                 rect.size.width - _TABLENUMBER_LABEL_MARGIN,
//                                                 self.tableNumberLabel.font.pointSize + 2.0f);
//    }
}

- (UILabel *)tableNumberLabel
{
    if(_tableNumberLabel == nil){
        _tableNumberLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _tableNumberLabel.textColor = [self titleColorForState:UIControlStateNormal];
        _tableNumberLabel.highlightedTextColor = [self titleColorForState:UIControlStateHighlighted];
        _tableNumberLabel.backgroundColor = [UIColor clearColor];
        _tableNumberLabel.adjustsFontSizeToFitWidth = YES;
        _tableNumberLabel.minimumScaleFactor = 0.8f;
        [self addSubview:_tableNumberLabel];
    }
    return _tableNumberLabel;
}

- (void)setEnLarged:(BOOL)val
{
    _enLarged = val;
    if(_enLarged){
        _tableNumberLabel.font = [UIFont systemFontOfSize:60.0f];
        _tableNumberLabel.minimumScaleFactor = 0.2f;
    } else {
        _tableNumberLabel.font = self.titleLabel.font;
    }
    
    [self setNeedsDisplay];
}


@end
