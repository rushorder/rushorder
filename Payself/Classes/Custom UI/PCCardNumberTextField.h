//
//  PCCardNumberTextField.h
//  RushOrder
//
//  Created by Conan on 2/27/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FormatterManager.h"

@protocol PCCardNumberTextFieldDelegate;

@interface PCCardNumberTextField : UITextField <UITextFieldDelegate>

@property (weak, nonatomic) id<PCCardNumberTextFieldDelegate> forwardDelegate;
@property (nonatomic) Issuer issuer;
@property (nonatomic, readonly) NSString *cardNumber;
@property (nonatomic, readwrite, assign) IBOutlet UITextField *nextField;
@property (nonatomic) BOOL underLine;

- (void)valueChanged:(id)sender;

@end

@protocol PCCardNumberTextFieldDelegate <UITextFieldDelegate>
@optional
- (void)cardNumberTextField:(PCCardNumberTextField *)textField
            didChangeIssuer:(Issuer)issuer
               issuerString:(NSString *)string;
@end