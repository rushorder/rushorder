//
//  NPStretchableButton.h
//  RushOrder
//
//  Created by Conan on 12/21/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NPStretchableButton : UIButton

@property (nonatomic) BOOL highlightedImage;
@end
