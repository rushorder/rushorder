//
//  NPBadgeView.h
//  RushOrder
//
//  Created by Conan on 1/11/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

#define MIN_BADGE_SIZE  27.0f

@interface NPBadgeView : UIView

@property (nonatomic) NSInteger badgeValue;
@property (copy, nonatomic) NSString *badgeStringValue;

@property (strong, nonatomic) id target;
@property (nonatomic) SEL action;

@property (strong, nonatomic) UIButton *countButton;
@end
