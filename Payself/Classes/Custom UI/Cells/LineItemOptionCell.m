//
//  LineItemOptionCell.m
//  RushOrder
//
//  Created by Conan on 7/8/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "LineItemOptionCell.h"

@implementation LineItemOptionCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)fillContents
{
    self.lineItemOptionNameLabel.text = [self.optionDict objectForKey:@"option_name"];
    PCCurrency optionQuantityPrice = [self.optionDict currencyForKey:@"option_price_quantity"];
    if(optionQuantityPrice > 0){
        self.lineItemOptionPriceLabel.text = [[NSNumber numberWithCurrency:optionQuantityPrice] currencyString];
    } else {
        self.lineItemOptionPriceLabel.text = nil;
    }
    self.lineView.hidden = ![self.optionDict boolForKey:@"is_last_item"];
}

- (void)setOptionDict:(NSDictionary *)dict
{
    _optionDict = dict;
    [self fillContents];
}

@end
