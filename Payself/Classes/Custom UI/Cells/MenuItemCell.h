//
//  MenuItemCell.h
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuItem.h"
#import "MenuSubCategory.h"

@interface MenuItemCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *selectButton;
//@property (weak, nonatomic) IBOutlet UIView *imageContainerView;
//@property (weak, nonatomic) IBOutlet UIImageView *imgFrameView;
@property (weak, nonatomic) IBOutlet NPImageButton *menuImageButton;
@property (weak, nonatomic) IBOutlet UILabel *menuNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *menuPriceLabel;
@property (strong, nonatomic) MenuItem *item;
@property (weak, nonatomic) IBOutlet UILabel *menuDescLabel;

- (void)setItemForHeight:(MenuItem *)item;
- (CGFloat)cellHeight;
@end

@protocol MenuItemCellDelegate <UITableViewDelegate>
@optional
- (void)cell:(MenuItemCell *)cell didTouchedAddToCartButtonAtIndexPath:(NSIndexPath *)indexPath;
- (void)cell:(MenuItemCell *)cell didTouchedPhotoButtonAtIndexPath:(NSIndexPath *)indexPath;
@end


//******************************************************************************
//******************************************************************************

@interface MenuSubCategoryCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *menuSubCategoryNameLabel;
@property (strong, nonatomic) MenuSubCategory *subCategory;

- (CGFloat)cellHeight;
@end


//******************************************************************************
//******************************************************************************

@interface MenuCategorySectionView : UIView

@property (weak, nonatomic) IBOutlet UIButton *button;
@end;