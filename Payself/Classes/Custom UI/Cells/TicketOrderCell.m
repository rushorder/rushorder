//
//  TicketOrderCell.m
//  RushOrder
//
//  Created by Conan on 5/23/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "TicketOrderCell.h"

@interface TicketOrderCell()




@end

@implementation TicketOrderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)fillContentsWithOrderComment:(NSString *)comment
{
    self.priceLabel.text = self.lineItem.lineAmountString;
    self.menuNameLabel.text = self.lineItem.displayMenuName;
    self.qtyLabel.text = self.lineItem.serializedOptionNames;
}

- (void)setLineItem:(LineItem *)lineItem
{
    _lineItem = lineItem;
}


@end