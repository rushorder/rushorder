//
//  OrderMenuCell.h
//  RushOrder
//
//  Created by Conan on 5/23/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LineItem.h"
#import "TouchableLabel.h"

@interface MenuOrderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *menuNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *qtyLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *paidImageView;
@property (strong, nonatomic) IBOutlet UIButton *removeButton;

@property (strong, nonatomic) LineItem *lineItem;

- (void)fillContentsWithOrderComment:(NSString *)comment;

@end


@protocol MenuOrderCellDelegate <UITableViewDelegate>
@optional
- (void)cell:(MenuOrderCell *)cell didTouchedRemoveFromCartButtonAtIndexPath:(NSIndexPath *)indexPath;
@end
