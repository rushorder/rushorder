//
//  ProcessingCell.h
//  RushOrder
//
//  Created by Conan on 6/10/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProcessingCellDelegate;

@interface ProcessingCell : UITableViewCell
<
LogoCircleImageViewAction,
TouchableLabelAction
>

@property (strong, nonatomic) id orderOrCart;

@property (weak, nonatomic) id<ProcessingCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet TouchableLabel *merchantNameLabel;
@property (weak, nonatomic) IBOutlet TouchableLabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;

@property (weak, nonatomic) IBOutlet UIImageView *orderTypeIcon;
@property (weak, nonatomic) IBOutlet UILabel *restaurantTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *payAmountTopConstraint;
@property (weak, nonatomic) IBOutlet UILabel *payAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *payAmountTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *tableNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *guideLabel;
@property (weak, nonatomic) IBOutlet LogoCircleImageView *thumbnailImageView;


- (void)fillContents;
- (void)fillContentsForHeight:(BOOL)forHeight;

@end

@protocol ProcessingCellDelegate <UITableViewDelegate>
@optional
- (void)tableView:(UITableView *)tableView cell:(ProcessingCell *)cell didTouchProcessButtonAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableView:(UITableView *)tableView cell:(ProcessingCell *)cell didTouchClearButtonAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableView:(UITableView *)tableView cell:(ProcessingCell *)cell didTouchCancelButtonAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableView:(UITableView *)tableView cell:(ProcessingCell *)cell didTouchGotItButtonAtIndexPath:(NSIndexPath *)indexPath;
@end
