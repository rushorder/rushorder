//
//  RecentAddressCell.h
//  RushOrder
//
//  Created by Conan on 2/28/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Addresses.h"
#import "DeliveryAddress.h"
@interface RecentAddressCell : UITableViewCell

@property (strong, nonatomic) Addresses *addreses;
@property (strong, nonatomic) DeliveryAddress *deliveryAddress;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *checkImageView;

- (void)fillContent;
@end
