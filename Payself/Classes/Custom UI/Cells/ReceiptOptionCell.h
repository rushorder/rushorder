//
//  ReceiptOptionCell.h
//  RushOrder
//
//  Created by Conan on 6/12/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LineItem.h"

@interface ReceiptOptionCell : UITableViewCell

@property (strong, nonatomic) NSDictionary *optionDict;
@end
