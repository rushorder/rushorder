//
//  ReceiptOptionCell.m
//  RushOrder
//
//  Created by Conan on 6/12/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "ReceiptOptionCell.h"

@interface ReceiptOptionCell()
@property (weak, nonatomic) IBOutlet UILabel *menuNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

@end

@implementation ReceiptOptionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)fillContents
{
    self.menuNameLabel.text = [self.optionDict objectForKey:@"option_name"];
    self.amountLabel.text = [self.optionDict objectForKey:@"option_price"];
    
    PCLog(@"self.menuNameLabel.text %@", self.menuNameLabel.text);
    PCLog(@"self.amountLabel.text %@", self.amountLabel.text);
}

- (void)setOptionDict:(NSDictionary *)optionDict
{
    _optionDict = optionDict;
    [self fillContents];
}

@end
