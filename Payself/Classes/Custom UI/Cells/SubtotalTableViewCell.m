//
//  SubtotalTableViewCell.m
//  RushOrder
//
//  Created by Conan on 9/26/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "SubtotalTableViewCell.h"

@interface SubtotalTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *subtotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryFeeLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceFeeLabel;
@property (weak, nonatomic) IBOutlet UILabel *tipLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPaidLabel;
@property (weak, nonatomic) IBOutlet UILabel *orangePointLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *totalPaidTopConstraint;
@property (weak, nonatomic) IBOutlet UILabel *totalPaidTitleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orangePointTopConstraint;
@property (weak, nonatomic) IBOutlet UILabel *orangePointTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *deliveryFeeTitleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deliveryFeeTopConstraint;
@property (weak, nonatomic) IBOutlet UILabel *serviceFeeTitleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *serviceFeeTopConstraint;
@property (weak, nonatomic) IBOutlet UILabel *tipTitleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tipTopConstraint;
@property (weak, nonatomic) IBOutlet UILabel *discountTitleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discountTopConstraint;
@end

@implementation SubtotalTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)drawData
{
    if(self.favoriteOrder != nil){
        
        self.subtotalLabel.text = self.favoriteOrder.subTotalString;
        self.taxLabel.text = self.favoriteOrder.taxNroServiceFeeAmountString;
        if(self.favoriteOrder.roServiceFee > 0){
            self.taxTitleLabel.text = NSLocalizedString(@"Tax & Fees", nil);
        } else {
            self.taxTitleLabel.text = NSLocalizedString(@"Tax", nil);
        }
        
        if(self.favoriteOrder.deliveryFee > 0 || self.order.orderType == OrderTypeDelivery){
            self.deliveryFeeLabel.text = self.favoriteOrder.deliveryFeeString;
        } else {
            self.deliveryFeeLabel.text = nil;
        }
        
        // Commented Out - Merge Service Fee + Tax in one line
//        if(self.favoriteOrder.roServiceFee > 0){
//            self.serviceFeeLabel.text = self.favoriteOrder.roServiceFeeString;
//        } else {
//            self.serviceFeeLabel.text = nil;
//        }
        self.serviceFeeLabel.text = nil; // Instead hide
        
        if(self.favoriteOrder.tips > 0){
            self.tipLabel.text = self.favoriteOrder.tipsString;
        } else {
            self.tipLabel.text = nil;
        }
        
        self.discountLabel.text = nil;
        
        self.totalLabel.text = [[NSNumber numberWithCurrency:self.favoriteOrder.total + self.favoriteOrder.tips] currencyString];
        
        self.totalPaidLabel.text = nil;
        self.orangePointLabel.text = nil;
        
    } else {
        self.subtotalLabel.text = self.order.subTotalString;
        self.taxLabel.text = self.order.taxNroServiceFeeAmountString;
        if(self.order.roServiceFee > 0){
            self.taxTitleLabel.text = NSLocalizedString(@"Tax & Fees", nil);
        } else {
            self.taxTitleLabel.text = NSLocalizedString(@"Tax", nil);
        }

        
        if(self.order.deliveryFee > 0 || self.order.orderType == OrderTypeDelivery){
            self.deliveryFeeLabel.text = self.order.deliveryFeeString;
        } else {
            self.deliveryFeeLabel.text = nil;
        }
    
        // Commented Out - Merge Service Fee + Tax in one line
//        if(self.order.roServiceFee > 0){
//            self.serviceFeeLabel.text = self.order.roServiceFeeString;
//        } else {
//            self.serviceFeeLabel.text = nil;
//        }
        self.serviceFeeLabel.text = nil; // Instead hide
        
        if(self.order.tips > 0){
            self.tipLabel.text = self.order.tipsString;
        } else {
            self.tipLabel.text = nil;
        }
        
        if(self.order.discountedAmount > 0){
            self.discountLabel.text = self.order.discountedAmountString;
        } else {
            self.discountLabel.text = nil;
        }
        
        self.totalLabel.text = self.order.dueTotalString;
        self.totalPaidLabel.text = self.order.cardAmountString;
        
        if(self.order.creditAmounts > 0){
            self.orangePointLabel.text = self.order.creditAmountString;
            self.totalPaidTitleLabel.text = NSLocalizedString(@"Credit Card", nil);
        } else {
            self.orangePointLabel.text = nil;
            self.totalPaidTitleLabel.text = NSLocalizedString(@"Total Paid Amount", nil);
        }
        
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if(self.deliveryFeeLabel.text != nil){
        self.deliveryFeeTopConstraint.constant = 4.0f;
    } else {
        self.deliveryFeeTopConstraint.constant = 0.0f;
    }
    self.deliveryFeeTitleLabel.hidden = self.deliveryFeeLabel.text == nil;
    
    if(self.serviceFeeLabel.text != nil){
        self.serviceFeeTopConstraint.constant = 4.0f;
    } else {
        self.serviceFeeTopConstraint.constant = 0.0f;
    }
    self.serviceFeeTitleLabel.hidden = self.serviceFeeLabel.text == nil;
    
    if(self.tipLabel.text != nil){
        self.tipTopConstraint.constant = 4.0f;
    } else {
        self.tipTopConstraint.constant = 0.0f;
    }
    self.tipTitleLabel.hidden = self.tipLabel.text == nil;
    
    if(self.discountLabel.text != nil){
        self.discountTopConstraint.constant = 4.0f;
    } else {
        self.discountTopConstraint.constant = 0.0f;
    }
    self.discountTitleLabel.hidden = self.discountLabel.text == nil;
    
    if(self.totalPaidLabel.text != nil){
        self.totalPaidTopConstraint.constant = 4.0f;
    } else {
        self.totalPaidTopConstraint.constant = 0.0f;
    }
    self.totalPaidTitleLabel.hidden = self.totalPaidLabel.text == nil;
    
    if(self.orangePointLabel.text != nil){
        self.orangePointTopConstraint.constant = 4.0f;
    } else {
        self.orangePointTopConstraint.constant = 0.0f;
    }
    self.orangePointTitleLabel.hidden = self.orangePointLabel.text == nil;
}

@end
