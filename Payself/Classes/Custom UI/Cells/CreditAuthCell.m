//
//  CreditAuthCell.m
//  RushOrder
//
//  Created by Conan on 3/5/14.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "CreditAuthCell.h"

@implementation CreditAuthCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)verifyButtonTouched:(id)sender
{
    if([self.delegate respondsToSelector:@selector(creditAuthCell:didTouchedVerifyButton:)]){
        [self.delegate creditAuthCell:self didTouchedVerifyButton:sender];
    }
}

@end