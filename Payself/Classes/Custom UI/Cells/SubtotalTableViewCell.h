//
//  SubtotalTableViewCell.h
//  RushOrder
//
//  Created by Conan on 9/26/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavoriteOrder.h"

@interface SubtotalTableViewCell : UITableViewCell

@property (strong, nonatomic) Order *order;
@property (strong, nonatomic) FavoriteOrder *favoriteOrder;

- (void)drawData;

@end
