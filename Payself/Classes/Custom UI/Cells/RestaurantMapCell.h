//
//  RestaurantMapCell.h
//  RushOrder
//
//  Created by Conan on 11/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Merchant.h"
#import "RestaurantMiniSummaryView.h"

@protocol RestaurantMapCellDelegate;

@interface RestaurantMapCell : UITableViewCell
<
    RestaurantMiniSummaryViewDelegate
>
@property (nonatomic, weak) id<RestaurantMapCellDelegate>delegate;
@property (nonatomic, strong) Merchant *merchant;
@property (nonatomic) BOOL needMap;

- (void)fillContents;
- (void)fillContentsForHeight:(BOOL)forHeight;
@end

@protocol RestaurantMapCellDelegate <UITableViewDelegate>
@optional
- (void)tableView:(UITableView *)tableView
             cell:(RestaurantMapCell *)cell
didTouchActionButtonAtIndexPath:(NSIndexPath *)indexPath
     withCartType:(CartType)cartType;
@end