//
//  RestaurantCell.m
//  RushOrder
//
//  Created by Conan on 11/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "RestaurantCell.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface RestaurantCell()

@property (strong, nonatomic) RestaurantMiniSummaryView *restaurantView;

@end

@implementation RestaurantCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.restaurantView = [RestaurantMiniSummaryView viewWithNibName:@"RestaurantMiniSummaryView"];
    self.restaurantView.translatesAutoresizingMaskIntoConstraints = NO;
    self.restaurantView.delegate = self;
    [self.contentView addSubview:self.restaurantView];
    
    NSDictionary *viewsDictionary = @{@"restaurantView":self.restaurantView};

    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[restaurantView]|"
                                                          options:0
                                                          metrics:nil
                                                            views:viewsDictionary];
    [self.contentView addConstraints:constraints];

    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[restaurantView]|"
                                                          options:0
                                                          metrics:nil
                                                            views:viewsDictionary];
    [self.contentView addConstraints:constraints];
}

- (void)fillContentsForHeight
{
    [self fillContentsForHeight:NO];
}

- (void)fillContentsForHeight:(BOOL)forHeight
{
    self.restaurantView.merchant = self.merchant;
    [self.restaurantView drawRestaurant:forHeight];
}

- (void)restaurantMiniSummaryView:(RestaurantMiniSummaryView *)viewController
             didTouchActionButton:(UIButton *)sender
{
    UITableView *tableView = self.tableView;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    
    if([tableView.delegate respondsToSelector:@selector(tableView:cell:didTouchActionButtonAtIndexPath:withCartType:)]){
        [((id <RestaurantCellDelegate>)tableView.delegate) tableView:tableView
                                                                cell:self
                                     didTouchActionButtonAtIndexPath:indexPath
                                                        withCartType:(CartType)sender.tag];
    }
}


@end
