//
//  CreditCell.m
//  RushOrder
//
//  Created by Conan on 4/24/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "CreditCell.h"

@implementation CreditCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)sliderValueChanged:(id)sender
{
    UITableView *tableView = self.tableView;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    if([tableView.delegate respondsToSelector:@selector(tableView:sliderValueChange:forRowAtIndexPath:)]){
        [(id<CreditCellDelegate>)tableView.delegate tableView:tableView
                         sliderValueChange:sender
                         forRowAtIndexPath:indexPath];
    }
}

@end
