//
//  PaymentInfoTableViewCell.h
//  RushOrder
//
//  Created by Conan on 9/26/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentInfoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cardInfoLabel;
@property (weak, nonatomic) IBOutlet UIImageView *refundImageView;
@property (weak, nonatomic) IBOutlet UIView *editButton;
@property (weak, nonatomic) IBOutlet UIButton *receiptButton;

@end


@protocol PaymentInfoTableViewCellDelegate <UITableViewDelegate>
- (void)tableView:(UITableView *)tableView didTouchReceiptButtonAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableView:(UITableView *)tableView didTouchEditButtonAtIndexPath:(NSIndexPath *)indexPath;
@end