//
//  FavoriteOrderCollectionViewCell.h
//  RushOrder
//
//  Created by Conan on 8/12/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FavoriteOrderCollectionViewCellDelegate;

@interface FavoriteOrderCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) id <FavoriteOrderCollectionViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *removeButton;
@property (weak, nonatomic) IBOutlet UIImageView *backImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

- (void)startJiggling;
@end


@protocol FavoriteOrderCollectionViewCellDelegate<NSObject>
- (void)cellDidTouchRemoveButton:(FavoriteOrderCollectionViewCell *)cell;
@end