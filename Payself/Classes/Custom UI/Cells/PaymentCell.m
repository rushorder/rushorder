//
//  PaymentCell.m
//  RushOrder
//
//  Created by Conan on 2/26/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PaymentCell.h"
#import "FormatterManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MerchantViewController.h"
#import "OrderManager.h"

@interface PaymentCell()


@end

@implementation PaymentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.thumbnailView.target = self;
}

- (void)circleImageViewActionTriggerred:(id)sender
{
    [ORDER resetGraphWithMerchant:self.order.merchant];
    MerchantViewController *viewController = [UIStoryboard viewController:@"MerchantViewController"
                                                                     from:@"Restaurants"];
    
    if(APP.tabBarController.presentedViewController == nil){
        [APP.tabBarController presentViewControllerInNavigation:viewController
                                                       animated:YES
                                                     completion:^{
                                                         
                                                     }];
    } else {
       [APP.tabBarController dismissViewControllerAnimated:YES
                                                completion:^{
                                                    [APP.tabBarController presentViewControllerInNavigation:viewController
                                                                                                   animated:YES
                                                                                                 completion:^{

                                                                                                 }];
                                                }];
    }
}

- (void)touchableLabelActionTriggerred:(id)sender
{
    [self circleImageViewActionTriggerred:sender];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)fillContents
{
    [self fillContentsForHeight:NO];
}

- (void)fillContentsForHeight:(BOOL)forHeight
{
    self.testIcon.hidden = !self.order.isTestMode;
    self.orderTypeIcon.image = self.order.orderTypeIcon;
    self.restaurantLabel.text = self.order.merchantName;
    self.restaurantTypeLabel.text = self.order.typeString;
//    self.addressLabel.text = self.order.merchant.displayMultilineAddress;
    self.addressLabel.text = self.order.merchantDesc;
    self.lineItemNamesLabel.text = self.order.lineItemNames;
    
    self.declinedIcon.hidden = (self.order.status != OrderStatusDeclined);
    
//    [self.restaurantLabel drawBorder];
//    [self.restaurantTypeLabel drawBorder];
//    [self.addressLabel drawBorder];
//    [self.lineItemNamesLabel drawBorder];
    
    // !!!: Replaced, due to (Customer App: remove "PAID AMOUNT" line from status [replace with TOTAL ORDER AMOUNT])
    // https://app.asana.com/0/inbox/23035815421587/98751436415373/35984714847338
//    NSNumber *payAmtNumber = [NSNumber numberWithCurrency:self.order.payAmounts];
    
//    if(self.order.tips == 0){
////        self.payAmountLabel.text = [NSString stringWithFormat:@"%@ Paid",
////                                    [payAmtNumber currencyString]];
//        
//        // !!!: Replaced, due to (Customer App: remove "PAID AMOUNT" line from status [replace with TOTAL ORDER AMOUNT])
//        // https://app.asana.com/0/inbox/23035815421587/98751436415373/35984714847338
//        self.payAmountLabel.text = self.order.totalString;
//        
//    } else {
//        NSNumber *tipAmount = [NSNumber numberWithCurrency:self.order.tips];
//        
////        self.payAmountLabel.text = [NSString stringWithFormat:@"%@ Paid (Tip %@)",
////                                    [payAmtNumber currencyString],
////                                    [tipAmount currencyString]];
//        
//        self.payAmountLabel.text = [NSString stringWithFormat:@"%@ (Tip %@)",
//                                    self.order.totalString,
//                                    [tipAmount currencyString]];
//    }
    
    self.payAmountLabel.text = self.order.grandTotalString;
    
    
    
    self.dateLabel.text = [self.order.orderDate timeString];
    
    if(!forHeight){
        NSURL *url = self.order.merchantLogoURL;
        if(url == nil) url = self.order.merchantImageURL;
        
        [self.thumbnailView sd_setImageWithURL:url
                              placeholderImage:[UIImage imageNamed:@"placeholder_logo"]];
    }
}

@end