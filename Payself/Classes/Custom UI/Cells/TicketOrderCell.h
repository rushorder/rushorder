//
//  OrderMenuCell.h
//  RushOrder
//
//  Created by Conan on 5/23/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LineItem.h"

@interface TicketOrderCell : UITableViewCell

@property (strong, nonatomic) LineItem *lineItem;
@property (weak, nonatomic) IBOutlet UILabel *menuNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *qtyLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

- (void)fillContentsWithOrderComment:(NSString *)comment;

@end