//
//  BillOrderCell.h
//  RushOrder
//
//  Created by Conan on 5/27/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LineItem.h"

@protocol BillOrderCellDelegate;

@interface BillOrderCell : UITableViewCell

@property (strong, nonatomic) LineItem *lineItem;
@end


@protocol BillOrderCellDelegate <UITableViewDelegate>
@optional
-(void)cell:(BillOrderCell *)cell didTouchCheckButtonAtIndexPath:(NSIndexPath *)indexPath;
@end