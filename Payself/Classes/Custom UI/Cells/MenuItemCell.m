//
//  MenuItemCell.m
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MenuItemCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface MenuItemCell()
@property (strong, nonatomic) id<SDWebImageOperation> lastDnOperation;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alcoholImageWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alcoholImageLeadingConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *alcoholIcon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menuPhotoButtonWidthConstraint;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

@implementation MenuItemCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.separatorInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    self.menuImageButton.action = @selector(photoButtonTouched:);
}

- (void)fillContents
{
    [self fillContentsForHeight:NO];
}

- (UIEdgeInsets)layoutMargins
{
    return UIEdgeInsetsZero;
}

- (void)fillContentsForHeight:(BOOL)forHeight
{
    self.menuNameLabel.text = self.item.name;
    self.menuPriceLabel.text = self.item.defaultMenuPrice.priceString;
    self.menuDescLabel.text = self.item.desc;
    
    if(!forHeight){
        
        [self.lastDnOperation cancel];
        
        self.menuImageButton.image = [UIImage imageNamed:@"placeholder_menu"];
        self.menuImageButton.hidden = NO;
        self.activityIndicator.hidden = YES;
        [self.activityIndicator stopAnimating];
        
        if(self.item.thumbnailURL != nil){
            self.menuImageButton.hidden = YES;
            self.activityIndicator.hidden = NO;
            [self.activityIndicator startAnimating];
            
            self.lastDnOperation = [[SDWebImageManager sharedManager] downloadImageWithURL:self.item.thumbnailURL
                                                                                   options:0
                                                                                  progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                                                      
                                                                                  } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                                                                      if(finished){
                                                                                          self.menuImageButton.hidden = NO;
                                                                                          if(cacheType == SDImageCacheTypeNone){
                                                                                              if(error == nil && image != nil){
                                                                                                  self.menuImageButton.image = image;
                                                                                                  self.menuImageButton.alpha = 0.0f;
                                                                                                  
                                                                                                  [UIView animateWithDuration:0.4f
                                                                                                                   animations:^{
                                                                                                                       self.menuImageButton.alpha = 1.0f;
                                                                                                                   } completion:^(BOOL finished) {
                                                                                                                       [self.activityIndicator stopAnimating];
                                                                                                                       self.activityIndicator.hidden = YES;
                                                                                                                   }];
                                                                                              } else {
                                                                                                  PCError(@"Image download error %@" , error);
                                                                                                  self.menuImageButton.image = [UIImage imageNamed:@"placeholder_menu"];
                                                                                                  [self.activityIndicator stopAnimating];
                                                                                                  self.activityIndicator.hidden = YES;
                                                                                                  
                                                                                              }
                                                                                          } else {
                                                                                              self.menuImageButton.image = image;
                                                                                              self.menuImageButton.alpha = 1.0f;
                                                                                              [self.activityIndicator stopAnimating];
                                                                                              self.activityIndicator.hidden = YES;
                                                                                          }
                                                                                      } else {
                                                                                          
                                                                                      }
                                                                                  }];

        } else {
//            self.menuPhotoButtonWidthConstraint.constant = 0;
        }
        
        self.alcoholIcon.hidden = !self.item.isAlcohol;
        
        if(self.alcoholIcon.hidden){
            self.alcoholImageWidthConstraint.constant = 0.0f;
            self.alcoholImageLeadingConstraint.constant = 0.0f;
        } else {
            self.alcoholImageWidthConstraint.constant = 14.0f;
            self.alcoholImageLeadingConstraint.constant = 8.0f;
        }
        
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
//    [self.menuNameLabel sizeToHeightFit];
//    [self.menuPriceLabel sizeToHeightFit];
//    self.menuPriceLabel.y = CGRectGetMaxY(self.menuNameLabel.frame) + 3.0f;
//    self.lineView.y = [self cellHeight] - 1.0f;
}


- (void)setItem:(MenuItem *)item
{
    _item = item;
    [self fillContents];
}

- (void)setItemForHeight:(MenuItem *)item
{
    _item = item;
    [self fillContentsForHeight:YES];
    [self layoutSubviews];
}

- (CGFloat)cellHeight
{
    return MAX((CGRectGetMaxY(self.menuPriceLabel.frame) + 18.0f), 76.0f);
}

- (IBAction)addToCartButtonTouched:(id)sender
{
    UITableView *tableView = self.tableView;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    
    if([tableView.delegate respondsToSelector:@selector(cell:didTouchedAddToCartButtonAtIndexPath:)]){
        [((id <MenuItemCellDelegate>)tableView.delegate) cell:self didTouchedAddToCartButtonAtIndexPath:indexPath];
    }
}

- (IBAction)photoButtonTouched:(id)sender
{
    UITableView *tableView = self.tableView;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    
    if([tableView.delegate respondsToSelector:@selector(cell:didTouchedPhotoButtonAtIndexPath:)]){
        [((id <MenuItemCellDelegate>)tableView.delegate) cell:self didTouchedPhotoButtonAtIndexPath:indexPath];
    }
}

@end

//******************************************************************************
//******************************************************************************

@interface MenuSubCategoryCell()
@end

@implementation MenuSubCategoryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)fillContents
{
    self.menuSubCategoryNameLabel.text = self.subCategory.name;
    [self.menuSubCategoryNameLabel updateConstraints];
}


- (CGFloat)cellHeight
{
    self.menuSubCategoryNameLabel.preferredMaxLayoutWidth = self.menuSubCategoryNameLabel.width - 5.0f; //Hevetical Lighgt can not fit auto layout's intrinsic size give the buffer
    
    [self setNeedsUpdateConstraints];
    [self updateConstraintsIfNeeded];
    [self setNeedsLayout];
    [self layoutIfNeeded];
    
    CGSize cellSize = [self.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return cellSize.height + 1.0f;
}

- (void)setSubCategory:(MenuSubCategory *)subCategory
{
    _subCategory = subCategory;
    [self fillContents];
}

@end

//******************************************************************************
//******************************************************************************
@interface MenuCategorySectionView()
@end

@implementation MenuCategorySectionView

@end
