//
//  FavoriteOrderHeaderView.h
//  RushOrder
//
//  Created by Conan on 1/22/15.
//  Copyright (c) 2015 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoriteOrderHeaderView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *guideLabel;
@end
