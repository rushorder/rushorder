//
//  FavoriteRestCollectionViewCell.h
//  RushOrder
//
//  Created by Conan on 8/12/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CircleImageView.h"

@interface FavoriteRestCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet CircleImageView *backImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
