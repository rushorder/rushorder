//
//  DiscountTableViewCell.m
//  RushOrder
//
//  Created by Conan on 5/23/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "DiscountTableViewCell.h"



@interface DiscountTableViewCell()


@property (weak, nonatomic) IBOutlet UILabel *promotionTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *promotionDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountAmtLabel;
@property (weak, nonatomic) IBOutlet UIButton *removeButton;
@end

@implementation DiscountTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)fillContent
{
//    if([self.promotion.desc length] > 0){
//        self.promotionTitleLabel.text = self.promotion.desc;
//    } else {
    self.promotionTitleLabel.text = self.promotion.title;
    self.promotionDescLabel.text = self.promotion.desc;
//    }
    
    self.discountAmtLabel.text = self.promotion.appliedDiscountAmountString;
    
    self.removeButton.hidden = !self.promotion.isRequiredCode;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)discountButtonTouched:(id)sender
{
    NSIndexPath *selfIndexPath = [self.tableView indexPathForCell:self];
    
    if([self.delegate respondsToSelector:@selector(discountTableViewCell:didTouchRemoveButtonAtIndexPath:)]){
        [self.delegate discountTableViewCell:self
             didTouchRemoveButtonAtIndexPath:selfIndexPath];
    }
}

@end

