//
//  PaymentCell.h
//  RushOrder
//
//  Created by Conan on 2/26/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Payment.h"
#import "Receipt.h"

@interface PaymentCell : UITableViewCell
<
LogoCircleImageViewAction,
TouchableLabelAction
>

//@property (strong, nonatomic) Receipt *receipt;
@property (strong, nonatomic) Order *order;
           
@property (weak, nonatomic) IBOutlet UILabel *restaurantLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *payAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet LogoCircleImageView *thumbnailView;
@property (weak, nonatomic) IBOutlet UILabel *restaurantTypeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *testIcon;
@property (weak, nonatomic) IBOutlet UIImageView *orderTypeIcon;
@property (weak, nonatomic) IBOutlet UILabel *lineItemNamesLabel;
@property (weak, nonatomic) IBOutlet UIImageView *declinedIcon;

@property (nonatomic, getter = isShowAddButton) BOOL showAddButton;

- (void)fillContents;
- (void)fillContentsForHeight:(BOOL)forHeight;
@end



