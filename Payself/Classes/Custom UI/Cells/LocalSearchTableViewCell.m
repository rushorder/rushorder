//
//  LocalSearchTableViewCell.m
//  RushOrder
//
//  Created by Conan on 10/29/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "LocalSearchTableViewCell.h"

@implementation LocalSearchTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
