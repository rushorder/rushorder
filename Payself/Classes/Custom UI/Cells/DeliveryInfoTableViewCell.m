//
//  DeliveryInfoTableViewCell.m
//  RushOrder
//
//  Created by Conan on 9/26/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "DeliveryInfoTableViewCell.h"

@implementation DeliveryInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)editButtonTouched:(id)sender
{
    UITableView *tableView = self.tableView;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    if([tableView.delegate respondsToSelector:@selector(cell:didTouchEditButton:)]){
        [(id<DeliveryInfoTableViewCellDelegate>)tableView.delegate cell:self
                                                     didTouchEditButton:indexPath];
    }
}

@end
