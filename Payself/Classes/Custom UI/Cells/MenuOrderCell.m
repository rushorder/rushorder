//
//  MenuOrderCell.m
//  RushOrder
//
//  Created by Conan on 5/23/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MenuOrderCell.h"
#import "MenuManager.h"

@interface MenuOrderCell()


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *removeButtonWidthConstraint;
@property (weak, nonatomic) IBOutlet TouchableLabel *warningLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *warningTopSpaceConstraint;

@end

@implementation MenuOrderCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
//    self.warningLabel.target = self;
}

- (void)fillContentsWithOrderComment:(NSString *)comment
{
    if(self.lineItem.status == LineItemStatusOrdering && self.lineItem.isMine){
        self.removeButton.hidden = NO;
        self.removeButtonWidthConstraint.constant = 36.0f;
    } else {
        self.removeButton.hidden = YES;
        self.removeButtonWidthConstraint.constant = 10.0f;
    }
    self.priceLabel.text = self.lineItem.lineAmountString;
    self.menuNameLabel.text = self.lineItem.displayMenuName; 
    self.qtyLabel.text = self.lineItem.serializedOptionNames;
    
}

//- (void)touchableLabelActionTriggerred:(id)sender
//{
//    if(sender == self.warningLabel){
//        if((self.lineItem.menuValidStatus & MenuValidStatusMenuRemoved) == MenuValidStatusMenuRemoved){
//            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Restaurant no longer carries this menu item.", nil)];
//        } else if((self.lineItem.menuValidStatus & MenuValidStatusOutOfHours) == MenuValidStatusOutOfHours){
//            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"This item is from a menu that is available only at certain times.", nil)];
//        } else if((self.lineItem.menuValidStatus & MenuValidStatusOptionChanged) == MenuValidStatusOptionChanged){
//            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Selection option is no longer available.", nil)];
//        } else if((self.lineItem.menuValidStatus & MenuValidStatusPriceChanged) == MenuValidStatusPriceChanged){
//            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"No longer available at this price.", nil)];
//        } else {
//            //
//        }
//    }
//}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.warningLabel.text = nil;
    self.paidImageView.image = nil;
    
    if(self.lineItem.isPaid){
        self.paidImageView.image = [UIImage imageNamed:@"icon_paid"];
    } else {
        if((self.lineItem.menuValidStatus & MenuValidStatusMenuRemoved) == MenuValidStatusMenuRemoved){
            self.warningLabel.text = NSLocalizedString(@" Item Removed ", nil);
        } else if((self.lineItem.menuValidStatus & MenuValidStatusOutOfHours) == MenuValidStatusOutOfHours){
            self.warningLabel.text = NSLocalizedString(@" From Unavailable Menu ", nil);
        } else if((self.lineItem.menuValidStatus & MenuValidStatusNotAvailableOrderType) == MenuValidStatusNotAvailableOrderType){
            if(self.lineItem.substitutableMenuItem != nil){
                self.warningLabel.text = NSLocalizedString(@" Price Change ", nil);
            } else {
                self.warningLabel.text = [NSString stringWithFormat:NSLocalizedString(@" Item Not Available For %@ ", nil), MENUPAN.menuListTypeString];
            }
        } else if((self.lineItem.menuValidStatus & MenuValidStatusOptionChanged) == MenuValidStatusOptionChanged){
            self.warningLabel.text = NSLocalizedString(@" Option Removed ", nil);
        } else if((self.lineItem.menuValidStatus & MenuValidStatusPriceChanged) == MenuValidStatusPriceChanged){
            self.warningLabel.text = NSLocalizedString(@" Price Change ", nil);
        } else {
            //
        }
    }
    
    self.paidImageView.hidden = YES;
    self.warningLabel.hidden = YES;
    
    
    if(self.warningLabel.text != nil || self.paidImageView.image != nil || self.lineItem.status == LineItemStatusFixed){
        [self setMainTextColor:[UIColor lightGrayColor]];
        
        if(self.paidImageView.image != nil){
            self.paidImageView.hidden = NO;
        }
        
        if(self.warningLabel.text != nil){
            self.warningLabel.hidden = NO;
            self.warningTopSpaceConstraint.constant = 4.0f;
        } else {
            self.warningTopSpaceConstraint.constant = 0.0f;
        }
    } else {
        if(self.lineItem.isMine){
            [self setMainTextColor:[UIColor blackColor]];
        } else {
            [self setMainTextColor:[UIColor colorWithWhite:0.5f alpha:1.0f]];
        }
    }
}

- (void)setMainTextColor:(UIColor *)color
{
    self.menuNameLabel.textColor = color;
    self.qtyLabel.textColor = color;
    self.priceLabel.textColor = color;
}


- (void)setLineItem:(LineItem *)lineItem
{
    _lineItem = lineItem;
}

- (IBAction)removeFromCartButtonTouched:(id)sender
{
    UITableView *tableView = self.tableView;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    
    if([tableView.delegate respondsToSelector:@selector(cell:didTouchedRemoveFromCartButtonAtIndexPath:)]){
        [((id <MenuOrderCellDelegate>)tableView.delegate) cell:self didTouchedRemoveFromCartButtonAtIndexPath:indexPath];
    }
}

@end
