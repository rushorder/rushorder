//
//  BillOrderCell.m
//  RushOrder
//
//  Created by Conan on 5/27/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "BillOrderCell.h"

@interface BillOrderCell()
@property (weak, nonatomic) IBOutlet UILabel *menuLabel;
@property (weak, nonatomic) IBOutlet UILabel *listAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *qtyLabel;
@property (weak, nonatomic) IBOutlet UIImageView *paidImageView;
@property (weak, nonatomic) IBOutlet NPToggleButton *checkButton;

@end

@implementation BillOrderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.indentationWidth = 0;
        self.checkButton.manualSelection = YES;
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)fillContents
{
    self.listAmountLabel.text = self.lineItem.lineAmountString;
    [self.listAmountLabel updateConstraints];
    
    self.menuLabel.text = self.lineItem.displayMenuName;
    self.qtyLabel.text = self.lineItem.serializedOptionNames;
    
    self.paidImageView.hidden = !self.lineItem.isPaid;
    
    self.menuLabel.enabled = !(self.lineItem.isSplitExcept || self.lineItem.isPaid);
    
    if(self.lineItem.isPaid)
        self.lineItem.splitExcept = YES;
    
    self.checkButton.selected = !self.lineItem.isSplitExcept;
    self.checkButton.hidden = self.lineItem.isPaid;
    
    self.listAmountLabel.enabled = self.menuLabel.enabled;
    self.qtyLabel.enabled = self.menuLabel.enabled;
}


- (void)setLineItem:(LineItem *)lineItem
{
    _lineItem = lineItem;
    [self fillContents];
}

- (IBAction)checkButtonTouched:(id)sender
{
    UITableView *tableView = self.tableView;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    
    if([tableView.delegate respondsToSelector:@selector(cell:didTouchCheckButtonAtIndexPath:)]){
        [((id <BillOrderCellDelegate>)tableView.delegate) cell:self didTouchCheckButtonAtIndexPath:indexPath];
    }
}
@end
