//
//  RestaurantMapCell.m
//  RushOrder
//
//  Created by Conan on 11/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "RestaurantMapCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface RestaurantMapCell()

@property (strong, nonatomic) RestaurantMiniSummaryView *restaurantView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation RestaurantMapCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.restaurantView = [RestaurantMiniSummaryView viewWithNibName:@"RestaurantMiniSummaryView"];
    self.restaurantView.translatesAutoresizingMaskIntoConstraints = NO;
    self.restaurantView.delegate = self;
    [self.contentView addSubview:self.restaurantView];
    
    NSDictionary *viewsDictionary = @{@"mapView":self.mapView,
                                      @"restaurantView":self.restaurantView};
    
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|-11-[restaurantView]-11-|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:viewsDictionary];
    [self.contentView addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-54-[restaurantView]-0-[mapView]"
                                                          options:0
                                                          metrics:nil
                                                            views:viewsDictionary];
    [self.contentView addConstraints:constraints];
}

- (void)fillContents
{
    [self fillContentsForHeight:NO];
}

- (void)fillContentsForHeight:(BOOL)forHeight
{
    self.restaurantView.merchant = self.merchant;
    [self.restaurantView drawRestaurant:forHeight];
    if(!forHeight){
        self.mapView.region = MKCoordinateRegionMakeWithDistance(self.merchant.location.coordinate,
                                                                 500,
                                                                 500);
    }
}


- (void)restaurantMiniSummaryView:(RestaurantMiniSummaryView *)viewController
             didTouchActionButton:(UIButton *)sender
{
    UITableView *tableView = self.tableView;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    
    if([tableView.delegate respondsToSelector:@selector(tableView:cell:didTouchActionButtonAtIndexPath:withCartType:)]){
        [((id <RestaurantMapCellDelegate>)tableView.delegate) tableView:tableView cell:self didTouchActionButtonAtIndexPath:indexPath withCartType:(CartType)sender.tag];
    }
}



@end
