//
//  RecentTableViewCell.h
//  RushOrder
//
//  Created by Conan on 8/13/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"

@interface RecentTableViewCell : UITableViewCell

@property (nonatomic, getter = isRapidMode) BOOL rapidMode;

@property (nonatomic, strong) Order *order;
- (void)drawData;
@end
