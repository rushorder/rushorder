//
//  OrangeCell.h
//  RushOrder
//
//  Created by Conan on 4/24/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OrangeViewDelegate;

@interface OrangeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *creditBalaceLabel;
@property (weak, nonatomic) IBOutlet UILabel *useCreditLabel;
@property (weak, nonatomic) IBOutlet UILabel *useCreditAmountLabel;
@property (weak, nonatomic) IBOutlet NPToggleButton *useButton;
@property (weak, nonatomic) id<OrangeViewDelegate> viewDelegate;
@end


@protocol OrangeCellDelegate<UITableViewDelegate>
- (void)tableView:(UITableView *)tableView toggleButtonTouched:(NPToggleButton *)sender forRowAtIndexPath:indexPath;
@end

@protocol OrangeViewDelegate<NSObject>
- (void)orangeCelltoggleButtonTouched:(NPToggleButton *)sender;
@end