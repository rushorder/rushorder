//
//  MenuOptionCell.h
//  RushOrder
//
//  Created by Conan on 12/12/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuOptionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *checkImageView;
@end
