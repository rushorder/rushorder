//
//  DeliveryInfoTableViewCell.h
//  RushOrder
//
//  Created by Conan on 9/26/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DeliveryInfoTableViewCellDelegate;

@interface DeliveryInfoTableViewCell : UITableViewCell

@property (weak, nonatomic) id<DeliveryInfoTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *deliveryAddressLabel;
@property (weak, nonatomic) IBOutlet UIView *editButton;
@end

@protocol DeliveryInfoTableViewCellDelegate <UITableViewDelegate>
- (void)cell:(DeliveryInfoTableViewCell *)cell didTouchEditButton:(NSIndexPath *)indexPath;
@end