//
//  RestaurantCell.h
//  RushOrder
//
//  Created by Conan on 11/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Merchant.h"
#import "RestaurantMiniSummaryView.h"

@protocol RestaurantCellDelegate;

@interface RestaurantCell : UITableViewCell
<
RestaurantMiniSummaryViewDelegate
>

@property (nonatomic, weak) id<RestaurantCellDelegate>delegate;
@property (nonatomic, strong) Merchant *merchant;

- (void)fillContentsForHeight;
- (void)fillContentsForHeight:(BOOL)forHeight;

@end

@protocol RestaurantCellDelegate <UITableViewDelegate>
@optional
- (void)tableView:(UITableView *)tableView
             cell:(RestaurantCell *)cell
didTouchActionButtonAtIndexPath:(NSIndexPath *)indexPath
     withCartType:(CartType)cartType;
@end