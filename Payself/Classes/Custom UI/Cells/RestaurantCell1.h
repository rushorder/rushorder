//
//  RestaurantCell1.h
//  RushOrder
//
//  Created by Conan on 8/12/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Merchant.h"

@protocol RestaurantCell1Delegate;

@interface RestaurantCell1 : UITableViewCell
@property (weak, nonatomic) IBOutlet id<RestaurantCell1Delegate> delegate;
@property(weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) Merchant *merchant;
@property (weak, nonatomic) IBOutlet UIView *bottomLineView;
@property (nonatomic, getter = isShowDeliveryDistance) BOOL showDeliveryDistance;
- (void)drawData;
- (void)drawDataForHeight:(BOOL)forHeight;
@end

@protocol RestaurantCell1Delegate <NSObject>
- (void)tableView:(UITableView *)tableView
             cell:(RestaurantCell1 *)cell
didTouchActionButtonAtIndexPath:(NSIndexPath *)indexPath
     withCartType:(CartType)cartType;
@end