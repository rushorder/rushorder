//
//  RestaurantCell1.m
//  RushOrder
//
//  Created by Conan on 8/12/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import "RestaurantCell1.h"
#import "CircleImageView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface RestaurantCell1()

@property (weak, nonatomic) IBOutlet CircleImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *favoriteImageView;
@property (weak, nonatomic) IBOutlet UILabel *closedLabel;
@property (weak, nonatomic) IBOutlet UIView *closedBottomView;
@property (weak, nonatomic) IBOutlet UIView *buttonsContainer;
@property (strong, nonatomic) IBOutlet NPStretchableButton *processButton;
@property (strong, nonatomic) IBOutlet NPStretchableButton *takeoutButton;
@property (strong, nonatomic) IBOutlet DoubleImageButton *deliveryButton;
@property (weak, nonatomic) IBOutlet UIImageView *distanceIcon;
@property (weak, nonatomic) IBOutlet UIView *promotionContainerView;
@property (weak, nonatomic) IBOutlet UILabel *promotionLabel;
@property (weak, nonatomic) IBOutlet UIView *titleContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *distanceIconWidthContraint;
@property (weak, nonatomic) IBOutlet UIImageView *imnewImage;

@end

@implementation RestaurantCell1

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
//    self.processButton.translatesAutoresizingMaskIntoConstraints = NO;
//    self.takeoutButton.translatesAutoresizingMaskIntoConstraints = NO;
//    self.deliveryButton.translatesAutoresizingMaskIntoConstraints = NO;
//    self.buttonsContainer.translatesAutoresizingMaskIntoConstraints = NO;
    
//    self.distanceLabel.translatesAutoresizingMaskIntoConstraints = NO;
//    self.distanceIcon.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)drawData
{
    [self drawDataForHeight:NO];
}

- (void)drawDataForHeight:(BOOL)forHeight
{
//    self.deliveryButton.subImageOn = self.merchant.hasRoServiceFee;
    
    if(self.isShowDeliveryDistance){
        if(self.merchant.isDemoMode){
            self.distanceLabel.hidden = YES;
            self.distanceIcon.hidden = YES;
            self.distanceLabel.text = nil;
            self.distanceIconWidthContraint.constant = 0.0f;
        } else {
            self.distanceLabel.hidden = NO;
            self.distanceIcon.hidden = NO;
            self.distanceLabel.text = self.merchant.deliveryDistanceString;
            self.distanceIconWidthContraint.constant = 9.0f;
        }
    } else {
        if(self.merchant.isDemoMode || (APP.location == nil)){
            self.distanceLabel.hidden = YES;
            self.distanceIcon.hidden = YES;
            self.distanceLabel.text = nil;
            self.distanceIconWidthContraint.constant = 0.0f;
        } else {
            self.distanceLabel.hidden = NO;
            self.distanceIcon.hidden = NO;
            self.distanceLabel.text = self.merchant.distanceString;
            self.distanceIconWidthContraint.constant = 9.0f;
        }
    }
    
    if(self.merchant.price > 0){
        NSMutableString *priceString = [NSMutableString string];
        for(int i = 0; i < self.merchant.price ; i++){
            [priceString appendString:@"$"];
        }
        self.priceLabel.text = priceString;
    } else {
        self.priceLabel.text = nil;
    }
    
    if(self.merchant.promotion != nil){
       
//        NSString *shortenedTitle = nil;
//        if(self.merchant.promotion.isFlatDiscount){
//            shortenedTitle = [NSString stringWithFormat:@"%@ OFF", [[NSNumber numberWithCurrency:self.merchant.promotion.discountAmount] currencyStringWithoutCent]];
//        } else {
//            shortenedTitle = [NSString stringWithFormat:@"%@ OFF", [[NSNumber numberWithCurrency:self.merchant.promotion.discountRate] percentString]];
//        }
//        
        self.promotionLabel.text = self.merchant.promotion.title;
        self.promotionContainerView.hidden = NO;
    } else {
        self.promotionLabel.text = nil;
        self.promotionContainerView.hidden = YES;
    }
    
    self.nameLabel.text = self.merchant.name;
    [self.nameLabel updateConstraints];
    
    if(!forHeight){
        self.favoriteImageView.hidden = !self.merchant.isFavorite;
        self.closedLabel.hidden = self.merchant.isOpenHour;
        
        if(self.merchant.isOnlyDelivery){ //IF Open once more check
            // This .isOnlyDelivery should be run at once for setting up nearestDeliveryOperationHour variable
            // So, don't rewrite above if clause by taking below self.closedLabel.hidden condition
            BOOL inDeliveryHour = self.merchant.isDeliveryHour;
            if(self.closedLabel.hidden){ //Don't move to above clause
                self.closedLabel.hidden = inDeliveryHour;
            }
        }
        
//        PCLog(@"Intersect Operation Hours \n%@ from \n%@ and \n%@ of %@ (%d / %d)",
//              self.merchant.intersectHours,
//              [self.merchant operationHours],
//              [self.merchant deliveryHours],
//              self.merchant.name, self.merchant.autoOpenClose, self.merchant.autoDeliveryHours);
        
        if(!self.closedLabel.hidden){
            if(self.merchant.isOnlyDelivery){
                if([self.merchant.intersectHours count] > 0){
                    self.closedLabel.text = [self.merchant nextAvailableIntersectTime];
                } else {
                    self.closedLabel.text = NSLocalizedString(@"Closed", nil);
                }
            } else {
                if(self.merchant.nextAvailableTime == nil){
                    self.closedLabel.text = NSLocalizedString(@"Closed", nil);
                } else {
                    self.closedLabel.text = self.merchant.nextAvailableTime;
                }
            }
//            if(self.merchant.nearestOperationHour == nil){
//                if(self.merchant.nearestDeliveryOperationHour == nil){
//                    self.closedLabel.text = NSLocalizedString(@"Closed", nil);
//                } else {
//                    if(self.merchant.isOnlyDelivery){
//                        self.closedLabel.text = self.merchant.nextDeliveryAvailableHumanReadableTime;
//                    } else {
//                        self.closedLabel.text = NSLocalizedString(@"Closed", nil);
//                    }
//                }
//            } else {
//                if(self.merchant.isOnlyDelivery){
//                    NSComparisonResult rs = [self.merchant.nearestOperationHour compare:self.merchant.nearestDeliveryOperationHour];
//                    if(rs == NSOrderedAscending){
//                        self.closedLabel.text = self.merchant.nextAvailableTime;
//                    } else {
//                        self.closedLabel.text = self.merchant.nextDeliveryAvailableHumanReadableTime;
//                    }
//                } else {
//                    self.closedLabel.text = self.merchant.nextAvailableTime;
//                }
//            }
        }
        self.closedBottomView.hidden = self.closedLabel.hidden;
        
        [self.logoImageView sd_setImageWithURL:self.merchant.logoURL
                              placeholderImage:[UIImage imageNamed:@"placeholder_logo"]];
        
        self.backgroundImageView.hidden = YES;
        
        if(self.merchant.imageURL != nil){
            
            [self.backgroundImageView sd_setImageWithURL:self.merchant.imageURL
                                        placeholderImage:[UIImage imageNamed:@"placeholder_restaurantlist_m"]
                                                 options:0
                                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                   
                                                   if(image != nil && cacheType == SDImageCacheTypeNone){
                                                       self.backgroundImageView.hidden = NO;
                                                       self.backgroundImageView.alpha = 0.0;
                                                       [UIView animateWithDuration:0.5f
                                                                        animations:^{
                                                                            self.backgroundImageView.alpha = 1.0;
                                                                        }];
                                                       
                                                   } else {
                                                       self.backgroundImageView.hidden = (image == nil);
                                                   }
                                                   
                                               }];
        }
        
        ////////////
        [self.buttonsContainer removeAllSubviews];
        [self.buttonsContainer updateConstraints];
//        NSArray *containerConstraints = [self.buttonsContainer constraints];
//        [self.buttonsContainer removeConstraints:containerConstraints];
        
        NPStretchableButton *leftButton = nil;
        NPStretchableButton *middleButton = nil;
        NPStretchableButton *rightButton = nil;
        NPStretchableButton *soleButton = nil;
        

        if(self.merchant.isAbleTakeout && self.merchant.isAbleDelivery){
            if(self.merchant.isAbleDinein){
                // Dine-in , Takeout, Delivery
                
                [self.buttonsContainer addSubview:self.processButton];
                leftButton = self.processButton;
                [self.buttonsContainer addSubview:self.takeoutButton];
                middleButton = self.takeoutButton;
                [self.buttonsContainer addSubview:self.deliveryButton];
                rightButton = self.deliveryButton;
                
            } else {
                // Takeout, Delivery
                [self.buttonsContainer addSubview:self.takeoutButton];
                leftButton = self.takeoutButton;
                [self.buttonsContainer addSubview:self.deliveryButton];
                rightButton = self.deliveryButton;
                
            }
        } else if(self.merchant.isAbleTakeout){
            if(self.merchant.isAbleDinein){
                // Dine-in , Takeout
                [self.buttonsContainer addSubview:self.processButton];
                leftButton = self.processButton;
                [self.buttonsContainer addSubview:self.takeoutButton];
                rightButton = self.takeoutButton;
                
            } else {
                // Takeout
                [self.buttonsContainer addSubview:self.takeoutButton];
                soleButton = self.takeoutButton;
            }
            
        } else if(self.merchant.isAbleDelivery){
            if(self.merchant.isAbleDinein){
                // Dine-in, Delivery
                [self.buttonsContainer addSubview:self.processButton];
                [self.buttonsContainer addSubview:self.deliveryButton];
                leftButton = self.processButton;
                rightButton = self.deliveryButton;
            } else {
                // Delivery
                [self.buttonsContainer addSubview:self.deliveryButton];
                soleButton = self.deliveryButton;
            }
        } else {
            if(self.merchant.isAbleDinein){
                // Dine-in
                [self.buttonsContainer addSubview:self.processButton];
                soleButton = self.processButton;
                
            } else {
                //Error
                
            }
        }
        
        if(self.merchant.isOpenHour){
            [self.deliveryButton setTitleColor:[UIColor colorWithR:96.f G:103.f B:118.f]
                                      forState:UIControlStateNormal];
            [self.takeoutButton setTitleColor:[UIColor colorWithR:96.f G:103.f B:118.f]
                                     forState:UIControlStateNormal];
            [self.processButton setTitleColor:[UIColor colorWithR:96.f G:103.f B:118.f]
                                     forState:UIControlStateNormal];
            [self.deliveryButton setImage:[UIImage imageNamed:@"button_icon_delivery"]
                                 forState:UIControlStateNormal];
            [self.takeoutButton setImage:[UIImage imageNamed:@"button_icon_takeout"]
                                forState:UIControlStateNormal];
            [self.processButton setImage:[UIImage imageNamed:@"button_icon_dinein"]
                                forState:UIControlStateNormal];
        } else {
            [self.deliveryButton setTitleColor:[UIColor lightGrayColor]
                                      forState:UIControlStateNormal];
            [self.takeoutButton setTitleColor:[UIColor lightGrayColor]
                                     forState:UIControlStateNormal];
            [self.processButton setTitleColor:[UIColor lightGrayColor]
                                     forState:UIControlStateNormal];
            [self.deliveryButton setImage:[UIImage imageNamed:@"button_icon_delivery_g"]
                                forState:UIControlStateNormal];
            [self.takeoutButton setImage:[UIImage imageNamed:@"button_icon_takeout_g"]
                                forState:UIControlStateNormal];
            [self.processButton setImage:[UIImage imageNamed:@"button_icon_dinein_g"]
                                forState:UIControlStateNormal];
        }
        
        if(!self.merchant.isDeliveryHour){
            [self.deliveryButton setImage:[UIImage imageNamed:@"button_icon_delivery_g"]
                                 forState:UIControlStateNormal];
            [self.deliveryButton setTitleColor:[UIColor lightGrayColor]
                                      forState:UIControlStateNormal];
        }
        
        if(self.merchant.isTableBase){
            self.processButton.tag = CartTypeCart;
        } else {
            if(self.merchant.isServicedByStaff){
                self.processButton.tag = CartTypePickupServiced;
            } else {
                self.processButton.tag = CartTypePickup;
            }
        }

        
        NSMutableDictionary *viewsDictionary = [NSMutableDictionary dictionaryWithObject:self.buttonsContainer
                                                                                  forKey:@"buttonsContainer"];
        if(leftButton) [viewsDictionary setObject:leftButton forKey:@"leftButton"];
        if(middleButton) [viewsDictionary setObject:middleButton forKey:@"middleButton"];
        if(rightButton) [viewsDictionary setObject:rightButton forKey:@"rightButton"];
        if(soleButton) [viewsDictionary setObject:soleButton forKey:@"soleButton"];
        
        if (leftButton == nil && middleButton == nil && rightButton == nil && soleButton == nil){
            
        } else {
            
            NSMutableString *formatString = [NSMutableString string];
            [formatString appendString:@"|"];
            if(soleButton){
                [formatString appendString:@"-0-[soleButton]"];
            } else {
                if(leftButton){
                    [formatString appendString:@"-0-[leftButton]"];
                }
                if(middleButton){
                    [formatString appendString:@"-0-[middleButton(==leftButton)]"];
                }
                if(rightButton){
                    if(middleButton){
                        [formatString appendString:@"-0-"];
                    } else {
                        [formatString appendString:@"-(-1)-"];
                    }
                    [formatString appendString:@"[rightButton(==leftButton)]"];
                }
            }
            [formatString appendString:@"-0-|"];
            NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:formatString
                                                                           options:0
                                                                           metrics:nil
                                                                             views:viewsDictionary];
            [self.buttonsContainer addConstraints:constraints];
            
            if(soleButton){
                constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[soleButton]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:viewsDictionary];
                [self.buttonsContainer addConstraints:constraints];
            } else {
                if(leftButton){
                    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[leftButton]|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:viewsDictionary];
                    [self.buttonsContainer addConstraints:constraints];
                }
                if(middleButton){
                    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[middleButton]|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:viewsDictionary];
                    [self.buttonsContainer addConstraints:constraints];
                }
                if(rightButton){
                    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[rightButton]|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:viewsDictionary];
                    [self.buttonsContainer addConstraints:constraints];
                }
            }
            
            if(soleButton){
                [soleButton setBackgroundImage:[UIImage imageNamed:@"button_order"]
                                      forState:UIControlStateNormal];
                [soleButton setBackgroundImage:[UIImage imageNamed:@"button_order_h"]
                                      forState:UIControlStateHighlighted];
                [soleButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
                
            } else {
                if(leftButton){
                    [leftButton setBackgroundImage:[UIImage imageNamed:@"button_order_left"]
                                          forState:UIControlStateNormal];
                    [leftButton setBackgroundImage:[UIImage imageNamed:@"button_order_left_h"]
                                          forState:UIControlStateHighlighted];
                    [leftButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
                }
                if(middleButton){
                    [middleButton setBackgroundImage:[UIImage imageNamed:@"button_order_middle"]
                                            forState:UIControlStateNormal];
                    [middleButton setBackgroundImage:[UIImage imageNamed:@"button_order_middle_h"]
                                            forState:UIControlStateHighlighted];
                    [middleButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
                }
                if(rightButton){
                    [rightButton setBackgroundImage:[UIImage imageNamed:@"button_order_right"]
                                           forState:UIControlStateNormal];
                    [rightButton setBackgroundImage:[UIImage imageNamed:@"button_order_right_h"]
                                           forState:UIControlStateHighlighted];
                    [rightButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
                }
            }
        }
    }
    
    self.imnewImage.hidden = !((self.merchant.pickType & PickTypeNew) == PickTypeNew);
    
    [self.contentView updateConstraints];
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
}

- (IBAction)actionButtonTouched:(UIButton *)sender
{
    UITableView *tableView = self.tableView;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    
    if([tableView.delegate respondsToSelector:@selector(tableView:cell:didTouchActionButtonAtIndexPath:withCartType:)]){
        [((id <RestaurantCell1Delegate>)tableView.delegate) tableView:tableView
                                                                 cell:self
                                      didTouchActionButtonAtIndexPath:indexPath
                                                         withCartType:(CartType)sender.tag];
    }
}

@end
