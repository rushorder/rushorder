//
//  ReceiptItemCell.m
//  RushOrder
//
//  Created by Conan on 6/12/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "ReceiptItemCell.h"

@interface ReceiptItemCell()
@property (weak, nonatomic) IBOutlet UILabel *menuNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

@end

@implementation ReceiptItemCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)fillContents
{
    self.menuNameLabel.text = [self.lineItem displayMenuNameWithPrice:YES];
    self.amountLabel.text = [[NSNumber numberWithCurrency:(self.lineItem.unitPrice * self.lineItem.quantity)] currencyString];
}

- (void)setLineItem:(LineItem *)lineItem
{
    _lineItem = lineItem;
    [self fillContents];
}

@end
