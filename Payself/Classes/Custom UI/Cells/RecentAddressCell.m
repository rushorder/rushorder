//
//  RecentAddressCell.m
//  RushOrder
//
//  Created by Conan on 2/28/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import "RecentAddressCell.h"

@implementation RecentAddressCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)fillContent
{
//    self.addressLabel.text = self.addreses.address;
    self.addressLabel.text = self.deliveryAddress.shortAddressWithName;
}

@end
