//
//  PaymentInfoTableViewCell.m
//  RushOrder
//
//  Created by Conan on 9/26/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "PaymentInfoTableViewCell.h"

@implementation PaymentInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)receiptButtonTouched:(id)sender
{
    UITableView *tableView = self.tableView;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    if([tableView.delegate respondsToSelector:@selector(tableView:didTouchReceiptButtonAtIndexPath:)]){
        [(id<PaymentInfoTableViewCellDelegate>)tableView.delegate tableView:tableView
                                           didTouchReceiptButtonAtIndexPath:indexPath];
    }
}

- (IBAction)editButtonTouched:(id)sender
{
    UITableView *tableView = self.tableView;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    if([tableView.delegate respondsToSelector:@selector(tableView:didTouchEditButtonAtIndexPath:)]){
        [(id<PaymentInfoTableViewCellDelegate>)tableView.delegate tableView:tableView
                                           didTouchEditButtonAtIndexPath:indexPath];
    }
}

@end
