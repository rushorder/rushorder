//
//  CustomerRequestTableViewCell.h
//  RushOrder
//
//  Created by Conan on 9/26/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerRequestTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *customerRequestLabel;
@property (weak, nonatomic) IBOutlet UIView *editButton;

@end
