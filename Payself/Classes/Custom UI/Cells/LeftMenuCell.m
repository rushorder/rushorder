//
//  LeftMenuCell.m
//  RushOrder
//
//  Created by Conan on 5/24/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "LeftMenuCell.h"

@interface LeftMenuCell()


@end

@implementation LeftMenuCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
