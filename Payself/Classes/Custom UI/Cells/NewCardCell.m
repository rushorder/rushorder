//
//  NewCardCell.m
//  RushOrder
//
//  Created by Conan on 5/3/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "NewCardCell.h"

@implementation NewCardCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.textLabel.x = 31.0f;
    self.textLabel.y = 6.0f;
}

@end
