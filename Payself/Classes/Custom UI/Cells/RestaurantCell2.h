//
//  RestaurantCell2.h
//  RushOrder
//
//  Created by Conan on 8/12/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Merchant.h"

@protocol RestaurantCell2Delegate;

@interface RestaurantCell2 : UITableViewCell
@property (weak, nonatomic) IBOutlet id<RestaurantCell2Delegate> delegate;
@property (strong, nonatomic) Merchant *merchant;
- (void)drawData;
- (void)drawDataForHeight:(BOOL)forHeight;
@end

@protocol RestaurantCell2Delegate <NSObject>
- (void)tableView:(UITableView *)tableView
             cell:(RestaurantCell2 *)cell
didTouchActionButtonAtIndexPath:(NSIndexPath *)indexPath
     withCartType:(CartType)cartType;
@end