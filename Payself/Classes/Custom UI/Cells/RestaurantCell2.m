//
//  RestaurantCell2.m
//  RushOrder
//
//  Created by Conan on 8/12/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import "RestaurantCell2.h"
#import "CircleImageView.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface RestaurantCell2()
@property(weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet CircleImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *favoriteImageView;
@property (weak, nonatomic) IBOutlet UIImageView *panelImageView;
@property (weak, nonatomic) IBOutlet UILabel *closedLabel;
@property (weak, nonatomic) IBOutlet UIView *buttonsContainer;
@property (strong, nonatomic) IBOutlet NPStretchableButton *processButton;
@property (strong, nonatomic) IBOutlet NPStretchableButton *takeoutButton;
@property (strong, nonatomic) IBOutlet DoubleImageButton *deliveryButton;
@property (weak, nonatomic) IBOutlet UIImageView *distanceIcon;
@end

@implementation RestaurantCell2

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.processButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.takeoutButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.deliveryButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.buttonsContainer.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)drawData
{
    [self drawDataForHeight:NO];
}

- (void)drawDataForHeight:(BOOL)forHeight
{
//    self.deliveryButton.subImageOn = self.merchant.hasRoServiceFee; //Remove [SF] Service Fee Icon from All Screens Except for Checkout Screen
    
    if(self.merchant.isDemoMode || (APP.location == nil)){
        self.distanceLabel.hidden = YES;
        self.distanceIcon.hidden = YES;
        self.distanceLabel.text = nil;
    } else {
        self.distanceLabel.hidden = NO;
        self.distanceIcon.hidden = NO;
        self.distanceLabel.text = self.merchant.distanceString;
    }
    
    
    self.nameLabel.text = self.merchant.displayName;
//    [self.nameLabel updateConstraints];
    self.descLabel.text = self.merchant.desc;
    
    self.favoriteImageView.hidden = !self.merchant.isFavorite;
    self.closedLabel.hidden = self.merchant.isOpenHour;
    self.closedLabel.text = self.merchant.nextAvailableTime;
    self.panelImageView.hidden = self.closedLabel.hidden;
    
    self.distanceLabel.text = self.merchant.distanceString;
    
    if(!forHeight){
        [self.logoImageView sd_setImageWithURL:self.merchant.logoURL
                           placeholderImage:[UIImage imageNamed:@"placeholder_logo"]];
        
        self.backgroundImageView.contentMode = UIViewContentModeCenter;
        if(self.merchant.imageURL == nil){
            self.backgroundImageView.image = [UIImage imageNamed:@"placeholder_restaurantlist"];
        } else {
            [self.backgroundImageView sd_setImageWithURL:self.merchant.imageURL
                                     placeholderImage:[UIImage imageNamed:@"placeholder_restaurantlist"]
                                              options:0
                                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                if(image != nil){
                                                    self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
                                                } else {
                                                    self.backgroundImageView.contentMode = UIViewContentModeCenter;
                                                }
                                            }];
        }
        
        
        ////////////
        [self.buttonsContainer removeAllSubviews];
        [self.buttonsContainer updateConstraints];
        NSArray *containerConstraints = [self.buttonsContainer constraints];
//        [self.buttonsContainer removeConstraints:containerConstraints];
        
        NPStretchableButton *leftButton = nil;
        NPStretchableButton *middleButton = nil;
        NPStretchableButton *rightButton = nil;
        NPStretchableButton *soleButton = nil;
        
        if(self.merchant.menuOrder){
            if(self.merchant.isAbleTakeout && self.merchant.isAbleDelivery){
                if(self.merchant.isAbleDinein){
                    // Dine-in , Takeout, Delivery
                    
                    [self.buttonsContainer addSubview:self.processButton];
                    leftButton = self.processButton;
                    [self.buttonsContainer addSubview:self.takeoutButton];
                    middleButton = self.takeoutButton;
                    [self.buttonsContainer addSubview:self.deliveryButton];
                    rightButton = self.deliveryButton;
                    
                } else {
                    // Takeout, Delivery
                    [self.buttonsContainer addSubview:self.takeoutButton];
                    leftButton = self.takeoutButton;
                    [self.buttonsContainer addSubview:self.deliveryButton];
                    rightButton = self.deliveryButton;
                    
                }
            } else if(self.merchant.isAbleTakeout){
                if(self.merchant.isAbleDinein){
                    // Dine-in , Takeout
                    [self.buttonsContainer addSubview:self.processButton];
                    leftButton = self.processButton;
                    [self.buttonsContainer addSubview:self.takeoutButton];
                    rightButton = self.takeoutButton;
                    
                } else {
                    // Takeout
                    [self.buttonsContainer addSubview:self.takeoutButton];
                    soleButton = self.takeoutButton;
                }
                
            } else if(self.merchant.isAbleDelivery){
                if(self.merchant.isAbleDinein){
                    // Dine-in, Delivery
                    [self.buttonsContainer addSubview:self.processButton];
                    [self.buttonsContainer addSubview:self.deliveryButton];
                    leftButton = self.processButton;
                    rightButton = self.deliveryButton;
                } else {
                    // Delivery
                    [self.buttonsContainer addSubview:self.deliveryButton];
                    soleButton = self.deliveryButton;
                }
            } else {
                if(self.merchant.isAbleDinein){
                    // Dine-in
                    [self.buttonsContainer addSubview:self.processButton];
                    soleButton = self.processButton;
                    
                } else {
                    //Error
                    
                }
            }
            
            
            if(self.merchant.isTableBase){
                self.processButton.tag = CartTypeCart;
            } else {
                if(self.merchant.isServicedByStaff){
                    self.processButton.tag = CartTypePickupServiced;
                } else {
                    self.processButton.tag = CartTypePickup;
                }
            }
            
        } else {
            // Dine-in
            [self.buttonsContainer addSubview:self.processButton];
            soleButton = self.processButton;
        }
        
        NSMutableDictionary *viewsDictionary = [NSMutableDictionary dictionaryWithObject:self.buttonsContainer
                                                                                  forKey:@"buttonsContainer"];
        if(leftButton) [viewsDictionary setObject:leftButton forKey:@"leftButton"];
        if(middleButton) [viewsDictionary setObject:middleButton forKey:@"middleButton"];
        if(rightButton) [viewsDictionary setObject:rightButton forKey:@"rightButton"];
        if(soleButton) [viewsDictionary setObject:soleButton forKey:@"soleButton"];
        
        containerConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[buttonsContainer(==35@1000)]"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:viewsDictionary];
        [self.buttonsContainer addConstraints:containerConstraints];
        
        NSMutableString *formatString = [NSMutableString string];
        [formatString appendString:@"|"];
        if(soleButton){
            [formatString appendString:@"-0-[soleButton]"];
        } else {
            if(leftButton){
                [formatString appendString:@"-0-[leftButton]"];
            }
            if(middleButton){
                [formatString appendString:@"-0-[middleButton(==leftButton)]"];
            }
            if(rightButton){
                if(middleButton){
                    [formatString appendString:@"-0-"];
                } else {
                    [formatString appendString:@"-(-1)-"];
                }
                [formatString appendString:@"[rightButton(==leftButton)]"];
            }
        }
        [formatString appendString:@"-0-|"];
        NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:formatString
                                                                       options:0
                                                                       metrics:nil
                                                                         views:viewsDictionary];
        [self.buttonsContainer addConstraints:constraints];
        
        if(soleButton){
            constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[soleButton]|"
                                                                  options:0
                                                                  metrics:nil
                                                                    views:viewsDictionary];
            [self.buttonsContainer addConstraints:constraints];
        } else {
            if(leftButton){
                constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[leftButton]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:viewsDictionary];
                [self.buttonsContainer addConstraints:constraints];
            }
            if(middleButton){
                constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[middleButton]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:viewsDictionary];
                [self.buttonsContainer addConstraints:constraints];
            }
            if(rightButton){
                constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[rightButton]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:viewsDictionary];
                [self.buttonsContainer addConstraints:constraints];
            }
        }
        
        if(soleButton){
            [soleButton setBackgroundImage:[UIImage imageNamed:@"button_order"]
                                  forState:UIControlStateNormal];
            [soleButton setBackgroundImage:[UIImage imageNamed:@"button_order_h"]
                                  forState:UIControlStateHighlighted];
            [soleButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
            
        } else {
            if(leftButton){
                [leftButton setBackgroundImage:[UIImage imageNamed:@"button_order_left"]
                                      forState:UIControlStateNormal];
                [leftButton setBackgroundImage:[UIImage imageNamed:@"button_order_left_h"]
                                      forState:UIControlStateHighlighted];
                [leftButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
            }
            if(middleButton){
                [middleButton setBackgroundImage:[UIImage imageNamed:@"button_order_middle"]
                                        forState:UIControlStateNormal];
                [middleButton setBackgroundImage:[UIImage imageNamed:@"button_order_middle_h"]
                                        forState:UIControlStateHighlighted];
                [middleButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
            }
            if(rightButton){
                [rightButton setBackgroundImage:[UIImage imageNamed:@"button_order_right"]
                                       forState:UIControlStateNormal];
                [rightButton setBackgroundImage:[UIImage imageNamed:@"button_order_right_h"]
                                       forState:UIControlStateHighlighted];
                [rightButton applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
            }
        }
        
    }
}

- (IBAction)actionButtonTouched:(UIButton *)sender
{
    UITableView *tableView = self.tableView;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    
    if([tableView.delegate respondsToSelector:@selector(tableView:cell:didTouchActionButtonAtIndexPath:withCartType:)]){
        [((id <RestaurantCell2Delegate>)tableView.delegate) tableView:tableView
                                                                 cell:self
                                      didTouchActionButtonAtIndexPath:indexPath
                                                         withCartType:(CartType)sender.tag];
    }
}
@end
