//
//  LocalSearchTableViewCell.h
//  RushOrder
//
//  Created by Conan on 10/29/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocalSearchTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *subLabel;

@end
