//
//  CustomCell.h
//  RushOrder
//
//  Created by Conan on 12/13/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *badgeView;

- (void)setBadgeValue:(NSInteger)value;
@end
