//
//  DetailOrderMenuCell.h
//  RushOrder
//
//  Created by Conan on 5/27/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LineItem.h"

@interface DetailOrderMenuCell : UITableViewCell

@property (strong, nonatomic) LineItem *lineItem;
@property (weak, nonatomic) IBOutlet UILabel *menuLabel;
@end
