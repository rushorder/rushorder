//
//  OrangeCell.m
//  RushOrder
//
//  Created by Conan on 4/24/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "OrangeCell.h"

@implementation OrangeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)useButtonTouched:(NPToggleButton *)sender
{
    if(self.viewDelegate == nil){
        
        UITableView *tableView = self.tableView;
        
        NSIndexPath *indexPath = [tableView indexPathForCell:self];
        if([tableView.delegate respondsToSelector:@selector(tableView:toggleButtonTouched:forRowAtIndexPath:)]){
            [(id<OrangeCellDelegate>)tableView.delegate tableView:tableView
                                              toggleButtonTouched:sender
                                                forRowAtIndexPath:indexPath];
        }
    } else {
        
        [self.viewDelegate orangeCelltoggleButtonTouched:sender];
    }
}

@end
