//
//  DiscountTableViewCell.h
//  RushOrder
//
//  Created by Conan on 5/23/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Promotion.h"

@protocol DiscountTableViewCellDelegate;

@interface DiscountTableViewCell : UITableViewCell

@property (weak, nonatomic) id<DiscountTableViewCellDelegate> delegate;
@property (strong, nonatomic) Promotion *promotion;

- (void)fillContent;
@end

@protocol DiscountTableViewCellDelegate <UITableViewDelegate>
- (void)discountTableViewCell:(DiscountTableViewCell *)cell
didTouchRemoveButtonAtIndexPath:(NSIndexPath *)indexPath;
@end
