//
//  PersonTableViewCell.m
//  RushOrder
//
//  Created by Conan on 8/21/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import "PersonTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation PersonTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)drawData
{
//    self.nameLabel.text = self.person.name;
//    [self.photoImageView setImageWithURL:self.person.photo];
//    self.statusButton.hidden = self.person.isMine;
//
//    if(self.person.isDone){
//        [self.statusButton setBackgroundImage:[UIImage imageNamed:@"button_bg_orange"]
//                                     forState:UIControlStateNormal];
//        [self.statusButton applyResizableImageFromCenter];
//        [self.statusButton setTitle:@"Done"
//                           forState:UIControlStateNormal];
//    } else {
        [self.statusButton setBackgroundImage:[UIImage imageNamed:@"button_bg_green_small"]
                                     forState:UIControlStateNormal];
        [self.statusButton applyResizableImageFromCenter];
        [self.statusButton setTitle:@"Ordering"
                           forState:UIControlStateNormal];
//    }
}

@end
