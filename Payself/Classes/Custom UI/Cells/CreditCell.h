//
//  CreditCell.h
//  RushOrder
//
//  Created by Conan on 4/24/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreditCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *creditBalaceLabel;
@property (weak, nonatomic) IBOutlet UILabel *useCreditLabel;
@property (weak, nonatomic) IBOutlet UISlider *useCreditSlider;
@end


@protocol CreditCellDelegate<UITableViewDelegate>
- (void)tableView:(UITableView *)tableView sliderValueChange:(UISlider *)slider forRowAtIndexPath:(NSIndexPath *)indexPath;
@end