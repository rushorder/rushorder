//
//  ReceiptItemCell.h
//  RushOrder
//
//  Created by Conan on 6/12/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LineItem.h"

@interface ReceiptItemCell : UITableViewCell

@property (strong, nonatomic) LineItem *lineItem;
@end
