//
//  CustomCell.m
//  RushOrder
//
//  Created by Conan on 12/13/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "CustomCell.h"

@interface CustomCell()
@property (weak, nonatomic) IBOutlet UILabel *badgeLabel;
@end

@implementation CustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.badgeView.layer.cornerRadius = 12.5f;
    self.badgeView.clipsToBounds = true;
    self.badgeView.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setBadgeValue:(NSInteger)value
{
    if(value > 0){
        self.badgeView.hidden = NO;
        self.badgeLabel.text = [NSString stringWithFormat:@"%ld", (long)value];
    } else {
        self.badgeView.hidden = YES;
    }
}
@end
