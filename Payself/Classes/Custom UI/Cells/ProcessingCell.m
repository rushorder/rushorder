//
//  ProcessingCell.m
//  RushOrder
//
//  Created by Conan on 6/10/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "ProcessingCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MerchantViewController.h"
#import "OrderManager.h"
#import <Smooch/Smooch.h>
#import "PCCredentialService.h"

typedef enum processingCellActionButtonType_{
    ProcessingCellActionButtonTypeNone = 0,
    ProcessingCellActionButtonTypeCheckout,
    ProcessingCellActionButtonTypeClear,
    ProcessingCellActionButtonTypeAdjust,
    ProcessingCellActionButtonTypeCancel,
} ProcessingCellActionButtonType;

@interface ProcessingCell()

@property (weak, nonatomic) IBOutlet NPStretchableButton *step1Button;
@property (weak, nonatomic) IBOutlet NPStretchableButton *step2Button;
@property (weak, nonatomic) IBOutlet NPStretchableButton *step3Button;
@property (weak, nonatomic) IBOutlet NPStretchableButton *step4Button;

@property (weak, nonatomic) IBOutlet UILabel *tableNoTitle;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet NPStretchableButton *checkoutButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *cancelOrderButton;

@property (weak, nonatomic) IBOutlet UILabel *thankyouLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *expectedTimeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *expectedTimeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *expectedTimeTopConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *disclosureIndicatorImageView;
@property (weak, nonatomic) IBOutlet UIImageView *declinedIconImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *declinedIconRightMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *declinedIconWidthContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cancelOrderTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cancelOrderHeightConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *testIcon;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *button2EqualConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *button3EqualConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *button2WidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *button3WidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *button2LeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *button3LeadingConstraint;

@end

@implementation ProcessingCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.checkoutButton applyResizableImageFromCenterForState:UIControlStateNormal];
    self.thumbnailImageView.target = self;
}

- (void)circleImageViewActionTriggerred:(id)sender
{
    Merchant *merchant = nil;
    if([self.orderOrCart isKindOfClass:[Order class]]){
        Order *order = (Order *)self.orderOrCart;
        merchant = order.merchant;
    } else if([self.orderOrCart isKindOfClass:[Cart class]]){
        Cart *cart = (Cart *)self.orderOrCart;
        merchant = cart.merchant;
    }
    
    [ORDER resetGraphWithMerchant:merchant];
    MerchantViewController *viewController = [UIStoryboard viewController:@"MerchantViewController"
                                                                     from:@"Restaurants"];
    [APP.tabBarController presentViewControllerInNavigation:viewController
                                                   animated:YES
                                                 completion:^{
                                                     
                                                 }];
}

- (void)touchableLabelActionTriggerred:(id)sender
{
    [self circleImageViewActionTriggerred:sender];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)fillContents
{
    return [self fillContentsForHeight:NO];
}

- (void)fillContentsForHeight:(BOOL)forHeight
{
    NSURL *imgURL = nil;
    
    NSString *itemsString = nil;
    
    self.thankyouLabel.textColor = [UIColor c11Color];
    
    self.thankyouLabel.font = [UIFont systemFontOfSize:17.0f];
    self.declinedIconImageView.image = nil;
    self.declinedIconRightMarginConstraint.constant = 0;
    self.declinedIconWidthContraint.constant = 0;
    
    if([self.orderOrCart isKindOfClass:[Order class]]){
        Order *order = (Order *)self.orderOrCart;
        self.merchantNameLabel.text = order.merchantName;
        self.testIcon.hidden = !order.isTestMode;
        self.orderTypeIcon.image = order.orderTypeIcon;
        self.restaurantTypeLabel.text = order.typeString;
        self.itemNameLabel.text = order.lineItemNames;
        
        self.addressLabel.text = order.merchant.displayMultilineAddress;

        if(order.orderDate == nil){
            self.dateLabel.text = order.createdDate.minutesString;
            self.timeTitleLabel.text = NSLocalizedString(@"Created Time", nil);
        } else {
            self.dateLabel.text = order.orderDate.minutesString;
            self.timeTitleLabel.text = NSLocalizedString(@"Order Time", nil);
        }
        
        if(order.status == OrderStatusDeclined){
            self.guideLabel.text = order.rejectReason;
            self.guideLabel.textColor = [UIColor c14Color];
        } else {
            self.guideLabel.text = [order statusGuideStringWithMerchant:order.merchant];
            self.guideLabel.textColor = [UIColor c11Color];
        }
        
        if(order.status <= OrderStatusDraft){
            self.thankyouLabel.text = NSLocalizedString(@"Ordering", nil);
        } else {
            if((order.status & (OrderStatusDeclined | OrderStatusCanceled)) > 0){
                if(order.status == OrderStatusDeclined){
                    self.thankyouLabel.text = NSLocalizedString(@"Declined", nil);
                } else if(order.status == OrderStatusCanceled){
                    self.thankyouLabel.text = NSLocalizedString(@"Removed Order", nil);
                }
                self.thankyouLabel.textColor = [UIColor c14Color];
                self.declinedIconImageView.image = [UIImage imageNamed:@"icon_declined"];
                self.declinedIconRightMarginConstraint.constant = 10;
                self.declinedIconWidthContraint.constant = 37.0f;
                self.thankyouLabel.font = [UIFont systemFontOfSize:24.0f];
            } else {
                self.thankyouLabel.text = NSLocalizedString(@"Thank you!", nil);
            }
        }
        [self textStepLabelWithType:order.orderType
                         isDeclined:(order.status == OrderStatusDeclined)
                        isCancelled:(order.status == OrderStatusCanceled)
                         isServiced:([order.tableNumber length] > 0)
                   isNonInteractive:order.isTwoStepProcess];
        
        itemsString = order.lineItemNames;
        
        if(order.isTicketBase){
            
            if([order.tableNumber length] > 0){
                self.tableNumberLabel.text = order.tableNumber;
                self.tableNoTitle.text = NSLocalizedString(@"Table No.", nil);
            } else {
                self.tableNumberLabel.text = [NSString stringWithLongLong:order.orderNo];
                self.tableNoTitle.text = NSLocalizedString(@"Ticket No.", nil);
            }
            
            if(order.orderType == OrderTypeDelivery || order.orderType == OrderTypeTakeout){
                if((order.status & (OrderStatusConfirmed | OrderStatusReady | OrderStatusFixed | OrderStatusCompleted)) == order.status){
                    switch(order.orderType){
                        case OrderTypeDelivery:
                            self.expectedTimeTitleLabel.text = NSLocalizedString(@"Estimated Delivery Time", nil);
                            break;
                        case OrderTypeTakeout:
                            self.expectedTimeTitleLabel.text = NSLocalizedString(@"Estimated Pickup Time", nil);
                            break;
                        default:
                            break;
                    }
                    [self showExpectedTime:order.expectedDate];
                } else {
                    [self showExpectedTime:nil];
                }
            } else {
                [self showExpectedTime:nil];
            }
            
            if(order.isTwoStepProcess){
//                self.readyButtonWidthConstraint.constant = 0.0f;
                self.button2EqualConstraint.priority = 500;
                self.button2WidthConstraint.priority = 999;
                self.button3EqualConstraint.priority = 500;
                self.button3WidthConstraint.priority = 999;
                self.button2LeadingConstraint.constant = 0.0f;
                self.button3LeadingConstraint.constant = 0.0f;
            } else {
//                self.readyButtonWidthConstraint.constant = 75.0f;
                self.button2EqualConstraint.priority = 999;
                self.button2WidthConstraint.priority = 500;
                self.button3EqualConstraint.priority = 999;
                self.button3WidthConstraint.priority = 500;
                self.button2LeadingConstraint.constant = -4.0f;
                self.button3LeadingConstraint.constant = -4.0f;
            }
            
            switch(order.status){
                case OrderStatusDraft:
                    [self paintStepLabelWithStep:0];
                    [self showButton:YES type:ProcessingCellActionButtonTypeCheckout];
                    break;
                case OrderStatusPaid:
                    [self paintStepLabelWithStep:1];
                    
                    if(order.didTimedOut){
                        [self showButton:YES type:ProcessingCellActionButtonTypeClear];
                    } else {
                        [self hideButton];
                    }
                    break;
                case OrderStatusConfirmed:
                case OrderStatusAccepted:
                case OrderStatusCompleted: // This is not completed status like on the server. - server completed is 'Fixed'
                    if(order.isTwoStepProcess){
                        [self paintStepLabelWithStep:1];
                    } else {
                        [self paintStepLabelWithStep:2];
                    }
                    if(order.didTimedOut){
                        [self showButton:YES type:ProcessingCellActionButtonTypeClear];
                    } else {
                        [self hideButton];
                    }
                    break;
                case OrderStatusDeclined:
                case OrderStatusRefused:
                    if(order.isTwoStepProcess){
                        [self paintStepLabelWithStep:4];
                    } else {
                        [self paintStepLabelWithStep:2];
                    }
                    if(order.needToAdjustment){
                        [self showButton:YES type:ProcessingCellActionButtonTypeAdjust];
                    } else {
                        [self showButton:YES type:ProcessingCellActionButtonTypeCancel];
                    }
                    break;
                case OrderStatusReady:
                case OrderStatusPrepared:
                    [self paintStepLabelWithStep:3];
                    [self showButton:NO type:ProcessingCellActionButtonTypeNone];
                    
                    if(order.didTimedOut){
                        [self showButton:YES type:ProcessingCellActionButtonTypeClear];
                    } else {
                        [self hideButton];
                    }
                    break;
                case OrderStatusCanceled:
                case OrderStatusFixed:
                    [self paintStepLabelWithStep:4];
                    [self showButton:YES type:ProcessingCellActionButtonTypeClear];
                    break;
                default:
                    [self paintStepLabelWithStep:0];
                    if(order.didTimedOut){
                        [self showButton:YES type:ProcessingCellActionButtonTypeClear];
                    } else {
                        [self hideButton];
                    }
                    break;
            }
        } else {
            // Dine-in (waiter service)
            [self showExpectedTime:nil];
            
            self.tableNoTitle.text = NSLocalizedString(@"Table No.", nil);
            
            if(order.tableNumber != nil){
                self.tableNumberLabel.text = order.tableNumber;
            } else {
                self.tableNumberLabel.text = @"";
            }
            
            switch(order.status){
                case OrderStatusDraft:
                    [self paintStepLabelWithStep:2];
                    if(order.didTimedOut){
                        [self showButton:YES type:ProcessingCellActionButtonTypeClear];
                    } else {
                        [self hideButton];
                    }
                    break;
                case OrderStatusConfirmed:
                case OrderStatusAccepted:
                    if(order.orderType == OrderTypeOrderOnly){
                        if(order.didTimedOut){
                            [self showButton:YES type:ProcessingCellActionButtonTypeClear];
                        } else {
                            [self hideButton];
                        }
                    } else {
                        [self showButton:YES type:ProcessingCellActionButtonTypeCheckout];
                    }
                    [self paintStepLabelWithStep:3];
                    break;
                case OrderStatusDeclined:
                case OrderStatusRefused:
                    [self paintStepLabelWithStep:3];
                    [self showButton:YES type:ProcessingCellActionButtonTypeAdjust];
                    if(order.needToAdjustment){
                        [self showButton:YES type:ProcessingCellActionButtonTypeAdjust];
                    } else {
                        [self showButton:YES type:ProcessingCellActionButtonTypeCancel];
                    }
                    break;
                case OrderStatusCompleted:
                case OrderStatusFixed:
                case OrderStatusCanceled:
                    [self paintStepLabelWithStep:4];
                    [self showButton:YES type:ProcessingCellActionButtonTypeClear];
                    break;
                default:
                    [self paintStepLabelWithStep:0];
                    if(order.didTimedOut){
                        [self showButton:YES type:ProcessingCellActionButtonTypeClear];
                    } else {
                        [self hideButton];
                    }
                    break;
            }
        }
        
        self.amountLabel.text = order.grandTotalString;
        self.payAmountLabel.text = order.cardAmountString;
        
        imgURL = order.merchant.logoURL;
        if(imgURL == nil) imgURL = order.merchant.imageURL;
        
    } else if([self.orderOrCart isKindOfClass:[Cart class]]){
        
        [self showExpectedTime:nil];
        
        Cart *cart = (Cart *)self.orderOrCart;
        self.merchantNameLabel.text = cart.merchantName;
        self.testIcon.hidden = YES;
        self.orderTypeIcon.image = cart.cartTypeIcon;
        self.restaurantTypeLabel.text = cart.typeString;
        self.itemNameLabel.text = cart.lineItemNames;
        self.addressLabel.text = cart.merchant.address;
        
        itemsString = cart.lineItemNames;
        
        self.dateLabel.text = cart.createdDate.minutesString;
        
        self.guideLabel.text = [cart statusGuideString];
        
        if(cart.isLocal){ //Maybe pickup cart
            
            self.thankyouLabel.text = NSLocalizedString(@"Ordering", nil);
            
            self.timeTitleLabel.text = NSLocalizedString(@"Created Time", nil);
            
            if(cart.cartType == CartTypePickupServiced){
                [self textStepLabelWithType:OrderTypePickup isServiced:YES isNonInteractive:cart.isNonInteractive];
            } else {
                [self textStepLabelWithType:OrderTypePickup isNonInteractive:cart.isNonInteractive];
            }
            
            self.tableNumberLabel.text = NSLocalizedString(@"Not Placed", nil);
            
            if(cart.cartType == CartTypePickupServiced){
                self.tableNoTitle.text = NSLocalizedString(@"Table No.", nil);
            } else {
                self.tableNoTitle.text = NSLocalizedString(@"Ticket No.", nil);
            }
            
            if(cart.isNonInteractive){
//                self.readyButtonWidthConstraint.constant = 0.0f;
                self.button2EqualConstraint.priority = 500;
                self.button2WidthConstraint.priority = 999;
                self.button3EqualConstraint.priority = 500;
                self.button3WidthConstraint.priority = 999;
                self.button2LeadingConstraint.constant = 0.0f;
                self.button3LeadingConstraint.constant = 0.0f;
            } else {
//                self.readyButtonWidthConstraint.constant = 75.0f;
                self.button2EqualConstraint.priority = 999;
                self.button2WidthConstraint.priority = 500;
                self.button3EqualConstraint.priority = 999;
                self.button3WidthConstraint.priority = 500;
                self.button2LeadingConstraint.constant = -4.0f;
                self.button3LeadingConstraint.constant = -4.0f;
            }
            
            switch(cart.status){
                case CartStatusOrdering:
                    [self paintStepLabelWithStep:0];
                    [self showButton:YES type:ProcessingCellActionButtonTypeCheckout];
                    break;
                case CartStatusSubmitted:
                    [self paintStepLabelWithStep:1];
                    [self hideButton];
                    break;
                default:
                    [self paintStepLabelWithStep:0];
                    [self hideButton];
                    break;
            }
            
        } else {
            
            [self textStepLabelWithType:OrderTypeCart isNonInteractive:cart.isNonInteractive];
            
            if([cart.tableLabel length] > 0){
                self.tableNumberLabel.text = cart.tableLabel;
                self.tableNoTitle.text = NSLocalizedString(@"Table No.", nil);
            } else {
                self.tableNumberLabel.text = @"";
                self.tableNoTitle.text = NSLocalizedString(@"Ticket No.", nil);
            }
           
            if(cart.status <= CartStatusOrdering){
                self.thankyouLabel.text = NSLocalizedString(@"Ordering", nil);
                self.timeTitleLabel.text = NSLocalizedString(@"Created Time", nil);
            } else {
                self.thankyouLabel.text = NSLocalizedString(@"Thank you!", nil);
                self.timeTitleLabel.text = NSLocalizedString(@"Order Time", nil);
            }
            
            switch(cart.status){
                case CartStatusOrdering:
                    [self paintStepLabelWithStep:1];
                    [self hideButton];
                    break;
                case CartStatusSubmitted:
                    [self paintStepLabelWithStep:2];
                    [self hideButton];
                    break;
                default:
                    [self paintStepLabelWithStep:0];
                    [self hideButton];
                    break;
            }
        }
        
        self.amountLabel.text = cart.subtotalAmountString; //Subtotal(total) + tax
        self.payAmountLabel.text = NSLocalizedString(@"--", nil); //Subtotal(total) + tax
        
        self.payAmountLabel.text = nil;
        
        imgURL = cart.merchant.logoURL;
        if(imgURL == nil) imgURL = cart.merchant.imageURL;
    }
    
    self.payAmountTopConstraint.constant = (self.payAmountLabel.text == nil) ? 0.0f : 4.0f;
    self.payAmountTitleLabel.hidden = (self.payAmountLabel.text == nil);
    
    if(!forHeight){
        if(imgURL != nil){
            
            [self.thumbnailImageView sd_setImageWithURL:imgURL
                                       placeholderImage:[UIImage imageNamed:@"placeholder_logo"]
                                                options:0];
        } else {
            self.thumbnailImageView.image = [UIImage imageNamed:@"placeholder_logo"];
        }
    }
}

- (void)showExpectedTime:(NSDate *)expectedTime
{
    if(expectedTime == nil){
        self.expectedTimeTitleLabel.text = nil;
        self.expectedTimeLabel.text = nil;
        self.expectedTimeTopConstraint.constant = 0.0f;
    } else {
//        self.expectedTimeTitleLabel.text = NSLocalizedString(@"Expected Ready Time", nil);
        self.expectedTimeLabel.text = [expectedTime hourMinuteString];
        self.expectedTimeTopConstraint.constant = 3.0f;
    }
}

- (void)hideButton
{
    [self showButton:NO type:ProcessingCellActionButtonTypeNone];
}

- (void)showButton:(BOOL)isShow type:(ProcessingCellActionButtonType)type
{
    self.checkoutButton.hidden = !isShow;
    self.checkoutButton.tag = type;
    if(type == ProcessingCellActionButtonTypeAdjust){
        self.disclosureIndicatorImageView.hidden = NO;
    } else {
        self.disclosureIndicatorImageView.hidden = isShow;
    }
    self.buttonHeightConstraint.constant = isShow ? 44.0f : 0.0f;
    self.buttonTopConstraint.constant = isShow ? 12.0f : 0.0f;
    
    [self.checkoutButton setBackgroundImage:[UIImage imageNamed:@"button_bg_orange"]
                                   forState:UIControlStateNormal];
    
    self.cancelOrderTopConstraint.constant = 0.0f;
    self.cancelOrderHeightConstraint.constant = 0.0f;
    self.cancelOrderButton.hidden = YES;
    
    
    if(isShow){
        switch(type){
            case ProcessingCellActionButtonTypeCheckout:
                self.checkoutButton.buttonTitle = NSLocalizedString(@"Check Out", nil);
                break;
            case ProcessingCellActionButtonTypeClear:
                self.checkoutButton.buttonTitle = NSLocalizedString(@"Complete Order", nil);
                break;
            case ProcessingCellActionButtonTypeAdjust:
                self.cancelOrderTopConstraint.constant = 8.0f;
                self.cancelOrderHeightConstraint.constant = 44.0f;
                self.cancelOrderButton.hidden = NO;
                
                self.checkoutButton.buttonTitle = NSLocalizedString(@"Update Existing Order", nil);
                break;
            case ProcessingCellActionButtonTypeCancel:
                self.checkoutButton.buttonTitle = NSLocalizedString(@"Ok. Got it!", nil);
                [self.checkoutButton setBackgroundImage:[UIImage imageNamed:@"button_bg_green_small"]
                                               forState:UIControlStateNormal];
                break;
            default:
                self.checkoutButton.buttonTitle = NSLocalizedString(@"Error", nil);
                break;
        }
    }
    [self.checkoutButton applyResizableImageFromCenter];
}


- (void)textStepLabelWithType:(OrderType)orderType isNonInteractive:(BOOL)isNonInteractive
{
    [self textStepLabelWithType:orderType isDeclined:NO isCancelled:NO isServiced:NO isNonInteractive:isNonInteractive];
}

- (void)textStepLabelWithType:(OrderType)orderType isServiced:(BOOL)isServiced isNonInteractive:(BOOL)isNonInteractive
{
    [self textStepLabelWithType:orderType isDeclined:NO isCancelled:NO isServiced:isServiced isNonInteractive:isNonInteractive];
}

- (void)textStepLabelWithType:(OrderType)orderType
                   isDeclined:(BOOL)isDeclined
                  isCancelled:(BOOL)isCancelled
                   isServiced:(BOOL)isServiced
             isNonInteractive:(BOOL)isNonInteractive
{
    
    switch(orderType){
        case OrderTypeBill:
            self.step1Button.buttonTitle = NSLocalizedString(@"Requested", nil);
            self.step2Button.buttonTitle = NSLocalizedString(@"Issued", nil);
            break;
        default:
            
            if(orderType == OrderTypeCart || orderType == OrderTypeOrderOnly){
                self.step1Button.buttonTitle = NSLocalizedString(@"Submitted", nil);
            } else {
                self.step1Button.buttonTitle = NSLocalizedString(@"Placed", nil);
            }
            self.step2Button.buttonTitle = NSLocalizedString(@"Preparing", nil);
            break;
    }
    
    switch(orderType){
        case OrderTypeBill:
            self.step1Button.buttonTitle = NSLocalizedString(@"Requested", nil);
            self.step2Button.buttonTitle = NSLocalizedString(@"Issued", nil);
            break;
        case OrderTypeTakeout:
            self.step3Button.buttonTitle = NSLocalizedString(@"Ready", nil);
            self.step4Button.buttonTitle = NSLocalizedString(@"Picked Up", nil);
            break;
        case OrderTypeDelivery:
            self.step3Button.buttonTitle = NSLocalizedString(@"Picked Up", nil);
            self.step4Button.buttonTitle = NSLocalizedString(@"Delivered", nil);
            break;
        case OrderTypePickup:
            
            if(isServiced){
                self.step3Button.buttonTitle = NSLocalizedString(@"Serving", nil);
                self.step4Button.buttonTitle = NSLocalizedString(@"Completed", nil);
            } else {
                self.step3Button.buttonTitle = NSLocalizedString(@"Ready", nil);
                self.step4Button.buttonTitle = NSLocalizedString(@"Picked Up", nil);
            }
            
            break;
        case OrderTypeOrderOnly:
            self.step3Button.buttonTitle = NSLocalizedString(@"Ready", nil);
            self.step4Button.buttonTitle = NSLocalizedString(@"Completed", nil);
            break;
        default:
            self.step3Button.buttonTitle = NSLocalizedString(@"Payable", nil);
            self.step4Button.buttonTitle = NSLocalizedString(@"Completed", nil);
            break;
    }
    
    [self.step2Button setBackgroundImage:[UIImage imageNamed:@"progressbar_activeorder_02_red"]
                                forState:UIControlStateSelected];
    [self.step3Button setBackgroundImage:[UIImage imageNamed:@"progressbar_activeorder_02_red"]
                                forState:UIControlStateSelected];
    [self.step4Button setBackgroundImage:[UIImage imageNamed:@"progressbar_activeorder_03_red"]
                      forState:UIControlStateSelected];
    
    if(isNonInteractive){
        if(isDeclined){
            self.step4Button.buttonTitle = NSLocalizedString(@"Declined", nil);
            [self.step4Button setBackgroundImage:[UIImage imageNamed:@"progressbar_activeorder_03_declined"]
                                        forState:UIControlStateSelected];
        } else if(isCancelled){
            self.step4Button.buttonTitle = NSLocalizedString(@"Removed", nil);
            [self.step4Button setBackgroundImage:[UIImage imageNamed:@"progressbar_activeorder_03_declined"]
                                        forState:UIControlStateSelected];
        } else {
            self.step4Button.buttonTitle = NSLocalizedString(@"Confirmed", nil);
        }
    } else {
        if(isDeclined){
            self.step2Button.buttonTitle = NSLocalizedString(@"Declined", nil);
            [self.step2Button setBackgroundImage:[UIImage imageNamed:@"progressbar_activeorder_02_declined"]
                                        forState:UIControlStateSelected];
        } else if(isCancelled){
            self.step3Button.buttonTitle = NSLocalizedString(@"Removed", nil);
            [self.step3Button setBackgroundImage:[UIImage imageNamed:@"progressbar_activeorder_02_declined"]
                                        forState:UIControlStateSelected];
        }
    }
    
    [self.step1Button applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateSelected];
    [self.step2Button applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateSelected];
    [self.step3Button applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateSelected];
    [self.step4Button applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateSelected];
    
    [self.step1Button setNeedsDisplay];
    [self.step2Button setNeedsDisplay];
    [self.step3Button setNeedsDisplay];
    [self.step4Button setNeedsDisplay];
}

- (void)paintStepLabelWithStep:(NSInteger)step
{
    self.step1Button.selected = step >= 1;
    self.step2Button.selected = step >= 2;
    self.step3Button.selected = step >= 3;
    self.step4Button.selected = step >= 4; //안양 4동 중앙성당
}

- (IBAction)checkoutButtonTouched:(UIButton *)sender
{
    NSIndexPath *selfIndexPath = [self.tableView indexPathForCell:self];
    
    if(sender.tag == ProcessingCellActionButtonTypeClear){
        if([self.delegate respondsToSelector:@selector(tableView:cell:didTouchClearButtonAtIndexPath:)]){
            [self.delegate tableView:self.tableView
                                cell:self
      didTouchClearButtonAtIndexPath:selfIndexPath];
        }
    } else if(sender.tag == ProcessingCellActionButtonTypeAdjust){
        if([self.delegate respondsToSelector:@selector(tableView:cell:didTouchProcessButtonAtIndexPath:)]){
            [self.delegate tableView:self.tableView
                                cell:self
    didTouchProcessButtonAtIndexPath:selfIndexPath];
        }
    } else if(sender.tag == ProcessingCellActionButtonTypeCancel){
        if([self.delegate respondsToSelector:@selector(tableView:cell:didTouchGotItButtonAtIndexPath:)]){
            [self.delegate tableView:self.tableView
                                cell:self
     didTouchGotItButtonAtIndexPath:selfIndexPath];
        }
    } else if(sender.tag == ProcessingCellActionButtonTypeCheckout){
        if([self.delegate respondsToSelector:@selector(tableView:cell:didTouchProcessButtonAtIndexPath:)]){
            [self.delegate tableView:self.tableView
                                cell:self
    didTouchProcessButtonAtIndexPath:selfIndexPath];
        }
    }
}

- (IBAction)callHelpLineButtonTouched:(id)sender
{
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://424-488-3170"]]){
        if(![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://424-488-3170"]]){
//            User's cancel
        }
    } else {
        [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to Call",nil)
                                     message:NSLocalizedString(@"Try to call manually", nil)];
    }
}

- (IBAction)textHelpLineButtonTouched:(id)sender
{
    if(CRED.isSignedIn){
        [SKTUser currentUser].firstName = CRED.customerInfo.firstName;
        [SKTUser currentUser].lastName = CRED.customerInfo.lastName;
        [SKTUser currentUser].email = CRED.customerInfo.email;
        [[SKTUser currentUser] addProperties:@{ @"Customer" : [NSString stringWithFormat:@"https://www.rushorderapp.com/customers/customers/%lld", CRED.customerInfo.customerNo]
                                                , @"phoneNumber" : CRED.customerInfo.phoneNumber ? CRED.customerInfo.phoneNumber : @"Unknown"
                                                , @"birthday" : CRED.customerInfo.birthday ? CRED.customerInfo.birthday : @"Unknown"
                                                , @"UUID": [NSString stringWithFormat:@"https://www.rushorderapp.com/douglas/guests/%@", CRED.udid]
                                                }];
    } else {
        [SKTUser currentUser].firstName = [NSUserDefaults standardUserDefaults].custName ? [NSUserDefaults standardUserDefaults].custName : @"?";
        [SKTUser currentUser].lastName = @"Guest";
        [SKTUser currentUser].email = [NSUserDefaults standardUserDefaults].emailAddress;
        [[SKTUser currentUser] addProperties:@{ @"phoneNumber" : [NSUserDefaults standardUserDefaults].custPhone ? [NSUserDefaults standardUserDefaults].custPhone : @"Unknown"
                                                , @"UUID": [NSString stringWithFormat:@"https://www.rushorderapp.com/douglas/guests/%@", CRED.udid]
                                                }];
    }
    
    if([self.orderOrCart isKindOfClass:[Order class]]){
        Order *order = (Order *)self.orderOrCart;
        
        [[SKTUser currentUser] addProperties:@{ @"Merchant" : order.merchantName
                                                ,@"Order" : [NSString stringWithFormat:@"https://www.rushorderapp.com/douglas/orders/%lld", order.orderNo]
                                                }];
    }
    
    
    [Smooch show];
}

- (IBAction)cancelOrderButtonTouched:(id)sender
{
    NSIndexPath *selfIndexPath = [self.tableView indexPathForCell:self];
    
    if([self.delegate respondsToSelector:@selector(tableView:cell:didTouchCancelButtonAtIndexPath:)]){
        [self.delegate tableView:self.tableView
                            cell:self
 didTouchCancelButtonAtIndexPath:selfIndexPath];
    }
}
@end
