//
//  MobileCardCell.h
//  RushOrder
//
//  Created by Conan on 2/22/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MobileCardCellDelegate;

@interface MobileCardCell : UITableViewCell

@property (weak, nonatomic) id<MobileCardCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *neoCardLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

@property (weak, nonatomic) IBOutlet NPToggleButton *checkMarkView;
@end

@protocol MobileCardCellDelegate <UITableViewDelegate>
- (void)tableView:(UITableView *)tableView
             cell:(MobileCardCell *)cell
didTouchActionButtonAtIndex:(NSIndexPath *)indexPath;
@end
