//
//  PersonTableViewCell.h
//  RushOrder
//
//  Created by Conan on 8/21/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NPStretchableButton.h"

@interface PersonTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet CircleImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet NPStretchableButton *statusButton;

- (void)drawData;
@end
