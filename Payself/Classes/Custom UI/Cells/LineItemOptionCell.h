//
//  LineItemOptionCell.h
//  RushOrder
//
//  Created by Conan on 7/8/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LineItemOptionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lineItemOptionNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lineItemOptionPriceLabel;
@property (weak, nonatomic) IBOutlet UIView *lineView;

@property (strong, nonatomic) NSDictionary *optionDict;

@end
