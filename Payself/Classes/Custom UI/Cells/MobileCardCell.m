//
//  MobileCardCell.m
//  RushOrder
//
//  Created by Conan on 2/22/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MobileCardCell.h"

@interface MobileCardCell()


@end

@implementation MobileCardCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)actionButtonTouched:(id)sender
{
    UITableView *tableView = self.tableView;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    
    if([tableView.delegate respondsToSelector:@selector(tableView:cell:didTouchActionButtonAtIndex:)]){
        [((id <MobileCardCellDelegate>)tableView.delegate) tableView:tableView cell:self didTouchActionButtonAtIndex:indexPath];
    }
}

@end
