//
//  CreditAuthCell.h
//  RushOrder
//
//  Created by Conan on 3/5/14.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CreditAuthCellDelegate;

@interface CreditAuthCell : UITableViewCell
@property (weak, nonatomic) id<CreditAuthCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *creditBalaceLabel;
@end


@protocol CreditAuthCellDelegate<NSObject>
- (void)creditAuthCell:(CreditAuthCell *)cell didTouchedVerifyButton:(id)sender;
@end