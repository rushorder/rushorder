//
//  DetailOrderMenuCell.m
//  RushOrder
//
//  Created by Conan on 5/27/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "DetailOrderMenuCell.h"

@interface DetailOrderMenuCell()

@property (weak, nonatomic) IBOutlet UILabel *listAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *qtyLabel;
@property (weak, nonatomic) IBOutlet UIImageView *paidImageView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *specialInstructionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *talkIcon;
@property (weak, nonatomic) IBOutlet UIView *lineView;

@end

@implementation DetailOrderMenuCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.indentationWidth = 0;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)fillContents
{
    self.menuLabel.text = self.lineItem.displayMenuName;
    
//    if(self.lineItem.selectedPrice != nil){
        self.listAmountLabel.text = [[NSNumber numberWithCurrency:(self.lineItem.unitPrice * self.lineItem.quantity)] currencyString];
//    } else {
//        self.listAmountLabel.text = self.lineItem.lineAmountString;
//    }
    
    
    self.qtyLabel.text = nil;//self.lineItem.serializedOptionNames;
    self.specialInstructionLabel.text = self.lineItem.specialInstruction;
    if([self.lineItem.specialInstruction length] > 0){
        self.talkIcon.image = [UIImage imageNamed:@"icon_comment"];
    } else {
        self.talkIcon.image = nil;
    }
    
    if([self.lineItem.menuOptionNamesObj count] > 0){
        self.lineView.hidden = YES;
    } else {
        self.lineView.hidden = NO;
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.paidImageView.hidden = !self.lineItem.isPaid;
    
//    self.menuLabel.enabled = !(self.lineItem.isSplitExcept || self.lineItem.isPaid);
//    self.listAmountLabel.enabled = self.menuLabel.enabled;
//    self.qtyLabel.enabled = self.menuLabel.enabled;
}

- (void)setLineItem:(LineItem *)lineItem
{
    _lineItem = lineItem;
    [self fillContents];
}

@end
