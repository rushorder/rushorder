//
//  AddressTableViewCell.h
//  RushOrder
//
//  Created by Conan on 8/19/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@end
