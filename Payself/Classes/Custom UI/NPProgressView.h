//
//  NPProgressView.h
//  RushOrder
//
//  Created by Conan Kim on 11. 10. 5..
//  Copyright 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    NPProgressViewStyleSmall,
    NPProgressViewStyleLarge,
    NPProgressViewStyleNone
} NPProgressViewStyle;

@interface NPProgressView : UIProgressView {
    UIImage *gaugeBackImage;
    UIImage *gaugeImage;
    
    CGFloat validWidth;
} 

- (id)initWithFrame:(CGRect)frame withStyle:(NPProgressViewStyle)progressViewStyle;
- (void)resetProgress:(CGFloat)val;
@end