//
//  NPStretchableButton.m
//  RushOrder
//
//  Created by Conan on 12/21/12.
//  Copyright (c) 2012 RushOrder. All rights reserved.
//

#import "NPStretchableButton.h"

@implementation NPStretchableButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if (self) {
//        [self commonInit];
    }
    return self;
}

- (void)commonInit
{    
    if(self.highlightedImage){
        [self applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
    } else {
        [self applyResizableImageFromCenterForState:UIControlStateNormal];
    }
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    if(self.highlightedImage){
        [self applyResizableImageFromCenterForState:UIControlStateNormal | UIControlStateHighlighted];
    } else {
        [self applyResizableImageFromCenterForState:UIControlStateNormal];
    }
}

@end
