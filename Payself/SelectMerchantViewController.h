//
//  SelectMerchantViewController.h
//  RushOrder
//
//  Created by Conan on 2/13/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectMerchantViewController : PCViewController <UITableViewDelegate, UITableViewDataSource>

@end
