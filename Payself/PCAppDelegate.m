//
//  PCAppDelegate.m
//  RushOrder
//
//  Created by Conan on 2/13/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCAppDelegate.h"
#import "BillViewController.h"
#import "PCPaymentService.h"
#import "Order.h"
#import "Cuisine.h"
#import "Merchant.h"
#import "TablesetViewController.h"
#import "BillViewController.h"
#import "MenuPageViewController.h"
#import "TogoAuthViewController.h"
#import "BillRequest.h"
#import <AudioToolbox/AudioToolbox.h>
#import "PCCredentialService.h"
#import "PassCodeViewController.h"
#import "PCNavigationController.h"
#import "OrderManager.h"
#import "MenuManager.h"
#import "TransactionManager.h"
#import "NoticeViewController.h"
#import "ReceiptViewController.h"
#import "ReachabilityService.h"
#import "SelectCardViewController.h"
#import "RemoteDataManager.h"
#import <GoogleMaps/GoogleMaps.h>
#import "RestaurantViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Bolts/Bolts.h>
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>
#import <Crashlytics/Crashlytics.h>
#import <DigitsKit/DigitsKit.h>
#import <Smooch/Smooch.h>
#import <Google/Analytics.h>
#import "MainTabbarController.h"
#import "RushOrder-Swift.h"

@import Firebase;

#define MIXPANEL_TOKEN @"5431ac4db17ce6eecb05cb77ebf85a6b"

#ifdef FLURRY_ENABLED
void uncaughtExceptionHandler(NSException *exception) {
    [Flurry logError:@"Uncaught" message:@"Crash!" exception:exception];
}
#endif

NSString * const locationUpdatedNotificationKey = @"locationUpdatedNotificationKey";
NSString * const OrderConfirmedNotificationKey = @"OrderConfirmedNotificationKey";
NSString * const CartItemUpdatedNotificationKey = @"CartItemUpdatedNotificationKey";

typedef enum moveAlertType_
{
    MoveAlertTypeBill = 3,
    MoveAlertTypeReady = 4,
} MoveAlertType;


@interface PCAppDelegate()
{
    BOOL _didCheckedMyBillRequests;
    
    SystemSoundID soundFileObject;
    SystemSoundID silentSoundFileObject;
}

@property (strong, nonatomic) NoticeViewController *noticeViewController;
@property (strong, nonatomic) NSDictionary *lastPushUserInfo;
@property (strong, nonatomic) Order *pushedOrder;
@property (strong, nonatomic) Merchant *pushedMerchant;
@property (strong, nonatomic) NSMutableArray *myBillRequestList;

@property (strong, nonatomic) IBOutlet UIView *eventContainer;
@property (strong, nonatomic) IBOutlet UILabel *eventLabel;

@property (nonatomic) PCSerial noticeNo;

@property (nonatomic) BOOL locationAlerted;

@end

@implementation PCAppDelegate

//@synthesize managedObjectContext = _managedObjectContext;
//@synthesize managedObjectModel = _managedObjectModel;
//@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

+ (BOOL)isRetina
{
    return ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale >= 2.0))?1:0;
}

+ (void)initialize
{
    NSDictionary *appDefaults = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [NSNumber numberWithBool:NO], testDriveModeDefaultsKey,
                                 [NSNumber numberWithBool:NO], demoOnDefaultsKey,
                                 [NSNumber numberWithBool:YES], adoptLastTipRate,
                                 [NSNumber numberWithBool:YES], adoptLastTakeoutTipRate,
                                 [NSNumber numberWithBool:YES], adoptLastDeliveryTipRate,
                                 nil];
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    PCLog(@"%@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);

    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
#if DEBUG
//    self.location = [[CLLocation alloc] initWithLatitude:34.062670
//                                               longitude:-118.375091];
        self.location = [[CLLocation alloc] initWithLatitude:34.0598774
                                                   longitude:-118.301307];
#endif
    
    MainTabbarController *tabBarController = (MainTabbarController *)self.window.rootViewController;
    
//    [tabBarController showIntroView];
    
//    [[Crashlytics sharedInstance] setDebugMode:YES];
    [Fabric with:@[[Digits class], [Twitter class], [Crashlytics class]]];

    SKTSettings *setting = [SKTSettings settingsWithAppToken:@"49bsn5m1h4i5zab0c9xeobuut"];
    setting.conversationStatusBarStyle = UIStatusBarStyleLightContent;
    
    [Smooch initWithSettings:setting];
    
    
    [SVProgressHUD setBackgroundColor:[UIColor colorWithWhite:0.0f
                                                        alpha:0.25f]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:2.0f];
    [NSUserDefaults standardUserDefaults].didAlertPushNotification = NO;
    
#if DEBUG
    NSString *payselfUUID = [CRED.payselfUUID objectForKey:(__bridge id)(kSecValueData)];
//    NSString *payselfUUID = nil;
//    NSString *payselfUUID = @"2925A31EBA534EF7AB8E9A5A6F574D4E"; //For testing
#else
    NSString *payselfUUID = [CRED.payselfUUID objectForKey:(__bridge id)(kSecValueData)];
#endif
    
    if([payselfUUID length] == 0){
        
        //Set to keychain
        payselfUUID = [[[NSUUID UUID] UUIDString] stringByReplacingOccurrencesOfString:@"-"
                                                                            withString:@""];
        [CRED.payselfUUID setObject:payselfUUID
                             forKey:(__bridge id)(kSecValueData)];
    }
    
    serviceUUID = payselfUUID;
    clientAppVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    CRED.udid = payselfUUID;
    
    [ReachabilityService sharedService];
    
    // Configure UINavigationBar appearance
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor c12Color], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0f], NSFontAttributeName,
      [NSValue valueWithCGPoint:CGPointMake(0.0f, 0.0f)], NSShadowAttributeName,nil]];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    // Application Appearance
    self.window.tintColor = [UIColor c11Color];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [UINavigationBar appearance].barTintColor = [UIColor c11Color];
    [UINavigationBar appearance].tintColor = [UIColor whiteColor];
    [UINavigationBar appearance].titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
//    [UITabBar appearance].barTintColor = [UIColor whiteColor];
    ////////////////////////////////////////////////////////////////////////////
    
//    [UINavigationBar appearance].backIndicatorImage = [UIImage imageNamed:@"button_back_orange"];
//    [UINavigationBar appearance].backIndicatorTransitionMaskImage = [UIImage imageNamed:@"button_back_orange"];
    
    UIImage *backImage = nil;
    if(OVER_IOS7){
        backImage = [UIImage imageNamed:@"bg_navigationbar"];
    } else {
        backImage = [UIImage imageNamed:@"bg_navigationbar_u7"];
    }
    
    [[UINavigationBar appearance] setBackgroundImage:backImage
                                              forBarMetrics:UIBarMetricsDefault];
    [self startApp];
    
    PCLog(@"serviceUUID : %@", serviceUUID);
    PCLog(@"clientAppVersion : %@", clientAppVersion);
    
    _didCheckedMyBillRequests = NO;
    
#if DEBUG
    [[PCLogger shared] setLogLevel:LevelLog];
#else
    [[PCLogger shared] setLogLevel:LevelError];
#endif

    
#if TARGET_IPHONE_SIMULATOR
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    userDefault.deviceToken = @"Test token";
#else
    UIUserNotificationSettings *settings = [UIUserNotificationSettings
                                            settingsForTypes:UIUserNotificationTypeBadge |
                                            UIUserNotificationTypeSound |
                                            UIUserNotificationTypeAlert
                                            categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
#endif
    [self requestConfiguration];
    
    [self startLocationUpdates];
    
    CFBundleRef mainBundle = CFBundleGetMainBundle ();
    
    // Get the URL to the sound file to play. The file in this case
    // is "tap.aif"
    CFURLRef soundFileURLRef  = CFBundleCopyResourceURL (
                                                         mainBundle,
                                                         CFSTR ("doorchime"),
                                                         CFSTR ("wav"),
                                                         NULL
                                                         );
    
    // Create a system sound object representing the sound file
    AudioServicesCreateSystemSoundID (
                                      soundFileURLRef,
                                      &soundFileObject
                                      );
    
    // Get the URL to the sound file to play. The file in this case
    // is "tap.aif"
    CFURLRef silentSoundFileURLRef  = CFBundleCopyResourceURL (
                                                         mainBundle,
                                                         CFSTR ("silentchime"),
                                                         CFSTR ("wav"),
                                                         NULL
                                                         );
    
    // Create a system sound object representing the sound file
    AudioServicesCreateSystemSoundID (
                                      silentSoundFileURLRef,
                                      &silentSoundFileObject
                                      );
    
#define CUSTOM_URL_SCHEME @"frbrushorderapp"
    
    [FIROptions defaultOptions].deepLinkURLScheme = CUSTOM_URL_SCHEME;
    [FIRApp configure];
    
#if FIREBASE_ENABLED
    
    [FIRAnalytics setUserPropertyString:serviceUUID forName:@"UUID"];
    
    
    
    
    GAI *gai = [GAI sharedInstance];
    [gai trackerWithTrackingId:@"UA-42625126-1"];
    
    // Optional: automatically report uncaught exceptions.
    gai.trackUncaughtExceptions = YES;
    
#if DEBUG
    // Optional: set Logger to VERBOSE for debug information.
    // Remove before app release.
    gai.logger.logLevel = kGAILogLevelVerbose;
#endif
#endif
    
    
#if FBAPPEVENT_ENABLED
    [FBSDKAppEvents setUserID:serviceUUID];
    
    NSDictionary *user_props = [[NSDictionary alloc] initWithObjectsAndKeys:serviceUUID, @"UUID", nil];
    
    [FBSDKAppEvents updateUserProperties:user_props
                                 handler:^void(FBSDKGraphRequestConnection *connection,
                                               id result,
                                               NSError *error) {PCLog(@"FBAppEventProperty %@",[error localizedDescription]);}];
    
#endif

    
#ifdef FLURRY_ENABLED
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    [Flurry setCrashReportingEnabled:YES];
    [Flurry setAppVersion:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    [Flurry setSessionContinueSeconds:30];
    [Flurry startSession:@"JD2C79F3GC6WQJW8C8RW"];

    [Flurry setUserID:serviceUUID];
    
#endif
    
    [self.window makeKeyAndVisible];
    
    if(![self showPasscodeView:NO]){

    }
    

    if([CRED generateUserAccount]){
        PCLog(@"Customer generated in successfully");
    } else {
        PCLog(@"Customer generated failed");
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(transactionListChanged:)
                                                 name:TransactionListChangedNotification
                                               object:nil];
    
    
    ////////////////////////////////////////////////////////////////////////////
    
    // Tab bar viewController settings
    self.tabBarController = tabBarController;
    self.tabBarController.delegate = self;
//    for(UITabBarItem *tabBarItem in tabBarController.tabBar.items){
//        tabBarItem.selectedImage = [tabBarItem.selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    }
    
#ifdef FLURRY_ENABLED
    [Flurry logAllPageViewsForTarget:tabBarController];
#endif
    
    ((UITabBarItem *)tabBarController.tabBar.items[0]).selectedImage = [UIImage imageNamed:@"icon_restaurant_tab_bar"];
    ((UITabBarItem *)tabBarController.tabBar.items[1]).selectedImage = [UIImage imageNamed:@"icon_tabbar_rapidro_h"];
    ((UITabBarItem *)tabBarController.tabBar.items[3]).selectedImage = [UIImage imageNamed:@"icon_tabbar_orders_h"];
    ((UITabBarItem *)tabBarController.tabBar.items[4]).selectedImage = [UIImage imageNamed:@"icon_tabbar_settings_h"];
    
    // Restaurant View Controller
    UINavigationController *aNavigationController = [[tabBarController viewControllers] objectAtIndex:0];
    [aNavigationController setViewControllers:@[[UIStoryboard initialViewControllerFrom:@"Restaurants"]]];


    // RapidRO View Controller
    aNavigationController = [[tabBarController viewControllers] objectAtIndex:1];
    [aNavigationController setViewControllers:@[[UIStoryboard viewController:@"RapidRO"
                                                                        from:@"Orders"]]];
    
    // RapidRO View Controller
    aNavigationController = [[tabBarController viewControllers] objectAtIndex:2];
    [aNavigationController setViewControllers:@[[UIStoryboard initialViewControllerFrom:@"explore"]]];
    
    // Order View Controller
    aNavigationController = [[tabBarController viewControllers] objectAtIndex:3];
    [aNavigationController setViewControllers:@[[UIStoryboard initialViewControllerFrom:@"Orders"]]];
    
    // Setting View Controller
    aNavigationController = [[tabBarController viewControllers] objectAtIndex:4];
    [aNavigationController setViewControllers:@[[UIStoryboard initialViewControllerFrom:@"Settings"]]];
    ////////////////////////////////////////////////////////////////////////////
    
    [NSUserDefaults standardUserDefaults].autoMailingMe = YES;
    
    if(![NSUserDefaults standardUserDefaults].didUploadRecentAddresses){
        [self updateRecentAddresses];
    }
    
    return YES;
}

- (void)tabBarController:(UITabBarController *)tabBarController
 didSelectViewController:(UIViewController *)viewController
{
    static UIViewController *previousController = nil;
    UIViewController *targetViewController = [[(UINavigationController *)viewController viewControllers] objectAtIndex:0];
    if (previousController == targetViewController) {
        // the same tab was tapped a second time
        if ([targetViewController respondsToSelector:@selector(scrollToTop)]) {
            [targetViewController performSelector:@selector(scrollToTop)];
        }
    }
    previousController = targetViewController;
}

- (void)updateRecentAddresses
{
    NSMutableArray *addresses = [Addresses listAll];
    
    if([addresses count] == 0){
        [NSUserDefaults standardUserDefaults].didUploadRecentAddresses = YES;
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    
    NSMutableArray *arrayForJSON = [NSMutableArray array];
    NSInteger i = 0;
    
    for(Addresses *anAddresses in addresses){
        [arrayForJSON addObject:[anAddresses dictRep]];
        i++;
        if(i >= 5) break;
    }
    
    RequestResult result = RRFail;

    NSString *name = [NSUserDefaults standardUserDefaults].custName;
    NSString *phoneNumber = [NSUserDefaults standardUserDefaults].custPhone;
    
    if(CRED.isSignedIn){
        name = CRED.customerInfo.name;
        phoneNumber = CRED.customerInfo.phoneNumber;
    }
    
    result = [PAY requestUpdateRecentDeliveryAddress:arrayForJSON
                                                name:name
                                         phoneNumber:phoneNumber
                                     completionBlock:^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode) {
                                         if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                                             switch(response.errorCode){
                                                 case ResponseSuccess:{
                                                     [NSUserDefaults standardUserDefaults].didUploadRecentAddresses = YES;
                                                     [[NSUserDefaults standardUserDefaults] synchronize];
                                                     [REMOTE handleDeliveryAddressesResponse:(NSMutableArray *)response
                                                                            withNotification:YES];
                                                     
                                                 }
                                                     break;
                                                 default:
                                                 {
                                                     NSString *message = [response objectForKey:@"message"];
                                                     NSString *title = [response objectForKey:@"title"];
                                                     if(title == nil){
                                                         [UIAlertView alert:message];
                                                     } else {
                                                         [UIAlertView alertWithTitle:title
                                                                             message:message];
                                                     }
                                                 }
                                                     break;
                                             }
                                         } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                                             [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                                             [CRED resetUserAccount];
                                         } else {
                                             if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                                                 [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                                                     object:nil];
                                             } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                                                 [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                                                     ,statusCode]];
                                             }
                                         }
                                         [SVProgressHUD dismiss];
                                     }];
    
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            [SVProgressHUD dismiss];
            break;
        default:
            [SVProgressHUD dismiss];
            break;
    }
}

- (void)startApp
{
    [CRED requestAppStartWithCompletionBlock:
     ^(BOOL isSuccess, NSArray *response, NSInteger statusCode){
         if(isSuccess && 200 <= statusCode && statusCode < 300){
             if(response.errorCode == ResponseSuccess){
                 
                 self.appStarted = YES;
                 self.cuisines = [NSMutableArray array];
                 
                 Cuisine *allCuisine = [[Cuisine alloc] init];
                 allCuisine.no = 0;
                 allCuisine.name = NSLocalizedString(@"All Cuisines", nil);
                 allCuisine.selected = YES;
                 [self.cuisines addObject:allCuisine];
                 
                 for(NSDictionary *dict in response){
                     Cuisine *cuisine = [[Cuisine alloc] initWithDictionary:dict];
                     cuisine.selected = NO;
                     [self.cuisines addObject:cuisine];
                 }
             }
         }
     }];
}
- (void)transactionListChanged:(NSNotification *)noti
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = CRED.customerInfo.referralBadgeCount;
}

//- (BOOL)viewDeckController:(IIViewDeckController*)viewDeckController shouldOpenViewSide:(IIViewDeckSide)viewDeckSide
//{
//    if(viewDeckSide == IIViewDeckLeftSide){
//        PCNavigationController *navi = (PCNavigationController *)viewDeckController.centerController;
//        if([navi.viewControllers count] == 1){
//            return YES;
//        } else {
//            return NO;
//        }
//    } else {
//        return YES;
//    }
//}
//
//- (IIViewDeckController *)viewDeckController
//{
//    return (IIViewDeckController *)self.window.rootViewController;
//}

- (void)didPassCodeAuthorized:(PassCodeViewController *)passCodeViewController
{
    [self.window makeKeyAndVisible];
}

- (void) vibratePhone {
    AudioServicesPlaySystemSound (kSystemSoundID_Vibrate);
}

- (IBAction) playSystemSound {
    AudioServicesPlaySystemSound (soundFileObject);
}

- (IBAction) playSilentSound {
    AudioServicesPlaySystemSound (silentSoundFileObject);
}

- (void)applicationWillResignActive:(UIApplication *)application
{

#ifdef FLURRY_ENABLED
    [Flurry endTimedEvent:@"ResignActive"
            withParameters:nil];
#endif
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    [self showPasscodeView:NO];
}

- (BOOL)showPasscodeView:(BOOL)animated
{
    if(CRED.isPinSet){
        PassCodeViewController *viewController = [PassCodeViewController viewControllerFromNib];
        viewController.passMode = YES;
        viewController.delegate = self;
        viewController.modal = YES;
        viewController.passCodeMode = PassCodeModeRun;
        PCNavigationController *navigationController = [[PCNavigationController alloc] initWithRootViewController:viewController];
        if(self.window.rootViewController.presentedViewController != nil){
            [self.window.rootViewController dismissViewControllerAnimated:NO
                                                               completion:^{
                                                                   
                                                               }];
        }
        [self.window.rootViewController presentViewController:navigationController
                                                     animated:animated
                                                   completion:^(){
                                                   }];
        return YES;
    }
    return NO;
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    _didCheckedMyBillRequests = NO;
    TRANS.dirty = YES;
    CRED.dirtyCredit = YES;
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    if(!self.appStarted){
        [self startApp];
    }
    
    [self requestConfiguration];

    CRED.unattendedSignUp = NO; // Reset for safety
    
    BOOL isNotificationOn = YES;
    
    isNotificationOn = [[UIApplication sharedApplication] isRegisteredForRemoteNotifications];
    

    if (!isNotificationOn){
        if(![NSUserDefaults standardUserDefaults].didAlertPushNotification){
            [UIAlertView alertWithTitle:NSLocalizedString(@"Push Notification Service is Off.", nil)
                                message:NSLocalizedString(@"RushOrder uses push notification service to notify order and payment progress for you. Please turn on it in Settings→Notifications", nil)];
        }
        PCWarning(@"User doesn't want to receive push-notifications");
        [NSUserDefaults standardUserDefaults].didAlertPushNotification = YES;
    } else {
        [NSUserDefaults standardUserDefaults].didAlertPushNotification = NO;
    }
}

- (void)checkNotice
{
    PCLog(@"========================== checking Notice!!!! ==========================");
    
    //Notice
    [PAY requestNoticeWithCompletionBlock:
     ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode) {
         if(response.errorCode == ResponseSuccess){
             
             self.noticeNo = [response serialForKey:@"id"];
             NSString *format = [response objectForKey:@"format"];
             NSString *content = [response objectForKey:@"contents"];
             
             if([NSUserDefaults standardUserDefaults].lastAccessedNoticeId < self.noticeNo){
                 if([format isEqual:@"text"]){
                     
//                     self.eventLabel.text = content;
//                     [self.eventLabel sizeToHeightFit];
//                     
//                     CGFloat containerHeight = CGRectGetMaxY(self.eventLabel.frame) + 20.0f;
//                     
//                     self.eventContainer.frame = CGRectMake(0.0f,
//                                                            self.window.frame.size.height - containerHeight,
//                                                            self.self.eventContainer.width,
//                                                            containerHeight);
//                     
//                     [self.window addSubview:self.eventContainer];
                     
                 } else {
                     
                     self.noticeViewController = [NoticeViewController viewControllerFromNib];
                     self.noticeViewController.noticeNo = self.noticeNo;
                     self.noticeViewController.format = format;
                     self.noticeViewController.content = content;
                     
                     [self.tabBarController presentViewControllerInNavigation:self.noticeViewController
                                                                       animated:YES
                                                                     completion:^{

                                                                     }
                      
                      ];
                 }
             }
         }
     }];
}

- (IBAction)eventCloseButtonTouched:(id)sender
{
    [NSUserDefaults standardUserDefaults].lastAccessedNoticeId = self.noticeNo;
    
    [UIView animateWithDuration:0.35f
                     animations:^{
                         self.eventContainer.y = 600.0f;
                     } completion:^(BOOL finished) {
                         [self.eventContainer removeFromSuperview];
                         self.eventLabel = nil;
                         self.eventContainer = nil;
                     }];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Logs 'install' and 'app activate' App Events.
    [FBSDKAppEvents activateApp];
    [TRANS requestMyOrders];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.

//    [self saveContext];
}

//- (void)saveContext
//{
//    NSError *error = nil;
//    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
//    if (managedObjectContext != nil) {
//        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
//             // Replace this implementation with code to handle the error appropriately.
//             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
//            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//            abort();
//        }
//    }
//}

#pragma mark - Push notification related
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSMutableString *deviceId = [NSMutableString string];
    
    const unsigned char* ptr = (const unsigned char*) [deviceToken bytes];
    
    for(int i = 0 ; i < 32 ; i++)  {
        [deviceId appendFormat:@"%02x", ptr[i]];
    }
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    PCLog(@"Push Token %@", deviceId);

    if([userDefault.deviceToken isEqualToString:deviceId]){
        // Keep going
    } else {
        userDefault.deviceToken = deviceId;
        [userDefault synchronize];
        if(CRED.customerInfo != nil){
            [CRED requestUpdateDeviceToken:userDefault.deviceToken
                           completionBlock:^(BOOL isSuccess, id response, NSInteger statusCode) {
                               if(isSuccess && 200 <= statusCode && statusCode < 300){
                                   
                               } else {
                                   
                               }
                           }];
        }
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    PCError(@"APNS Device Token Error : %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    PCLog(@"[APN] userInfo:%@", userInfo);
    
    NSString *pushTypeStr = [userInfo objectForKey:@"type"];
    if(pushTypeStr == nil){
        AudioServicesPlaySystemSound(1004);
        [self vibratePhone];
        
    } else {
        NSInteger pushType = [pushTypeStr integerValue];
        
        if(pushType == 12){
            [self playSilentSound];
        } else {
            [self playSystemSound];
        }
        
        switch(pushType){
            case 2: // Bill issued
            case 3: // Order confirmed
            case 8: // TOGO confirm
            case 9: // Pickup Ready
            case 10: // Cart ordering
            case 11: // Cart submitted
            case 13: // Cart removed
            case 14: // Payment
            case 15: // Completed (Without pay)
                TRANS.dirty = YES;
                [[NSNotificationCenter defaultCenter] postNotificationName:OrderConfirmedNotificationKey
                                                                    object:self
                                                                  userInfo:userInfo];
                break;
            case 21: // Order declined
                TRANS.dirty = YES;
                [[NSNotificationCenter defaultCenter] postNotificationName:OrderConfirmedNotificationKey
                                                                    object:self
                                                                  userInfo:userInfo];
                break;
            case 22:
                // TODO : Refunded Handling
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"CreditAwardedNotifications"
                                                                    object:self];
                
                break;
            case 12: // Cart item edited
                [[NSNotificationCenter defaultCenter] postNotificationName:CartItemUpdatedNotificationKey
                                                                    object:self
                                                                  userInfo:userInfo];
                
                break;
            case 1: // Bill requested (for merchant app)
            case 4: // Payment (for merchant app)
            case 5: // Cart submitted (for merchant app)
            case 6: // Cart ordering (for merchant app)
            case 7: // TOGO payment (for merchant app)
                // Do nonthing because this is not merchant application
                break;
            case 30:{
                // CREDIT AWARDED
                //                    NSString *referrer = [userInfo objectForKey:@"referrer"];
                //                    NSInteger creditAmount = [userInfo integerForKey:@"amount"];
                
                NSDictionary *appDict = [userInfo objectForKey:@"aps"];
                [UIAlertView alertWithTitle:[appDict objectForKey:@"alert"] message:nil];
                
                CRED.dirtyCredit = YES;
                
                [CRED requestProfileWithCompletionBlock:^(BOOL isSuccess, id response, NSInteger statusCode) {
                    if(CRED.customerInfo.referralBadgeCount > 0){
                        ((UITabBarItem *)APP.tabBarController.tabBar.items[4]).badgeValue = [NSString stringWithFormat:@"%lu",CRED.customerInfo.referralBadgeCount];
                    } else {
                        ((UITabBarItem *)APP.tabBarController.tabBar.items[4]).badgeValue = nil;
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"CreditAwardedNotifications"
                                                                        object:self];
                }];
            }
                break;
            default:
                // Do nothing
                break;
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
//- (NSManagedObjectContext *)managedObjectContext
//{
//    if (_managedObjectContext != nil) {
//        return _managedObjectContext;
//    }
//    
//    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
//    if (coordinator != nil) {
//        _managedObjectContext = [[NSManagedObjectContext alloc] init];
//        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
//    }
//    return _managedObjectContext;
//}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
//- (NSManagedObjectModel *)managedObjectModel
//{
//    if (_managedObjectModel != nil) {
//        return _managedObjectModel;
//    }
//    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Payself" withExtension:@"momd"];
//    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
//    return _managedObjectModel;
//}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
//- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
//{
//    if (_persistentStoreCoordinator != nil) {
//        return _persistentStoreCoordinator;
//    }
//    
//    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Payself.sqlite"];
//    
//    NSError *error = nil;
//    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
//    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
//        /*
//         Replace this implementation with code to handle the error appropriately.
//         
//         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
//         
//         Typical reasons for an error here include:
//         * The persistent store is not accessible;
//         * The schema for the persistent store is incompatible with current managed object model.
//         Check the error message to determine what the actual problem was.
//         
//         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
//         
//         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
//         * Simply deleting the existing store:
//         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
//         
//         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
//         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
//         
//         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
//         
//         */
//        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//        abort();
//    }
//    
//    return _persistentStoreCoordinator;
//}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Location service
- (void)startLocationUpdates
{
    // Create the location manager if this object does not
    // already have one.
    if (nil == self.locationManager){
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        // Set a movement threshold for new events.
        self.locationManager.distanceFilter = 30;

    }
    
    [self.locationManager requestWhenInUseAuthorization];
    
    //RushOrder needs your current location to work properly. Please allow access by going to your device's Settings→Privacy→Location Services→RushOrder, and then select 'While Using the App'. You're the best!
    if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
        [self stopLocationUpdates];
        if(!self.locationAlerted){
            self.locationAlerted = YES;
//            [UIAlertView alertWithTitle:NSLocalizedString(@"Enable Location Services", nil)
//                                message:NSLocalizedString(@"Please go to Settings→Privacy→Location Services→RushOrder, and then select 'While Using The App'", nil)
//                               delegate:self
//                                    tag:406];
            
            [UIAlertView alertWithTitle:NSLocalizedString(@"Location Services Required", nil)
                                message:NSLocalizedString(@"RushOrder needs to know your location in order to show you nearby restaurants. Please allow Location permissions within your Settings.", nil)
                               delegate:self
                                    tag:406];
        }
    } else {
        
    }
    
    [self.locationManager stopUpdatingLocation];
    [self.locationManager startUpdatingLocation];
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    
    [self performSelector:@selector(unlockProgressing)
               withObject:nil
               afterDelay:40.0f];
}

- (void)unlockProgressing
{
    [SVProgressHUD dismiss];
}

#pragma mark - Location service delegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(unlockProgressing)
                                               object:nil];
    [SVProgressHUD dismiss];
    // If it's a relatively recent event, turn off updates to save power
    CLLocation *location = [locations lastObject];
    [self locationManager:manager didUpdateToLocation:location];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(unlockProgressing)
                                               object:nil];
    [SVProgressHUD dismiss];
    [self locationManager:manager didUpdateToLocation:newLocation];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)location
{
//    NSDate *eventDate = location.timestamp;
//    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    
    
    self.location = location;
    
#if DEBUG
//    self.location = [[CLLocation alloc] initWithLatitude:34.062670
//                                               longitude:-118.375091];
    self.location = [[CLLocation alloc] initWithLatitude:34.0598774
                                               longitude:-118.301307];
#endif
    
    PCLog(@"Location updated %@", self.location);
    
#ifdef FLURRY_ENABLED
    [Flurry setLatitude:location.coordinate.latitude
              longitude:location.coordinate.longitude
     horizontalAccuracy:location.horizontalAccuracy
       verticalAccuracy:location.verticalAccuracy];
#endif
    [[NSNotificationCenter defaultCenter] postNotificationName:locationUpdatedNotificationKey
                                                        object:manager
                                                      userInfo:[NSDictionary dictionaryWithObjectsAndKeys:location, @"location",
                                                                nil]];
    
    [self stopLocationUpdates];

}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(unlockProgressing)
                                               object:nil];
    [SVProgressHUD dismiss];
    PCError(@"Location failed : %@", error);
//    self.location = nil;

#if DEBUG
//    self.location = [[CLLocation alloc] initWithLatitude:34.062670
//                                               longitude:-118.375091];
    self.location = [[CLLocation alloc] initWithLatitude:34.0598774
                                               longitude:-118.301307];
#endif
    
    if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
        [self stopLocationUpdates];
        if(!self.locationAlerted){
            self.locationAlerted = YES;
//            [UIAlertView alertWithTitle:NSLocalizedString(@"Enable Location Services", nil)
//                                message:NSLocalizedString(@"Please go to Settings→Privacy→Location Services→RushOrder, and then select 'While Using The App'", nil)
//                               delegate:self
//                                    tag:406];
            [UIAlertView alertWithTitle:NSLocalizedString(@"Location Services Required", nil)
                                message:NSLocalizedString(@"RushOrder needs to know your location in order to show you nearby restaurants. Please allow Location permissions within your Settings.", nil)
                               delegate:self
                                    tag:406];
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:locationUpdatedNotificationKey
                                                        object:nil
                                                      userInfo:nil];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
//        [self.locationManager startUpdatingLocation];
    } else if (status == kCLAuthorizationStatusAuthorizedAlways) {
        // iOS 7 will redundantly call this line.
//        [self.locationManager startUpdatingLocation];
    } else if (status > kCLAuthorizationStatusNotDetermined) {
        //
    }
}

- (void)stopLocationUpdates
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(unlockProgressing)
                                               object:nil];
    [SVProgressHUD dismiss];
    [self.locationManager stopUpdatingLocation];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 203:{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:CUSTOMER_ITUNES_URL]];
        }
            break;
        case 406:
            self.locationAlerted = NO;
            break;
        default:
            // Do nothing
            break;
    }
}

- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary<NSString *, id> *)options {
    
    FIRDynamicLink *dynamicLink =
    [[FIRDynamicLinks dynamicLinks] dynamicLinkFromCustomSchemeURL:url];
    
    if (dynamicLink) {
        // Handle the deep link. For example, show the deep-linked content or
        // apply a promotional offer to the user's account.
        // ...
        return [self application:app openURL:url sourceApplication:nil annotation:@{}];
    }
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:app
                                                                  openURL:url
                                                        sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                               annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                    ];
    // Add any custom logic here.
    return handled;
}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    // attempt to extract a token from the url
//    BOOL urlWasHandled = [FBAppCall handleOpenURL:url
//                                sourceApplication:sourceApplication
//                                  fallbackHandler:^(FBAppCall *call) {
//                                      NSArray *paths = [[call appLinkData].originalURL pathComponents];
//                                      
//                                      PCLog(@"paths %@",paths);
//                                      
//                                      NSString *host = [call appLinkData].originalURL.host;
//                                      
//                                      if([host isEqual:@"show"]){
//                                          if(![paths[0] isEqual:@"/"]){
//                                              return;
//                                          }
//                                          
//                                          if([paths[1] isEqual:@"mid"]){
//                                              PCLog(@"Merchant ID : %@",paths[2]);
//                                              PCSerial merchantId = [((NSString *)paths[2]) integerValue];
//                                              
//                                              // Choose tab index to 0 if not selected
//                                              [self.tabBarController setSelectedIndex:0];
//                                              
//                                              // Go to root view controller
//                                              [((UINavigationController *)self.tabBarController.viewControllers[0]) popToRootViewControllerAnimated:YES];
//                                              [((UINavigationController *)self.tabBarController.viewControllers[1]) popToRootViewControllerAnimated:YES];
//                                              [((UINavigationController *)self.tabBarController.viewControllers[2]) popToRootViewControllerAnimated:YES];
//                                              
//                                              // Show Merchant View ? or Show Menu View?
//                                              //
//                                              RestaurantViewController *restaurantViewController = (RestaurantViewController *)((UINavigationController *)self.tabBarController.viewControllers[0]).rootViewController;
//                                              Merchant *merchant = [[Merchant alloc] init];
//                                              merchant.merchantNo = merchantId;
//                                              [restaurantViewController showMerchantInfo:merchant];
//                                          }
//                                      }
//                                  }
//                          ];
//    
//    return urlWasHandled;
    
    FIRDynamicLink *dynamicLink =
    [[FIRDynamicLinks dynamicLinks] dynamicLinkFromCustomSchemeURL:url];
    
    if (dynamicLink) {
        // Handle the deep link. For example, show the deep-linked content or
        // apply a promotional offer to the user's account.
        // ...
        return YES;
    }
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation
            ];
}

- (BOOL)application:(UIApplication *)application
continueUserActivity:(NSUserActivity *)userActivity
 restorationHandler:(void (^)(NSArray *))restorationHandler {
    
    BOOL handled = [[FIRDynamicLinks dynamicLinks]
                    handleUniversalLink:userActivity.webpageURL
                    completion:^(FIRDynamicLink * _Nullable dynamicLink,
                                 NSError * _Nullable error) {
                        // ...
                    }];
    
    
    return handled;
}

- (void)requestConfiguration
{
    NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
    RequestResult result = RRFail;
    result = [CRED requestConfigWithVersion:appVersion
                            completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              // What?
                              /*
                               {
                               compatibility = 1;
                               "customer_version" = "2.1.0";
                               "dev_phone" = "+1 650-542-7071";
                               "production_phone" = "+1 650-300-4932";
                               "server_version" = "2.1";
                               "staging_phone" = "+1 650-542-7070";
                               }
                               */
                              
                              
                              NSNumber *compatibilityNumber = [response objectForKey:@"compatibility"];
                              
                              BOOL compatibility = YES;
                              if(compatibilityNumber != nil){
                                  compatibility = [compatibilityNumber boolValue];
                              }
                              
                              if(!compatibility){
//                                  [UIAlertView alertWithTitle:NSLocalizedString(@"Out of dated version", nil)
//                                                      message:NSLocalizedString(@"You need to update RushOrder APP for further using.", nil)
//                                                     delegate:self
//                                                          tag:203];
                                  [UIAlertView alertWithTitle:NSLocalizedString(@"A newer version of RushOrder is available. Please update to continue.", nil)
                                                      message:nil
                                                     delegate:self
                                                          tag:203];
                              } else {
                                  [self checkNotice];
                              }
                              
#if DEBUG
    #if USE_INTERNAL_SERVER
                              self.credPhoneNumber = @"";
    #elif DEBUG_STAGING_SERVER
                              self.credPhoneNumber = [response objectForKey:@"staging_phone"];
    #elif DEBUG_PRODUCT_SERVER
                              self.credPhoneNumber = [response objectForKey:@"production_phone"];
    #else
                              self.credPhoneNumber = [response objectForKey:@"dev_phone"];
    #endif
#else
    #if APPSTORE
                              self.credPhoneNumber = [response objectForKey:@"production_phone"];
    #else
                              self.credPhoneNumber = [response objectForKey:@"staging_phone"];
    #endif
#endif
                          }
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while transferring configuration", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
              }];
    switch(result){
        case RRSuccess:
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

@end
