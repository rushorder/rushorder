//
//  SelectMerchantViewController.m
//  RushOrder
//
//  Created by Conan on 2/13/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "SelectMerchantViewController.h"


@interface SelectMerchantViewController ()


@end

@implementation SelectMerchantViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"
                                                            forIndexPath:indexPath];
    
    switch(indexPath.row){
        case 0:
            cell.textLabel.text = @"Jitlada, Los Angeles";
            cell.detailTextLabel.text = @"1 Jan 2013";
            break;
        case 1:
            cell.textLabel.text = @"Boulevard, San Francisco";
            cell.detailTextLabel.text = @"13 Jan 2013";
            break;
        case 2:
            cell.textLabel.text = @"Son of a Gun, Los Angeles";
            cell.detailTextLabel.text = @"14 Jan 2013";
            break;
    }
    return cell;
}

@end
