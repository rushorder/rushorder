//
//  PCAppDelegate.h
//  RushOrder
//
//  Created by Conan on 2/13/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "PassCodeViewController.h"
#import "PCNavigationController.h"

#define APP ((PCAppDelegate *)[[UIApplication sharedApplication] delegate])

extern NSString * const locationUpdatedNotificationKey;
extern NSString * const OrderConfirmedNotificationKey;
extern NSString * const CartItemUpdatedNotificationKey;

@interface PCAppDelegate : UIResponder
<
UIApplicationDelegate,
CLLocationManagerDelegate,
UIAlertViewDelegate,
PassCodeViewControllerDelegate,
UITabBarControllerDelegate
>

@property (strong, nonatomic) IBOutlet UIWindow *window;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *location;

@property (strong, nonatomic) UITabBarController *tabBarController;


@property (strong, nonatomic) NSString *credPhoneNumber;

@property (strong, nonatomic) NSMutableArray *cuisines;

@property (nonatomic) BOOL appStarted;

+ (BOOL)isRetina;

//- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (void)startLocationUpdates;

- (IBAction)eventCloseButtonTouched:(id)sender;

@end


