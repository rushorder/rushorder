//
//  main.m
//  RushOrder
//
//  Created by Conan on 2/13/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PCAppDelegate class]));
    }
}
