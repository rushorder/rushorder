//
//  RushOrder.h
//  RushOrder
//
//  Created by Conan on 2/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#ifndef RushOrder_RushOrder_h
#define RushOrder_RushOrder_h

#define RUSHORDER_MERCHANT        1


#import "PayselfTypes.h"

//#ifndef DEBUG
    #define FLURRY_ENABLED      1
//#endif


#define DB_NAME @"PayselfRegister.sqlite"

#define MAX_TABLES_NUMBER   110

#define ATTR_ACCOUNT @"PayselfMerchantUserAccount"

//#define POS_ENABLED     1

#import "PCMerchantCredentialService.h"
#import "PCViewController.h"
#import "PCiPadViewController.h"
#import "PCTextField.h"
#import "PCCurrencyTextField.h"
#import "NPKeyboardAwareScrollView.h"
#import "NPToggleButton.h"
#import "NPStretchableButton.h"
#import "NPValidation.h"
#import "M13ProgressHUD.h"
#import "PCNavigationController.h"
#import "PCSplitViewController.h"
#import "NPImageButton.h"

#ifdef FLURRY_ENABLED
    #import "Flurry.h"
#endif

#pragma mark - BXL Printer constant definition

#define	RUSHORDER_BXL_PRINTER_NOT_SET             3001
#define	RUSHORDER_BXL_CONNECT_ERROR               3002
#define	RUSHORDER_BXL_LOGO_NOT_SET                3003
#define	RUSHORDER_BXL_PRINTER_NOT_FOUND           3004
#define	RUSHORDER_BXL_PRINTER_SET_UNAVAILABLE     3005
#define	RUSHORDER_BXL_NOT_CONNECTED               3006

#define RUSHORDER_PRINTER_NOT_SET                 203001

#endif