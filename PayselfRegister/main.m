//
//  main.m
//  RushOrderMerchant
//
//  Created by Conan on 2/28/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PCiAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PCiAppDelegate class]));
    }
}