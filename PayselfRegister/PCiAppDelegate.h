//
//  PCiAppDelegate.h
//  RushOrderMerchant
//
//  Created by Conan on 2/28/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "NoticeViewControllerIPad.h"
#import "IIViewDeckController.h"

#define APP ((PCiAppDelegate *)[[UIApplication sharedApplication] delegate])


extern NSString * const locationUpdatedNotificationKey;
extern NSString * const OrderConfirmedNotificationKey;

@interface PCiAppDelegate : UIResponder
<
UIApplicationDelegate,
CLLocationManagerDelegate,
NoticeViewControllerIPadDelegate,
UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UIWindow *window;
@property (strong, nonatomic) IIViewDeckController *masterViewController;
//@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
//@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
//@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *location;

@property (nonatomic) PrinterMakerCode selectedMakerCode;
@property (copy, nonatomic) NSString *selectedPrinterId;

@property (readonly) IIViewDeckController *viewDeckController;

+ (BOOL)isRetina;

+ (BOOL)isOver60;
//- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (void)transitDashboard;
- (void)transitFastFood;
- (void)transitTableStatus;
- (void)transitReport;
- (void)transitLogin;
- (void)transitIntroduction;
- (void)transitStaffJoin;
- (void)transitSelectMerchant;
- (void)hideIntroduction;

- (void)checkNotice;

- (void)vibratePhone;
- (IBAction)playSystemSound;


@end
