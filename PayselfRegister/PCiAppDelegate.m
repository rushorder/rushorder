//
//  PCiAppDelegate.m
//  RushOrderMerchant
//
//  Created by Conan on 2/28/13.
//  Copyright (c) 2013 Payself. All rights reserved.
//

#import "PCiAppDelegate.h"
#import "SignInViewController.h"
#import "ImageBackViewController.h"
#import "Mixpanel.h"
#import "TableStatusManager.h"
#import "ReportMenuTableViewController.h"
#import "AggregateViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "MenuManager.h"
#import "TableButtonViewController.h"
#import "PCMerchantService.h"
#import "TableStatusViewController.h"
#import "UserInfo.h"
#import "StaffJoinViewController.h"
#import "JoinedMerchantViewController.h"
#import "PCMerchantCredentialService.h"
#import "BixPrintManager.h"
#import "EpsonPrintManager.h"
#import "StarPrintManager.h"

#define MIXPANEL_TOKEN @"5431ac4db17ce6eecb05cb77ebf85a6b"

#ifdef FLURRY_ENABLED
void uncaughtExceptionHandler(NSException *exception) {
    [Flurry logError:@"Uncaught" message:@"Crash!" exception:exception];
}
#endif

NSString * const locationUpdatedNotificationKey = @"locationUpdatedNotificationKey";
NSString * const OrderConfirmedNotificationKey = @"OrderConfirmedNotificationKey";

@interface PCiAppDelegate()
{
    NSUInteger locationRetryCount;
    SystemSoundID soundFileObject;
    
}

@property (weak, nonatomic) IBOutlet SignInViewController *signInViewController;
@property (weak, nonatomic) IBOutlet PCNavigationController *credNavigationController;

@property (weak, nonatomic) IBOutlet ImageBackViewController *imageBackViewController;
@property (weak, nonatomic) IBOutlet PCSplitViewController *splitViewController;
@property (strong, nonatomic) IBOutlet UIViewController *fastFoodViewController;
@property (strong, nonatomic) IBOutlet TableButtonViewController *sitDownRestaurantViewController;


@property (strong, nonatomic) IBOutlet UIView *eventContainer;
@property (strong, nonatomic) IBOutlet UILabel *eventLabel;
@property (weak, nonatomic) IBOutlet NoticeViewControllerIPad *noticeViewController;
@property (weak, nonatomic) IBOutlet UINavigationController *noticeNavigationController;

@property (nonatomic) PCSerial noticeNo;

@property (strong, nonatomic) UIViewController *tempModalViewController;


@end

@implementation PCiAppDelegate

//@synthesize managedObjectContext = _managedObjectContext;
//@synthesize managedObjectModel = _managedObjectModel;
//@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

+ (BOOL)isRetina
{
    return ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale >= 2.0))?1:0;
}

+ (void)initialize
{
    NSDictionary *appDefaults = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [NSNumber numberWithBool:YES], testDriveModeDefaultsKey,
                                 [NSNumber numberWithBool:YES], @"blinkWithNewOrder",
                                 [NSNumber numberWithBool:NO], autoLoginUserDefaultsKey,
                                 nil];
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL)isOver60
{
    NSString *reqSysVer = @"6.0";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    return ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending);
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [SVProgressHUD setBackgroundColor:[UIColor colorWithWhite:0.0f
                                                        alpha:0.25f]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD setRingThickness:2.0f];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.window.rootViewController = self.imageBackViewController;
    }
    
    NSString *payselfUUID = [CRED.payselfUUID objectForKey:(__bridge id)(kSecValueData)];
    
    if([payselfUUID length] == 0){
        
        //Set to keychain
        payselfUUID = [[[NSUUID UUID] UUIDString] stringByReplacingOccurrencesOfString:@"-"
                                                                            withString:@""];
        [CRED.payselfUUID setObject:payselfUUID
                                     forKey:(__bridge id)(kSecValueData)];
    }
    
    serviceUUID = payselfUUID;
    clientAppVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    CRED.udid = payselfUUID;
    
//    PCLog(@"payselfUUID %@", payselfUUID);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(SignedOut:)
                                                 name:SignedOutNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [UIApplication sharedApplication].idleTimerDisabled = YES;
    }
    
//    UIImage *backImage = nil;
    UIImage *toolBarBackImage = nil;
    if(OVER_IOS7){
//        backImage = [UIImage imageNamed:@"navigationbar_ios7"];
        toolBarBackImage = [UIImage imageNamed:@"navigationbar_dashboard_ios7"];
    } else {
//        backImage = [UIImage imageNamed:@"navigationbar"];
        toolBarBackImage = [UIImage imageNamed:@"navigationbar_dashboard"];
//        
//        [[UINavigationBar appearance] setTitleVerticalPositionAdjustment:5.0f
//                                                           forBarMetrics:UIBarMetricsDefault];
    }
    
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor colorWithR:40.0f G:40.0f B:40.0f], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"Helvetica-Bold" size:18.0], NSFontAttributeName, nil]];
    
    [[UINavigationBar appearance] setBackgroundImage:toolBarBackImage
                                       forBarMetrics:UIBarMetricsDefault];
    
//    [[UIToolbar appearance] setBackgroundImage:toolBarBackImage
//                            forToolbarPosition:UIBarPositionTop
//                                    barMetrics:UIBarMetricsDefault];
    
#if DEBUG
    [[PCLogger shared] setLogLevel:LevelLog];
#else
    [[PCLogger shared] setLogLevel:LevelError];
#endif

#if TARGET_IPHONE_SIMULATOR
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    userDefault.deviceToken = @"iOS simulator fake device token";
#else
    if(OVER_IOS8){
        UIUserNotificationSettings *settings = [UIUserNotificationSettings
                                                settingsForTypes:UIUserNotificationTypeBadge |
                                                UIUserNotificationTypeSound |
                                                UIUserNotificationTypeAlert
                                                categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        [[UIApplication sharedApplication]
         registerForRemoteNotificationTypes:(UIRemoteNotificationType)(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
#endif
    
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        self.window.rootViewController = self.imageBackViewController;
    }
    
    [self.window makeKeyAndVisible];
    
    if([self.window respondsToSelector:@selector(setTintColor:)]){
        self.window.tintColor = [UIColor colorWithR:251.0f G:168.0f B:8.0f];
    }
    
    if([NSUserDefaults standardUserDefaults].autoLogin){
        NSString *userId = [CRED.userAccountItem objectForKey:(__bridge id)(kSecAttrAccount)];
        NSString *password = [CRED.userAccountItem objectForKey:(__bridge id)(kSecValueData)];
    
        if([userId length] > 0 && [password length] > 0){
            RequestResult result = RRFail;
            
            NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
            
            result = [CRED requestSignIn:userId
                                password:password
                             deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                                 version:appVersion
                         completionBlock:
                      ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                          
                          if(isSuccess && ((HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES) || statusCode == HTTP_STATUS_UNAUTHORIZED)){

                              if(response.errorCode == ResponseSuccess){
                                  
                                  [self checkNotice];

                                  CRED.merchant = [response objectForKey:@"merchant"];
                                  
                                  if(CRED.merchant){
                                      
                                      if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                                          if(CRED.merchant.isTableBase){
                                              [self transitDashboard];
                                          } else {
                                              [self transitFastFood];
                                          }
                                      } else {
                                          [self transitTableStatus];
                                      }
                                  } else {
                                      if([CRED.merchants count] > 1){
                                          // Show select merchant
                                             switch(CRED.userInfo.role){
                                                 case UserRoleOwner:
                                                     CRED.merchant = [CRED.merchants objectAtIndex:0];
                                                     if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                                                         if(CRED.merchant.isTableBase){
                                                             [self transitDashboard];
                                                         } else {
                                                             [self transitFastFood];
                                                         }
                                                     } else {
                                                         [self transitTableStatus];
                                                     }
                                                     break;
                                                 case UserRoleStaff:
                                                     [self transitSelectMerchant];
                                                     break;
                                                 case UserRoleUnknown:
                                                 default:
                                                     
                                                     break;
                                             }
                                      } else {
                                          // No merchant
                                          switch(CRED.userInfo.role){
                                              case UserRoleOwner:
                                                  [self transitIntroduction];
                                                  break;
                                              case UserRoleStaff:
                                                  [self transitStaffJoin];
                                                  break;
                                              case UserRoleUnknown:
                                              default:
                                                  
                                                  break;
                                          }
                                      }
                                  }
                                  
                              } else {
                                  [self presentSignInViewController:YES];
                              }
                          } else {
                              [self presentSignInViewController:YES];
                          }
                          
                          [SVProgressHUD dismiss];
                      }];
            
            switch(result){
                case RRNotReachable:
                    
                    [self presentSignInViewController:YES];
                    
                    break;
                case RRSuccess:
                    [SVProgressHUD showWithStatus:NSLocalizedString(@"Signing In...",nil)
                                         maskType:SVProgressHUDMaskTypeClear];
                    break;
                case RRParameterError:
                    [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                    
                    [self presentSignInViewController:YES];
                    
                    break;
                default:
                    break;
            }
        } else {
            [self presentSignInViewController:YES];
        }
    } else {
        [self presentSignInViewController:YES];
    }

    
    CFBundleRef mainBundle = CFBundleGetMainBundle ();
    
    // Get the URL to the sound file to play. The file in this case
    // is "tap.aif"
    CFURLRef soundFileURLRef  = CFBundleCopyResourceURL (
                                                mainBundle,
                                                CFSTR ("doorbell"),
                                                CFSTR ("wav"),
                                                NULL
                                                );
    
    // Create a system sound object representing the sound file
    AudioServicesCreateSystemSoundID (
                                      soundFileURLRef,
                                      &soundFileObject
                                      );
    
    NSArray *printers = [NSUserDefaults standardUserDefaults].receiptPrinters;
    if([printers count] > 0){
        NSDictionary *selectedPrinter = [printers objectAtIndex:0];
        self.selectedMakerCode = (PrinterMakerCode)[selectedPrinter integerForKey:@"MakerCode"];
        self.selectedPrinterId = [selectedPrinter objectForKey:@"PrinterId"];
    }
    
    // PrintSettingViewController
    switch(self.selectedMakerCode){
        case PrinterMakerEpson:
            EPSON.deviceName = self.selectedPrinterId;
            break;
        case PrinterMakerStar:
            STAR.portName = self.selectedPrinterId;
            STAR.portSettings = @"Standard";
            break;
        case PrinterMakerBixolon:
            BIX.macAddress = self.selectedPrinterId;
            break;
        default:
            // Nothing
            break;
    }
    
    
#ifdef FLURRY_ENABLED
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    [Flurry setCrashReportingEnabled:YES];
    
    [Flurry startSession:@"2TW9YFJY7M72BMKHDF6T"];
    [Flurry setUserID:CRED.udid];
#endif

    return YES;

}

- (void)keyWillHide:(NSNotification *)notification
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        if(OVER_IOS8){
            [UIApplication sharedApplication].idleTimerDisabled = YES;
        }
    }
}


- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        return UIInterfaceOrientationMaskLandscape;
    } else {
        return UIInterfaceOrientationMaskPortrait;
    }
}

- (void)SignedOut:(NSNotification *)aNoti
{
    MENUPAN.dirty = YES; //Not use yet
    MENUPAN.wholeSerialMenuList = nil;
    MENUPAN.wholeMenuList = nil;
    MENUPAN.activeMenuList = nil;
    MENUPAN.menuFetched = NO;
}

- (void) vibratePhone {
    AudioServicesPlaySystemSound (kSystemSoundID_Vibrate);
}

- (IBAction) playSystemSound {
    AudioServicesPlaySystemSound (soundFileObject);
}

- (void)presentSignInViewController
{
    [self presentSignInViewController:NO];
}

- (void)presentSignInViewController:(BOOL)checkNotice
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [self.credNavigationController popToRootViewControllerAnimated:NO];
        [self.window.rootViewController presentViewController:self.credNavigationController
                                                     animated:YES
                                                   completion:^(){
                                                       if(checkNotice){
                                                           [self checkNotice];
                                                       }
                                                   }];
    } else {
        SignInViewController *viewController = [SignInViewController viewControllerFromNib];
        [self.window.rootViewController presentViewControllerInNavigation:viewController
                                                                 animated:YES
                                                               completion:^{
                                                                   if(checkNotice){
                                                                       [self checkNotice];
                                                                   }
                                                               }];
    }
}

- (void)transitDashboard
{
    self.splitViewController.viewControllers = [NSArray arrayWithObjects:self.masterViewController,
                                                self.sitDownRestaurantViewController,
                                                nil];
    
    if(self.window.rootViewController != self.splitViewController){
        self.window.rootViewController = self.splitViewController;
    }
}

- (void)transitFastFood
{
    self.splitViewController.viewControllers = [NSArray arrayWithObjects:self.masterViewController,
                                                self.fastFoodViewController,
                                                nil];
    
    if(self.window.rootViewController != self.splitViewController){
        self.window.rootViewController = self.splitViewController;
    }
}

- (void)transitTableStatus
{
    self.window.rootViewController = self.masterViewController;
}

- (void)transitStaffJoin
{
    if(self.window.rootViewController != self.imageBackViewController){
        self.window.rootViewController = self.imageBackViewController;
        [self presentStaffJoinViewController];
    } else {
        if(!self.imageBackViewController.introductionView.hidden){
            [self hideIntroduction];
        }
        [self presentStaffJoinViewController];
    }
}

- (void)transitSelectMerchant
{
    if(self.window.rootViewController != self.imageBackViewController){
        self.window.rootViewController = self.imageBackViewController;
        [self presentSelectMerchantViewController];
    } else {
        if(!self.imageBackViewController.introductionView.hidden){
            [self hideIntroduction];
        }
        [self presentSelectMerchantViewController];
    }
}

- (void)presentStaffJoinViewController
{
    [self presentStaffJoinViewController:NO];
}

- (void)presentStaffJoinViewController:(BOOL)checkNotice
{
//    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
//        [self.credNavigationController popToRootViewControllerAnimated:NO];
//        [self.window.rootViewController presentViewController:self.credNavigationController
//                                                     animated:YES
//                                                   completion:^(){
//                                                       if(checkNotice){
//                                                           [self checkNotice];
//                                                       }
//                                                   }];
//    } else {
        StaffJoinViewController *viewController = [StaffJoinViewController viewControllerFromNib];
        [self.window.rootViewController presentViewControllerInNavigation:viewController
                                                                 animated:YES
                                                               completion:^{
                                                                   if(checkNotice){
                                                                       [self checkNotice];
                                                                   }
                                                               }];
//    }
}

- (void)presentSelectMerchantViewController
{
    [self presentSelectMerchantViewController:NO];
}

- (void)presentSelectMerchantViewController:(BOOL)checkNotice
{
    //    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
    //        [self.credNavigationController popToRootViewControllerAnimated:NO];
    //        [self.window.rootViewController presentViewController:self.credNavigationController
    //                                                     animated:YES
    //                                                   completion:^(){
    //                                                       if(checkNotice){
    //                                                           [self checkNotice];
    //                                                       }
    //                                                   }];
    //    } else {
    JoinedMerchantViewController *viewController = [JoinedMerchantViewController viewControllerFromNib];
    [self.window.rootViewController presentViewControllerInNavigation:viewController
                                                             animated:YES
                                                           completion:^{
                                                               if(checkNotice){
                                                                   [self checkNotice];
                                                               }
                                                           }];
    //    }
}


- (void)transitReport
{
    PCSplitViewController *splitViewController = [[PCSplitViewController alloc] init];

    ReportMenuTableViewController *menuViewController = [[ReportMenuTableViewController alloc]
                                                         initWithStyle:UITableViewStyleGrouped];
    
    PCNavigationController *masterNavi = [[PCNavigationController alloc] initWithRootViewController:menuViewController];
    
    AggregateViewController *detailViewController = [AggregateViewController viewControllerFromNib];

    PCNavigationController *detailNavi = [[PCNavigationController alloc] initWithRootViewController:detailViewController];

    splitViewController.viewControllers = [NSArray arrayWithObjects:masterNavi,detailNavi, nil];

    self.window.rootViewController = splitViewController;
}

- (void)transitLogin
{
    if(self.window.rootViewController != self.imageBackViewController){
        self.window.rootViewController = self.imageBackViewController;
        [self presentSignInViewController];
    } else {
        if(!self.imageBackViewController.introductionView.hidden){
            [self hideIntroduction];
        }
        [self presentSignInViewController];
    }
}

- (void)transitIntroduction
{
    if(self.window.rootViewController == self.imageBackViewController){
        self.imageBackViewController.introductionView.hidden = NO;
        [UIView animateWithDuration:0.5f
                         animations:^(){
                             self.imageBackViewController.introductionView.alpha = 1.0;
                         }];
    }
}

- (void)hideIntroduction
{
    if(self.window.rootViewController == self.imageBackViewController){
//        self.imageBackViewController.introductionView.hidden = NO;
        [UIView animateWithDuration:0.3f
                         animations:^(){
                             self.imageBackViewController.introductionView.alpha = 0.0;
                         }
                         completion:^(BOOL finished){
                             self.imageBackViewController.introductionView.hidden = YES;
                         }
         ];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
#ifdef FLURRY_ENABLED
    [Flurry endTimedEvent:@"ResignActive"
           withParameters:nil];
#endif
    
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{

    if(BIX.controller.isConnected){
        [BIX.controller disconnect];
    }

    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    if(CRED.merchant.isMenuOrder){
        [MENUPAN requestMenus];
    }
    
    if(CRED.merchant != nil){
        if(CRED.merchant.isTestMode && !CRED.merchant.isDemoMode){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"You are running in Test Mode",nil)
                                                                message:NSLocalizedString(@"If you want to run your restaurant with real transactions, you have to turn on Real Mode.",nil)
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"Run as Test", nil)
                                                      otherButtonTitles:NSLocalizedString(@"Go to Real", nil), nil];
            alertView.tag = 303;
            [alertView show];
        }
    }
    
    [self checkNotice];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{

    if(BIX.controller.isConnected){
        [BIX.controller disconnect];
    }
}



#pragma mark - Core Data stack



#pragma mark - Push notification related
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSMutableString *deviceId = [NSMutableString string];
    
    const unsigned char* ptr = (const unsigned char*) [deviceToken bytes];
    
    for(int i = 0 ; i < 32 ; i++)  {
        [deviceId appendFormat:@"%02x", ptr[i]];
    }
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if([userDefault.deviceToken isEqualToString:deviceId]){
        // Keep going
    } else {
        userDefault.deviceToken = deviceId;
        
        if(CRED.userInfo != nil){
            [CRED requestUpdateDeviceToken:userDefault.deviceToken
                           completionBlock:^(BOOL isSuccess, id response, NSInteger statusCode) {
                               if(isSuccess && 200 <= statusCode && statusCode < 300){

                               } else {

                               }
                           }];
        }
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    PCError(@"APNS Device Token Error : %@", error);
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    PCLog(@"[APN] userInfo:%@", userInfo);
    
    [self playSystemSound];
    [self vibratePhone];
    
    NSInteger pushType = [userInfo integerForKey:@"type"];
    NSString *message = nil;
    switch(pushType){
        case 1: // bill requested
            message = [NSString stringWithFormat:NSLocalizedString(@"Bill requested at table %@", nil),
                       [userInfo objectForKey:@"table_label"]];
            break;
        case 4: // table payment
            message = [NSString stringWithFormat:NSLocalizedString(@"Paid at table %@", nil),
                       [userInfo objectForKey:@"table_label"]];
            TABLE.paymentOrderNo = [userInfo serialForKey:@"order_id"];
            TABLE.paymentPaymentNo = [userInfo serialForKey:@"payment_id"];
            break;
        case 5: // Order placed
            message = [NSString stringWithFormat:NSLocalizedString(@"New Order at table %@", nil),
                       [userInfo objectForKey:@"table_label"]];
            break;
        case 6: // Order canceled
            message = [NSString stringWithFormat:NSLocalizedString(@"Paid at table %@", nil),
                       [userInfo objectForKey:@"table_label"]];
            break;
        case 7: // togo (Order + payment)
            message = [NSString stringWithFormat:NSLocalizedString(@"New Order - Ticket No.%@", nil),
                       [userInfo objectForKey:@"order_id"]];
            break;
        default: //
            message = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    }
    
    [SVProgressHUD showSuccessWithStatus:message];
    [TABLE reloadTableStatus:YES];
}

#pragma mark - Location service
- (void)startLocationUpdates
{
    // Create the location manager if this object does not
    // already have one.
    
    if(OVER_IOS8){
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location service is not enabled.", nil)
                                                                        message:NSLocalizedString(@"This device is not active on location server. If you go on, turn on location service.", nil)
                                                                       delegate:nil
                                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                              otherButtonTitles:nil];
        [servicesDisabledAlert show];
    }
    
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    
    // Set a movement threshold for new events.
    self.locationManager.distanceFilter = 30;
    
    [self.locationManager startUpdatingLocation];
}

#pragma mark - Location service delegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    // If it's a relatively recent event, turn off updates to save power
    CLLocation *location = [locations lastObject];
    [self locationManager:manager didUpdateToLocation:location];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    [self locationManager:manager didUpdateToLocation:newLocation];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)location
{
    NSDate *eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (fabs(howRecent) > 15.0 || self.location == nil) {
        
        self.location = location;
        
//        PCLog(@"Location updated %@", self.location);
        [[NSNotificationCenter defaultCenter] postNotificationName:locationUpdatedNotificationKey
                                                            object:manager
                                                          userInfo:[NSDictionary dictionaryWithObject:location
                                                                                               forKey:@"location"]
         ];
        
        [self stopLocationUpdates];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    PCError(@"Location failed : %@", error);
    if(locationRetryCount > 3){
        [self stopLocationUpdates];
//        PCLog(@"locationRetryCount %u", locationRetryCount);
        [UIAlertView alert:NSLocalizedString(@"Cannot retrieve location.", nil)];
    } else {
        locationRetryCount++;
    }
}

- (void)stopLocationUpdates
{
//    PCLog(@"locationRetryCount %u", locationRetryCount);
    locationRetryCount = 0;
    [self.locationManager stopUpdatingLocation];
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)checkNotice
{
    RequestResult result = RRFail;
    result = [MERCHANT requestNoticeWithCompletionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              self.noticeNo = [response serialForKey:@"id"];
                              NSString *format = [response objectForKey:@"format"];
                              NSString *content = [response objectForKey:@"contents"];
                              
                              if([NSUserDefaults standardUserDefaults].lastAccessedNoticeId < self.noticeNo){
                                  if([format isEqual:@"text"]){
                                  
                                      self.eventLabel.text = content;
                                      [self.eventLabel sizeToHeightFit];
                                      
                                      NSArray *viewControllers = self.splitViewController.viewControllers;
                                      if([viewControllers count] > 1){
                                          UIViewController *detailViewController = [viewControllers objectAtIndex:1];
                                          
                                          CGFloat containerHeight = CGRectGetMaxY(self.eventLabel.frame) + 20.0f;
                                          
                                          self.eventContainer.frame = CGRectMake(0.0f,
                                                                                 detailViewController.view.frame.size.height - containerHeight,
                                                                                 self.self.eventContainer.width,
                                                                                 containerHeight);
                                          PCLog(@"self.eventContainer %@", self.eventContainer);
                                          [detailViewController.view addSubview:self.eventContainer];
                                      }
                                      
                                  } else {
                                      
                                      self.noticeViewController.noticeNo = self.noticeNo;
                                      self.noticeViewController.format = format;
                                      self.noticeViewController.content = content;
                                      self.noticeViewController.delegate = self;
                                      
                                      [self.credNavigationController popToRootViewControllerAnimated:NO];
                                      
                                      if(self.window.rootViewController.presentedViewController != nil){
                                          if(self.window.rootViewController.presentedViewController != self.noticeNavigationController){
                                              self.tempModalViewController = self.window.rootViewController.presentedViewController;
                                          }
                                          [self.window.rootViewController dismissViewControllerAnimated:YES
                                                                                             completion:^{
                                                                                                 [self.window.rootViewController presentViewController:self.noticeNavigationController
                                                                                                                                              animated:YES
                                                                                                                                            completion:^(){
                                                                                                                                            }];
                                                                                             }];
                                      } else {
                                          [self.window.rootViewController presentViewController:self.noticeNavigationController
                                                                                       animated:YES
                                                                                     completion:^(){
                                                                                     }];
                                      }
                                  }
                              }
                          }
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting notice", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }

                      
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        default:
            
            break;
    }
}

- (void)noticeViewController:(NoticeViewControllerIPad *)viewController didTouchCloseButton:(id)sender
{
    [self.window.rootViewController dismissViewControllerAnimated:YES
                                                       completion:^{
                                                           if(self.tempModalViewController != nil){
                                                               [self.window.rootViewController presentViewController:self.tempModalViewController
                                                                                                            animated:YES
                                                                                                          completion:^(){
                                                                                                          }];
                                                           }
                                                           self.tempModalViewController = nil;
                                                       }];
}

- (IBAction)closeButtonTouched:(id)sender
{
    [NSUserDefaults standardUserDefaults].lastAccessedNoticeId = self.noticeNo;
    
    [UIView animateWithDuration:0.35f
                     animations:^{
                         self.eventContainer.y = self.eventContainer.y + self.eventContainer.height + 20.0f;
                     } completion:^(BOOL finished) {
                         [self.eventContainer removeFromSuperview];
//                         self.eventLabel = nil;
//                         self.eventContainer = nil;
                     }];
}
// The reason why I use viewDecController was for staff order - it was viewDeckController
// But, now it is changed to general view controller stack
// You can remove it, but test
- (IIViewDeckController *)masterViewController
{
    if(_masterViewController == nil){
        
        TableStatusViewController *viewController = [TableStatusViewController viewControllerFromNib];
        
        PCNavigationController *navi = [[PCNavigationController alloc] initWithRootViewController:viewController];
        navi.navigationBar.barStyle = UIBarStyleDefault;
        navi.navigationBar.translucent = NO;
        
        _masterViewController =  [[IIViewDeckController alloc] initWithCenterViewController:navi];
        _masterViewController.panningMode = IIViewDeckNavigationBarPanning;
        
    }
    return _masterViewController;
}

- (IIViewDeckController *)viewDeckController
{
    return (IIViewDeckController *)_masterViewController;
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 303:{
            if(buttonIndex == AlertButtonIndexYES){
                [[NSNotificationCenter defaultCenter] postNotificationName:OpenSettingPopoverViewNotification
                                                                    object:self];
            }
        }
            break;
        default:
            break;
    }
}


- (CLLocationManager *)locationManager
{
    if(_locationManager == nil){
        _locationManager = [[CLLocationManager alloc] init];
    }
    return _locationManager;
}

@end
