//
//  PCMerchantCredentialService.m
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCMerchantCredentialService.h"
#import "Merchant.h"
#import "PCMerchantService.h"
#import "MenuManager.h"

NSString * const SignedInNotification = @"SignedInNotification";
NSString * const SignedOutNotification = @"SignedOutNotification";
NSString * const OpenSettingPopoverViewNotification = @"OpenSettingPopoverViewNotification";

@interface PCMerchantCredentialService()
@property (nonatomic, getter = isTableLablesAsking) BOOL tableLablesAsking;
@property (nonatomic, getter = isSigningIn) BOOL signingIn;
@end

@implementation PCMerchantCredentialService

+ (PCMerchantCredentialService *)sharedService
{
    static PCMerchantCredentialService *aService = nil;
    
    @synchronized(self){
        if(aService == nil){
            aService = [[self alloc] init];
        }
    }
    
    return aService;
}


- (id)init
{
    self = [super init];
    if(self != nil){
        // Initializer
        self.userAccountItem = [[KeychainItemWrapper alloc] initWithIdentifier:ATTR_ACCOUNT
                                                                   accessGroup:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(menuListChanged:)
                                                     name:MenuListChangedNotification
                                                   object:nil];
        
        self.payselfUUID = [[KeychainItemWrapper alloc] initWithIdentifier:@"PayselfApplicationUUIDs"
                                                               accessGroup:@"773V8DE3EH.com.payselfmobile"];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (RequestResult)requestSignUp:(NSString *)email
                      password:(NSString *)password
                          role:(UserRole)role
                          name:(NSString *)name
               completionBlock:(CompletionBlock)completionBlock
{
    if([email length] == 0 || [password length] == 0){
        return RRParameterError;
    }
    
    email = [email lowercaseString];
    
    NSString *baseParameter = [NSString stringWithFormat:SIGN_UP_PARAM,
                               email,
                               password,
                               [NSString stringWithUserRole:role],
                               [name urlEncodedString]
                               ];
    
    return [super requestPostWithAPI:SIGN_UP_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestSignIn:(NSString *)email
                      password:(NSString *)password
                   deviceToken:(NSString *)deviceToken
                       version:(NSString *)version
               completionBlock:(CompletionBlock)completionBlock
{
    if([email length] == 0 || [password length] == 0){
        return RRParameterError;
    }
    
    email = [email lowercaseString];
    
    NSString *baseParameter = [NSString stringWithFormat:SIGN_IN_PARAM,
                               email,
                               password,
                               deviceToken ? deviceToken : @"",
                               [[UIDevice currentDevice] name].urlEncodedString,
                               version ? version: @""];
    
    PCSerial selectedMerchantNo = 0;
    
    selectedMerchantNo = [[NSUserDefaults standardUserDefaults] lastLogedInMerchantIdWithId:email];
    if(selectedMerchantNo > 0){
        baseParameter = [baseParameter stringByAppendingString:[NSString stringWithFormat:SIGN_IN_MERCHANT_PARAM,
                                                                selectedMerchantNo]];
    }
    
    if(self.isSigningIn){
        PCWarning(@"Already in progressing of signing in");
        return RRProgressing;
    }
    
    self.signingIn = YES;
    
    RequestResult result = RRSuccess;
    result = [super requestPostWithAPI:SIGN_IN_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                
                NSMutableDictionary *mutableRespnse = [response mutableCopy];
                
                if([[response objectForKey:@"auth_token"] length] == 0){
                    if([mutableRespnse objectForKey:@"error_code"] == nil){
                        [mutableRespnse setValue:[NSNumber numberWithInteger:ResponseErrorUnknown]
                                          forKey:@"error_code"];
                    }
                }
                
                if(mutableRespnse.errorCode == ResponseSuccess){
                    
                    NSNumber *compatibilityNumber = [response objectForKey:@"compatibility"];
                    
                    BOOL compatibility = YES;
                    if(compatibilityNumber != nil){
                        compatibility = [compatibilityNumber boolValue];
                    }
                    
                    if(!compatibility){
                        [UIAlertView alertWithTitle:NSLocalizedString(@"A newer version of RushOrder is available. Please update to continue.", nil)
                                            message:nil
                                           delegate:self
                                                tag:203];

                    } else {
                        
                        NSDictionary *userDict = [response objectForKey:@"owner"];
                        
                        if(userDict != nil){
                            self.userInfo = [[UserInfo alloc] initWithDictionary:userDict];
                        } else {
                            self.userInfo = [[UserInfo alloc] initWithDictionary:mutableRespnse.data];
                        }
                        
                        self.userInfo.authToken = [response objectForKey:@"auth_token"];
                        self.userInfo.deviceToken = deviceToken;
                        
                        //check merchant , and if not request merchant
                        NSArray *merchantList = [response objectForKey:@"merchants"];
                        
                        PCSerial selectedMerchantNo = [response serialForKey:@"current_merchant_id"];
                        
                        if(merchantList != nil){
                        
                            Merchant *aMerchant = nil;
                            
                            if([merchantList count] > 0){
                                self.merchants = [NSMutableArray array];
                                for(NSDictionary *merchantDict in merchantList){
                                    Merchant *tempMerchant = [[Merchant alloc] initWithDictionary:merchantDict];
                                    if(tempMerchant.merchantNo == selectedMerchantNo){
                                        aMerchant = tempMerchant;
                                    }
                                    [self.merchants addObject:tempMerchant];
                                }
                            }
                            
                            if(aMerchant != nil){
                                [mutableRespnse setObject:aMerchant
                                                   forKey:@"merchant"];
                            }
                            
                            if(aMerchant.isMenuOrder){
                                [MENUPAN resetGraphWithMerchant:aMerchant];
                                
                                if(MENUPAN.isMenuFetched){
                                    if([MENUPAN.activeMenuList count] == 0){
                                        [UIAlertView askWithTitle:NSLocalizedString(@"Please Fill Out Your Menu",nil)
                                                          message:NSLocalizedString(@"Your restaurant is set to accept Order by customer. Ordering needs menu. Do you want to make menu now?", nil)
                                                         delegate:self
                                                              tag:101];
                                    }
                                } else {
                                    [MENUPAN requestMenuPan];
                                    [MENUPAN requestMenus];
                                }
                            } else {
                                [MENUPAN resetGraphWithMerchant:nil];
                            }
                            
                            completionBlock(YES, mutableRespnse, statusCode);
                            
                            NSString *latestDeviceToken = [NSUserDefaults standardUserDefaults].deviceToken;
                            if(![self.userInfo.deviceToken isEqualToString:latestDeviceToken]){
                                [self requestUpdateDeviceToken:latestDeviceToken
                                               completionBlock:^(BOOL isSuccess, id response, NSInteger statusCode) {
                                                   if(isSuccess && 200 <= statusCode && statusCode < 300){
                                                       
                                                   } else {
                                                       
                                                   }
                                               }];
                            }
                            
                            if(aMerchant.isTestMode && !aMerchant.isDemoMode){
                                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"You are running in Test Mode",nil)
                                                                                    message:NSLocalizedString(@"If you want to run your restaurant with real transactions, you have to turn on Real Mode.",nil)
                                                                                   delegate:self
                                                                          cancelButtonTitle:NSLocalizedString(@"Run as Test", nil)
                                                                          otherButtonTitles:NSLocalizedString(@"Go to Real", nil), nil];
                                alertView.tag = 303;
                                [alertView show];
                            }
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:SignedInNotification
                                                                                object:self
                                                                              userInfo:nil];
                            
                        } else {
                            
                            RequestResult result = RRFail;
                            result = [MERCHANT requestMyMerchantCompletionBlock:
                                      ^(BOOL isSuccess, Merchant *merchant, NSInteger statusCode){
                                          if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                                              switch(response.errorCode){
                                                  case ResponseSuccess:
                                                  {
                                                      if(merchant != nil){
                                                          [mutableRespnse setObject:merchant
                                                                             forKey:@"merchant"];
                                                      }
                                                      
                                                      if(merchant.isTestMode && !merchant.isDemoMode){
                                                          UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"You are running in Test Mode",nil)
                                                                                                              message:NSLocalizedString(@"If you want to run your restaurant with real transactions, you have to turn on Real Mode.",nil)
                                                                                                             delegate:self
                                                                                                    cancelButtonTitle:NSLocalizedString(@"Run as Test", nil)
                                                                                                    otherButtonTitles:NSLocalizedString(@"Go to Real", nil), nil];
                                                          alertView.tag = 303;
                                                          [alertView show];
                                                      }
                                                      
                                                      if(merchant.isMenuOrder){
                                                          [MENUPAN resetGraphWithMerchant:merchant];
                                                          
                                                          if(MENUPAN.isMenuFetched){
                                                              if([MENUPAN.activeMenuList count] == 0){
                                                                  [UIAlertView askWithTitle:NSLocalizedString(@"Please Fill Out Your Menu",nil)
                                                                                    message:NSLocalizedString(@"Your restaurant is set to accept Order by customer. Ordering needs menu. Do you want to make menu now?", nil)
                                                                                   delegate:self
                                                                                        tag:101];
                                                              }
                                                          } else {
//                                                              [MENUPAN requestMenuPan];
//                                                              [MENUPAN requestMenus];
                                                          }
                                                      } else {
                                                          [MENUPAN resetGraphWithMerchant:nil];
                                                      }
                                                      
                                                      NSString *latestDeviceToken = [NSUserDefaults standardUserDefaults].deviceToken;
                                                      if(![self.userInfo.deviceToken isEqualToString:latestDeviceToken]){
                                                          [self requestUpdateDeviceToken:latestDeviceToken
                                                                         completionBlock:^(BOOL isSuccess, id response, NSInteger statusCode) {
                                                                             if(isSuccess && 200 <= statusCode && statusCode < 300){
                                                                                 
                                                                             } else {
                                                                                 
                                                                             }
                                                                         }];
                                                      }
                                                      
                                                      [[NSNotificationCenter defaultCenter] postNotificationName:SignedInNotification
                                                                                                          object:self
                                                                                                        userInfo:nil];
                                                      
                                                      break;
                                              }
                                                  default:
                                                      
                                                      break;
                                              }
                                          }
                                          completionBlock(YES, mutableRespnse, statusCode);
                                      }];
                            switch(result){
                                case RRSuccess:
                                    // Wait for my merchant information
                                    break;
                                case RRParameterError:
                                    completionBlock(YES, mutableRespnse, statusCode);
                                    [[NSNotificationCenter defaultCenter] postNotificationName:SignedInNotification
                                                                                        object:self
                                                                                      userInfo:nil];
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                } else {
                    completionBlock(YES, mutableRespnse, statusCode);
                }
                
                self.signingIn = NO;
            }];
    
    if(result != RRSuccess && result != RRNotReachable){
        self.signingIn = NO;
    }
    
    return result;
}

- (void)menuListChanged:(NSNotification *)aNoti
{
    if(MENUPAN.isMenuFetched){
        if(self.userInfo.merchant.isMenuOrder){
            if([MENUPAN.activeMenuList count] == 0){
                [UIAlertView askWithTitle:NSLocalizedString(@"Please Fill Out Your Menu",nil)
                                  message:NSLocalizedString(@"Your restaurant is set to accept Order by customer. Ordering needs menu. Do you want to make menu now?", nil)
                                 delegate:self
                                      tag:101];
            }
        }
    } else {
        
    }
}


- (RequestResult)requestSignOutCompletionBlock:(CompletionBlock)completionBlock
{
    if(self.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               self.userInfo.authToken];
    
    return [super requestDeleteWithAPI:SIGN_OUT_API_URL
                             parameter:baseParameter
                   withCompletionBlock:completionBlock
                   withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                
                if((HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES) || statusCode == HTTP_STATUS_UNAUTHORIZED){
                    if(response.errorCode == ResponseSuccess){
                        [[NSUserDefaults standardUserDefaults] setLastLogedInMerchantId:self.merchant.merchantNo
                                                                                 withId:self.userInfo.email];
                        self.userInfo = nil;
                        [[NSNotificationCenter defaultCenter] postNotificationName:SignedOutNotification
                                                                            object:self
                                                                          userInfo:nil];
                    }
                } else {
                    
                }
                
                completionBlock(YES, response, statusCode);
                
            }];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 101:
            if(buttonIndex == AlertButtonIndexYES){
//                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:MERCHANT_WEB_SIGNIN_URL]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:MERCHANT_MENU_LIST_URL, CRED.userInfo.authToken]]];
            }
            break;
        case 203:{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:MERCHANT_ITUNES_URL]];
            exit(0);
        }
            break;
        case 303:{
            if(buttonIndex == AlertButtonIndexYES){
                [[NSNotificationCenter defaultCenter] postNotificationName:OpenSettingPopoverViewNotification
                                                                    object:self];
            }
        }
            break;
        default:
            break;
    }
}

- (Merchant *)merchant
{
    return self.userInfo.merchant;
}

- (void)setMerchant:(Merchant *)merchant
{
    self.userInfo.merchant = merchant;
}

- (RequestResult)requestUpdateDeviceToken:(NSString *)newDeviceToken
                          completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. requestUpdateDeviceToken");
        return RRAuthenticateError;
    }
    
    if([newDeviceToken length] == 0){
        PCError(@"Device token is not set to update device token");
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:UPDATE_DEVICE_TOKEN_PARAM,
                               [newDeviceToken urlEncodedString],
                               CRED.userInfo.authToken];
    
    return [super requestWithAPI:UPDATE_DEVICE_TOKEN_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_async(aQueue, ^{
                    
                    dispatch_async(dispatch_get_main_queue(),^{
                        completionBlock(YES, response, statusCode);
                    });
                });
            }];
}

- (BOOL)requestChangePassword:(NSString *)currentPassword
                  newPassword:(NSString *)newPassword
              completionBlock:(CompletionBlock)completionBlock
{
    if([currentPassword length] == 0){
        PCError(@"Merchant change password, currentPassword should be specified to change password");
        return RRParameterError;
    }
    
    if([newPassword length] == 0){
        PCError(@"Merchant change password , newPassword should be specified to change password");
        return RRParameterError;
    }
    
    if([self.userInfo.authToken length] == 0){
        PCError(@"Merchant change password request must have authToken");
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:MERCHANT_PASSWORD_PARAM,
                               [currentPassword urlEncodedString],
                               [newPassword urlEncodedString],
                               self.userInfo.authToken];
    
    
    return [super requestPutWithAPI:MERCHANT_PASSWORD_API
                          parameter:baseParameter
                withCompletionBlock:completionBlock
                withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                    // Success Change Password
                    
                } else {
                    // Fail to Change Password
                    
                }
                completionBlock(YES, response, statusCode);
            }];
}

- (BOOL)requestForgotPassword:(NSString *)email
              completionBlock:(CompletionBlock)completionBlock
{
    if([email length] == 0){
        PCError(@"email should be specified to Find password");
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:MERCHANT_FORGOT_PASSWORD_PARAM,
                               [email urlEncodedString]];
    
    return [super requestPostWithAPI:MERCHANT_FORGOT_PASSWORD_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                if(HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                    // Success email
                    
                } else {
                    // Fail to email
                    
                }
                completionBlock(YES, response, statusCode);
            }];
}


@end
