//
//  PCMerchantService.h
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCServiceBase.h"
#import <CoreLocation/CoreLocation.h>
#import "Order.h"
#import "Payment.h"

typedef enum _merchantSwitchType{
    MerchantSwitchTypeTest = 1,
    MerchantSwitchTypeOpen,
    MerchantSwitchTypePublish
} MerchantSwitchType;

#if RUSHORDER_MERCHANT
    #define MERCHANT [PCMerchantService sharedService]
#endif

@interface PCMerchantService : PCServiceBase

+ (PCMerchantService *)sharedService;

// New merchant
- (RequestResult)requestNewMerchant:(Merchant *)merchant
                    completionBlock:(CompletionBlock)completionBlock;

// Demo mode
- (RequestResult)requestMerchant:(Merchant *)merchant
                            type:(MerchantSwitchType)type
                           value:(BOOL)value
                 completionBlock:(CompletionBlock)completionBlock;

// Tax
- (RequestResult)requestTaxMerchant:(Merchant *)merchant
                          taxAmount:(NSNumber *)taxAmount
                        deliveryFee:(PCCurrency)deliveryFee
         minimumDeliveryOrderAmount:(PCCurrency)minimumDeliveryOrderAmount
                   deliveryDistance:(NSNumber *)deliveryDistance
                         acceptTips:(BOOL)acceptTips
                    completionBlock:(CompletionBlock)completionBlock;

// Location Update
- (RequestResult)requestMerchant:(Merchant *)merchant
                        location:(CLLocation *)location
                      geoAddress:(NSString *)geoAddress
                 completionBlock:(CompletionBlock)completionBlock;

// Account setting
- (RequestResult)requestMerchant:(Merchant *)merchant
                        bankName:(NSString *)bankName
                    accOwnerName:(NSString *)accOwnerName
                       accNumber:(NSString *)accNumber
                   routingNumber:(NSString *)routingNumber
                             ein:(NSString *)ein
                             ssn:(NSString *)ssn
                 completionBlock:(CompletionBlock)completionBlock;

// Modify merhcant
- (RequestResult)requestModifyMerchant:(Merchant *)merchant
                       completionBlock:(CompletionBlock)completionBlock;

// Get my merchant
- (RequestResult)requestMyMerchantCompletionBlock:(CompletionBlock)completionBlock;


// New order
- (RequestResult)requestNewOrder:(Order *)order
                 completionBlock:(CompletionBlock)completionBlock;

// Update order
- (RequestResult)requestUpdateOrder:(Order *)order
                    completionBlock:(CompletionBlock)completionBlock;

// Get a order
- (RequestResult)requestOrder:(PCSerial)orderNo
              completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestOrder:(PCSerial)orderNo
              removeCompleted:(BOOL)removeCompleted
              completionBlock:(CompletionBlock)completionBlock;

// Order status list - combined Table Status Pickup Status
- (RequestResult)requestOrderStatus:(PCSerial)merchantNo
                    completionBlock:(CompletionBlock)completionBlock;

// Order list by dates
- (RequestResult)requestOrder:(PCSerial)merchantNo
                    startDate:(NSDate *)startDate
                      endDate:(NSDate *)endDate
              completionBlock:(CompletionBlock)completionBlock;

// Table labels
- (RequestResult)requestTableLabels:(PCSerial)merchantNo
                    completionBlock:(CompletionBlock)completionBlock;

// Set table labels
- (RequestResult)requestSetTableLabels:(PCSerial)merchantNo
                          labelSetJson:(NSString *)json
                       completionBlock:(CompletionBlock)completionBlock;

// Request notice
- (RequestResult)requestNoticeWithCompletionBlock:(CompletionBlock)completionBlock;

// Staff join
- (RequestResult)requestStaffInMerchantCode:(NSString *)merchantCode
                            completionBlock:(CompletionBlock)completionBlock;

// List of merchant I requested to join
- (RequestResult)requestRequestedListWithCompletionBlock:(CompletionBlock)completionBlock;

// Remove cart
- (RequestResult)requestDeleteCart:(Cart *)cart
                   completionBlock:(CompletionBlock)completionBlock;

// Switch merchant (staff only now)
- (RequestResult)requestSelectMerchant:(PCSerial)merchantNo
                       completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestPOSAddItems:(NSArray *)lineItems
                         tableLabel:(NSString *)tableLabel
                    completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestPOSUpdateItems:(Cart *)cart
                             lineItems:(NSArray *)lineItems
                       completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestPOSDeleteItems:(Cart *)cart
                             lineItems:(NSArray *)lineItems
                       completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestMerchantAdditional:(Merchant *)merchant
                           completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestUpdateOrderingForCart:(Cart *)cart
                              completionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestGetNoticationTimeWithCompletionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestSetActiveMenu:(PCSerial)menuListNo
                           atMerchant:(PCSerial)merchantNo
                      completionBlock:(CompletionBlock)completionBlock;
@end