//
//  PCMerchantService.m
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCMerchantService.h"
#import "Merchant.h"
#import "PCMerchantCredentialService.h"
#import "TableInfo.h"
#import "MenuManager.h"

@implementation PCMerchantService

+ (PCMerchantService *)sharedService
{
    static PCMerchantService *aService = nil;
    
    @synchronized(self){
        if(aService == nil){
            aService = [[self alloc] init];
        }
    }
    
    return aService;
}

- (id)init
{
    self = [super init];
    if(self != nil){
        // Initializer
    }
    return self;
}

- (RequestResult)requestModifyMerchant:(Merchant *)merchant
                    completionBlock:(CompletionBlock)completionBlock
{
    if(merchant.merchantNo == 0){
        PCError(@"Merchant no should be defined");
        return RRParameterError;
    }
    
    return [self requestNewMerchant:merchant
                    completionBlock:completionBlock];
}

- (RequestResult)requestNewMerchant:(Merchant *)merchant
                    completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    if([merchant.name length] == 0){
        return RRParameterError;
    }
    
    if(merchant.numberOfTables <= 0 && merchant.isTableBase){
        PCError(@"numberOfTables must be greater than 0");
        return RRParameterError;
    }
    
    if(merchant.location == nil){
        PCError(@"Location of the merchant should be specified");
        return RRParameterError;
    }
                        
    NSString *baseParameter = [NSString stringWithFormat:MERCHANTS_NEW_PARAM,
                               merchant.name.urlEncodedString,
                               (merchant.ownerName == nil) ? @"" : merchant.ownerName.urlEncodedString,
                               (merchant.address == nil) ? @"" : merchant.address.urlEncodedString,
                               (merchant.country == nil) ? @"" : merchant.country.urlEncodedString,
                               (merchant.state == nil) ? @"" : merchant.state.urlEncodedString,
                               (merchant.city == nil) ? @"" : merchant.city.urlEncodedString,
                               (merchant.address1 == nil) ? @"" : merchant.address1.urlEncodedString,
                               (merchant.address2 == nil) ? @"" : merchant.address2.urlEncodedString,
                               (merchant.zip == nil) ? @"" : merchant.zip.urlEncodedString,
                               merchant.location.coordinate.latitude,
                               merchant.location.coordinate.longitude,
                               (merchant.geoAddress == nil) ? @"" : [merchant.geoAddress urlEncodedString],
                               merchant.isTestMode ? @"demo" : @"live",
                               (merchant.phoneNumber== nil) ? @"" : merchant.phoneNumber.urlEncodedString,
                               merchant.numberOfTables,
                               merchant.isAblePayment ? @"true" : @"false",
                               merchant.isMenuOrder ? @"true" : @"false",
                               merchant.isTableBase ? @"false" : @"true",
                               merchant.isAbleTakeout ? @"true" : @"false",
                               merchant.isAbleDelivery ? @"true" : @"false",
                               merchant.servicedByString,
                               CRED.userInfo.authToken];
    
    if(merchant.merchantNo == 0){
        return [super requestPostWithAPI:MERCHANTS_API_URL
                               parameter:baseParameter
                     withCompletionBlock:completionBlock
                     withDataHandleBlock:
                ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                    completionBlock(YES, response, statusCode);
                }];
    } else {
        NSString *apiUrlString = [NSString stringWithFormat:MERCHANT_API_URL,
                                  merchant.merchantNo];
        
        return [super requestPutWithAPI:apiUrlString
                              parameter:baseParameter
                    withCompletionBlock:completionBlock
                    withDataHandleBlock:
                ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                    completionBlock(YES, response, statusCode);
                }];
    }
}

- (RequestResult)requestMerchant:(Merchant *)merchant
                            type:(MerchantSwitchType)type
                           value:(BOOL)value
                 completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    if(merchant.merchantNo <= 0){
        PCError(@"Merchant no should be specified");
        return RRParameterError;
    }
    
    NSString *baseParameter = nil;
    
    switch(type){
        case MerchantSwitchTypeTest:
            baseParameter = [NSString stringWithFormat:MERCHANT_DEMO_PARAM,
                             value ? @"demo" : @"live",
                             CRED.userInfo.authToken];
            break;
        case MerchantSwitchTypeOpen:
            baseParameter = [NSString stringWithFormat:MERCHANT_OPEN_PARAM,
                             value ? @"true" : @"false",
                             CRED.userInfo.authToken];
            break;
        case MerchantSwitchTypePublish:
            baseParameter = [NSString stringWithFormat:MERCHANT_PUBLISH_PARAM,
                             value ? @"true" : @"false",
                             CRED.userInfo.authToken];
            break;
        default:
            return RRParameterError;
    }
    
    NSString *apiUrlString = [NSString stringWithFormat:MERCHANT_API_URL,
                              merchant.merchantNo];
    
    return [super requestPutWithAPI:apiUrlString
                          parameter:baseParameter
                withCompletionBlock:completionBlock
                withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestTaxMerchant:(Merchant *)merchant
                          taxAmount:(NSNumber *)taxAmount
                        deliveryFee:(PCCurrency)deliveryFee
         minimumDeliveryOrderAmount:(PCCurrency)minimumDeliveryOrderAmount
                   deliveryDistance:(NSNumber *)deliveryDistance
                         acceptTips:(BOOL)acceptTips
                    completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    if(merchant.merchantNo <= 0){
        PCError(@"Merchant no should be specified");
        return RRParameterError;
    }
    
    NSString *baseParameter = nil;
    
    baseParameter = [NSString stringWithFormat:MERCHANT_TAX_PARAM,
                     taxAmount == nil ? 0.0f : [taxAmount floatValue] * 100,
                     deliveryFee,
                     minimumDeliveryOrderAmount,
                     deliveryDistance == nil ? 0.0f : [deliveryDistance doubleValue],
                     [NSString stringWithBool:acceptTips],
                     CRED.userInfo.authToken
                     ];
    
    NSString *apiUrlString = [NSString stringWithFormat:MERCHANT_API_URL,
                              merchant.merchantNo];
    
    return [super requestPutWithAPI:apiUrlString
                          parameter:baseParameter
                withCompletionBlock:completionBlock
                withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestMerchant:(Merchant *)merchant
                        location:(CLLocation *)location
                      geoAddress:(NSString *)geoAddress
                 completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    NSString *baseParameter = nil;
    
    baseParameter = [NSString stringWithFormat:MERCHANT_LOCATION_PARAM,
                     location.coordinate.latitude,
                     location.coordinate.longitude,
                     (geoAddress == nil) ? @"" : [geoAddress urlEncodedString],
                     CRED.userInfo.authToken
                     ];
    
    NSString *apiUrlString = [NSString stringWithFormat:MERCHANT_API_URL,
                              merchant.merchantNo];
    
    return [super requestPutWithAPI:apiUrlString
                          parameter:baseParameter
                withCompletionBlock:completionBlock
                withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestMerchant:(Merchant *)merchant
                        bankName:(NSString *)bankName
                    accOwnerName:(NSString *)accOwnerName
                       accNumber:(NSString *)accNumber
                   routingNumber:(NSString *)routingNumber
                             ein:(NSString *)ein
                             ssn:(NSString *)ssn
                 completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    if(merchant.merchantNo <= 0){
        PCError(@"Merchant no should be specified");
        return RRParameterError;
    }
    
    NSString *baseParameter = nil;
    
    baseParameter = [NSString stringWithFormat:MERCHANT_ACCOUNT_PARAM,
                     accOwnerName.urlEncodedString,
                     accNumber.urlEncodedString,
                     routingNumber.urlEncodedString,
                     bankName.urlEncodedString,
                     ein.urlEncodedString,
                     ssn.urlEncodedString,
                     CRED.userInfo.authToken];
    
    NSString *apiUrlString = [NSString stringWithFormat:MERCHANT_API_URL,
                              merchant.merchantNo];
    
    return [super requestPutWithAPI:apiUrlString
                          parameter:baseParameter
                withCompletionBlock:completionBlock
                withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}


- (RequestResult)requestMyMerchantCompletionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    NSString *apiUrlString = [NSString stringWithFormat:MY_MERCHANT_API_URL];
    
    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               CRED.userInfo.authToken];
    
    return [super requestWithAPI:apiUrlString
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_async(aQueue, ^{
                    
                    NSDictionary *merchantDict = [response data];
                    
                    
                    Merchant *aMerchant = nil;
                    if(merchantDict != nil){
                        aMerchant = [[Merchant alloc] initWithDictionary:merchantDict];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(),^{
                        completionBlock(YES, aMerchant, statusCode);
                    });
                });
            }];
}

- (RequestResult)requestOrder:(PCSerial)merchantNo
                    startDate:(NSDate *)startDate
                      endDate:(NSDate *)endDate
              completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    if(merchantNo <= 0){
        return RRParameterError;
    }

//    NSDate *nextDayOfEndDay = [endDate dateByAddingTimeInterval:(60 * 60 * 24)];
    
    NSString *baseParameter = [NSString stringWithFormat:ORDER_BY_DATE_PARAM,
                               [startDate dateStringWithDashedFormatGMT].urlEncodedString,
                               [endDate dateStringWithDashedFormatGMT].urlEncodedString,
                               merchantNo,
                               CRED.userInfo.authToken];
    
    return [super requestWithAPI:ORDER_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestNewOrder:(Order *)order
                 completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    if(order.merchantNo <= 0){
        return RRParameterError;
    }
    
    if(order.tableNumber <= 0){
        return RRParameterError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:NEW_ORDER_PARAM,
                               (long)order.subTotal,
                               (long)order.subTotalTaxable,
                               (long)order.taxes,
                               order.statusString,
                               order.merchantNo,
                               [[order.tableNumber uppercaseString] urlEncodedString],
                               CRED.userInfo.authToken];
    
    return [super requestPostWithAPI:ORDER_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestUpdateOrder:(Order *)order
                    completionBlock:(CompletionBlock)completionBlock
{
    if(order.orderNo <= 0){
        return RRParameterError;
    }
    
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    NSString *apiURL = [NSString stringWithFormat:A_ORDER_API_URL,
                        order.orderNo];
    
    NSString *baseParameter = [NSString stringWithFormat:UPDATE_ORDER_PARAM,
                               (long)order.subTotal,
                               (long)order.subTotalTaxable,
                               (long)order.taxes,
                               order.statusString,
                               order.completeReasonJson ? order.completeReasonJson.urlEncodedString : @"",
                               order.expectedDate ? [order.expectedDate dateStringWithDashedFormatGMTWithZone].urlEncodedString : @"",
                               order.expectedPreparingDate ? [order.expectedPreparingDate dateStringWithDashedFormatGMTWithZone].urlEncodedString : @"",
                               CRED.userInfo.authToken];
    
    return [super requestPutWithAPI:apiURL
                          parameter:baseParameter
                withCompletionBlock:completionBlock
                withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestOrder:(PCSerial)orderNo
              completionBlock:(CompletionBlock)completionBlock
{
    return [self requestOrder:orderNo
              removeCompleted:YES
              completionBlock:completionBlock];
}

- (RequestResult)requestOrder:(PCSerial)orderNo
              removeCompleted:(BOOL)removeCompleted
              completionBlock:(CompletionBlock)completionBlock
{
    
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               CRED.userInfo.authToken];
    
    if(orderNo <= 0){
        return RRParameterError;
    }
    
    NSString *apiURL = [NSString stringWithFormat:A_ORDER_API_URL,
                        orderNo];
    
    return [super requestWithAPI:apiURL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_async(aQueue, ^{
                    
                    NSMutableDictionary *mutableResponse = [response mutableCopy];
                    if([mutableResponse.data isEqual:@"empty"]){
                        [mutableResponse removeObjectForKey:@"data"];
                    } else if([[mutableResponse.data objectForKey:@"status"] isEqual:@"completed"] && removeCompleted){
                        [mutableResponse removeObjectForKey:@"data"];
                    }
                    
                    NSMutableArray *paymentArray = [NSMutableArray array];
                    NSArray *payments = [response objectForKey:@"payments"];
                    for(NSDictionary *aPaymentDict in payments){
                        Payment *payment = [[Payment alloc] initWithDictionary:aPaymentDict];
                        [paymentArray addObject:payment];
                    }
                    
                    if([paymentArray count] > 0){
                        [mutableResponse setObject:paymentArray
                                            forKey:@"payments"];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(),^{
                        completionBlock(YES, mutableResponse, statusCode);
                    });
                });
            }];
}

- (RequestResult)requestOrderStatus:(PCSerial)merchantNo
                    completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    if(merchantNo <= 0){
        return RRParameterError;
    }
    
    NSString *apiURL = [NSString stringWithFormat:ORDER_STATUS_API_URL,
                        merchantNo];
    
    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               CRED.userInfo.authToken];
    
    return [super requestWithAPI:apiURL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_async(aQueue, ^{
                    
                    NSMutableArray *orderList = nil;
                    NSMutableDictionary *rtnDict = [NSMutableDictionary dictionary];
                    
                    NSInteger newOrderCount = 0;
                    
                    // Table base
                    NSInteger completedCount = 0;
                    NSInteger billRequestedCount = 0;
                    NSInteger partialPaidCount = 0;
                    NSInteger emptyCount = 0;
                    NSInteger billIssuedCount = 0;
                    NSMutableArray *tableList = nil;
                    
                    // Pickup, Takeout, delivery
                    NSInteger draftCount = 0;
                    NSInteger paidCount = 0;
                    NSInteger confirmedCount = 0;
                    NSInteger readyCount = 0;
                    
                    if(response.errorCode == ResponseSuccess){
                        
                        [NSUserDefaults standardUserDefaults].lastNotificationTime = [response objectForKey:@"last_notification_time"];
                        PCLog(@"Last Notification Time %@", [NSUserDefaults standardUserDefaults].lastNotificationTime);
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        NSArray *tableArray = [response objectForKey:@"table_orders"];
                        
                        tableList = [NSMutableArray array];
                        
                        for(NSDictionary *tableDict in tableArray){
                            TableInfo *tableInfo = [[TableInfo alloc] initWithDictionary:tableDict];
                            tableInfo.merchantNo = merchantNo;
                            [tableList addObject:tableInfo];
                            
                            // !!!:Cart must be handled prior to order
                            NSDictionary *billRequestDict = [tableDict objectForKey:@"bill_request"];
                            if(billRequestDict != nil){
                                tableInfo.billRequest = [[BillRequest alloc] initWithDictionary:billRequestDict];
                            }
                            
                            NSDictionary *cartDict = [tableDict objectForKey:@"cart"];
                            
                            if([cartDict objectForKey:@"id"] != nil && ![[cartDict objectForKey:@"status"] isEqual:@"fixed"]){
                                tableInfo.cart = [[Cart alloc] init];
                                tableInfo.cart.merchant = CRED.merchant;
                                tableInfo.cart.merchantNo = CRED.merchant.merchantNo;
//                                tableInfo.cart.tableNo = tableInfo.tableNo;
                                tableInfo.cart.tableLabel = tableInfo.tableNumber;
                                
                                // This code call menuList URLRequest in thread
                                // Request is not thread safe
                                // So delay linkMenuItemInMenuList:MENUPAN.wholeSerialMenuList:
                                [tableInfo.cart updateWithDictionary:cartDict
                                                             menuPan:nil];
                                [tableInfo configureTableStatus];
                            }
                            
                            NSDictionary *orderDict = [tableDict objectForKey:@"order"];
                            if(orderDict != nil){
                                tableInfo.order = [[Order alloc] initWithDictionary:orderDict
                                                                           merchant:nil];
                            }
                            
                            switch(tableInfo.status){
                                case TableStatusBillReqeusted:
                                case TableStatusOrderSubmitted:
                                    newOrderCount++;
                                    // Don't break here! - billRequestedCount has counts above too
                                    // break;
                                case TableStatusOrdering:
                                    billRequestedCount++;
                                    break;
                                case TableStatusBillIssued:
                                    billIssuedCount++;
                                    break;
                                case TableStatusPartialPaid:
                                    partialPaidCount++;
                                    break;
                                case TableStatusCompleted:
                                    completedCount++;
                                    break;
                                case TableStatusUnknown:
                                case TableStatusEmpty:
                                default:
                                    emptyCount++;
                                    break;
                            }
                        }
                        ////////////////////////////////////////////////////////
                        ////////////////////////////////////////////////////////
                        
                        
                        
                        orderList = [NSMutableArray array]; //For Pickup, Takeout
                        
                        NSArray *ticketOrders = [response objectForKey:@"ticket_orders"];
                        
                        for(NSDictionary *orderDict in ticketOrders){
                            
                            if(orderDict != nil){
                                Order *order = [[Order alloc] initWithDictionary:orderDict
                                                                        merchant:nil];
                                
                                NSUInteger insertedIndex = [orderList indexOfObject:order
                                                                      inSortedRange:NSMakeRange(0, [orderList count])
                                                                            options:NSBinarySearchingInsertionIndex
                                                                    usingComparator:^NSComparisonResult(id obj1, id obj2) {
                                                                        return [obj1 compareTimeWith:obj2];
                                                                    }];
                                [orderList insertObject:order atIndex:insertedIndex];
//                                [orderList addObject:order];
                                
                                switch(order.status){
                                    case OrderStatusDraft:
                                        draftCount++;
                                        break;
                                    case OrderStatusPaid:
                                        newOrderCount++;
                                        paidCount++;
                                        break;
                                    case OrderStatusConfirmed:
                                    case OrderStatusAccepted:
                                        confirmedCount++;
                                        break;
                                    case OrderStatusReady:
                                    case OrderStatusPrepared:
                                        readyCount++;
                                        break;
                                    default:
                                        
                                        break;
                                }
                            }
                        }
                    }
                    
                    if(tableList != nil){
                        [rtnDict setObject:tableList forKey:@"tableList"];
                    }
                    
                    [rtnDict setObject:[NSNumber numberWithInteger:newOrderCount] forKey:@"newOrderCount"];
                    
                    [rtnDict setObject:[NSNumber numberWithInteger:completedCount] forKey:@"completedCount"];
                    [rtnDict setObject:[NSNumber numberWithInteger:billRequestedCount] forKey:@"billRequestedCount"];
                    [rtnDict setObject:[NSNumber numberWithInteger:partialPaidCount] forKey:@"partialPaidCount"];
                    [rtnDict setObject:[NSNumber numberWithInteger:emptyCount] forKey:@"emptyCount"];
                    [rtnDict setObject:[NSNumber numberWithInteger:billIssuedCount] forKey:@"billIssuedCount"];
                    
                    if(orderList != nil){
                        [rtnDict setObject:orderList forKey:@"orderList"];
                    }
                    [rtnDict setObject:[NSNumber numberWithInteger:draftCount] forKey:@"draftCount"];
                    [rtnDict setObject:[NSNumber numberWithInteger:paidCount] forKey:@"paidCount"];
                    [rtnDict setObject:[NSNumber numberWithInteger:confirmedCount] forKey:@"confirmedCount"];
                    [rtnDict setObject:[NSNumber numberWithInteger:readyCount] forKey:@"readyCount"];
                    
                    
                    dispatch_async(dispatch_get_main_queue(),^{
                        completionBlock(YES, rtnDict, statusCode);
                    });
                });
            }];
}

- (RequestResult)requestTableLabels:(PCSerial)merchantNo
                    completionBlock:(CompletionBlock)completionBlock
{
    if(merchantNo <= 0){
        return RRParameterError;
    }
    
    NSString *apiURL = [NSString stringWithFormat:TABLE_API_URL,
                        merchantNo];
    
    return [super requestWithAPI:apiURL
                       parameter:nil
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_async(aQueue, ^{
                    
                    /*
                     "created_at" = "2013-03-19T10:17:27Z";
                     id = 63;
                     label = 1;
                     "merchant_id" = 9;
                     "updated_at" = "2013-03-19T10:17:27Z";&*/
                    
                    NSMutableArray *tableList = nil;
                    if(response.errorCode == ResponseSuccess){
                        tableList = [NSMutableArray array];
                        for(NSDictionary *tableDict in response.data){
                            TableInfo *tableInfo = [[TableInfo alloc] initWithModelDictionary:tableDict];
                            [tableList addObject:tableInfo];
                        }                        
                    }
                    
                    dispatch_async(dispatch_get_main_queue(),^{
                        completionBlock(YES, tableList, statusCode);
                    });
                });
            }];
}

- (RequestResult)requestSetTableLabels:(PCSerial)merchantNo
                          labelSetJson:(NSString *)json
                       completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    if(merchantNo <= 0){
        return RRParameterError;
    }
    
    NSString *apiURL = [NSString stringWithFormat:SET_TABLE_LABEL_API_URL,
                        merchantNo];
    
    NSString *baseParameter = [NSString stringWithFormat:TABLE_LABEL_SET_PARAM,
                               CRED.userInfo.authToken,
                               json.urlEncodedString];
    
    return [super requestPostWithAPI:apiURL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestNoticeWithCompletionBlock:(CompletionBlock)completionBlock
{
    return [super requestWithAPI:NOTICE_API_URL
                       parameter:@"target=merchant"
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSArray *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestDeleteCart:(Cart *)cart
                   completionBlock:(CompletionBlock)completionBlock
{
    if(cart == nil){
        return RRParameterError;
    }
    
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               CRED.userInfo.authToken];
    
    NSString *apiURL = [NSString stringWithFormat:CART_API_URL, cart.cartNo];
    
    return [super requestDeleteWithAPI:apiURL
                             parameter:baseParameter
                   withCompletionBlock:completionBlock
                   withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_async(aQueue, ^{
                    dispatch_async(dispatch_get_main_queue(),^{
                        completionBlock(YES, response, statusCode);
                    });
                });
            }];
}

- (RequestResult)requestStaffInMerchantCode:(NSString *)merchantCode
                            completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. in Staff Join.");
        return RRAuthenticateError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:STAFF_JOIN_PARAM,
                               CRED.userInfo.authToken,
                               merchantCode];
    
    return [super requestPostWithAPI:STAFF_JOIN_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_async(aQueue, ^{
                    
                    dispatch_async(dispatch_get_main_queue(),^{
                        completionBlock(YES, response, statusCode);
                    });
                });
            }];
}

- (RequestResult)requestRequestedListWithCompletionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. Merchants list requested.");
        return RRAuthenticateError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               CRED.userInfo.authToken];
    
    return [super requestWithAPI:STAFF_JOIN_REQUEST_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_async(aQueue, ^{
                    
                    dispatch_async(dispatch_get_main_queue(),^{
                        completionBlock(YES, response, statusCode);
                    });
                });
            }];
}

- (RequestResult)requestSelectMerchant:(PCSerial)merchantNo
                       completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. requestSelectMerchant");
        return RRAuthenticateError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:SELECT_MERCHANT_PARAM,
                               merchantNo,
                               CRED.userInfo.authToken];
    
    return [super requestWithAPI:SELECT_MERCHANT_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_async(aQueue, ^{
                    
                    dispatch_async(dispatch_get_main_queue(),^{
                        completionBlock(YES, response, statusCode);
                    });
                });
            }];
}

- (RequestResult)requestPOSAddItems:(NSArray *)lineItems
                         tableLabel:(NSString *)tableLabel
                    completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }

    NSMutableArray *arrayForJSON = [NSMutableArray array];
    for(LineItem * lineItem in lineItems){
        [arrayForJSON addObject:[lineItem dictRepWithDeviceToken:nil]];
    }
    
    NSString *jsonString = [arrayForJSON JSONString];
    
    NSString *baseParameter = [NSString stringWithFormat:ADD_LINEITEM_POS_PARAM,
                               [tableLabel urlEncodedString],
                               CRED.userInfo.authToken,
                               [jsonString urlEncodedString]];
    
    return [super requestPostWithAPI:ADD_LINEITEM_POS_API_URL
                           parameter:baseParameter
                 withCompletionBlock:completionBlock
                 withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_async(aQueue, ^{
                    
                    dispatch_async(dispatch_get_main_queue(),^{
                        completionBlock(YES, response, statusCode);
                    });
                });
            }];
}

- (RequestResult)requestPOSUpdateItems:(Cart *)cart
                             lineItems:(NSArray *)lineItems
                       completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    if(cart == nil){
        return RRParameterError;
    }
    
    NSMutableArray *arrayForJSON = [NSMutableArray array];
    for(LineItem * lineItem in lineItems){
        [arrayForJSON addObject:[lineItem updateDictRepWithDeviceToken:nil]];
    }
    
    NSString *apiURL = [NSString stringWithFormat:UPDATE_LINEITEM_POS_API_URL, cart.cartNo];
    
    NSString *jsonString = [arrayForJSON JSONString];
    PCLog(@"jsonString %@", jsonString);
    NSString *baseParameter = [NSString stringWithFormat:UPDATE_LINEITEM_POS_PARAM,
                               [jsonString urlEncodedString],
                               CRED.userInfo.authToken];
    
    return [super requestPutWithAPI:apiURL
                          parameter:baseParameter
                withCompletionBlock:completionBlock
                withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_async(aQueue, ^{
                    
                    dispatch_async(dispatch_get_main_queue(),^{
                        completionBlock(YES, response, statusCode);
                    });
                });
            }];
}

- (RequestResult)requestPOSDeleteItems:(Cart *)cart
                             lineItems:(NSArray *)lineItems
                       completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    if(cart == nil){
        return RRParameterError;
    }
    
    NSMutableArray *arrayForJSON = [NSMutableArray array];
    for(LineItem * lineItem in lineItems){
        [arrayForJSON addObject:[NSNumber numberWithSerial:lineItem.lineItemNo]];
    }
    
    NSString *apiURL = [NSString stringWithFormat:DELETE_LINEITEM_POS_API_URL, cart.cartNo];
    
    NSString *baseParameter = [NSString stringWithFormat:UPDATE_LINEITEM_POS_PARAM,
                               [[arrayForJSON JSONString] urlEncodedString],
                               CRED.userInfo.authToken];
    
    return [super requestDeleteWithAPI:apiURL
                             parameter:baseParameter
                   withCompletionBlock:completionBlock
                   withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestMerchantAdditional:(Merchant *)merchant
                           completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. sign-in process is required.");
        return RRAuthenticateError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:MERCHANT_ADITIONAL_PARAM,
                               (merchant.desc == nil) ? @"" : merchant.desc.urlEncodedString,
                               (merchant.takeoutRemark == nil) ? @"" : merchant.takeoutRemark.urlEncodedString,
                               (merchant.homepage == nil) ? @"" : merchant.homepage.urlEncodedString,
                               CRED.userInfo.authToken];
    
    NSString *apiUrlString = [NSString stringWithFormat:MERCHANT_API_URL,
                              merchant.merchantNo];
    
    return [super requestPutWithAPI:apiUrlString
                          parameter:baseParameter
                withCompletionBlock:completionBlock
                withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestUpdateOrderingForCart:(Cart *)cart
                              completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. Update ordering for Cart requir auth token.");
        return RRAuthenticateError;
    }
    
    NSString *baseParameter = [NSString stringWithFormat:AUTH_TOKEN_PARAM,
                               CRED.userInfo.authToken];
    
    NSString *apiUrlString = [NSString stringWithFormat:UPDATE_CART_ORDERING_API_URL,
                              cart.cartNo];
    
    return [super requestWithAPI:apiUrlString
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}


- (RequestResult)requestGetNoticationTimeWithCompletionBlock:(CompletionBlock)completionBlock
{
    return [super requestWithAPI:GET_LAST_NOTIFICATION_TIME
                       parameter:nil
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

- (RequestResult)requestSetActiveMenu:(PCSerial)menuListNo
                           atMerchant:(PCSerial)merchantNo
                      completionBlock:(CompletionBlock)completionBlock
{
    if(CRED.userInfo.authToken == nil){
        PCError(@"Auth token is not specified. Setting active menu requre auth token.");
        return RRAuthenticateError;
    }
    
    
    NSString *baseParameter = [NSString stringWithFormat:MENU_ACTIVE_PARAM,
                               merchantNo,
                               menuListNo,
                               CRED.userInfo.authToken];
    
    return [super requestWithAPI:MENU_ACTIVE_API_URL
                       parameter:baseParameter
             withCompletionBlock:completionBlock
             withDataHandleBlock:
            ^(NSDictionary *response, CompletionBlock completionBlock, NSInteger statusCode){
                completionBlock(YES, response, statusCode);
            }];
}

@end
