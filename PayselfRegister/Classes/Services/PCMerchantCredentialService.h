//
//  PCCredentialService.h
//  RushOrder
//
//  Created by Conan on 2/18/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCServiceBase.h"
#import "UserInfo.h"
#import "KeychainItemWrapper.h"

extern NSString * const SignedInNotification;
extern NSString * const SignedOutNotification;
extern NSString * const OpenSettingPopoverViewNotification;

#define CRED [PCMerchantCredentialService sharedService]

@interface PCMerchantCredentialService : PCServiceBase

@property (strong, atomic) UserInfo *userInfo;
@property (strong, atomic) NSMutableArray *merchants;

@property (nonatomic, strong) KeychainItemWrapper *userAccountItem;
@property (nonatomic, strong) KeychainItemWrapper *payselfUUID;

@property (copy, nonatomic) NSString *udid;

//ReadOnly
@property (strong, nonatomic) Merchant *merchant;


+ (PCMerchantCredentialService *)sharedService;

// Sign up
- (RequestResult)requestSignUp:(NSString *)email
                      password:(NSString *)password
                          role:(UserRole)role
                          name:(NSString *)name
               completionBlock:(CompletionBlock)completionBlock;

// Sign in
- (RequestResult)requestSignIn:(NSString *)email
                      password:(NSString *)password
                   deviceToken:(NSString *)deviceToken
                       version:(NSString *)version
               completionBlock:(CompletionBlock)completionBlock;

// Sign out
- (RequestResult)requestSignOutCompletionBlock:(CompletionBlock)completionBlock;

- (RequestResult)requestUpdateDeviceToken:(NSString *)newDeviceToken
                          completionBlock:(CompletionBlock)completionBlock;

- (BOOL)requestChangePassword:(NSString *)currentPassword
                  newPassword:(NSString *)newPassword
              completionBlock:(CompletionBlock)completionBlock;

- (BOOL)requestForgotPassword:(NSString *)email
              completionBlock:(CompletionBlock)completionBlock;

@end