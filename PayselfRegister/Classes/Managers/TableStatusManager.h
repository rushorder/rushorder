//
//  TableStatusManager.h
//  RushOrder
//
//  Created by Conan on 3/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TABLE [TableStatusManager sharedManager]

extern NSString * const TogoListUpdatedNotification;
extern NSString * const TableListUpdatedNotification;
extern NSString * const TableListErrorNotification;
extern NSString * const SumCountUpdatedNotification;
extern NSString * const TableInfoChangedNotification;
extern NSString * const OrderUpdatedNotification;

@interface TableStatusManager : NSObject

+ (TableStatusManager *)sharedManager;

@property (strong, nonatomic) NSMutableArray *tableOnlyList;
@property (strong, nonatomic) NSMutableArray *tableList;
@property (strong, nonatomic) NSMutableArray *notEmptyTableList;
@property (strong, nonatomic) NSMutableArray *activeTableList;
@property (strong, nonatomic) NSMutableArray *waitingTableList;

//For Ticket View
@property (strong, nonatomic) NSMutableArray *newTableList;
@property (strong, nonatomic) NSMutableArray *preparingTableList;
@property (strong, nonatomic) NSMutableArray *readyTableList;

@property (nonatomic, getter = isDirty) BOOL dirty;

@property (nonatomic) NSInteger completedCount;
@property (nonatomic) NSInteger billRequestedCount;
@property (nonatomic) NSInteger partialPaidCount;
@property (nonatomic) NSInteger emptyCount;
@property (nonatomic) NSInteger billIssuedCount;

@property (nonatomic) NSInteger newOrderCount;

@property (nonatomic) NSInteger paymentOrderNo;
@property (nonatomic) NSInteger paymentPaymentNo;

- (BOOL)reloadTableStatusIfNeeded;
- (BOOL)reloadTableStatus;
- (BOOL)reloadTableStatus:(BOOL)withAlert;


@end