//
//  PrintWrappers.m
//  RushOrder
//
//  Created by Conan on 2/5/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import "PrintWrappers.h"


NSString * const NoPrinterFoundNotification = @"NoPrinterFoundNotification";

@implementation BillWrapper

- (id)initWithOrder:(Order *)order
{
    self = [super init];
    if(self != nil){
        self.order = order;
    }
    return self;
}

@end

@implementation ReceiptWrapper

- (id)initWithOrder:(Order *)order withPaymentIndex:(NSInteger)paymentIndex
{
    self = [super init];
    if(self != nil){
        self.order = order;
        self.paymentIndex = paymentIndex;
    }
    return self;
}

@end
