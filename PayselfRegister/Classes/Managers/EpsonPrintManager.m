//
//  EpsonPrintManager.m
//  RushOrder
//
//  Created by Conan on 8/30/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "EpsonPrintManager.h"
#import "MenuPrice.h"
#import "MenuOption.h"
#import "MenuOptionGroup.h"
#import "Payment.h"
#import <SDWebImage/SDWebImageManager.h>
#import "CustomerInfo.h"

#define MONEY_RECEIPT_LEFT_MARGIN 25


@interface EpsonPrintManager()

@property (strong, nonatomic) NSMutableArray *printQueue;
@property (copy, nonatomic) NSString *printerName;
@property (nonatomic) NSInteger language;

@property (nonatomic, getter = isConnected) BOOL connected;
@property (strong, nonatomic) EposBuilder *builder;
@end

@implementation EpsonPrintManager

+ (EpsonPrintManager *)sharedManager
{
    static EpsonPrintManager *aManager = nil;
    
    @synchronized(self){
        if(aManager == nil){
            aManager = [[self alloc] init];
        }
    }
    
    return aManager;
}

- (id)init
{
    self = [super init];
    if(self != nil){
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didBecomeActive:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(willResignActive:)
                                                     name:UIApplicationWillResignActiveNotification
                                                   object:nil];
    }
    return self;
}

- (BOOL)connect
{
    if(!self.isConnected){
        int result = [self.controller openPrinter:EPOS_OC_DEVTYPE_TCP
                                       DeviceName:self.deviceName
                                          Enabled:EPOS_OC_FALSE
                                         Interval:1000];
        
        if(result != EPOS_OC_SUCCESS){
            PCError(@"Cannot open the EPSON printer ErrCode : %d", result);
            [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot Connect To Printer",nil)
                                message:[NSString stringWithFormat:NSLocalizedString(@"Make sure the printer is connected properly : %@", nil),
                                         self.deviceName]];
            [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot Connect To Printer",nil)
                                message:NSLocalizedString(@"Make sure the printer is connected properly", nil)];
            self.connected = NO;
            return NO;
        }
        self.connected = YES;
        self.printerName = @"TM-T20";
        self.language = EPOS_OC_MODEL_ANK;
    }
    
    return YES;
}

- (void)disconnect
{
    if(self.isConnected){
        int result = [self.controller closePrinter];
        if(result != EPOS_OC_SUCCESS){
            PCError(@"Cannot close the EPSON printer ErrCode : %d", result);
        }
        self.connected = NO;
    } else {
        PCWarning(@"No connected printer (EPSON)");
    }
}

- (void)didBecomeActive:(NSNotification *)noti
{

}

- (void)willResignActive:(NSNotification *)noti
{

}

- (long)printTest
{
    if(self.builder != nil){
        //Create a print document
        int errorStatus = EPOS_OC_SUCCESS;
        errorStatus = [self.builder addTextLang: EPOS_OC_LANG_EN];
        errorStatus = [self.builder addTextSmooth: EPOS_OC_TRUE];
        errorStatus = [self.builder addTextFont: EPOS_OC_FONT_A];
        errorStatus = [self.builder addTextSize: 1 Height: 1];
        errorStatus = [self.builder addText: @"01234567890123456789012345678901234567890123456789012345678901234567890123456789\n"];
        errorStatus = [self.builder addTextSize: 2 Height: 2];
        errorStatus = [self.builder addText: @"01234567890123456789012345678901234567890123456789\n"];
        errorStatus = [self.builder addTextSize: 3 Height: 3];
        errorStatus = [self.builder addText: @"012345678901234567890123456789\n"];
        errorStatus = [self.builder addTextSize: 4 Height: 4];
        errorStatus = [self.builder addText: @"01234567890123456789\n"];
        errorStatus = [self.builder addTextSize: 5 Height: 5];
        errorStatus = [self.builder addText: @"01234567890\n"];
        errorStatus = [self.builder addCut: EPOS_OC_CUT_FEED];
        
        if(self.controller != nil){
            
            if([self connect]){
                int errorStatus = EPOS_OC_SUCCESS;
                unsigned long status;
                errorStatus = [self.controller sendData:self.builder Timeout:10000 Status:&status];
                if(errorStatus != EPOS_OC_SUCCESS){
                    PCError(@"Error to sendData into EPSON printer : %d", errorStatus);
                }
            }
            [self disconnect];
        }
    }
    self.builder = nil;
    
    return 0;
}

#pragma mark - Print

- (long)printOrder:(Order *)order
{
    return [self printOrder:order enqueueing:YES];
}

- (long)printOrder:(Order *)order enqueueing:(BOOL)enqueueing
{
    if(order == nil) {
        PCError(@"No order to print");
        return EPOS_OC_ERR_PARAM;
    }
    
    if([self.deviceName length] == 0){
        return RUSHORDER_BXL_PRINTER_NOT_SET;
    }
    
    if(self.builder != nil){
        //Create a print document
        int errorStatus = EPOS_OC_SUCCESS;
        errorStatus = [self.builder addTextLang: EPOS_OC_LANG_EN];
        errorStatus = [self.builder addTextSmooth: EPOS_OC_TRUE];
        errorStatus = [self.builder addTextFont: EPOS_OC_FONT_A];
        errorStatus = [self.builder addTextSize:1 Height:1];

        [self.builder addText:@"\n\n\n\n\n\n\n\n\n\n\n\n\n"];
        
        NSString *orderTypeString = nil;
        
        if(order.isTicketBase){
            
            switch(order.orderType){
                case OrderTypeTakeout:
                    orderTypeString = NSLocalizedString(@"Take-out", nil);
                    break;
                case OrderTypeDelivery:
                    orderTypeString = NSLocalizedString(@"Delivery", nil);
                    break;
                default:
                    orderTypeString = NSLocalizedString(@"Dine-in", nil);
                    break;
            }
        }
        
        if([order.tableNumber length] > 0){
            errorStatus = [self printTextLeft:[NSString stringWithFormat:@"TBL#%@",order.tableNumber]
                                    rightText:[NSString stringWithFormat:@"ORD#%lld",order.orderNo]
                                     textSize:2];
        } else {
            errorStatus = [self printTextLeft:orderTypeString
                                    rightText:[NSString stringWithFormat:@"TIX#%lld",order.orderNo]
                                     textSize:2];
        }
        
        [self drawDoubleLine];
        
        [self.builder addTextAlign:EPOS_OC_ALIGN_RIGHT];
        [self.builder addText:[[order.orderDate secondsString] stringByAppendingString:@" \n"]];
        
        if(order.pickupAfter != 0 || order.isTakeout || order.isDelivery){
            [self drawSingleLine];
            NSDate *dueDate = nil;
            if(order.pickupAfter != 0){
                dueDate = [order.orderDate dateByAddingTimeInterval:(order.pickupAfter * 60)];
            }
            if(order.isTakeout || order.isDelivery){
                [self printTextLeft:NSLocalizedString(@"Desired Time:", nil)
                          rightText:(dueDate == nil)?NSLocalizedString(@"ASAP", nil):[dueDate hourMinuteString]
                          textWidth:2
                             height:1];
            }
        }
        
        [self drawSingleLine];

        [self.builder addTextStyle:EPOS_OC_FALSE
                                Ul:EPOS_OC_FALSE
                                Em:EPOS_OC_TRUE
                             Color:EPOS_OC_COLOR_1];
        
        NSInteger i = 0;
        for(LineItem *lineItem in order.lineItems){

            [self.builder addTextSize:2 Height:2];
            [self.builder addText:[@"*" stringByAppendingString:lineItem.displayMenuName]];
            
//            if([lineItem.menuChoiceName length] > 0){
//                [self.builder addText:@"\n"];
//                [self.builder addTextSize:1 Height:1];
//                [self.builder addText:[@"-" stringByAppendingString:lineItem.menuChoiceName]];
//            }
            
            for(NSDictionary *groupDict in lineItem.menuOptionNamesObj){
                
                NSString *groupName = [groupDict objectForKey:@"name"];
                
                [self.builder addText:@"\n"];
                [self.builder addTextSize:1 Height:1];
                [self.builder addText:[NSString stringWithFormat:@"[%@]",
                                       groupName]];
                
                NSArray *selectedOptions = [groupDict objectForKey:@"menu_option_names"];
                for(NSDictionary *optionDict in selectedOptions){
                    NSString *optionName = [optionDict objectForKey:@"option_name"];
                    
                    [self.builder addText:@"\n"];
                    [self.builder addTextSize:2 Height:2];
                    [self.builder addText:[@"-" stringByAppendingString:optionName]];
                }
            }
            
            if([lineItem.specialInstruction length] > 0){
                [self.builder addText:@"\n"];
                [self.builder addTextSize:1 Height:1];
                [self.builder addText:NSLocalizedString(@"[Special Instruction]", nil)];
                
                [self.builder addText:@"\n"];
                [self.builder addTextSize:2 Height:2];
                [self.builder addText:lineItem.specialInstruction];
            }
            
            i++;
            
            if(i < [order.lineItems count]){
                [self.builder addText:@"\n"];
                [self drawSingleLine];
            }
        }
        
        [self.builder addTextStyle:EPOS_OC_FALSE
                                Ul:EPOS_OC_FALSE
                                Em:EPOS_OC_FALSE
                             Color:EPOS_OC_COLOR_1];
        
        [self.builder addText:@"\n"];
        
        if([order.customerRequest length] > 0){
            [self drawDoubleLine];
            [self.builder addTextSize:1 Height:1];
            [self.builder addText:NSLocalizedString(@"[Customer Request]", nil)];
            [self.builder addFeedLine:1];
            [self.builder addTextSize:2 Height:2];
            [self.builder addText:order.customerRequest];
            [self.builder addFeedLine:1];
        }
        
        
        if(order.isTicketBase){
            
            NSString *nameString = nil;
            NSString *phoneString = nil;
            
            if([order.customers count] > 0){
                if([order.receiverName length] > 0){
                    nameString = order.receiverName;
                } else {
                    NSMutableString *names = [NSMutableString string];
                    
                    for(CustomerInfo *customer in order.customers){
                        if([names length] > 0) [names appendString:@", "];
                        [names appendString:customer.name];
                    }
                    
                    nameString = names;
                }
            } else {
                if([order.receiverName length] > 0){
                    nameString = order.receiverName;
                }
            }
            
            if([order.phoneNumber length] > 0){
                phoneString = order.phoneNumber;
            }
            
            if(([nameString length] > 0) || ([phoneString length] > 0)){
                [self drawDoubleLine];
            }
            if([nameString length] > 0){
                [self.builder addTextSize:1 Height:1];
                [self.builder addText:[NSString stringWithFormat:NSLocalizedString(@"Name  : %@", nil), nameString]];
                [self.builder addText:@"\n"];
            }
            if([phoneString length] > 0){
                [self.builder addTextSize:1 Height:1];
                [self.builder addText:[NSString stringWithFormat:NSLocalizedString(@"Phone : %@", nil), phoneString]];
                [self.builder addText:@"\n"];
            }
            if(([nameString length] > 0) || ([phoneString length] > 0)){
                [self drawDoubleLine];
            }
            
            if(order.orderType == OrderTypeDelivery){
                [self.builder addTextSize:1 Height:1];
                [self printTextLeft:NSLocalizedString(@"Delivery Address", nil)
                          rightText:@""];
                
                [self drawSingleLine];
                
                [self.builder addText:order.address];
                [self.builder addText:@"\n"];
                
                [self drawDoubleLine];
            }
        } else {
            [self drawDoubleLine];
        }
        
        errorStatus = [self.builder addFeedLine:1];
        errorStatus = [self.builder addCut: EPOS_OC_CUT_FEED];
        
        if(self.controller != nil){
            
            if([self connect]){
                int errorStatus = EPOS_OC_SUCCESS;
                unsigned long status;
                errorStatus = [self.controller sendData:self.builder Timeout:10000 Status:&status];
                if(errorStatus != EPOS_OC_SUCCESS){
                    PCError(@"Error to sendData into EPSON printer : %d", errorStatus);
                }
            }
            [self disconnect];
        }
    } else {
        PCError(@"Cannot create EPOS builder");
    }
    
    self.builder = nil;
    
    return 0;
}

- (long)printCart:(Cart *)cart
{
    return [self printCart:cart enqueueing:YES];
}

- (long)printCart:(Cart *)cart enqueueing:(BOOL)enqueueing
{
    if(cart == nil) {
        PCError(@"No cart to print");
        return EPOS_OC_ERR_PARAM;
    }
    
    if([self.deviceName length] == 0){
        return RUSHORDER_BXL_PRINTER_NOT_SET;
    }
    
    if(self.builder != nil){

        int errorStatus = EPOS_OC_SUCCESS;
        errorStatus = [self.builder addTextLang: EPOS_OC_LANG_EN];
        errorStatus = [self.builder addTextSmooth: EPOS_OC_TRUE];
        errorStatus = [self.builder addTextFont: EPOS_OC_FONT_A];
        errorStatus = [self.builder addTextSize:1 Height:1];
        
        [self.builder addText:@"\n\n\n\n\n\n\n\n\n\n\n\n\n"];
        
        if(cart.orderNo == 0){
            [self printTextLeft:[NSString stringWithFormat:@"TBL#%@",cart.tableLabel]
                      rightText:[NSString stringWithFormat:@" TMP#%lld",cart.cartNo]
                       textSize:2];
        } else {
            [self printTextLeft:[NSString stringWithFormat:@"TBL#%@",cart.tableLabel]
                      rightText:[NSString stringWithFormat:@" ORD#%lld",cart.orderNo]
                       textSize:2];
        }
        
        [self drawDoubleLine];
        
        
        
        [self.builder addTextSize:1 Height:1];
        [self.builder addTextAlign:EPOS_OC_ALIGN_RIGHT];
        [self.builder addText:[[[NSDate date] secondsString] stringByAppendingString:@" \n"]];
        
        [self drawSingleLine];
        
        [self.builder addTextStyle:EPOS_OC_FALSE
                                Ul:EPOS_OC_FALSE
                                Em:EPOS_OC_TRUE
                             Color:EPOS_OC_COLOR_1];
        
        [self.builder addTextAlign:EPOS_OC_ALIGN_LEFT];
        
        NSInteger i = 0;
        for(LineItem *lineItem in cart.lineItems){
            
            [self.builder addTextSize:1 Height:2];

            [self.builder addText:[@"*" stringByAppendingString:lineItem.displayMenuName]];
            
//            if([lineItem.menuChoiceName length] > 0){
//                [self.builder addText:@"\n"];
//                [self.builder addTextSize:1 Height:1];
//                [self.builder addText:[@"-" stringByAppendingString:lineItem.menuChoiceName]];
//            }
            
            for(NSDictionary *groupDict in lineItem.menuOptionNamesObj){
                
                NSString *groupName = [groupDict objectForKey:@"name"];
                
                [self.builder addText:@"\n"];
                [self.builder addTextSize:1 Height:1];
                [self.builder addText:[NSString stringWithFormat:@"[%@]",
                                       groupName]];
                
                NSArray *selectedOptions = [groupDict objectForKey:@"menu_option_names"];
                for(NSDictionary *optionDict in selectedOptions){
                    NSString *optionName = [optionDict objectForKey:@"option_name"];
                    
                    [self.builder addText:@"\n"];
                    [self.builder addTextSize:2 Height:2];
                    [self.builder addText:[@"-" stringByAppendingString:optionName]];
                }
            }
            
            if([lineItem.specialInstruction length] > 0){
                [self.builder addText:@"\n"];
                [self.builder addTextSize:1 Height:1];
                [self.builder addText:NSLocalizedString(@"[Special Instruction]", nil)];
                
                [self.builder addText:@"\n"];
                [self.builder addTextSize:2 Height:2];
                [self.builder addText:lineItem.specialInstruction];
            }
            i++;
            
            if(i < [cart.lineItems count]){
                [self.builder addText:@"\n"];
                [self drawSingleLine];
            }
        }
        
        [self.builder addTextStyle:EPOS_OC_FALSE
                                Ul:EPOS_OC_FALSE
                                Em:EPOS_OC_FALSE
                             Color:EPOS_OC_COLOR_1];
        
        [self.builder addText:@"\n"];
        [self drawDoubleLine];
        
        [self.builder addFeedLine:1];
        errorStatus = [self.builder addCut: EPOS_OC_CUT_FEED];
        
        if(self.controller != nil){
            
            if([self connect]){
                int errorStatus = EPOS_OC_SUCCESS;
                unsigned long status;
                errorStatus = [self.controller sendData:self.builder Timeout:10000 Status:&status];
                if(errorStatus != EPOS_OC_SUCCESS){
                    PCError(@"Error to sendData into EPSON printer : %d", errorStatus);
                }
            }
            [self disconnect];
        }
        
    } else {
        PCError(@"Cannot create EPOS builder");
    }
    
    self.builder = nil;
    return 0;
}

- (long)printBill:(BillWrapper *)bill
{
    return [self printBill:bill enqueueing:YES];
}

- (long)printBill:(BillWrapper *)bill enqueueing:(BOOL)enqueueing
{
    Order *order = (Order *)bill.order;
    
    if(order == nil) {
        PCError(@"No order to print for Bill");
        return EPOS_OC_ERR_PARAM;
    }
    
    if([self.deviceName length] == 0){
        return RUSHORDER_BXL_PRINTER_NOT_SET;
    }
    
    if(self.builder != nil){
    
        int errorStatus = EPOS_OC_SUCCESS;
        errorStatus = [self.builder addTextLang: EPOS_OC_LANG_EN];
        errorStatus = [self.builder addTextSmooth: EPOS_OC_TRUE];
        errorStatus = [self.builder addTextFont: EPOS_OC_FONT_A];
        errorStatus = [self.builder addTextSize:1 Height:1];
        
        ////////////////////////////////////////////////////////////////////////
        [self.builder addTextAlign:EPOS_OC_ALIGN_CENTER];
        [self.builder addTextSize:2 Height:2];
        
        [self.builder addText:CRED.merchant.name];
        
        [self.builder addFeedLine:3];
        ////////////////////////////////////////////////////////////////////////
        
        ////////////////////////////////////////////////////////////////////////
        [self.builder addTextSize:1 Height:1];
        
        if(order.isTicketBase){
            if(order.isTakeout){
                [self printTextLeft:NSLocalizedString(@"Take-out Order", nil)
                          rightText:[NSString stringWithFormat:@"Ticket No.%lld",order.orderNo]];
            } else if (order.isDelivery){
                [self printTextLeft:NSLocalizedString(@"Delivery Order", nil)
                          rightText:[NSString stringWithFormat:@"Ticket No.%lld",order.orderNo]];
            } else {
                if([order.tableNumber length] > 0){
                    [self printTextLeft:NSLocalizedString(@"Dine-in Order", nil)
                              rightText:[NSString stringWithFormat:@"Table No.%@",order.tableNumber]];
                    
                    [self.builder addTextAlign:EPOS_OC_ALIGN_RIGHT];
                    [self.builder addText:[NSString stringWithFormat:@"Order No.%lld",order.orderNo]];
                } else {
                    [self printTextLeft:NSLocalizedString(@"Dine-in Order", nil)
                              rightText:[NSString stringWithFormat:@"Ticket No.%lld",order.orderNo]];
                }
            }
        } else {
            [self printTextLeft:[NSString stringWithFormat:@"Table No.%@",order.tableNumber]
                      rightText:[NSString stringWithFormat:@"Order No.%lld",order.orderNo]];
        }
        
        [self drawDoubleLine];
        
        [self.builder addTextAlign:EPOS_OC_ALIGN_RIGHT];
        [self.builder addText:[[[NSDate date] secondsString] stringByAppendingString:@" "]];
        
        [self.builder addText:@"\n"];
        
        [self drawSingleLine];
        ////////////////////////////////////////////////////////////////////////
        
        NSInteger i = 0;
        for(LineItem *lineItem in order.lineItems){
            
            [self printTextLeft:lineItem.displayMenuName
                      rightText:[[NSNumber numberWithCurrency:(lineItem.unitPrice * lineItem.quantity)] currencyString]];

            for(NSDictionary *groupDict in lineItem.menuOptionNamesObj){
                
                NSArray *selectedOptions = [groupDict objectForKey:@"menu_option_names"];
                for(NSDictionary *optionDict in selectedOptions){
                    NSString *optionName = [optionDict objectForKey:@"option_name"]; //option_price
                    PCCurrency optionPrice = [optionDict currencyForKey:@"option_price"];
                    NSString *optionPriceString = nil;
                    if(optionPrice != 0){
                        optionPriceString = [[NSNumber numberWithCurrency:(optionPrice * lineItem.quantity)] currencyString];
                    }
                    [self printTextLeft:[@"    " stringByAppendingString:optionName]
                              rightText:optionPriceString];
                }
            }
            
            i++;
        }
        
        if(i > 0){
            [self drawSingleLine];
        }
        
        [self printTextLeft:NSLocalizedString(@"Subtotal", nil)
                  rightText:order.subTotalString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
        
        [self printTextLeft:NSLocalizedString(@"Tax", nil)
                  rightText:order.taxesString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
        
        if(order.orderType == OrderTypeDelivery){
            [self printTextLeft:NSLocalizedString(@"Delivery Fee", nil)
                      rightText:order.totalPaidDeliveryFeeAmountsString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        }
        
        if(order.roServiceFee > 0){
            [self printTextLeft:NSLocalizedString(@"Service Fee", nil)
                      rightText:order.roServiceFeeString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        }
        
        [self drawSingleLine];
        
        [self printTextLeft:NSLocalizedString(@"Amount Due", nil)
                  rightText:order.totalString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
        
        [self drawDoubleLine];
        
        if(order.isTicketBase){
            
            NSString *nameString = nil;
            NSString *phoneString = nil;
            
            if([order.customers count] > 0){
                if([order.receiverName length] > 0){
                    nameString = order.receiverName;
                } else {
                    NSMutableString *names = [NSMutableString string];
                    
                    for(CustomerInfo *customer in order.customers){
                        if([names length] > 0) [names appendString:@", "];
                        [names appendString:customer.name];
                    }
                    
                    nameString = names;
                }
            } else {
                if([order.receiverName length] > 0){
                    nameString = order.receiverName;
                }
            }
            
            if([order.phoneNumber length] > 0){
                phoneString = order.phoneNumber;
            }
            
            if([nameString length] > 0){
                [self.builder addText:[NSString stringWithFormat:NSLocalizedString(@"Name  : %@", nil), nameString]];
                [self.builder addText:@"\n"];
            }
            if([phoneString length] > 0){
                [self.builder addText:[NSString stringWithFormat:NSLocalizedString(@"Phone : %@", nil), phoneString]];
                [self.builder addText:@"\n"];
            }
            
            BOOL printedDesiredTime = NO;
            if(order.pickupAfter != 0 || order.isTakeout || order.isDelivery){
                NSDate *dueDate = nil;
                if(order.pickupAfter != 0){
                    dueDate = [order.orderDate dateByAddingTimeInterval:(order.pickupAfter * 60)];
                }
                if(order.isTakeout || order.isDelivery){
                    [self.builder addTextAlign:EPOS_OC_ALIGN_LEFT];
                    [self.builder addTextSize:1 Height:1];
                    [self.builder addText:[NSString stringWithFormat:NSLocalizedString(@"Desired Time : %@", nil),
                                           (dueDate == nil)?NSLocalizedString(@"ASAP", nil):[dueDate hourMinuteString]]];
                    [self.builder addText:@"\n"];
                    
                }
                printedDesiredTime = YES;
            }
            
            if(([nameString length] > 0) || ([phoneString length] > 0) || printedDesiredTime){
                [self drawDoubleLine];
            }
            
            if(order.orderType == OrderTypeDelivery){
                
                [self printTextLeft:NSLocalizedString(@"Delivery Address", nil)
                          rightText:@""];
                
                [self drawSingleLine];
                
                [self.builder addText:order.address];
                [self.builder addText:@"\n"];
                
                [self drawDoubleLine];
            }
        } else {
            [self.builder addText:@"Checkout Instruction:"];
            [self.builder addText:@"\n"];
            [self.builder addText:@"1. Sign-in and Checkout on your RushOrder app whenever you're ready."];
            [self.builder addText:@"\n"];
            [self.builder addText:@"2. All available promotions or coupons will be applied at checkout."];
            [self.builder addText:@"\n"];
            [self drawSingleLine];
        }
        
        [self.builder addFeedLine:1];
        errorStatus = [self.builder addCut: EPOS_OC_CUT_FEED];
        
        if(self.controller != nil){
            
            if([self connect]){
                int errorStatus = EPOS_OC_SUCCESS;
                unsigned long status;
                errorStatus = [self.controller sendData:self.builder Timeout:10000 Status:&status];
                if(errorStatus != EPOS_OC_SUCCESS){
                    PCError(@"Error to sendData into EPSON printer : %d", errorStatus);
                }
            }
            [self disconnect];
        }
        
    } else {
        PCError(@"Cannot create EPOS builder");
    }
    
    self.builder = nil;
    
    return 0;
}

- (long)printReceipt:(ReceiptWrapper *)receipt atIndex:(NSInteger)paymentIndex
{
    return [self printReceipt:receipt atIndex:paymentIndex enqueueing:YES];
}

- (long)printReceipt:(ReceiptWrapper *)receipt atIndex:(NSInteger)paymentIndex enqueueing:(BOOL)enqueueing
{
    if([self.deviceName length] == 0){
        return RUSHORDER_BXL_PRINTER_NOT_SET;
    }
    
    Order *order = (Order *)receipt.order;
    
    if(order == nil) {
        PCError(@"No order to print for receipt");
        return EPOS_OC_ERR_PARAM;
    }
    
    if([order.paymentList count] <= paymentIndex){
        PCError(@"No Payment to print for receipt");
        return EPOS_OC_ERR_PARAM;
    }
    
    Payment *selectedPayment = [order.paymentList objectAtIndex:paymentIndex];
 
    if(self.builder != nil){
        
        int errorStatus = EPOS_OC_SUCCESS;
        errorStatus = [self.builder addTextLang: EPOS_OC_LANG_EN];
        errorStatus = [self.builder addTextSmooth: EPOS_OC_TRUE];
        errorStatus = [self.builder addTextFont: EPOS_OC_FONT_A];
        errorStatus = [self.builder addTextSize:1 Height:1];
        
        [self.builder addTextAlign:EPOS_OC_ALIGN_CENTER];
        ////////////////////////////////////////////////////////////////////////

        UIImage *logoImage = CRED.merchant.logoImage;
        if(logoImage != nil){
            //        self.controller.attribute = BXL_FT_FONTC;
            

            [self.builder addImage:logoImage
                                 X:0 //(100.0f - logoImage.size.width) / 2
                                 Y:0
                             Width:logoImage.size.width
                            Height:logoImage.size.height
                             Color:EPOS_OC_PARAM_DEFAULT];
            
            [self.builder addText:@"\n"];
        } else {
            if(CRED.merchant.logoURL != nil){
                SDWebImageManager *manager = [SDWebImageManager sharedManager];
                [manager downloadImageWithURL:CRED.merchant.logoURL
                                      options:0
                                     progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                         PCLog(@"%u / %lld", receivedSize, expectedSize);
                                     } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                         if (image && finished)
                                         {
                                             CRED.merchant.logoImage = image;
                                             
                                             NSInteger i = 0;
                                             
                                             for(i = ([self.printQueue count] - 1) ; i >= 0 ; i--){
                                                 id obj = [self.printQueue objectAtIndex:i];
                                                 
                                                 if([obj isKindOfClass:[ReceiptWrapper class]]){
                                                     
                                                     ReceiptWrapper *receipt = (ReceiptWrapper *)obj;
                                                     [self.printQueue removeObjectAtIndex:i];
                                                     
                                                     [self printReceipt:receipt
                                                                atIndex:receipt.paymentIndex
                                                             enqueueing:NO];
                                                     
                                                 }
                                             }
                                         }
                                     }];
                
                if(enqueueing) [self.printQueue addObject:receipt];
                return RUSHORDER_BXL_LOGO_NOT_SET;
                
            } else {
                // Keep going to print
            }
        }
        
        [self.builder addTextAlign:EPOS_OC_ALIGN_CENTER];
        [self.builder addTextSize:2 Height:2];
        
        
        [self.builder addText:CRED.merchant.name];
        [self.builder addText:@"\n"];
        
        [self.builder addTextSize:1 Height:1];
        [self.builder addText:CRED.merchant.multilineAddress];
        [self.builder addText:@"\n"];
        [self.builder addText:CRED.merchant.phoneNumber];
        
        [self.builder addFeedLine:2];
        ////////////////////////////////////////////////////////////////////////
        
    
        ////////////////////////////////////////////////////////////////////////        
        [self drawDoubleLine];
        
        if(order.isTicketBase){
            if(order.isTakeout){
                [self printTextLeft:NSLocalizedString(@"Take-out Order", nil)
                          rightText:[NSString stringWithFormat:@"Ticket No.%lld",order.orderNo]];
            } else if (order.isDelivery){
                [self printTextLeft:NSLocalizedString(@"Delivery Order", nil)
                          rightText:[NSString stringWithFormat:@"Ticket No.%lld",order.orderNo]];
            } else {
                if([order.tableNumber length] > 0){
                    [self printTextLeft:NSLocalizedString(@"Dine-in Order", nil)
                              rightText:[NSString stringWithFormat:@"Table No.%@",order.tableNumber]];
                    
                    [self.builder addTextAlign:EPOS_OC_ALIGN_RIGHT];
                    [self.builder addText:[NSString stringWithFormat:@"Order No.%lld",order.orderNo]];
                } else {
                    [self printTextLeft:NSLocalizedString(@"Dine-in Order", nil)
                              rightText:[NSString stringWithFormat:@"Ticket No.%lld",order.orderNo]];
                }
            }
        } else {
            [self printTextLeft:[NSString stringWithFormat:@"Table No.%@",order.tableNumber]
                      rightText:[NSString stringWithFormat:@"Order No.%lld",order.orderNo]];
        }

        [self.builder addText:[NSString stringWithFormat:@"Transaction No.%lld",selectedPayment.paymentNo]];
        [self.builder addText:@"\n"];

        [self drawSingleLine];
        
        [self.builder addTextAlign:EPOS_OC_ALIGN_RIGHT];
        [self.builder addText:[[selectedPayment.payDate secondsString] stringByAppendingString:@" "]];

        [self.builder addText:@"\n"];

        [self drawDoubleLine];
        ////////////////////////////////////////////////////////////////////////


        NSInteger i = 0;

        for(LineItem *lineItem in selectedPayment.lineItems){

            [self printTextLeft:lineItem.displayMenuName
                      rightText:[[NSNumber numberWithCurrency:(lineItem.unitPrice * lineItem.quantity)] currencyString]];
            
            for(NSDictionary *groupDict in lineItem.menuOptionNamesObj){
                
                NSArray *selectedOptions = [groupDict objectForKey:@"menu_option_names"];
                for(NSDictionary *optionDict in selectedOptions){
                    NSString *optionName = [optionDict objectForKey:@"option_name"]; //option_price
                    PCCurrency optionPrice = [optionDict currencyForKey:@"option_price"];
                    NSString *optionPriceString = nil;
                    if(optionPrice != 0){
                        optionPriceString = [[NSNumber numberWithCurrency:(optionPrice * lineItem.quantity)] currencyString];
                    }
                    [self printTextLeft:[@"    " stringByAppendingString:optionName]
                              rightText:optionPriceString];
                }
            }

            i++;
        }
        
        if(i > 0){
            [self drawSingleLine];
        }

        if(order.orderType == OrderTypeBill){
            [self printTextLeft:NSLocalizedString(@"Subtotal", nil)
                      rightText:order.subTotalString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
            
            [self printTextLeft:NSLocalizedString(@"Tax", nil)
                      rightText:order.taxesString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        } else {
            [self printTextLeft:NSLocalizedString(@"Subtotal", nil)
                      rightText:selectedPayment.subTotalString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
            
            [self printTextLeft:NSLocalizedString(@"Tax", nil)
                      rightText:selectedPayment.taxesString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        }
        
        
        [self printTextLeft:NSLocalizedString(@"Tip", nil)
                  rightText:selectedPayment.tipAmountString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
        
        if(selectedPayment.deliveryFeeAmount > 0){
            [self printTextLeft:NSLocalizedString(@"Delivery Fee", nil)
                      rightText:selectedPayment.deliveryFeeAmountString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        }
        
        if(selectedPayment.roServiceFeeAmount > 0){
            [self printTextLeft:NSLocalizedString(@"Service Fee", nil)
                      rightText:selectedPayment.roServiceFeeAmountString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        }
        
        if(selectedPayment.discountAmount > 0){
            [self printTextLeft:NSLocalizedString(@"Discount", nil)
                      rightText:selectedPayment.discountAmountString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        }
        
        if(order.orderType == OrderTypeBill){
            [self printTextLeft:NSLocalizedString(@"Total", nil)
                      rightText:[[NSNumber numberWithCurrency:(order.total + selectedPayment.tipAmount)] currencyString]
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        } else {
            [self printTextLeft:NSLocalizedString(@"Total", nil)
                      rightText:selectedPayment.grandTotalString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        }

        [self drawSingleLine];
        
        [self printTextLeft:NSLocalizedString(@"Amount Paid", nil)
                  rightText:selectedPayment.netAmountString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
        
        if(selectedPayment.creditAmount > 0){
            [self printTextLeft:NSLocalizedString(@"Points Amount Paid", nil)
                      rightText:selectedPayment.creditAmountString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        }
        
        [self drawSingleLine];
        
        [self.builder addText:[NSString stringWithFormat:NSLocalizedString(@"Card Type  %@",nil), [NSString issuerName:selectedPayment.issuer]]];
        [self.builder addText:@"\n"];
        
        [self.builder addText:[NSString stringWithFormat:NSLocalizedString(@"Acc No.    %@",nil), [selectedPayment formattedCardNumberWithSecureString:@"*"]]];
        [self.builder addText:@"\n"];
        
//        [self.builder addText:[NSString stringWithFormat:NSLocalizedString(@"Charge ID.   %@",nil), selectedPayment.chargeId]];
//        [self.builder addText:@"\n"];
        
        [self drawSingleLine];
        
        if(selectedPayment.rewardedAmount > 0){
            [self printTextLeft:NSLocalizedString(@"Rewarded Point", nil)
                      rightText:selectedPayment.rewardedAmountString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
            [self drawSingleLine];
        }
        
        if(order.isTicketBase){
            
            NSString *nameString = nil;
            NSString *phoneString = nil;
            
            if([order.customers count] > 0){
                if([order.receiverName length] > 0){
                    nameString = order.receiverName;
                } else {
                    NSMutableString *names = [NSMutableString string];
                    
                    for(CustomerInfo *customer in order.customers){
                        if([names length] > 0) [names appendString:@", "];
                        [names appendString:customer.name];
                    }
                    
                    nameString = names;
                }
            } else {
                if([order.receiverName length] > 0){
                    nameString = order.receiverName;
                }
            }
            
            if([order.phoneNumber length] > 0){
                phoneString = order.phoneNumber;
            }
            
            if([nameString length] > 0){
                [self.builder addTextAlign:EPOS_OC_ALIGN_LEFT];
                [self.builder addTextSize:1 Height:1];
                [self.builder addText:[NSString stringWithFormat:NSLocalizedString(@"Name  : %@", nil), nameString]];
                [self.builder addText:@"\n"];
            }
            if([phoneString length] > 0){
                [self.builder addTextAlign:EPOS_OC_ALIGN_LEFT];
                [self.builder addTextSize:1 Height:1];
                [self.builder addText:[NSString stringWithFormat:NSLocalizedString(@"Phone : %@", nil), phoneString]];
                [self.builder addText:@"\n"];
            }
            
            BOOL printedDesiredTime = NO;
            if(order.pickupAfter != 0 || order.isTakeout || order.isDelivery){
                NSDate *dueDate = nil;
                if(order.pickupAfter != 0){
                    dueDate = [order.orderDate dateByAddingTimeInterval:(order.pickupAfter * 60)];
                }
                if(order.isTakeout || order.isDelivery){
                    [self.builder addTextAlign:EPOS_OC_ALIGN_LEFT];
                    [self.builder addTextSize:1 Height:1];
                    [self.builder addText:[NSString stringWithFormat:NSLocalizedString(@"Desired Time : %@", nil),
                                                (dueDate == nil)?NSLocalizedString(@"ASAP", nil):[dueDate hourMinuteString]]];
                    [self.builder addText:@"\n"];

                }
                printedDesiredTime = YES;
            }
            
            if(([nameString length] > 0) || ([phoneString length] > 0) || printedDesiredTime){
                [self drawDoubleLine];
            }
            
            if(order.orderType == OrderTypeDelivery){
                
                [self printTextLeft:NSLocalizedString(@"Delivery Address", nil)
                          rightText:@""];
                
                [self drawSingleLine];
                
                [self.builder addText:order.address];
                [self.builder addText:@"\n"];
                
                [self drawDoubleLine];
            }
        }
        
        
        
        [self.builder addFeedLine:2];
        
        [self.builder addTextAlign:EPOS_OC_ALIGN_CENTER];
        [self.builder addText:NSLocalizedString(@"- Customer Copy -", nil)];
        [self.builder addText:@"\n"];
        [self.builder addText:NSLocalizedString(@"Thank you for dining with us~", nil)];
        
        
        [self.builder addFeedLine:3];
        
        [self drawDoubleLine];
        
        [self.builder addFeedLine:1];
        
        errorStatus = [self.builder addCut: EPOS_OC_CUT_FEED];
        
        
        if(self.controller != nil){
            
            if([self connect]){
                int errorStatus = EPOS_OC_SUCCESS;
                unsigned long status;
                errorStatus = [self.controller sendData:self.builder Timeout:10000 Status:&status];
                if(errorStatus != EPOS_OC_SUCCESS){
                    PCError(@"Error to sendData into EPSON printer : %d", errorStatus);
                }
            }
            [self disconnect];
        }
    } else {
        PCError(@"Cannot create EPOS builder");
    }
    
    self.builder = nil;
    
    return 0;
}

- (void)drawSingleLine
{
    [self.builder addTextAlign:EPOS_OC_ALIGN_LEFT];
    [self.builder addTextSize:1 Height:1];
    [self.builder addText:@"------------------------------------------------\n"];
}

- (void)drawDoubleLine
{
    [self.builder addTextAlign:EPOS_OC_ALIGN_LEFT];
    [self.builder addTextSize:1 Height:1];
    [self.builder addText:@"================================================\n"];

}

- (int)printTextLeft:(NSString *)leftText
           rightText:(NSString *)rightText
      withLeftMargin:(NSInteger)leftMargin
         rightMargin:(NSInteger)rightMargin
{
    return [self printTextLeft:leftText
                     rightText:rightText
                withLeftMargin:leftMargin
                   rightMargin:rightMargin
                     textWidth:1
                        height:1];
}

- (int)printTextLeft:(NSString *)leftText
           rightText:(NSString *)rightText
{
    return [self printTextLeft:leftText
                     rightText:rightText
                     textWidth:1
                        height:1];
}

- (int)printTextLeft:(NSString *)leftText
           rightText:(NSString *)rightText
            textSize:(long)size
{
    return [self printTextLeft:leftText
                     rightText:rightText
                withLeftMargin:0
                   rightMargin:0
                     textWidth:size
                        height:size];
}

- (int)printTextLeft:(NSString *)leftText
           rightText:(NSString *)rightText
           textWidth:(long)width
              height:(long)height
{
    return [self printTextLeft:leftText
                     rightText:rightText
                withLeftMargin:0
                   rightMargin:0
                     textWidth:width
                        height:height];
}

- (int)printTextLeft:(NSString *)leftText
           rightText:(NSString *)rightText
      withLeftMargin:(NSInteger)leftMargin
         rightMargin:(NSInteger)rightMargin
           textWidth:(long)width
              height:(long)height
{
    if(leftText == nil) leftText = @"";
    if(rightText == nil) rightText = @"";
    
    int err = 0;
    
    NSUInteger maxCharacterLength = 48;
    
    switch(width){
        case 1:
            maxCharacterLength = 48;
            break;
        case 2:
            maxCharacterLength = 24;
            break;
        case 3:
            maxCharacterLength = 16;
            break;
        case 4:
            maxCharacterLength = 12;
            break;
        case 5:
            maxCharacterLength = 8;
            break;
        default:
            maxCharacterLength = 6;
            break;
    }
    
    err = [self.builder addTextAlign:EPOS_OC_ALIGN_LEFT];
    err = [self.builder addTextSize:width Height:height];
    
    NSInteger leftTextCount = [leftText length];
    NSInteger rightTextCount = [rightText length];
    NSInteger occupiedCount = leftTextCount + rightTextCount + leftMargin + rightMargin;
    
    if(leftMargin > 0){
        
        occupiedCount -= leftTextCount;
        
        NSString *leftMarginString = nil;
        
        switch(width){
            case 1:
                leftMarginString = @"                                                "; //48
                break;
            case 2:
                leftMarginString = @"                        "; //24
                break;
            case 3:
                leftMarginString = @"                "; //16
                break;
            case 4:
                leftMarginString = @"            "; //12
                break;
            case 5:
                leftMarginString = @"        "; //8
                break;
            default:
                leftMarginString = @"      "; //6
                break;
        }
        
        NSInteger index = (maxCharacterLength - leftMargin) + leftTextCount;
        index = MAX(index, 0);
        leftMarginString = [leftMarginString substringFromIndex:index];
        err = [self.builder addText:leftMarginString];
    }
    
    
    if(occupiedCount > maxCharacterLength){
        NSInteger restCount = occupiedCount - maxCharacterLength;
        
        leftText = [leftText substringToIndex:([leftText length] - (restCount + 1))];
        occupiedCount -= (restCount + 1);
    }
    err = [self.builder addText:leftText];
   
    NSString *spaceString = nil;
    
    switch(width){
        case 1:
            spaceString = @"                                                "; //48
            break;
        case 2:
            spaceString = @"                        "; //24
            break;
        case 3:
            spaceString = @"                "; //16
            break;
        case 4:
            spaceString = @"            "; //12
            break;
        case 5:
            spaceString = @"        "; //8
            break;
        default:
            spaceString = @"      "; //6
            break;
    }
    
//    NSInteger subIndex = occupiedCount;
    occupiedCount = MAX(occupiedCount, 0);
    
    spaceString = [spaceString substringFromIndex:occupiedCount];
    
    err = [self.builder addText:spaceString];
    err = [self.builder addText:rightText];
    err = [self.builder addText:@"\n"];
    
    return err;
}



- (void)treatPrintError:(long)errorCode
{
//    switch(errorCode){
//        case RUSHORDER_BXL_CONNECT_ERROR:
//            [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot connect to printer", nil)
//                                message:NSLocalizedString(@"Check your printer connected properly.", nil)
//                               delegate:self
//                                    tag:105];
//            return;
//        case RUSHORDER_BXL_PRINTER_NOT_FOUND:
//            [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot find any printers connected", nil)
//                                message:NSLocalizedString(@"Be sure the printer connected properly to the local network.", nil) // If you don't have printer, turn off Auto Printing at \"Settings\"
//                               delegate:self
//                                    tag:105];
//            return;
//        case BXL_BC_DATA_ERROR:
//            [UIAlertView alertWithTitle:NSLocalizedString(@"There is no data for printing", nil)
//                                message:NSLocalizedString(@"Each kind of printing needs appropriate data.", nil) // If you don't have printer, turn off Auto Printing at \"Settings\"
//                               delegate:self
//                                    tag:105];
//            return;
//        default:
//            return;
//    }
}


- (NSMutableArray *)printQueue
{
    if(_printQueue == nil){
        _printQueue = [NSMutableArray array];
    }
    return _printQueue;
}

- (EposPrint *)controller
{
    if(_controller == nil){
        _controller = [[EposPrint alloc] init];
    }
    return _controller;
}


- (EposBuilder *)builder
{
    if(_builder == nil){
        _builder = [[EposBuilder alloc] initWithPrinterModel:@"TM-T88V" Lang: EPOS_OC_MODEL_ANK];
    }
    
    return _builder;
}

@end
