//
//  AlertManager.m
//  RushOrder
//
//  Created by Conan on 21/4/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import "AlertManager.h"

@interface AlertManager()
@property (strong, nonatomic) UIButton *alertViewButton;
@end

@implementation AlertManager

+ (AlertManager *)sharedManager
{
    static AlertManager *aManager = nil;
    
    @synchronized(self){
        if(aManager == nil){
            aManager = [[self alloc] init];
        }
    }
    
    return aManager;
}

- (id)init
{
    self = [super init];
    if(self != nil){
       
    }
    return self;
}

- (void)dealloc
{
    [self turnOffAlertView];
}

- (UIButton *)alertViewButton
{
    if(_alertViewButton == nil){
        _alertViewButton = [[UIButton alloc] initWithFrame:APP.window.frame];
        _alertViewButton.backgroundColor = [UIColor c11Color];
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){

        }
        [_alertViewButton setImage:[UIImage imageNamed:@"logo_whtie"]
                          forState:UIControlStateNormal];
        [_alertViewButton addTarget:self
                             action:@selector(alertViewButtonTouched:)
                   forControlEvents:UIControlEventTouchDown];
    }
    return _alertViewButton;
}

- (void)alertViewButtonTouched:(id)sender
{
    [self turnOffAlertView];
}

- (void)turnOffAlertView
{
    [self.alertViewButton removeFromSuperview];
    [self.alertViewButton.layer removeAllAnimations];
}

- (void)turnOnAlertView
{
    [self turnOnAlertViewInView:nil];
}

- (void)turnOnAlertViewInView:(UIView *)view
{
    if(view == nil){
        CGRect newFrame = CGRectZero;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            if(OVER_IOS8){
                newFrame.size.width = APP.window.rootViewController.view.frame.size.width;
                newFrame.size.height = APP.window.rootViewController.view.frame.size.height;
            } else {
                newFrame.size.width = APP.window.rootViewController.view.frame.size.height;
                newFrame.size.height = APP.window.rootViewController.view.frame.size.width;
            }
        } else {
            newFrame.size.width = APP.window.rootViewController.view.frame.size.width;
            newFrame.size.height = APP.window.rootViewController.view.frame.size.height;
        }
        self.alertViewButton.frame = newFrame;
        [APP.window.rootViewController.view addSubview:self.alertViewButton];
    } else {
        self.alertViewButton.frame = view.bounds;
        [view addSubview:self.alertViewButton];
    }
    
    [UIView animateWithDuration:0.6f
                          delay:0.0f
                        options:
     UIViewAnimationOptionAllowUserInteraction |
     UIViewAnimationOptionRepeat
                     animations:^(){
                         self.alertViewButton.selected = !self.alertViewButton.selected;
                         self.alertViewButton.alpha = self.alertViewButton.selected ? 0.2f : 0.7f;
                     }
                     completion:^(BOOL finished){
                         
                     }];
}


@end
