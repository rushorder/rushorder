//
//  EpsonPrintManager.h
//  RushOrder
//
//  Created by Conan on 8/30/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ePOS-Print.h"
#import "Order.h"
#import "Cart.h"
#import "PCPrintManagerProtocol.h"
#import "PrintWrappers.h"

#define EPSON [EpsonPrintManager sharedManager]
#define EPSONCON [EpsonPrintManager sharedManager].controller

@interface EpsonPrintManager : NSObject

@property (strong, nonatomic) EposPrint *controller;
@property (copy, nonatomic) NSString *deviceName;

+ (EpsonPrintManager *)sharedManager;

- (BOOL)connect;
- (void)disconnect;

- (long)printOrder:(Order *)order;
- (long)printCart:(Cart *)cart;
- (long)printBill:(BillWrapper *)bill;
- (long)printReceipt:(ReceiptWrapper *)receipt atIndex:(NSInteger)paymentIndex;
- (long)printTest;

- (void)treatPrintError:(long)errorCode;

@end
