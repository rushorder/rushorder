//
//  AlertManager.h
//  RushOrder
//
//  Created by Conan on 4/21/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ALERT [AlertManager sharedManager]


@interface AlertManager : NSObject

+ (AlertManager *)sharedManager;

- (void)turnOffAlertView;
- (void)turnOnAlertView;
- (void)turnOnAlertViewInView:(UIView *)view;

@end