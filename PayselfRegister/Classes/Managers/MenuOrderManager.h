//
//  MenuOrderManager.h
//  RushOrder
//
//  Created by Conan on 5/20/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MenuItem.h"
#import "LineItem.h"
#import "TableInfo.h"
#import "Cart.h"

#define MENUORDER [MenuOrderManager sharedManager]

extern NSString * const TableListChangedNotification;
extern NSString * const MerchantInformationUpdatedNotification;

@interface MenuOrderManager : NSObject

@property (strong, nonatomic) Merchant *merchant;
@property (strong, nonatomic) NSArray *tableList;
@property (strong, nonatomic) Order *order;
@property (strong, nonatomic) Cart *cart;
@property (strong, nonatomic) TableInfo *tableInfo;

@property (nonatomic) BOOL setTable;
@property (nonatomic, readonly) BOOL canCheckOut;
@property (nonatomic, readonly) PCCurrency tempSubtotal;
@property (nonatomic, readonly) NSString *tempSubtotalString;
@property (nonatomic, readonly) PCCurrency tempTaxes;
@property (nonatomic, readonly) NSString *tempTaxesString;
@property (nonatomic, readonly) PCCurrency tempTotal;
@property (nonatomic, readonly) NSString *tempTotalString;

@property (nonatomic, getter = isLoading) BOOL loading;
@property (strong, nonatomic) NSDate *lastUpdatedDate;


@property (readonly) BOOL shouldOpenRightSlide;

+ (MenuOrderManager *)sharedManager;


- (void)resetGraph;
- (void)resetGraphWithMerchant:(Merchant *)merchant;

- (BOOL)checkTable:(TableInfo *)tableInfo;

- (BOOL)checkTable:(TableInfo *)tableInfo
        assertMine:(BOOL)assertMine;

- (BOOL)checkTable:(TableInfo *)tableInfo
           success:(void (^)())successBlock
           failure:(void (^)())failureBlock;

- (BOOL)checkTable:(TableInfo *)tableInfo
        assertMine:(BOOL)assertMine
           success:(void (^)())successBlock
           failure:(void (^)())failureBlock;

@end