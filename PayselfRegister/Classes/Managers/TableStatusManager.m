//
//  TableStatusManager.m
//  RushOrder
//
//  Created by Conan on 3/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "TableStatusManager.h"
#import "TableInfo.h"
#import "PCMerchantService.h"
#import "PCMenuService.h"
#import "MenuManager.h"
#import "AlertManager.h"
#import "PCiAppDelegate.h"
#import "BixPrintManager.h"
#import "EpsonPrintManager.h"
#import "StarPrintManager.h"

NSString * const TogoListUpdatedNotification = @"TogoListUpdatedNotification";
NSString * const TableListUpdatedNotification = @"TableListUpdatedNotification";
NSString * const TableListErrorNotification = @"TableListErrorNotification";
NSString * const SumCountUpdatedNotification = @"SumCountUpdatedNotification";
NSString * const TableInfoChangedNotification = @"TableInfoChangedNotification";
NSString * const OrderUpdatedNotification = @"OrderUpdatedNotification";

@interface TableStatusManager()
@property (strong, atomic) NSArray *lastOrderList;
@property (strong, atomic) NSArray *activeBillList;

@property (strong, nonatomic) NSTimer *heartbeatTimer;


@end

@implementation TableStatusManager

+ (TableStatusManager *)sharedManager
{
    static TableStatusManager *aManager = nil;
    
    @synchronized(self){
        if(aManager == nil){
            aManager = [[self alloc] init];
        }
    }
    
    return aManager;
}

- (id)init
{
    self = [super init];
    if(self != nil){
        self.paymentOrderNo = NSNotFound;
        self.paymentPaymentNo = NSNotFound;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(SignedIn:)
                                                     name:SignedInNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(SignedOut:)
                                                     name:SignedOutNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(tableInfoChanged:)
                                                     name:TableInfoChangedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(orderUpdated:)
                                                     name:OrderUpdatedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didBecomeActive:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didEnterBackground:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
        
        [self reloadTableStatus];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)SignedIn:(NSNotification *)aNoti
{
    [self reloadTableStatus];
}

- (void)requestTableLables
{
    RequestResult result = RRFail;
    result = [MERCHANT requestTableLabels:CRED.merchant.merchantNo
                          completionBlock:
              ^(BOOL isSuccess, NSMutableArray *tableList, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
//                      self.tableList = tableList;
                      
                      
                      //                      [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting table labels", nil)];
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)SignedOut:(NSNotification *)aNoti
{
    [self stopHeartBeat];
    [self resetList];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:TableListUpdatedNotification
                                                        object:self
                                                      userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES]
                                                                                           forKey:@"signedOutKey"]];
}

- (void)resetList
{
    [self resetLazyList];
    
    self.tableList = nil;
    self.tableOnlyList = nil;
}

- (void)resetLazyList
{
    self.activeTableList = nil;
    self.notEmptyTableList = nil;
    self.waitingTableList = nil;
    self.newTableList = nil;
    self.preparingTableList = nil;
    self.readyTableList = nil;
}

- (void)orderUpdated:(NSNotification *)aNoti
{    
    BOOL isOrder = NO;
    
    id obj = [aNoti.userInfo objectForKey:@"tableInfo"];
    
    if(obj == nil){
        obj = [aNoti.userInfo objectForKey:@"order"];
        isOrder = YES;
    }
    
    if(obj != nil){
        if(!isOrder){
            TableInfo *castedTableInfo = (TableInfo *)obj;
            
            switch(castedTableInfo.bfStatus){
                case TableStatusBillReqeusted:
                case TableStatusOrdering:
                case TableStatusOrderSubmitted:
                    self.billRequestedCount--;
                    break;
                case TableStatusBillIssued:
                    self.billIssuedCount--;
                    break;
                case TableStatusPartialPaid:
                    self.partialPaidCount--;
                    break;
                case TableStatusCompleted:
                    self.completedCount--;
                    break;
                case TableStatusUnknown:
                case TableStatusEmpty:
                default:
                    self.emptyCount--;
                    break;
            }
            
            switch(castedTableInfo.status){
                case TableStatusBillReqeusted:
                case TableStatusOrdering:
                case TableStatusOrderSubmitted:
                    self.billRequestedCount++;
                    break;
                case TableStatusBillIssued:
                    self.billIssuedCount++;
                    break;
                case TableStatusPartialPaid:
                    self.partialPaidCount++;
                    break;
                case TableStatusCompleted:
                    self.completedCount++;
                    break;
                case TableStatusUnknown:
                case TableStatusEmpty:
                default:
                    self.emptyCount++;
                    break;
            }
            
        } else {
            Order *castedOrder = (Order *)obj;
            
            switch(castedOrder.orderType){
                case OrderTypeCart:
                case OrderTypeBill:
                case OrderTypeOrderOnly:
                    
                    PCWarning(@"Separated Order not allowd type cannot counted here: %@", castedOrder);
                    
                    break;
                case OrderTypePickup:
                case OrderTypeTakeout:
                case OrderTypeDelivery:
                    switch(castedOrder.bfStatus){
                        case OrderStatusDraft:
                            self.billIssuedCount--;
                            break;
                        case OrderStatusPaid:
                            self.partialPaidCount--;
                            break;
                        case OrderStatusConfirmed:
                        case OrderStatusAccepted:
                            self.completedCount--;
                            break;
                        case OrderStatusReady:
                        case OrderStatusPrepared:
                            self.billRequestedCount--;
                            break;
                        default:

                            break;
                    }
                    break;
                default:
                    
                    break;
            }
            
            switch(castedOrder.orderType){
                case OrderTypeCart:
                case OrderTypeBill:
                case OrderTypeOrderOnly:
                    
                    PCWarning(@"Separated Order not allowd type cannot counted here: %@", castedOrder);
                    
                    break;
                case OrderTypePickup:
                case OrderTypeTakeout:
                case OrderTypeDelivery:
                    switch(castedOrder.status){
                        case OrderStatusDraft:
                            self.billIssuedCount++;
                            break;
                        case OrderStatusPaid:
                            self.partialPaidCount++;
                            break;
                        case OrderStatusConfirmed:
                        case OrderStatusAccepted:
                            self.completedCount++;
                            break;
                        case OrderStatusReady:
                        case OrderStatusPrepared:
                            self.billRequestedCount++;
                            break;
                        default:
                            
                            break;
                    }
                    break;
                default:
                    
                    break;
            }
            
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SumCountUpdatedNotification
                                                        object:obj
                                                      userInfo:nil];
}

- (void)tableInfoChanged:(NSNotification *)aNoti
{
    BOOL isOrder = NO;
    
    id obj = [aNoti.userInfo objectForKey:@"tableInfo"];
    
    if(obj == nil){
        obj = [aNoti.userInfo objectForKey:@"order"];
        isOrder = YES;
    }
    
    NSUInteger objIndex = [self.activeTableList indexOfObject:obj];
    
    NSUInteger fromIndex = NSNotFound;
    
    NSIndexPath *fromIndexPath = nil;
    NSIndexPath *toIndexPath = nil;
    
    if(objIndex == NSNotFound){
        if(isOrder){
            Order *castedOrder = (Order *)obj;
            
            if(castedOrder.orderType == OrderTypeCart){
                self.emptyCount--;
            }
        } else {
            self.emptyCount--;
        }
    } else {
        id exObj = [self.activeTableList objectAtIndex:objIndex];
        
        if([exObj isKindOfClass:[TableInfo class]]){
            TableInfo *castedTableInfo = (TableInfo *)exObj;
            
            switch(castedTableInfo.bfStatus){
                case TableStatusBillReqeusted:
                case TableStatusOrdering:
                case TableStatusOrderSubmitted:
                    self.billRequestedCount--;
                    break;
                case TableStatusBillIssued:
                    self.billIssuedCount--;
                    break;
                case TableStatusPartialPaid:
                    self.partialPaidCount--;
                    break;
                case TableStatusCompleted:
                    self.completedCount--;
                    break;
                case TableStatusUnknown:	
                case TableStatusEmpty:
                default:
                    self.emptyCount--;
                    break;
            }
            
        } else {
            Order *castedOrder = (Order *)exObj;
            switch(castedOrder.orderType){
                case OrderTypeCart:
                case OrderTypeBill:
                case OrderTypeOrderOnly:
                    
                    PCWarning(@"Separated Order not allowd type cannot counted here: %@", castedOrder);
                    
                    break;
                case OrderTypePickup:
                case OrderTypeTakeout:
                case OrderTypeDelivery:
                    switch(castedOrder.bfStatus){
                        case OrderStatusDraft:
                            self.billIssuedCount--;
                            fromIndex = [self.newTableList indexOfObject:castedOrder];
                            if(fromIndex != NSNotFound){
                                fromIndexPath = [NSIndexPath indexPathForRow:fromIndex
                                                                   inSection:0];
                                [self.newTableList removeObjectAtIndex:fromIndex];
                            } else {
                                
                            }
                            break;
                        case OrderStatusPaid:
                            self.partialPaidCount--;
                            fromIndex = [self.newTableList indexOfObject:castedOrder];
                            if(fromIndex != NSNotFound){
                                fromIndexPath = [NSIndexPath indexPathForRow:fromIndex
                                                                   inSection:0];
                                [self.newTableList removeObjectAtIndex:fromIndex];
                            } else {
                                
                            }
                            break;
                        case OrderStatusConfirmed:
                        case OrderStatusAccepted:
                            self.completedCount--;
                            fromIndex = [self.preparingTableList indexOfObject:castedOrder];
                            if(fromIndex != NSNotFound){
                                fromIndexPath = [NSIndexPath indexPathForRow:fromIndex
                                                                   inSection:1];
                                [self.preparingTableList removeObjectAtIndex:fromIndex];
                            } else {
                                
                            }
                            break;
                        case OrderStatusReady:
                        case OrderStatusPrepared:
                            self.billRequestedCount--;
                            fromIndex = [self.readyTableList indexOfObject:castedOrder];
                            if(fromIndex != NSNotFound){
                                fromIndexPath = [NSIndexPath indexPathForRow:fromIndex
                                                                   inSection:2];
                                [self.readyTableList removeObjectAtIndex:fromIndex];
                            } else {
                                
                            }
                            break;
                        default:
                            
                            break;
                    }
                    break;
                default:
                    
                    break;
            }
        }
    }
    
    NSArray *removedIndexPaths = nil;
    NSArray *addedIndexPaths = nil;
    NSArray *updatedIndexPaths = nil;
    
    if(isOrder){
        
        Order *order = (Order *)obj;
        
        if(order.status == OrderStatusFixed || order.status == OrderStatusCanceled || order.status == OrderStatusDeclined){
            if(objIndex != NSNotFound){
                [self.activeTableList removeObjectAtIndex:objIndex];
                removedIndexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:objIndex
                                                                                inSection:0]];
            }
        } else {
            if(objIndex == NSNotFound){
                //Add
                [self.activeTableList insertObject:order
                                           atIndex:0];
                addedIndexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:0
                                                                              inSection:0]];
            } else {
                //Replace
                [self.activeTableList replaceObjectAtIndex:objIndex
                                                withObject:order];
                updatedIndexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:objIndex
                                                                                inSection:0]];
            }
            
            switch(order.orderType){
                case OrderTypeCart:
                case OrderTypeBill:
                case OrderTypeOrderOnly:
                    
                    PCWarning(@"Separated Order not allowd type cannot counted here: %@", order);
                    
                    break;
                case OrderTypePickup:
                case OrderTypeTakeout:
                case OrderTypeDelivery:
                {
                    
                    NSMutableArray *targetList = nil;
                    NSUInteger section = NSNotFound;
                    switch(order.status){
                        case OrderStatusDraft:
                            self.billIssuedCount++;
                            targetList = self.newTableList;
                            section = 0;
                            break;
                        case OrderStatusConfirmed:
                        case OrderStatusAccepted:
                            self.completedCount++;
                            targetList = self.preparingTableList;
                            section = 1;
                            break;
                        case OrderStatusReady:
                        case OrderStatusPrepared:
                            self.billRequestedCount++;
                            targetList = self.readyTableList;
                            section = 2;
                            break;
                        default:
                            
                            break;
                    }
                    
                    if(targetList != nil){
                        NSUInteger insertedIndex = [targetList indexOfObject:order
                                                               inSortedRange:NSMakeRange(0, [targetList count])
                                                                     options:NSBinarySearchingInsertionIndex
                                                             usingComparator:^NSComparisonResult(id obj1, id obj2) {
                                                                 return [obj1 compareTimeWith:obj2];
                                                             }];
                        [targetList insertObject:order
                                         atIndex:insertedIndex];
                        toIndexPath = [NSIndexPath indexPathForRow:insertedIndex
                                                         inSection:section];
                    }
                    
                }
                    break;
                default:
                    
                    break;
            }
        }

    } else {
        TableInfo *tableInfo = (TableInfo *)obj;
        
        if((tableInfo.status & (TableStatusFixed | TableStatusEmpty)) > 0){
            if(objIndex != NSNotFound){
                [self.activeTableList removeObjectAtIndex:objIndex];
                removedIndexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:objIndex
                                                                                inSection:0]];
                
                self.emptyCount++;
            }
        } else {
            if(objIndex == NSNotFound){
                //Add
                [self.activeTableList insertObject:tableInfo
                                           atIndex:0];
                addedIndexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:0
                                                                              inSection:0]];
            } else {
                //Replace
                [self.activeTableList replaceObjectAtIndex:objIndex
                                                withObject:tableInfo];
                updatedIndexPaths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:objIndex
                                                                                inSection:0]];
            }
            
            switch(tableInfo.status){
                case TableStatusBillReqeusted:
                case TableStatusOrderSubmitted:
                case TableStatusOrdering:
                    self.billRequestedCount++;
                    break;
                case TableStatusBillIssued:
                    self.billIssuedCount++;
                    break;
                case TableStatusPartialPaid:
                    self.partialPaidCount++;
                    break;
                case TableStatusCompleted:
                    self.completedCount++;
                    break;
                case TableStatusUnknown:
                case TableStatusEmpty:
                default:
                    self.emptyCount++;
                    break;
            }
        }
    }
    
    NSMutableDictionary *userInfoDict = [NSMutableDictionary dictionary];
    
    if(removedIndexPaths != nil){
        [userInfoDict setObject:removedIndexPaths forKey:@"removedIndexPaths"];
    }
    
    if(addedIndexPaths != nil){
        [userInfoDict setObject:addedIndexPaths forKey:@"addedIndexPaths"];
    }
    
    if(updatedIndexPaths != nil){
        [userInfoDict setObject:updatedIndexPaths forKey:@"updatedIndexPaths"];
    }
    
    if(fromIndexPath != nil && toIndexPath != nil){
        [userInfoDict setObject:fromIndexPath forKey:@"shiftFromIndexPath"];
        [userInfoDict setObject:toIndexPath forKey:@"shiftToIndexPath"];
    }
    
    if(fromIndexPath != nil){
        [userInfoDict setObject:fromIndexPath forKey:@"shiftFromIndexPath"];
    }
    
    if(toIndexPath != nil){
        [userInfoDict setObject:toIndexPath forKey:@"shiftToIndexPath"];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:TableListUpdatedNotification
                                                        object:self
                                                      userInfo:userInfoDict];
}

- (void)didBecomeActive:(NSNotification *)aNoti
{
    [self reloadTableStatusIfNeeded];
}

- (void)didEnterBackground:(NSNotification *)aNoti
{
    self.dirty = YES;
}

- (BOOL)reloadTableStatusIfNeeded
{
    if(self.isDirty){
        return [self reloadTableStatus];
    }
    
    return NO;
}

- (BOOL)reloadTableStatus
{
    return [self reloadTableStatus:NO];
}

- (BOOL)reloadTableStatus:(BOOL)withAlert
{
    self.dirty = NO;
    
    if(CRED.merchant.merchantNo > 0){
        return [self requestOrderStatus:withAlert];
    } else {
        PCWarning(@"Merchant number is not defined for reloadTableStatus");
    }
    
    return NO;
}

- (BOOL)requestOrderStatus
{
    return [self requestOrderStatus:NO];
}

- (BOOL)requestOrderStatus:(BOOL)withAlert
{
    RequestResult result = RRFail;
    result = [MERCHANT requestOrderStatus:CRED.merchant.merchantNo
                          completionBlock:
              ^(BOOL isSuccess, NSDictionary *tableStatusDict, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      
                      [self heartbeatTimer];
                      
                      self.newOrderCount = [tableStatusDict integerForKey:@"newOrderCount"];
                      
                      if(CRED.merchant.isTableBase){
                          self.completedCount = [tableStatusDict integerForKey:@"completedCount"];
                          self.billRequestedCount = [tableStatusDict integerForKey:@"billRequestedCount"];
                          self.partialPaidCount = [tableStatusDict integerForKey:@"partialPaidCount"];
                          self.billIssuedCount = [tableStatusDict integerForKey:@"billIssuedCount"];
                          self.emptyCount = [tableStatusDict integerForKey:@"emptyCount"];
                      } else {
                          self.completedCount = [tableStatusDict integerForKey:@"confirmedCount"];
                          self.billRequestedCount = [tableStatusDict integerForKey:@"readyCount"];
                          self.partialPaidCount = [tableStatusDict integerForKey:@"paidCount"];
                          self.billIssuedCount = [tableStatusDict integerForKey:@"draftCount"];
                      }
                      
                      [self resetLazyList];
                      
                      self.tableList = [NSMutableArray array];
                      self.tableOnlyList = [tableStatusDict objectForKey:@"tableList"];
                      if([self.tableOnlyList count] > 0){
                          [self.tableList addObjectsFromArray:self.tableOnlyList];
                      }
                      
                      NSArray *orderArray = [tableStatusDict objectForKey:@"orderList"];
                      if([orderArray count] > 0){
                          [self.tableList addObjectsFromArray:orderArray];
                      }
                      
                      if(self.newOrderCount > 0 && withAlert){
                          if([NSUserDefaults standardUserDefaults].blinkWithNewOrder){
                              [ALERT turnOnAlertViewInView:nil];
                          }
                      }
                      
                      [[NSNotificationCenter defaultCenter] postNotificationName:TableListUpdatedNotification
                                                                          object:self
                                                                        userInfo:nil];
                      
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      
                      [[NSNotificationCenter defaultCenter] postNotificationName:TableListErrorNotification
                                                                          object:self
                                                                        userInfo:nil];
                      
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                      
                      [[NSNotificationCenter defaultCenter] postNotificationName:TableListErrorNotification
                                                                          object:self
                                                                        userInfo:nil];
                  }
                  
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            if(![SVProgressHUD isVisible]){
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            }
            return YES;
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
    return NO;
}


- (void)requestOrderAndCartAtTable:(TableInfo *)tableInfo
{
    if(tableInfo == nil){
        PCError(@"TableInfo should not be nil");
        return;
    }
    
    RequestResult result = RRFail;
    result = [MENU requestOrderAndCart:CRED.merchant.merchantNo
                             tableInfo:tableInfo
                       completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode) {
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              NSDictionary *cartDict = [response objectForKey:@"cart"];
                              
                              if([cartDict objectForKey:@"id"] != nil){
                                  
                                  tableInfo.cart = [[Cart alloc] init];
                                  tableInfo.cart.merchant = CRED.merchant;
                                  tableInfo.cart.merchantNo = CRED.merchant.merchantNo;
//                                  tableInfo.cart.tableNo = tableInfo.tableNo;
                                  tableInfo.cart.tableLabel = tableInfo.tableNumber;
                                  
                                  
                              } else {
                                  tableInfo.cart = nil;
                              }
                              
                              [tableInfo.cart updateWithDictionary:cartDict menuPan:MENUPAN.wholeSerialMenuList];
                              
                              
                              [[NSNotificationCenter defaultCenter] postNotificationName:TableListUpdatedNotification
                                                                                  object:self
                                                                                userInfo:nil];
                          }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting menu", nil)];
                              }
                          }
                              
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                      
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            
            break;
        default:
            break;
    }
}

- (NSMutableArray *)notEmptyTableList
{
    if(self.tableList == nil)
        return nil;
    
    if(_notEmptyTableList == nil){
        NSInteger notEmptyTableStatus = TableStatusAll;
        
        _notEmptyTableList = [NSMutableArray array];
        
        for(TableInfo *aTableInfo in self.tableList){
            if(((aTableInfo.status & notEmptyTableStatus) == aTableInfo.status) &&
               [aTableInfo isKindOfClass:[TableInfo class]]){
                [_notEmptyTableList addObject:aTableInfo];
            }
        }
    }
    return _notEmptyTableList;
}

- (NSMutableArray *)activeTableList
{
    if(self.tableList == nil)
        return nil;
    
    if(_activeTableList == nil){
        NSInteger activeTableStatus = TableStatusNotEmpty;
        
        _activeTableList = [NSMutableArray array];
        _newTableList = [NSMutableArray array];
        _preparingTableList = [NSMutableArray array];
        _readyTableList = [NSMutableArray array];
        
        PCSerial lastOrderNumber = [NSUserDefaults standardUserDefaults].lastNewPrintedNumber;
        PCSerial lastCartNumber = [NSUserDefaults standardUserDefaults].lastNewPrintedCartNumber;
        
        NSMutableArray *printingOrderList = [NSMutableArray array];
        
        for(id obj in self.tableList){
            if([obj isKindOfClass:[TableInfo class]]){
                TableInfo *aTableInfo = (TableInfo *)obj;
                if((aTableInfo.status & activeTableStatus) == aTableInfo.status
                   || (aTableInfo.cart != nil)){
                    
                    if([NSUserDefaults standardUserDefaults].printWhenNewOrder){
                        if(aTableInfo.status == TableStatusOrderSubmitted){
                            if(aTableInfo.cart.cartNo > lastCartNumber){
                                [printingOrderList addObject:aTableInfo.cart];
                            }
                        }
                    } else {
                        [NSUserDefaults standardUserDefaults].lastNewPrintedCartNumber = aTableInfo.cart.cartNo;
                    }
                    
                    if([NSUserDefaults standardUserDefaults].autoReceiptPrint){
                        if(self.paymentOrderNo != NSNotFound
                           && self.paymentPaymentNo != NSNotFound){
                            if(self.paymentOrderNo == aTableInfo.order.orderNo){
                                if(aTableInfo.order != nil){
                                    [printingOrderList addObject:aTableInfo.order];
                                }
                            }
                        }
                    }
                    
                    [_activeTableList addObject:aTableInfo];
                }
            } else if ([obj isKindOfClass:[Order class]]){
                Order *order = (Order *)obj;
                
                BOOL isActive = NO;
                switch(order.status){
                    case OrderStatusDraft:
                    case OrderStatusPaid:
                        [self.newTableList addObject:order];
                        
                        if([NSUserDefaults standardUserDefaults].printWhenNewOrder){
                            if(order.orderNo > lastOrderNumber){
                                [printingOrderList addObject:order];
                            }
                        } else {
                            [NSUserDefaults standardUserDefaults].lastNewPrintedNumber = order.orderNo;
                        }
                        isActive = YES;
                        break;
                    case OrderStatusConfirmed:
                    case OrderStatusAccepted:
                        [self.preparingTableList addObject:order];
                        isActive = YES;
                        break;
                    case OrderStatusReady:
                    case OrderStatusPrepared:
                    case OrderStatusCompleted:
                        [self.readyTableList addObject:order];
                        isActive = YES;
                        break;
                    case OrderStatusFixed:
                    case OrderStatusCanceled:
                    case OrderStatusDeclined:
                    case OrderStatusRefused:
                    case OrderStatusUnknown:
                    default:
                        break;
                        
                }
                
                if(isActive){
                    [_activeTableList insertObject:order atIndex:0];
                }
            }
        }
        
        if([printingOrderList count] > 0){
            [self printNewCartOrderList:printingOrderList];
        } else {
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
    }
    return _activeTableList;
}

- (void)printNewCartOrderList:(NSMutableArray *)orderList
{
    for(id obj in orderList){
        if([obj isKindOfClass:[Order class]]){
            Order *order = (Order *)obj;
            
            if([NSUserDefaults standardUserDefaults].autoReceiptPrint){
                if(order.status == OrderStatusConfirmed || order.status == OrderStatusCompleted){
                    if(self.paymentOrderNo == order.orderNo
                       && self.paymentPaymentNo != NSNotFound){
                        [self requestOrderForPrinting:order forReceipt:YES];
                    }
                }
            }
            
            if([NSUserDefaults standardUserDefaults].printWhenNewOrder
               && order.status == OrderStatusPaid){
                if(order.orderNo > [NSUserDefaults standardUserDefaults].lastNewPrintedNumber){
                    if([NSUserDefaults standardUserDefaults].autoReceiptPrint
                       || [NSUserDefaults standardUserDefaults].autoKitchenPrint){
                        [self requestOrderForPrinting:order];
                    }
                }
            }
            
            [NSUserDefaults standardUserDefaults].lastNewPrintedNumber = order.orderNo;
            
        } else if([obj isKindOfClass:[Cart class]]){
            Cart *cart = (Cart *)obj;
            
            if([NSUserDefaults standardUserDefaults].autoKitchenPrint
//               || [NSUserDefaults standardUserDefaults].autoBillPrint
               ){
                [self requestCartForPrinting:cart];
            }
            
            [NSUserDefaults standardUserDefaults].lastNewPrintedCartNumber = cart.cartNo;
        }
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)requestOrderForPrinting:(Order *)order
{
    [self requestOrderForPrinting:order forReceipt:NO];
}

- (void)requestOrderForPrinting:(Order *)order forReceipt:(BOOL)forReceipt
{
    RequestResult result = RRFail;
    result = [MERCHANT requestOrder:order.orderNo
                    removeCompleted:NO
                    completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              order.paymentList = [response objectForKey:@"payments"];
                              
                              NSDictionary *orderDict = response.data;
                              [order updateWithDictionary:orderDict
                                                      merchant:nil
                                                       menuPan:MENUPAN.wholeSerialMenuList];
                              
                              [order updateSumWithPayments:order.paymentList];
                              
                              long bixRtn = 0;
                              
                              if(forReceipt){
                                  
                                  long bixRtn = 0;
                                  
                                  NSInteger paymentIndex = 0;
                                  
                                  for(Payment *payment in order.paymentList){
                                      if(payment.paymentNo == self.paymentPaymentNo){
                                          break;
                                      }
                                      paymentIndex++;
                                  }
                                  
                                  switch(APP.selectedMakerCode){
                                      case PrinterMakerEpson:
                                          bixRtn = [EPSON printReceipt:[[ReceiptWrapper alloc] initWithOrder:order
                                                                                            withPaymentIndex:paymentIndex]
                                                               atIndex:paymentIndex];
                                          break;
                                      case PrinterMakerStar:
                                          bixRtn = [STAR printReceipt:[[ReceiptWrapper alloc] initWithOrder:order
                                                                                           withPaymentIndex:paymentIndex]
                                                              atIndex:paymentIndex];
                                          break;
                                      case PrinterMakerBixolon:
                                          bixRtn = [BIX printReceipt:[[ReceiptWrapper alloc] initWithOrder:order
                                                                                          withPaymentIndex:paymentIndex]
                                                             atIndex:paymentIndex];
                                          break;
                                      default:
                                          bixRtn = RUSHORDER_PRINTER_NOT_SET;
                                          break;
                                  }
                                  
                                  self.paymentPaymentNo = NSNotFound;
                                  self.paymentOrderNo = NSNotFound;
                                  
                                  [self treatPrintError:bixRtn];
                                  
                              } else {
                                  if([NSUserDefaults standardUserDefaults].autoKitchenPrint){
                                      
                                      switch(APP.selectedMakerCode){
                                          case PrinterMakerEpson:
                                              bixRtn = [EPSON printOrder:order];
                                              break;
                                          case PrinterMakerStar:
                                              bixRtn = [STAR printOrder:order];
                                              break;
                                          case PrinterMakerBixolon:
                                              bixRtn = [BIX printOrder:order];
                                              break;
                                          default:
                                              bixRtn = RUSHORDER_PRINTER_NOT_SET;
                                              break;
                                      }
                                      [self treatPrintError:bixRtn];
                                  }
                                  
                                  if([NSUserDefaults standardUserDefaults].autoReceiptPrint){
                                      switch(APP.selectedMakerCode){
                                          case PrinterMakerEpson:
                                              bixRtn = [EPSON printReceipt:[[ReceiptWrapper alloc] initWithOrder:order withPaymentIndex:0] atIndex:0];
                                              break;
                                          case PrinterMakerStar:
                                              bixRtn = [STAR printReceipt:[[ReceiptWrapper alloc] initWithOrder:order withPaymentIndex:0] atIndex:0];
                                              break;
                                          case PrinterMakerBixolon:
                                              bixRtn = [BIX printReceipt:[[ReceiptWrapper alloc] initWithOrder:order withPaymentIndex:0] atIndex:0];
                                              break;
                                          default:
                                              bixRtn = RUSHORDER_PRINTER_NOT_SET;
                                              break;
                                      }
                                  }
                              }
                              
                              
                              
                              [self treatPrintError:bixRtn];
                              
                              break;
                          }
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting table information", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)requestCartForPrinting:(Cart *)cart
{
    RequestResult result = RRFail;
    result = [MENU requestGettingCart:cart
                      completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              long bixRtn = 0;
                              
                              [cart updateWithDictionary:response.data
                                                 menuPan:MENUPAN.wholeSerialMenuList];
                              
                              if([NSUserDefaults standardUserDefaults].autoKitchenPrint){
                                  switch(APP.selectedMakerCode){
                                      case PrinterMakerEpson:
                                          bixRtn = [EPSON printCart:cart];
                                          break;
                                      case PrinterMakerStar:
                                          bixRtn = [STAR printCart:cart];
                                          break;
                                      case PrinterMakerBixolon:
                                          bixRtn = [BIX printCart:cart];
                                          break;
                                      default:
                                          bixRtn = RUSHORDER_PRINTER_NOT_SET;
                                          break;
                                  }
                              }
                              
                              if([NSUserDefaults standardUserDefaults].autoBillPrint){
                                  switch(APP.selectedMakerCode){
                                      case PrinterMakerEpson:
                                          
                                          break;
                                      case PrinterMakerStar:

                                          break;
                                      case PrinterMakerBixolon:

                                          break;
                                      default:

                                          break;
                                  }
                              }
                              
                              [self treatPrintError:bixRtn];
                          }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          case ResponseErrorNoCart:{
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Cart does not exist", nil)
                                                  message:NSLocalizedString(@"Customer might cancel their or remove all items from the list", nli)];
                              [TABLE reloadTableStatus];
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting order list information", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (NSMutableArray *)newTableList
{
    if(_newTableList == nil){
        [self activeTableList];
    }
    
    return _newTableList;
}

- (NSMutableArray *)preparingTableList
{
    if(_preparingTableList == nil){
        [self activeTableList];
    }
    
    return _preparingTableList;
}

- (NSMutableArray *)readyTableList
{
    if(_readyTableList == nil){
        [self activeTableList];
    }
    
    return _readyTableList;
}

- (NSMutableArray *)waitingTableList
{
    if(self.tableList == nil)
        return nil;
    
    if(_waitingTableList == nil){
        NSInteger waitingTableStatus = TableStatusBillReqeusted | TableStatusCompleted;
        
        _waitingTableList = [NSMutableArray array];
        
        for(TableInfo *aTableInfo in self.tableList){
            if(((aTableInfo.status & waitingTableStatus) == aTableInfo.status) &&
               [aTableInfo isKindOfClass:[TableInfo class]]){
                [_waitingTableList addObject:aTableInfo];
            }
        }
        
        [_waitingTableList sortUsingComparator:
         ^NSComparisonResult(TableInfo *obj1, TableInfo *obj2) {
             return [obj1 compareTimeWith:obj2];
         }];
    }
    
    return _waitingTableList;
}

- (NSTimer *)heartbeatTimer
{
    if(_heartbeatTimer == nil){
        _heartbeatTimer = [NSTimer scheduledTimerWithTimeInterval:15.0f
                                                           target:self
                                                         selector:@selector(heartbeatCheck:)
                                                         userInfo:nil
                                                          repeats:YES];

    }
    return _heartbeatTimer;
}

- (void)stopHeartBeat
{
    if(_heartbeatTimer != nil){
        [self.heartbeatTimer invalidate];
        self.heartbeatTimer = nil;
    }
}

- (void)heartbeatCheck:(NSTimer *)timer
{
    RequestResult result = RRFail;
    result = [MERCHANT requestGetNoticationTimeWithCompletionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      if(response.errorCode == ResponseSuccess){

                          NSDate *newLastNotificationTime = [response objectForKey:@"last_notification_time"];
                          NSDate *lastNotificationTime = [NSUserDefaults standardUserDefaults].lastNotificationTime;
                          
                          if(newLastNotificationTime == nil){
                              
                          } else {
                              if(![newLastNotificationTime isEqual:lastNotificationTime]){
                                  //
                                  [NSUserDefaults standardUserDefaults].lastNotificationTime = newLastNotificationTime;
                                  PCLog(@"Last Notification Time %@", [NSUserDefaults standardUserDefaults].lastNotificationTime);
                                  [[NSUserDefaults standardUserDefaults] synchronize];
                                  
    
                                  [APP playSystemSound];
                                  [APP vibratePhone];
                                  
                                  [self reloadTableStatus:YES];
    
                              }
                          }
                      } else {
                          PCError(@"heartbeatCheck response error %d", response.errorCode);
                      }
                  } else {
                      PCError(@"heartbeatCheck statusCode error %d", statusCode);
                  }
              }];
    switch(result){
        case RRSuccess:

            break;
        case RRParameterError:
            PCError(@"heartbeatCheck parameter error");
            break;
        default:
            break;
    }
}

- (void)treatPrintError:(long)errorCode
{
    switch(APP.selectedMakerCode){
        case PrinterMakerEpson:
            [EPSON treatPrintError:errorCode];
            break;
        case PrinterMakerStar:
            [STAR treatPrintError:errorCode];
            break;
        case PrinterMakerBixolon:
            [BIX treatPrintError:errorCode];
            break;
        default:
            if(errorCode == RUSHORDER_PRINTER_NOT_SET){
                [UIAlertView alertWithTitle:NSLocalizedString(@"Printer is Not Set", nil)
                                    message:NSLocalizedString(@"Printer maker and model are not set. Please go to Settings > Print Settings to set.", nil)
                                   delegate:self
                                        tag:104];
            }
            break;
    }
}


@end
