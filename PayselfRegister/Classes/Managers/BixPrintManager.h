//
//  BixPrintManager.h
//  RushOrder
//
//  Created by Conan on 8/30/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BXPrinterController.h"
#import "Order.h"
#import "Cart.h"
#import "PCPrintManagerProtocol.h"
#import "PrintWrappers.h"

#define BIX [BixPrintManager sharedManager]
#define BIXCON [BixPrintManager sharedManager].controller

@interface BixPrintManager : NSObject <BXPrinterControlDelegate>

@property BXPrinterController *controller;

@property (copy, nonatomic) NSString *macAddress;

+ (BixPrintManager *)sharedManager;

//- (BOOL)connect;
//- (void)disconnect;


- (long)printOrder:(Order *)order;
- (long)printCart:(Cart *)cart;
- (long)printBill:(BillWrapper *)bill;
- (long)printReceipt:(ReceiptWrapper *)receipt atIndex:(NSInteger)paymentIndex;
- (long)printTest;

- (void)treatPrintError:(long)errorCode;

- (void)lookup;
@end
