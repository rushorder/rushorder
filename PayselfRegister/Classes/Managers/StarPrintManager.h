//
//  StarPrintManager.h
//  RushOrder
//
//  Created by Conan on 8/30/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Order.h"
#import "Cart.h"
#import "PCPrintManagerProtocol.h"
#import "PrintWrappers.h"

#define STAR [StarPrintManager sharedManager]
#define STARCON [StarPrintManager sharedManager].controller


@interface StarPrintManager : NSObject

@property (copy, nonatomic) NSString *portName;
@property (copy, nonatomic) NSString *portSettings;

+ (StarPrintManager *)sharedManager;

//- (BOOL)connect;
//- (void)disconnect;

- (long)printOrder:(Order *)order;
- (long)printCart:(Cart *)cart;
- (long)printBill:(BillWrapper *)bill;
- (long)printReceipt:(ReceiptWrapper *)receipt atIndex:(NSInteger)paymentIndex;
- (long)printTest;

- (void)treatPrintError:(long)errorCode;

@end
