//
//  StarPrintManager.m
//  RushOrder
//
//  Created by Conan on 8/30/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "StarPrintManager.h"
#import "MenuPrice.h"
#import "MenuOption.h"
#import "MenuOptionGroup.h"
#import "Payment.h"
#import "PrinterFunctions.h"
#import <SDWebImage/SDWebImageManager.h>
#import "CustomerInfo.h"

#define MONEY_RECEIPT_LEFT_MARGIN 25

@interface StarPrintManager()

@property (strong, nonatomic) NSMutableArray *printQueue;

@property (copy, nonatomic) NSMutableAttributedString *string;
@property (strong, nonatomic) UIImage *image;
@end

@implementation StarPrintManager


+ (StarPrintManager *)sharedManager
{
    static StarPrintManager *aManager = nil;
    
    @synchronized(self){
        if(aManager == nil){
            aManager = [[self alloc] init];
        }
    }
    
    return aManager;
}

- (id)init
{
    self = [super init];
    if(self != nil){
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didBecomeActive:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(willResignActive:)
                                                     name:UIApplicationWillResignActiveNotification
                                                   object:nil];
    }
    return self;
}

//- (BOOL)connect
//{
//    if(self.controller.target == nil){
//        [self.controller lookup];
//        return NO;
//    } else {
//        return [self.controller connect];
//    }
//}
//
//- (void)disconnect
//{
//    [self.controller disconnect];
//}

- (void)didBecomeActive:(NSNotification *)noti
{

}

- (void)willResignActive:(NSNotification *)noti
{

}

- (long)printTest
{
    [self addText:@"01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789\n"
         textSize:0];
    [self lineFeed:1];
    [self addText:@"01234567890123456789012345678901234567890123456789012345678901234567890123456789\n"
         textSize:1];
    [self lineFeed:1];
    [self addText:@"012345678901234567890123456789012345678901234567890123456789\n"
         textSize:2];
    [self lineFeed:1];
    [self addText:@"01234567890123456789012345678901234567890123456789\n"
         textSize:3];
    [self lineFeed:1];
    [self addText:@"0123456789012345678901234567890123456789\n"
         textSize:4];
    [self addText:@"0123456789012345678901234567890123456789\n"
         textSize:5];
    [self addText:@"0123456789012345678901234567890123456789\n"
         textSize:6];
    [self addText:@"0123456789012345678901234567890123456789\n"
         textSize:7];
    
    
    [self print:self.string];
    
    self.string = nil;
    self.image = nil;

    return 0;
}

#pragma mark - Print

- (long)printOrder:(Order *)order
{
    return [self printOrder:order enqueueing:YES];
}

- (long)printOrder:(Order *)order enqueueing:(BOOL)enqueueing
{
    if(order == nil) {
        PCError(@"No order to print");
        return 1;
    }
    
    
    [self addText:@"\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"];
    
    NSString *orderTypeString = nil;
    
    if(order.isTicketBase){
        
        switch(order.orderType){
            case OrderTypeTakeout:
                orderTypeString = NSLocalizedString(@"Take-out", nil);
                break;
            case OrderTypeDelivery:
                orderTypeString = NSLocalizedString(@"Delivery", nil);
                break;
            default:
                orderTypeString = NSLocalizedString(@"Dine-in", nil);
                break;
        }
    }
    
    if([order.tableNumber length] > 0){
        [self printTextLeft:[NSString stringWithFormat:@"TBL#%@",order.tableNumber]
                  rightText:[NSString stringWithFormat:@"ORD#%lld",order.orderNo]
                   textSize:4];
    } else {
        [self printTextLeft:orderTypeString
                  rightText:[NSString stringWithFormat:@"TIX#%lld",order.orderNo]
                   textSize:4];
    }
    
    [self drawDoubleLine];
    

    [self printTextLeft:nil
              rightText:[order.orderDate secondsString]];
    
    
    
    if(order.pickupAfter != 0 || order.isTakeout || order.isDelivery){
        [self drawSingleLine];
        NSDate *dueDate = nil;
        if(order.pickupAfter != 0){
            dueDate = [order.orderDate dateByAddingTimeInterval:(order.pickupAfter * 60)];
        }
        if(order.isTakeout || order.isDelivery){
            [self printTextLeft:NSLocalizedString(@"Desired Time:", nil)
                      rightText:(dueDate == nil)?NSLocalizedString(@"ASAP", nil):[dueDate hourMinuteString]
                       textSize:2];
        }
    }
    
    [self drawSingleLine];
    
    NSInteger i = 0; //menuOptionNames
    for(LineItem *lineItem in order.lineItems){
        
        [self addText:[@"*" stringByAppendingString:lineItem.displayMenuName]
             textSize:5];
        
//        if([lineItem.menuChoiceName length] > 0){
//            [self addText:@"\n"];
//            [self addText:[@"-" stringByAppendingString:lineItem.menuChoiceName]
//                 textSize:4];
//        }
        
        for(NSDictionary *groupDict in lineItem.menuOptionNamesObj){
            
            NSString *groupName = [groupDict objectForKey:@"name"];
            
            [self addText:@"\n"];
            [self addText:[NSString stringWithFormat:@"[%@]",
                           groupName]
                 textSize:2];
            
            NSArray *selectedOptions = [groupDict objectForKey:@"menu_option_names"];
            for(NSDictionary *optionDict in selectedOptions){
                NSString *optionName = [optionDict objectForKey:@"option_name"];
                
                [self addText:@"\n"];
                [self addText:[@"-" stringByAppendingString:optionName]
                     textSize:4];
            }
        }
        
        if([lineItem.specialInstruction length] > 0){
            [self addText:@"\n"];
            [self addText:NSLocalizedString(@"[Special Instruction]", nil)
                 textSize:2];
            [self addText:@"\n"];
            [self addText:lineItem.specialInstruction
                 textSize:4];
        }
        i++;
        
        if(i < [order.lineItems count]){
            [self addText:@"\n"];
            [self drawSingleLine];
        }
    }
    
    [self addText:@"\n"];
    
    if([order.customerRequest length] > 0){
        [self drawDoubleLine];
        [self addText:NSLocalizedString(@"[Customer Request]", nil)
             textSize:2];
        [self addText:@"\n"];
        [self addText:order.customerRequest
             textSize:4];
        [self lineFeed:2];
    }
    
    
    if(order.isTicketBase){

        NSString *nameString = nil;
        NSString *phoneString = nil;
        
        if([order.customers count] > 0){
            if([order.receiverName length] > 0){
                nameString = order.receiverName;
            } else {
                NSMutableString *names = [NSMutableString string];
                
                for(CustomerInfo *customer in order.customers){
                    if([names length] > 0) [names appendString:@", "];
                    [names appendString:customer.name];
                }
                
                nameString = names;
            }
        } else {
            if([order.receiverName length] > 0){
                nameString = order.receiverName;
            }
        }
        
        if([order.phoneNumber length] > 0){
            phoneString = order.phoneNumber;
        }
        
        if(([nameString length] > 0) || ([phoneString length] > 0)){
            [self drawDoubleLine];
        }
        
        if([nameString length] > 0){
            [self addText:[NSString stringWithFormat:NSLocalizedString(@"Name  : %@", nil), nameString]
                 textSize:1];
            [self addText:@"\n"];
        }
        if([phoneString length] > 0){
            [self addText:[NSString stringWithFormat:NSLocalizedString(@"Phone : %@", nil), phoneString]
                 textSize:1];
            [self addText:@"\n"];
        }
        if(([nameString length] > 0) || ([phoneString length] > 0)){
            [self drawDoubleLine];
        }
        
        if(order.orderType == OrderTypeDelivery){
            
            [self printTextLeft:NSLocalizedString(@"Delivery Address", nil)
                      rightText:@""];
            
            [self drawSingleLine];
            
            [self addText:order.address];
            [self addText:@"\n"];
            
            [self drawDoubleLine];
        }
    } else {
        [self drawDoubleLine];
    }
    
    [self lineFeed:1];
    
    [self print:self.string];
    
    self.string = nil;
    self.image = nil;
    
    return 0;
}

- (long)printCart:(Cart *)cart
{
    return [self printCart:cart enqueueing:YES];
}

- (long)printCart:(Cart *)cart enqueueing:(BOOL)enqueueing
{
    if(cart == nil) {
        PCError(@"No cart to print");
        return 1;
    }
    
    [self addText:@"\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"];
    
    if(cart.orderNo == 0){
        [self printTextLeft:[NSString stringWithFormat:@"TBL#%@",cart.tableLabel]
                  rightText:[NSString stringWithFormat:@" TMP#%lld",cart.cartNo]
                   textSize:4];
    } else {
        [self printTextLeft:[NSString stringWithFormat:@"TBL#%@",cart.tableLabel]
                  rightText:[NSString stringWithFormat:@" ORD#%lld",cart.orderNo]
                   textSize:4];
    }
    
    [self drawDoubleLine];
    
    
    [self printTextLeft:nil
              rightText:[[NSDate date] secondsString]];
    
    [self drawSingleLine];
    
    
    NSInteger i = 0;
    for(LineItem *lineItem in cart.lineItems){
        
        [self addText:[@"*" stringByAppendingString:lineItem.displayMenuName]
             textSize:5];
        
//        if([lineItem.menuChoiceName length] > 0){
//            [self addText:@"\n"];
//            [self addText:[@"-" stringByAppendingString:lineItem.menuChoiceName]
//                 textSize:4];
//        }

        for(NSDictionary *groupDict in lineItem.menuOptionNamesObj){
            
            NSString *groupName = [groupDict objectForKey:@"name"];
            
            [self addText:@"\n"];
            [self addText:[NSString stringWithFormat:@"[%@]",
                           groupName]
                 textSize:2];
            
            NSArray *selectedOptions = [groupDict objectForKey:@"menu_option_names"];
            for(NSDictionary *optionDict in selectedOptions){
                NSString *optionName = [optionDict objectForKey:@"option_name"];
                
                [self addText:@"\n"];
                [self addText:[@"-" stringByAppendingString:optionName]
                     textSize:4];
            }
        }
        
        if([lineItem.specialInstruction length] > 0){
            [self addText:@"\n"];
            [self addText:NSLocalizedString(@"[Special Instruction]", nil)
                 textSize:2];
            [self addText:@"\n"];
            [self addText:lineItem.specialInstruction
                 textSize:4];
        }
        i++;
        
        if(i < [cart.lineItems count]){
            [self addText:@"\n"];
            [self drawSingleLine];
        }
    }
    
    [self addText:@"\n"];
    [self drawDoubleLine];
    
    [self lineFeed:1];
    
    
    [self print:self.string];
    
    
    self.string = nil;
    self.image = nil;
    return 0;
}

- (long)printBill:(BillWrapper *)bill
{
    return [self printBill:bill enqueueing:YES];
}

- (long)printBill:(BillWrapper *)bill enqueueing:(BOOL)enqueueing
{
    Order *order = (Order *)bill.order;
    
    if(order == nil) {
        PCError(@"No order to print for Bill");
        return 1;
    }
    
    
    
    ////////////////////////////////////////////////////////////////////////
    [self addText:CRED.merchant.name
         textSize:4
        alignment:NSTextAlignmentCenter];
    
    [self lineFeed:3];
    ////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////
    if(order.isTicketBase){
        if(order.isTakeout){
            [self printTextLeft:NSLocalizedString(@"Take-out Order", nil)
                      rightText:[NSString stringWithFormat:@"Ticket No.%lld",order.orderNo]];
        } else if (order.isDelivery){
            [self printTextLeft:NSLocalizedString(@"Delivery Order", nil)
                      rightText:[NSString stringWithFormat:@"Ticket No.%lld",order.orderNo]];
        } else {
            if([order.tableNumber length] > 0){
                [self printTextLeft:NSLocalizedString(@"Dine-in Order", nil)
                          rightText:[NSString stringWithFormat:@"Table No.%@",order.tableNumber]];
                
                [self printTextLeft:nil
                          rightText:[[NSDate date] secondsString]];
            } else {
                [self printTextLeft:NSLocalizedString(@"Dine-in Order", nil)
                          rightText:[NSString stringWithFormat:@"Ticket No.%lld",order.orderNo]];
            }
        }
    } else {
        [self printTextLeft:[NSString stringWithFormat:@"Table No.%@",order.tableNumber]
                  rightText:[NSString stringWithFormat:@"Order No.%lld",order.orderNo]];
    }
    
    [self drawDoubleLine];

    [self printTextLeft:nil
              rightText:[[NSDate date] secondsString]];
    
    [self drawSingleLine];
    ////////////////////////////////////////////////////////////////////////
    
    NSInteger i = 0;
    for(LineItem *lineItem in order.lineItems){
        
        [self printTextLeft:lineItem.displayMenuName
                  rightText:[[NSNumber numberWithCurrency:(lineItem.unitPrice * lineItem.quantity)] currencyString]];
        
        for(NSDictionary *groupDict in lineItem.menuOptionNamesObj){
            
            NSArray *selectedOptions = [groupDict objectForKey:@"menu_option_names"];
            for(NSDictionary *optionDict in selectedOptions){
                NSString *optionName = [optionDict objectForKey:@"option_name"]; //option_price
                PCCurrency optionPrice = [optionDict currencyForKey:@"option_price"];
                NSString *optionPriceString = nil;
                if(optionPrice != 0){
                    optionPriceString = [[NSNumber numberWithCurrency:(optionPrice * lineItem.quantity)] currencyString];
                }
                [self printTextLeft:[@"    " stringByAppendingString:optionName]
                          rightText:optionPriceString];
            }
        }
        
        
        
        i++;
    }
    
    if(i > 0){
        [self drawSingleLine];
    }
    
    [self printTextLeft:NSLocalizedString(@"Subtotal", nil)
              rightText:order.subTotalString
         withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
            rightMargin:0];
    
    [self printTextLeft:NSLocalizedString(@"Tax", nil)
              rightText:order.taxesString
         withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
            rightMargin:0];
    
    if(order.orderType == OrderTypeDelivery){
        [self printTextLeft:NSLocalizedString(@"Delivery Fee", nil)
                  rightText:order.totalPaidDeliveryFeeAmountsString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
    }
    
    if(order.roServiceFee > 0){
        [self printTextLeft:NSLocalizedString(@"Service Fee", nil)
                  rightText:order.roServiceFeeString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
    }
    
    [self drawSingleLine];
    
    [self printTextLeft:NSLocalizedString(@"Amount Due", nil)
              rightText:order.totalString
         withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
            rightMargin:0];
    
    [self drawDoubleLine];
    
    if(order.isTicketBase){
        
        NSString *nameString = nil;
        NSString *phoneString = nil;
        
        if([order.customers count] > 0){
            if([order.receiverName length] > 0){
                nameString = order.receiverName;
            } else {
                NSMutableString *names = [NSMutableString string];
                
                for(CustomerInfo *customer in order.customers){
                    if([names length] > 0) [names appendString:@", "];
                    [names appendString:customer.name];
                }
                
                nameString = names;
            }
        } else {
            if([order.receiverName length] > 0){
                nameString = order.receiverName;
            }
        }
        
        if([order.phoneNumber length] > 0){
            phoneString = order.phoneNumber;
        }
        
        if([nameString length] > 0){
            [self addText:[NSString stringWithFormat:NSLocalizedString(@"Name  : %@", nil), nameString]
                 textSize:1];
            [self addText:@"\n"];
        }
        if([phoneString length] > 0){
            [self addText:[NSString stringWithFormat:NSLocalizedString(@"Phone : %@", nil), phoneString]
                 textSize:1];
            [self addText:@"\n"];
        }
        
        BOOL printedDesiredTime = NO;
        if(order.pickupAfter != 0 || order.isTakeout || order.isDelivery){
            NSDate *dueDate = nil;
            if(order.pickupAfter != 0){
                dueDate = [order.orderDate dateByAddingTimeInterval:(order.pickupAfter * 60)];
            }
            if(order.isTakeout || order.isDelivery){
                [self addText:[NSString stringWithFormat:NSLocalizedString(@"Desired Time : %@", nil),
                               (dueDate == nil)?NSLocalizedString(@"ASAP", nil):[dueDate hourMinuteString]]
                     textSize:1];
            }
            [self addText:@"\n"];
            printedDesiredTime = YES;
        }
        
        if(([nameString length] > 0) || ([phoneString length] > 0) || printedDesiredTime){
            [self drawDoubleLine];
        }
        
        if(order.orderType == OrderTypeDelivery){
            
            [self printTextLeft:NSLocalizedString(@"Delivery Address", nil)
                      rightText:@""];
            
            [self drawSingleLine];
            
            [self addText:order.address];
            [self addText:@"\n"];
            
            [self drawDoubleLine];
        }
    } else {
        [self addText:@"Checkout Instruction:"];
        [self addText:@"\n"];
        [self addText:@"1. Sign-in and Checkout on your RushOrder app whenever you're ready."];
        [self addText:@"\n"];
        [self addText:@"2. All available promotions or coupons will be applied at checkout."];
        [self addText:@"\n"];
        [self drawSingleLine];
    }

    [self lineFeed:1];
    
    
    [self print:self.string];
    
    
    self.string = nil;
    self.image = nil;
    
    return 0;
}

- (long)printReceipt:(ReceiptWrapper *)receipt atIndex:(NSInteger)paymentIndex
{
    return [self printReceipt:receipt atIndex:paymentIndex enqueueing:YES];
}

- (long)printReceipt:(ReceiptWrapper *)receipt atIndex:(NSInteger)paymentIndex enqueueing:(BOOL)enqueueing
{
    
    Order *order = (Order *)receipt.order;
    
    if(order == nil) {
        PCError(@"No order to print for receipt");
        return 1;
    }
    
    if([order.paymentList count] <= paymentIndex){
        PCError(@"No Payment to print for receipt");
        return 2;
    }
    
    Payment *selectedPayment = [order.paymentList objectAtIndex:paymentIndex];

    ////////////////////////////////////////////////////////////////////////
        
    UIImage *logoImage = CRED.merchant.logoImage;
    if(logoImage != nil){
        [self addImage:logoImage];
    } else {
        if(CRED.merchant.logoURL != nil){
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadWithURL:CRED.merchant.logoURL
                             options:0
                            progress:^(NSInteger receivedSize, NSInteger expectedSize)
             {
                 PCLog(@"%u / %lld", receivedSize, expectedSize);
             }
                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
             {
                 if (image && finished)
                 {
                     CRED.merchant.logoImage = image;
                     
                     NSInteger i = 0;
                     
                     for(i = ([self.printQueue count] - 1) ; i >= 0 ; i--){
                         id obj = [self.printQueue objectAtIndex:i];
                         
                         if([obj isKindOfClass:[ReceiptWrapper class]]){
                             
                             ReceiptWrapper *receipt = (ReceiptWrapper *)obj;
                             [self.printQueue removeObjectAtIndex:i];
                             
                             [self printReceipt:receipt
                                        atIndex:receipt.paymentIndex
                                     enqueueing:NO];
                             
                         }
                     }
                 }
             }];
            
            if(enqueueing) [self.printQueue addObject:receipt];
            return RUSHORDER_BXL_LOGO_NOT_SET;
            
        } else {
            // Keep going to print
        }
    }
    
    [self addText:CRED.merchant.name
         textSize:4
        alignment:NSTextAlignmentCenter];
    
    [self addText:@"\n"];
    
    [self addText:CRED.merchant.multilineAddress
        alignment:NSTextAlignmentCenter];
    
    [self addText:@"\n"];
    
    [self addText:CRED.merchant.phoneNumber
        alignment:NSTextAlignmentCenter];
    
    [self lineFeed:2];
    ////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////
    [self drawDoubleLine];
    
    if(order.isTicketBase){
        if(order.isTakeout){
            [self printTextLeft:NSLocalizedString(@"Take-out Order", nil)
                      rightText:[NSString stringWithFormat:@"Ticket No.%lld",order.orderNo]];
        } else if (order.isDelivery){
            [self printTextLeft:NSLocalizedString(@"Delivery Order", nil)
                      rightText:[NSString stringWithFormat:@"Ticket No.%lld",order.orderNo]];
        } else {
            if([order.tableNumber length] > 0){
                [self printTextLeft:NSLocalizedString(@"Dine-in Order", nil)
                          rightText:[NSString stringWithFormat:@"Table No.%@",order.tableNumber]];
                
                [self printTextLeft:nil
                          rightText:[[NSDate date] secondsString]];
            } else {
                [self printTextLeft:NSLocalizedString(@"Dine-in Order", nil)
                          rightText:[NSString stringWithFormat:@"Ticket No.%lld",order.orderNo]];
            }
        }
    } else {
        [self printTextLeft:[NSString stringWithFormat:@"Table No.%@",order.tableNumber]
                  rightText:[NSString stringWithFormat:@"Order No.%lld",order.orderNo]];
    }
    
    [self addText:[NSString stringWithFormat:@"Transaction No.%lld",selectedPayment.paymentNo]];
    [self addText:@"\n"];
    
    [self drawSingleLine];
    
    [self printTextLeft:nil
              rightText:[selectedPayment.payDate secondsString]];
    
    [self drawDoubleLine];
    ////////////////////////////////////////////////////////////////////////
    
    NSInteger i = 0;
    
    for(LineItem *lineItem in selectedPayment.lineItems){
  
        [self printTextLeft:lineItem.displayMenuName
                  rightText:[[NSNumber numberWithCurrency:(lineItem.unitPrice * lineItem.quantity)] currencyString]];
        
        for(NSDictionary *groupDict in lineItem.menuOptionNamesObj){

            NSArray *selectedOptions = [groupDict objectForKey:@"menu_option_names"];
            for(NSDictionary *optionDict in selectedOptions){
                NSString *optionName = [optionDict objectForKey:@"option_name"]; //option_price
                PCCurrency optionPrice = [optionDict currencyForKey:@"option_price"];
                NSString *optionPriceString = nil;
                if(optionPrice != 0){
                    optionPriceString = [[NSNumber numberWithCurrency:(optionPrice * lineItem.quantity)] currencyString];
                }
                [self printTextLeft:[@"    " stringByAppendingString:optionName]
                          rightText:optionPriceString];
            }
        }
        
        i++;
    }
    
    if(i > 0){
        [self drawSingleLine];
    }
    
    if(order.orderType == OrderTypeBill){
        [self printTextLeft:NSLocalizedString(@"Subtotal", nil)
                  rightText:order.subTotalString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
        
        [self printTextLeft:NSLocalizedString(@"Tax", nil)
                  rightText:order.taxesString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
    } else {
        [self printTextLeft:NSLocalizedString(@"Subtotal", nil)
                  rightText:selectedPayment.subTotalString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
        
        [self printTextLeft:NSLocalizedString(@"Tax", nil)
                  rightText:selectedPayment.taxesString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
    }
    
    [self printTextLeft:NSLocalizedString(@"Tip", nil)
              rightText:selectedPayment.tipAmountString
         withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
            rightMargin:0];
    
    if(selectedPayment.deliveryFeeAmount > 0){
        [self printTextLeft:NSLocalizedString(@"Delivery Fee", nil)
                  rightText:selectedPayment.deliveryFeeAmountString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
    }
    
    if(selectedPayment.roServiceFeeAmount > 0){
        [self printTextLeft:NSLocalizedString(@"Service Fee", nil)
                  rightText:selectedPayment.roServiceFeeAmountString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
    }
    
    if(selectedPayment.discountAmount > 0){
        [self printTextLeft:NSLocalizedString(@"Discount", nil)
                  rightText:selectedPayment.discountAmountString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
    }
    
    if(order.orderType == OrderTypeBill){
        [self printTextLeft:NSLocalizedString(@"Total", nil)
                  rightText:[[NSNumber numberWithCurrency:(order.total + selectedPayment.tipAmount)] currencyString]
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
    } else {
        [self printTextLeft:NSLocalizedString(@"Total", nil)
                  rightText:selectedPayment.grandTotalString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
    }
    
    [self drawSingleLine];
    
    [self printTextLeft:NSLocalizedString(@"Amount Paid", nil)
              rightText:selectedPayment.netAmountString
         withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
            rightMargin:0];
    
    if(selectedPayment.creditAmount > 0){
        [self printTextLeft:NSLocalizedString(@"Points Amount Paid", nil)
                  rightText:selectedPayment.creditAmountString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
    }
    
    [self drawSingleLine];
    
    [self addText:[NSString stringWithFormat:NSLocalizedString(@"Card Type  %@",nil), [NSString issuerName:selectedPayment.issuer]]];
    [self addText:@"\n"];
    
    [self addText:[NSString stringWithFormat:NSLocalizedString(@"Acc No.    %@",nil), [selectedPayment formattedCardNumberWithSecureString:@"*"]]];
    [self addText:@"\n"];
    
    //        [self addText:[NSString stringWithFormat:NSLocalizedString(@"Charge ID.   %@",nil), selectedPayment.chargeId]];
    //        [self addText:@"\n"];
    
    [self drawSingleLine];
    
    if(selectedPayment.rewardedAmount > 0){
        [self printTextLeft:NSLocalizedString(@"Rewarded Point", nil)
                  rightText:selectedPayment.rewardedAmountString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
        [self drawSingleLine];
    }
    
    if(order.isTicketBase){
        
        NSString *nameString = nil;
        NSString *phoneString = nil;
        
        if([order.customers count] > 0){
            if([order.receiverName length] > 0){
                nameString = order.receiverName;
            } else {
                NSMutableString *names = [NSMutableString string];
                
                for(CustomerInfo *customer in order.customers){
                    if([names length] > 0) [names appendString:@", "];
                    [names appendString:customer.name];
                }
                
                nameString = names;
            }
        } else {
            if([order.receiverName length] > 0){
                nameString = order.receiverName;
            }
        }
        
        if([order.phoneNumber length] > 0){
            phoneString = order.phoneNumber;
        }
        
        if([nameString length] > 0){
            [self addText:[NSString stringWithFormat:NSLocalizedString(@"Name  : %@", nil), nameString]
                 textSize:1];
            [self addText:@"\n"];
        }
        if([phoneString length] > 0){
            [self addText:[NSString stringWithFormat:NSLocalizedString(@"Phone : %@", nil), phoneString]
                 textSize:1];
            [self addText:@"\n"];
        }
        
        BOOL printedDesiredTime = NO;
        if(order.pickupAfter != 0 || order.isTakeout || order.isDelivery){
            NSDate *dueDate = nil;
            if(order.pickupAfter != 0){
                dueDate = [order.orderDate dateByAddingTimeInterval:(order.pickupAfter * 60)];
            }
            if(order.isTakeout || order.isDelivery){
                [self addText:[NSString stringWithFormat:NSLocalizedString(@"Desired Time : %@", nil),
                               (dueDate == nil)?NSLocalizedString(@"ASAP", nil):[dueDate hourMinuteString]]
                     textSize:1];
            }
            [self addText:@"\n"];
            printedDesiredTime = YES;
        }
        
        if(([nameString length] > 0) || ([phoneString length] > 0) || printedDesiredTime){
            [self drawDoubleLine];
        }
        
        if(order.orderType == OrderTypeDelivery){
            
            [self printTextLeft:NSLocalizedString(@"Delivery Address", nil)
                      rightText:@""];
            
            [self drawSingleLine];
            
            [self addText:order.address];
            [self addText:@"\n"];
            
            [self drawDoubleLine];
        }
    }
    
    //        [self.controller printBarcode:[[NSString stringWithFormat:@"%012lld", order.orderNo] cStringUsingEncoding:NSASCIIStringEncoding]
    //                            symbology:BXL_BCS_UPCA
    //                                width:4
    //                               height:80];
    
    [self lineFeed:2];
    
    [self addText:NSLocalizedString(@"- Customer Copy -", nil)
        alignment:NSTextAlignmentCenter];
    [self addText:@"\n"];
    [self addText:NSLocalizedString(@"Thank you for dining with us~", nil)
        alignment:NSTextAlignmentCenter];
    
    
    [self lineFeed:3];
    
    [self drawDoubleLine];
    
    [self lineFeed:1];
   
    [self print:self.string];
    
    self.string = nil;
    self.image = nil;
    return 0;
}

- (void)drawSingleLine
{
    [self addText:@"---------------------------------------\n"
         textSize:1
        alignment:NSTextAlignmentLeft];
}

- (void)drawDoubleLine
{
    [self addText:@"=======================================\n"
         textSize:1
        alignment:NSTextAlignmentLeft];
}

- (int)printTextLeft:(NSString *)leftText
           rightText:(NSString *)rightText
      withLeftMargin:(NSInteger)leftMargin
         rightMargin:(NSInteger)rightMargin
{
    return [self printTextLeft:leftText
                     rightText:rightText
                withLeftMargin:leftMargin
                   rightMargin:rightMargin
                     textSize:1];
}

- (int)printTextLeft:(NSString *)leftText
           rightText:(NSString *)rightText
{
    return [self printTextLeft:leftText
                     rightText:rightText
                     textSize:1];
}


- (int)printTextLeft:(NSString *)leftText
           rightText:(NSString *)rightText
            textSize:(NSInteger)size
{
    return [self printTextLeft:leftText
                     rightText:rightText
                withLeftMargin:0
                   rightMargin:0
                     textSize:size];
}

- (int)printTextLeft:(NSString *)leftText
           rightText:(NSString *)rightText
      withLeftMargin:(NSInteger)leftMargin
         rightMargin:(NSInteger)rightMargin
            textSize:(NSInteger)size
{
    if(leftText == nil) leftText = @"";
    if(rightText == nil) rightText = @"";
    
    int err = 0;
    
    NSUInteger maxCharacterLength = 39;
    
    switch(size){
            case 0:
            maxCharacterLength = 52;
            break;
        case 1:
            maxCharacterLength = 39;
            break;
        case 2:
            maxCharacterLength = 29;
            break;
        case 3:
            maxCharacterLength = 23;
            break;
        case 4:
            maxCharacterLength = 29;
            break;
        case 5:
            maxCharacterLength = 16;
            break;
        default:
            maxCharacterLength = 14;
            break;
    }
    

    NSInteger leftTextCount = [leftText length];
    NSInteger rightTextCount = [rightText length];
    NSInteger occupiedCount = leftTextCount + rightTextCount + leftMargin + rightMargin;
    
    if(leftMargin > 0){
        
        occupiedCount -= leftTextCount;
        
        NSString *leftMarginString = nil;
        
        switch(size){
            case 0:
                leftMarginString = @"                                                    "; //52
                break;
            case 1:
                leftMarginString = @"                                       "; //39
                break;
            case 2:
                leftMarginString = @"                             "; //29
                break;
            case 3:
                leftMarginString = @"                       "; //23
                break;
            case 4:
                leftMarginString = @"                   "; //19
                break;
            case 5:
                leftMarginString = @"                "; //16
                break;
            default:
                leftMarginString = @"              "; //14
                break;
        }
        
        NSInteger index = (maxCharacterLength - leftMargin) + leftTextCount;
        index = MAX(index, 0);
        leftMarginString = [leftMarginString substringFromIndex:index];
        [self addText:leftMarginString
             textSize:size
            alignment:NSTextAlignmentLeft];
    }
    
    
    if(occupiedCount > maxCharacterLength){
        NSInteger restCount = occupiedCount - maxCharacterLength;
        
        if(leftText != nil){
            leftText = [leftText substringToIndex:([leftText length] - (restCount + 1))];
            occupiedCount -= (restCount + 1);
        }
    }

    [self addText:leftText
         textSize:size
        alignment:NSTextAlignmentLeft];
    
    NSString *spaceString = nil;
    
    switch(size){
        case 0:
            spaceString = @"                                                    "; //52
            break;
        case 1:
            spaceString = @"                                       "; //39
            break;
        case 2:
            spaceString = @"                             "; //29
            break;
        case 3:
            spaceString = @"                       "; //23
            break;
        case 4:
            spaceString = @"                   "; //19
            break;
        case 5:
            spaceString = @"                "; //16
            break;
        default:
            spaceString = @"              "; //14
            break;
    }
    
    //    NSInteger subIndex = occupiedCount;
    occupiedCount = MAX(occupiedCount, 0);
    
    spaceString = [spaceString substringFromIndex:occupiedCount];
    
    [self addText:spaceString
         textSize:size
        alignment:NSTextAlignmentLeft];
    [self addText:rightText
         textSize:size
        alignment:NSTextAlignmentLeft];
    [self lineFeed:1];
    
    return err;
}

#define IMAGE_MARGIN 20
- (void)print:(NSAttributedString *)string
{
    
    int width = 570;
    
    CGSize size = CGSizeMake(width, 10000);
    CGRect messuredRect = [string boundingRectWithSize:size
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
	CGSize messuredSize = messuredRect.size;
    
    CGFloat imageSpace = 0.0f;
    if(self.image != nil){
        imageSpace = self.image.size.height + IMAGE_MARGIN;
    }
    messuredSize.height += imageSpace;
    
	if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
		if ([[UIScreen mainScreen] scale] == 2.0) {
			UIGraphicsBeginImageContextWithOptions(messuredSize, NO, 1.0);
		} else {
			UIGraphicsBeginImageContext(messuredSize);
		}
	} else {
		UIGraphicsBeginImageContext(messuredSize);
	}
    CGContextRef ctr = UIGraphicsGetCurrentContext();
    
    UIColor *color = [UIColor whiteColor];
    [color set];
    
    CGRect rect = CGRectMake(0, 0, messuredSize.width, messuredSize.height);
    CGContextFillRect(ctr, rect);
    
    if(self.image != nil){
        CGRect imgFrame = CGRectMake((width - self.image.size.width) / (float)2 , 0.0f
                                     , self.image.size.width
                                     , self.image.size.height);
        [self.image drawInRect:imgFrame];
    }

    rect.origin.y += imageSpace;
    rect.size.height -= imageSpace;
    
    color = [UIColor blackColor];
    [color set];
    
    [string drawWithRect:rect
                 options:NSStringDrawingUsesLineFragmentOrigin
                 context:nil];
    
    UIImage *imageToPrint = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    [PrinterFunctions PrintImageWithPortname:self.portName
                                portSettings:self.portSettings
                                imageToPrint:imageToPrint
                                    maxWidth:width * 2
                           compressionEnable:YES
                              withDrawerKick:NO];
}

- (int)lineFeed:(NSInteger)feedValue
{
    NSMutableString *mutableString = [NSMutableString string];
    for(NSInteger i = 0 ; i < feedValue ; i++){
        [mutableString appendString:@"\n"];
    }
    return [self addText:mutableString];
}

- (int)addText:(NSString *)text
     alignment:(NSTextAlignment)alignment
{
    return [self addText:text
                textSize:1
               alignment:alignment];
}

- (int)addText:(NSString *)text
{
    return [self addText:text textSize:1];
}

- (int)addText:(NSString *)text
      textSize:(NSInteger)size
{
    return [self addText:text
                textSize:size
               alignment:NSTextAlignmentLeft];
}

- (int)addText:(NSString *)text
      textSize:(NSInteger)size
     alignment:(NSTextAlignment)alignment
{
    if(text == nil){
        return 1;
    }
    CGFloat fontSize = 24.0f;
    switch(size){
        case 0: fontSize = 18.0f; break;
        case 1: fontSize = 24.0f; break;
        case 2: fontSize = 32.0f; break;
        case 3: fontSize = 40.0f; break;
        case 4: fontSize = 48.0f; break;
        case 5: fontSize = 56.0f; break;
        case 6: fontSize = 64.0f; break;
        case 7: fontSize = 72.0f; break;
        case 8: fontSize = 80.0f; break;
        default:
            fontSize = 24.0f;
    }
    UIFont *font = [UIFont fontWithName:@"Courier"
                                   size:fontSize];
    NSMutableParagraphStyle *para = [[NSMutableParagraphStyle alloc] init];
    para.alignment = alignment;
    NSDictionary *attr = @{NSFontAttributeName:font,
                           NSParagraphStyleAttributeName:para};
    
    [self.string appendAttributedString:
     [[NSAttributedString alloc] initWithString:text
                                     attributes:attr]];
    
    return 0;
}

- (int)addImage:(UIImage *)image
{
    self.image = image;
    
    return 0;
}

- (NSMutableAttributedString *)string
{
    if(_string == nil){
        _string = [[NSMutableAttributedString alloc] init];
    }
    
    return _string;
}



- (void)treatPrintError:(long)errorCode
{
//    switch(errorCode){
//        case RUSHORDER_BXL_CONNECT_ERROR:
//            [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot connect to printer", nil)
//                                message:NSLocalizedString(@"Check your printer connected properly.", nil)
//                               delegate:self
//                                    tag:105];
//            return;
//        case RUSHORDER_BXL_PRINTER_NOT_FOUND:
//            [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot find any printers connected", nil)
//                                message:NSLocalizedString(@"Be sure the printer connected properly to the local network.", nil) // If you don't have printer, turn off Auto Printing at \"Settings\"
//                               delegate:self
//                                    tag:105];
//            return;
//        case BXL_BC_DATA_ERROR:
//            [UIAlertView alertWithTitle:NSLocalizedString(@"There is no data for printing", nil)
//                                message:NSLocalizedString(@"Each kind of printing needs appropriate data.", nil) // If you don't have printer, turn off Auto Printing at \"Settings\"
//                               delegate:self
//                                    tag:105];
//            return;
//        default:
//            return;
//    }
}

- (NSMutableArray *)printQueue
{
    if(_printQueue == nil){
        _printQueue = [NSMutableArray array];
    }
    return _printQueue;
}

@end
