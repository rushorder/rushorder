//
//  PrintWrappers.h
//  RushOrder
//
//  Created by Conan on 2/5/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const NoPrinterFoundNotification;

@interface BillWrapper : NSObject
@property (strong, nonatomic) Order *order;

- (id)initWithOrder:(Order *)order;
@end

@interface ReceiptWrapper : NSObject
@property (strong, nonatomic) Order *order;
@property (nonatomic) NSInteger paymentIndex;

- (id)initWithOrder:(Order *)order withPaymentIndex:(NSInteger)paymentIndex;
@end
