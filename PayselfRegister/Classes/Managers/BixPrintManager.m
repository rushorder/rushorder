//
//  BixPrintManager.m
//  RushOrder
//
//  Created by Conan on 8/30/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "BixPrintManager.h"
#import "MenuPrice.h"
#import "MenuOption.h"
#import "MenuOptionGroup.h"
#import "Payment.h"
#import <SDWebImage/SDWebImageManager.h>
#import "CustomerInfo.h"

#define MONEY_RECEIPT_LEFT_MARGIN 25

@interface BixPrintManager()

@property (strong, nonatomic) NSMutableArray *printQueue;

@end

@implementation BixPrintManager

+ (BixPrintManager *)sharedManager
{
    static BixPrintManager *aManager = nil;
    
    @synchronized(self){
        if(aManager == nil){
            aManager = [[self alloc] init];
        }
    }
    
    return aManager;
}

- (id)init
{
    self = [super init];
    if(self != nil){
        self.controller = [BXPrinterController getInstance];
        self.controller.delegate = self;
        self.controller.lookupCount = 5;
        self.controller.AutoConnection = BXL_CONNECTIONMODE_NOAUTO;
        self.controller.textEncoding = NSUTF8StringEncoding;
        
        [self.controller open];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didBecomeActive:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(willResignActive:)
                                                     name:UIApplicationWillResignActiveNotification
                                                   object:nil];
    }
    return self;
}

//- (BOOL)connect
//{
//    if(self.controller.target == nil){
//        [self.controller lookup];
//        return NO;
//    } else {
//        return [self.controller connect];
//    }
//}
//
//- (void)disconnect
//{
//    [self.controller disconnect];
//}

- (void)didBecomeActive:(NSNotification *)noti
{
    [self.controller open];
}

- (void)willResignActive:(NSNotification *)noti
{
    [self.controller close];
}

- (long)printTest
{
    //42
    //21
    //14
    //10
    //8
    
    if(self.controller.target == nil){
        [self.controller lookup];
        return RUSHORDER_BXL_PRINTER_NOT_SET;
    }
    
    if([self.controller connect]){
        
        self.controller.textSize = BXL_TS_0WIDTH | BXL_TS_0HEIGHT;
        [self.controller printText:@"01234567890123456789012345678901234567890123456789012345678901234567890123456789"];
        [self.controller lineFeed:1];
        self.controller.textSize = BXL_TS_1WIDTH | BXL_TS_1HEIGHT;
        [self.controller printText:@"01234567890123456789012345678901234567890123456789"];
        [self.controller lineFeed:1];
        self.controller.textSize = BXL_TS_2WIDTH | BXL_TS_2HEIGHT;
        [self.controller printText:@"012345678901234567890123456789"];
        [self.controller lineFeed:1];
        self.controller.textSize = BXL_TS_3WIDTH | BXL_TS_3HEIGHT;
        [self.controller printText:@"01234567890123456789"];
        [self.controller lineFeed:1];
        self.controller.textSize = BXL_TS_4WIDTH | BXL_TS_4HEIGHT;
        [self.controller printText:@"01234567890"];
        [self.controller lineFeed:1];
        
        [self.controller cutPaper];
        
//        [self.controller disconnect];
        
    } else {
        PCError(@"print Test error");
    }
    
    return BXL_SUCCESS;
}

#pragma mark - Print

- (long)printOrder:(Order *)order
{
    return [self printOrder:order enqueueing:YES];
}

- (long)printOrder:(Order *)order enqueueing:(BOOL)enqueueing
{
    if(order == nil) {
        PCError(@"No order to print");
        return BXL_BC_DATA_ERROR;
    }
    
    if(self.controller.target == nil){
        [self.controller lookup];
        if(enqueueing) [self.printQueue addObject:order];
        return RUSHORDER_BXL_PRINTER_NOT_SET;
    }
    
    if(!self.controller.isConnected){
        if(enqueueing) [self.printQueue addObject:order];
        [self.controller connect];
        return RUSHORDER_BXL_NOT_CONNECTED;
    }
    
    if([self.controller connect]){
        
        [self.controller lineFeed:8];
        
        self.controller.textSize = BXL_TS_1WIDTH | BXL_TS_1HEIGHT;
        
        NSString *orderTypeString = nil;
        
        if(order.isTicketBase){
            
            switch(order.orderType){
                case OrderTypeTakeout:
                    orderTypeString = NSLocalizedString(@"Take-out", nil);
                    break;
                case OrderTypeDelivery:
                    orderTypeString = NSLocalizedString(@"Delivery", nil);
                    break;
                default:
                    orderTypeString = NSLocalizedString(@"Dine-in", nil);
                    break;
            }
        }
        
        if([order.tableNumber length] > 0){
            [self printTextLeft:[NSString stringWithFormat:@"TBL#%@",order.tableNumber]
                      rightText:[NSString stringWithFormat:@"ORD#%lld",order.orderNo]];
        } else {
            [self printTextLeft:orderTypeString
                      rightText:[NSString stringWithFormat:@"TIX#%lld",order.orderNo]];
        }
        
        [self drawDoubleLine];
        
        self.controller.textSize = BXL_TS_0WIDTH | BXL_TS_0HEIGHT;
        self.controller.alignment = BXL_ALIGNMENT_RIGHT;
        [self.controller printText:[[order.orderDate secondsString] stringByAppendingString:@" "]];
        [self.controller lineFeed:1];
        
        if(order.pickupAfter != 0 || order.isTakeout || order.isDelivery){
            
            self.controller.textSize = BXL_TS_1WIDTH | BXL_TS_1HEIGHT;
            self.controller.attribute = BXL_FT_DEFAULT;
            
            [self drawSingleLine];
            NSDate *dueDate = nil;
            if(order.pickupAfter != 0){
                dueDate = [order.orderDate dateByAddingTimeInterval:(order.pickupAfter * 60)];
            }
            [self printTextLeft:NSLocalizedString(@"Desired Time:", nil)
                      rightText:(dueDate == nil)?NSLocalizedString(@"ASAP", nil):[dueDate hourMinuteString]];
            
        }
        
        [self drawSingleLine];

        self.controller.alignment = BXL_ALIGNMENT_LEFT;
        
        NSInteger i = 0;
        for(LineItem *lineItem in order.lineItems){
            
            self.controller.textSize = BXL_TS_1WIDTH | BXL_TS_1HEIGHT;
//            self.controller.attribute = BXL_FT_BOLD;
            [self.controller printText:[@"*" stringByAppendingString:lineItem.displayMenuName]];
            
//            if([lineItem.menuChoiceName length] > 0){
//                [self.controller lineFeed:1];
//                self.controller.textSize = BXL_TS_1WIDTH | BXL_TS_1HEIGHT;
//                self.controller.attribute = BXL_FT_DEFAULT;
//                [self.controller printText:[@"-" stringByAppendingString:lineItem.menuChoiceName]];
//            }
            

            for(NSDictionary *groupDict in lineItem.menuOptionNamesObj){
                
                NSString *groupName = [groupDict objectForKey:@"name"];
            
                [self.controller lineFeed:1];
                self.controller.textSize = BXL_TS_0WIDTH | BXL_TS_0HEIGHT;
                self.controller.attribute = BXL_FT_DEFAULT;
                [self.controller printText:[NSString stringWithFormat:@"[%@]",
                                            groupName]];
                
                NSArray *selectedOptions = [groupDict objectForKey:@"menu_option_names"];
                for(NSDictionary *optionDict in selectedOptions){
                    NSString *optionName = [optionDict objectForKey:@"option_name"];

                    [self.controller lineFeed:1];
                    self.controller.textSize = BXL_TS_1WIDTH | BXL_TS_1HEIGHT;
                    self.controller.attribute = BXL_FT_DEFAULT;
                    [self.controller printText:[@"-" stringByAppendingString:optionName]];
                }
            }
            
            if([lineItem.specialInstruction length] > 0){
                [self.controller lineFeed:1];
                self.controller.textSize = BXL_TS_0WIDTH | BXL_TS_0HEIGHT;
                self.controller.attribute = BXL_FT_DEFAULT;
                [self.controller printText:NSLocalizedString(@"[Special Instruction]", nil)];
                [self.controller lineFeed:1];
                self.controller.textSize = BXL_TS_1WIDTH | BXL_TS_1HEIGHT;
                self.controller.attribute = BXL_FT_DEFAULT;
                [self.controller printText:lineItem.specialInstruction];
            }
            
            i++;
            
            if(i < [order.lineItems count]){
                [self.controller lineFeed:1];
                [self drawSingleLine];
            }
        }
        
        [self.controller lineFeed:1];
        
        if([order.customerRequest length] > 0){
            [self drawDoubleLine];
            self.controller.textSize = BXL_TS_0WIDTH | BXL_TS_0HEIGHT;
            [self.controller printText:NSLocalizedString(@"[Customer Request]", nil)];
            [self.controller lineFeed:1];
            self.controller.textSize = BXL_TS_1WIDTH | BXL_TS_1HEIGHT;
            [self.controller printText:order.customerRequest];
            [self.controller lineFeed:1];
        }
        
        
        self.controller.textSize = BXL_TS_0WIDTH | BXL_TS_0HEIGHT;
        self.controller.attribute = BXL_FT_DEFAULT;
        
        if(order.isTicketBase){
            
            NSString *nameString = nil;
            NSString *phoneString = nil;
            
            if([order.customers count] > 0){
                if([order.receiverName length] > 0){
                    nameString = order.receiverName;
                } else {
                    NSMutableString *names = [NSMutableString string];
                    
                    for(CustomerInfo *customer in order.customers){
                        if([names length] > 0) [names appendString:@", "];
                        [names appendString:customer.name];
                    }
                    
                    nameString = names;
                }
            } else {
                if([order.receiverName length] > 0){
                    nameString = order.receiverName;
                }
            }
            
            if([order.phoneNumber length] > 0){
                phoneString = order.phoneNumber;
            }
            
            if(([nameString length] > 0) || ([phoneString length] > 0)){
                [self drawSingleLine];
            }
            if([nameString length] > 0){
                [self.controller printText:[NSString stringWithFormat:NSLocalizedString(@"Name  : %@", nil), nameString]];
                [self.controller lineFeed:1];
            }
            if([phoneString length] > 0){
                [self.controller printText:[NSString stringWithFormat:NSLocalizedString(@"Phone : %@", nil), phoneString]];
                [self.controller lineFeed:1];
            }
            if(([nameString length] > 0) || ([phoneString length] > 0)){
                [self drawSingleLine];
            }
            
            if(order.orderType == OrderTypeDelivery){
                
                [self printTextLeft:NSLocalizedString(@"Delivery Address", nil)
                          rightText:@""];
                
                [self drawSingleLine];
                
                [self.controller printText:order.address];
                [self.controller lineFeed:1];
                
                [self drawDoubleLine];
            }
        } else {
            [self drawDoubleLine];
        }
        
        [self.controller lineFeed:4];
        [self.controller cutPaper];
        
//        [self.controller disconnect];
    } else {
        PCError(@"Bix Printer connection error");
        self.controller.target = nil;
        return RUSHORDER_BXL_CONNECT_ERROR;
    }
    
    return BXL_SUCCESS;
}

- (long)printCart:(Cart *)cart
{
    return [self printCart:cart enqueueing:YES];
}

- (long)printCart:(Cart *)cart enqueueing:(BOOL)enqueueing
{
    if(cart == nil) {
        PCError(@"No cart to print");
        return BXL_BC_DATA_ERROR;
    }
    
    if(self.controller.target == nil){
        [self.controller lookup];
        if(enqueueing) [self.printQueue addObject:cart];
        return RUSHORDER_BXL_PRINTER_NOT_SET;
    }
    
    if(!self.controller.isConnected){
        if(enqueueing) [self.printQueue addObject:cart];
        [self.controller connect];
        return RUSHORDER_BXL_NOT_CONNECTED;
    }
    
    if([self.controller connect]){
        
        [self.controller lineFeed:8];
        
        self.controller.textSize = BXL_TS_1WIDTH | BXL_TS_1HEIGHT;
        
        if(cart.orderNo == 0){
            [self printTextLeft:[NSString stringWithFormat:@"TBL#%@",cart.tableLabel]
                      rightText:[NSString stringWithFormat:@" TMP#%lld",cart.cartNo]];
        } else {
            [self printTextLeft:[NSString stringWithFormat:@"TBL#%@",cart.tableLabel]
                      rightText:[NSString stringWithFormat:@" ORD#%lld",cart.orderNo]];
        }
        
        [self drawDoubleLine];
        
        self.controller.textSize = BXL_TS_0WIDTH | BXL_TS_0HEIGHT;
        self.controller.alignment = BXL_ALIGNMENT_RIGHT;
        [self.controller printText:[[[NSDate date] secondsString] stringByAppendingString:@" "]];
        [self.controller lineFeed:1];
        
        [self drawSingleLine];
        
        self.controller.alignment = BXL_ALIGNMENT_LEFT;
        
        NSInteger i = 0;
        for(LineItem *lineItem in cart.lineItems){
            
            self.controller.textSize = BXL_TS_0WIDTH | BXL_TS_1HEIGHT;
//            self.controller.attribute = BXL_FT_BOLD;
            [self.controller printText:[@"*" stringByAppendingString:lineItem.displayMenuName]];
            
//            if([lineItem.menuChoiceName length] > 0){
//                [self.controller lineFeed:1];
//                self.controller.textSize = BXL_TS_1WIDTH | BXL_TS_1HEIGHT;
//                self.controller.attribute = BXL_FT_DEFAULT;
//                [self.controller printText:[@"-" stringByAppendingString:lineItem.menuChoiceName]];
//            }
            
            
            for(NSDictionary *groupDict in lineItem.menuOptionNamesObj){
                
                NSString *groupName = [groupDict objectForKey:@"name"];
                
                [self.controller lineFeed:1];
                self.controller.textSize = BXL_TS_0WIDTH | BXL_TS_0HEIGHT;
                self.controller.attribute = BXL_FT_DEFAULT;
                [self.controller printText:[NSString stringWithFormat:@"[%@]",
                                            groupName]];
                
                NSArray *selectedOptions = [groupDict objectForKey:@"menu_option_names"];
                for(NSDictionary *optionDict in selectedOptions){
                    NSString *optionName = [optionDict objectForKey:@"option_name"];
                    
                    [self.controller lineFeed:1];
                    self.controller.textSize = BXL_TS_1WIDTH | BXL_TS_1HEIGHT;
                    self.controller.attribute = BXL_FT_DEFAULT;
                    [self.controller printText:[@"-" stringByAppendingString:optionName]];
                }
            }
            
            if([lineItem.specialInstruction length] > 0){
                [self.controller lineFeed:1];
                self.controller.textSize = BXL_TS_0WIDTH | BXL_TS_0HEIGHT;
                self.controller.attribute = BXL_FT_DEFAULT;
                [self.controller printText:NSLocalizedString(@"[Special Instruction]", nil)];
                [self.controller lineFeed:1];
                self.controller.textSize = BXL_TS_1WIDTH | BXL_TS_1HEIGHT;
                self.controller.attribute = BXL_FT_DEFAULT;
                [self.controller printText:lineItem.specialInstruction];
            }
            
            i++;
            
            if(i < [cart.lineItems count]){
                [self.controller lineFeed:1];
                [self drawSingleLine];
            }
        }
        
        [self.controller lineFeed:1];
        [self drawDoubleLine];
        
        [self.controller lineFeed:4];
        [self.controller cutPaper];
        
//        [self.controller disconnect];
    } else {
        PCError(@"Bix Printer connection error");
        self.controller.target = nil;
        return RUSHORDER_BXL_CONNECT_ERROR;
    }
    
    return BXL_SUCCESS;
}

- (long)printBill:(BillWrapper *)bill
{
    return [self printBill:bill enqueueing:YES];
}

- (long)printBill:(BillWrapper *)bill enqueueing:(BOOL)enqueueing
{
    Order *order = (Order *)bill.order;
    
    if(order == nil) {
        PCError(@"No order to print for Bill");
        return BXL_BC_DATA_ERROR;
    }
    
    if(self.controller.target == nil){
        [self.controller lookup];
        if(enqueueing) [self.printQueue addObject:bill];
        return RUSHORDER_BXL_PRINTER_NOT_SET;
    }
    
    if(!self.controller.isConnected){
        if(enqueueing) [self.printQueue addObject:bill];
        [self.controller connect];
        return RUSHORDER_BXL_NOT_CONNECTED;
    }
    
    if([self.controller connect]){
    
        ////////////////////////////////////////////////////////////////////////
        //        self.controller.attribute = BXL_FT_FONTC;
        self.controller.alignment = BXL_ALIGNMENT_CENTER;
        self.controller.textSize = BXL_TS_1WIDTH | BXL_TS_1HEIGHT;
        
        [self.controller printText:CRED.merchant.name];
        
        [self.controller lineFeed:2];
        ////////////////////////////////////////////////////////////////////////
        
        ////////////////////////////////////////////////////////////////////////
        self.controller.textSize = BXL_TS_0WIDTH | BXL_TS_0HEIGHT;
        
        if(order.isTicketBase){
            if(order.isTakeout){
                [self printTextLeft:NSLocalizedString(@"Take-out Order", nil)
                          rightText:[NSString stringWithFormat:@"Ticket No.%lld",order.orderNo]];
            } else if (order.isDelivery){
                [self printTextLeft:NSLocalizedString(@"Delivery Order", nil)
                          rightText:[NSString stringWithFormat:@"Ticket No.%lld",order.orderNo]];
            } else {
                if([order.tableNumber length] > 0){
                    [self printTextLeft:NSLocalizedString(@"Dine-in Order", nil)
                              rightText:[NSString stringWithFormat:@"Table No.%@",order.tableNumber]];
                    
                    self.controller.alignment = BXL_ALIGNMENT_RIGHT;
                    [self.controller printText:[NSString stringWithFormat:@"Order No.%lld",order.orderNo]];
                } else {
                    [self printTextLeft:NSLocalizedString(@"Dine-in Order", nil)
                              rightText:[NSString stringWithFormat:@"Ticket No.%lld",order.orderNo]];
                }
            }
        } else {
            [self printTextLeft:[NSString stringWithFormat:@"Table No.%@",order.tableNumber]
                      rightText:[NSString stringWithFormat:@"Order No.%lld",order.orderNo]];
        }
        
        [self drawDoubleLine];
        
        self.controller.alignment = BXL_ALIGNMENT_RIGHT;
        [self.controller printText:[[[NSDate date] secondsString] stringByAppendingString:@" "]];
        
        [self.controller lineFeed:1];
        
        [self drawSingleLine];
        ////////////////////////////////////////////////////////////////////////
        
        NSInteger i = 0;
        for(LineItem *lineItem in order.lineItems){
            
            if(lineItem.selectedPrice != nil){
                [self printTextLeft:lineItem.displayMenuName
                          rightText:[[NSNumber numberWithCurrency:(lineItem.selectedPrice.price * lineItem.quantity)] currencyString]];
            } else {
                [self printTextLeft:lineItem.displayMenuName
                          rightText:lineItem.lineAmountString];
            }
            
            
            for(NSDictionary *groupDict in lineItem.menuOptionNamesObj){
                
                NSArray *selectedOptions = [groupDict objectForKey:@"menu_option_names"];
                for(NSDictionary *optionDict in selectedOptions){
                    NSString *optionName = [optionDict objectForKey:@"option_name"]; //option_price
                    PCCurrency optionPrice = [optionDict currencyForKey:@"option_price"];
                    NSString *optionPriceString = nil;
                    if(optionPrice != 0){
                        optionPriceString = [[NSNumber numberWithCurrency:(optionPrice * lineItem.quantity)] currencyString];
                    }
                    [self printTextLeft:[@"    " stringByAppendingString:optionName]
                              rightText:optionPriceString];
                }
            }
            
//            if([lineItem.serializedOptionNames length] > 0){
//                self.controller.alignment = BXL_ALIGNMENT_LEFT;
//                self.controller.attribute = BXL_FT_FONTB;
//                [self.controller printText:lineItem.serializedOptionNames];
//                self.controller.attribute = BXL_FT_DEFAULT;
//                
//                [self.controller lineFeed:1];
//            }
            
            
            
            i++;
        }
        
        if(i > 0){
            [self drawSingleLine];
        }
        
        [self printTextLeft:NSLocalizedString(@"Subtotal", nil)
                  rightText:order.subTotalString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
        
        [self printTextLeft:NSLocalizedString(@"Tax", nil)
                  rightText:order.taxesString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
        
        if(order.orderType == OrderTypeDelivery){
            [self printTextLeft:NSLocalizedString(@"Delivery Fee", nil)
                      rightText:order.totalPaidDeliveryFeeAmountsString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        }
        
        if(order.roServiceFee > 0){
            [self printTextLeft:NSLocalizedString(@"Service Fee", nil)
                      rightText:order.roServiceFeeString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        }
        
        [self drawSingleLine];
        
        [self printTextLeft:NSLocalizedString(@"Amount Due", nil)
                  rightText:order.totalString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
        
        [self drawDoubleLine];
        
        if(order.isTicketBase){
            
            NSString *nameString = nil;
            NSString *phoneString = nil;
            
            if([order.customers count] > 0){
                if([order.receiverName length] > 0){
                    nameString = order.receiverName;
                } else {
                    NSMutableString *names = [NSMutableString string];
                    
                    for(CustomerInfo *customer in order.customers){
                        if([names length] > 0) [names appendString:@", "];
                        [names appendString:customer.name];
                    }
                    
                    nameString = names;
                }
            } else {
                if([order.receiverName length] > 0){
                    nameString = order.receiverName;
                }
            }
            
            if([order.phoneNumber length] > 0){
                phoneString = order.phoneNumber;
            }
            
            if([nameString length] > 0){
                self.controller.alignment = BXL_ALIGNMENT_LEFT;
                [self.controller printText:[NSString stringWithFormat:NSLocalizedString(@"Name  : %@", nil), nameString]];
                self.controller.attribute = BXL_FT_DEFAULT;
                [self.controller lineFeed:1];
            }
            if([phoneString length] > 0){
                self.controller.alignment = BXL_ALIGNMENT_LEFT;
                [self.controller printText:[NSString stringWithFormat:NSLocalizedString(@"Phone : %@", nil), phoneString]];
                self.controller.attribute = BXL_FT_DEFAULT;
                [self.controller lineFeed:1];
            }
            
            BOOL printedDesiredTime = NO;
            if(order.pickupAfter != 0 || order.isTakeout || order.isDelivery){
                NSDate *dueDate = nil;
                if(order.pickupAfter != 0){
                    dueDate = [order.orderDate dateByAddingTimeInterval:(order.pickupAfter * 60)];
                }
                if(order.isTakeout || order.isDelivery){
                    self.controller.alignment = BXL_ALIGNMENT_LEFT;
                    [self.controller printText:[NSString stringWithFormat:NSLocalizedString(@"Desired Time : %@", nil),
                                                (dueDate == nil)?NSLocalizedString(@"ASAP", nil):[dueDate hourMinuteString]]];
                    self.controller.attribute = BXL_FT_DEFAULT;
                }
                [self.controller lineFeed:1];
                printedDesiredTime = YES;
            }
            
            if(([nameString length] > 0) || ([phoneString length] > 0) || printedDesiredTime){
                [self drawSingleLine];
            }
            
            if(order.orderType == OrderTypeDelivery){
                self.controller.alignment = BXL_ALIGNMENT_LEFT;
//                self.controller.attribute = BXL_FT_FONTB;
                
                [self.controller printText:NSLocalizedString(@"Delivery Address", nil)];
                [self.controller lineFeed:1];
                [self drawSingleLine];
                
                [self.controller printText:order.address];
                [self.controller lineFeed:1];
                
                [self drawDoubleLine];
            }
        } else {
            [self.controller printText:@"Checkout Instruction:"];
            [self.controller lineFeed:1];
            [self.controller printText:@"1. Sign-in and Checkout on your RushOrder app whenever you're ready."];
            [self.controller lineFeed:1];
            [self.controller printText:@"2. All available promotions or coupons will be applied at checkout."];
            [self.controller lineFeed:1];
            [self drawSingleLine];
        }
        
        [self.controller lineFeed:4];
        [self.controller cutPaper];
        
//        [self.controller disconnect];
    } else {
        PCError(@"Bix Printer connection error");
        self.controller.target = nil;
        return RUSHORDER_BXL_CONNECT_ERROR;
    }
    
    return BXL_SUCCESS;
}

- (long)printReceipt:(ReceiptWrapper *)receipt atIndex:(NSInteger)paymentIndex
{
    return [self printReceipt:receipt atIndex:paymentIndex enqueueing:YES];
}

- (long)printReceipt:(ReceiptWrapper *)receipt atIndex:(NSInteger)paymentIndex enqueueing:(BOOL)enqueueing
{
    Order *order = (Order *)receipt.order;
    
    if(order == nil) {
        PCError(@"No order to print for receipt");
        return BXL_BC_DATA_ERROR;
    }
    
    if([order.paymentList count] <= paymentIndex){
        PCError(@"No Payment to print for receipt");
        return BXL_BC_DATA_ERROR;
    }
    
    Payment *selectedPayment = [order.paymentList objectAtIndex:paymentIndex];
    
    if(self.controller.target == nil){
        [self.controller lookup];
        if(enqueueing) [self.printQueue addObject:receipt];
        return RUSHORDER_BXL_PRINTER_NOT_SET;
    }
    
    if(!self.controller.isConnected){
        if(enqueueing) [self.printQueue addObject:receipt];
        [self.controller connect];
        return RUSHORDER_BXL_NOT_CONNECTED;
    }

    if([self.controller connect]){
        
        ////////////////////////////////////////////////////////////////////////
        self.controller.alignment = BXL_ALIGNMENT_CENTER;
        UIImage *logoImage = CRED.merchant.logoImage;
        if(logoImage != nil){
            //        self.controller.attribute = BXL_FT_FONTC;
            self.controller.alignment = BXL_ALIGNMENT_CENTER;
            [self.controller printBitmapWithImage:logoImage
                                            width:((80 * logoImage.size.width) / logoImage.size.height)
                                            level:1040];
            [self.controller lineFeed:1];
        } else {
            if(CRED.merchant.logoURL != nil){
                SDWebImageManager *manager = [SDWebImageManager sharedManager];
                [manager downloadWithURL:CRED.merchant.logoURL
                                 options:0
                                progress:^(NSInteger receivedSize, NSInteger expectedSize)
                 {
                     PCLog(@"%u / %lld", receivedSize, expectedSize);
                 }
                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
                 {
                     if (image && finished)
                     {
                         CRED.merchant.logoImage = image;
                         
                         NSInteger i = 0;
                         
                         for(i = ([self.printQueue count] - 1) ; i >= 0 ; i--){
                             id obj = [self.printQueue objectAtIndex:i];
                             
                             if([obj isKindOfClass:[ReceiptWrapper class]]){
                                 
                                 ReceiptWrapper *receipt = (ReceiptWrapper *)obj;
                                 [self.printQueue removeObjectAtIndex:i];
                                 
                                 [self printReceipt:receipt
                                            atIndex:receipt.paymentIndex
                                         enqueueing:NO];
                                 
                             }
                         }
                     }
                 }];
                
                if(enqueueing) [self.printQueue addObject:receipt];
                return RUSHORDER_BXL_LOGO_NOT_SET;
                
            } else {
                // Keep going to print
            }
        }
        
        self.controller.alignment = BXL_ALIGNMENT_CENTER;
        self.controller.textSize = BXL_TS_1WIDTH | BXL_TS_1HEIGHT;
        
        [self.controller printText:CRED.merchant.name];
        [self.controller lineFeed:1];
        
        self.controller.textSize = BXL_TS_0WIDTH | BXL_TS_0HEIGHT;
        NSArray *addressArray = [CRED.merchant.multilineAddress componentsSeparatedByString:@"\n"];
        for(NSString *line in addressArray){
            [self.controller printText:line];
            [self.controller lineFeed:1];
        }
        
        [self.controller printText:CRED.merchant.phoneNumber];
        
        [self.controller lineFeed:2];
        ////////////////////////////////////////////////////////////////////////
        
    
        ////////////////////////////////////////////////////////////////////////        
        [self drawDoubleLine];
        
        if(order.isTicketBase){
            if(order.isTakeout){
                [self printTextLeft:NSLocalizedString(@"Take-out Order", nil)
                          rightText:[NSString stringWithFormat:@"Ticket No.%lld",order.orderNo]];
            } else if (order.isDelivery){
                [self printTextLeft:NSLocalizedString(@"Delivery Order", nil)
                          rightText:[NSString stringWithFormat:@"Ticket No.%lld",order.orderNo]];
            } else {
                if([order.tableNumber length] > 0){
                    [self printTextLeft:NSLocalizedString(@"Dine-in Order", nil)
                              rightText:[NSString stringWithFormat:@"Table No.%@",order.tableNumber]];
                    
                    self.controller.alignment = BXL_ALIGNMENT_RIGHT;
                    [self.controller printText:[NSString stringWithFormat:@"Order No.%lld",order.orderNo]];
                } else {
                    [self printTextLeft:NSLocalizedString(@"Dine-in Order", nil)
                              rightText:[NSString stringWithFormat:@"Ticket No.%lld",order.orderNo]];
                }
            }
        } else {
            [self printTextLeft:[NSString stringWithFormat:@"Table No.%@",order.tableNumber]
                      rightText:[NSString stringWithFormat:@"Order No.%lld",order.orderNo]];
        }
        
        [self.controller printText:[NSString stringWithFormat:@"Transaction No.%lld",selectedPayment.paymentNo]];
        [self.controller lineFeed:1];
        
        [self drawSingleLine];
        
        self.controller.alignment = BXL_ALIGNMENT_RIGHT;
        [self.controller printText:[[selectedPayment.payDate secondsString] stringByAppendingString:@" "]];
        
        [self.controller lineFeed:1];
        
        [self drawDoubleLine];
        ////////////////////////////////////////////////////////////////////////
        
        NSInteger i = 0;
        
        for(LineItem *lineItem in selectedPayment.lineItems){
            
            [self printTextLeft:lineItem.displayMenuName
                      rightText:[[NSNumber numberWithCurrency:(lineItem.unitPrice * lineItem.quantity)] currencyString]];
            
            for(NSDictionary *groupDict in lineItem.menuOptionNamesObj){
                
                NSArray *selectedOptions = [groupDict objectForKey:@"menu_option_names"];
                for(NSDictionary *optionDict in selectedOptions){
                    NSString *optionName = [optionDict objectForKey:@"option_name"]; //option_price
                    PCCurrency optionPrice = [optionDict currencyForKey:@"option_price"];
                    NSString *optionPriceString = nil;
                    if(optionPrice != 0){
                        optionPriceString = [[NSNumber numberWithCurrency:(optionPrice * lineItem.quantity)] currencyString];
                    }
                    [self printTextLeft:[@"    " stringByAppendingString:optionName]
                              rightText:optionPriceString];
                }
            }
//            
//            if([lineItem.serializedOptionNames length] > 0){
//                self.controller.alignment = BXL_ALIGNMENT_LEFT;
//                self.controller.attribute = BXL_FT_FONTB;
//                [self.controller printText:lineItem.serializedOptionNames];
//                self.controller.attribute = BXL_FT_DEFAULT;
//                
//                [self.controller lineFeed:1];
//            }
            i++;
        }
        
        if(i > 0){
            [self drawSingleLine];
        }

        if(order.orderType == OrderTypeBill){
            [self printTextLeft:NSLocalizedString(@"Subtotal", nil)
                      rightText:order.subTotalString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
            
            [self printTextLeft:NSLocalizedString(@"Tax", nil)
                      rightText:order.taxesString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        } else {
            [self printTextLeft:NSLocalizedString(@"Subtotal", nil)
                      rightText:selectedPayment.subTotalString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
            
            [self printTextLeft:NSLocalizedString(@"Tax", nil)
                      rightText:selectedPayment.taxesString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        }
        
        
        [self printTextLeft:NSLocalizedString(@"Tip", nil)
                  rightText:selectedPayment.tipAmountString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
        
        if(selectedPayment.deliveryFeeAmount > 0){
            [self printTextLeft:NSLocalizedString(@"Delivery Fee", nil)
                      rightText:selectedPayment.deliveryFeeAmountString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        }
        
        if(selectedPayment.roServiceFeeAmount > 0){
            [self printTextLeft:NSLocalizedString(@"Service Fee", nil)
                      rightText:selectedPayment.roServiceFeeAmountString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        }
        
        if(selectedPayment.discountAmount > 0){
            [self printTextLeft:NSLocalizedString(@"Discount", nil)
                      rightText:selectedPayment.discountAmountString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        }
        
        if(order.orderType == OrderTypeBill){
            [self printTextLeft:NSLocalizedString(@"Total", nil)
                      rightText:[[NSNumber numberWithCurrency:(order.total + selectedPayment.tipAmount)] currencyString]
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        } else {
            [self printTextLeft:NSLocalizedString(@"Total", nil)
                      rightText:selectedPayment.grandTotalString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        }

        [self drawSingleLine];
        
        [self printTextLeft:NSLocalizedString(@"Amount Paid", nil)
                  rightText:selectedPayment.netAmountString
             withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                rightMargin:0];
        
        if(selectedPayment.creditAmount > 0){
            [self printTextLeft:NSLocalizedString(@"Points Amount Paid", nil)
                      rightText:selectedPayment.creditAmountString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
        }
        
        [self drawSingleLine];
        
        [self.controller printText:[NSString stringWithFormat:NSLocalizedString(@"Card Type  %@",nil), [NSString issuerName:selectedPayment.issuer]]];
        [self.controller lineFeed:1];
        
        [self.controller printText:[NSString stringWithFormat:NSLocalizedString(@"Acc No.    %@",nil), [selectedPayment formattedCardNumberWithSecureString:@"*"]]];
        [self.controller lineFeed:1];
        
//        [self.controller printText:[NSString stringWithFormat:NSLocalizedString(@"Charge ID.   %@",nil), selectedPayment.chargeId]];
//        [self.controller lineFeed:1];
        
        [self drawSingleLine];
        
        if(selectedPayment.rewardedAmount > 0){
            [self printTextLeft:NSLocalizedString(@"Rewarded Point", nil)
                      rightText:selectedPayment.rewardedAmountString
                 withLeftMargin:MONEY_RECEIPT_LEFT_MARGIN
                    rightMargin:0];
            [self drawSingleLine];
        }
        
        if(order.isTicketBase){
            
            NSString *nameString = nil;
            NSString *phoneString = nil;
            
            if([order.customers count] > 0){
                if([order.receiverName length] > 0){
                    nameString = order.receiverName;
                } else {
                    NSMutableString *names = [NSMutableString string];
                    
                    for(CustomerInfo *customer in order.customers){
                        if([names length] > 0) [names appendString:@", "];
                        [names appendString:customer.name];
                    }
                    
                    nameString = names;
                }
            } else {
                if([order.receiverName length] > 0){
                    nameString = order.receiverName;
                }
            }
            
            if([order.phoneNumber length] > 0){
                phoneString = order.phoneNumber;
            }
            
            if([nameString length] > 0){
                self.controller.alignment = BXL_ALIGNMENT_LEFT;
//                self.controller.attribute = BXL_FT_FONTB;
                [self.controller printText:[NSString stringWithFormat:NSLocalizedString(@"Name  : %@", nil), nameString]];
                self.controller.attribute = BXL_FT_DEFAULT;
                [self.controller lineFeed:1];
            }
            if([phoneString length] > 0){
                self.controller.alignment = BXL_ALIGNMENT_LEFT;
//                self.controller.attribute = BXL_FT_FONTB;
                [self.controller printText:[NSString stringWithFormat:NSLocalizedString(@"Phone : %@", nil), phoneString]];
                self.controller.attribute = BXL_FT_DEFAULT;
                [self.controller lineFeed:1];
            }
            
            BOOL printedDesiredTime = NO;
            if(order.pickupAfter != 0 || order.isTakeout || order.isDelivery){
                NSDate *dueDate = nil;
                if(order.pickupAfter != 0){
                    dueDate = [order.orderDate dateByAddingTimeInterval:(order.pickupAfter * 60)];
                }
                if(order.isTakeout || order.isDelivery){
                    self.controller.alignment = BXL_ALIGNMENT_LEFT;
                    [self.controller printText:[NSString stringWithFormat:NSLocalizedString(@"Desired Time : %@", nil),
                                                (dueDate == nil)?NSLocalizedString(@"ASAP", nil):[dueDate hourMinuteString]]];
                    self.controller.attribute = BXL_FT_DEFAULT;
                }
                [self.controller lineFeed:1];
                printedDesiredTime = YES;
            }
            
            if(([nameString length] > 0) || ([phoneString length] > 0) || printedDesiredTime){
                [self drawSingleLine];
            }
            
            if(order.orderType == OrderTypeDelivery){
                self.controller.alignment = BXL_ALIGNMENT_LEFT;
//                self.controller.attribute = BXL_FT_FONTB;
                
                [self.controller printText:NSLocalizedString(@"Delivery Address", nil)];
                [self.controller lineFeed:1];
                [self drawSingleLine];
                
                [self.controller printText:order.address];
                [self.controller lineFeed:1];
                
                [self drawDoubleLine];
            }
        }
        
        
//        [self.controller printBarcode:[[NSString stringWithFormat:@"%012lld", order.orderNo] cStringUsingEncoding:NSASCIIStringEncoding]
//                            symbology:BXL_BCS_UPCA
//                                width:4
//                               height:80];
        
        [self.controller lineFeed:2];
        
        self.controller.alignment = BXL_ALIGNMENT_CENTER;
        [self.controller printText:NSLocalizedString(@"- Customer Copy -", nil)];
        [self.controller lineFeed:1];
        [self.controller printText:NSLocalizedString(@"Thank you for dining with us~", nil)];
        
        
        [self.controller lineFeed:3];
        
        [self drawDoubleLine];
        
        [self.controller lineFeed:4];
        
        [self.controller cutPaper];
        
        
        
    } else {
        PCError(@"Bix Printer connection error");
        self.controller.target = nil;
        return RUSHORDER_BXL_CONNECT_ERROR;
    }
    
    return BXL_SUCCESS;
}

- (void)drawSingleLine
{
    int bfAlignment = self.controller.alignment;
    
    self.controller.textSize = BXL_TS_0WIDTH | BXL_TS_0HEIGHT;
    self.controller.alignment = BXL_ALIGNMENT_LEFT;
    [self.controller printText:@"------------------------------------------"];
    [self.controller lineFeed:1];
    self.controller.alignment = bfAlignment;
}

- (void)drawDoubleLine
{
    int bfAlignment = self.controller.alignment;
    
    self.controller.textSize = BXL_TS_0WIDTH | BXL_TS_0HEIGHT;
    self.controller.alignment = BXL_ALIGNMENT_LEFT;
    [self.controller printText:@"=========================================="];
    [self.controller lineFeed:1];
    
    self.controller.alignment = bfAlignment;
}
- (void)printTextLeft:(NSString *)leftText rightText:(NSString *)rightText
{
    [self printTextLeft:leftText
              rightText:rightText
         withLeftMargin:0
            rightMargin:0];
    [self.controller lineFeed:1];
}

- (void)printTextLeft:(NSString *)leftText
            rightText:(NSString *)rightText
       withLeftMargin:(NSInteger)leftMargin
          rightMargin:(NSInteger)rightMargin
{
    if(leftText == nil) leftText = @"";
    if(rightText == nil) rightText = @"";
    
    NSUInteger maxCharacterLength = 41;
    
    NSInteger bxlTsWidth = BXL_TS_0WIDTH;
    
    if((self.controller.textSize & BXL_TS_1WIDTH) == BXL_TS_1WIDTH){
        maxCharacterLength = 20;
        bxlTsWidth = BXL_TS_1WIDTH;
    }
    
    self.controller.alignment = BXL_ALIGNMENT_LEFT;
    
    NSInteger leftTextCount = [leftText length];
    NSInteger rightTextCount = [rightText length];
    NSInteger occupiedCount = leftTextCount + rightTextCount + leftMargin + rightMargin;
    
    if(leftMargin > 0){
        
        occupiedCount -= leftTextCount;
        
        NSString *leftMarginString = nil;
        
        switch(bxlTsWidth){
            case BXL_TS_0WIDTH:
                leftMarginString = @"                                         "; //41
                break;
            case BXL_TS_1WIDTH:
                leftMarginString = @"                    "; //20
                break;
            default:
                break;
        }
        
        NSInteger index = (maxCharacterLength - leftMargin) + leftTextCount;
        index = MAX(index, 0);
        leftMarginString = [leftMarginString substringFromIndex:index];
        [self.controller printText:leftMarginString];
    }
    
    
    if(occupiedCount > maxCharacterLength){
        NSInteger restCount = occupiedCount - maxCharacterLength;
        
        leftText = [leftText substringToIndex:([leftText length] - (restCount + 1))];
        occupiedCount -= (restCount + 1);
    }
    
    [self.controller printText:leftText];

    NSString *spaceString = nil;
    
    switch(bxlTsWidth){
        case BXL_TS_0WIDTH:
            spaceString = @"                                         "; //41
            break;
        case BXL_TS_1WIDTH:
            spaceString = @"                    "; //20
            break;
        default:
            break;
    }
    
    NSInteger subIndex = (occupiedCount - 1);
    subIndex = MAX(subIndex, 0);
    
    spaceString = [spaceString substringFromIndex:subIndex];
    
    [self.controller printText:spaceString];
    [self.controller printText:rightText];
}

- (void)lookup
{
    [self.controller lookup];
}

#pragma mark - BXPrinterDelegate
- (void)message:(BXPrinterController *)controller
           text:(NSString *)text
{
    PCLog(@"[PRINTER message] %@", text);
}


-(void)didUpdateStatus:(BXPrinterController*) controller
                status:(NSNumber*) status
{
    PCLog(@"didUpdateStatus");
}


- (void)msrArrived:(BXPrinterController *)controller
             track:(NSNumber *)track
{
    PCLog(@"msrArrived");
}

- (void)msrTerminated:(BXPrinterController *)controller
{
    PCLog(@"msrTerminated");
}

- (void)willLookupPrinters:(BXPrinterController *)controller
{
    PCLog(@"willLookupPrinters");
}

- (void)didLookupPrinters:(BXPrinterController *)controller
{
    PCLog(@"didLookupPrinters");
    if(BXL_SUCCESS == [self.controller selectTarget]) {
        // Success
    } else {
        [self.printQueue removeAllObjects];
        [self treatPrintError:RUSHORDER_BXL_PRINTER_SET_UNAVAILABLE];
        [[NSNotificationCenter defaultCenter] postNotificationName:NoPrinterFoundNotification
                                                            object:self
                                                          userInfo:nil];
        self.controller.target = nil;
        return;
    }
    
    [self.controller connect]; //Initial Connect
}

- (void)treatPrintError:(long)errorCode
{
    switch(errorCode){
        case RUSHORDER_BXL_CONNECT_ERROR:
            [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot connect to printer", nil)
                                message:NSLocalizedString(@"Check your printer connected properly.", nil)
                               delegate:self
                                    tag:105];
            return;
        case RUSHORDER_BXL_PRINTER_NOT_FOUND:
            [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot find any printers connected", nil)
                                message:NSLocalizedString(@"Be sure the printer connected properly to the local network.", nil) // If you don't have printer, turn off Auto Printing at \"Settings\"
                               delegate:self
                                    tag:105];
            return;
        case BXL_BC_DATA_ERROR:
            [UIAlertView alertWithTitle:NSLocalizedString(@"There is no data for printing", nil)
                                message:NSLocalizedString(@"Each kind of printing needs appropriate data.", nil) // If you don't have printer, turn off Auto Printing at \"Settings\"
                               delegate:self
                                    tag:105];
            return;
        case RUSHORDER_BXL_PRINTER_SET_UNAVAILABLE:
            [UIAlertView alertWithTitle:NSLocalizedString(@"Unstable Printer Connection", nil)
                                message:NSLocalizedString(@"Please check to see if your printer is connected properly.", nil) // If you don't have printer, turn off Auto Printing at \"Settings\"
                               delegate:self
                                    tag:105];
            return;
        default:
            return;
    }
}

- (void)didFindPrinter:(BXPrinterController *)controller
               printer:(BXPrinter *)printer
{
    PCLog(@"didFindPrinter %@", printer);
    
    //Add 7
    if([printer.macAddress isEqualToString:self.macAddress]){
        self.controller.target = printer;
    }
}

- (void)willConnect:(BXPrinterController *)controller
            printer:(BXPrinter *)printer
{
    PCLog(@"willConnect");
}

- (void)didConnect:(BXPrinterController *)controller
           printer:(BXPrinter *)printer
{
    if(self.controller.target != nil){
        NSInteger i = 0;
        
        for(i = [self.printQueue count] - 1 ; i >= 0 ; i--){
            id obj = [self.printQueue objectAtIndex:i];
            
            long bixRtn = 0;
            
            if([obj isKindOfClass:[Order class]]){
                bixRtn = [self printOrder:obj enqueueing:NO];
            } else if([obj isKindOfClass:[Cart class]]){
                bixRtn = [self printCart:obj enqueueing:NO];
            } else if([obj isKindOfClass:[BillWrapper class]]){
                bixRtn = [self printBill:obj enqueueing:NO];
            } else if([obj isKindOfClass:[ReceiptWrapper class]]){
                ReceiptWrapper *receipt = (ReceiptWrapper *)obj;
                bixRtn = [self printReceipt:obj atIndex:receipt.paymentIndex enqueueing:NO];
            } else {
                PCError(@"Cannot find to print type");
            }
            
            if(bixRtn == 0){
                [self.printQueue removeObjectAtIndex:i];
            } else {
                [self treatPrintError:bixRtn];
            }
        }
    } else {
        [self.printQueue removeAllObjects];
        [self treatPrintError:RUSHORDER_BXL_PRINTER_NOT_FOUND];
        [[NSNotificationCenter defaultCenter] postNotificationName:NoPrinterFoundNotification
                                                            object:self
                                                          userInfo:nil];
    }
}

- (void)didNotConnect:(BXPrinterController *)controller
              printer:(BXPrinter *)printer
            withError:(NSError *)error
{
    PCLog(@"didNotConnect");
}

- (void)didDisconnect:(BXPrinterController *)controller
              printer:(BXPrinter *)printer
{
    PCLog(@"didDisconnect");
}

- (void)didBeBrokenConnection:(BXPrinterController *)controller
                      printer:(BXPrinter *)printer
                    withError:(NSError *)error
{
    PCLog(@"didBeBrokenConnection");
}

- (NSMutableArray *)printQueue
{
    if(_printQueue == nil){
        _printQueue = [NSMutableArray array];
    }
    return _printQueue;
}

@end
