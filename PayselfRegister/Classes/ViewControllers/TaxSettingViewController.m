//
//  TaxSettingViewController.m
//  RushOrder
//
//  Created by Conan on 7/30/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "TaxSettingViewController.h"
#import "FormatterManager.h"
#import "PCMerchantService.h"
#import "PCMerchantCredentialService.h"
#import "Merchant.h"

@interface TaxSettingViewController ()

@property (strong, nonatomic) NSNumberFormatter *numberFormatter;
@property (strong, nonatomic) NSNumberFormatter *deliveryNumberFormatter;
@property (weak, nonatomic) IBOutlet PCTextField *taxAmountTextField;
@property (weak, nonatomic) IBOutlet PCCurrencyTextField *deliveryFeeTextField;
@property (weak, nonatomic) IBOutlet PCCurrencyTextField *minimumOrderTextField;
@property (weak, nonatomic) IBOutlet PCTextField *deliveryLimitTextField;
@property (strong, nonatomic) NSNumber *taxRateNumber;
@property (strong, nonatomic) NSNumber *deliveryLimitNumber;
@property (nonatomic) NSInteger dotLocation;
@property (copy, nonatomic) NSString *nextDotString;
@property (nonatomic) NSInteger deliveryDotLocation;
@property (copy, nonatomic) NSString *deliveryNextDotString;
@property (weak, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NPStretchableButton *cancelButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *acceptTipButton;
@end

@implementation TaxSettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Tax, Delivery, Tip", nil);
    //JS did not add dine-in, take-out minimum order amount
    self.taxRateNumber = [NSNumber numberWithFloat:CRED.merchant.taxRate / 100.0f];
    self.deliveryLimitNumber = [NSNumber numberWithDouble:CRED.merchant.deliveryDistanceLimit];
    [self.taxAmountTextField becomeFirstResponder];
    [self fillTaxAmount:self.taxAmountTextField];
    [self fillDeliveryLimit:self.deliveryLimitTextField];
    
    self.deliveryFeeTextField.amount = CRED.merchant.deliveryFee;
    self.minimumOrderTextField.amount = CRED.merchant.deliveryMinOrderAmount;
    
    self.acceptTipButton.selected = CRED.merchant.isAllowTip;
    
    [self.scrollView setContentSizeWithBottomView:self.cancelButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTaxAmountTextField:nil];
    [super viewDidUnload];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == self.taxAmountTextField){
        if([string length] > 0){
            NSMutableCharacterSet *numberSet = [[NSCharacterSet decimalDigitCharacterSet] mutableCopy];
        //        [numberSet formUnionWithCharacterSet:[NSCharacterSet whitespaceCharacterSet]];
            [numberSet formUnionWithCharacterSet:[NSCharacterSet characterSetWithCharactersInString:@"."]];
            
            NSRange nonNumberRange = [string rangeOfCharacterFromSet:[numberSet invertedSet]];
            
            if(nonNumberRange.location != NSNotFound){
                return NO;
            }
        }

        NSMutableString *compString = [textField.text mutableCopy];

        BOOL hasPercentSuffix = [compString hasSuffix:@"%"];

        if(hasPercentSuffix){
            NSInteger percentIndex = [compString length] - 1;
            [compString deleteCharactersInRange:NSMakeRange(percentIndex, 1)];
            
            if(range.length > 0){
                range.length -= (percentIndex == range.location) ? 1 : 0;
            }
            
            range.location -= percentIndex >= range.location ? 0 : 1;
        }

        // Main behavior
        if(range.length > 0){
            [compString deleteCharactersInRange:range];
        } else {
            [compString insertString:string atIndex:range.location];
        }

        if([compString length] == 0){
            self.taxRateNumber = @0;
            [self fillTaxAmount:textField];
//            textField.text = nil;
        } else {

            NSRange dotRange = [compString rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"."]];
            self.dotLocation = dotRange.location;
            
            if(self.dotLocation != NSNotFound){
                
                if([compString length] > self.dotLocation){
                    
                    self.nextDotString = [compString substringFromIndex:self.dotLocation];
                    
                    
                    NSInteger lastValidCharacterIndex = NSNotFound;
                    for(NSInteger i = 0 ; i < [self.nextDotString length] ; i++){
                        unichar aChar = [self.nextDotString characterAtIndex:i];
                        if(aChar != '.' && aChar != '0'){
                            //Must be value above 0
                            lastValidCharacterIndex = i;
                        }
                    }
                    
                    if(lastValidCharacterIndex == NSNotFound){
                        [compString deleteCharactersInRange:NSMakeRange(self.dotLocation, [compString length] - self.dotLocation)];
                    } else {
                        lastValidCharacterIndex++;
                        
                        if([self.nextDotString length] > lastValidCharacterIndex){
                            self.nextDotString = [self.nextDotString substringFromIndex:lastValidCharacterIndex];
                            NSInteger newIndex = self.dotLocation + lastValidCharacterIndex;
                            self.dotLocation = newIndex;
                            [compString deleteCharactersInRange:NSMakeRange(newIndex, [compString length] - newIndex)];
                        } else {
                            self.dotLocation = NSNotFound;
                        }
                    }
                }
            }
            
            
            [compString appendString:@"%"];
        //        compString = [NSMutableString stringWithString:@"32.393892393929383838373737%"]; //Test code
            self.taxRateNumber = [self.numberFormatter numberFromString:compString];
            
            [self fillTaxAmount:textField];
        }

        //    self.taxRateNumber = [NSNumber numberWithFloat:([compString floatValue] / 100.0f)];


        PCLog(@"String: %@", textField.text);
        PCLog(@"Range : %@", NSStringFromRange(range));
        PCLog(@"R S   : %@", string);
        PCLog(@"Number: %@", self.taxRateNumber);
        
    } else {
        
        if([string length] > 0){
            
            NSMutableCharacterSet *numberSet = [[NSCharacterSet decimalDigitCharacterSet] mutableCopy];

            [numberSet formUnionWithCharacterSet:[NSCharacterSet characterSetWithCharactersInString:@"."]];
            
            NSRange nonNumberRange = [string rangeOfCharacterFromSet:[numberSet invertedSet]];
            
            if(nonNumberRange.location != NSNotFound){
                return NO;
            }
        }

        NSMutableString *compString = [textField.text mutableCopy];

#define SUFFIX_LENGTH   3
        BOOL hasPercentSuffix = [compString hasSuffix:@" mi"];

        if(hasPercentSuffix){
            NSInteger percentIndex = [compString length] - SUFFIX_LENGTH;
            [compString deleteCharactersInRange:NSMakeRange(percentIndex, SUFFIX_LENGTH)];
            
            if(range.length > 0){
                range.length -= (percentIndex == range.location) ? SUFFIX_LENGTH : 0;
            }
            
            range.location -= percentIndex >= range.location ? 0 : SUFFIX_LENGTH;
        }
        
        // Main behavior
        if(range.length > 0){
            [compString deleteCharactersInRange:range];
        } else {
            [compString insertString:string atIndex:range.location];
        }
        
        if([compString length] == 0){
//            textField.text = nil;
            self.deliveryLimitNumber = @0;
            [self fillDeliveryLimit:textField];
        } else {
            
            NSRange dotRange = [compString rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"."]];
            self.deliveryDotLocation = dotRange.location;
            
            if(self.deliveryDotLocation != NSNotFound){
                
                if([compString length] > self.deliveryDotLocation){
                    
                    self.deliveryNextDotString = [compString substringFromIndex:self.deliveryDotLocation];
                    
                    NSInteger lastValidCharacterIndex = NSNotFound;
                    for(NSInteger i = 0 ; i < [self.deliveryNextDotString length] ; i++){
                        unichar aChar = [self.deliveryNextDotString characterAtIndex:i];
                        if(aChar != '.' && aChar != '0'){
                            lastValidCharacterIndex = i;
                        }
                    }
                    
                    if(lastValidCharacterIndex == NSNotFound){
                        [compString deleteCharactersInRange:NSMakeRange(self.deliveryDotLocation, [compString length] - self.deliveryDotLocation)];
                    } else {
                        lastValidCharacterIndex++;
                        
                        if([self.deliveryNextDotString length] > lastValidCharacterIndex){
                            self.deliveryNextDotString = [self.deliveryNextDotString substringFromIndex:lastValidCharacterIndex];
                            NSInteger newIndex = self.deliveryDotLocation + lastValidCharacterIndex;
                            self.deliveryDotLocation = newIndex;
                            [compString deleteCharactersInRange:NSMakeRange(newIndex, [compString length] - newIndex)];
                        } else {
                            self.deliveryDotLocation = NSNotFound;
                        }
                    }
                }
            }
            
            if([compString floatValue] >= 1000.0f){
                
            } else {
                self.deliveryLimitNumber = [self.deliveryNumberFormatter numberFromString:compString];
                
                [self fillDeliveryLimit:textField];
            }
        }
        
        //    self.taxRateNumber = [NSNumber numberWithFloat:([compString floatValue] / 100.0f)];
        
        PCLog(@"String: %@", textField.text);
        PCLog(@"Range : %@", NSStringFromRange(range));
        PCLog(@"R S   : %@", string);
        PCLog(@"Number: %@", self.deliveryLimitNumber);
    }
    
    return NO;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if(textField == self.taxAmountTextField){
        self.taxRateNumber = @0;
        [self fillTaxAmount:textField];
        return NO;
    } else {
        self.deliveryLimitNumber = @0;
        [self fillDeliveryLimit:textField];
        return NO;
    }
    
    return YES;
}

- (void)fillTaxAmount:(UITextField *)textField
{
//    self.taxRateNumber = [NSNumber numberWithFloat:0.4567];
    
    if(self.taxRateNumber == nil){
        textField.text = @"100%";
    } else {
        NSMutableString *resultString = [[self.numberFormatter stringFromNumber:self.taxRateNumber] mutableCopy];
        
        if(self.dotLocation != NSNotFound){
            if(self.nextDotString != nil){
                [resultString insertString:self.nextDotString atIndex:self.dotLocation];
            }
        }
        
        textField.text = resultString;
    }
    
    self.dotLocation = NSNotFound;
    self.nextDotString = nil;
    
    UITextRange *textRange = textField.selectedTextRange;
    
    if(textRange != nil){
        UITextPosition *newPosition = [textField positionFromPosition:textField.endOfDocument
                                                               offset:-1];
        textField.selectedTextRange = [textField textRangeFromPosition:newPosition
                                                            toPosition:newPosition];
    }
}

- (void)fillDeliveryLimit:(UITextField *)textField
{
    if(self.deliveryLimitNumber == nil){
        textField.text = @"1 mi";
    } else {
        NSMutableString *resultString = [[self.deliveryNumberFormatter stringFromNumber:self.deliveryLimitNumber] mutableCopy];
        [resultString appendString:@" mi"];
        if(self.deliveryDotLocation != NSNotFound){
            if(self.deliveryNextDotString != nil){
                [resultString insertString:self.deliveryNextDotString atIndex:self.deliveryDotLocation];
            }
        }
        
        textField.text = resultString;
    }
    
    self.deliveryDotLocation = NSNotFound;
    self.deliveryNextDotString = nil;
    
    UITextRange *textRange = textField.selectedTextRange;
    
    if(textRange != nil){
        UITextPosition *newPosition = [textField positionFromPosition:textField.endOfDocument
                                                               offset:-SUFFIX_LENGTH];
        textField.selectedTextRange = [textField textRangeFromPosition:newPosition
                                                            toPosition:newPosition];
    }
}

- (IBAction)doneButtonTouched:(id)sender
{
    RequestResult result = RRFail;
    result = [MERCHANT requestTaxMerchant:CRED.merchant
                                taxAmount:self.taxRateNumber
                              deliveryFee:self.deliveryFeeTextField.amount
               minimumDeliveryOrderAmount:self.minimumOrderTextField.amount
                         deliveryDistance:self.deliveryLimitNumber
                               acceptTips:self.acceptTipButton.isSelected
                          completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              Merchant *merchant = nil;
                              
                              if(response.data != nil){
                                  merchant = [[Merchant alloc] initWithDictionary:response.data];
                              }
                              CRED.merchant = merchant;
                              
                              [self dismissViewControllerAnimated:YES
                                                       completion:^{}];
                          }
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while updating tax amount.", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (IBAction)cancelButtonTouched:(id)sender
{
    if(self.isRootViewController){
        [self dismissViewControllerAnimated:YES
                                 completion:^{
                                     
                                 }];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

//
//unsigned short MakeCRC16(unsigned char *buffer, int len)
//{
//    unsigned short crc_accum = 0;
//    int i, j;
//    for (j=0; j < len; j++){
//        i = ((unsigned short)(crc_accum >> 8) ^ *buffer++) & 0xff;
//        crc_accum = (crc_accum << 8) ^ crc_table[i];
//    }
//    return = crc_accum;
//}

- (NSNumberFormatter *)numberFormatter
{
    if(_numberFormatter == nil){
        _numberFormatter = [[NSNumberFormatter alloc] init];
        _numberFormatter.numberStyle = NSNumberFormatterPercentStyle;
        _numberFormatter.allowsFloats = YES;
        _numberFormatter.maximum = [NSNumber numberWithInteger:1];
        _numberFormatter.maximumFractionDigits = 4;
    }
    return _numberFormatter;
}

- (NSNumberFormatter *)deliveryNumberFormatter
{
    if(_deliveryNumberFormatter == nil){
        _deliveryNumberFormatter = [[NSNumberFormatter alloc] init];
        _deliveryNumberFormatter.numberStyle = kCFNumberFormatterDecimalStyle;
        _deliveryNumberFormatter.allowsFloats = YES;
        _deliveryNumberFormatter.maximum = [NSNumber numberWithInteger:999];
        _deliveryNumberFormatter.maximumFractionDigits = 2;
    }
    return _deliveryNumberFormatter;
}

@end