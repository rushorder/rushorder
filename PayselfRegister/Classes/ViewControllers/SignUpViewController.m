//
//  SignUpViewController.m
//  RushOrder
//
//  Created by Conan on 3/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "SignUpViewController.h"
#import "MerchantSettingViewController.h"
#import "StaffJoinViewController.h"

@interface SignUpViewController ()

@property (weak, nonatomic) IBOutlet PCTextField *idTextField;
@property (weak, nonatomic) IBOutlet PCTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet PCTextField *password2TextField;
@property (weak, nonatomic) IBOutlet PCTextField *nameTextField;
@property (weak, nonatomic) IBOutlet UISwitch *testDriveSwitch;

@property (weak, nonatomic) IBOutlet NPStretchableButton *doneButton;
@property (weak, nonatomic) IBOutlet UISegmentedControl *roleSegmentedControl;
@property (weak, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;

@end

@implementation SignUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Sign up", nil);
    
    self.idTextField.text = self.email;
    
    [self.scrollView setContentSizeWithBottomView:self.doneButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [self setIdTextField:nil];
    [self setPasswordTextField:nil];
    [self setPassword2TextField:nil];
    [self setTestDriveSwitch:nil];
    [self setTestDriveSwitch:nil];
    [self setDoneButton:nil];
    [super viewDidUnload];
}

- (IBAction)doneButtonTouched:(id)sender
{
    if(![VALID validate:self.idTextField
                message:NSLocalizedString(@"Enter your ID (email address)", nil)])
        return;
    
    
    if(![VALID validate:self.passwordTextField
                message:NSLocalizedString(@"Enter password.", nil)]) return;
    
    if(![self.passwordTextField.text isEqualToString:self.password2TextField.text]){
        [UIAlertView alertWithTitle:UNDEFINED_ALERT_TITLE
                            message:NSLocalizedString(@"Password does not match", nil)
                           delegate:self
                                tag:101];
        return;
    }
    
//    if(![VALID validate:self.nameTextField
//                message:NSLocalizedString(@"Enter your name.", nil)]) return;
    

    [self proceedSignUp];
}


- (void)proceedSignUp
{
    RequestResult result = RRFail;
    result = [CRED requestSignUp:self.idTextField.text
                        password:self.passwordTextField.text
                            role:(self.roleSegmentedControl.selectedSegmentIndex + 1)
                            name:self.nameTextField.text
                 completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              [self requestSignIn];
                              
                              break;
                          case ResponseErrorSignUp: //Any error
                          {
                              NSDictionary *message = [response objectForKey:@"message"];
                              
                              NSArray *keys = [message allKeys];

                              NSMutableString *msg = [NSMutableString string];
                              
                              for(NSString *key in keys){
                                  if([msg length] > 0) [msg appendString:@", "];
                                  NSArray *value = [message objectForKey:key];
                                  [msg appendString:key];
                                  [msg appendString:@" "];
                                  
                                  NSInteger i = 0;
                                  for(NSString *val in value){
                                      if(i > 0) [msg appendString:@", "];
                                      [msg appendString:val];
                                      i++;
                                  }
                              }
                              
                              if(message != nil){
                                  [UIAlertView alert:msg];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while signing up", nil)];
                              }
                              break;
                          }
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while signing up", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
                  self.doneButton.enabled = YES;
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            self.doneButton.enabled = NO;
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)requestSignIn
{    
    [NSUserDefaults standardUserDefaults].lastTriedUserId = self.idTextField.text;
    
    NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
    RequestResult result = RRFail;
    result = [CRED requestSignIn:self.idTextField.text
                        password:self.passwordTextField.text
                     deviceToken:[NSUserDefaults standardUserDefaults].deviceToken
                         version:appVersion
                 completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
//                              [APP checkNotice];
                              
                              [NSUserDefaults standardUserDefaults].autoLogin = YES;
                              
                              [CRED.userAccountItem setObject:self.idTextField.text
                                                       forKey:(__bridge id)(kSecAttrAccount)];
                              [CRED.userAccountItem setObject:self.passwordTextField.text
                                                       forKey:(__bridge id)(kSecValueData)];
                                                            
                              
                              CRED.userInfo.email = self.idTextField.text;
                              CRED.merchant = [response objectForKey:@"merchant"];
                              
                              if(CRED.merchant == nil){
                                  
                                  switch(CRED.userInfo.role){
                                      case UserRoleOwner:
                                          [self pushMerchantSettingViewController];
                                          break;
                                      case UserRoleStaff:
                                          [self pushStaffJoinViewController];
                                          break;
                                      case UserRoleUnknown:
                                      default:
                                          
                                          break;
                                  }
                                  
                              } else {
                                  [self dismissViewControllerAnimated:YES
                                                           completion:^(){
                                                               if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                                                                   if(CRED.merchant.isTableBase){
                                                                       [APP transitDashboard];
                                                                   } else {
                                                                       [APP transitFastFood];
                                                                   }
                                                               } else {
                                                                   [APP transitTableStatus];
                                                               }
                                                           }];
                              }
                              
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while signing in", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                      [NSUserDefaults standardUserDefaults].autoLogin = NO;
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)pushMerchantSettingViewController
{
    MerchantSettingViewController *viewController = [MerchantSettingViewController viewControllerFromNib];
    viewController.inProcessSignUp = YES;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)pushStaffJoinViewController
{
    StaffJoinViewController *viewController = [StaffJoinViewController viewControllerFromNib];
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 1:
            if(buttonIndex == AlertButtonIndexYES){
                [self proceedSignUp];
            }
            break;
        case 2:
            if(buttonIndex == AlertButtonIndexYES){
                [self proceedSignUp];
            }
            break;
        default:
            break;
    }
}
- (IBAction)testSwitchValueChanged:(id)sender
{


}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isKindOfClass:[PCTextField class]]){
//        [textField resignFirstResponder];
        [[(PCTextField *)textField nextField] becomeFirstResponder];
    }
    return YES;
}
@end
