//
//  TableButtonViewController.m
//  RushOrder
//
//  Created by Conan on 3/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "TableButtonViewController.h"
#import "MerchantSettingViewController.h"
#import "TableButton.h"
#import "TableStatusManager.h"
#import "OrderDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "TableLabelSettingViewController.h"
#import "PCMerchantService.h"
#import "TaxSettingViewController.h"
#import "MerchantAccountViewController.h"
#import "OrderPopoverBackgroundView.h"
#import "MenuManager.h"
#import "SwitchMenuViewController.h"

@interface TableButtonViewController ()

@property (strong, nonatomic) UIActionSheet *actionSheet;
@property (strong, nonatomic) UIPopoverController *popover;
@property (weak, nonatomic) IBOutlet UILabel *merchantTitleLabel;
@property (weak, nonatomic) IBOutlet PCDynamicGridView *gridView;

@property (weak, nonatomic) IBOutlet UILabel *completeCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *billRequestedCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *partialPaidCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *emptyCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *billIssuedCountLabel;

@property (weak, nonatomic) IBOutlet UIView *legendContainer;
@property (weak, nonatomic) IBOutlet UIView *emptyLegendBox;

@property (strong, nonatomic) UIPopoverController *settingPopoverController;
@property (strong, nonatomic) IBOutlet UIView *noItemView;

@property (weak, nonatomic) IBOutlet UIButton *settingButton;
@property (strong, nonatomic) IBOutlet UIButton *locationUpdateButton;
@property (strong, nonatomic) IBOutlet UIButton *menuSwitchButton;

@property (strong, nonatomic) IBOutlet UILabel *dashboardTitleLabel;
@end

@implementation TableButtonViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self != nil){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signedIn:)
                                                 name:SignedInNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(openSettingView:)
                                                name:OpenSettingPopoverViewNotification
                                              object:nil];
}

- (void)sumUpdated
{
    self.completeCountLabel.text = [NSString stringWithInteger:TABLE.completedCount];
    self.billRequestedCountLabel.text = [NSString stringWithInteger:TABLE.billRequestedCount];
    self.emptyCountLabel.text = [NSString stringWithInteger:TABLE.emptyCount];
    self.billIssuedCountLabel.text = [NSString stringWithInteger:TABLE.billIssuedCount + TABLE.partialPaidCount];
}

- (void)tableStatusUpdated:(NSNotification *)aNoti
{
    if(aNoti.object != nil){
        [self.gridView reloadData];
        [self sumUpdated];
    }
}

- (void)sumCountUpdated:(NSNotification *)aNoti
{
    if(aNoti.object != nil){
        [self sumUpdated];
        if([aNoti.object isKindOfClass:[TableInfo class]]){
            TableInfo *castedTableInfo = (TableInfo *)aNoti.object;
            NSUInteger i = 0;
            for(TableInfo *info in TABLE.tableOnlyList){
                if([info.tableNumber isCaseInsensitiveEqual:castedTableInfo.tableNumber]){
                    break;
                }
                i++;
            }
            [self.gridView updateCellAtIndex:i];
        }
    }
}

- (void)orderUpdate:(NSNotification *)aNoti
{
    if([aNoti.object isKindOfClass:[TableInfo class]]){
        [self.gridView reloadData];
        [self sumUpdated];
    }
}

- (void)signedIn:(NSNotification *)aNoti
{
    self.merchantTitleLabel.text = CRED.merchant.name;
    self.dashboardTitleLabel.text = CRED.merchant.name;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.edgesForExtendedLayout = UIRectEdgeNone;
    
//    self.gridView.dataSrouce = self;
//    self.gridView.delegate = self;
    
    self.legendContainer.layer.cornerRadius = 8.0f;
    self.legendContainer.layer.masksToBounds = YES;
    
    [self.emptyLegendBox drawBorderWithColor:[UIColor darkGrayColor]];
    
//    self.openSwitch.on = CRED.merchant.isOpenHour;
//    self.openSwitch.onText = @"OPEN";
//    self.openSwitch.offText= @"CLOSED";
//    self.openSwitch.onTintColor = [UIColor colorWithR:231.0f G:158.0f B:58.0f];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.gridView reloadData];
    [self sumUpdated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tableStatusUpdated:)
                                                 name:TableListUpdatedNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sumCountUpdated:)
                                                 name:SumCountUpdatedNotification
                                               object:nil];
    
    [self layoutDirectUpdateButtons];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:TableListUpdatedNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:SumCountUpdatedNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)uiLogoutProcess
{
    [APP transitLogin];
    [CRED.userAccountItem resetKeychainItem];
    [NSUserDefaults standardUserDefaults].autoLogin = NO;
}

- (IBAction)signOutButtonTouched:(id)sender
{
    [self uiLogoutProcess];
    
    RequestResult result = RRFail;
    result = [CRED requestSignOutCompletionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              PCLog(@"Successfully signed out");
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while signing out", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (IBAction)settingButtonTouched:(id)sender
{    
//    [self.actionSheet showFromBarButtonItem:sender
//                                   animated:YES];
    
    [self.settingPopoverController dismissPopoverAnimated:NO];
    MerchantSettingMenuViewController *viewController = [MerchantSettingMenuViewController viewControllerFromNib];
    viewController.delegate = self;
    
    PCNavigationController *navi = [[PCNavigationController alloc] initWithRootViewController:viewController];
    
//    if(self.settingPopoverController == nil){
        self.settingPopoverController = [[UIPopoverController alloc]
                                         initWithContentViewController:navi];
        self.settingPopoverController.delegate = self;
        self.settingPopoverController.popoverBackgroundViewClass = [OrderPopoverBackgroundView class];
//    } else {
//        self.settingPopoverController.contentViewController = navi;
//    }
    viewController.popover = self.settingPopoverController;
    
    if([sender isKindOfClass:[UIBarButtonItem class]]){
        [self.settingPopoverController presentPopoverFromBarButtonItem:(UIBarButtonItem *)sender
                                              permittedArrowDirections:UIPopoverArrowDirectionAny
                                                              animated:YES];
    } else {
        [self.settingPopoverController presentPopoverFromRect:((UIButton *)sender).frame
                                                       inView:self.view
                                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                                     animated:YES];
    }
}

- (void)openSettingView:(NSNotification *)aNoti
{
    if(self.parentViewController != nil){
        [self settingButtonTouched:self.settingButton];
    }
}

- (UIActionSheet *)actionSheet
{
    if(_actionSheet == nil){
        
        NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
        NSString *appShortVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        
        _actionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"v%@ (%@)", appShortVersion, appVersion]
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"Cancel",nil)
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:
                       NSLocalizedString(@"Merchant Information",nil),
                       NSLocalizedString(@"Manage Table Labels",nil), nil];
    }
    return _actionSheet;
}

- (IBAction)sesetAllButtonTouched:(id)sender
{
//    [self.gridView reloadData];
}

- (IBAction)reportButtonTouched:(id)sender
{
    [self.settingPopoverController dismissPopoverAnimated:NO];
    [self.popover dismissPopoverAnimated:NO];
    [APP transitReport];
}

#pragma mark - PCDynamicGridViewDataSource
- (NSInteger)numberOfItemsInGridView:(PCDynamicGridView *)gridView
{
//    return CRED.merchant.numberOfTables;
    NSInteger ctr = [TABLE.tableOnlyList count];
    
    if(ctr == 0){
        self.noItemView.frame = self.gridView.frame;
        [self.view addSubview:self.noItemView];
    } else {
        [self.noItemView removeFromSuperview];
    }
    return ctr;
}

- (UIView *)gridView:(PCDynamicGridView *)gridView cellForItemIndex:(NSInteger)index
{
    TableButton *tableButton = [[TableButton alloc] initWithFrame:CGRectZero];
    tableButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    tableButton.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    tableButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 5.0f, 7.0f);
    tableButton.titleLabel.font = [UIFont systemFontOfSize:13.0f];
    tableButton.titleLabel.numberOfLines = 3;
    [tableButton setTitleColor:[UIColor whiteColor]
                      forState:UIControlStateNormal];
    [tableButton setTitleColor:[UIColor colorWithWhite:0.9f alpha:1.0f]
                      forState:UIControlStateHighlighted];
    
    [tableButton addTarget:self
                    action:@selector(tableButtonTouched:)
          forControlEvents:UIControlEventTouchUpInside];
    tableButton.swipeTarget = self;
    tableButton.swipeSelector = @selector(tableButtonSwiped:);

    return tableButton;
}

- (void)gridView:(PCDynamicGridView *)gridView updateCell:(UIView *)cell atIndex:(NSInteger)index;
{
    TableInfo *tableInfo = [TABLE.tableOnlyList objectAtIndex:index];
    
    if(![tableInfo isKindOfClass:[TableInfo class]]){
        return;
    }
         
    TableButton *tableButton = (TableButton *)cell;
    tableButton.tableNumberLabel.text = tableInfo.tableNumber;
    tableButton.tableNumber = tableInfo.tableNumber;
    [tableButton setImage:nil
                 forState:UIControlStateNormal];
    
//    tableButton.enLarged = NO;
    
    if(tableInfo.status == TableStatusEmpty){
        tableButton.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.35f];
        
        tableButton.buttonTitle = nil;

    } else {
        if(tableInfo.status == TableStatusBillReqeusted){
            tableButton.buttonTitle = nil;
            [tableButton setImage:[UIImage imageNamed:@"icon_bell"]
                         forState:UIControlStateNormal];
            
//            tableButton.enLarged = YES;
        } else {
            NSString *payAmountString = [[NSNumber numberWithCurrency:tableInfo.order.netPayAmounts + tableInfo.order.discountedAmount] currencyString];

            tableButton.buttonTitle = [NSString stringWithFormat:@"%@\n(%@)\n%@",
                                       payAmountString,
                                       tableInfo.order.tipsString,
                                       tableInfo.order.totalString];
        }
        tableButton.backgroundColor = tableInfo.statusColor;
    }
    
    if(tableInfo.cart){
        if(tableInfo.cart.status == CartStatusSubmitted){
            tableButton.buttonTitle = NSLocalizedString(@"New Order", nil);
            tableButton.buttonTitle = [NSString stringWithFormat:@"New Order\n%@",
                                       tableInfo.cart.subtotalAmountString];
        } else if(tableInfo.cart.status == CartStatusOrdering){
            tableButton.buttonTitle = NSLocalizedString(@"Ordering", nil);
            tableButton.buttonTitle = [NSString stringWithFormat:@"Ordering\n%@",
                                       tableInfo.cart.subtotalAmountString];
        }
        tableButton.backgroundColor = tableInfo.statusColor;
    }
}

- (IBAction)onSwitchValueChanged:(UISwitch *)sender
{
    if(!sender.on){
        [UIAlertView askWithTitle:NSLocalizedString(@"Confirm Close?", nil)
                          message:NSLocalizedString(@"Are you sure to close the restaurant?", nil)
                         delegate:self
                              tag:303];
    } else {
        [self requestMerchantModeChangeWithType:MerchantSwitchTypeOpen value:sender.on];
    }
}


- (void)requestMerchantModeChangeWithType:(MerchantSwitchType)type
                                    value:(BOOL)value
{
    RequestResult result = RRFail;
    result = [MERCHANT requestMerchant:CRED.merchant
                                  type:type
                                 value:value
                       completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              switch(type){
                                  case MerchantSwitchTypeTest:
                                      CRED.merchant.testMode = value;
                                      break;
                                  case MerchantSwitchTypeOpen:
                                      CRED.merchant.open = value;
                                      CRED.merchant.lastOperationDate = [response dateForKey:@"last_modified_open_close_at"];
                                      break;
                                  case MerchantSwitchTypePublish:
                                      CRED.merchant.published = value;
                                      break;
                              }
                              
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while changing open status", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  
//                  self.openSwitch.on = CRED.merchant.isOpenHour;
                  [SVProgressHUD dismiss];
              }];
    
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)tableButtonSwiped:(TableButton *)sender
{
    NSInteger index = sender.tag;
    
    TableInfo *tableInfo = [TABLE.tableOnlyList objectAtIndex:index];
    if(tableInfo.status == TableStatusCompleted){
        Order *updateOrder = [tableInfo.order copy];
        updateOrder.bfStatus = updateOrder.status;
        updateOrder.status = OrderStatusFixed;
        
        [self requestUpdateWithOrder:updateOrder withTableInfo:tableInfo];
    }
}

- (void)requestUpdateWithOrder:(Order *)updateOrder withTableInfo:(TableInfo *)tableInfo
{
    RequestResult result = RRFail;
    result = [MERCHANT requestUpdateOrder:updateOrder
                          completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              [tableInfo.order updateWithDictionary:response.data
                                                           merchant:nil
                                                            menuPan:MENUPAN.wholeSerialMenuList];
                              
                              if(tableInfo.order.status == OrderStatusFixed ||
                                 tableInfo.order.status == OrderStatusCanceled ||
                                 tableInfo.order.status == OrderStatusDeclined){
                                  
                                  TableStatus tempStatus = tableInfo.status;
                                  tableInfo.order = nil;
                                  tableInfo.billRequest = nil;
                                  tableInfo.bfStatus = tempStatus;
                                  
                              }
                              
                              NSDictionary *userInfo = nil;
                              
                              userInfo = [NSDictionary dictionaryWithObject:tableInfo forKey:@"tableInfo"];
                              
                              [[NSNotificationCenter defaultCenter] postNotificationName:TableInfoChangedNotification
                                                                                  object:self
                                                                                userInfo:userInfo];
                              
                          }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while updating this order", nil)];
                                  
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}


- (void)tableButtonTouched:(TableButton *)sender
{
    NSInteger index = sender.tag;
    
    TableInfo *tableInfo = [TABLE.tableOnlyList objectAtIndex:index];
    if(![tableInfo isKindOfClass:[TableInfo class]]){
        PCWarning(@"TableInfo is not the Table Info %@", tableInfo);
        return;
    }
    
    if(tableInfo.status == TableStatusEmpty){
        PCLog(@"Table Empty");
        return;
    }
    
    OrderDetailViewController *viewController = [OrderDetailViewController viewControllerFromNib];
    viewController.tableInfo = tableInfo;
    
    PCNavigationController *navi = [[PCNavigationController alloc] initWithRootViewController:viewController];
    navi.navigationBarHidden = YES;
    
//    if(self.popover == nil){
        UIPopoverController *popoverController = [[UIPopoverController alloc]
                                                  initWithContentViewController:navi];
        popoverController.popoverBackgroundViewClass = [OrderPopoverBackgroundView class];
        
        self.popover = popoverController;
        self.popover.delegate = self;
//    } else {
//        self.popover.contentViewController = navi;
//    }
    
    viewController.popover = self.popover;
    
    [self.popover presentPopoverFromRect:[self.view convertRect:sender.frame
                                                       fromView:sender.superview]
                                  inView:self.view
                permittedArrowDirections:UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionRight
                                animated:YES];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex){
        case 0: //Merchant information
            [self showMerchantInformationSetting];
            break;
        case 1: //Table labels
            [self showTableLabelSettings];
            break;
    }
}

- (void)showMerchantInformationSetting
{
    MerchantSettingViewController *viewController = [MerchantSettingViewController viewControllerFromNib];
    viewController.modalPresentationStyle = UIModalPresentationFormSheet;
    viewController.merchantNo = CRED.merchant.merchantNo;
    [self presentViewControllerInNavigation:viewController
                       animated:YES
                     completion:^(){

                     }];
}

- (void)showTableLabelSettings
{
    TableLabelSettingViewController *viewController = [TableLabelSettingViewController viewControllerFromNib];
    viewController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^(){
                                     
                                 }];
}

#pragma mark - MerchantSettingMenuViewControllerDelegate
- (BOOL)merchantSettingMenuViewController:(MerchantSettingMenuViewController *)viewController
                            didSelectMenu:(TableItem *)tableItem
{
    if([self respondsToSelector:tableItem.action] ){
        [self performSelector:tableItem.action
                   withObject:viewController];
        return YES;
    } else {
        return NO;
    }
}

- (void)merchantSettingMenuViewControllerWillChangeToLive:(MerchantSettingMenuViewController *)viewController
{
    [self accountSetting:viewController];
}

- (void)merchantInformation:(id)sender
{
    [self.settingPopoverController dismissPopoverAnimated:YES];
    self.settingPopoverController = nil;
    
    MerchantSettingViewController *viewController = [MerchantSettingViewController viewControllerFromNib];
    viewController.modalPresentationStyle = UIModalPresentationFormSheet;
    viewController.merchantNo = CRED.merchant.merchantNo;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^(){
                                 }];
}

- (void)taxSetting:(id)sender
{
    [self.settingPopoverController dismissPopoverAnimated:YES];
    self.settingPopoverController = nil;
    
    TaxSettingViewController *viewController = [TaxSettingViewController viewControllerFromNib];
    viewController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^(){
                                 }];
}

- (void)accountSetting:(id)sender
{
    [self.settingPopoverController dismissPopoverAnimated:YES];
    self.settingPopoverController = nil;
    
    MerchantAccountViewController *viewController = [MerchantAccountViewController viewControllerFromNib];
    viewController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^(){
                                 }];
}

- (void)manageTablelabels:(id)sender
{
    [self.settingPopoverController dismissPopoverAnimated:YES];
    self.settingPopoverController = nil;
    
    TableLabelSettingViewController *viewController = [TableLabelSettingViewController viewControllerFromNib];
    viewController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^(){
                                     
                                 }];
}

- (void)optionViewController:(OptionViewController *)viewController
didChangeOneTouchLocationUpdate:(UISwitch *)aSwitch
{
    [self layoutDirectUpdateButtons];
}

- (void)optionViewController:(OptionViewController *)viewController
   didChangeDirectMenuSwitch:(UISwitch *)aSwitch
{
    [self layoutDirectUpdateButtons];
}

- (void)layoutDirectUpdateButtons
{
    [self layoutDirectUpdateButtonsWithAnimation:NO];
}

- (void)layoutDirectUpdateButtonsWithAnimation:(BOOL)animation
{
    self.locationUpdateButton.hidden = ![NSUserDefaults standardUserDefaults].oneTouchLocationUpdate;
//    self.menuSwitchButton.hidden = ![NSUserDefaults standardUserDefaults].directSwitchMenu;
    self.menuSwitchButton.hidden = YES;
    
    CGFloat startX = self.settingButton.x - 8.0f;
    
    if(!self.locationUpdateButton.hidden){
        self.locationUpdateButton.x = startX - self.locationUpdateButton.width;
        
        startX = self.locationUpdateButton.x - 8.0f;
    }
    
    if(!self.menuSwitchButton.hidden){
        self.menuSwitchButton.x = startX - self.menuSwitchButton.width;
    }
}

- (IBAction)locationUpdateButtonTouched:(id)sender
{
    MerchantLocationViewController *viewController = [MerchantLocationViewController viewControllerFromNib];
    viewController.modalPresentationStyle = UIModalPresentationFormSheet;
    viewController.initialLocation = CRED.merchant.location;
    viewController.delegate = self;
    
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^(){
                                 }];
}

- (IBAction)switchMenuButtonTouched:(id)sender
{
    [self.settingPopoverController dismissPopoverAnimated:NO];
    
    SwitchMenuViewController *viewController = [SwitchMenuViewController viewControllerFromNib];
    viewController.title = NSLocalizedString(@"Switch Menu", nil);
    PCNavigationController *navi = [[PCNavigationController alloc] initWithRootViewController:viewController];
    
//    if(self.settingPopoverController == nil){
        self.settingPopoverController = [[UIPopoverController alloc]
                                         initWithContentViewController:navi];
        self.settingPopoverController.delegate = self;
        self.settingPopoverController.popoverBackgroundViewClass = [OrderPopoverBackgroundView class];
//    } else {
//        self.settingPopoverController.contentViewController = navi;
//    }
    
    if([sender isKindOfClass:[UIBarButtonItem class]]){
        [self.settingPopoverController presentPopoverFromBarButtonItem:(UIBarButtonItem *)sender
                                              permittedArrowDirections:UIPopoverArrowDirectionAny
                                                              animated:YES];
    } else {
        [self.settingPopoverController presentPopoverFromRect:((UIButton *)sender).frame
                                                       inView:self.view
                                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                                     animated:YES];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 303:
            if(buttonIndex == AlertButtonIndexYES){
                [self requestMerchantModeChangeWithType:MerchantSwitchTypeOpen value:NO];
            } else {
//                self.openSwitch.on = YES;
            }
            break;
        default:
            break;
    }
}


- (void)mapViewController:(MerchantLocationViewController *)mapViewController
      didSelectPlacemarks:(NSArray *)placemarks
            pinAnnotation:(PinAnnotation *)pinAnnotation
{
    if([placemarks count] > 0){
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:pinAnnotation.coordinate.latitude
                                                          longitude:pinAnnotation.coordinate.longitude];
        
        RequestResult result = RRFail;
        result = [MERCHANT requestMerchant:CRED.merchant
                                  location:location
                                geoAddress:placemark.address
                           completionBlock:
                  ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                      
                      if(isSuccess && 200 <= statusCode && statusCode < 300){
                          switch(response.errorCode){
                              case ResponseSuccess:
                                  
                                  CRED.merchant.location = location;
                                  
                                  break;
                              default:{
                                  NSString *message = [response objectForKey:@"message"];
                                  if([message isKindOfClass:[NSString class]]){
                                      [UIAlertView alert:message];
                                  } else {
                                      [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while setting location", nil)];
                                  }
                              }
                                  break;
                          }
                      }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                          [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                          //Signed out in requestMyCreditWithCompletionBlock
                      } else {
                          if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                              [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                          } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                              [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                                  ,statusCode]];
                          }
                      }
                      [SVProgressHUD dismiss];
                  }];
        switch(result){
            case RRSuccess:
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                break;
            case RRParameterError:
                [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                break;
            default:
                break;
        }
        
    } else {
    }
}

@end
