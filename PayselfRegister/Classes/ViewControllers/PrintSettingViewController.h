//
//  PrintSettingViewController.h
//  RushOrder
//
//  Created by Conan on 9/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCiPadViewController.h"
#import "BXPrinterController.h"
#import "TableSectionItem.h"

@protocol PrintSettingViewControllerDelegate;

@interface PrintSettingViewController : PCiPadViewController
<
UITableViewDelegate,
UITableViewDataSource,
BXPrinterControlDelegate
>

@property (weak, nonatomic) id<PrintSettingViewControllerDelegate> delegate;

@end

@protocol PrintSettingViewControllerDelegate<NSObject>
- (void)printSettingViewController:(PrintSettingViewController *)viewController
                            didSelectMenu:(TableItem *)tableItem;
@end;
