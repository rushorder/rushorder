//
//  RejectViewController.m
//  RushOrder
//
//  Created by Conan on 11/15/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "RejectViewController.h"
#import "NPKeyboardAwareScrollView.h"

@interface RejectViewController ()

@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@property (strong, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NPToggleButton *allowAdjustmentButton;
@end

@implementation RejectViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Decline", nil);
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonSystemItem:UIBarButtonSystemItemCancel
                                                                          target:self
                                                                          action:@selector(cancelButtonTouched:)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneButtonTouched:(id)sender
{
    if([self.delegate respondsToSelector:@selector(rejectViewController:allowCustomerAdjustment:rejectWithMessage:)]){
        [self.delegate rejectViewController:self
                    allowCustomerAdjustment:self.allowAdjustmentButton.selected
                          rejectWithMessage:self.messageTextView.text];
    }
}

- (IBAction)cancelButtonTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
