//
//  JoinedMerchantViewController.m
//  RushOrder
//
//  Created by Conan on 10/7/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "JoinedMerchantViewController.h"
#import "PCMerchantService.h"
#import "TableSectionItem.h"
#import "PCMenuService.h"
#import "MenuManager.h"
#import "StaffJoinViewController.h"

@interface StaffJoinRequest : NSObject

@property (nonatomic) PCSerial requestNo;
@property (nonatomic) StaffJoinRequestStatus status;
@property (nonatomic, copy) NSDate *requestDate;
@property (nonatomic, copy) NSDate *updateDate;
@property (nonatomic, strong) Merchant *merchant;
@property (readonly) NSString *statusString;

- (id)initWithDictionary:(NSDictionary *)dictionary;

@end

@implementation StaffJoinRequest

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if(self != nil){
        self.requestNo = [dictionary serialForKey:@"id"];
        self.status = [[dictionary objectForKey:@"status"] staffJoinRequestStatusEnum];
        self.requestDate = [dictionary dateForKey:@"created_at"];
        self.updateDate = [dictionary dateForKey:@"updated_at"];
        
        self.merchant = [[Merchant alloc] initWithDictionary:[dictionary objectForKey:@"merchant"]];
                          
        
    }
    return self;
}

- (NSString *)statusString;
{
    switch(self.status){
        case StaffJoinRequestStatusRejected:
            return NSLocalizedString(@"Rejected", nil);
            break;
        case StaffJoinRequestStatusConfirmed:
            return NSLocalizedString(@"Accepted", nil);
            break;
        case StaffJoinRequestStatusRequested:
            return NSLocalizedString(@"Requested", nil);
            break;
        default:
            return nil;
    }
    return nil;
}


@end

@implementation NSString (StaffJoinRequest)

- (StaffJoinRequestStatus)staffJoinRequestStatusEnum
{
    if([self isEqualToString:@"requested"]){
        return StaffJoinRequestStatusRequested;
    } else if([self isEqualToString:@"confirmed"]){
        return StaffJoinRequestStatusConfirmed;
    } else if([self isEqualToString:@"rejected"]){
        return StaffJoinRequestStatusRejected;
    } else {
        return StaffJoinRequestStatusUnknown;
    }
}

@end

@interface JoinedMerchantViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *sectionList;
@property (nonatomic, strong) Merchant *selectedMerchant;
@end

@implementation JoinedMerchantViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if([CRED.merchants count] > 1){
        if(CRED.merchant != nil){
            self.title = NSLocalizedString(@"Switch", nil);
        } else {
            self.title = NSLocalizedString(@"Select", nil);
        }
    } else {
        self.title = NSLocalizedString(@"Status", nil);
    }
    
    if(self.navigationController.rootViewController == self){
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonSystemItem:UIBarButtonSystemItemAction
                                                                              target:self
                                                                              action:@selector(functionButtonTouched:)];
        if(CRED.userInfo.role == UserRoleStaff){
            self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                   target:self
                                                                                   action:@selector(newStaffJoinViewController:)];
        }
    }
    
    [self getRequestedList];
}

- (void)functionButtonTouched:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"Sign Out", nil)
                                                    otherButtonTitles:nil];
    actionSheet.tag = 1;
    [actionSheet showFromBarButtonItem:sender
                              animated:YES];
}

- (void)newStaffJoinViewController:(id)sender
{
    StaffJoinViewController *viewController = [StaffJoinViewController viewControllerFromNib];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getRequestedList
{
    RequestResult result = RRFail;
    result = [MERCHANT requestRequestedListWithCompletionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              NSArray *joinRequestList = [response objectForKey:@"join_requests"];
                              NSArray *joinedMerchant = [response objectForKey:@"joined_merchants"];
                           
                              NSMutableArray *merchantList = [NSMutableArray array];
                              for(NSDictionary *joinedMerchantDict in joinedMerchant){
                                  Merchant *merchant = [[Merchant alloc] initWithDictionary:joinedMerchantDict];
                                  [merchantList addObject:merchant];
                              }
                              
                              CRED.merchants = merchantList;
                              
                              NSMutableArray *requestList = [NSMutableArray array];
                              for(NSDictionary *joinRequestDict in joinRequestList){
                                  BOOL shouldBeAdded = YES;
                                  
                                  StaffJoinRequest *request = [[StaffJoinRequest alloc] initWithDictionary:joinRequestDict];
                                  
                                  for(Merchant *aMerchant in merchantList){
                                      if(request.merchant.merchantNo == aMerchant.merchantNo){
                                          shouldBeAdded = NO;
                                          break;
                                      }
                                  }
                                  
                                  if(shouldBeAdded && request.status == StaffJoinRequestStatusRejected){
//                                      PCSerial maxRequestNo = request.requestNo;
                                      for(NSDictionary *aRequestDict in joinRequestList){
                                          PCSerial merchantNo = [[aRequestDict objectForKey:@"merchant"] serialForKey:@"id"];
                                          PCSerial requestNo = [aRequestDict serialForKey:@"id"];
                                          if(request.merchant.merchantNo == merchantNo){
                                              if(request.requestNo < requestNo){
                                                  shouldBeAdded = NO;
                                                  break;
                                              }
                                          }
                                      }
                                      
                                      
                                  }
                                  
                                  if(shouldBeAdded){
                                      [requestList addObject:request];
                                  }
                                  
                              }
                              
                              [self configureSection:requestList];
                              [self.tableView reloadData];
                              
                          }
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting list of merchants I requested to", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)configureSection:(NSMutableArray *)requestedList
{
    self.sectionList = nil;
    
    TableSection *section = [[TableSection alloc] init];
    section.title = NSLocalizedString(@"Requesting to Join", nil);
    section.menus = requestedList;
    section.tag = 0;
    [self.sectionList addObject:section];

    section = [[TableSection alloc] init];
    section.title = NSLocalizedString(@"Participating Merchants", nil);
    section.menus = CRED.merchants;
    section.tag = 1;
    [self.sectionList addObject:section];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sectionList count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    TableSection *tableSection = [self.sectionList objectAtIndex:section];
    return tableSection.title;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    TableSection *tableSection = [self.sectionList objectAtIndex:section];
    return [tableSection.menus count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                      reuseIdentifier:identifier];
        
        
    }
    
    TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
    id item = [tableSection.menus objectAtIndex:indexPath.row];
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    if([item isKindOfClass:[Merchant class]]){
        Merchant *castedMerchant = (Merchant *)item;
        cell.textLabel.text = castedMerchant.name;
        if(castedMerchant.merchantNo == CRED.merchant.merchantNo){
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    } else {
        StaffJoinRequest *castedRequest = (StaffJoinRequest *)item;
        cell.textLabel.text = castedRequest.merchant.name;
        cell.detailTextLabel.text = castedRequest.statusString;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
    id item = [tableSection.menus objectAtIndex:indexPath.row];
    
    if([item isKindOfClass:[Merchant class]]){
        Merchant *castedMerchant = (Merchant *)item;
        if(CRED.merchant.merchantNo != castedMerchant.merchantNo){
            self.selectedMerchant = castedMerchant;
            
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Do you want to switch(sign-in) to %@?", nil),
                                                                               castedMerchant.name]
                                                                     delegate:self
                                                            cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                       destructiveButtonTitle:NSLocalizedString(@"OK", nil)
                                                            otherButtonTitles:nil];
            actionSheet.tag = 2;
            [actionSheet showInView:self.view];
        }
    } else {

    }
    [self.tableView deselectRowAtIndexPath:indexPath
                                  animated:YES];
}

- (NSMutableArray *)sectionList
{
    if(_sectionList == nil){
        _sectionList = [NSMutableArray array];
    }
    return _sectionList;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(actionSheet.tag){
        case 1:
            if(actionSheet.destructiveButtonIndex == buttonIndex){ //Sign out
                [self signOut];
            }
            break;
        case 2:
            if(buttonIndex == actionSheet.destructiveButtonIndex){
                [self switchMerchant];
            }
            break;
        defatul:
            break;
    }
}

- (void)switchMerchant
{
    if(self.selectedMerchant == nil){
        PCError(@"Selected merchant should not be nil while switching merchant");
        return;
    }
    
    RequestResult result = RRFail;
    result = [MERCHANT requestSelectMerchant:self.selectedMerchant.merchantNo
                             completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              CRED.merchant = self.selectedMerchant;
                              [[NSUserDefaults standardUserDefaults] setLastLogedInMerchantId:CRED.merchant.merchantNo
                                                                                       withId:CRED.userInfo.email];
                              if(self.selectedMerchant.isMenuOrder){
                                  [MENUPAN resetGraphWithMerchant:self.selectedMerchant];
                                  
                                  if(MENUPAN.isMenuFetched){
                                      if([MENUPAN.activeMenuList count] == 0){
                                          [UIAlertView askWithTitle:NSLocalizedString(@"Please Fill Out Your Menu",nil)
                                                            message:NSLocalizedString(@"Your restaurant is set to accept Order by customer. Ordering needs menu. Do you want to make menu now?", nil)
                                                           delegate:self
                                                                tag:101];
                                      }
                                  } else {
                                      [MENUPAN requestMenus];
                                  }
                              } else {
                                  [MENUPAN resetGraphWithMerchant:nil];
                              }
                              
                              [self.tableView reloadData];
                              
                              [[NSNotificationCenter defaultCenter] postNotificationName:SignedInNotification
                                                                                  object:self
                                                                                userInfo:nil];
                              
                              if(self.isRootViewController){
                                  [self dismissViewControllerAnimated:YES
                                                           completion:^{
                                                               if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                                                                   if(CRED.merchant.isTableBase){
                                                                       [APP transitDashboard];
                                                                   } else {
                                                                       [APP transitFastFood];
                                                                   }
                                                               } else {
                                                                   [APP transitTableStatus];
                                                               }
                                                           }];
                              }
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while switching merchant", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)signOut
{    
    RequestResult result = RRFail;
    result = [CRED requestSignOutCompletionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              [self dismissViewControllerAnimated:YES
                                                       completion:^{
                                                           [self uiLogoutProcess];
                                                       }];
                          }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while signing out", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];                      
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)uiLogoutProcess
{
    [APP transitLogin];
    [CRED.userAccountItem resetKeychainItem];
    [NSUserDefaults standardUserDefaults].autoLogin = NO;
}

@end