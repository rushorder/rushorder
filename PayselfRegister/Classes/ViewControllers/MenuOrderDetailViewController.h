//
//  MenuOrderDetailViewController.h
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "MenuItem.h"
#import "LineItem.h"
#import "Cart.h"

@protocol MenuOrderDetailViewControllerDelegate;

@interface MenuOrderDetailViewController : PCViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) id<MenuOrderDetailViewControllerDelegate> delegate;
@property (strong, nonatomic) MenuItem *item;
@property (strong, nonatomic) LineItem *lineItem;
@property (strong, nonatomic) Cart *cart;

@property (nonatomic, readonly) BOOL isModifyingMode;
@property (nonatomic, readonly) BOOL canEdit;
@end

@protocol MenuOrderDetailViewControllerDelegate <NSObject>
@optional
- (void)menuOrderDetailViewController:(MenuOrderDetailViewController *)viewController didNewMenuItem:(MenuItem *)menuItem quantity:(NSInteger)quantity;
- (void)menuOrderDetailViewController:(MenuOrderDetailViewController *)viewController didUpdateLineItem:(LineItem *)lineItem;
@end
