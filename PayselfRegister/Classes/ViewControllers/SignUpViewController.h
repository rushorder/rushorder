//
//  SignUpViewController.h
//  RushOrder
//
//  Created by Conan on 3/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCiPadViewController.h"

@interface SignUpViewController : PCiPadViewController
<
UIAlertViewDelegate,
UITextFieldDelegate
>

@property (copy, nonatomic) NSString *email;
@end
