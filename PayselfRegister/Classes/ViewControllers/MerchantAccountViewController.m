//
//  MerchantAccountViewController.m
//  RushOrder
//
//  Created by Conan on 8/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MerchantAccountViewController.h"
#import "PCMerchantService.h"
#import "PCMerchantCredentialService.h"
#import "EULAViewController.h"

@interface MerchantAccountViewController ()

@property (weak, nonatomic) IBOutlet PCTextField *einTextField;
@property (weak, nonatomic) IBOutlet PCTextField *ssnTextField;
@property (weak, nonatomic) IBOutlet PCTextField *accOwnerTextField;
@property (weak, nonatomic) IBOutlet PCTextField *routingNumberTextField;
@property (weak, nonatomic) IBOutlet PCTextField *accNumberTextField;
@property (weak, nonatomic) IBOutlet NPStretchableButton *cancelButton;
@property (strong, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@property (weak, nonatomic) IBOutlet PCTextField *bankNameTextField;
@end

@implementation MerchantAccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Activating", nil);
    
    [self.scrollView setContentSizeWithBottomView:self.cancelButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)validateInputWithString:(NSString *)aString
{
    NSString * const regularExpression = @"^[1-9]\\d?-\\d{7}$";
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regularExpression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    if (error) {
        NSLog(@"error %@", error);
    }
    
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:aString
                                                        options:0
                                                          range:NSMakeRange(0, [aString length])];
    return numberOfMatches > 0;
}

- (BOOL)validateSSNInputWithString:(NSString *)aString
{
    NSString * const regularExpression = @"^(?!(000|666|9))\\d{3}-(?!00)\\d{2}-(?!0000)\\d{4}$";
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regularExpression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    if (error) {
        NSLog(@"error %@", error);
    }
    
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:aString
                                                        options:0
                                                          range:NSMakeRange(0, [aString length])];
    return numberOfMatches > 0;
}

- (IBAction)doneButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    if(![VALID validate:self.accOwnerTextField
                message:NSLocalizedString(@"Account owner is required", nil)]){
        return;
    }
    
    if([self.einTextField.text length] == 0
       && [self.ssnTextField.text length] == 0){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Required Field", nil)
                            message:NSLocalizedString(@"EIN(Tax ID) or SSN is required", nil)];
        return;
    }
    
    if([self.einTextField.text length] > 0){
        if(![self validateInputWithString:self.einTextField.text]){
            [UIAlertView alertWithTitle:NSLocalizedString(@"Invalid Format", nil)
                                message:NSLocalizedString(@"EIN(Tax ID) must be formated\n12-1234567", nil)
                               delegate:self
                                    tag:101];
            return;
        }
    }
    
    if([self.ssnTextField.text length] > 0){
        if(![self validateSSNInputWithString:self.ssnTextField.text]){
            [UIAlertView alertWithTitle:NSLocalizedString(@"Invalid Format", nil)
                                message:NSLocalizedString(@"SSN must be formated\n123-45-6789", nil)
                               delegate:self
                                    tag:102];
            return;
        }
    }
   
    if(![VALID validate:self.accNumberTextField
                message:NSLocalizedString(@"Account number is required", nil)]){
        return;
    }
    
    if(![VALID validate:self.routingNumberTextField
                message:NSLocalizedString(@"Bank routing number is required", nil)]){
        return;
    }
    
    RequestResult result = RRFail;
    result = [MERCHANT requestMerchant:CRED.merchant
                              bankName:self.bankNameTextField.text
                          accOwnerName:self.accOwnerTextField.text
                             accNumber:self.accNumberTextField.text
                         routingNumber:self.routingNumberTextField.text
                                   ein:self.einTextField.text
                                   ssn:self.ssnTextField.text
                       completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              CRED.merchant.authenticated = [response boolForKey:@"authenticated"];
                              
                              [self requestMerchantTestModeChangeWithValue:NO]; //No is live , YES is test
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while setting merchant account information", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)requestMerchantTestModeChangeWithValue:(BOOL)value
{
    RequestResult result = RRFail;
    result = [MERCHANT requestMerchant:CRED.merchant
                                  type:MerchantSwitchTypeTest
                                 value:value
                       completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              CRED.merchant.testMode = value;
                              
                              if(self.presentingViewController != nil){
                                  [self dismissViewControllerAnimated:YES
                                                           completion:^{
                                                               
                                                           }];
                              } else {
                                  [self.navigationController popViewControllerAnimated:YES];
                              }
                              break;
                          case ResponseErrorUnauthorized:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"You're not activated", nil)
                                                  message:NSLocalizedString(@"You have to activate your account before turning on real mode.", nil)
                                                 delegate:self
                                                      tag:302];
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while changing running mode", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}


- (IBAction)cancelButtonTouched:(id)sender
{
    if(self.presentingViewController != nil){
        [self dismissViewControllerAnimated:YES
                                 completion:^{
                                     
                                 }];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)userAgreementButtonTouched:(id)sender
{
    EULAViewController *viewController = [EULAViewController viewControllerFromNib];
    viewController.title = NSLocalizedString(@"User Agreement", nil);
    ((EULAViewController *) viewController).urlString = @"http://rushorderapp.com/legal/merchant_ua/";
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 101:
            [self.einTextField becomeFirstResponder];
            break;
        case 102:
            [self.ssnTextField becomeFirstResponder];
            break;
        case 302:
            // nothing
            break;
        default:
            break;
    }
}

- (void)viewDidUnload {
    [self setEinTextField:nil];
    [self setAccOwnerTextField:nil];
    [self setRoutingNumberTextField:nil];
    [self setAccNumberTextField:nil];
    [super viewDidUnload];
}

@end