//
//  OrderListViewController.m
//  RushOrder
//
//  Created by Conan on 5/14/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "OrderListViewController.h"
#import "MenuOrderManager.h"
#import "MenuOrderCell.h"
#import "LineItem.h"
#import "MenuItem.h"
#import "Merchant.h"
#import "TableSectionItem.h"
#import "PCOrderService.h"
#import "MenuManager.h"
#import "BixPrintManager.h"
#import "TableStatusManager.h"

enum{
    SubmitOrderTag = 10,
    CanelSubmitTag = 20,
    CheckOutTag = 30,
    OnlyCheckOutTag = 40,
    OrderDoneTag = 50,
    ConfirmOrderTag = 60,
    ErrorButtonTag = 100,
};

@interface OrderListViewController ()
@property (strong, nonatomic) NSMutableArray *orderList;
@property (weak, nonatomic) IBOutlet NPBatchTableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *subTotalAmtLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxAmtLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalAmtLabel;
@property (strong, nonatomic) IBOutlet UIView *bottomView;
@property (strong, nonatomic) IBOutlet UIView *noItemView;
@property (weak, nonatomic) IBOutlet UIButton *checkInButton;
@property (weak, nonatomic) IBOutlet UILabel *tableNumberLabel;

@property (weak, nonatomic) IBOutlet NPStretchableButton *doneButton;
@property (strong, nonatomic) LineItem *selectedLineItem;
@property (weak, nonatomic) IBOutlet UIButton *removeButton;
@property (nonatomic, readonly) BOOL inPopover;

@end

@implementation OrderListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.exceptDefalutBackground = YES;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(orderListChanged:)
                                                     name:LineItemChangedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(applicationWillEnterForeground:)
                                                   name:UIApplicationWillEnterForegroundNotification
                                                 object:nil];
    }
    return self;
}

- (void)merchantUpdated:(NSNotification *)notification
{
    if(MENUORDER.cart.isLocal){
        [self reloadData];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.rowHeight = 68.0f;
    self.title = NSLocalizedString(@"Order", nil);
    
    if(!MENUORDER.merchant.isTableBase){
        self.tableView.refreshHeaderView = nil;
    }
    
    
    [self reloadData];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)applicationWillEnterForeground:(NSNotification *)aNoti
{
    [self refresh];
}

- (void)cartItemChanged:(NSNotification *)aNoti
{
    NSDictionary *userInfo = aNoti.userInfo;
    
    PCSerial cartNo = [userInfo serialForKey:@"cart_id"];
    
    if(MENUORDER.cart.cartNo == cartNo){
        [self refresh];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setSubTotalAmtLabel:nil];
    [self setDoneButton:nil];
    [self setBottomView:nil];
    [self setNoItemView:nil];
//    [self setOnlyCheckoutButton:nil];
    [self setCheckInButton:nil];
    [self setTaxAmtLabel:nil];
    [self setTotalAmtLabel:nil];
    [self setTableNumberLabel:nil];
    [self setRemoveButton:nil];
    [super viewDidUnload];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
//    [self.tableView scrollToBottom:YES];
}

#pragma mark - UITableView Delegate & Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return [MENUORDER.cart.lineItemsSection count] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    switch(section){
        case 0:
            return [MENUORDER.order.lineItems count];
            break;
        default:
        {
            TableSection *tableSection = [MENUORDER.cart.lineItemsSection objectAtIndex:(section - 1)];
            return [tableSection.menus count];
        }
            break;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    switch(section){
        case 0:
            if([MENUORDER.order.lineItems count] > 0){
                UIView *headerView = [self makeHeaderViewWithTitle:NSLocalizedString(@"Confirmed Order", nil)
                                                         atSection:section];
                return headerView;
            } else {
                return nil;
            }
            break;
        default:
        {
            if(MENUORDER.merchant.isTableBase){
                TableSection *tableSection = [MENUORDER.cart.lineItemsSection objectAtIndex:(section - 1)];
                UIView *headerView = [self makeHeaderViewWithTitle:tableSection.title
                                                         atSection:section];
                
                return headerView;
            } else {
                return nil;
            }
        }
            break;
    }
}

#define SECTION_HEADER_HEIGHT   30.0f

- (UIView *)makeHeaderViewWithTitle:(NSString *)sectionTitle atSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, SECTION_HEADER_HEIGHT)];
//    sectionHeaderView.color = 
//    sectionHeaderView.alpha = 0.8f;
    
    UIImage *image = [[UIImage imageNamed:@"table_box_cell"] resizableImageFromCenter];
    UIImageView *backImageView = [[UIImageView alloc] initWithImage:image];
    backImageView.frame = sectionHeaderView.bounds;
    backImageView.contentMode = UIViewContentModeScaleToFill;
    
    [sectionHeaderView addSubview:backImageView];
    
    if(section > 0){
        UIImageView *lineImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dot_line_220"]];
        lineImageView.frame = CGRectMake(58.0f, 1.0f, 242.0f, 5.0f);
        [sectionHeaderView addSubview:lineImageView];
    }
    
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(55.0f, 5.0f, 235.0f, SECTION_HEADER_HEIGHT - 5.0f)];
    titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    titleLabel.textColor = [UIColor darkGrayColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = sectionTitle;
    
    [sectionHeaderView addSubview:titleLabel];
    
    return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch(section){
        case 0:
            if([MENUORDER.order.lineItems count] > 0){
                return SECTION_HEADER_HEIGHT;
            } else {
                return 0.0f;
            }
            break;
        default:
        {
            if(MENUORDER.merchant.isTableBase){
                return SECTION_HEADER_HEIGHT;
            } else {
                return 0.0f;
            }
        }
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
//    NSArray *sectionList = [MENUORDER.orderList objectAtIndex:indexPath.section];
//    LineItem *lineItem = [sectionList objectAtIndex:indexPath.row];
    
    LineItem *lineItem = nil;

    NSString *orderComment = nil;
    switch(indexPath.section){
        case 0:
            lineItem = [MENUORDER.order.lineItems objectAtIndex:indexPath.row];
            lineItem.status = LineItemStatusFixed;
            break;
        default:{
            
            TableSection *tableSection = [MENUORDER.cart.lineItemsSection objectAtIndex:(indexPath.section - 1)];
            orderComment = tableSection.title;
            lineItem = [tableSection.menus objectAtIndex:indexPath.row];
            
            switch(MENUORDER.cart.status){
                case CartStatusOrdering:
                    lineItem.status = LineItemStatusOrdering;
                    break;
                case CartStatusSubmitted:
                    lineItem.status = LineItemStatusSubmitted;
                    break;
                case CartStatusFixed:
                default:
                    lineItem.status = LineItemStatusFixed;
                    break;
            }
            break;
        }
    }
    
    NSString *cellIdentifier = @"MenuOrderCell";

    MenuOrderCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        cell = [MenuOrderCell cell];
    }
    
    cell.lineItem = lineItem;
    [cell fillContentsWithOrderComment:orderComment];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LineItem *lineItem = nil;
    
    switch(indexPath.section){
        case 0:
            lineItem = [MENUORDER.order.lineItems objectAtIndex:indexPath.row];
            break;
        default:
        {
            TableSection *tableSection = [MENUORDER.cart.lineItemsSection objectAtIndex:(indexPath.section - 1)];
            lineItem = [tableSection.menus objectAtIndex:indexPath.row];
        }
            break;
    }

    MenuOrderDetailViewController *viewController = [MenuOrderDetailViewController viewControllerFromNib];
    viewController.lineItem = lineItem;
    viewController.delegate = self;
    viewController.cart = MENUORDER.cart;
    
    [self.tableView deselectRowAtIndexPath:indexPath
                                  animated:YES];
    
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:NULL];
}

- (void)cell:(MenuOrderCell *)cell didTouchedRemoveFromCartButtonAtIndexPath:(NSIndexPath *)indexPath
{
    LineItem *lineItem = nil;
    
    switch(indexPath.section){
        case 0:
            lineItem = [MENUORDER.order.lineItems objectAtIndex:indexPath.row];
            break;
        default:{
            TableSection *tableSection = [MENUORDER.cart.lineItemsSection objectAtIndex:(indexPath.section - 1)];
            lineItem = [tableSection.menus objectAtIndex:indexPath.row];
        }
            break;
    }
    
    if(MENUORDER.cart.status == CartStatusOrdering){
        if(lineItem.quantity <= 0){
            UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"%@ will be removed.\nDo you want to proceed?", nil), lineItem.menuName]
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                 destructiveButtonTitle:NSLocalizedString(@"Remove", nil)
                                                      otherButtonTitles:nil];
            self.selectedLineItem = lineItem;
            sheet.tag = 102;
            [sheet showInView:self.view];
        } else {
            [MENUORDER.cart subtractLineItem:lineItem
                                     success:^(BOOL isOnlyLocal){
                                         //                                   if(isOnlyLocal)
                                         //                                       TRANS.localDirty = YES;
                                         //                                   else
                                         //                                       TRANS.dirty = YES;
                                     }
                                     failure:^{
                                         [self refresh];
                                     }];
        }
    } else {
        PCWarning(@"Order list(cart) status is not CartStatusOrdering, Remove operation needs CartStatusOrdering status");
    }
}

#pragma mark - MenuOrderDetailViewControllerDelegate
- (void)menuOrderDetailViewController:(MenuOrderDetailViewController *)viewController didUpdateLineItem:(LineItem *)lineItem;
{
    [MENUORDER.cart updateLineItem:lineItem
                       success:^(BOOL isOnlyLocal){
//                           if(isOnlyLocal)
//                               TRANS.localDirty = YES;
//                           else
//                               TRANS.dirty = YES;
                           [self reloadData];
                       }
                       failure:^{
                           [self refresh];
                       }];
}

- (void)orderListChanged:(NSNotification *)notification
{
    [self.tableView.refreshHeaderView refreshLastUpdatedDate];
    [self reloadData];
}

- (void)reloadData
{
    if([MENUORDER.cart.lineItems count] > 0 || [MENUORDER.order.lineItems count] > 0){
        self.tableView.tableFooterView = self.bottomView;
        self.subTotalAmtLabel.text = MENUORDER.tempSubtotalString;
        self.taxAmtLabel.text = MENUORDER.tempTaxesString;
        self.totalAmtLabel.text = MENUORDER.tempTotalString;
    } else {
        self.tableView.tableFooterView = self.noItemView;
    }
    
    self.doneButton.enabled = YES;
    
    if(!MENUORDER.merchant.isTableBase || MENUORDER.cart.isLocal || MENUORDER.canCheckOut){ //Pickup or conditioned table base
        
        if(MENUORDER.merchant.isAblePayment){
            if(MENUORDER.order.status == OrderStatusCompleted){
                
                self.doneButton.buttonTitle = NSLocalizedString(@"Done", nil);
                self.doneButton.tag = OrderDoneTag;
            } else {
                
                self.doneButton.buttonTitle = NSLocalizedString(@"Check Out", nil);
                self.doneButton.tag = CheckOutTag;
                
                if(MENUORDER.merchant.isTableBase){
                    //Sit-down
                    if(MENUORDER.cart.isLocal){
                        // Sit-down but. order is for pickup
                        self.doneButton.enabled = NO;
                        
                    } else {

                    }
                } else {

                }
            }
        } else {
            // Only Menu ordering mode
            self.doneButton.buttonTitle = NSLocalizedString(@"Done", nil);
            self.doneButton.tag = OrderDoneTag;
            
        }
        
    } else if(MENUORDER.cart.status == CartStatusOrdering){
        
        self.doneButton.buttonTitle = NSLocalizedString(@"Place Order", nil);
        self.doneButton.tag = SubmitOrderTag;
        
    } else if(MENUORDER.cart.status == CartStatusSubmitted){
        
        self.doneButton.buttonTitle = NSLocalizedString(@"Place Order", nil);
        self.doneButton.tag = ConfirmOrderTag;
        
    } else {
        
        self.doneButton.buttonTitle = NSLocalizedString(@"Error", nil);
        self.doneButton.tag = ErrorButtonTag;
    }
    
    if(self.noItemView == self.tableView.tableFooterView){
        
    } else {
        
        UIView *bottomView = self.tableView.tableFooterView;
        bottomView.height = CGRectGetMaxY(self.doneButton.frame) + 25.0f;
        
        self.tableView.tableFooterView = bottomView;
    }
    
    self.checkInButton.hidden = YES;
    self.removeButton.hidden = YES;
    if(MENUORDER.merchant.isTableBase && MENUORDER.tableInfo == nil && !MENUORDER.cart.isLocal){
        self.checkInButton.hidden = NO;
    } else if(MENUORDER.order == nil && MENUORDER.cart.isLocal){
        self.removeButton.hidden = NO;
    }
    
    self.tableNumberLabel.hidden = !self.checkInButton.hidden;
    
    if(MENUORDER.tableInfo != nil){
        self.tableNumberLabel.text = [NSString stringWithFormat:@"Table No. %@", MENUORDER.tableInfo.tableNumber];
        self.tableNumberLabel.hidden = NO;
    } else {
        self.tableNumberLabel.hidden = YES;
    }
    
    [self.tableView reloadData];
}

- (IBAction)doneButtonTouched:(NPStretchableButton *)sender
{
    switch(sender.tag){
        case SubmitOrderTag:
            [self submitOrder];
            break;
        case ConfirmOrderTag:
        {
            if(MENUORDER.order == nil){
                [self confirm];
            } else {
                [self confirmAdd];
            }
        }
            break;
        case CanelSubmitTag:
            [self cancelSubmit];
            break;
        case CheckOutTag:
            [self checkOut];
            break;
        case OnlyCheckOutTag:
            [self onlyCheckOutTag];
            break;
        case OrderDoneTag:
            [self orderDoneButtonTouched];
            break;
        case ErrorButtonTag:
            [UIAlertView alertWithTitle:NSLocalizedString(@"Unknown error", nil)
                                message:NSLocalizedString(@"Sorry, there must be something wrong. Ask to help to server and retry after restarting RushOrder application.", nil)];
            break;
        default:
            break;
    }
}

- (void)cancelSubmit
{
    [MENUORDER.cart updateOrderingWithSuccess:^(BOOL isOnlyLocal){
//                         if(isOnlyLocal)
//                             TRANS.localDirty = YES;
//                         else
//                             TRANS.dirty = YES;
                     } failure:^(){
                         [self refresh];
                     }];
}

- (void)submitOrder
{
//    [UIAlertView askWithTitle:NSLocalizedString(@"All of orders on this table will be submitted", nil)
//                      message:NSLocalizedString(@"You have to check that the others at your table have finished choosing menus before placing order. Do you want to proceed?", nil)
//                     delegate:self
//                          tag:101];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Place this order?", nil)
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"OK", nil)
                                                    otherButtonTitles:nil];
    actionSheet.tag = 101;
    [actionSheet showInView:self.view];
    
}

- (void)checkOut
{
//    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Payment method", nil)
//                                                             delegate:self
//                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
//                                               destructiveButtonTitle:NSLocalizedString(@"Credit Card", nil)
//                                                    otherButtonTitles:NSLocalizedString(@"Cash", nil), nil];
//    actionSheet.tag = 501;
//    
//    [actionSheet showInView:self.view];
    
    if([self.delegate respondsToSelector:@selector(orderListViewController:didTouchCheckoutButton:)]){
        [self.delegate orderListViewController:self
                        didTouchCheckoutButton:nil];
    }
}

- (void)onlyCheckOutTag
{
    if([self.delegate respondsToSelector:@selector(orderListViewController:didTouchOnlyCheckoutButton:)]){
        [self.delegate orderListViewController:self
                didTouchOnlyCheckoutButton:nil];
    }
}

- (void)orderDoneButtonTouched
{
    if([self.delegate respondsToSelector:@selector(orderListViewController:didTouchOrderDoneButton:)]){
        //Save order
        // Moved to OrderManager.m
//        - (BOOL)checkTable:(TableInfo *)tableInfo
//    assertMine:(BOOL)assertMine
//    success:(void (^)())successBlock
//    failure:(void (^)())failureBlock
        
//        [MENUORDER.merchant save];
//        [MENUORDER.order save];
        [self.delegate orderListViewController:self
                       didTouchOrderDoneButton:nil];
    }
}


- (IBAction)checkInButtonTouched:(id)sender
{
    if([self.delegate respondsToSelector:@selector(orderListViewController:didTouchCheckinButton:)]){
        [self.delegate orderListViewController:self
                     didTouchCheckinButton:sender];
    }
}

- (void)viewDeckController:(IIViewDeckController*)viewDeckController willOpenViewSide:(IIViewDeckSide)viewDeckSide animated:(BOOL)animated
{
    self.view.userInteractionEnabled = YES;
}

- (void)viewDeckController:(IIViewDeckController*)viewDeckController didOpenViewSide:(IIViewDeckSide)viewDeckSide animated:(BOOL)animated
{
    [self.tableView scrollToBottom:YES];
}


- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(actionSheet.tag){
        case 101:
            //Place Order
            if(buttonIndex == actionSheet.destructiveButtonIndex){
                if(MENUORDER.order == nil){
                    [self confirm];
                } else {
                    [self confirmAdd];
                }
            }
            break;
        case 102:
            //remove item
            if(buttonIndex == actionSheet.destructiveButtonIndex){
                [MENUORDER.cart subtractLineItem:self.selectedLineItem
                                         success:^(BOOL isOnlyLocal){
//                                       if(isOnlyLocal)
//                                           TRANS.localDirty = YES;
//                                       else
//                                           TRANS.dirty = YES;
                                         }
                                         failure:^{
                                             [self refresh];
                                         }];
            }
            break;
        case 103:
        {
            if(buttonIndex == actionSheet.destructiveButtonIndex){
                Cart *cart = MENUORDER.cart;
                MENUORDER.cart = nil;
//                TRANS.localDirty = YES;
                [cart delete];
            }
        }
            break;
        case 501: //Check out
        {
            
        }
            break;
    }
}

- (void)confirm
{
    RequestResult result = RRFail;
    result = [ORDER requestConfirmCart:MENUORDER.cart
                            merchantId:CRED.merchant.merchantNo
                       completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              PCLog(@"Success Confirm");
                              
                              MENUORDER.tableInfo.order = [[Order alloc] initWithDictionary:response.data
                                                                              merchant:CRED.merchant
                                                                               menuPan:MENUPAN.wholeSerialMenuList];
                              MENUORDER.cart.status = CartStatusFixed;
                              
                              if([NSUserDefaults standardUserDefaults].autoKitchenPrint){
                                  long bixRtn = 0;
                                  
                                  if(MENUORDER.tableInfo == nil){
                                      // Pickup Order
                                      bixRtn = [BIX printOrder:MENUORDER.order];
                                  } else {
                                      // Table Order
                                      MENUORDER.cart.tableLabel = MENUORDER.tableInfo.tableNumber;
                                      MENUORDER.cart.orderNo = MENUORDER.tableInfo.order.orderNo;
                                      bixRtn = [BIX printCart:MENUORDER.cart];
                                  }
                                  [self treatPrintError:bixRtn];
                              }
                              
                              if([NSUserDefaults standardUserDefaults].autoBillPrint){
                                  long bixRtn = [BIX printBill:[[BillWrapper alloc] initWithOrder:MENUORDER.tableInfo.order]];
                                  [self treatPrintError:bixRtn];
                              }
                              
                              if(self.inPopover){
                                  [self dismissPopover:self];
                              } else {
                                  [self.navigationController popViewControllerAnimated:YES];
                              }
                              
                              //                              [self reloadData];
                              
                              [TABLE reloadTableStatus];
                              
                              if([self.delegate respondsToSelector:@selector(orderListViewController:didTouchOrderDoneButton:)]){
                                  [self.delegate orderListViewController:self
                                                 didTouchOrderDoneButton:nil];
                              }
                              
                              break;
                          case ResponseErrorCartIsNotSubmitted:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Customer is editing Order",nil)
                                                  message:NSLocalizedString(@"Wait for customer to place order.",nil)];
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while confirming orders.", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)confirmAdd
{
    RequestResult result = RRFail;
    result = [ORDER requestConfirmAddCart:MENUORDER.cart
                                  toOrder:MENUORDER.order
                               merchantId:CRED.merchant.merchantNo
                          completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              PCLog(@"Success Confirm add");
                              
                              MENUORDER.tableInfo.order = [[Order alloc] initWithDictionary:response.data
                                                                              merchant:CRED.merchant
                                                                               menuPan:MENUPAN.wholeSerialMenuList];
                              
                              MENUORDER.tableInfo.cart.status = CartStatusFixed;
                              
                              
                              if([NSUserDefaults standardUserDefaults].autoKitchenPrint){
                                  long bixRtn = 0;
                                  
                                  // Confirm Add do not include Pickup order case - Only available in Table Order
                                  MENUORDER.tableInfo.cart.tableLabel = MENUORDER.tableInfo.tableNumber;
                                  MENUORDER.tableInfo.cart.orderNo = MENUORDER.tableInfo.order.orderNo;
                                  bixRtn = [BIX printCart:MENUORDER.tableInfo.cart];
                                  
                                  [self treatPrintError:bixRtn];
                              }
                              
                              if([NSUserDefaults standardUserDefaults].autoBillPrint){
                                  long bixRtn = [BIX printBill:[[BillWrapper alloc] initWithOrder:MENUORDER.tableInfo.order]];
                                  [self treatPrintError:bixRtn];
                              }
                              
                              
                              if(self.inPopover){
                                  [self dismissPopover:self];
                              } else {
                                  [self.navigationController popViewControllerAnimated:YES];
                              }
                              
                              [TABLE reloadTableStatus];
                              
                              if([self.delegate respondsToSelector:@selector(orderListViewController:didTouchOrderDoneButton:)]){
                                  [self.delegate orderListViewController:self
                                                 didTouchOrderDoneButton:nil];
                              }
                              
                              break;
                          case ResponseErrorCartIsNotSubmitted:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Customer is editing Order",nil)
                                                  message:NSLocalizedString(@"Wait for customer to place order.",nil)];
                              

                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while confirming additional orders.", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }    
}

- (IBAction)removeButtonTouched:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Your order will be removed. Do you want to proceed?", nil)
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"OK", nil)
                                                    otherButtonTitles:nil];
    actionSheet.tag = 103;
    [actionSheet showInView:self.view];
}

#pragma mark - Pull to refresh
- (BOOL)refresh
{
    if(MENUORDER.merchant.isTableBase){
        return [MENUORDER checkTable:MENUORDER.tableInfo
                         success:^{
                             [self.tableView.refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
                         }
                         failure:^{
                             [self.tableView.refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
                         }];
    } else {
        [self.tableView.refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
        return NO;
    }
}

- (void)treatPrintError:(long)errorCode
{
    switch(errorCode){
        case RUSHORDER_BXL_CONNECT_ERROR:
            [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot connect to printer", nil)
                                message:NSLocalizedString(@"Check your printer connected properly.", nil)
                               delegate:self
                                    tag:105];
            return;
        case RUSHORDER_BXL_PRINTER_NOT_FOUND:
            [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot find any printers connected", nil)
                                message:NSLocalizedString(@"Be sure the printer connected properly to the local network.", nil) // If you don't have printer, turn off Auto Printing at \"Settings\"
                               delegate:self
                                    tag:105];
            return;
        case BXL_BC_DATA_ERROR:
            [UIAlertView alertWithTitle:NSLocalizedString(@"There is no data for printing", nil)
                                message:NSLocalizedString(@"Each kind of printing needs appropriate data.", nil) // If you don't have printer, turn off Auto Printing at \"Settings\"
                               delegate:self
                                    tag:105];
            return;
        default:
            return;
    }
}

- (NSDate *)lastUpdatedDate
{
    return MENUORDER.lastUpdatedDate;
}

- (BOOL)inNewLoading
{
    return MENUORDER.isLoading;
}

- (BOOL)isThereRefreshableData
{
    if(MENUORDER.merchant.isTableBase){
        return YES;
    } else {
        return NO;
    }
}

- (void)dismissPopover:(id)sender
{
    [self.popover dismissPopoverAnimated:YES];
}

- (BOOL)inPopover
{
//    return self.navigationController.parentViewController == nil;
    return NO;
}
@end
