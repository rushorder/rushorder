//
//  LocationViewController.m
//  RushOrder
//
//  Created by Conan on 8/13/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import "LocationViewController.h"
#import <MapKit/MapKit.h>
#import <AddressBook/AddressBook.h>
#import "LocalSearchTableViewCell.h"
#import <AddressBookUI/AddressBookUI.h>
#import "Addresses.h"
#import "GMPrediction.h"


#define SECTION_HEADER_HEIGHT   22.0f

@interface LocationViewController ()

@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *addressListView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarTopConstraint;

@property (strong, nonatomic) NSMutableArray *localSearchResult;
@property (strong, nonatomic) NSMutableArray *addresses;
@property (strong, nonatomic) NSMutableArray *searchedAddresses;
@property (strong, nonatomic) IBOutlet UIButton *myLocationButton;

@property (strong, nonatomic) CLGeocoder *geoCoder;

@property (strong, nonatomic) id selectedObj;
@end

@implementation LocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self){
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(locationUpdated:)
                                                 name:locationUpdatedNotificationKey
                                               object:nil];
}

- (void)locationUpdated:(NSNotification *)noti
{
    CLLocation *location = [noti.userInfo objectForKey:@"location"];
    [self.mapView setCenterCoordinate:location.coordinate animated:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addresses];
    
//    self.searchDisplayController.displaysSearchBarInNavigationBar = YES;
    
    if([self.selectedAddress.title length] > 0)
        self.searchDisplayController.searchBar.placeholder = self.selectedAddress.title;
    
    [self.mapView addAnnotation:self.selectedAddress];
    
    if(self.selectedObj == nil && [self.selectedAddress.title length] == 0){
        [self setLocationWithCoords:self.selectedAddress.coordinate];
    }
    
    MKCoordinateSpan span = {0.01, 0.01};
    MKCoordinateRegion region = {self.selectedAddress.coordinate, span};
    
    [self.mapView setRegion:region];
    
//    for(AddressTableViewController *viewController in self.childViewControllers){
//        if([viewController isKindOfClass:[AddressTableViewController class]]){
//            viewController.delegate = self;
//        }
//    }
    
    CGRect frame = self.mapView.frame;
    self.tableView.tableFooterView = nil;
    
    CGFloat availableHeight = self.tableView.frame.size.height - CGRectGetMaxY(self.searchDisplayController.searchBar.frame) - 64.0f;
    frame.size.height = availableHeight;
    self.mapView.frame = frame;

    [self.mapView addSubview:self.myLocationButton];
    
    [self.mapView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[myLocationButton(38)]"
                                                                         options:0
                                                                         metrics:nil
                                                                           views:@{@"myLocationButton":self.myLocationButton}]];
    [self.mapView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[myLocationButton(38)]"
                                                                         options:0
                                                                         metrics:nil
                                                                           views:@{@"myLocationButton":self.myLocationButton}]];
    [self.mapView updateConstraints];
    
//    CGRect buttonFrame = self.myLocationButton.frame;
//    buttonFrame.origin.x = 10.0f;
//    buttonFrame.origin.y = 10.0f;
//    self.myLocationButton.frame = frame;

    self.tableView.tableFooterView = self.mapView;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.addressTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)applyButtonTouched:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        if([self.delegate respondsToSelector:@selector(locationViewController:didApplyCoordinate:name:placemarkOrAddress:)]){
            [self.delegate locationViewController:self
                               didApplyCoordinate:self.selectedAddress.coordinate
                                             name:self.selectedAddress.title
                               placemarkOrAddress:self.selectedObj];
        }
    }];
}

- (IBAction)cancelButtonTouched:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        if([self.delegate respondsToSelector:@selector(didCancelLocationViewController:)]){
            [self.delegate didCancelLocationViewController:self];
        }
    }];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}

- (IBAction)myPositionButtonTouched:(id)sender
{
    [APP startLocationUpdates];
    
    [self.mapView setCenterCoordinate:self.mapView.userLocation.coordinate
                             animated:YES];
    [self setLocationWithCoords:self.mapView.userLocation.coordinate];
}

- (CLGeocoder *)geoCoder
{
    if(_geoCoder == nil){
        _geoCoder = [[CLGeocoder alloc] init];
    }
    return _geoCoder;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == self.searchDisplayController.searchResultsTableView){
        return 2;
    } else {
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == self.searchDisplayController.searchResultsTableView){

        switch(section){
            case 0:
                return [self.searchedAddresses count];
                break;
            case 1:
                return [self.localSearchResult count];
                break;
            default:
                return 0;
                break;
        }
        
    } else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == self.searchDisplayController.searchResultsTableView){
    switch(section){
            case 0:
                if([self.searchedAddresses count] > 0){
                    return SECTION_HEADER_HEIGHT;
                } else {
                    return 0;
                }
                break;
            case 1:
                if([self.localSearchResult count] > 0){
                    return SECTION_HEADER_HEIGHT;
                } else {
                    return 0;
                }
                break;
            default:
                return 0;
                break;
        }
    } else {
        return 0.0f;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.searchDisplayController.searchResultsTableView){
        return 65.0f;
    }
    return 0.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.searchDisplayController.searchResultsTableView){
        LocalSearchTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"LocalSearchTableViewCell"];
        
        switch(indexPath.section){
            case 0:
            {
                Addresses *address = [self.searchedAddresses objectAtIndex:indexPath.row];
                cell.addressLabel.text = address.address1;
                NSString *addresses = address.address;
                cell.subLabel.text = addresses;
                return cell;
            }
                break;
            case 1:
            {
                id item = [self.localSearchResult objectAtIndex:indexPath.row];
                if([item isKindOfClass:[MKMapItem class]]){
                    MKMapItem *mapItem = (MKMapItem *)item;
                    cell.addressLabel.text = mapItem.name;
                    cell.subLabel.text = mapItem.placemark.address;
                } else if([item isKindOfClass:[GMPrediction class]]){
                    GMPrediction *prediction = (GMPrediction *)item;
                    cell.addressLabel.text = prediction.desc;
                    cell.subLabel.text = nil;
                }
                return cell;
            }
                break;
            default:
                return nil;
                break;
        }
    }
    return nil;
}

- (void)removeAllAnnotationWithoutUserLocation
{
    for(id <MKAnnotation>annotation in self.mapView.annotations){
        if(![annotation isKindOfClass:[MKUserLocation class]])
            [self.mapView removeAnnotation:annotation];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.searchDisplayController.searchResultsTableView){
        [self.searchDisplayController setActive:NO animated:YES];
        
        switch(indexPath.section){
            case 0:
            {
                Addresses *address = [self.searchedAddresses objectAtIndex:indexPath.row];
                self.selectedObj = address;
                self.selectedAddress.coordinate = address.coordinate;
                self.selectedAddress.title = address.address1;
                self.selectedAddress.subtitle = address.address;
                
                [self centeringSelectedLocation];
            }
                break;
            case 1:
            {
                id item = [self.localSearchResult objectAtIndex:indexPath.row];
                
                if([item isKindOfClass:[MKMapItem class]]){
                    MKMapItem *mapItem = (MKMapItem *)item;
                    
                    self.selectedObj = mapItem.placemark;
                    self.selectedAddress.coordinate = mapItem.placemark.location.coordinate;
                    self.selectedAddress.title = mapItem.name;
                    self.selectedAddress.subtitle = mapItem.placemark.address;
                    
                    [self centeringSelectedLocation];
                } else if([item isKindOfClass:[GMPrediction class]]){
                    GMPrediction *prediction = (GMPrediction *)item;
                    
                    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&language=en-us&key=AIzaSyCZgeLhjJ4Vos750fZ7cD3r3oLjgngT5U4",
                                           prediction.placeId];
                    PCLog(@"urlString : %@", urlString);
                    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
                    
                    [NSURLConnection sendAsynchronousRequest:request
                                                       queue:[NSOperationQueue mainQueue]
                                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                                               NSString *dataString = [[NSString alloc] initWithData:data
                                                                                            encoding:NSUTF8StringEncoding];
                                               NSDictionary *resultDict = [dataString objectFromJSONString];
                                               
                                               [prediction updateWithDetailDictionary:[resultDict objectForKey:@"result"]];
                                               
                                               PCLog(@"Place Detail : %@", resultDict);
                                               
                                               self.selectedObj = prediction;
                                               self.selectedAddress.coordinate = prediction.coordinate;
                                               self.selectedAddress.title = prediction.name;
                                               self.selectedAddress.subtitle = prediction.desc;
                                               
                                               [self centeringSelectedLocation];
                                               
                                           }];
                }
            }
                break;
        }
    }
}

- (void)centeringSelectedLocation
{
    [self removeAllAnnotationWithoutUserLocation];
    self.searchDisplayController.searchBar.placeholder = self.selectedAddress.title;
    [self.mapView addAnnotation:self.selectedAddress];
    [self.mapView setCenterCoordinate:self.selectedAddress.coordinate
                             animated:NO];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == self.searchDisplayController.searchResultsTableView){
        UIView *sectionHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, SECTION_HEADER_HEIGHT)];
        sectionHeaderView.backgroundColor = [UIColor whiteColor];
        
        NSString *imageName = nil;
        switch(section){
            case 0:
                imageName = @"icon_clock";
                break;
                
            case 1:
                imageName = @"icon_map";
                break;
        }
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        CGRect frame = imageView.frame;
        frame.origin.x = 15.0f;
        frame.origin.y = (SECTION_HEADER_HEIGHT - frame.size.height) / 2;
        imageView.frame = frame;
        
        [sectionHeaderView addSubview:imageView];
        
        return sectionHeaderView;
    }
    return nil;
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.searchedAddresses = [NSMutableArray array];
    self.localSearchResult = [NSMutableArray array];
    
    PCLog(@"Text : %@, Length : %d", searchText, [searchText length]);
    
    if([searchText length] > 0){
        if(![searchText isEqualToString:@"\b"]){
            
            NSString *newSearchText = searchText;
            if([searchText hasPrefix:@"\b"]){
                newSearchText = [searchText substringFromIndex:1];
            }
            
            for(Addresses *address in self.addresses){
                NSRange range = [address.address1 rangeOfString:newSearchText
                                                        options:NSCaseInsensitiveSearch];
                if(range.location != NSNotFound){
                    [self.searchedAddresses addObject:address];
                }
            }
            
//            MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
//            request.naturalLanguageQuery = newSearchText;
//            request.region = self.mapView.region;
//            
//            // Create and initialize a search object.
//            MKLocalSearch *search = [[MKLocalSearch alloc] initWithRequest:request];
//            
//            // Start the search and display the results as annotations on the map.
//            [search startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error)
//             {
//                 if(error != nil){
//                     PCError(@"NSLocalSearch Error : %@", error);
//                     return;
//                 }
//                 
//                 for (MKMapItem *item in response.mapItems) {
//                     [self.localSearchResult addObject:item];
//                 }
//                 
//                 [self.searchDisplayController.searchResultsTableView reloadData];
//             }];
            
            NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?sensor=false&key=AIzaSyCZgeLhjJ4Vos750fZ7cD3r3oLjgngT5U4&components=country:us&input=%@&location=%f,%f"
                                   , [newSearchText urlEncodedString]
                                   , self.mapView.region.center.latitude
                                   , self.mapView.region.center.longitude];
            PCLog(@"urlString : %@", urlString);
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                                       NSString *dataString = [[NSString alloc] initWithData:data
                                                                                  encoding:NSUTF8StringEncoding];
                                       NSDictionary *resultDict = [dataString objectFromJSONString];
                                       
                                       for(NSDictionary *predictionDict in [resultDict objectForKey:@"predictions"]){
                                           GMPrediction *prediction = [[GMPrediction alloc] initWithDictionary:predictionDict];
                                           [self.localSearchResult addObject:prediction];
                                       }
                                       
                                       PCLog(@"self.localSearchResult %@", self.localSearchResult);
                                       
                                       [self.searchDisplayController.searchResultsTableView reloadData];
                                       PCLog(@"Reload Data called");
                                   }];
            
        } else {
            self.searchedAddresses = [NSMutableArray arrayWithArray:self.addresses];
        }
    } else {
        self.searchedAddresses = [NSMutableArray arrayWithArray:self.addresses];
    }
    
    
    if([searchText length] == 0){
        searchBar.text = @"\b";
    }
}

//- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
//{
////    PCLog(@"resultString : %@", searchText);
//    
//    self.searchedAddresses = [NSMutableArray array];
//    self.localSearchResult = [NSMutableArray array];
//    
//    if([searchText length] > 0){
//        if(![searchText isEqualToString:@"\b"]){
//        
//            NSString *newSearchText = searchText;
//            if([searchText hasPrefix:@"\b"]){
//                newSearchText = [searchText substringFromIndex:1];
//            }
//            
//            PCLog(@"Text : %@, Length : %d", newSearchText, [newSearchText length]);
//            
//            for(Addresses *address in self.addresses){
//                NSRange range = [address.address1 rangeOfString:newSearchText];
//                if(range.location != NSNotFound){
//                    [self.searchedAddresses addObject:address];
//                }
//            }
//            
//            MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
//            request.naturalLanguageQuery = newSearchText;
//            request.region = self.mapView.region;
//            
//            // Create and initialize a search object.
//            MKLocalSearch *search = [[MKLocalSearch alloc] initWithRequest:request];
//            
//            // Start the search and display the results as annotations on the map.
//            [search startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error)
//             {
//                 if(error != nil){
//                     PCError(@"NSLocalSearch Error : %@", error);
//                     return;
//                 }
//                 
//                 for (MKMapItem *item in response.mapItems) {
//                     [self.localSearchResult addObject:item];
//                 }
//                 
//                 [self.searchDisplayController.searchResultsTableView reloadData];
//             }];
//        } else {
//            self.searchedAddresses = [NSMutableArray arrayWithArray:self.addresses];
//        }
//    } else {
//        self.searchedAddresses = [NSMutableArray arrayWithArray:self.addresses];
//    }
//    
//    [self.searchDisplayController.searchResultsTableView reloadData];
//    
//    if([searchText length] == 0){
//        searchBar.text = @"\b";
//    }
//}

- (void) searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller
{
    controller.searchBar.text = @"\b";
    
    self.searchedAddresses = [NSMutableArray array];
    self.localSearchResult = [NSMutableArray array];
    
    self.searchedAddresses = [NSMutableArray arrayWithArray:self.addresses];

    [self.searchDisplayController.searchResultsTableView reloadData];
}



- (NSMutableArray *)addresses
{
    if(_addresses == nil){
        NSMutableArray *addresses = [Addresses listAll];
        if([addresses count] > 5){
            [addresses removeObjectsInRange:NSMakeRange(5, [addresses count] - 5)];
        }
        _addresses = addresses;
        
        self.searchedAddresses = [NSMutableArray arrayWithArray:_addresses];
    }
    return _addresses;
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    if(self.selectedAddress == nil){
        return nil;
    }
    
    MKPinAnnotationView *pin = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"pin"];
    if(pin == nil){
        pin = [[MKPinAnnotationView alloc] initWithAnnotation:self.selectedAddress
                                              reuseIdentifier:@"pin"];
        pin.canShowCallout = YES;
        pin.animatesDrop = YES;
        
        
    }
    
    return pin;
}

- (IBAction)MapLongPressed:(UIGestureRecognizer *)sender
{
    if(sender.state == UIGestureRecognizerStateBegan){
        CGPoint point = [sender locationInView:self.view];
        PCLog(@"Point %@", NSStringFromCGPoint(point));
        
        CLLocationCoordinate2D coords = [self.mapView convertPoint:point
                                              toCoordinateFromView:self.view];
        
        [self setLocationWithCoords:coords];
    }
}

- (void)setLocationWithCoords:(CLLocationCoordinate2D)coords
{
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&key=AIzaSyCZgeLhjJ4Vos750fZ7cD3r3oLjgngT5U4&language=en&location_type=ROOFTOP"
                           , coords.latitude
                           , coords.longitude];
    
    PCLog(@"urlString : %@", urlString);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               NSString *dataString = [[NSString alloc] initWithData:data
                                                                            encoding:NSUTF8StringEncoding];
                               NSDictionary *resultDict = [dataString objectFromJSONString];
                               
                               PCLog(@"resultDict %@", resultDict);
                               NSArray *resultList = [resultDict objectForKey:@"results"];
                               if([resultList count] > 0){
                                   NSDictionary *addrDict = [resultList objectAtIndex:0];
                                   GMPrediction *prediction = [[GMPrediction alloc] initWithDictionary:addrDict];
                                   [prediction updateWithDetailDictionary:addrDict];
                                   
                                   AddressAnnotation *addressAnotation = [[AddressAnnotation alloc] init];
                                   addressAnotation.coordinate = coords;
                                   
                                   self.selectedAddress.coordinate = prediction.coordinate;
                                   addressAnotation.title = prediction.name;
                                   addressAnotation.subtitle = prediction.desc;
                                   self.selectedAddress = addressAnotation;
                                   
                                   self.selectedObj = prediction;
                                   self.searchDisplayController.searchBar.placeholder = self.selectedAddress.subtitle;
                                   [self removeAllAnnotationWithoutUserLocation];
                                   [self.mapView addAnnotation:self.selectedAddress];

                               }
                           }];
    
    

//    [self.geoCoder reverseGeocodeLocation:[[CLLocation alloc] initWithLatitude:coords.latitude
//                                                                     longitude:coords.longitude]
//                        completionHandler:^(NSArray *placemarks, NSError *error)
//     {
//         if(error != nil){
//             PCError(@"Reverse geocding Error %@", error);
//             
//             return;
//         }
//         
//         
//         if([placemarks count] > 0){
//             CLPlacemark *firstPlacemark = [placemarks objectAtIndex:0];
//             
//             AddressAnnotation *addressAnotation = [[AddressAnnotation alloc] init];
//             addressAnotation.coordinate = coords;
//             addressAnotation.title = firstPlacemark.name;
//             addressAnotation.subtitle = firstPlacemark.address;
//             self.selectedAddress = addressAnotation;
//             
//             self.selectedObj = firstPlacemark;
//             
//             self.searchDisplayController.searchBar.placeholder = self.selectedAddress.title;
//             
//             [self removeAllAnnotationWithoutUserLocation];
//             [self.mapView addAnnotation:self.selectedAddress];
//         } else {
//             //Nothing Found
//         }
//         
//     }];
}

@end



