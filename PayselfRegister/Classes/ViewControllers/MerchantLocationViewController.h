//
//  MerchantLocationViewController.h
//  RushOrder
//
//  Created by Conan on 3/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCiPadViewController.h"
#import <MapKit/MapKit.h>

@protocol MerchantLocationViewControllerDelegate;
@class PinAnnotation;

@interface MerchantLocationViewController : PCiPadViewController
<
MKMapViewDelegate,
UITableViewDelegate,
UITableViewDataSource,
UIGestureRecognizerDelegate,
UISearchDisplayDelegate,
UISearchBarDelegate
>

@property (weak, nonatomic) id<MerchantLocationViewControllerDelegate> delegate;
@property (strong, nonatomic) CLLocation *initialLocation;
//@property (copy, nonatomic) NSString *initialAddress;
@end

@protocol MerchantLocationViewControllerDelegate <NSObject>
@optional
- (void)mapViewController:(MerchantLocationViewController *)mapViewController
      didSelectPlacemarks:(NSArray *)placemarks
            pinAnnotation:(PinAnnotation *)pinAnnotation;
@end

/*============================================================================*/
@interface PinAnnotation : NSObject <MKAnnotation>
{
    CLLocationCoordinate2D _coordinate;
}
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@end