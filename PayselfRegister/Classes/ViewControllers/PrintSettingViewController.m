//
//  PrintSettingViewController.m
//  RushOrder
//
//  Created by Conan on 9/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PrintSettingViewController.h"
#import <StarIO/SMPort.h>
#import "ePOS-Print.h"
#import "BixPrintManager.h"
#import "EpsonPrintManager.h"
#import "StarPrintManager.h"

#define EPSON_DISCOVERY_INTERVAL 0.5f

#define SEARCHING_TIMEOUT_COUNT 14 //per 0.5f

@interface PrintSettingViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *sectionList;
@property (strong, nonatomic) UISwitch *kitchenSwitch;
@property (strong, nonatomic) UISwitch *billSwitch;
@property (strong, nonatomic) UISwitch *receiptSwitch;

@property (strong, nonatomic) BXPrinterController *bixController;

@property (strong, nonatomic) NSMutableArray *bixPrinters;
@property (strong, nonatomic) NSMutableArray *starPrinters;
@property (strong, nonatomic) NSMutableArray *epsonPrinters;

@property (nonatomic) BOOL inSearchingBix;
@property (nonatomic) BOOL inSearchingEpson;
@property (nonatomic) BOOL inSearchingStar;

@property (strong, nonatomic) NSTimer *epsonTimer;
@property (nonatomic) NSUInteger epsonTimerCount;

//@property (nonatomic) PrinterMakerCode selectedMakerCode;
//@property (copy, nonatomic) NSString *selectedPrinterId;
@property (strong, nonatomic) IBOutlet UITableViewCell *whenToPrintCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *selectPrinterCell;

@property (weak, nonatomic) IBOutlet NPToggleButton *neoOrderButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *confirmButton;

@end

@implementation PrintSettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self != nil){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
//    NSArray *printers = [NSUserDefaults standardUserDefaults].receiptPrinters;
//    if([printers count] > 0){
//        NSDictionary *selectedPrinter = [printers objectAtIndex:0];
//        APP.selectedMakerCode = [selectedPrinter integerForKey:@"MakerCode"];
//        APP.selectedPrinterId = [selectedPrinter objectForKey:@"PrinterId"];
//    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    BIXCON.delegate = BIX;
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)savePrintSetting
{
    if(APP.selectedPrinterId != nil && APP.selectedMakerCode != 0){
        NSDictionary *selectedPrinter = @{@"MakerCode" : [NSNumber numberWithInteger:APP.selectedMakerCode],
                                          @"PrinterId" : APP.selectedPrinterId};
        NSArray *selectedPrinters = [NSArray arrayWithObject:selectedPrinter];
        [NSUserDefaults standardUserDefaults].receiptPrinters = selectedPrinters;
    } else {
        [NSUserDefaults standardUserDefaults].receiptPrinters = nil;
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)addDefaultItems
{
    ///////////////////////// Sections /////////////////////////////////////////
    NSMutableArray *sectionItems = [NSMutableArray array];
    
    //Menu
    TableItem *tableItem = [[TableItem alloc] init];
    tableItem.title = NSLocalizedString(@"Auto Kitchen Print", nil);
    tableItem.tag = 1;
    [sectionItems addObject:tableItem];
    
    if(CRED.merchant.isTableBase){
        tableItem = [[TableItem alloc] init];
        tableItem.title = NSLocalizedString(@"Auto Bill Print (Dine-in Only)", nil);
        tableItem.tag = 2;
        [sectionItems addObject:tableItem];
    }
    
    tableItem = [[TableItem alloc] init];
    tableItem.title = NSLocalizedString(@"Auto Receipt Print", nil);
    tableItem.tag = 3;
    [sectionItems addObject:tableItem];
    
    //Section
    TableSection *tableSection = [[TableSection alloc] init];
    tableSection.title = NSLocalizedString(@"Auto Printing", nil);
    tableSection.menus = sectionItems;
    [self.sectionList addObject:tableSection];
    //////////
    
    ///////////////////////// Sections /////////////////////////////////////////
    sectionItems = [NSMutableArray array];
    
    //Menu
    tableItem = [[TableItem alloc] init];
    tableItem.title = NSLocalizedString(@"When to print", nil);
    tableItem.tag = 4;
    [sectionItems addObject:tableItem];
    
    //Menu
    tableItem = [[TableItem alloc] init];
    tableItem.title = NSLocalizedString(@"Select Printer You Are Using", nil);
    tableItem.tag = 5;
    [sectionItems addObject:tableItem];
    
    //Section
    tableSection = [[TableSection alloc] init];
    tableSection.title = NSLocalizedString(@"Print New Order When", nil);
    tableSection.menus = sectionItems;
    [self.sectionList addObject:tableSection];
    //////////
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = NSLocalizedString(@"Print Setting", nil);
    
    self.self.preferredContentSize = CGSizeMake(self.view.frame.size.width
                                                  ,100.0f);
    
    self.neoOrderButton.selected = [NSUserDefaults standardUserDefaults].printWhenNewOrder;
    self.confirmButton.selected = !self.neoOrderButton.selected;
    
    [self searchPrinters];
}

- (void)searchPrinters
{
    self.inSearchingBix = YES;
    // Bixolon /////////////////////////////////////////////////////////////////
    if(self.bixController.isConnected){
        [self.bixController disconnect];
    } else {
        self.inSearchingBix = YES;
        self.bixPrinters = [NSMutableArray array];
        [self.bixController lookup];
    }
    
    // Bixolon /////////////////////////////////////////////////////////////////
    
    // Star micronics //////////////////////////////////////////////////////////
    self.inSearchingStar = YES;
    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT
                                                        , 0);
    dispatch_async(aQueue, ^{
        NSArray *printers = [SMPort searchPrinter];
        
        self.starPrinters = [NSMutableArray arrayWithArray:printers];
        self.inSearchingStar = NO;
        dispatch_async(dispatch_get_main_queue(),^{
            
            // Reconfigure
            [self configureList];
            // Reload
            [self.tableView reloadData];
            
            for(PortInfo *port in printers){
                NSLog(@"printers portName:%@\n\tmacAddress:%@\n\tmodelName:%@"
                      , port.portName
                      , port.macAddress
                      , port.modelName);
            }
        });
    });
    // Star micronics //////////////////////////////////////////////////////////
    
    // Epson ///////////////////////////////////////////////////////////////////
    [EpsonIoFinder stop];
    
    if(self.epsonTimer != nil){
        [self.epsonTimer invalidate];
        self.epsonTimer = nil;
    }
    
    int result = [EpsonIoFinder start:EPSONIO_OC_DEVTYPE_TCP FindOption:@"255.255.255.255"];
    if(result != EPSONIO_OC_SUCCESS){
        PCError(@"Epson printer finding got error");
    }
    
    self.epsonTimerCount = 0;
    self.inSearchingEpson = YES;
    self.epsonTimer = [NSTimer scheduledTimerWithTimeInterval:EPSON_DISCOVERY_INTERVAL
                                              target:self
                                            selector:@selector(timerFindPrinter:)
                                            userInfo:nil
                                             repeats:YES];
    [self configureList];
    [self.tableView reloadData];
}

- (void)timerFindPrinter:(NSTimer*)timer
{
    if(self.epsonTimerCount >= SEARCHING_TIMEOUT_COUNT){
        
        int errStatus = [EpsonIoFinder stop];
        
        if(errStatus != EPSONIO_OC_SUCCESS){
            PCError(@"Epson printer stopping got error");
        }
        
        [self.epsonTimer invalidate];
        self.epsonTimer = nil;
        self.inSearchingEpson = NO;
        [self performSelectorOnMainThread:@selector(reloadPrinterView)
                               withObject:nil
                            waitUntilDone:YES];
        return;
    }
    
    self.epsonTimerCount++;
    
    int result = 0;
    NSArray *items = [EpsonIoFinder getResult:&result];
    if(items != nil && result == EPSONIO_OC_SUCCESS){
        bool change = NO;
        if([self.epsonPrinters count] != [items count]){
            self.epsonPrinters = [NSMutableArray arrayWithArray:items];
            PCLog(@"self.epsonPrinters %@", self.epsonPrinters);
            change = YES;
        }
        if(change){
            [self performSelectorOnMainThread:@selector(reloadPrinterView)
                                   withObject:nil
                                waitUntilDone:YES];
        }
    }
}

- (void)configureList
{
    self.sectionList = [NSMutableArray array];
    
    [self addDefaultItems];
    
    NSMutableArray *sectionItems = [NSMutableArray array];
    
    if([self.epsonPrinters count] > 0){
        
        for(NSString *ipAddress in self.epsonPrinters){
            //Menu
            TableItem *tableItem = [[TableItem alloc] init];
            tableItem.title = ipAddress;
            tableItem.action = NULL;
            tableItem.tag = PrinterMakerEpson;
            tableItem.object = ipAddress;
            [sectionItems addObject:tableItem];
        }
    } else {
        
        //Menu
        TableItem *tableItem = [[TableItem alloc] init];
        tableItem.title = !self.inSearchingEpson ? NSLocalizedString(@"Not Found", nil) : NSLocalizedString(@"Searching...", nil);
        tableItem.action = NULL;
        tableItem.tag = PrinterMakerEpson;
        [sectionItems addObject:tableItem];
    }
    
    //Section
    TableSection *tableSection = [[TableSection alloc] init];
    tableSection.title = NSLocalizedString(@"EPSON Printers", nil);
    tableSection.menus = sectionItems;
    [self.sectionList addObject:tableSection];
    
    sectionItems = [NSMutableArray array];
    
    if([self.starPrinters count] > 0){
        
        for(PortInfo *port in self.starPrinters){
            //Menu
            TableItem *tableItem = [[TableItem alloc] init];
            tableItem.title = port.portName;
            tableItem.subTitle = port.modelName;
            tableItem.action = NULL;
            tableItem.tag = PrinterMakerStar;
            tableItem.object = port;
            [sectionItems addObject:tableItem];
        }
    } else {
        
        //Menu
        TableItem *tableItem = [[TableItem alloc] init];
        tableItem.title = !self.inSearchingStar ? NSLocalizedString(@"Not Found", nil) : NSLocalizedString(@"Searching...", nil);
        tableItem.action = NULL;
        tableItem.tag = PrinterMakerStar;
        [sectionItems addObject:tableItem];
    }
    
    //Section
    tableSection = [[TableSection alloc] init];
    tableSection.title = NSLocalizedString(@"Star Micronics Printers", nil);
    tableSection.menus = sectionItems;
    [self.sectionList addObject:tableSection];
    
    sectionItems = [NSMutableArray array];
    
    if([self.bixPrinters count] > 0){
        
        for(BXPrinter *printer in self.bixPrinters){
            //Menu
            TableItem *tableItem = [[TableItem alloc] init];
            tableItem.title = printer.address;
            tableItem.subTitle = printer.friendlyName;
            tableItem.action = NULL;
            tableItem.tag = PrinterMakerBixolon;
            tableItem.object = printer;
            [sectionItems addObject:tableItem];
        }
    } else {
        //Menu
        TableItem *tableItem = [[TableItem alloc] init];
        tableItem.title = !self.inSearchingBix ? NSLocalizedString(@"Not Found", nil) : NSLocalizedString(@"Searching...", nil);
        tableItem.action = NULL;
        tableItem.tag = PrinterMakerBixolon;
        [sectionItems addObject:tableItem];
    }
    
    //Section
    tableSection = [[TableSection alloc] init];
    tableSection.title = NSLocalizedString(@"Bixolon Printers", nil);
    tableSection.menus = sectionItems;
    [self.sectionList addObject:tableSection];
}

- (void)reloadPrinterView
{
    // Reconfigure
    [self configureList];
    // Reload
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self.epsonTimer invalidate];
    [super viewDidUnload];
}

#pragma mark - UITableViewDelegate & DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sectionList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    TableSection *tableSection = [self.sectionList objectAtIndex:section];
    return [tableSection.menus count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    TableSection *tableSection = [self.sectionList objectAtIndex:section];
    return tableSection.title;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:CellIdentifier];
    }
    
    TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
    TableItem *tableItem = [tableSection.menus objectAtIndex:indexPath.row];
    
    cell.textLabel.text = tableItem.title;
    cell.detailTextLabel.text = tableItem.subTitle;
    
    if(tableItem.action != NULL){
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    } else {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.accessoryView = nil;
    switch(tableItem.tag){
        case 1:
            cell.accessoryView = self.kitchenSwitch;
            self.kitchenSwitch.on = [NSUserDefaults standardUserDefaults].autoKitchenPrint;
            break;
        case 2:
            cell.accessoryView = self.billSwitch;
            self.billSwitch.on = [NSUserDefaults standardUserDefaults].autoBillPrint;
            break;
        case 3:
            cell.accessoryView = self.receiptSwitch;
            self.receiptSwitch.on = [NSUserDefaults standardUserDefaults].autoReceiptPrint;
            break;
        case 4:
            return self.whenToPrintCell;
            break;
        case 5:
            return self.selectPrinterCell;
            break;
        case PrinterMakerEpson:
            if(APP.selectedMakerCode == tableItem.tag){
                NSString *ipAddr = (NSString *)tableItem.object;
                if([APP.selectedPrinterId isEqualToString:ipAddr]){
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                } else {
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            } else {
                if(self.inSearchingEpson){
                    UIActivityIndicatorView *view = [[UIActivityIndicatorView alloc] init];
                    view.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
                    [view startAnimating];
                    cell.accessoryView = view;
                } else {
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            }
            break;
        case PrinterMakerStar:
            if(APP.selectedMakerCode == tableItem.tag){
                PortInfo *port = (PortInfo *)tableItem.object;
                if([APP.selectedPrinterId isEqualToString:port.portName]){
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                } else {
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            } else {
                if(self.inSearchingStar){
                    UIActivityIndicatorView *view = [[UIActivityIndicatorView alloc] init];
                    view.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
                    [view startAnimating];
                    cell.accessoryView = view;
                } else {
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            }
            break;
        case PrinterMakerBixolon:
            if(APP.selectedMakerCode == tableItem.tag){
                BXPrinter *printer = (BXPrinter *)tableItem.object;
                if([APP.selectedPrinterId isEqualToString:printer.macAddress]){
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                } else {
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            } else {
                if(self.inSearchingBix){
                    UIActivityIndicatorView *view = [[UIActivityIndicatorView alloc] init];
                    view.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
                    [view startAnimating];
                    cell.accessoryView = view;
                } else {
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            }
            break;
    }
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
    TableItem *tableItem = [tableSection.menus objectAtIndex:indexPath.row];
    

    if(tableItem.tag == 4){
        return 83.0f;
    } else if(tableItem.tag == 5){
        return 50.0f;
    } else {
        return 44.0f;
    }
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
    TableItem *tableItem = [tableSection.menus objectAtIndex:indexPath.row];
    
    if(tableItem.action != NULL){
        if([self respondsToSelector:tableItem.action]){
            [self performSelector:tableItem.action
                       withObject:tableItem];
            [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        } else {
            if([self.delegate respondsToSelector:@selector(printSettingViewController:didSelectMenu:)]){
                [self.delegate printSettingViewController:self
                                            didSelectMenu:tableItem];
            }
        }
    } else {
        if(tableItem.object != nil){
            APP.selectedMakerCode = tableItem.tag;
            
            switch(tableItem.tag){
                case PrinterMakerEpson:{
                    NSString *ipAddr = (NSString *)tableItem.object;
                    if([ipAddr isEqualToString:APP.selectedPrinterId]){
                        APP.selectedPrinterId = nil;
                        EPSON.deviceName = nil;
                        APP.selectedMakerCode = PrinterMakerUnknown;
                    } else {
                        APP.selectedPrinterId = ipAddr;
                        EPSON.deviceName = APP.selectedPrinterId;
                    }
                }
                    break;
                case PrinterMakerStar:
                {
                    PortInfo *port = (PortInfo *)tableItem.object;
                    if([port.portName isEqualToString:APP.selectedPrinterId]){
                        APP.selectedPrinterId = nil;
                        STAR.portName = nil;
                        STAR.portSettings = nil;
                        APP.selectedMakerCode = PrinterMakerUnknown;
                    } else {
                        APP.selectedPrinterId = port.portName;
                        STAR.portName = APP.selectedPrinterId;
                        STAR.portSettings = @"Standard";
                    }
                }
                    break;
                case PrinterMakerBixolon:
                {
                    BXPrinter *printer = (BXPrinter *)tableItem.object;
                    if([printer.macAddress isEqualToString:APP.selectedPrinterId]){
                        APP.selectedPrinterId = nil;
                        BIX.macAddress = printer.macAddress;
                        APP.selectedMakerCode = PrinterMakerUnknown;
                    } else {
                        APP.selectedPrinterId = printer.macAddress;
                        BIX.macAddress = printer.macAddress;
                    }
                }
                    break;
            }
        } else {
            APP.selectedPrinterId = nil;
            APP.selectedMakerCode = 0;
        }
    }
    
    [self savePrintSetting];
    [self.tableView reloadData];
}

- (UISwitch *)kitchenSwitch
{
    if(_kitchenSwitch == nil){
        _kitchenSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 136.0f, 27.0f)];
        [_kitchenSwitch addTarget:self
                           action:@selector(kitchenSwitchValueChanged:)
                 forControlEvents:UIControlEventValueChanged];
    }
    
    return _kitchenSwitch;
}

- (UISwitch *)billSwitch
{
    if(_billSwitch == nil){
        _billSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 136.0f, 27.0f)];
        [_billSwitch addTarget:self
                           action:@selector(billSwitchValueChanged:)
                 forControlEvents:UIControlEventValueChanged];
    }
    
    return _billSwitch;
}

- (UISwitch *)receiptSwitch
{
    if(_receiptSwitch == nil){
        _receiptSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 136.0f, 27.0f)];
        [_receiptSwitch addTarget:self
                           action:@selector(receiptSwitchValueChanged:)
                 forControlEvents:UIControlEventValueChanged];
    }
    
    return _receiptSwitch;
}

- (void)kitchenSwitchValueChanged:(UISwitch *)sender
{
    [NSUserDefaults standardUserDefaults].autoKitchenPrint = sender.on;
}

- (void)billSwitchValueChanged:(UISwitch *)sender
{
    [NSUserDefaults standardUserDefaults].autoBillPrint = sender.on;
}

- (void)receiptSwitchValueChanged:(UISwitch *)sender
{
    [NSUserDefaults standardUserDefaults].autoReceiptPrint = sender.on;
}

- (IBAction)bixolonMoreButtonTouched:(UIButton *)sender
{
    NSString *url = nil;
    switch(sender.tag){
        case 1:
            url = @"http://www.bixolon.com/html/en/product/product_detail.xhtml?pgNum=&prod_id=2&large_cd=0001&small_cd=";
            break;
        case 2:
            url = @"http://www.bixolon.com/html/en/product/product_detail.xhtml?pgNum=&prod_id=1&large_cd=0001&small_cd=";
            break;
    }
    
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (BXPrinterController *)bixController
{
    if(_bixController == nil){
        _bixController = [BXPrinterController getInstance];
        _bixController.delegate = self;
        _bixController.lookupCount = 5;
        _bixController.AutoConnection = BXL_CONNECTIONMODE_NOAUTO;
    }
    return _bixController;
}

#pragma mark - BXPrinterDelegate
- (void)message:(BXPrinterController *)controller
           text:(NSString *)text
{
    PCLog(@"[PRINTER message] %@", text);
}


-(void)didUpdateStatus:(BXPrinterController*) controller
                status:(NSNumber*) status
{
    PCLog(@"didUpdateStatus");
}


- (void)msrArrived:(BXPrinterController *)controller
             track:(NSNumber *)track
{
    PCLog(@"msrArrived");
}

- (void)msrTerminated:(BXPrinterController *)controller
{
    PCLog(@"msrTerminated");
}

- (void)willLookupPrinters:(BXPrinterController *)controller
{
    PCLog(@"willLookupPrinters");
}

- (void)didLookupPrinters:(BXPrinterController *)controller
{
    PCLog(@"Did lookup printer %@", controller);
    self.inSearchingBix = NO;
    // Reconfigure
    [self configureList];
    // Reload
    [self.tableView reloadData];
    
}

- (void)didFindPrinter:(BXPrinterController *)controller
               printer:(BXPrinter *)printer
{
    NSUInteger i = 0;
    
    if([self.bixPrinters count] > 0){
        for(i = 0 ; i < [self.bixPrinters count]; i++){
            BXPrinter *aPrinter = [self.bixPrinters objectAtIndex:i];
            if([aPrinter.macAddress isEqualToString:printer.macAddress]){
                continue;
            } else {
                [self.bixPrinters addObject:printer];
                break;
            }
        }
    } else {
        [self.bixPrinters addObject:printer];
    }
    
    PCLog(@"didFindPrinter %@\n\tname: %@\n\taddress:%@ \n\tmodelStr:%@\n\tversionStr:%@\n\tfriendlyName:%@\n\tmacAddress:%@"
          ,printer
          ,printer.name
          ,printer.address
          ,printer.modelStr
          ,printer.versionStr
          ,printer.friendlyName
          ,printer.macAddress);
}


- (void)willConnect:(BXPrinterController *)controller
            printer:(BXPrinter *)printer
{
    PCLog(@"willConnect");
}

- (void)didConnect:(BXPrinterController *)controller
           printer:(BXPrinter *)printer
{
    PCLog(@"didConnect");
}

- (void)didNotConnect:(BXPrinterController *)controller
              printer:(BXPrinter *)printer
            withError:(NSError *)error
{
    PCLog(@"didNotConnect");
}

- (void)didDisconnect:(BXPrinterController *)controller
              printer:(BXPrinter *)printer
{
    [self.bixController lookup];
    self.inSearchingBix = YES;
    self.bixPrinters = [NSMutableArray array];
}

- (void)didBeBrokenConnection:(BXPrinterController *)controller
                      printer:(BXPrinter *)printer
                    withError:(NSError *)error
{
    PCLog(@"didBeBrokenConnection");
}

- (IBAction)toggleButtonTouched:(NPToggleButton *)sender
{
    sender.selected = !sender.isSelected;
    
    if(sender == self.neoOrderButton){
        self.confirmButton.selected = !sender.isSelected;
    } else if(sender == self.confirmButton){
        self.neoOrderButton.selected = !sender.isSelected;
    }
    [NSUserDefaults standardUserDefaults].printWhenNewOrder = self.neoOrderButton.isSelected;
    
    if(![NSUserDefaults standardUserDefaults].printWhenNewOrder){
        [NSUserDefaults standardUserDefaults].lastNewPrintedNumber = 0;
        [NSUserDefaults standardUserDefaults].lastNewPrintedCartNumber = 0;
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
