//
//  TableButtonViewController.h
//  RushOrder
//
//  Created by Conan on 3/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCDynamicGridView.h"
#import "MerchantSettingMenuViewController.h"
#import "MerchantLocationViewController.h"
#import "DCRoundSwitch.h"
#import "OptionViewController.h"

@interface TableButtonViewController : PCiPadViewController
<
PCDynamicGridViewDelegate,
PCDynamicGridViewDataSource,
UIPopoverControllerDelegate,
UIActionSheetDelegate,
MerchantSettingMenuViewControllerDelegate,
OptionViewControllerDelegate,
MerchantLocationViewControllerDelegate,
UIAlertViewDelegate
>

- (IBAction)onSwitchValueChanged:(DCRoundSwitch *)sender;

@end
