//
//  CompleteOptionViewController.m
//  RushOrder
//
//  Created by Conan on 3/13/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "CompleteOptionViewController.h"
#import "JSONKit.h"

@interface CompleteOptionViewController ()

@property (weak, nonatomic) IBOutlet NPStretchableButton *doneButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *cancelButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *cashButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *cardButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *otherButton;
@property (weak, nonatomic) IBOutlet PCTextField *memoTextField;
@property (strong, nonatomic) NSMutableDictionary *optionDict;

@property (nonatomic) BOOL doneTouched;
@end

@implementation CompleteOptionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [self.view addDefaultBackgroundImage];
    
    self.title = NSLocalizedString(@"Complete confirmation", nil);
    if(self.navigationController.parentViewController == nil){
        //In popover
        self.cancelButton.hidden = NO;
        self.preferredContentSize = CGSizeMake(self.view.width,
                                                      CGRectGetMaxY(self.cancelButton.frame) + DEFAULT_BOTTOM_MARGIN);
    } else {
        //Master
        self.cancelButton.hidden = YES;
    }
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    if(self.doneTouched){
        [self.delegate completeOptionViewController:self
                                    didSelectOption:[self.optionDict JSONString]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setDoneButton:nil];
    [self setCancelButton:nil];
    [self setCashButton:nil];
    [self setCardButton:nil];
    [self setOtherButton:nil];
    [self setMemoTextField:nil];
    [super viewDidUnload];
}

- (IBAction)doneButtonTouched:(id)sender
{
    
    self.optionDict = [NSMutableDictionary dictionary];
    
    [self.optionDict setObject:[NSNumber numberWithBool:self.cashButton.selected]
                   forKey:@"cash"];
    
    [self.optionDict setObject:[NSNumber numberWithBool:self.cardButton.selected]
                   forKey:@"card"];
    
    [self.optionDict setObject:[NSNumber numberWithBool:self.otherButton.selected]
                   forKey:@"other"];
    
    [self.optionDict setObject:self.memoTextField.text
                   forKey:@"memo"];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    self.doneTouched = YES;
}

- (IBAction)cancelButtonTouched:(id)sender
{   
    [self.navigationController popViewControllerAnimated:YES];
}

@end
