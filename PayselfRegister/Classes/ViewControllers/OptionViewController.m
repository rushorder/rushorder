//
//  OptionViewController.m
//  RushOrder
//
//  Created by Conan on 4/21/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "OptionViewController.h"

@interface OptionViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISwitch *blinkSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *locationSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *directMenuSwitch;
@property (strong, nonatomic) IBOutlet UITableViewCell *flashCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *locationCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *menuSwitchCell;
@end

@implementation OptionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Options", nil);

    self.blinkSwitch.on = [NSUserDefaults standardUserDefaults].blinkWithNewOrder;
    self.locationSwitch.on = [NSUserDefaults standardUserDefaults].oneTouchLocationUpdate;
    self.directMenuSwitch.on = [NSUserDefaults standardUserDefaults].directSwitchMenu;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)blinkSwitchChanged:(id)sender
{
    [NSUserDefaults standardUserDefaults].blinkWithNewOrder = self.blinkSwitch.on;
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)onetouchSwitchChanged:(id)sender
{
    [NSUserDefaults standardUserDefaults].oneTouchLocationUpdate = self.locationSwitch.on;
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if([self.delegate respondsToSelector:@selector(optionViewController:didChangeOneTouchLocationUpdate:)]){
        [self.delegate optionViewController:self
            didChangeOneTouchLocationUpdate:sender];
    }
}

- (IBAction)directMenuSwitchChanged:(id)sender
{
    [NSUserDefaults standardUserDefaults].directSwitchMenu = self.directMenuSwitch.on;
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if([self.delegate respondsToSelector:@selector(optionViewController:didChangeDirectMenuSwitch:)]){
        [self.delegate optionViewController:self
                  didChangeDirectMenuSwitch:sender];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch(indexPath.row){
        case 0:
            return self.flashCell;
            break;
        case 1:
            return self.locationCell;
            break;
        case 2:
            return self.menuSwitchCell;
            break;
    }
    return nil;
}

@end
