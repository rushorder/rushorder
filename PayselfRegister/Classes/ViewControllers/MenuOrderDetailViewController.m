//
//  MenuOrderDetailViewController.m
//  RushOrder
//
//  Created by Conan on 5/16/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MenuOrderDetailViewController.h"
#import "MenuOptionGroup.h"
#import "MenuOption.h"
#import "OrderManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MenuManager.h"
#import "MenuSubCategory.h"
#import "MenuCategory.h"

@interface MenuOrderDetailViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NPStretchableButton *doneButton;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *menuImageView;
@property (weak, nonatomic) IBOutlet UILabel *menuNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *menuDescLabel;
@property (weak, nonatomic) IBOutlet UILabel *menuPriceLabel;
@property (strong, nonatomic) IBOutlet UIStepper *stepper;
@property (strong, nonatomic) IBOutlet UITableViewCell *quantityCell;
@property (weak, nonatomic) IBOutlet UILabel *quantityCellLabel;

@property (nonatomic) NSInteger quantity;

@property (nonatomic, getter = isAnyChanged) BOOL anyChanged;

@end

@implementation MenuOrderDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    if(self.isModifyingMode){
        if(self.canEdit){
            self.title = NSLocalizedString(@"Edit item", nil);
            self.doneButton.buttonTitle = NSLocalizedString(@"Update ordered item", nil);
            
            self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Update",nil)
                                                                                      target:self
                                                                                      action:@selector(updateButtonTouched:)];
            self.navigationItem.rightBarButtonItem.button.enabled = NO;
            
            self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Cancel",nil)
                                                                                     target:self
                                                                                     action:@selector(closeButtonTouched:)];
        } else {
            self.title = NSLocalizedString(@"Ordered item", nil);
            self.tableView.tableFooterView = nil;
            
            self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Close",nil)
                                                                                      target:self
                                                                                      action:@selector(closeButtonTouched:)];
        }
        
        [self.doneButton removeTarget:self
                               action:@selector(doneButtonTouched:)
                     forControlEvents:UIControlEventTouchUpInside];
        
        [self.doneButton addTarget:self
                            action:@selector(updateButtonTouched:)
                  forControlEvents:UIControlEventTouchUpInside];
        self.doneButton.enabled = NO;
        
        [self fillLineItemContents];
        
    } else {
        self.title = NSLocalizedString(@"Select Options", nil);
        self.doneButton.buttonTitle = NSLocalizedString(@"Add to Order List", nil);
        
        self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Add",nil)
                                                                                  target:self
                                                                                  action:@selector(doneButtonTouched:)];
        
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Cancel",nil)
                                                                                 target:self
                                                                                 action:@selector(closeButtonTouched:)];
        [self fillContents];
    }
    
    self.stepper.minimumValue = 1;
    self.stepper.maximumValue = 100;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fillContents
{
    if(self.lineItem == nil){
        self.quantity = 1;
    } else {
        self.quantity = self.lineItem.quantity;
    }
    self.stepper.value = self.quantity;
    
    if(self.item != nil){
        self.menuNameLabel.text = self.item.name;
        self.menuPriceLabel.text = [[NSNumber numberWithCurrency:self.item.price] currencyString];
    
        [self.menuImageView setImageWithURL:self.item.thumbnailURL
                           placeholderImage:[UIImage imageNamed:@"placeholder_menu"]];
    
        self.menuDescLabel.text = self.item.desc;
    } else {
        self.menuNameLabel.text = self.lineItem.menuName;
        
        self.menuPriceLabel.text = [[NSNumber numberWithCurrency:self.lineItem.unitPrice] currencyString];
        
        self.menuImageView.image = [UIImage imageNamed:@"placeholder_menu"];
        
        self.menuDescLabel.text = self.lineItem.serializedOptionNames;
    }
    
    [self.menuDescLabel sizeToHeightFit];
    self.headerView.height = CGRectGetMaxY(self.menuDescLabel.frame);
    self.tableView.tableHeaderView = self.headerView;
}

- (void)fillLineItemContents
{
    if(self.lineItem.menuItem == nil){
        [self.lineItem linkMenuItemInMenuList:MENUPAN.wholeSerialMenuList];
    }
    
    if(self.lineItem.menuItem == nil){
        
    } else {
        self.item = self.lineItem.menuItem;
        
        
        for(MenuPrice *price in self.item.prices){
            if(price.menuPriceNo == self.lineItem.selectedPrice.menuPriceNo){
                price.selected = YES;
            } else {
                price.selected = NO;
            }
        }
        
        
        for(MenuOptionGroup *group in self.item.optionGroups){
            for(MenuOption *option in group.options){
                
                BOOL gotcha = NO;
                for(MenuOption *menuOption in self.lineItem.selectedOptions){
                    if(option.menuOptionNo == menuOption.menuOptionNo){
                        gotcha = YES;
                        break;
                    }
                }
                option.selected = gotcha;
            }
        }
    }
    
    [self fillContents];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger sectionCount = [self.item.optionGroups count];
    if([self.item.prices count] > 1){
        sectionCount++;
    }
    
    sectionCount++; // For quantity
    
    return sectionCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger adjustedSection = section;
    
    if(section == 0) return 1; //For quantity
    adjustedSection--;
    
    
    if([self.item.prices count] > 1){
        if(section == 1){
            return [self.item.prices count];
        } else {
            adjustedSection--;
        }
    }
       
    MenuOptionGroup *group = [self.item.optionGroups objectAtIndex:adjustedSection];
    return [group.options count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSInteger adjustedSection = section;
    
    if(section == 0) return NSLocalizedString(@"Quantity", nil); //For quantity
    adjustedSection--; // For quantity
    
    
    if([self.item.prices count] > 1){
        if(section == 1){
            return @"Prices";
        } else {
            adjustedSection--;
        }
    }
    
    MenuOptionGroup *group = [self.item.optionGroups objectAtIndex:adjustedSection];
    
    return group.name;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        self.quantityCellLabel.text = [NSString stringWithFormat:@"%d", self.quantity];
        self.stepper.enabled = self.canEdit;
        return self.quantityCell;
    }
    
    NSInteger adjustedSection = indexPath.section;
    
    NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                      reuseIdentifier:cellIdentifier];
    }
    
    cell.accessoryView = nil;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    
    if([self.item.prices count] > 1){
        if(indexPath.section == 1){
            MenuPrice *price = [self.item.prices objectAtIndex:indexPath.row];
            
            cell.textLabel.text = price.name;
            cell.detailTextLabel.text = price.priceString;
            if(price.isSelected){
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            return cell;
        } else {
            adjustedSection--;
        }
    }
    
    adjustedSection--; // For quantity
    
    MenuOptionGroup *group = [self.item.optionGroups objectAtIndex:adjustedSection];
    MenuOption *option = [group.options objectAtIndex:indexPath.row];
    
    cell.textLabel.text = option.name;
    
    if(option.price > 0){
        cell.detailTextLabel.text = [@"+" stringByAppendingString:option.priceString];
    } else if(option.price < 0){
        cell.detailTextLabel.text = option.priceString;
    }  else {
        cell.detailTextLabel.text = @"";
    }
    
    if(option.isSelected){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!self.canEdit){
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        return;
    }
    
    NSInteger adjustedSection = indexPath.section;
    
    adjustedSection--; // For quantity
    
    if([self.item.prices count] > 1){
        if(indexPath.section == 1){
            
            NSInteger i = 0;
            for(i = 0 ; i < [self.item.prices count] ; i++){
                MenuPrice *price = [self.item.prices objectAtIndex:i];
                price.selected = (indexPath.row == i);
            }
            
            goto sum;
        } else {
            adjustedSection--;
        }
    }
    
    if([self.item.optionGroups count] > 0 && adjustedSection >= 0){
        MenuOptionGroup *group = [self.item.optionGroups objectAtIndex:adjustedSection];
        
        NSInteger i = 0;
        
        if(group.optionType == MenuOptionTypeChoose){
            for(i = 0 ; i < [group.options count] ; i++){
                MenuOption *option = [group.options objectAtIndex:i];
                if(indexPath.row == i){
                    option.selected = !option.selected;
                } else {
                    option.selected = NO;
                }
            }
        } else {
            MenuOption *option = [group.options objectAtIndex:indexPath.row];
            option.selected = !option.selected;
        }
    }
    
sum:
    self.navigationItem.rightBarButtonItem.button.enabled = YES;
    self.doneButton.enabled = YES;
    if(indexPath.section != 0){
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                      withRowAnimation:UITableViewRowAnimationFade];
    }
    
    self.menuPriceLabel.text = [[NSNumber numberWithCurrency:self.item.price] currencyString];
}

- (IBAction)closeButtonTouched:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:NULL];
}

- (IBAction)stepperValueChanged:(id)sender
{
    self.quantity = (NSInteger)self.stepper.value;
    self.quantityCellLabel.text = [NSString stringWithFormat:@"%d", self.quantity];
    
    self.navigationItem.rightBarButtonItem.button.enabled = YES;
    self.doneButton.enabled = YES;
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setDoneButton:nil];
    [self setStepper:nil];
    [self setHeaderView:nil];
    [self setQuantityCell:nil];
    [self setQuantityCellLabel:nil];
    [super viewDidUnload];
}

- (IBAction)doneButtonTouched:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^(){
                                 if([self.delegate respondsToSelector:@selector(menuOrderDetailViewController:didNewMenuItem:quantity:)]){
                                     [self.delegate menuOrderDetailViewController:self
                                                                   didNewMenuItem:self.item
                                                                         quantity:self.quantity];
                                 }
                             }];
}

- (IBAction)updateButtonTouched:(id)sender
{
    [self applyLineItem];
    [self dismissViewControllerAnimated:YES
                             completion:^(){
                                 if([self.delegate respondsToSelector:@selector(menuOrderDetailViewController:didUpdateLineItem:)]){
                                     [self.delegate menuOrderDetailViewController:self
                                                                didUpdateLineItem:self.lineItem];
                                 }
                             }];
}


- (void)applyLineItem
{
    if(self.lineItem != nil){
        self.lineItem.menuValidStatus = MenuValidStatusValid;
        self.lineItem.selectedPrice = [self.item selectedMenuPrice];
        self.lineItem.selectedOptions = [self.item selectedOptions];
        self.lineItem.quantity = self.quantity;
    }
}

- (BOOL)isModifyingMode
{
    return (self.lineItem != nil);
}

- (BOOL)canEdit
{
    return ((self.lineItem.isMine && self.lineItem.status == LineItemStatusOrdering) || self.lineItem == nil || self.cart.isMine);
}
@end
