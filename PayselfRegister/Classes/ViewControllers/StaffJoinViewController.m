//
//  StaffJoinViewController.m
//  RushOrder
//
//  Created by Conan on 10/7/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "StaffJoinViewController.h"
#import "PCMerchantService.h"
#import "JoinedMerchantViewController.h"

@interface StaffJoinViewController ()

@property (weak, nonatomic) IBOutlet PCTextField *merchantCodeTextField;
@end

@implementation StaffJoinViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Staff Join", nil);
    
    if(self.navigationController.rootViewController == self){
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonSystemItem:UIBarButtonSystemItemAction
                                                                              target:self
                                                                              action:@selector(functionButtonTouched:)];
        
        self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonSystemItem:UIBarButtonSystemItemOrganize
                                                                               target:self
                                                                               action:@selector(myRequestListBouttonTouched:)];
    }
    
    [self.merchantCodeTextField becomeFirstResponder];
}

- (void)functionButtonTouched:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"Sign Out", nil)
                                                    otherButtonTitles:nil];
    actionSheet.tag = 1;
    [actionSheet showFromBarButtonItem:sender
                              animated:YES];
}

- (void)myRequestListBouttonTouched:(id)sender
{
    JoinedMerchantViewController *viewController = [JoinedMerchantViewController viewControllerFromNib];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneButtonTouched:(id)sender
{
    RequestResult result = RRFail;
    result = [MERCHANT requestStaffInMerchantCode:self.merchantCodeTextField.text
                                  completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              PCLog(@"Response %@", response);
                              
                              NSString *status = [response objectForKey:@"status"];
                              
                              NSString *merchantName = [[response objectForKey:@"merchant"] objectForKey:@"name"];
                              if([status isEqualToString:@"requested"]){
                                  [UIAlertView alertWithTitle:NSLocalizedString(@"Staff Join Requested", nil)
                                                      message:[NSString stringWithFormat:NSLocalizedString(@"Requested Successfully. The manager of %@ will handle your request. If accepted you can sign in %@", nil),
                                                               merchantName, merchantName]];
                                  
                                  if(self.isRootViewController){
                                      [self myRequestListBouttonTouched:nil];
                                  } else {
                                      [self.navigationController popViewControllerAnimated:YES];
                                  }
                                  
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while Joining merchant's staff", nil)];
                              }
                          }
                              break;
                          case ResponseErrorNoMerchant:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Merchant does not exist", nil)
                                                  message:NSLocalizedString(@"The code you entered may be invalid. Ask the manager of the merchant for the code if you don't know exactly", nil)];
                              break;
                          case ResponseErrorStaffJoinAlreadyRequested:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"You have reqeuested already", nil)
                                                  message:NSLocalizedString(@"Your request is in the middle of processing. To verrify the confirmation about joining, ask the manager of the merchant.", nil)];
                              break;
                          case ResponseErrorStaffJoinAlreadyJoined:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"You participate in already", nil)
                                                  message:NSLocalizedString(@"You cannot request to join again to the joined merchant.", nil)];
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while Joining merchant's staff", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(actionSheet.tag){
        case 1:
            if(actionSheet.destructiveButtonIndex == buttonIndex){ //Sign out
                [self signOut];
            }
            
            break;
        default:
            break;
    }
}

- (void)signOut
{
    RequestResult result = RRFail;
    result = [CRED requestSignOutCompletionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              [self dismissViewControllerAnimated:YES
                                                       completion:^{
                                                           [self uiLogoutProcess];
                                                       }];
                          }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while signing out", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)uiLogoutProcess
{
    [APP transitLogin];
    [CRED.userAccountItem resetKeychainItem];
    [NSUserDefaults standardUserDefaults].autoLogin = NO;
}

@end
