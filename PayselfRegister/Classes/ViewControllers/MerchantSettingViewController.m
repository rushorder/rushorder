//
//  MerchantSettingViewController.m
//  RushOrder
//
//  Created by Conan on 3/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MerchantSettingViewController.h"
#import "PhoneNumberFormatter.h"
#import "PCMerchantService.h"
#import "TableStatusManager.h"
#import "MenuManager.h"

@interface MerchantSettingViewController ()
{
    int _textFieldSemaphore;
    __strong PhoneNumberFormatter *_phoneNumberFormatter;
}
@property (weak, nonatomic) IBOutlet PCTextField *nameTextField;
@property (weak, nonatomic) IBOutlet PCTextField *ownerNameTextField;
@property (weak, nonatomic) IBOutlet PCTextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UIButton *locationButton;
@property (weak, nonatomic) IBOutlet PCTextField *addressTextField;
@property (weak, nonatomic) IBOutlet PCTextField *numberOfTableTextField;
@property (weak, nonatomic) IBOutlet NPStretchableButton *cancelButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *saveButton;
@property (weak, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@property (copy, nonatomic) NSString *geoAddress;
@property (strong, nonatomic) CLPlacemark *placemark;
@property (strong, nonatomic) CLLocation *location;
@property (weak, nonatomic) IBOutlet UILabel *promotionLabel;

@property (weak, nonatomic) IBOutlet NPToggleButton *sitDownButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *fastFoodButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *allowMenuButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *selfServiceButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *tableServiceButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *takeoutOnlyButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *acceptTakeoutButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *acceptPaymentButton;
@property (weak, nonatomic) IBOutlet NPToggleButton *acceptDeliveryButton;

@property (weak, nonatomic) IBOutlet UILabel *menuLinkLabel;
@property (weak, nonatomic) IBOutlet UIButton *menuLinkButton;
@property (weak, nonatomic) IBOutlet UIView *menuLinkLine;
@property (strong, nonatomic) Merchant *merchant;
@end

@implementation MerchantSettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _textFieldSemaphore = 0;
        _phoneNumberFormatter = [[PhoneNumberFormatter alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Merchant Information", nil);

    
    [self.scrollView setContentSizeWithBottomView:self.cancelButton];
    
    self.merchant = CRED.merchant;
    self.sitDownButton.manualSelection = YES;
    self.fastFoodButton.manualSelection = YES;

//    self.allowMenuButton.manualSelection = YES;
    self.acceptPaymentButton.manualSelection = YES;
    self.acceptTakeoutButton.manualSelection = YES;
    self.acceptDeliveryButton.manualSelection = YES;
    self.takeoutOnlyButton.manualSelection = YES;
    self.tableServiceButton.manualSelection = YES;
    self.selfServiceButton.manualSelection = YES;
    
    // Custom initialization
    if(self.merchantNo > 0){
        [self requestMerchant];
        self.cancelButton.buttonTitle = NSLocalizedString(@"Cancel", nil);
    } else {
        self.cancelButton.buttonTitle = NSLocalizedString(@"I will set later", nil);
    }
    
    if(self.inProcessSignUp){
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemCancel
                                                                            target:self
                                                                            action:@selector(cancelButtonTouched:)];
    }
    
    [self fillMerchantInfo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setNameTextField:nil];
    [self setNumberOfTableTextField:nil];
    [self setCancelButton:nil];
    [self setScrollView:nil];
    [self setOwnerNameTextField:nil];
    [self setPhoneNumberTextField:nil];
    [self setAddressTextField:nil];
    [self setLocationButton:nil];
    [self setPromotionLabel:nil];
    [self setSaveButton:nil];
    [self setSitDownButton:nil];
    [self setFastFoodButton:nil];
//    [self setAllowMenuButton:nil];
    [self setAcceptPaymentButton:nil];
    [super viewDidUnload];
}

- (void)requestMerchant
{
    RequestResult result = RRFail;
    result = [MERCHANT requestMyMerchantCompletionBlock:
              ^(BOOL isSuccess, Merchant *aMerchant, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      
                      if(aMerchant.merchantNo != 0){
                          
                          self.merchant = aMerchant;
                          CRED.merchant = aMerchant;
                          [self fillMerchantInfo];
                          
                      } else {
                          PCWarning(@"No merchant of mine");
//                          [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while retrieving merchant information", nil)];
                          
                      }
                      
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED) {
                      
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                      
                  } else {
                      
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                      
                  }
                  
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }    
}

- (void)fillMerchantInfo
{
    if(self.merchant == nil){
        self.menuLinkButton.hidden = YES;
        self.menuLinkLabel.hidden = YES;
        self.menuLinkLine.hidden = YES;
        return;
    }
    
    self.nameTextField.text = self.merchant.name;
    self.addressTextField.text = self.merchant.address;
    self.numberOfTableTextField.text = [NSString stringWithInteger:self.merchant.numberOfTables];
    
    self.ownerNameTextField.text = self.merchant.ownerName;
    self.phoneNumberTextField.text = self.merchant.phoneNumber;
    self.location = self.merchant.location;
    
    self.numberOfTableTextField.enabled = self.merchant.isTableBase || (self.merchant.servicedBy == ServicedByStaff);
    
    if(self.merchant.promotion != nil){
        self.promotionLabel.hidden = NO;
        self.promotionLabel.text = [NSLocalizedString(@"[PROMO]", nil) stringByAppendingString:self.merchant.promotion.title];
    } else {
        if([self.merchant.creditPolicyName length] > 0){
            
            self.promotionLabel.hidden = NO;
            self.promotionLabel.text = [NSLocalizedString(@"[EVENT]", nil) stringByAppendingString:self.merchant.creditPolicyName];
            
        } else {
            
            self.promotionLabel.hidden = YES;
            
            self.saveButton.y = self.promotionLabel.y + 10.0f;
            self.cancelButton.y = CGRectGetMaxY(self.saveButton.frame) + 7.0f;
        }
    }
    
    self.sitDownButton.selected = self.merchant.isTableBase;
    self.fastFoodButton.selected = !self.merchant.isTableBase && (self.merchant.servicedBy != ServicedByNone);

//    self.allowMenuButton.selected = self.merchant.isMenuOrder;
//    self.acceptPaymentButton.selected = self.merchant.isAblePayment;
    
//    self.allowMenuButton.enabled = self.merchant.isTableBase;
//    self.acceptPaymentButton.enabled = self.merchant.isTableBase;
    
    self.selfServiceButton.enabled = !self.merchant.isTableBase;
    self.tableServiceButton.enabled = !self.merchant.isTableBase;
    
    self.selfServiceButton.selected = (self.merchant.servicedBy == ServicedBySelf);
    self.tableServiceButton.selected = (self.merchant.servicedBy == ServicedByStaff);
    self.takeoutOnlyButton.selected = (self.merchant.servicedBy == ServicedByNone);
    
    self.acceptTakeoutButton.selected = self.merchant.isAbleTakeout;
    self.acceptDeliveryButton.selected = self.merchant.isAbleDelivery;
    
    self.geoAddress = self.merchant.geoAddress;
    
//    self.allowMenuButton.enabled = (!self.acceptTakeoutButton.selected && !self.acceptDeliveryButton.selected) && self.sitDownButton.selected;
//    self.acceptPaymentButton.enabled = (!self.acceptTakeoutButton.selected && !self.acceptDeliveryButton.selected) && self.sitDownButton.selected;
    
    if(self.acceptTakeoutButton.selected || self.acceptDeliveryButton.selected){
//        self.allowMenuButton.selected = YES;
//        self.acceptPaymentButton.selected = YES;
    }
}

- (IBAction)doneButtonTouched:(id)sender
{
    NSInteger tableNumber = 0;
    if([self.numberOfTableTextField.text length] > 0){
        tableNumber = [self.numberOfTableTextField.text integerValue];
    }
    
    if(self.sitDownButton.selected || self.tableServiceButton.selected){
        if(tableNumber == 0 || tableNumber > MAX_TABLES_NUMBER){
            [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Number of tables must be greater than 0 and less than or equal to %d", nil)
                                ,MAX_TABLES_NUMBER]];
            [self.numberOfTableTextField becomeFirstResponder];
            return;
        }
    }
    
    [self.view endEditing:YES];
    
    if(![VALID validate:self.nameTextField
                message:NSLocalizedString(@"Enter your merchant name", nil)]) return;

    if(self.location == nil){
        [UIAlertView alert:NSLocalizedString(@"You should specify the location of the merchant", nil)];
        return;
    }
    
    RequestResult result = RRFail;

    Merchant *merchantForUpdate = nil;
    
    if(self.merchant == nil){
        merchantForUpdate = [[Merchant alloc] init];
    } else {
        merchantForUpdate = [self.merchant copy];
    }
    
    
    merchantForUpdate.name = self.nameTextField.text;
    merchantForUpdate.ownerName = self.ownerNameTextField.text;
    merchantForUpdate.address = self.addressTextField.text;
    merchantForUpdate.geoAddress = self.geoAddress;
    
    merchantForUpdate.address1 = [self.placemark.addressDictionary objectForKey:@"Street"];
    // merchantForUpdate.address2 Address는 placemark로 찾을 수 없어 업데이트 불가.
    // !!!:만약 주소 텍스트를 수정한 경우도 문제 발생!!!!
    // TODO: 방법 찾아야 함.
    merchantForUpdate.city = [self.placemark.addressDictionary objectForKey:@"City"];
    merchantForUpdate.state = [self.placemark.addressDictionary objectForKey:@"State"];
    merchantForUpdate.zip = [self.placemark.addressDictionary objectForKey:@"ZIP"];
    merchantForUpdate.country = [self.placemark.addressDictionary objectForKey:@"Country"];
    
    merchantForUpdate.desc = nil;
    merchantForUpdate.location = self.location;
    merchantForUpdate.phoneNumber = self.phoneNumberTextField.text;
    merchantForUpdate.numberOfTables = [self.numberOfTableTextField.text integerValue];

    merchantForUpdate.tableBase = self.sitDownButton.selected;
//    merchantForUpdate.menuOrder = self.allowMenuButton.selected;
//    merchantForUpdate.ablePayment = self.acceptPaymentButton.selected;
    
    merchantForUpdate.ableTakeout = self.acceptTakeoutButton.selected;
    merchantForUpdate.ableDelivery = self.acceptDeliveryButton.selected;
    
    if(self.takeoutOnlyButton.selected){
        merchantForUpdate.servicedBy = ServicedByNone;
    } else {
        if(self.tableServiceButton.selected){
            merchantForUpdate.servicedBy = ServicedByStaff;
        } else {
            merchantForUpdate.servicedBy = ServicedBySelf;
        }
    }
    
    result = [MERCHANT requestNewMerchant:merchantForUpdate
                          completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              Merchant *merchant = nil;
                              
                              if(response.data != nil){
                                  merchant = [[Merchant alloc] initWithDictionary:response.data];
                              }
                              CRED.merchant = merchant;
                              
                              if(CRED.merchant.isMenuOrder){                                  
                                  [MENUPAN resetGraphWithMerchant:merchant];
                              }
                              
                              if(CRED.merchant == nil){
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while setting merchant information", nil)];
                                  
                                  [self requestMerchant];
                              } else {
                                  
                                  BOOL shouldDismiss = YES;
                                  if(MENUPAN.isMenuFetched){
                                      if(merchant.isMenuOrder){
                                          if([MENUPAN.activeMenuList count] == 0){
                                              [UIAlertView askWithTitle:NSLocalizedString(@"Please Fill Out Your Menu",nil)
                                                                message:NSLocalizedString(@"Your restaurant is set to accept Order by customer. Ordering needs menu. Do you want to make menu now?", nil)
                                                               delegate:self
                                                                    tag:104];
                                              shouldDismiss = NO;
                                          }
                                      }
                                  } else {
                                      [MENUPAN requestMenus];
                                  }
                                  
                                  if(shouldDismiss){
                                      [self dismissViewControllerAnimated:YES
                                                               completion:^(){
                                                                   if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                                                                       if(CRED.merchant.isTableBase){
                                                                           [APP transitDashboard];
                                                                       } else {
                                                                           [APP transitFastFood];
                                                                       }
                                                                   } else {
                                                                       [APP transitTableStatus];
                                                                   }
                                                               }];
                                  }
                                  
                                  [[NSNotificationCenter defaultCenter] postNotificationName:SignedInNotification
                                                                                      object:self
                                                                                    userInfo:nil];
                              }
                          }
                              break;
                          case ResponseErrorIncompleteOrderExists:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"There are some active orders", nil)
                                                  message:NSLocalizedString(@"You should complete or reset all active orders before changing restaurant type", nil)];
                              [TABLE reloadTableStatus];
                              break;
                          case ResponseErrorActiveOrderExists:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"There are some active orders", nil)
                                                  message:NSLocalizedString(@"You should complete or reset all active orders before changing number of tables", nil)];
                              [TABLE reloadTableStatus];
                              break;
                          case ResponseErrorInvalidParameters:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                          }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while setting merchant information", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (IBAction)cancelButtonTouched:(id)sender
{
    if([self isRootViewController] || self.inProcessSignUp){
        [self dismissViewControllerAnimated:YES
                                 completion:^(){
                                     [APP transitIntroduction];
                                 }];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)mapButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    MerchantLocationViewController *viewController = [MerchantLocationViewController viewControllerFromNib];
    viewController.initialLocation = self.location;
    viewController.delegate = self;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}



- (void)mapViewController:(MerchantLocationViewController *)mapViewController
      didSelectPlacemarks:(NSArray *)placemarks
            pinAnnotation:(PinAnnotation *)pinAnnotation
{
    if([placemarks count] > 0){
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        self.placemark = placemark;
        self.location = [[CLLocation alloc] initWithLatitude:pinAnnotation.coordinate.latitude
                                                   longitude:pinAnnotation.coordinate.longitude];
        self.locationButton.buttonTitle = placemark.address;
        self.geoAddress = placemark.address;
        if([self.addressTextField.text length] == 0){
            self.addressTextField.text = placemark.address;
        } else {
            [UIAlertView askWithTitle:NSLocalizedString(@"Address field changed", nil)
                              message:NSLocalizedString(@"Do you want to modify address field with applied address on the map?", nil)
                             delegate:self
                                  tag:101];
        }
    } else {
        self.location = nil;
        self.locationButton.buttonTitle = NSLocalizedString(@"Merchant Location", nil);
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 101:
            if(buttonIndex == AlertButtonIndexYES){
                self.addressTextField.text = self.placemark.address;
            }
            break;
        case 102:
            // Pickup service
//            self.allowMenuButton.selected = YES;
//            self.acceptPaymentButton.selected = YES;
//            self.allowMenuButton.enabled = NO;
//            self.acceptPaymentButton.enabled = NO;
            
            break;
        case 103:
            // Table base
//            self.allowMenuButton.selected = YES;            
            break;
        case 104:
            if(buttonIndex == AlertButtonIndexYES){
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:MERCHANT_MENU_LIST_URL, CRED.userInfo.authToken]]];
            }
            // If popover close, this delegate method make crash.
            // So dismiss delayed
            [self dismissViewControllerAnimated:YES
                                     completion:^(){
                                         
                                         if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                                             if(CRED.merchant.isTableBase){
                                                 [APP transitDashboard];
                                             } else {
                                                 [APP transitFastFood];
                                             }
                                         } else {
                                             [APP transitTableStatus];
                                         }
                                     }];
            break;
        default:
            
            break;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == self.numberOfTableTextField){
        [self mapButtonTouched:textField];
    } else if(textField == self.addressTextField){
        [textField resignFirstResponder];
    } else {
        if ([textField isKindOfClass:[PCTextField class]]){
            [[(PCTextField *)textField nextField] becomeFirstResponder];
        }
    }
    return YES;
}

- (void)setButtonsEnable:(RestaurantType)restaurantType
           isAbleTakeout:(BOOL)isAbleTakeout
          isAbleDelivery:(BOOL)isAbleDelivery
{
    // Enabling
    if(restaurantType == RestaurantTypeSitdown){
        self.acceptDeliveryButton.enabled = YES;
        self.acceptTakeoutButton.enabled = YES;
        self.tableServiceButton.enabled = NO;
    } else if(restaurantType == RestaurantTypeFastfood){
        self.acceptDeliveryButton.enabled = YES;
        self.acceptTakeoutButton.enabled = YES;
        self.tableServiceButton.enabled = YES;
    } else { //Takeout only
        self.acceptDeliveryButton.enabled = YES;
        self.acceptTakeoutButton.enabled = NO;
        self.tableServiceButton.enabled = NO;
    }
    
    // Selection
    self.sitDownButton.selected = (restaurantType == RestaurantTypeSitdown);
    self.fastFoodButton.selected = (restaurantType == RestaurantTypeFastfood);
    self.takeoutOnlyButton.selected = (restaurantType == RestaurantTypeTakeoutOnly);
    
    if(restaurantType == RestaurantTypeSitdown){
        self.selfServiceButton.selected = NO;
        self.tableServiceButton.selected = YES;
    } else if(restaurantType == RestaurantTypeFastfood){

    } else { //Takeout only
        self.tableServiceButton.selected = NO;
        self.acceptTakeoutButton.selected = YES;
    }
    
    self.numberOfTableTextField.enabled = self.tableServiceButton.selected;
}

- (IBAction)sitDownButtonTouched:(NPToggleButton *)sender
{
    if(!sender.selected){

        if(!CRED.merchant.isTableBase){
            if([TABLE.activeTableList count] > 0){
                [UIAlertView alertWithTitle:NSLocalizedString(@"There Are Some Active Orders", nil)
                                    message:NSLocalizedString(@"You should complete or reset all active orders before changing restaurant type", nil)];
                
                [TABLE reloadTableStatus];
                return;
            }
        }
        
        [self setButtonsEnable:RestaurantTypeSitdown
                 isAbleTakeout:self.acceptTakeoutButton.selected
                isAbleDelivery:self.acceptDeliveryButton.selected];
        
        
//        if(!self.allowMenuButton.selected && !self.acceptPaymentButton.selected){
//            [UIAlertView alertWithTitle:NSLocalizedString(@"You should select at least one acceptance option.", nil)
//                                message:NSLocalizedString(@"Accept Order option will be checked.", nil)
//                               delegate:self
//                                    tag:103];
//        }
    }
}

- (IBAction)fastFoodButtonTouched:(NPToggleButton *)sender
{
//    self.merchant.tableBase = !sender.selected;
    if(!sender.selected){
        
        if(CRED.merchant.isTableBase){
            if([TABLE.activeTableList count] > 0){
                [UIAlertView alertWithTitle:NSLocalizedString(@"There are some active orders", nil)
                                    message:NSLocalizedString(@"You should complete or reset all active orders before changing restaurant type", nil)];
                
                [TABLE reloadTableStatus];
                return;
            }
        }
        
        [self setButtonsEnable:RestaurantTypeFastfood
                 isAbleTakeout:self.acceptTakeoutButton.selected
                isAbleDelivery:self.acceptDeliveryButton.selected];
        
//        if(!self.allowMenuButton.selected){
//            [UIAlertView alertWithTitle:NSLocalizedString(@"Fast Food or Cafe type requires Menu (for ordering)", nil)
//                                message:NSLocalizedString(@"Accept Order option will be checked.", nil)
//                               delegate:self
//                                    tag:102];
//        } else if(!self.acceptPaymentButton.selected){
//            [UIAlertView alertWithTitle:NSLocalizedString(@"Fast Food or Cafe type requires Acceptance of Payment", nil)
//                                message:NSLocalizedString(@"Accept Payment option will be checked.", nil)
//                               delegate:self
//                                    tag:102];
//        } else {
//            
//        }
    }
}

- (IBAction)takeoutOnlyButtonTouched:(NPToggleButton *)sender
{
    if(!sender.selected){
        
        if(CRED.merchant.isTableBase){
            if([TABLE.activeTableList count] > 0){
                [UIAlertView alertWithTitle:NSLocalizedString(@"There are some active orders", nil)
                                    message:NSLocalizedString(@"You should complete or reset all active orders before changing restaurant type", nil)];
                
                [TABLE reloadTableStatus];
                return;
            }
        }
        
        [self setButtonsEnable:RestaurantTypeTakeoutOnly
                 isAbleTakeout:self.acceptTakeoutButton.selected
                isAbleDelivery:self.acceptDeliveryButton.selected];
        
//        if(!self.allowMenuButton.selected){
//            [UIAlertView alertWithTitle:NSLocalizedString(@"Takeout only type requires Menu (for ordering)", nil)
//                                message:NSLocalizedString(@"Accept Order option will be checked.", nil)
//                               delegate:self
//                                    tag:102];
//        } else if(!self.acceptPaymentButton.selected){
//            [UIAlertView alertWithTitle:NSLocalizedString(@"Takeout only type requires Acceptance of Payment", nil)
//                                message:NSLocalizedString(@"Accept Payment option will be checked.", nil)
//                               delegate:self
//                                    tag:102];
//        } else {
//            
//        }
    }
}

- (IBAction)allowMenuButtonTouched:(NPToggleButton *)sender
{
    if(self.acceptTakeoutButton.selected && sender.selected){
        [UIAlertView alertWithTitle:NSLocalizedString(@"You should select Accept Order option", nil)
                            message:NSLocalizedString(@"Takeout Order needs Accept Order option", nil)];
        return;
    }
    
    if(self.acceptDeliveryButton.selected && sender.selected){
        [UIAlertView alertWithTitle:NSLocalizedString(@"You should select Accept Order option", nil)
                            message:NSLocalizedString(@"Delivery Order needs Accept Order option", nil)];
        return;
    }
    
    if(sender.selected){
        if(!self.acceptPaymentButton.selected){
            [UIAlertView alertWithTitle:NSLocalizedString(@"You should select at least one acceptance option.", nil)
                                message:NSLocalizedString(@"If you want not to use Menu ordering, you should select Accept Payment option instead.", nil)];
            return;
        }
    }
    sender.selected = !sender.selected;
}

- (IBAction)acceptPaymentButtonTouched:(NPToggleButton *)sender
{
    if(self.acceptTakeoutButton.selected && sender.selected){
        [UIAlertView alertWithTitle:NSLocalizedString(@"You should select Accept Payment option", nil)
                            message:NSLocalizedString(@"Takeout Order needs Accept Payment option", nil)];
        return;
    }
    
    if(self.acceptDeliveryButton.selected && sender.selected){
        [UIAlertView alertWithTitle:NSLocalizedString(@"You should select Accept Payment option", nil)
                            message:NSLocalizedString(@"Delivery Order needs Accept Payment option", nil)];
        return;
    }
    
    if(sender.selected){
//        if(!self.allowMenuButton.selected){
//            [UIAlertView alertWithTitle:NSLocalizedString(@"You should select at least one acceptance option.", nil)
//                                message:NSLocalizedString(@"If you want not to use Payment, you should select Accept Order option instead.", nil)];
//            return;
//        }
    }
    sender.selected = !sender.selected;
}

- (IBAction)servicedByButtonTouched:(NPToggleButton *)sender
{
    sender.selected = !sender.selected;
    
    if(sender == self.selfServiceButton){
        self.tableServiceButton.selected = !sender.selected;
    } else if(sender == self.tableServiceButton){
        self.selfServiceButton.selected = !sender.selected;
    }
    
    self.numberOfTableTextField.enabled = self.tableServiceButton.selected;
}

- (IBAction)acceptTakeoutButtonTouched:(NPToggleButton *)sender
{
    sender.selected = !sender.selected;
    
//    self.allowMenuButton.enabled = !sender.selected && self.sitDownButton.selected;
//    self.acceptPaymentButton.enabled = !sender.selected && self.sitDownButton.selected;
    
    if(sender.selected){
//        self.allowMenuButton.selected = YES;
//        self.acceptPaymentButton.selected = YES;
    }
}

- (IBAction)acceptDeliveryButtonTouched:(NPToggleButton *)sender
{
    sender.selected = !sender.selected;
    
//    self.allowMenuButton.enabled = !sender.selected && self.sitDownButton.selected;
//    self.acceptPaymentButton.enabled = !sender.selected && self.sitDownButton.selected;
    
    if(sender.selected){
//        self.allowMenuButton.selected = YES;
//        self.acceptPaymentButton.selected = YES;
    }
}


- (IBAction)openURLButtonTouched:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:MERCHANT_MENU_LIST_URL, CRED.userInfo.authToken]]];
}

- (IBAction)phoneNumberValueChanged:(UITextField *)sender
{
    if(_textFieldSemaphore) return;
    
    _textFieldSemaphore = 1;
    
    NSString *locale = [[NSLocale currentLocale] localeIdentifier];
    sender.text = [_phoneNumberFormatter format:sender.text
                                     withLocale:locale];
    _textFieldSemaphore = 0;
}

@end