//
//  AddressTableViewController.h
//  RushOrder
//
//  Created by Conan on 8/19/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddressTableViewControllerDelegate;
@interface AddressTableViewController : UITableViewController

@property (weak, nonatomic) IBOutlet id<AddressTableViewControllerDelegate> delegate;
@end

@protocol AddressTableViewControllerDelegate<NSObject>
- (void)addressTableViewControllerDidSelectAddress:(NSString *)address;
@end
