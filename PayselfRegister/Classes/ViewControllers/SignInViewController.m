//
//  SignInViewController.m
//  RushOrder
//
//  Created by Conan on 3/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "SignInViewController.h"
#import "SignUpViewController.h"
#import "MenuManager.h"
#import "ForgotPasswordMerchantViewController.h"

@interface SignInViewController ()

@property (weak, nonatomic) IBOutlet PCTextField *idTextField;
@property (weak, nonatomic) IBOutlet PCTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UISwitch *autoLoginSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *testDriveSwitch;
@property (weak, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

@end

@implementation SignInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.idTextField.text = [NSUserDefaults standardUserDefaults].lastTriedUserId;
    self.title = NSLocalizedString(@"Sign In", nil);
    
    if(self.scrollView != nil){
        [self.scrollView setContentSizeWithBottomView:self.bottomView];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (IBAction)signInButtonTouched:(id)sender
{
    if(![VALID validate:self.idTextField
                message:NSLocalizedString(@"Enter your ID (email address)", nil)]){
        return;
    }
    
    if(![VALID validate:self.passwordTextField
                message:NSLocalizedString(@"Enter your password", nil)]){
        return;
    }
    
    [NSUserDefaults standardUserDefaults].lastTriedUserId = self.idTextField.text;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *deviceToken = userDefault.deviceToken;
    
    NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
    RequestResult result = RRFail;
    result = [CRED requestSignIn:self.idTextField.text
                        password:self.passwordTextField.text
                     deviceToken:deviceToken
                         version:appVersion
                 completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && ((HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES) || statusCode == HTTP_STATUS_UNAUTHORIZED)){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              // Auto sign in
                              CRED.merchant = [response objectForKey:@"merchant"];
                              
                              [NSUserDefaults standardUserDefaults].autoLogin = self.autoLoginSwitch.on;
                              
                              if(self.autoLoginSwitch.on){
                                  [CRED.userAccountItem setObject:self.idTextField.text
                                                           forKey:(__bridge id)(kSecAttrAccount)];
                                  [CRED.userAccountItem setObject:self.passwordTextField.text
                                                           forKey:(__bridge id)(kSecValueData)];
                              } else {
                                  [CRED.userAccountItem resetKeychainItem];
                              }
                              //////////////////////////////////////////////////
                              
                              self.passwordTextField.text = nil;
                              
                              [self dismissViewControllerAnimated:YES
                                                       completion:^(){
                                                           if(CRED.merchant){
                                                               
                                                               if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                                                                   if(CRED.merchant.isTableBase){
                                                                       [APP transitDashboard];
                                                                   } else {
                                                                       [APP transitFastFood];
                                                                   }
                                                               } else {
                                                                   [APP transitTableStatus];
                                                               }
                                                               
                                                           } else {
                                                               if([CRED.merchants count] > 1){
                                                                   // Show select merchant
                                                                   [APP transitSelectMerchant];
                                                               } else {
                                                                   switch(CRED.userInfo.role){
                                                                       case UserRoleOwner:
                                                                           [APP transitIntroduction];
                                                                           break;
                                                                       case UserRoleStaff:
                                                                           [APP transitStaffJoin];
                                                                           break;
                                                                       case UserRoleUnknown:
                                                                       default:
                                                                           
                                                                           break;
                                                                   }
                                                               }
                                                           }
                                                       }];
                              break;
                          case ResponseErrorIncorrect:
                          case ResponseErrorIdIncorrect:
                              [NSUserDefaults standardUserDefaults].autoLogin = NO;
                              [UIAlertView alertWithTitle:NSLocalizedString(@"ID or Password is not correct", nil)
                                                  message:NSLocalizedString(@"Check your account information and try again", nil)];
                              break;
                          case ResponseErrorNetworkError:
                              [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                                  object:nil];
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                              [NSUserDefaults standardUserDefaults].autoLogin = NO;
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while signing in", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRNotReachable:
            [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                object:nil];
            break;
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (IBAction)signUpButtonTouched:(id)sender
{    
    SignUpViewController *viewController = [SignUpViewController viewControllerFromNib];
    viewController.email = self.idTextField.text;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (IBAction)testSwitchValueChanged:(UISwitch *)sender
{
    
}

- (IBAction)forgotPasswordButtonTouched:(id)sender
{
    ForgotPasswordMerchantViewController *viewController = [ForgotPasswordMerchantViewController viewControllerFromNib];
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == self.passwordTextField){
        [self.passwordTextField resignFirstResponder];
    } else {
        if ([textField isKindOfClass:[PCTextField class]]){
            [[(PCTextField *)textField nextField] becomeFirstResponder];
        }
    }
    return YES;
}
@end
