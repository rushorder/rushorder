//
//  CuisineTableViewCell.m
//  RushOrder
//
//  Created by Conan on 10/27/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "CuisineTableViewCell.h"

@interface CuisineTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@end

@implementation CuisineTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)drawData
{
    self.nameLabel.text = self.cuisine.name;
    self.accessoryType = self.cuisine.isSelected ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
}


@end
