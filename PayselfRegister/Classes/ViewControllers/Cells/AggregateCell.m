//
//  AggregateCell.m
//  RushOrder
//
//  Created by Conan on 3/11/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "AggregateCell.h"

@interface AggregateCell()

@property (weak, nonatomic) IBOutlet UILabel *orderNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *payCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxesLabel;
@property (weak, nonatomic) IBOutlet UILabel *tipsLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *paidAmountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *shadowView;
@property (weak, nonatomic) IBOutlet UILabel *rewardAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *rewardTitleLabel;

@end

@implementation AggregateCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)fillCell
{
    if(self.order.tableNumber == nil){
        self.orderNumberLabel.text = [NSString stringWithFormat:@"Order Number:%lld / Pickup Order",
                                      self.order.orderNo];
    } else {
    
        if(self.order.orderNo != 0){
            self.orderNumberLabel.text = [NSString stringWithFormat:@"Order Number:%lld / Table Number:%@",
                                          self.order.orderNo,
                                          self.order.tableNumber];
        } else {
            self.orderNumberLabel.text = [NSString stringWithFormat:@"Table Number:%@",
                                          self.order.tableNumber];
        }
    }
    
    if(self.order.orderDate != nil){
        self.payDateLabel.text = [self.order.orderDate secondsString];
    }
    
    self.payCountLabel.text = [NSString stringWithFormat:@"Paid With %d Cards",
                               0];
    
    self.subTotalLabel.text = [[NSNumber numberWithCurrency:self.order.subTotal] currencyString];
    self.taxesLabel.text = [[NSNumber numberWithCurrency:self.order.taxes] currencyString];
    self.tipsLabel.text = [[NSNumber numberWithCurrency:self.order.tips] currencyString];
    self.totalLabel.text = [[NSNumber numberWithCurrency:self.order.total] currencyString];
    self.paidAmountLabel.text = [[NSNumber numberWithCurrency:self.order.netPayAmounts] currencyString];
    
    self.rewardAmountLabel.text = [[NSNumber numberWithCurrency:self.order.rewardedAmount] pointString];
//    
//    if(self.order.rewardedAmount > 0){
//        
//    }
}

- (void)sumFormat
{
    [self sumFormat:NO];
}

- (void)sumFormat:(BOOL)isTotal
{
    if(isTotal){
        self.orderNumberLabel.text = NSLocalizedString(@"Total Sum", nil);
        self.backgroundColor = [UIColor darkGrayColor];
    } else {
        self.orderNumberLabel.text = NSLocalizedString(@"Group Sum", nil);
        self.backgroundColor = [UIColor lightGrayColor];
    }
    
//    self.payDateLabel.hidden = YES;
    
    for(UILabel *aLabel in self.contentView.subviews){
        if([aLabel isKindOfClass:[UILabel class]]){
            aLabel.textColor = [UIColor whiteColor];
            aLabel.font = [UIFont boldSystemFontOfSize:aLabel.font.pointSize];
        }
    }
}

@end