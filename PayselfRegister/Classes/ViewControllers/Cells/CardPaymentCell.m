//
//  CardPaymentCell.m
//  RushOrder
//
//  Created by Conan on 3/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "CardPaymentCell.h"

@interface CardPaymentCell()

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *payAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *creditAmountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *refundImageView;
@property (weak, nonatomic) IBOutlet UILabel *refundDateLabel;
@property (weak, nonatomic) IBOutlet UIButton *printButton;
@end

@implementation CardPaymentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)fillPayment
{
    self.titleLabel.text = self.payment.formattedCardNumber;
    self.subTitleLabel.text = self.payment.payDate.secondsString;
    
    if(self.payment.creditAmount > 0 || self.payment.rewardedAmount > 0){
        
        NSMutableString *extraString = [NSMutableString string];
        
        [extraString appendFormat:@"Card:%@  ",[[NSNumber numberWithCurrency:self.payment.netAmount] currencyString]];
        
        if(self.payment.creditAmount > 0){
            [extraString appendFormat:@"Point:%@  ",[[NSNumber numberWithCurrency:self.payment.creditAmount] pointString]];
        }
        
        if(self.payment.rewardedAmount > 0){
            [extraString appendFormat:@"Reward:%@  ",[[NSNumber numberWithCurrency:self.payment.rewardedAmount] pointString]];
        }
        
        self.creditAmountLabel.text = extraString;
        self.creditAmountLabel.hidden = NO;
    } else {
        self.creditAmountLabel.hidden = YES;
    }
    
    
    NSString *paymentString = nil;
    
    if(self.payment.payAmount != 0){
        paymentString = self.payment.payAmountString;
    } else {
        paymentString = self.payment.actualAmountString;
    }
    
    if(self.payment.tipAmount == 0){
        self.payAmountLabel.text = [NSString stringWithFormat:@"%@ Paid",
                                    paymentString];
    } else {
        
        self.payAmountLabel.text = [NSString stringWithFormat:@"%@ Paid (Incl. Tip %@)",
                                    paymentString,
                                    self.payment.tipAmountString];
    }

    if(self.payment.netAmount > 0){
        self.thumbnailImageView.image = self.payment.cardLogo;
    } else {
        self.thumbnailImageView.image = [UIImage imageNamed:@"icon_card"];
    }
    
    self.refundDateLabel.text = self.payment.refundedDate.secondsString;
    
    [self schemeToRefund:(self.payment.status == PaymentStatusRefunded)];
}

- (void)schemeToRefund:(BOOL)isRefunded
{
    self.refundDateLabel.hidden = !isRefunded;
    self.refundImageView.hidden = !isRefunded;
    
    self.titleLabel.textColor = !isRefunded ? [UIColor darkGrayColor] : [UIColor lightGrayColor];
    self.subTitleLabel.textColor = !isRefunded ? [UIColor darkGrayColor] : [UIColor lightGrayColor];
    self.payAmountLabel.textColor = !isRefunded ? [UIColor colorWithR:67 G:138 B:207] : [UIColor lightGrayColor];
    self.creditAmountLabel.textColor = !isRefunded ? [UIColor lightGrayColor] : [UIColor lightGrayColor];
}

- (IBAction)printButtonTouched:(UIButton *)sender
{
    self.printButton.enabled = NO;
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Printing...", nil) maskType:SVProgressHUDMaskTypeClear];
    
    [self performSelector:@selector(startPrinting)
               withObject:self
               afterDelay:0.01f];
}

- (void)startPrinting
{
    UITableView *tableView = self.tableView;
    
    NSIndexPath *myIndexPath = [tableView indexPathForCell:self];
    if([self.delegate respondsToSelector:@selector(tableView:didTouchPrintButtonAtIndexPath:)]){
        [self.delegate tableView:tableView didTouchPrintButtonAtIndexPath:myIndexPath];
    }
    self.printButton.enabled = YES;
    [SVProgressHUD dismiss];
}

@end
