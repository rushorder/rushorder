//
//  AggregateCell.h
//  RushOrder
//
//  Created by Conan on 3/11/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"

@interface AggregateCell : UITableViewCell


@property (strong, nonatomic) Order *order;
@property (weak, nonatomic) IBOutlet UILabel *payDateLabel;


- (void)fillCell;
- (void)sumFormat;
- (void)sumFormat:(BOOL)isTotal;
@end
