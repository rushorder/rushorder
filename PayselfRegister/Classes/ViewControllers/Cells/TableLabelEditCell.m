//
//  TableLabelEditCell.m
//  RushOrder
//
//  Created by Conan on 3/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "TableLabelEditCell.h"

@interface TableLabelEditCell()
@property (nonatomic, strong) NSIndexPath *indexPath;
@end

@implementation TableLabelEditCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([self.delegate respondsToSelector:@selector(tableLabelEditCell:textFieldShouldReturn:indexPath:)]){
        return [self.delegate tableLabelEditCell:self
                           textFieldShouldReturn:textField
                                       indexPath:self.indexPath];
    }
    return YES;
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if([self.delegate respondsToSelector:@selector(tableLabelEditCell:textFieldShouldClear:indexPath:)]){
        return [self.delegate tableLabelEditCell:self
                            textFieldShouldClear:textField
                                       indexPath:self.indexPath];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if([self.delegate respondsToSelector:@selector(tableLabelEditCell:textFieldDidEndEditing:indexPath:)]){
        [self.delegate tableLabelEditCell:self
                   textFieldDidEndEditing:textField
                                indexPath:self.indexPath];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField;
{
    UITableView *tableView = self.tableView;

    self.indexPath = [tableView indexPathForCell:self];
}
@end
