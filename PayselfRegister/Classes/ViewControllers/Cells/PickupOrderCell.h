//
//  PickupOrderCell.h
//  RushOrder
//
//  Created by Conan on 3/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Order.h"
#import "NPProgressView.h"

@interface PickupOrderCell : UITableViewCell

@property (strong, nonatomic) Order *order;

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet NPProgressView *payProgressBar;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *takeoutLabel;
@property (weak, nonatomic) IBOutlet UILabel *ticketNoLabel;

- (void)fillOrderInfo;
@end