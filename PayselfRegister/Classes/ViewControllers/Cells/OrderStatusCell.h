//
//  OrderStatusCell.h
//  RushOrder
//
//  Created by Conan on 3/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableInfo.h"
#import "NPProgressView.h"

@interface OrderStatusCell : UITableViewCell

@property (strong, nonatomic) Order *order;
@property (strong, nonatomic) Cart *cart;

@property (weak, nonatomic) IBOutlet UILabel *tableNumberTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *tableNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet NPProgressView *payProgressBar;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderNoTitleLabel;

- (void)fillOrderInfo;
@end
