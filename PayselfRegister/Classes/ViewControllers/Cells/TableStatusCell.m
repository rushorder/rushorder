//
//  TableStatusCell.m
//  RushOrder
//
//  Created by Conan on 3/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "TableStatusCell.h"
#import "FormatterManager.h"
#import "Cart.h"

@implementation TableStatusCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)fillTableInfo
{
    self.tableNumberLabel.text = self.tableInfo.tableNumber;
    
    if(self.tableInfo.order != nil){
        self.orderNoLabel.text = [NSString stringWithLongLong:self.tableInfo.order.orderNo];
        self.orderNoLabel.hidden = NO;
        self.orderNoTitleLabel.hidden = NO;
    } else {
        self.orderNoLabel.hidden = YES;
        self.orderNoTitleLabel.hidden = YES;
    }

    if(self.tableInfo.cart != nil && self.tableInfo.order == nil){
        if(self.tableInfo.cart.status == CartStatusOrdering){
            self.statusLabel.text = @"Ordering";
        } else if(self.tableInfo.cart.status == CartStatusUnknown){
            self.statusLabel.text = @"New Order";
        } else {
            self.statusLabel.text = @"New Order";
        }
    } else {
        self.statusLabel.text = self.tableInfo.statusString;
    }
    
    NSString *totalAmountString = self.tableInfo.order.totalString;
    
    NSString *payAmountString = [[NSNumber numberWithCurrency:self.tableInfo.order.netPayAmounts + self.tableInfo.order.discountedAmount] currencyString];
    
    if(self.tableInfo.cart != nil && self.tableInfo.order == nil){
        self.amountLabel.text = self.tableInfo.cart.subtotalAmountString;
    } else if(self.tableInfo.status == TableStatusPartialPaid ){
        self.amountLabel.text = [NSString stringWithFormat:@"%@ / %@",
                                 payAmountString,
                                 totalAmountString];
    } else {
        self.amountLabel.text = self.tableInfo.order.totalString;
    }
    
    self.payProgressBar.progress = self.tableInfo.order.payProgress;
    
    self.timeLabel.text = self.tableInfo.actionDate.secondsString;
    [self setColorScheme:self.tableInfo.statusColor];
}


- (void)setColorScheme:(UIColor *)color
{
    self.tableNumberLabel.textColor = color;
    self.statusLabel.textColor = color;
    self.amountLabel.textColor = color;
    
}

@end



