//
//  CuisineTableViewCell.h
//  RushOrder
//
//  Created by Conan on 10/27/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cuisine.h"

@interface CuisineTableViewCell : UITableViewCell

@property (strong, nonatomic) Cuisine *cuisine;

- (void)drawData;

@end