//
//  PickupOrderCell.m
//  RushOrder
//
//  Created by Conan on 3/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PickupOrderCell.h"
#import "FormatterManager.h"

@interface PickupOrderCell()

@property (weak, nonatomic) IBOutlet UIImageView *takeoutIcon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *takeoutIconWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *takeoutIconSpace;
@end

@implementation PickupOrderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)fillOrderInfo
{
    self.takeoutLabel.hidden = YES;
    self.ticketNoLabel.hidden = YES;
    
    if(self.order != nil){
        self.orderNoLabel.hidden = NO;
        
        if([self.order.tableNumber length] > 0){
            self.orderNoLabel.text = self.order.tableNumber;
            self.ticketNoLabel.text = [NSString stringWithFormat:@"Ticket No. %lld",self.order.orderNo];
            self.ticketNoLabel.hidden = NO;
        } else {
            self.orderNoLabel.text = [NSString stringWithLongLong:self.order.orderNo];
        }
    } else {
        self.orderNoLabel.hidden = YES;
    }
    
    if(self.order != nil){
//        BOOL needIcon = (self.order.isTakeout && self.order.isDelivery);
//        self.takeoutLabel.hidden = !needIcon;
//        self.takeoutIcon.hidden = !needIcon;
//        self.takeoutIconWidth.constant = needIcon ? 14.0f : 0.0f;
//        self.takeoutIconSpace.constant = needIcon ? 4.0f : 0.0f;
        if(self.order.orderType == OrderTypeTakeout){
            self.takeoutIcon.image = [UIImage imageNamed:@"icon_takeout_black"];
        } else if(self.order.orderType == OrderTypeDelivery){
            self.takeoutIcon.image = [UIImage imageNamed:@"icon_delivery_black"];
        } else {
            self.takeoutIcon.image = [UIImage imageNamed:@"icon_table_black"];
        }
        self.statusLabel.text = self.order.hrStatusPickupString;
    }
    
//    NSString *totalAmountString = [[NSNumber numberWithCurrency:self.order.total] currencyString];
//    NSString *payAmountString = [[NSNumber numberWithCurrency:self.order.netPayAmounts + + self.order.discountedAmount] currencyString];
//    
//    self.amountLabel.text = [NSString stringWithFormat:@"%@ / %@",
//                             payAmountString,
//                             totalAmountString];
    
    self.amountLabel.text = self.order.totalString;
    
    self.payProgressBar.progress = self.order.payProgress;
    
    self.timeLabel.text = self.order.lastAccessedTime.secondsString;
    [self setColorScheme:self.order.statusColor];
}


- (void)setColorScheme:(UIColor *)color
{
    self.statusLabel.textColor = color;
    self.amountLabel.textColor = color;
    
}

@end



