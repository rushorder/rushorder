//
//  CardPaymentCell.h
//  RushOrder
//
//  Created by Conan on 3/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Payment.h"

@protocol CardPaymentCellDelegate;

@interface CardPaymentCell : UITableViewCell

@property (weak, nonatomic) id<CardPaymentCellDelegate> delegate;
@property (strong, nonatomic) Payment *payment;

- (void)fillPayment;
@end


@protocol CardPaymentCellDelegate <UITableViewDelegate>
- (void)tableView:(UITableView *)tableView didTouchPrintButtonAtIndexPath:(NSIndexPath *)indexPath;
@end