//
//  OrderStatusCell.m
//  RushOrder
//
//  Created by Conan on 3/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "OrderStatusCell.h"
#import "FormatterManager.h"

@implementation OrderStatusCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)fillOrderInfo
{
    if(self.order.tableNumber == nil){
        self.tableNumberLabel.hidden = YES;
        self.tableNumberTitleLabel.hidden = YES;
    } else {
        if(self.order.orderType == OrderTypeCart){
            self.tableNumberTitleLabel.text = NSLocalizedString(@"Table#", nil);
        } else {
            self.tableNumberTitleLabel.text = NSLocalizedString(@"Table#", nil);
        }
        self.tableNumberLabel.hidden = NO;
        self.tableNumberTitleLabel.hidden = NO;
        self.tableNumberLabel.text = self.order.tableNumber;
    }
    
    if(self.order != nil){
        self.orderNoLabel.text = [NSString stringWithLongLong:self.order.orderNo];
        self.orderNoLabel.hidden = NO;
        self.orderNoTitleLabel.hidden = NO;
    } else {
        self.orderNoLabel.hidden = YES;
        self.orderNoTitleLabel.hidden = YES;
    }
    
    self.statusLabel.text = self.order.completeReason;
    
//    NSString *totalAmountString = [[NSNumber numberWithCurrency:self.order.total] currencyString];
//    
//    NSString *payAmountString = [[NSNumber numberWithCurrency:self.order.netPayAmounts + self.order.discountedAmount] currencyString];
//    
//    self.amountLabel.text = [NSString stringWithFormat:@"%@ / %@",
//                             payAmountString,
//                             totalAmountString];
    
    self.amountLabel.text = self.order.totalString;
    
    self.payProgressBar.progress = self.order.payProgress;
    
    self.timeLabel.text = self.order.updatedDate.secondsString;
    [self setColorScheme:[UIColor darkGrayColor]];
}

- (void)setColorScheme:(UIColor *)color
{
    self.orderNoLabel.textColor = color;
    self.statusLabel.textColor = color;
    self.amountLabel.textColor = color;
    
}

@end



