//
//  TableLabelEditCell.h
//  RushOrder
//
//  Created by Conan on 3/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TableLabelEditCellDelegate;

@interface TableLabelEditCell : UITableViewCell
<
UITextFieldDelegate
>

@property (weak, nonatomic) id <TableLabelEditCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *serialLabel;
@property (weak, nonatomic) IBOutlet UITextField *tableLabelTextField;
@end

@protocol TableLabelEditCellDelegate <NSObject>
@optional
- (BOOL)tableLabelEditCell:(TableLabelEditCell *)cell textFieldShouldReturn:(UITextField *)textField indexPath:(NSIndexPath *)indexPath;
- (BOOL)tableLabelEditCell:(TableLabelEditCell *)cell textFieldShouldClear:(UITextField *)textField indexPath:(NSIndexPath *)indexPath;
- (void)tableLabelEditCell:(TableLabelEditCell *)cell textFieldDidEndEditing:(UITextField *)textField indexPath:(NSIndexPath *)indexPath;
@end