//
//  PCNavigationController.m
//  RushOrder
//
//  Created by Conan on 3/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCNavigationController.h"
#import "ReachabilityService.h"

#define NETWORKERROR_DECISION_TIMEOUT   8

NSString * const NetworkProblemReportNotification = @"NetworkProblemReportNotification";
NSString * const NetworkGoodReportNotification = @"NetworkGoodReportNotification";
NSString * const NetworkErrorCloseNotification = @"NetworkErrorCloseNotification";

@interface PCNavigationController ()

@property (strong, nonatomic) UIView *networkAlertView;
@property (strong, nonatomic) NSDate *wakeUpTime;
@property (nonatomic) BOOL inPerformingNetworkProblem;
@property (nonatomic) BOOL inPerformingReachability;
@end

@implementation PCNavigationController

- (id)initWithRootViewController:(UIViewController *)rootViewController
{
    self = [super initWithRootViewController:rootViewController];
    if(self != nil){
        [self commonInit];
    }
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self != nil){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.navigationBar.translucent = NO;
    self.wakeUpTime = [NSDate date];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didEnterBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willEnterForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:ReachabilityStatusChangedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkProblemOccurred:)
                                                 name:NetworkProblemReportNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(successAPICalling:)
                                                 name:NetworkGoodReportNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networErrorCloseNotification:)
                                                 name:NetworkErrorCloseNotification
                                               object:nil];
}

- (void)networErrorCloseNotification:(NSNotification *)aNoti
{
    if(aNoti.object != self){
        [self showNetworkError:NO];
    }
}

- (void)networkProblemOccurred:(NSNotification *)aNoti
{
    if(aNoti == nil) self.inPerformingNetworkProblem = NO;
    NSTimeInterval interval = [self.wakeUpTime timeIntervalSinceNow];
    //interval is negative value
    if(interval > 0) {
        PCError(@"Interval at networkProblemOccurred should be under 0");
        self.inPerformingNetworkProblem = NO;
    }
    if(interval < -NETWORKERROR_DECISION_TIMEOUT){
        PCLog(@"networkProblemOccurred Interval %f / Status : %d", interval, REACH.status);
        if(REACH.status == NotReachable){
            [self showNetworkError:YES];
        } 
    } else {
        if(!self.inPerformingNetworkProblem){
            self.inPerformingNetworkProblem = YES;
            [self performSelector:@selector(networkProblemOccurred:)
                       withObject:nil
                       afterDelay:NETWORKERROR_DECISION_TIMEOUT+interval];
            PCLog(@"Start Timer at networkProblemOccurred");
        }
    }
}

- (void)successAPICalling:(NSNotification *)aNoti
{
    [self showNetworkError:NO];
}

- (void)didEnterBackground:(NSNotification *)aNoti
{
    self.inPerformingNetworkProblem = NO;
    self.inPerformingReachability = NO;
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(reachabilityChanged:)
                                               object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(networkProblemOccurred:)
                                               object:nil];
    [self showNetworkError:NO];
}

- (void)willEnterForeground:(NSNotification *)aNoti
{
    self.wakeUpTime = [NSDate date];
}

- (void)reachabilityChanged:(NSNotification *)aNoti
{
    if(aNoti == nil) self.inPerformingReachability = NO;
    
    if(REACH.status != NotReachable){
//        [self showNetworkError:NO];
    } else {
        // OOOPS
        NSTimeInterval interval = [self.wakeUpTime timeIntervalSinceNow];
        //interval is negative value
        if(interval > 0) {
            PCError(@"Interval at reachabilityChanged should be under 0");
            self.inPerformingReachability = NO;
        }
        if(interval < -NETWORKERROR_DECISION_TIMEOUT){
            PCLog(@"reachabilityChanged Interval %f / Status : %d", interval, REACH.status);
            [self showNetworkError:YES];
        } else {
            if(!self.inPerformingReachability){
                self.inPerformingReachability = YES;
                [self performSelector:@selector(reachabilityChanged:)
                           withObject:nil
                           afterDelay:NETWORKERROR_DECISION_TIMEOUT+interval];
                PCLog(@"Start Timer at reachabilityChanged");
            }
        }
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.view insertSubview:self.networkAlertView
                belowSubview:self.navigationBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO;
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
#if RUSHORDER_CUSTOMER
    if([self.viewControllers count] > 0){
        if(viewController.navigationItem.leftBarButtonItem == nil){
            viewController.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemBack
                                                                                          target:self
                                                                                          action:@selector(backButtonTouched:)];
            
        }
    }
#else
    if(!OVER_IOS7){
        if([self.viewControllers count] > 0){
            if(viewController.navigationItem.leftBarButtonItem == nil){
                viewController.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemBackOrange
                                                                                              target:self
                                                                                              action:@selector(backButtonTouched:)];
                
            }
        }
    }
#endif
    
    [super pushViewController:viewController
                     animated:animated];
}

- (void)backButtonTouched:(id)sender
{
    [self popViewControllerAnimated:YES];
}

- (UIView *)networkAlertView
{
    if(_networkAlertView == nil){
        _networkAlertView = [[UIView alloc] initWithFrame:CGRectMake(0, 20.0f, self.view.width, 43.0f)];
        _networkAlertView.backgroundColor = [UIColor c8Color];
        _networkAlertView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_network_error"]];
        imageView.frame = CGRectMake(10.0f, 12.0f, 19.0f, 17.0f);
        [_networkAlertView addSubview:imageView];
        
        UILabel *message = [[UILabel alloc] initWithFrame:CGRectMake(37.0f, 2.0f, 280.f, 40.0f)];
        message.text = NSLocalizedString(@"Poor Network Service.\nPlease check your internet connection.", nil);
        message.font = [UIFont systemFontOfSize:14.0f];
        message.textColor = [UIColor whiteColor];
        message.backgroundColor = [UIColor clearColor];
        message.numberOfLines = 2;
        [_networkAlertView addSubview:message];
        
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        closeButton.frame = CGRectMake((_networkAlertView.width - 30.0f), 0.0f, 30.0f, 43.0f);
        closeButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        [closeButton setImage:[UIImage imageNamed:@"button_close_black"]
                                         forState:UIControlStateNormal];
        [closeButton addTarget:self
                        action:@selector(networkErroClose:)
              forControlEvents:UIControlEventTouchUpInside];

        [_networkAlertView addSubview:closeButton];
        _networkAlertView.hidden = YES;
    }
    
    return _networkAlertView;
}

- (void)setNavigationBarHidden:(BOOL)hidden animated:(BOOL)animated
{
    [super setNavigationBarHidden:hidden animated:animated];
}

- (void)setToolbarHidden:(BOOL)hidden animated:(BOOL)animated
{
    [super setToolbarHidden:hidden animated:animated];
}

- (void)networkErroClose:(id)sender
{
    [self showNetworkError:NO];
    [[NSNotificationCenter defaultCenter] postNotificationName:NetworkErrorCloseNotification
                                                        object:self];
}

- (void)showNetworkError:(BOOL)isShow
{
    if(isShow){
        self.networkAlertView.hidden = NO;
    }
    
    [UIView animateWithDuration:isShow ? 0.2f : 0.4f
                          delay:0.0f
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         if(isShow && [self.networkAlertView superview] == nil){
                             [self.view insertSubview:self.networkAlertView
                                         belowSubview:self.navigationBar];
                         }
                         self.networkAlertView.y = isShow ? CGRectGetMaxY(self.navigationBar.frame) : -self.networkAlertView.height;
                     } completion:^(BOOL finished) {
                         if(!isShow && finished){
                             self.networkAlertView.hidden = YES;
                         }
                     }];
}


@end
