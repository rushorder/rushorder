//
//  ImageBackViewController.h
//  RushOrder
//
//  Created by Conan on 3/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageBackViewController : PCiPadViewController

@property (weak, nonatomic) IBOutlet UIView *introductionView;
@end
