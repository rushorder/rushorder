//
//  ChangePasswordMerchantViewController.m
//  RushOrder
//
//  Created by Conan on 2/20/14.
//  Copyright (c) 2014 RushOrder. All rights reserved.
//

#import "ChangePasswordMerchantViewController.h"
#import "PCCredentialService.h"

@interface ChangePasswordMerchantViewController ()

@property (weak, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NPStretchableButton *changeButton;
@property (weak, nonatomic) IBOutlet PCTextField *currentPasswordTextField;
@property (weak, nonatomic) IBOutlet PCTextField *neoPasswordTextField;
@property (weak, nonatomic) IBOutlet PCTextField *confirmPasswordTextField;
@end

@implementation ChangePasswordMerchantViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Change Password", nil);
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Cancel",nil)
                                                                             target:self
                                                                             action:@selector(closeButtonTouched:)];
    
    [self.scrollView setContentSizeWithBottomView:self.changeButton];
}

- (void)closeButtonTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)changeButtonTouched:(id)sender
{
    [self.view endEditing:YES];
    
    if([self.currentPasswordTextField.text length] == 0){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Current Password Needed", nil)
                            message:NSLocalizedString(@"To change password, please enter the current password", nil)
                           delegate:self
                                tag:102];
        return;
    }
    
    if([self.neoPasswordTextField.text length] == 0){
        [UIAlertView alertWithTitle:NSLocalizedString(@"New Password Needed", nil)
                            message:NSLocalizedString(@"Enter new password to change", nil)
                           delegate:self
                                tag:103];
        return;
    }
    
    if([self.confirmPasswordTextField.text length] == 0){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Confirm New Password Needed", nil)
                            message:NSLocalizedString(@"To check new password correctly, please enter confirm new password", nil)
                           delegate:self
                                tag:104];
        return;
    }
    
    if(![self.confirmPasswordTextField.text isEqualToString:self.neoPasswordTextField.text]){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Password mismatch", nil)
                            message:NSLocalizedString(@"The passwords you entered do not match. Please try again.", nil)
                           delegate:self
                                tag:104];
        return;
    }
    
    RequestResult result = RRFail;
    result = [CRED requestChangePassword:self.currentPasswordTextField.text
                             newPassword:self.neoPasswordTextField.text
                         completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Password has been Changed Successfully", nil)
                                                  message:NSLocalizedString(@"Now, you can sign in with changed password", nil)
                                                 delegate:self
                                                      tag:101];
                              
                              break;
                          case ResponseErrorPasswordIncorrect:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Passowrd is Incorrect", nil)
                                                  message:NSLocalizedString(@"Please enter correct current password", nil)
                                                 delegate:self
                                                      tag:102];
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while changing password", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 101:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case 102:
            [self.currentPasswordTextField becomeFirstResponder];
            break;
        case 103:
            [self.neoPasswordTextField becomeFirstResponder];
            break;
        case 104:
            [self.confirmPasswordTextField becomeFirstResponder];
            break;
        default:
            break;
    }
}

@end
