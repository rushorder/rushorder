//
//  MenuOrderViewController.m
//  RushOrder
//
//  Created by Conan on 5/14/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "MenuOrderViewController.h"
#import "IIViewDeckController.h"
#import "MenuCategory.h"
#import "MenuSubCategory.h"
#import "MenuOptionGroup.h"
#import "MenuOption.h"
#import "MenuItem.h"
#import "MenuManager.h"
#import "PCMenuService.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FormatterManager.h"
#import "MenuOrderManager.h"
#import "CheckOutViewController.h"
#import "TableStatusManager.h"

#define MENU_REL_XIB_NAME   @"MenuItemCell"

typedef enum menuItemType_{
    MenuItemTypeSubCategory = 1,
    MenuItemTypeItem
} MenuItemType;

@interface MenuOrderViewController ()

@property (strong, nonatomic) OrderListViewController *orderListViewController;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *tableNumberView;
@property (strong, nonatomic) IBOutlet NPKeyboardAwareScrollView *tableNumberScrollView;
@property (strong, nonatomic) MenuItem *selectedMenuItem;
@property (nonatomic) NSInteger selectedQuantity;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (strong, nonatomic) IBOutlet UIPickerView *tableNumberPickerView;
@property (strong, nonatomic) IBOutlet UIToolbar *tableNumberToolBar;
@property (weak, nonatomic) IBOutlet PCTextField *tableNumberTextField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *toggleBarButtonItem;
@property (strong, nonatomic) NSMutableArray *mobileCardList;

@property (nonatomic, getter = isLockReloadBadge) BOOL lockReloadBadge;
@property (nonatomic) BOOL didAppeared;
@property (nonatomic) BOOL checkedNoMenuItem;
@property (strong, nonatomic) IBOutlet UIView *noItemView;
@property (weak, nonatomic) IBOutlet UILabel *noMenuInfoLabel;
@property (weak, nonatomic) IBOutlet NPStretchableButton *gotoRequestBillButton;


@end

@implementation MenuOrderViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(cartUnsolicitChanged:)
                                                     name:CartUnsolicitChangedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(orderListChanged:)
                                                     name:LineItemChangedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(menuListChanged:)
                                                     name:MenuListChangedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(tableListChanged:)
                                                     name:TableListChangedNotification
                                                   object:nil];
    }
    return self;
}

- (void)merchantUpdated:(NSNotification *)notification
{
    BOOL invalidMerchant = [notification.userInfo boolForKey:@"InvalidMerchant"];
    if(invalidMerchant){
        [self.navigationController popViewControllerAnimated:YES];
    } else {

    }
}

- (void)cartUnsolicitChanged:(NSNotification *)notification
{
    if(self.tableInfo != nil){
        [MENUORDER checkTable:self.tableInfo
                  success:^(){
                      if(MENUORDER.shouldOpenRightSlide){
                          [APP.viewDeckController openRightViewAnimated:YES
                                                             completion:^(IIViewDeckController *controller, BOOL success) {
                                                                 
                                                             }];
                      }
                  }
                  failure:^{
                      
                  }];
    }
}

- (void)orderListChanged:(NSNotification *)notification
{
    if(self.isLockReloadBadge){
        return;
    }
    
    [self reloadBadgeNumber:[NSNumber numberWithBool:NO]];
}

- (void)menuListChanged:(NSNotification *)notification
{
    self.gotoRequestBillButton.hidden = YES;
    
    if([[self menuList] count] > 0){
        [self.tableView reloadData];
        self.tableView.tableFooterView = nil;
    } else if(CRED.merchant.isAblePayment){
        self.tableView.tableFooterView = self.noItemView;
        self.noMenuInfoLabel.text = NSLocalizedString(@"This restaurant may be configuring its menu so you cannot order right now.", nil);
        self.gotoRequestBillButton.hidden = NO;
        if(self.didAppeared){
//            [self pushTablesetViewController];
        } else {
            self.checkedNoMenuItem = YES;
        }
    } else {
        self.tableView.tableFooterView = self.noItemView;
        self.noMenuInfoLabel.text = NSLocalizedString(@"This restaurant may be configuring its menu so you cannot order right now.", nil);
        
        [self.tableView reloadData];
    }
}

- (IBAction)gotoRequestBillButton:(id)sender
{
//    [self pushTablesetViewController];
}

- (void)tableListChanged:(NSNotification *)notification
{
    [self.tableNumberPickerView reloadAllComponents];
    
    if([self.tableNumberTextField.text length] == 0){
        if([MENUORDER.tableList count] > 0){
            TableInfo *firstTableInfo = [MENUORDER.tableList objectAtIndex:0];
            self.tableNumberTextField.text = firstTableInfo.tableNumber;
        }
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Menu", nil);
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Order List", nil)
                                                                              target:self
                                                                              action:@selector(orderButtonTouched:)
                                                                            backType:NPBarButtonBackTypeAppDefault];
    
    self.orderListViewController = [OrderListViewController viewControllerFromNib];
    self.orderListViewController.delegate = self;
    
    self.tableView.rowHeight = 94.0f;
    
//    [self.tableNumberScrollView setContentSizeWithBottomView:self.tableNumberView];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        CGAffineTransform transform = CGAffineTransformMakeRotation(-M_PI_2);
        self.tableNumberScrollView.transform = transform;
    }
    
    self.tableNumberScrollView.frame = APP.window.frame;
    [self.tableNumberScrollView setContentSize:CGSizeMake(self.view.width, self.view.height)];
    self.tableNumberScrollView.adjustingOffsetY = -70.0f;
    
    self.tableNumberTextField.inputView = self.tableNumberPickerView;
    self.tableNumberTextField.inputAccessoryView = self.tableNumberToolBar;
    
    [self hideTableNumber:NO];

    if(self.tableInfo != nil){
        [MENUORDER checkTable:self.tableInfo
                  success:^(){
                      if(self.didAppeared){
                          if(MENUORDER.shouldOpenRightSlide){
                              [APP.viewDeckController openRightViewAnimated:YES
                                                                 completion:^(IIViewDeckController *controller, BOOL success) {

                                                                 }];
                          }
                      }
                  }
                  failure:^{
                      
                  }];
    }
    

}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    APP.viewDeckController.rightController = self.orderListViewController;
    [self reloadBadgeNumber:[NSNumber numberWithBool:NO]];
    
    if(!self.didAppeared){
        if(MENUORDER.shouldOpenRightSlide){
            [APP.viewDeckController openRightViewAnimated:YES
                                               completion:^(IIViewDeckController *controller, BOOL success) {
                                               }];
        }
    }
    self.didAppeared = YES;
    
    if(self.checkedNoMenuItem){
        self.checkedNoMenuItem = NO;
        if(CRED.merchant.isAblePayment){
//            [self pushTablesetViewController];
        }
    }
}

- (void)orderButtonTouched:(id)sender
{
    IIViewDeckController *deckController = nil;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        deckController = APP.viewDeckController;
    } else {
        deckController = (IIViewDeckController *)APP.window.rootViewController;
    }
    [deckController toggleRightViewAnimated:YES];
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [self setTableNumberView:nil];
    [self setTableNumberScrollView:nil];
    [self setTableNumberTextField:nil];

    [self setToggleBarButtonItem:nil];
    [self setNoItemView:nil];
    [self setNoMenuInfoLabel:nil];
    [self setGotoRequestBillButton:nil];
    [super viewDidUnload];
    
    IIViewDeckController *deckController = nil;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        deckController = APP.viewDeckController;
    } else {
        deckController = (IIViewDeckController *)APP.window.rootViewController;
    }
    
    if(deckController.rightController == self.orderListViewController)
        deckController.rightController = nil;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if(APP.viewDeckController.rightController == self.orderListViewController)
        APP.viewDeckController.rightController = nil;
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

#pragma mark - UITableView Delegate & Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return [[self menuList] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    MenuCategory *menuCategory = [[self menuList] objectAtIndex:section];
    return [menuCategory.items count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MenuCategorySectionView *view = [MenuCategorySectionView viewWithNibName:MENU_REL_XIB_NAME
                                                                     atIndex:2];
    MenuCategory *menuCategory = [[self menuList] objectAtIndex:section];
    view.button.buttonTitle = menuCategory.name;
    if([menuCategory.name length] == 0){
        return nil;
    } else {
        return view;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCategory *menuCategory = [[self menuList] objectAtIndex:indexPath.section];
    id item = [menuCategory.items objectAtIndex:indexPath.row];
    
    if([item isKindOfClass:[MenuSubCategory class]]){
        MenuSubCategory *subCategory = (MenuSubCategory *)item;
        if([subCategory.name length] > 0){
            return 35.0f;
        } else {
            return 0.0f;
        }
    } else {
        return 94.0f;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    MenuCategory *menuCategory = [[self menuList] objectAtIndex:section];
    
    if([menuCategory.name length] == 0){
        return 0.0f;
    } else {
        return 25.0f;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    MenuItemType itemType = MenuItemTypeItem;
    
    MenuCategory *menuCategory = [[self menuList] objectAtIndex:indexPath.section];
    id item = [menuCategory.items objectAtIndex:indexPath.row];
    
    if([item isKindOfClass:[MenuSubCategory class]]){
        itemType = MenuItemTypeSubCategory;
    }
    
    switch(itemType){
        case MenuItemTypeItem:
            return [self tableView:tableView menuItemCellForRowAtIndexPath:indexPath withMenuItem:item];
            break;
        case MenuItemTypeSubCategory:
            return [self tableView:tableView menuSubCategoryCellForRowAtIndexPath:indexPath withMenuSubCategory:item];
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView menuItemCellForRowAtIndexPath:(NSIndexPath *)indexPath withMenuItem:(MenuItem *)item
{
    NSString *cellIdentifier = @"MenuItemCell";
    
    MenuItemCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        cell = [MenuItemCell cell];
    }
    
    cell.item = item;
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView menuSubCategoryCellForRowAtIndexPath:(NSIndexPath *)indexPath withMenuSubCategory:(MenuSubCategory *)subCategory
{
    NSString *cellIdentifier = @"MenuSubCategoryCell";
    
    MenuSubCategoryCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        cell = [MenuSubCategoryCell cellWithNibName:MENU_REL_XIB_NAME
                                            atIndex:1];
    }
    cell.subCategory = subCategory;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCategory *menuCategory = [[self menuList] objectAtIndex:indexPath.section];
    id item = [menuCategory.items objectAtIndex:indexPath.row];
    
    if([item isKindOfClass:[MenuItem class]]){
        MenuItem *menuItem = item;
        if([menuItem.desc length] > 0){
            menuItem.flipped = !menuItem.flipped;
            
//            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
//                                   withRowAnimation:UITableViewRowAnimationFade];
            
            
//            MenuItemCell *cell = (MenuItemCell *)[self tableView:self.tableView menuItemCellForRowAtIndexPath:indexPath withMenuItem:menuItem];
            
  
            MenuItemCell *cell = (MenuItemCell *)[self.tableView cellForRowAtIndexPath:indexPath];
            
            
            [UIView transitionWithView:cell
                              duration:0.35f
                               options:UIViewAnimationOptionTransitionFlipFromBottom
                            animations:^{
                                [cell setNeedsLayout];
                            }
                            completion:NULL];
            
//            [UIView beginAnimations:@"FlipCellAnimation" context:nil];
//            [UIView setAnimationDuration:0.5f];
//            [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:cell cache:YES];
            
//            [cell setNeedsLayout];
//            [cell removeFromSuperview];
//            
//            [self.tableView addSubview:cell];
            
//            [UIView commitAnimations];
            
//            [tableView deselectRowAtIndexPath:indexPath
//                                     animated:YES];
            
        } else {
            [tableView deselectRowAtIndexPath:indexPath
                                     animated:YES];
        }
    }
}

- (void)cell:(MenuItemCell *)cell didTouchedAddToCartButtonAtIndexPath:(NSIndexPath *)indexPath
{    
    MenuCategory *menuCategory = [[self menuList] objectAtIndex:indexPath.section];
    id item = [menuCategory.items objectAtIndex:indexPath.row];
    
    if([item isKindOfClass:[MenuItem class]]){
        MenuItem *menuItem = item;
        
        self.selectedIndexPath = indexPath;
        
        if([menuItem.optionGroups count] > 0 || [menuItem.prices count] > 1){
            MenuOrderDetailViewController *viewController = [MenuOrderDetailViewController viewControllerFromNib];
            viewController.item = item;
            viewController.delegate = self;
            viewController.cart = MENUORDER.cart;
            
            [self presentViewControllerInNavigation:viewController
                                           animated:YES
                                         completion:NULL];
        } else {
            [self addMenuItem:menuItem quantity:1 specialInstructions:nil];
        }
    }
}

#pragma mark - MenuOrderDetailViewControllerDelegate
- (void)menuOrderDetailViewController:(MenuOrderDetailViewController *)viewController didNewMenuItem:(MenuItem *)menuItem quantity:(NSInteger)quantity
{
    [self addMenuItem:menuItem quantity:quantity specialInstructions:nil];
}

- (void)showTableNumber:(BOOL)animated
{
    [APP.window addSubview:self.tableNumberScrollView];
    [UIView animateWithDuration:0.3f
                     animations:^{
                         [self moveTableNumber:YES];
                     }];
}

- (void)hideTableNumber:(BOOL)animated
{
    [UIView animateWithDuration:0.3f
                     animations:^{
                                   [self moveTableNumber:NO];
                               }
                     completion:^(BOOL finished) {
                         if(finished){
                             [self.tableNumberScrollView removeFromSuperview];
                         }
                     }];
}

- (void)moveTableNumber:(BOOL)show
{
    self.tableNumberView.frame = CGRectMake(0.0f, show ? 76.0f : 600.0f,
                                            self.tableNumberView.frame.size.width,
                                            self.tableNumberView.frame.size.height);
    
    self.tableNumberScrollView.alpha = show ? 1.0f : 0.0f;
}


- (void)addMenuItem:(MenuItem *)menuItem quantity:(NSInteger)quantity specialInstructions:(NSString *)specialInstructions
{
    if(CRED.merchant.isTableBase){
        
        if(MENUORDER.tableInfo == nil){
            self.selectedMenuItem = menuItem;
            self.selectedQuantity = quantity;
            
            [self showTableNumber:YES];
//        [self.tableNumberTextField becomeFirstResponder];
        } else {
            if(MENUORDER.cart.status != CartStatusOrdering){
                [UIAlertView alertWithTitle:NSLocalizedString(@"Order list is not editable", nil)
                                    message:NSLocalizedString(@"Order list has been placed. To add/modify/remove orders, you have to postpone the confirmation of your order by \"Change Placed Order\" button", nil)
                                   delegate:self
                                        tag:100];
                [self resetSelection];
                return;
            }
            self.lockReloadBadge = YES;
            [MENUORDER.cart addMenuItem:menuItem
                               quantity:quantity
                    specialInstructions:specialInstructions
                            success:^(BOOL isOnlyLocal){
                                self.selectedMenuItem = nil;
                                [self cartAddAnimate];
                                
                                TABLE.dirty = YES;
                                
                            } failure:^{
                                [self resetSelection];
                                self.lockReloadBadge = NO;
                                
                                if(CRED.merchant.isTableBase){
                                    [MENUORDER checkTable:MENUORDER.tableInfo
                                              success:^{

                                              }
                                              failure:^{

                                              }];
                                }
                            }];
        }
        
    } else {
        // Pickup
        self.lockReloadBadge = YES;
        [MENUORDER.cart addMenuItem:menuItem
                           quantity:quantity
                specialInstructions:specialInstructions
                        success:^(BOOL isOnlyLocal){

                            self.selectedMenuItem = nil;
                            [self cartAddAnimate];
                            
                        } failure:^{
                            [self resetSelection];
                            self.lockReloadBadge = NO;
                        }];
    }
}

- (void)resetSelection
{
    self.selectedMenuItem = nil;
    self.selectedQuantity = 0;
    self.selectedIndexPath = nil;
}

- (IBAction)confirmButtonTouched:(id)sender
{
    if([self.tableNumberTextField.text length] == 0){
        [self.tableNumberTextField becomeFirstResponder];
        return;
    } else {
        
    }
    
    [self doneButtonTouched:sender];
    
    [self hideTableNumber:YES];
    
    [MENUORDER checkTable:self.tableInfo
           assertMine:NO
              success:^(){
                  if(self.selectedMenuItem != nil){
                      [self addMenuItem:self.selectedMenuItem quantity:self.selectedQuantity specialInstructions:nil];
                  } else {
                      
                  }
              }
              failure:^{
                  if(MENUORDER.order.status == OrderStatusCompleted){
                      [UIAlertView alertWithTitle:NSLocalizedString(@"Prior Table Orders Not Settled", nil)
                                          message:NSLocalizedString(@"Please wait for the restaurant to clear prior table orders. If table is not cleared for an extended period of time, please ask restaurant staff for assistance", nil)];
                  }
              }];

}

#pragma mark - OrderListViewControllerDelegate
- (void)orderListViewController:(OrderListViewController *)orderListViewController didTouchCheckoutButton:(id)sender
{
    [APP.viewDeckController closeRightViewAnimated:YES
                                        completion:^(IIViewDeckController *controller, BOOL success) {
                                            if(success){
                                                CheckOutViewController *viewController = [CheckOutViewController viewControllerFromNib];
                                                viewController.tableInfo = MENUORDER.tableInfo;
                                                viewController.order = MENUORDER.order;
                                                [self.navigationController pushViewController:viewController
                                                                                     animated:YES];
                                            }
                                        }];
}

- (void)orderListViewController:(OrderListViewController *)orderListViewController didTouchOrderDoneButton:(id)sender
{
    [APP.viewDeckController closeRightViewAnimated:YES
                                        completion:^(IIViewDeckController *controller, BOOL success) {
                                            if(success){
                                                [self.navigationController popToRootViewControllerAnimated:YES];
                                            }
                                        }];
}

- (void)orderListViewController:(OrderListViewController *)orderListViewController didTouchOnlyCheckoutButton:(id)sender
{
    [APP.viewDeckController closeRightViewAnimated:YES
                                        completion:^(IIViewDeckController *controller, BOOL success) {
                                            if(success){
//                                                TablesetViewController *viewController = [TablesetViewController viewControllerFromNib];
//                                                [self.navigationController pushViewController:viewController animated:YES];
                                            }
                                        }];
}

- (void)orderListViewController:(OrderListViewController *)orderListViewController didTouchCheckinButton:(id)sender
{
    [APP.viewDeckController closeRightViewAnimated:YES
                                        completion:
     ^(IIViewDeckController *controller, BOOL success) {
         if(success){
             [self showTableNumber:YES];
         }
     }];
}


- (IBAction)tableNumberCancelButton:(id)sender
{
    [self resetSelection];
    [self hideTableNumber:NO];
}

#pragma mark - UIPickerViewDataSource
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger count = [MENUORDER.tableList count];
    
    return MAX(count, 1);
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if([MENUORDER.tableList count] > row){
        TableInfo *table = [MENUORDER.tableList objectAtIndex:row];
        return table.tableNumber;
    }
    
    return @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if([MENUORDER.tableList count] > row){
        TableInfo *table = [MENUORDER.tableList objectAtIndex:row];
        self.tableInfo = table;
        self.tableNumberTextField.text = table.tableNumber;
    }
}

- (IBAction)toggleButtonTouched:(id)sender
{
    [self.tableNumberTextField resignFirstResponder];
    
    if(self.tableNumberTextField.inputView != nil){
        self.tableNumberTextField.inputView = nil;
        self.toggleBarButtonItem.title = NSLocalizedString(@"Picker", nil);
    } else {
        self.tableNumberTextField.inputView = self.tableNumberPickerView;
        self.toggleBarButtonItem.title = NSLocalizedString(@"Keyboard", nil);
    }
    
    [self.tableNumberTextField becomeFirstResponder];
}

- (IBAction)doneButtonTouched:(id)sender
{
    if(self.tableNumberTextField.inputView == nil){
        NSString *inputTableNumber = self.tableNumberTextField.text;
        if([inputTableNumber length] > 0){
            self.tableInfo = nil;
            for(TableInfo *tableInfo in MENUORDER.tableList){
                if([tableInfo.tableNumber isCaseInsensitiveEqual:inputTableNumber]){
                    self.tableInfo = tableInfo;
                    break;
                }
            }
            if(self.tableInfo == nil){
                [UIAlertView alertWithTitle:NSLocalizedString(@"Invalid Table Number", nil)
                                    message:NSLocalizedString(@"You have entered a table number outside the range established by the restaurant.  Please check and re-enter.", nil)];
                [self.tableNumberTextField becomeFirstResponder];
            }
        } else {
            // Nothing to do
        }
        
    } else {
        NSInteger selRow = [self.tableNumberPickerView selectedRowInComponent:0];
        
        if([MENUORDER.tableList count] > selRow){
            TableInfo *table = [MENUORDER.tableList objectAtIndex:selRow];
            self.tableInfo = table;
            self.tableNumberTextField.text = table.tableNumber;
        }
    }
    [self.tableNumberTextField resignFirstResponder];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if([MENUORDER.tableList count] == 0){
//        [self requestTableLables];
    }
}

#define ANIMA_DURATION 0.55f

- (void)cartAddAnimate
{
//    // grab the cell using indexpath
//    MenuItemCell *cell = (MenuItemCell *)[self.tableView cellForRowAtIndexPath:self.selectedIndexPath];
//    // grab the imageview using cell
//    UIView *container = cell.imageContainerView;
//    
//    CGRect rect = [APP.window convertRect:container.frame fromView:container.superview];
//    //    rect = CGRectMake(5, (rect.origin.y*-1)-10, imgV.frame.size.width, imgV.frame.size.height);
//    //    NSLog(@"rect is %f,%f,%f,%f",rect.origin.x,rect.origin.y,rect.size.width,rect.size.height);
//    
//    // create new duplicate image
//    
//    UIView *aniView = [[UIView alloc] initWithFrame:rect];
//    
//    UIImageView *starView = [[UIImageView alloc] initWithImage:[cell.menuImageButton imageForState:UIControlStateNormal]];
//    starView.contentMode = cell.menuImageButton.contentMode;
//    starView.clipsToBounds = YES;
//    starView.frame = CGRectMake(0.0f, 0.0f, rect.size.width, rect.size.height);
//    [aniView addSubview:starView];
//    
//    UIImageView *frameView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"edge_menu_frame"]];
//    frameView.frame = CGRectMake(0.0f, 0.0f, rect.size.width, rect.size.height);
//    [aniView addSubview:frameView];
//    
//    [APP.window addSubview:aniView];
//    
//    // begin ---- apply position animation
//    CAKeyframeAnimation *pathAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
//    pathAnimation.calculationMode = kCAAnimationPaced;
//    pathAnimation.fillMode = kCAFillModeForwards;
//    pathAnimation.removedOnCompletion = NO;
//    pathAnimation.duration=ANIMA_DURATION;
//    pathAnimation.delegate=self;
//    
//    // tab-bar right side item frame-point = end point
//    CGFloat rightEndX = CGRectGetMaxX(self.view.frame);
//    CGPoint endPoint = CGPointMake(rightEndX - 50.0f, 42.0f);
//    
//    CGMutablePathRef curvedPath = CGPathCreateMutable();
//    CGPathMoveToPoint(curvedPath, NULL, aniView.center.x, aniView.center.y);
//    CGPathAddCurveToPoint(curvedPath, NULL, endPoint.x, aniView.frame.origin.y, endPoint.x, aniView.frame.origin.y, endPoint.x, endPoint.y);
//    pathAnimation.path = curvedPath;
//    CGPathRelease(curvedPath);
//    // end ---- apply position animation
//    
//    // apply transform animation
//    CABasicAnimation *basic=[CABasicAnimation animationWithKeyPath:@"transform"];
//    [basic setToValue:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.01, 0.01, 0.01)]];
//    [basic setAutoreverses:NO];
//    [basic setDuration:ANIMA_DURATION];
//    basic.removedOnCompletion = NO;
//    [basic setValue:aniView forKey:@"animationView"];
//    basic.delegate = self;
//    
//    [aniView.layer addAnimation:pathAnimation forKey:@"curveAnimation"];
//    [aniView.layer addAnimation:basic forKey:@"transform"];
}

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag
{
    if(flag){
        UIImageView *view = [theAnimation valueForKey:@"animationView"];
        if (view) {
            [view removeFromSuperview];
            [theAnimation setValue:nil forKey:@"animationView"];
        }
        [self reloadBadgeNumber:[NSNumber numberWithBool:YES]];
    }
}

- (void)reloadBadgeNumber:(NSNumber *)withBounce
{
    NSInteger badgeCount = 0;
    if(MENUORDER.cart.status == CartStatusOrdering) badgeCount = MENUORDER.cart.quantities;
    
    NSString *countString = nil;
  
    if((MENUORDER.cart.quantities == 0 && MENUORDER.order.status == OrderStatusConfirmed) ||
       (!CRED.merchant.isTableBase && MENUORDER.order.status == OrderStatusDraft)){
        if(CRED.merchant.isAblePayment){
            countString = NSLocalizedString(@"Check", nil);
        } else {
            countString = NSLocalizedString(@"Done", nil);
        }
    } else {
        if(badgeCount > 0)
            countString = [NSString stringWithFormat:@"%d", badgeCount];
    }

    self.navigationItem.rightBarButtonItem.button.badgeValue = countString;

    if([withBounce boolValue]){
        [APP.viewDeckController previewBounceView:IIViewDeckRightSide];
    }

    self.lockReloadBadge = NO;
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 100:
            [APP.viewDeckController openRightViewAnimated:YES];
            break;
        default:
            break;
    }
}

- (IBAction)payWithCardButtonTouched:(id)sender
{

}

- (NSMutableArray *)menuList
{
    return [MENUPAN menuListAt:0];
}


@end
