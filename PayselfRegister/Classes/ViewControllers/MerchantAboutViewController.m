//
//  MerchantAboutViewController.m
//  RushOrder
//
//  Created by Conan on 2/22/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MerchantAboutViewController.h"

@interface MerchantAboutViewController ()

@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceLabel;
@end

@implementation MerchantAboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"About", nil);
//    [self.view addDefaultBackgroundImage];
    
    self.versionLabel.text = self.versionString;
    
    if([[NSBundle mainBundle].bundleIdentifier isEqualToString:@"com.payselfmobile.wallet"]){
        self.serviceLabel.text = NSLocalizedString(@"Wallet", nil);
    } else {
        self.serviceLabel.text = NSLocalizedString(@"Merchant", nil);
    }
    
    self.self.preferredContentSize = CGSizeMake(self.view.frame.size.width
                                                  , 400.0f);
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setVersionLabel:nil];
    [self setServiceLabel:nil];
    [super viewDidUnload];
}
@end
