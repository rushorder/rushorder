//
//  MerchantAccountViewController.h
//  RushOrder
//
//  Created by Conan on 8/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCiPadViewController.h"

@interface MerchantAccountViewController : PCiPadViewController <UIAlertViewDelegate>


@end
