//
//  CuisineTableViewController.h
//  RushOrder
//
//  Created by Conan on 10/27/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CuisineTableViewControllerDelegate;

@interface CuisineTableViewController : UITableViewController
@property (weak, nonatomic) id<CuisineTableViewControllerDelegate> delegate;
@end

@protocol CuisineTableViewControllerDelegate <NSObject>
- (void)cuisineTableViewController:(CuisineTableViewController *)viewController didSelectCuisines:(NSArray *)array;
@end
