//
//  MerchantSettingViewController.h
//  RushOrder
//
//  Created by Conan on 3/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCiPadViewController.h"
#import "MerchantLocationViewController.h"
#import "Merchant.h"

@interface MerchantSettingViewController : PCiPadViewController
<
MerchantLocationViewControllerDelegate,
UIAlertViewDelegate,
UITextFieldDelegate
>

@property (nonatomic) PCSerial merchantNo;
@property (nonatomic) BOOL inProcessSignUp;
@end
