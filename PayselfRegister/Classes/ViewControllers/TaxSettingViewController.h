//
//  TaxSettingViewController.h
//  RushOrder
//
//  Created by Conan on 7/30/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaxSettingViewController : PCiPadViewController
<
UITextFieldDelegate
>
@end
