//
//  JoinedMerchantViewController.h
//  RushOrder
//
//  Created by Conan on 10/7/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"


typedef enum staffJoinRequestStatus_{
    StaffJoinRequestStatusUnknown = 0,
    StaffJoinRequestStatusRequested,
    StaffJoinRequestStatusRejected,
    StaffJoinRequestStatusConfirmed
}
StaffJoinRequestStatus;

@interface JoinedMerchantViewController : PCViewController
<
UIActionSheetDelegate
>
@end


@interface NSString (StaffJoinRequest)
- (StaffJoinRequestStatus)staffJoinRequestStatusEnum;
@end