//
//  TableStatusViewController.h
//  RushOrder
//
//  Created by Conan on 2/28/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NPBatchTableView.h"

@interface TableStatusViewController : PCiPadViewController
<
UITableViewDelegate,
UITableViewDataSource,
UIActionSheetDelegate,
PullToRefreshing
>

@end
