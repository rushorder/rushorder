//
//  FastFoodViewController.h
//  RushOrder
//
//  Created by Conan on 7/1/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCiPadViewController.h"
#import "PCDynamicRowGridView.h"
#import "MerchantSettingMenuViewController.h"
#import "MerchantLocationViewController.h"
#import "OptionViewController.h"

@interface FastFoodViewController : PCiPadViewController
<
PCDynamicRowGridViewDelegate,
PCDynamicRowGridViewDataSource,
UIPopoverControllerDelegate,
UIActionSheetDelegate,
MerchantSettingMenuViewControllerDelegate,
OptionViewControllerDelegate,
MerchantLocationViewControllerDelegate,
UIAlertViewDelegate
>

@end
