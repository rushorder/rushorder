//
//  AggregateViewController.m
//  RushOrder
//
//  Created by Conan on 3/7/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "AggregateViewController.h"
#import "AggregateCell.h"
#import "PCMerchantService.h"
#import "AggregateGroup.h"

#define AGGREGATE_CELL_HEIGHT 96.0f

enum groupByType_{
    GroupByTypeNone = 0,
    GroupByTypeDaily,
    GroupByTypeWeekly,
    GroupByTypeMonthly
} GroupByType;

@interface AggregateViewController ()

@property (strong, atomic) Order *totalOrder;

@property (strong, nonatomic) AggregateCell *footerView;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *aggregateList;
@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *endDate;
@property (weak, nonatomic) IBOutlet UITextField *startDateTextField;
@property (weak, nonatomic) IBOutlet UITextField *endDateTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *groupSegmentedControl;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIPickerView *timePicker;
@property (strong, nonatomic) IBOutlet UIView *pickerContainer;
@property (strong, nonatomic) IBOutlet UIToolbar *datePickerToolBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *datePickerTitle;

//@property (weak, nonatomic) IBOutlet UINavigationItem *navigationBar;
@end

@implementation AggregateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Payment Summary",  nil);
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:@"Dashboard"
                                                                              target:self
                                                                              action:@selector(dashboardButtonTouched:)];
    self.navigationItem.rightBarButtonItem.width = 90.0f;
    
    self.tableView.rowHeight = AGGREGATE_CELL_HEIGHT;
    
    self.footerView = [AggregateCell cell];
    [self.footerView sumFormat:YES];
    
    
    self.startDateTextField.inputView = self.pickerContainer;
    self.startDateTextField.inputAccessoryView = self.datePickerToolBar;
    self.endDateTextField.inputView = self.pickerContainer;
    self.endDateTextField.inputAccessoryView = self.datePickerToolBar;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    BOOL useBefore = NO;
    if(userDefaults.dateQuerySettingDate != nil){
        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                                                   fromDate:userDefaults.dateQuerySettingDate];
        
        NSDateComponents *nowComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                                                   fromDate:[NSDate date]];
        
        if(nowComponents.year != components.year
           || nowComponents.month != components.month
           || nowComponents.day != components.day){
            useBefore = NO;
        } else {
            useBefore = YES;
        }
    }
    
    if(useBefore){
        self.startDate = userDefaults.startDate;
        self.endDate = userDefaults.endDate;
    } else {
        self.startDate = [[NSDate date] dateWithZero]; //[[[NSDate date] dateByAddingTimeInterval:-(60 * 60 * 24 * 7)] dateWithZero];
        self.endDate = [self.startDate dateByAddingTimeInterval:(60 * 60 * 24)];
    }
    
    [self updateDateTextFields];
    
    [self requestList];
    
//    [self.tableView addDefaultBackgroundImage];
    
}

- (void)dealloc
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    userDefaults.startDate = self.startDate;
    userDefaults.endDate = self.endDate;
    userDefaults.dateQuerySettingDate = [NSDate date];
    [userDefaults synchronize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
//    [self setNavigationBar:nil];
    [self setTableView:nil];
    [self setStartDateTextField:nil];
    [self setEndDateTextField:nil];
    [self setGroupSegmentedControl:nil];
    [self setDatePicker:nil];
    [self setDatePickerToolBar:nil];
    [self setDatePickerTitle:nil];
    [self setPickerContainer:nil];
    [self setTimePicker:nil];
    [super viewDidUnload];
}

- (void)dashboardButtonTouched:(id)sender
{
    if(CRED.merchant.isTableBase){
        [APP transitDashboard];
    } else {
        [APP transitFastFood];
    }
}

- (void)requestList
{
    RequestResult result = RRFail;
    result = [MERCHANT requestOrder:CRED.merchant.merchantNo
                          startDate:self.startDate
                            endDate:self.endDate
                    completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:{

                              self.totalOrder = [[Order alloc] init];
                              
                              dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                              dispatch_async(aQueue, ^{
                                  
                                  NSArray *list = (NSArray *)response.data;
                                  NSMutableArray *groupList = [NSMutableArray array];
                                  
                                  NSMutableArray *groupedList = [NSMutableArray array];
                                  
                                  NSDate *prevDate = nil;
                                  NSString *sectionTitle = nil;
                                  Order *sumOrder = nil;
                                  
                                  NSArray *sortedList = [list sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
                                      NSDate *date1 = [[obj1 objectForKey:@"created_at"] date];
                                      NSDate *date2 = [[obj2 objectForKey:@"created_at"] date];
                                      
                                      return [date1 compare:date2];
                                  }];
                                  
                                  for(NSDictionary *dict in sortedList){
                                      Order *anOrder = [[Order alloc] initWithDictionary:dict
                                                                                merchant:CRED.merchant];
                                      
                                      if(anOrder.status != OrderStatusFixed || anOrder.payAmounts == 0.0f){
                                          continue;
                                      }
                                      
//                                      PCLog(@"PaidAmount:%f",anOrder.payAmounts);
                                      
                                      [self.totalOrder sum:anOrder];
                                      
                                      BOOL groupChanged = NO;
                                      if(prevDate != nil){
                                          switch(self.groupSegmentedControl.selectedSegmentIndex + 1){
                                              case GroupByTypeDaily:
                                                  groupChanged = [anOrder dateChanged:prevDate];
                                                  sectionTitle = [prevDate dateStringForHumanReadableFormat];
                                                  break;
                                              case GroupByTypeWeekly:
                                                  groupChanged = [anOrder weekChanged:prevDate];
                                                  sectionTitle = [prevDate weekString];
                                                  break;
                                              case GroupByTypeMonthly:
                                                  groupChanged = [anOrder monthChanged:prevDate];
                                                  sectionTitle = [prevDate monthString];
                                                  break;
                                          }
                                      }
                                      
                                      if(groupChanged){
                                          AggregateGroup *group = [[AggregateGroup alloc] init];
                                          
                                          group.sectionTitle = sectionTitle;
                                          group.list = groupedList;
                                          group.sumOrder = sumOrder;
                                          sumOrder = nil;
                                          [groupList addObject:group];
                                          
                                          groupedList = [NSMutableArray array];
                                      }
                                      
                                      if(sumOrder == nil){
                                          sumOrder = [[Order alloc] init];
                                      }
                                      [sumOrder sum:anOrder];
                                      [groupedList addObject:anOrder];
                                      prevDate = anOrder.orderDate;
                                  }
                                  
                                  if([groupedList count] > 0){
                                      Order *anOrder = [groupedList objectAtIndex:0];
                                      AggregateGroup *group = [[AggregateGroup alloc] init];
                                      
                                      
                                      switch(self.groupSegmentedControl.selectedSegmentIndex + 1){
                                          case GroupByTypeDaily:
                                              sectionTitle = [anOrder.orderDate dateStringForHumanReadableFormat];
                                              break;
                                          case GroupByTypeWeekly:
                                              sectionTitle = [anOrder.orderDate weekString];
                                              break;
                                          case GroupByTypeMonthly:
                                              sectionTitle = [anOrder.orderDate monthString];
                                              break;
                                      }

                                      
                                      group.sectionTitle = sectionTitle;
                                      group.list = groupedList;
                                      group.sumOrder = sumOrder;
                                      sumOrder = nil;
                                      [groupList addObject:group];
                                  }
                                  
                                  
                                  dispatch_async(dispatch_get_main_queue(),^{
                                      self.aggregateList = groupList;
                                      self.footerView.order = self.totalOrder;
                                      [self.footerView fillCell];
                                      
                                      if([self.aggregateList count] > 0){
                                          self.footerView.payDateLabel.text = [NSString stringWithFormat:@"%@ - %@",
                                                                               self.startDateTextField.text,
                                                                               self.endDateTextField.text];
                                          self.tableView.tableFooterView = self.footerView;
                                      } else {
                                          self.tableView.tableFooterView = nil;
                                      }
                                      
                                      [self.tableView reloadData];
                                  });
                              });
                              break;
                          }
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting order list", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError: 
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.aggregateList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    AggregateGroup *aggregateGroup = [self.aggregateList objectAtIndex:section];
    
    return [aggregateGroup.list count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AggregateCell";
    
    AggregateCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil){
        cell = [AggregateCell cell];
    }
    
    AggregateGroup *aggregateGroup = [self.aggregateList objectAtIndex:indexPath.section];
    Order *anOrder = [aggregateGroup.list objectAtIndex:indexPath.row];
    
    cell.order = anOrder;
    [cell fillCell];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return AGGREGATE_CELL_HEIGHT;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    AggregateGroup *aggregateGroup = [self.aggregateList objectAtIndex:section];
    AggregateCell *cell = [AggregateCell cell];
    cell.order = aggregateGroup.sumOrder;
    cell.accessoryType = UITableViewCellAccessoryNone;
    [cell fillCell];
    [cell sumFormat];
    cell.payDateLabel.text = aggregateGroup.sectionTitle;
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    AggregateGroup *aggregateGroup = [self.aggregateList objectAtIndex:section];
    return aggregateGroup.sectionTitle;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.datePicker.tag = textField.tag;
    self.timePicker.tag = textField.tag;
    
    if(textField.tag == 1){
        
        self.datePicker.date = self.startDate;
        self.datePickerTitle.title = NSLocalizedString(@"From date(time)", nil);
        [self selectHourPickerByDate:self.startDate];
        
    } else if(textField.tag == 2){
        
        self.datePicker.date = self.endDate;
        self.datePickerTitle.title = NSLocalizedString(@"To date(time)", nil);
        [self selectHourPickerByDate:self.endDate];
        
    }
    return YES;
}

- (void)selectHourPickerByDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute)
                                               fromDate:date];
    NSInteger hour24 = [components hour];
    
    [self.timePicker selectRow:hour24
                   inComponent:0
                      animated:NO];
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    TableInfo *selectedTableInfo = [self.tableStatusList objectAtIndex:indexPath.row];
//    OrderDetailViewController *viewController = [OrderDetailViewController viewControllerFromNib];
//    viewController.tableInfo = selectedTableInfo;
//    [self.navigationController pushViewController:viewController
//                                         animated:YES];
//    
//    [self.tableView deselectRowAtIndexPath:indexPath
//                                  animated:YES];
}

- (IBAction)groupSegmentChanged:(id)sender
{
    [self requestList];
}

- (IBAction)datePickerValueChanged:(UIDatePicker *)sender
{
    [self setDateWithTag:sender.tag];
}

- (void)setDateWithTag:(NSInteger)tag
{
    NSInteger hour24 = [self.timePicker selectedRowInComponent:0];
        
    if(tag == 1){
        self.startDate = [self.datePicker.date dateInHour:hour24];
    } else if(tag == 2){
        self.endDate  = [self.datePicker.date dateInHour:hour24];
    }
    
    
    [self updateDateTextFields];
}

- (void)updateDateTextFields
{
    self.startDateTextField.text = [self.startDate dateHourStringWithDashedFormat];
    self.endDateTextField.text = [self.endDate dateHourStringWithDashedFormat];
}

- (IBAction)doneButtonTouched:(id)sender
{
    [self datePickerValueChanged:self.datePicker];
    [self.view endEditing:YES];
    [self requestList];
}

#pragma mark - UIPickerViewDatasource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch(component){
        case 0:
//            return 10000000;
            return 24;
            break;
        case 1:
            return 2;
            break;
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch(component){
        case 0:
            return [NSString stringWithFormat:@"%02d:00", row];
            break;
        case 1:
            switch(row){
                case 0:
                    return @"AM";
                    break;
                case 1:
                    return @"PM";
                    break;
            }
            break;
    }
    return nil;
}

//- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
//{
//    switch(component){
//        case 0:
//            return 100.0f;
//            break;
//        case 1:
//            return 60.0f;
//            break;
//    }
//    return 0.0f;
//}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self setDateWithTag:pickerView.tag];
}

@end
