//
//  ReportMenuTableViewController.m
//  RushOrder
//
//  Created by Conan on 3/7/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "ReportMenuTableViewController.h"
#import "TableSectionItem.h"

@interface ReportMenuTableViewController ()

@property (strong, nonatomic) NSMutableArray *sectionList;
@property (strong, nonatomic) UIView *logoView;
@property (strong, nonatomic) UIView *headerView;
@end

@implementation ReportMenuTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self != nil){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.sectionList = [NSMutableArray array];
    
    ///////////////////////// Sections /////////////////////////////////////////
    NSMutableArray *sectionItems = [NSMutableArray array];
    
    //Menu
    TableItem *tableItem = [[TableItem alloc] init];
    tableItem.title = NSLocalizedString(@"Payment Summary", nil);
    tableItem.action = @selector(goAggregation:);
    [sectionItems addObject:tableItem];
    //////
    
    //Section
    TableSection *tableSection = [[TableSection alloc] init];
    tableSection.title = NSLocalizedString(@"Sales", nil);
    tableSection.menus = sectionItems;
    [self.sectionList addObject:tableSection];
    //////////
    
    ///////////////////////// Sections /////////////////////////////////////////
//    sectionItems = [NSMutableArray array];
//    
//    //Menu
//    tableItem = [[TableItem alloc] init];
//    tableItem.title = NSLocalizedString(@"Report", nil);
//    tableItem.action = @selector(goAggregation:);
//    [sectionItems addObject:tableItem];
//    //////
//    
//    //Section
//    tableSection = [[TableSection alloc] init];
//    tableSection.title = NSLocalizedString(@"First", nil);
//    tableSection.menus = sectionItems;
//    [self.sectionList addObject:tableSection];
    /////////
    
    
    ////////////////////////////////////////////////////////////////////////////    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
#ifdef FLURRY_ENABLED
    [Flurry endTimedEvent:NSStringFromClass([self class])
           withParameters:nil];
#endif
    
}

- (void)goAggregation:(id)sender
{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];

//    self.title = NSLocalizedString(@"Report", nil);
    
    self.navigationItem.titleView = self.logoView;
    
    self.tableView.tableHeaderView = self.headerView;
    
}

- (UIView *)headerView
{
    if(_headerView == nil){
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 50.0f)];
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 50.0f)];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.text = NSLocalizedString(@"Report", nil);
        titleLabel.font = [UIFont boldSystemFontOfSize:21.0f];
        titleLabel.textColor = [UIColor whiteColor];
        _headerView.backgroundColor = [UIColor darkGrayColor];
        [_headerView addSubview:titleLabel];
    }
    return _headerView;
}

- (UIView *)logoView
{
    if(_logoView == nil){
        _logoView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 41.0, 38.0)];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 41.0f, 38.0f)];
        imageView.image = [UIImage imageNamed:@"logo_register"];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        [_logoView addSubview:imageView];
        
    }
    
    return _logoView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sectionList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    TableSection *tableSection = [self.sectionList objectAtIndex:section];
    return [tableSection.menus count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                      reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
    TableItem *tableItem = [tableSection.menus objectAtIndex:indexPath.row];
    
    cell.textLabel.text = tableItem.title;
    cell.detailTextLabel.text = tableItem.subTitle;
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    TableSection *tableSection = [self.sectionList objectAtIndex:section];
    
    return tableSection.title;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
    TableItem *tableItem = [tableSection.menus objectAtIndex:indexPath.row];
    
    [self performSelector:tableItem.action
               withObject:indexPath];
}

@end
