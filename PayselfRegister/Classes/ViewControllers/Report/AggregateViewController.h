//
//  AggregateViewController.h
//  RushOrder
//
//  Created by Conan on 3/7/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCiPadViewController.h"

@interface AggregateViewController : PCiPadViewController
<
UITextFieldDelegate,
UITableViewDelegate,
UITableViewDataSource,
UIPickerViewDataSource,
UIPickerViewDelegate
>


@end
