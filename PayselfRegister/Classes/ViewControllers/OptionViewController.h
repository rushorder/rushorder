//
//  OptionViewController.h
//  RushOrder
//
//  Created by Conan on 4/21/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "PCiPadViewController.h"

@protocol OptionViewControllerDelegate;
@interface OptionViewController : PCiPadViewController

@property (weak, nonatomic) id<OptionViewControllerDelegate> delegate;

@end

@protocol OptionViewControllerDelegate <NSObject>
- (void)optionViewController:(OptionViewController *)viewController
didChangeOneTouchLocationUpdate:(UISwitch *)aSwitch;

- (void)optionViewController:(OptionViewController *)viewController
didChangeDirectMenuSwitch:(UISwitch *)aSwitch;
@end
