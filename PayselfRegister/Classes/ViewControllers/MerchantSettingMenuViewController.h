//
//  MerchantSettingMenuViewController.h
//  RushOrder
//
//  Created by Conan on 4/2/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableSectionItem.h"
#import "MerchantLocationViewController.h"

@protocol MerchantSettingMenuViewControllerDelegate;

@interface MerchantSettingMenuViewController : UITableViewController
<
MerchantLocationViewControllerDelegate
>

@property (weak, nonatomic) id<MerchantSettingMenuViewControllerDelegate> delegate;
@property (weak, nonatomic) UIPopoverController *popover;

@end

@protocol MerchantSettingMenuViewControllerDelegate<NSObject>
- (BOOL)merchantSettingMenuViewController:(MerchantSettingMenuViewController *)viewController
                            didSelectMenu:(TableItem *)tableItem;

- (void)merchantSettingMenuViewControllerWillChangeToLive:(MerchantSettingMenuViewController *)viewController;

@end