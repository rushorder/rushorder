//
//  OrderListViewController.h
//  RushOrder
//
//  Created by Conan on 5/14/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "MenuOrderDetailViewController.h"
#import "NPBatchTableView.h"

@protocol OrderListViewControllerDelegate;

@interface OrderListViewController : PCViewController
<
UITableViewDataSource,
UITableViewDelegate,
MenuOrderDetailViewControllerDelegate,
UIAlertViewDelegate,
UIActionSheetDelegate,
PullToRefreshing,
UIActionSheetDelegate
>

@property (weak, nonatomic) id <OrderListViewControllerDelegate> delegate;

@property (weak, nonatomic) UIPopoverController *popover;

@end

@protocol OrderListViewControllerDelegate <NSObject>
@optional
- (void)orderListViewController:(OrderListViewController *)orderListViewController didTouchCheckoutButton:(id)sender;
- (void)orderListViewController:(OrderListViewController *)orderListViewController didTouchOrderDoneButton:(id)sender;
- (void)orderListViewController:(OrderListViewController *)orderListViewController didTouchOnlyCheckoutButton:(id)sender;
- (void)orderListViewController:(OrderListViewController *)orderListViewController didTouchCheckinButton:(id)sender;
@end