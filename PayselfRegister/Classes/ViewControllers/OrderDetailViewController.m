//
//  OrderDetailViewController.m
//  RushOrder
//
//  Created by Conan on 3/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "OrderDetailViewController.h"
#import "PCMerchantService.h"
#import "FormatterManager.h"
#import "TableStatusManager.h"
#import "NPProgressView.h"
#import "JSONKit.h"
#import "TableSectionItem.h"
#import "DetailOrderMenuCell.h"
#import "LineItemOptionCell.h"
#import "NPBatchTableView.h"
#import "PCMenuService.h"
#import "MenuManager.h"
#import "PCOrderService.h"
#import "MenuOrderViewController.h"
#import "MenuOrderManager.h"
#import "CheckOutViewController.h"
#import "BixPrintManager.h"
#import "EpsonPrintManager.h"
#import "StarPrintManager.h"
#import "CustomerInfo.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define MINUTE_SPAN     5
#define LIMIT_HOUR      3

#define CART_ORDER_LINEITEM_HEIGHT  40.0f
enum {
    SectionTagCart = 1,
    SectionTagOrder,
    SectionTagAdditionalInfo,
    SectionTagPayment
};

enum {
    ResetFunctionCheckout = 1,
    ResetFunctionDecline
};

@interface OrderDetailViewController ()

@property (strong, nonatomic) IBOutlet UIView *billContainerView;
@property (strong, nonatomic) IBOutlet UIView *paymentSummaryView;
@property (strong, nonatomic) IBOutlet UIView *cartOperationView;
@property (strong, nonatomic) IBOutlet UIView *orderHeaderView;
@property (weak, nonatomic) IBOutlet PCCurrencyTextField *subTotalTextField;
@property (weak, nonatomic) IBOutlet PCCurrencyTextField *taxesTextField;
@property (weak, nonatomic) IBOutlet PCCurrencyTextField *deliveryFeeTextField;
@property (weak, nonatomic) IBOutlet PCCurrencyTextField *serviceFeeTextField;
@property (weak, nonatomic) IBOutlet PCCurrencyTextField *paidTipTextField;
@property (weak, nonatomic) IBOutlet PCCurrencyTextField *totalPaidSumTextField;
@property (weak, nonatomic) IBOutlet PCCurrencyTextField *discountTextField;
@property (weak, nonatomic) IBOutlet PCCurrencyTextField *merchantDiscountTextField;
@property (weak, nonatomic) IBOutlet PCCurrencyTextField *totalTextField;
@property (weak, nonatomic) IBOutlet UILabel *chargedDeliveryFeeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceFeeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountedTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *merchantDiscountedTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *paidTipTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPaidTitleLabel;
@property (strong, nonatomic) NSMutableArray *timeArray;

@property (weak, nonatomic) IBOutlet NPBatchTableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *tableInfoView;
@property (weak, nonatomic) IBOutlet UILabel *tableInfoLabel;
@property (weak, nonatomic) IBOutlet UIView *sumContainer;
@property (strong, nonatomic) IBOutlet UIView *sectionHeaderView;
//@property (strong, nonatomic) NSArray *paymentList;
@property (weak, nonatomic) IBOutlet NPProgressView *payProgressView;
@property (weak, nonatomic) IBOutlet UILabel *grandTotalSumLabel;
@property (weak, nonatomic) IBOutlet UILabel *payAmountSumLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointSumLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointSumTitme;
@property (weak, nonatomic) IBOutlet UILabel *rewardedPointLabel;
@property (weak, nonatomic) IBOutlet UILabel *rewardedPointTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *remaingAmountSumLabel;
@property (weak, nonatomic) IBOutlet NPStretchableButton *processButton;
@property (weak, nonatomic) IBOutlet UILabel *totalTipLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryFeeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryFeeLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;

@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deliveryFeeTitleHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deliveryFeeHeightConstraint;

@property (strong, nonatomic) IBOutlet UIView *cartHeaderView;

@property (weak, nonatomic) IBOutlet NPStretchableButton *manualResetButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *confirmButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *rejectButton;
@property (weak, nonatomic) IBOutlet NPStretchableButton *requestChangeButton;

@property (weak, nonatomic) IBOutlet UIImageView *tableBackgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *orderListTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *ordererLabel;

@property (weak, nonatomic) IBOutlet UILabel *cartSubtotalAmount;
@property (weak, nonatomic) IBOutlet UILabel *cartTaxesAmount;
@property (weak, nonatomic) IBOutlet UILabel *totalAmount;
@property (strong, nonatomic) IBOutlet UIView *forFooterView;
@property (weak, nonatomic) IBOutlet NPStretchableButton *cancelButton1;
@property (weak, nonatomic) IBOutlet NPStretchableButton *cancelButton2;

@property (nonatomic) PCCurrency justBeforeTaxAmount;
@property (nonatomic, readonly) BOOL isAmountChanged;
@property (nonatomic, readonly) BOOL inPopover;
@property (strong, nonatomic) NSMutableArray *sectionList;
@property (nonatomic) BOOL didAppear;
@property (strong, nonatomic) IBOutlet UITableViewCell *additionalInfoCell;
@property (nonatomic) BOOL shouldFocus;
@property (weak, nonatomic) IBOutlet UIImageView *takeoutIcon;
@property (weak, nonatomic) IBOutlet UILabel *orderTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *customerRequestLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressTitleLabel;

@property (strong, nonatomic) IBOutlet UIView *paymentFooterView;
@property (strong, nonatomic) DetailOrderMenuCell *offScreenDetailOrderMenuCell;
@property (strong, nonatomic) LineItemOptionCell *offScreenLineItemOptionCell;

@property (weak, nonatomic) IBOutlet UILabel *orderTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickupTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *preorderImageView;

@property (weak, nonatomic) IBOutlet UILabel *orderTimeLabelCart;
@property (weak, nonatomic) IBOutlet UILabel *pickupTimeTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *pickupTimeIcon;

@property (strong, nonatomic) IBOutlet UIPickerView *timePickerView;
@property (strong, nonatomic) IBOutlet UIToolbar *pickerToolBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *timeInputViewTitleBarButtonItem;
@property (weak, nonatomic) IBOutlet UITextField *pickupTimeTextField;
@property (weak, nonatomic) IBOutlet NPStretchableButton *pickupTimeSelectButton;
@property (weak, nonatomic) IBOutlet UILabel *expectedTimeTitleLabel;
@property (nonatomic) BOOL wantToConfirm;
@property (copy, nonatomic) NSDate *expectedTime;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeContainerHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discountTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *discountTitleTopConstraint;

@property (weak, nonatomic) IBOutlet UILabel *orderCustomerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *orderCustomerPhoto;
@property (weak, nonatomic) IBOutlet UILabel *cartCustomerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cartCustomerPhoto;
@property (weak, nonatomic) IBOutlet UILabel *orderPhoneLabel;

@property (weak, nonatomic) IBOutlet UIButton *mainPrintButton;

@property (strong, nonatomic) IBOutlet UIView *timeSelectionView;
@property (strong, nonatomic) IBOutlet UIView *prepTimeSelectionView;
@property (strong, nonatomic) IBOutlet UIView *nonRoTimeSelectionView;
@property (nonatomic) NSInteger selectedInterval;
@property (weak, nonatomic) IBOutlet UITextView *declineGuideTextView;
@end

@implementation OrderDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)dealloc
{
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)orderUpdated:(NSNotification *)aNoti
{
    if(aNoti.object == self){
        return;
    }
    
    [self reloadData];
}

- (void)tableStatusUpdated:(NSNotification *)aNoti
{
    if(aNoti.object == self){
        return;
    }
    
    if(aNoti.userInfo == nil){
        
        NSUInteger tableIndex = [TABLE.tableList indexOfObject:self.tableInfo];
        if(tableIndex != NSNotFound){
            
            self.tableInfo = [TABLE.tableList objectAtIndex:tableIndex];
            
            if(self.tableInfo.cart != nil){
                [self requestCart];
            } else {
                if(self.tableInfo.order != nil){
                    self.order = self.tableInfo.order;
                    [self requestOrder];
                } else {
                    [self reloadData];
                }
            }
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.declineGuideTextView setTextContainerInset:UIEdgeInsetsZero];
    
//    self.view.translatesAutoresizingMaskIntoConstraints = NO;
//    self.processButton.translatesAutoresizingMaskIntoConstraints = NO;
//    self.cancelButton1.translatesAutoresizingMaskIntoConstraints = NO;
//    self.billContainerView.translatesAutoresizingMaskIntoConstraints = NO;

    [self.processButton applyResizableImageFromCenterForState:UIControlStateNormal|UIControlStateDisabled];
    if(self.inPopover){
        self.cancelButton1.buttonTitle = NSLocalizedString(@"Close", nil);
        self.cancelButton2.buttonTitle = NSLocalizedString(@"Close", nil);
    } else {
        self.cancelButton1.buttonTitle = NSLocalizedString(@"Back", nil);
        self.cancelButton2.buttonTitle = NSLocalizedString(@"Back", nil);
    }
    
    self.title = NSLocalizedString(@"Bill & Payment", nil);


    self.tableView.tableFooterView = self.forFooterView;
    
//    if(self.tableInfo.cart == nil && self.tableInfo.order == nil && self.order == nil){
//        self.shouldFocus = YES;
//    }
    
    if(self.tableInfo.cart != nil){
        [self requestCart];
    } else {
        if(self.tableInfo.order != nil){
            self.order = self.tableInfo.order;
            [self requestOrder];
        } else if(self.order != nil){
            [self requestOrder];
        } else {
            [self reloadData];
        }
    }
    
    
    self.pickupTimeTextField.inputAccessoryView = self.pickerToolBar;
    
    if(self.order.isDelivery){
        if(self.order.isRoService){
            self.pickupTimeTextField.inputView = self.prepTimeSelectionView;
            self.timeInputViewTitleBarButtonItem.title = NSLocalizedString(@"Prep Time", nil);
        } else {
            self.pickupTimeTextField.inputView = self.nonRoTimeSelectionView;
            self.timeInputViewTitleBarButtonItem.title = NSLocalizedString(@"Delivery Time", nil);
        }
    } else {
        self.pickupTimeTextField.inputView = self.timeSelectionView;
    }
    
    self.self.preferredContentSize = CGSizeMake(self.view.frame.size.width
                                                ,self.view.frame.size.height);
}

#pragma mark - UIPickerViewDatasource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.timeArray count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSDictionary *dict = [self.timeArray objectAtIndex:row];
    return [dict objectForKey:@"title"];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self setSelectedValue:row];
}

- (void)setSelectedValue:(NSInteger)index
{
    NSDictionary *dict = [self.timeArray objectAtIndex:index];
    
    NSString *specificTime = [dict objectForKey:@"specificTime"];
    self.pickupTimeTextField.text = specificTime;
    
    self.expectedTime = [dict objectForKey:@"specificNSDate"];
}

- (IBAction)timeInputButtonTouched:(id)sender
{
    if(self.pickupTimeTextField.isFirstResponder){
        [self inputDoneButtonTouched:sender];
    } else {
        [self.pickupTimeTextField becomeFirstResponder];
    }
}

- (void)editButtonTouched:(id)sender
{
    [self pushMenuViewController:CRED.merchant
                   withTableInfo:self.tableInfo];
}

- (IBAction)toolBarCancelButtonTouched:(id)sender
{
    self.wantToConfirm = NO;
    [self.pickupTimeTextField resignFirstResponder];
}

- (IBAction)inputDoneButtonTouched:(id)sender
{
    if(self.pickupTimeTextField.inputView == self.timeSelectionView || self.pickupTimeTextField.inputView == self.prepTimeSelectionView || self.pickupTimeTextField.inputView == self.nonRoTimeSelectionView){
        if(self.order.isDelivery && !self.order.isRoService){
            if(self.selectedInterval == 0){
                
                NSInteger lastUsedDeliveryInterval = [NSUserDefaults standardUserDefaults].lastUsedDeliveryInterval; //minute
                
                lastUsedDeliveryInterval = MAX(lastUsedDeliveryInterval, MINUTE_SPAN);
                
                [self setSelectedValue:((lastUsedDeliveryInterval / MINUTE_SPAN) - 1)];
                
            } else {
                [NSUserDefaults standardUserDefaults].lastUsedDeliveryInterval = self.selectedInterval;
            }
        } else {
            if(self.selectedInterval == 0){
                
                NSInteger lastUsedInterval = [NSUserDefaults standardUserDefaults].lastUsedInterval; //minute
                
                lastUsedInterval = MAX(lastUsedInterval, MINUTE_SPAN);
                
                [self setSelectedValue:((lastUsedInterval / MINUTE_SPAN) - 1)];
                
            } else {
                [NSUserDefaults standardUserDefaults].lastUsedInterval = self.selectedInterval;
            }
        }
        
    } else {
        NSInteger selectedRow = [self.timePickerView selectedRowInComponent:0];
        
        [self setSelectedValue:selectedRow];
        
        if(self.order.isDelivery && !self.order.isRoService){
            [NSUserDefaults standardUserDefaults].lastUsedDeliveryInterval = ((selectedRow + 1) * MINUTE_SPAN);
        } else {
            [NSUserDefaults standardUserDefaults].lastUsedInterval = ((selectedRow + 1) * MINUTE_SPAN);
        }
    }
    
    [self.pickupTimeTextField resignFirstResponder];
    
    if(self.order.isDelivery){
        if(self.order.isRoService){
            self.pickupTimeTextField.inputView = self.prepTimeSelectionView;
        } else {
            self.pickupTimeTextField.inputView = self.nonRoTimeSelectionView;
        }
    } else {
        self.pickupTimeTextField.inputView = self.timeSelectionView;
    }

    
    if(self.wantToConfirm){
        // TODO: Process Confirm
        if(self.order.hasAlcohol){
            [UIAlertView alertWithTitle:NSLocalizedString(@"Please Check ID", nil)
                                message:NSLocalizedString(@"This order includes alcoholic beverages", nil)];
        }
        [self requestUpdateOrder]; //Confirm
    }
    
    
}

- (NSMutableArray *)timeArray
{
    if(_timeArray == nil){
        _timeArray = [NSMutableArray array];
        
        NSInteger count = LIMIT_HOUR * (60 / MINUTE_SPAN);
        
        for(NSInteger i = 0 ; i < count ; i++){
            
            NSDate *now = [NSDate date];
            NSInteger afterMinute = (MINUTE_SPAN * (i + 1));
            NSTimeInterval afterInterval = afterMinute * 60;
            NSDate *afterNow = [now dateByAddingTimeInterval:afterInterval];
            NSMutableString *afterMinuteString = [NSMutableString stringWithString:[NSString stringWithInteger:((i + 1) * MINUTE_SPAN)]];
            [afterMinuteString appendString:@"Min. ("];
            [afterMinuteString appendString:afterNow.hourMinuteString];
            [afterMinuteString appendString:@")"];
            
            NSDictionary *timeDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                      afterMinuteString, @"title",
                                      [NSNumber numberWithInteger:afterMinute], @"afterMinute",
                                      afterNow.hourMinuteString, @"specificTime",
                                      afterNow, @"specificNSDate",
                                      nil];
            
            [_timeArray addObject:timeDict];
        }
    }
    return _timeArray;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([self.timePickerView selectedRowInComponent:0] == 0){
        if(self.order.pickupAfter == 0){
            if(self.order.isDelivery && !self.order.isRoService){
                NSInteger lastUsedDeliveryInterval = [NSUserDefaults standardUserDefaults].lastUsedDeliveryInterval; //minute
                if(lastUsedDeliveryInterval >= MINUTE_SPAN){
                    [self.timePickerView selectRow:((lastUsedDeliveryInterval / MINUTE_SPAN) - 1)
                                       inComponent:0
                                          animated:YES];
                }
            } else {
                NSInteger lastUsedInterval = [NSUserDefaults standardUserDefaults].lastUsedInterval; //minute
                if(lastUsedInterval >= MINUTE_SPAN){
                    [self.timePickerView selectRow:((lastUsedInterval / MINUTE_SPAN) - 1)
                                       inComponent:0
                                          animated:YES];
                }
            }
        } else {
            NSDate *dueDate = [self.order.orderDate dateByAddingTimeInterval:((self.order.pickupAfter + MINUTE_SPAN) * 60)];
            if([dueDate compare:[NSDate date]] == NSOrderedAscending){
                
            } else {
                NSTimeInterval diff = [dueDate timeIntervalSinceDate:[NSDate date]];
                NSInteger crick = ((NSInteger)(diff / 60) / MINUTE_SPAN - 1);
                if(crick < [self.timeArray count]){
                    [self.timePickerView selectRow:crick
                                       inComponent:0
                                          animated:YES];
                } else {
                    [self.timePickerView selectRow:crick
                                       inComponent:([self.timeArray count] - 1)
                                          animated:YES];
                }
            }
        }
    }
    return YES;
}

- (void)pushMenuViewController:(Merchant *)merchant withTableInfo:(TableInfo *)tableInfo
{
    BOOL pushMenuView = YES;
    if([MENUPAN resetGraphWithMerchant:merchant]){
        
    } else {
        // When same merchant
        if(MENUPAN.isMenuFetched && merchant.isAblePayment){
            if([MENUPAN.wholeMenuList count] == 0){
                pushMenuView = NO;
            } else {
                
            }
        } else {
            
        }
    }
    
    if(pushMenuView){
        MenuOrderViewController *viewController = [MenuOrderViewController viewControllerFromNib];
        [MENUORDER resetGraphWithMerchant:merchant];
        viewController.tableInfo = tableInfo;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
//    if(self.shouldFocus){
//        [self.subTotalTextField becomeFirstResponder];
//        self.shouldFocus = NO;
//    }
    self.didAppear = YES;
}

- (void)configureSection
{
    self.sectionList = [NSMutableArray array];
    
    // Line Items in Cart
//    if([self.tableInfo.cart.lineItems count] > 0){
    if(self.tableInfo.cart.isValid){
        TableSection *section = [[TableSection alloc] init];
        section.title = NSLocalizedString(@"Order Placed", nil);
//        section.menus = self.tableInfo.cart.lineItems;
        section.tag = SectionTagCart;
        [self.sectionList addObject:section];
    
        section.menus = [self buildItemization:self.tableInfo.cart.lineItems];
    }
    
    // Line Items in Order
    if(self.tableInfo.order != nil || self.order != nil || !self.tableInfo.cart.isValid){
        TableSection *section = [[TableSection alloc] init];
        section.title = NSLocalizedString(@"Order Confirmed", nil);

        
        NSArray *lineItems = nil;
        
        if(self.isTicketBase){
            lineItems = self.order.lineItems;
        } else {
            lineItems = self.tableInfo.order.lineItems;
        }
        
        section.menus = [self buildItemization:lineItems];
        section.tag = SectionTagOrder;
        [self.sectionList addObject:section];
    }
    
    if(self.order.isTakeout || self.order.isDelivery || [self.order.customerRequest length] > 0){
        TableSection *section = [[TableSection alloc] init];
        section.title = self.order.typeString;
        section.tag = SectionTagAdditionalInfo;
        [self.sectionList addObject:section];
    }
    
    // Payment
    {
        TableSection *section = [[TableSection alloc] init];
        section.title = NSLocalizedString(@"Payment", nil);
        if(self.isTicketBase){
            section.menus = [[NSMutableArray alloc] initWithArray:self.order.paymentList];
        } else {
            section.menus = [[NSMutableArray alloc] initWithArray:self.tableInfo.order.paymentList];
        }
        section.tag = SectionTagPayment;
        [self.sectionList addObject:section];
    }
}

- (NSMutableArray *)buildItemization:(NSArray *)lineItems
{
    NSMutableArray *items = [NSMutableArray array];
    
    for(LineItem *lineItem in lineItems){
        [items addObject:lineItem];
        NSInteger i = 0;
        for(NSDictionary *groupDict in lineItem.menuOptionNamesObj){
            NSArray *selectedOptions = [groupDict objectForKey:@"menu_option_names"];
            NSInteger j = 0;
            for(NSDictionary *optionDict in selectedOptions){
                NSMutableDictionary *optionMutableDict = [NSMutableDictionary dictionaryWithDictionary:optionDict];
                [optionMutableDict setObject:[NSNumber numberWithCurrency:[optionMutableDict currencyForKey:@"option_price"] * lineItem.quantity]
                                      forKey:@"option_price_quantity"];
                if((j == ([selectedOptions count] - 1)) && (i == ([lineItem.menuOptionNamesObj count] - 1))){
                    [optionMutableDict setObject:[NSNumber numberWithBool:YES]
                                          forKey:@"is_last_item"];
                }
                [items addObject:optionMutableDict];
                j++;
            }
            i++;
        }
    }
    
    return items;
}

- (void)tableInfoChanged:(NSNotification *)aNoti
{
    if(aNoti.object == self){
        return;
    }
    
    
    id obj = [aNoti.userInfo objectForKey:@"tableInfo"];
    
    if(obj == nil){
        obj = [aNoti.userInfo objectForKey:@"order"];
        
        if([obj isEqual:self.order]){
            [self reloadData];
        }
        
    } else {
        
        if([obj isEqual:self.tableInfo]){
            self.order = ((TableInfo *)obj).order;
            [self reloadData];
        }
    }
}

- (void)fillOrder
{
    if(self.tableInfo.cart.status == CartStatusSubmitted){
        self.confirmButton.enabled = YES;
        self.requestChangeButton.enabled = YES;
        self.requestChangeButton.hidden = NO;
        self.orderListTitleLabel.text = NSLocalizedString(@"New Order List", nil);
        
        if(self.tableInfo.cart.placedDate != nil){
            self.orderTimeLabelCart.text = [self.tableInfo.cart.placedDate shortDateMinutesString];
        } else {
            self.orderTimeLabelCart.text = NSLocalizedString(@"Not Placed" ,nil);
        }
        
    } else {
        self.confirmButton.enabled = NO;
        self.requestChangeButton.enabled = NO;
        self.requestChangeButton.hidden = YES;
        if(self.tableInfo.cart.status == CartStatusOrdering){
            self.orderTimeLabelCart.text = NSLocalizedString(@"Not Submitted" ,nil);
            self.orderListTitleLabel.text = NSLocalizedString(@"Ordering", nil);
        } else if(self.tableInfo.cart.status == CartStatusFixed){
            self.orderListTitleLabel.text = NSLocalizedString(@"Confirmed Order List", nil);
        } else {
            
        }
    }
    
    if(self.tableInfo.cart.status == CartStatusFixed){
        if(self.tableInfo.order.orderDate != nil){
            self.orderTimeLabel.text = [self.tableInfo.order.orderDate shortDateMinutesString];
        } else {
            self.orderTimeLabel.text = NSLocalizedString(@"Not Placed" ,nil);
        }
    } else {
        //Assume No Cart
        if(self.order.orderDate != nil){
            self.orderTimeLabel.text = [self.order.orderDate shortDateMinutesString];
        } else {
            self.orderTimeLabel.text = NSLocalizedString(@"Not Placed" ,nil);
        }
    }
    
    if([self.tableInfo.cart.customers count] > 0){
        CustomerInfo *customer = [self.tableInfo.cart.customers objectAtIndex:0];
        NSURL *photoURL = customer.photoURL;
        
        NSMutableString *names = [NSMutableString string];
        
        for(CustomerInfo *customer in self.tableInfo.cart.customers){
            if([names length] > 0) [names appendString:@", "];
            [names appendString:customer.name];
        }
        
        self.cartCustomerLabel.text = names;
        [self.cartCustomerPhoto sd_setImageWithURL:photoURL
                                placeholderImage:[UIImage imageNamed:@"placeholder_profile_photo"]];
    } else {
        self.cartCustomerLabel.text = nil;
        self.cartCustomerPhoto.image = nil;
    }
    
    if([self.tableInfo.order.customers count] > 0){
        CustomerInfo *customer = [self.tableInfo.order.customers objectAtIndex:0];
        NSURL *photoURL = customer.photoURL;
        
        NSMutableString *names = [NSMutableString string];
        
        for(CustomerInfo *customer in self.tableInfo.order.customers){
            if([names length] > 0) [names appendString:@", "];
            [names appendString:customer.name];
        }
        
        self.orderCustomerLabel.text = names;
        
        [self.orderCustomerPhoto sd_setImageWithURL:photoURL
                                placeholderImage:[UIImage imageNamed:@"placeholder_profile_photo"]];
    } else {
        self.orderCustomerLabel.text = nil;
        self.orderPhoneLabel.text = nil;
        self.orderCustomerPhoto.image = nil;
    }
    
    if([self.order.customers count] > 0){
        CustomerInfo *customer = [self.order.customers objectAtIndex:0];
        
        if([self.order.receiverName length] > 0){
            self.orderCustomerLabel.text = self.order.receiverName;
        } else {
            NSMutableString *names = [NSMutableString string];
            
            for(CustomerInfo *customer in self.order.customers){
                if([names length] > 0) [names appendString:@", "];
                [names appendString:customer.name];
            }
            
            self.orderCustomerLabel.text = names;
        }
        
        if([self.order.phoneNumber length] > 0){
            self.orderPhoneLabel.text = self.order.phoneNumber;
        }
        
        NSURL *photoURL = customer.photoURL;
        [self.orderCustomerPhoto sd_setImageWithURL:photoURL
                                placeholderImage:[UIImage imageNamed:@"placeholder_profile_photo"]];
    } else {
        if([self.order.receiverName length] > 0){
            self.orderCustomerLabel.text = self.order.receiverName;
            self.orderCustomerPhoto.image = [UIImage imageNamed:@"placeholder_profile_photo"];
        } else {
            self.orderCustomerLabel.text = nil;
            self.orderCustomerPhoto.image = nil;
        }
        
        if([self.order.phoneNumber length] > 0){
            self.orderPhoneLabel.text = self.order.phoneNumber;
        }
    }
    
    [self.orderPhoneLabel updateConstraints];
    
    if([self.tableInfo.cart.staffName length] > 0){
        self.ordererLabel.text = self.tableInfo.cart.staffName;
        self.ordererLabel.hidden = NO;
    } else {
        self.ordererLabel.hidden = YES;
//        self.ordererLabel.text = NSLocalizedString(@"Customer", nil);
    }
    
    BOOL isPickupTime = NO;
    if(self.order.orderType == OrderTypeDelivery){
        self.pickupTimeTitleLabel.text = NSLocalizedString(@"Desired Time", nil);
        isPickupTime = YES;
    } else if(self.order.orderType == OrderTypeTakeout){
        self.pickupTimeTitleLabel.text = NSLocalizedString(@"Desired Time", nil);
        isPickupTime = YES;
    } else {
        
    }
    
    if(self.order.isRoService && self.order.isDelivery){
        self.expectedTimeTitleLabel.text = NSLocalizedString(@"Prep Time", nil);
    } else {
        self.expectedTimeTitleLabel.text = NSLocalizedString(@"Expected Time", nil);
    }
    
    self.preorderImageView.hidden = YES;
    
    if(isPickupTime){
        self.pickupTimeTitleLabel.hidden = NO;
        self.pickupTimeLabel.hidden = NO;
        self.pickupTimeIcon.hidden = NO;
        
        if(self.order.pickupAfter == 0){
            self.pickupTimeLabel.text = NSLocalizedString(@"ASAP", nil);
            self.pickupTimeLabel.backgroundColor = [UIColor clearColor];
            self.pickupTimeLabel.textColor = [UIColor blueColor];
            self.pickupTimeLabel.font = [UIFont systemFontOfSize:16.0f];
            
            if(self.order.isDelivery && !self.order.isRoService){
                NSInteger lastUsedDeliveryInterval = [NSUserDefaults standardUserDefaults].lastUsedDeliveryInterval;
                lastUsedDeliveryInterval = MAX(lastUsedDeliveryInterval, MINUTE_SPAN);
                NSDate *lastUsedDeliveryIntervalDate = [[NSDate date] dateByAddingTimeInterval:(lastUsedDeliveryInterval * 60)];
                self.pickupTimeTextField.placeholder = [lastUsedDeliveryIntervalDate hourMinuteString];
            } else {
                NSInteger lastUsedInterval = [NSUserDefaults standardUserDefaults].lastUsedInterval;
                lastUsedInterval = MAX(lastUsedInterval, MINUTE_SPAN);
                NSDate *lastUsedIntervalDate = [[NSDate date] dateByAddingTimeInterval:(lastUsedInterval * 60)];
                self.pickupTimeTextField.placeholder = [lastUsedIntervalDate hourMinuteString];
            }
            
        } else {
            NSDate *dueDate = [self.order.orderDate dateByAddingTimeInterval:(self.order.pickupAfter * 60)];
            self.preorderImageView.hidden = NO;
            self.pickupTimeLabel.text = [dueDate hourMinuteString];
            self.pickupTimeLabel.backgroundColor = [UIColor yellowColor];
            self.pickupTimeLabel.textColor = [UIColor redColor];
            self.pickupTimeLabel.font = [UIFont boldSystemFontOfSize:16.0f];
            if(self.order.isRoService && self.order.isDelivery){
                NSDate *prepDate = [self.order.orderDate dateByAddingTimeInterval:((self.order.pickupAfter * 60) - (20*60))];
                self.pickupTimeTextField.placeholder = [prepDate hourMinuteString];
            } else {
                self.pickupTimeTextField.placeholder = [dueDate hourMinuteString];
            }
        }
        
        self.pickupTimeTitleLabel.backgroundColor = self.pickupTimeLabel.backgroundColor;
        self.pickupTimeTitleLabel.textColor = self.pickupTimeLabel.textColor;
        
        if(self.order.isRoService && self.order.isDelivery){
            self.pickupTimeTextField.text = [self.order.expectedPreparingDate hourMinuteString];
        } else {
            self.pickupTimeTextField.text = [self.order.expectedDate hourMinuteString];
        }
        self.expectedTimeTitleLabel.hidden = NO;
        self.pickupTimeSelectButton.hidden = NO;
        self.pickupTimeTextField.hidden = NO;
        
        if(self.order.status == OrderStatusPaid){
            self.pickupTimeTextField.enabled = YES;
            self.pickupTimeSelectButton.enabled = YES;
            
            self.pickupTimeTextField.textColor = [UIColor colorWithR:212.0f
                                                                   G:37.0f
                                                                   B:61.0f];
        } else {
            self.pickupTimeTextField.enabled = NO;
            self.pickupTimeSelectButton.enabled = NO;
            self.pickupTimeTextField.textColor = [UIColor colorWithR:192.0f
                                                                   G:127.0f
                                                                   B:138.0f];
        }
        self.timeContainerHeightConstraint.constant = 115.0f;
    } else {
        self.pickupTimeTitleLabel.hidden = YES;
        self.pickupTimeLabel.hidden = YES;
        self.pickupTimeIcon.hidden = YES;
        
        self.expectedTimeTitleLabel.hidden = YES;
        self.pickupTimeSelectButton.hidden = YES;
        self.pickupTimeTextField.hidden = YES;
        
        self.timeContainerHeightConstraint.constant = 45.0f;
        
    }
    
    self.cartSubtotalAmount.text = self.tableInfo.cart.amountString;
    self.cartTaxesAmount.text = self.tableInfo.cart.taxAmountString;
    self.totalAmount.text = self.tableInfo.cart.subtotalAmountString;
    
    if(self.isTicketBase){
        self.tableInfoLabel.text = [NSString stringWithFormat:@"Order No.%lld",
                                    self.order.orderNo];
    } else {
        if(self.tableInfo.status == TableStatusFixed){
            self.tableInfoLabel.text = [NSString stringWithFormat:@"Order No.%lld (%@)",
                                        self.tableInfo.order.orderNo,
                                        self.tableInfo.tableNumber];
        } else {
            self.tableInfoLabel.text = [NSString stringWithFormat:@"#%@ %@",
                                        self.tableInfo.tableNumber,
                                        self.tableInfo.statusString];
        }
    }
    
    self.subTotalTextField.amount = self.order.subTotal;
    self.taxesTextField.amount = self.order.taxes;

    if(self.order.roDiscountedAmount > 0){
        self.discountTextField.amount = -self.order.roDiscountedAmount;
        self.discountTextField.hidden = NO;
        self.discountedTitleLabel.text = NSLocalizedString(@"RushOrder Discount", nil);
    } else {
        self.discountedTitleLabel.text = nil;
        self.discountTextField.hidden = YES;
        self.discountTextField.text = nil;
    }
    
    if(self.order.meDiscountedAmount > 0){
        self.merchantDiscountTextField.amount = -self.order.meDiscountedAmount;
        self.merchantDiscountTextField.hidden = NO;
        self.merchantDiscountedTitleLabel.text = NSLocalizedString(@"Merchant Discount", nil);
    } else {
        self.merchantDiscountedTitleLabel.text = nil;
        self.merchantDiscountTextField.hidden = YES;
        self.merchantDiscountTextField.text = nil;
    }
    
    if(self.order.tips > 0){
        self.paidTipTitleLabel.text = NSLocalizedString(@"Tip", nil);
        self.paidTipTitleLabel.hidden = NO;
        self.paidTipTextField.amount = self.order.tips;
        self.paidTipTextField.hidden = NO;
    } else {
        self.paidTipTitleLabel.text = nil;
        self.paidTipTitleLabel.hidden = YES;
        self.paidTipTextField.text = nil;
        self.paidTipTextField.hidden = YES;
    }
    [self.paidTipTitleLabel updateConstraints];
    
    if(self.order.isDelivery){
        self.chargedDeliveryFeeTitleLabel.text = NSLocalizedString(@"Delivery Fee", nil);
        self.deliveryFeeTextField.hidden = NO;
        self.deliveryFeeTextField.amount = self.order.deliveryFee;
        
    } else {
        self.chargedDeliveryFeeTitleLabel.text = nil;
        self.deliveryFeeTextField.hidden = YES;
        self.deliveryFeeTextField.text = nil;
    }
    
    if(self.order.roServiceFee > 0){
        self.serviceFeeTitleLabel.text = NSLocalizedString(@"Service Fee", nil);
        self.serviceFeeTextField.hidden = NO;
        self.serviceFeeTextField.amount = self.order.roServiceFee;
    } else {
        self.serviceFeeTitleLabel.text = nil;
        self.serviceFeeTextField.hidden = YES;
        self.serviceFeeTextField.text = nil;
    }
    
    self.subTotalTextField.enabled = NO;
    self.taxesTextField.enabled = NO;
    
    self.totalTextField.amount = self.order.total;    
    
    if([self.order.paymentList count] > 0){
        self.totalPaidTitleLabel.text = NSLocalizedString(@"Paid", nil);
        self.totalPaidSumTextField.hidden = NO;
        self.totalPaidSumTextField.amount = self.order.payAmounts;
    } else {
        self.totalPaidTitleLabel.text = nil;
        self.totalPaidSumTextField.hidden = YES;
        self.totalPaidSumTextField.text = nil;
    }
    [self.totalPaidTitleLabel updateConstraints];
    
    self.totalAmountLabel.text = [[NSNumber numberWithCurrency:self.order.total]
                                  currencyString];
    self.totalTipLabel.text = [[NSNumber numberWithCurrency:self.order.tips]
                               currencyString];
    
    // Delivery Fee 가 포함되면 위 Bill과 맞지 않아 일단 제거
    self.deliveryFeeTitleHeightConstraint.constant = 0.0f;
    self.deliveryFeeHeightConstraint.constant = 0.0f;
    self.deliveryFeeTitleLabel.text = nil;
    self.deliveryFeeLabel.text = nil;
    
//    if(self.order.orderType == OrderTypeDelivery){
//        self.deliveryFeeLabel.text = self.order.totalPaidDeliveryFeeAmountsString;
//    } else {
//        self.deliveryFeeTitleHeightConstraint.constant = 0.0f;
//        self.deliveryFeeHeightConstraint.constant = 0.0f;
//        self.deliveryFeeTitleLabel.text = nil;
//        self.deliveryFeeLabel.text = nil;
//    }
    
    if(self.order.discountedAmount > 0){
        self.discountTitleLabel.text = NSLocalizedString(@"Discount", nil);
        self.discountLabel.text = self.order.discountedAmountString;
        self.discountTopConstraint.constant = 2.0f;
        self.discountTitleTopConstraint.constant = 1.0f;
    } else {
        self.discountTitleLabel.text = nil;
        self.discountLabel.text = nil;
        self.discountTopConstraint.constant = 0.0f;
        self.discountTitleTopConstraint.constant = 0.0f;
    }
    
    self.grandTotalSumLabel.text = [[NSNumber numberWithCurrency:self.order.grandTotal]
                                    currencyString];
    self.payAmountSumLabel.text = [[NSNumber numberWithCurrency:self.order.netPayAmounts]
                                   currencyString];
    
#define LINE_SPAN   5.0f
    
//    CGFloat originY = CGRectGetMaxY(self.payAmountSumLabel.frame) + LINE_SPAN;
    
    if(self.order.creditAmounts > 0){
//        self.pointSumLabel.y = originY;
//        self.pointSumTitme.y = originY;
        
        self.pointSumLabel.hidden = NO;
        self.pointSumTitme.hidden = NO;
        
        self.pointSumLabel.text = [[NSNumber numberWithCurrency:self.order.creditAmounts]
                                   pointString];
        
//        originY = CGRectGetMaxY(self.pointSumLabel.frame) + LINE_SPAN;
    } else {
        self.pointSumLabel.hidden = YES;
        self.pointSumTitme.hidden = YES;
    }
    
    if(self.order.rewardedAmount > 0){
//        self.rewardedPointLabel.y = originY;
//        self.rewardedPointTitleLabel.y = originY;
        
        self.rewardedPointLabel.hidden = NO;
        self.rewardedPointTitleLabel.hidden = NO;
        
        self.rewardedPointLabel.text = [[NSNumber numberWithCurrency:self.order.rewardedAmount]
                                   pointString];
        
//        originY = CGRectGetMaxY(self.rewardedPointLabel.frame) + LINE_SPAN;
    } else {
        self.rewardedPointLabel.hidden = YES;
        self.rewardedPointTitleLabel.hidden = YES;
    }
    
//    self.sumContainer.y = originY;
    
    self.remaingAmountSumLabel.text = [[NSNumber numberWithCurrency:self.order.remainingToPay]
                                        currencyString];
    self.payProgressView.progress = self.order.payProgress;
    
//    self.paymentSummaryView.height = CGRectGetMaxY(self.sumContainer.frame);
    
    [self updateButtonTitle];
    
//    if(self.shouldFocus && self.didAppear){
//        [self.subTotalTextField becomeFirstResponder];
//        self.shouldFocus = NO;
//    }
}

- (void)updateButtonTitle
{
    self.processButton.hidden = NO;
    self.processButton.enabled = YES;
    self.declineGuideTextView.hidden = YES;
    
    if(self.isTicketBase){
        switch(self.order.status){
            case OrderStatusDraft:
                self.processButton.buttonTitle = NSLocalizedString(@"Remove", nil);
                break;
            case OrderStatusPaid:
                if(self.order.isRoService && self.order.isDelivery){
                    self.processButton.buttonTitle = NSLocalizedString(@"Confirm", nil); //Accepted
                    // Commented out due to https://app.asana.com/0/11543106091665/193478309812140 (Remove ability for merchants to DECLINE orders)
//                    self.manualResetButton.buttonTitle = NSLocalizedString(@"Decline", nil); //Refused
                    self.manualResetButton.buttonTitle = nil;
                } else {
                    self.processButton.buttonTitle = NSLocalizedString(@"Confirm", nil);
                    // Commented out due to https://app.asana.com/0/11543106091665/193478309812140 (Remove ability for merchants to DECLINE orders)
//                    self.manualResetButton.buttonTitle = NSLocalizedString(@"Decline", nil);
                    self.manualResetButton.buttonTitle = nil;
                }
                break;
            case OrderStatusConfirmed:
            case OrderStatusAccepted:
                if(self.order.isDelivery){
                    if(self.order.isRoService){
                        self.processButton.buttonTitle = NSLocalizedString(@"DRIVER ON THE WAY!", nil);
                        self.processButton.enabled = NO;
                    } else {
                        self.processButton.buttonTitle = NSLocalizedString(@"Delivering", nil);
                    }
                } else if(self.order.isTakeout || CRED.merchant.servicedBy != ServicedByStaff){
                    self.processButton.buttonTitle = NSLocalizedString(@"Ready For Pickup", nil);
                } else {
                    self.processButton.buttonTitle = NSLocalizedString(@"Ready To Serve", nil);
                }
                break;
            case OrderStatusReady:
            case OrderStatusPrepared:
                if(self.order.isRoService && self.order.isDelivery){
                    self.processButton.buttonTitle = NSLocalizedString(@"DELIVERING!", nil);
                    self.processButton.enabled = NO;
                    // TODO:Show Deliverying text instead of this button
                } else {
                    self.processButton.buttonTitle = NSLocalizedString(@"Complete", nil);
                }
                break;
            case OrderStatusFixed:
            case OrderStatusCanceled:
            case OrderStatusDeclined:
            case OrderStatusRefused:
                self.processButton.hidden = YES;
                break;
            default:
                self.processButton.buttonTitle = NSLocalizedString(@"Unknown error!", nil);
                break;
        }
    } else {
        switch(self.tableInfo.status){
            case TableStatusEmpty:
            case TableStatusBillReqeusted:
                self.processButton.buttonTitle = NSLocalizedString(@"Issue Bill", nil);
                break;
            case TableStatusBillIssued:
            case TableStatusPartialPaid:
            case TableStatusCompleted:
                if(self.isAmountChanged){
                    self.processButton.buttonTitle = NSLocalizedString(@"Save Changed Amount", nil);
                    [self.processButton setBackgroundImage:[[UIImage imageNamed:@"button_bg_darkgrey"] resizableImageFromCenter]
                                                  forState:UIControlStateNormal];
//                    [self.processButton setTitleColor:[UIColor whiteColor]
//                                             forState:UIControlStateNormal];
                } else {
                    switch(self.tableInfo.status){
                        case TableStatusBillIssued:
                            self.processButton.buttonTitle = NSLocalizedString(@"Remove Bill", nil);
                            break;
                        case TableStatusPartialPaid:
                            self.processButton.buttonTitle = NSLocalizedString(@"Checkout Manually", nil);
                            break;
                        case TableStatusCompleted:
                            self.processButton.buttonTitle = NSLocalizedString(@"Reset Table", nil);
                            break;
                        default:
                            self.processButton.buttonTitle = NSLocalizedString(@"Unknown error!", nil);
                            break;
                    }
                    
                    if(self.tableInfo.status == TableStatusCompleted){
                        [self.processButton setBackgroundImage:[[UIImage imageNamed:@"button_bg_darkgrey"] resizableImageFromCenter]
                                                      forState:UIControlStateNormal];
                    } else {
                        [self.processButton setBackgroundImage:[[UIImage imageNamed:@"button_bg_grey"] resizableImageFromCenter]
                                                      forState:UIControlStateNormal];
                    }
                }
                break;
            case TableStatusFixed:
            case TableStatusOrdering:
            case TableStatusOrderSubmitted:
                self.processButton.hidden = YES;
                break;
            case TableStatusUnknown:
            default:
                self.processButton.buttonTitle = NSLocalizedString(@"Unknown error!", nil);
                break;
        }
    }
    
    if(self.tableInfo.status == TableStatusBillIssued){
        // Not Complete paid - for resetting table manually
        self.manualResetButton.hidden = NO;
        self.manualResetButton.buttonTitle = NSLocalizedString(@"Checkout Manually", nil);
        self.manualResetButton.tag = ResetFunctionCheckout;
        
        [self.manualResetButton setBackgroundImage:[[UIImage imageNamed:@"button_bg_darkgrey"] resizableImageFromCenter]
                                      forState:UIControlStateNormal];
        
    } else if(self.order.status == OrderStatusPaid){
        
        self.manualResetButton.hidden = YES;
        
        self.declineGuideTextView.hidden = NO;
        
        // Commented out due to https://app.asana.com/0/11543106091665/193478309812140 (Remove ability for merchants to DECLINE orders)
//        self.manualResetButton.hidden = NO;
//        self.manualResetButton.buttonTitle = NSLocalizedString(@"Decline", nil);
//        self.manualResetButton.tag = ResetFunctionDecline;
//        
//        [self.manualResetButton setBackgroundImage:[[UIImage imageNamed:@"button_bg_grey"] resizableImageFromCenter]
//                                          forState:UIControlStateNormal];
        
    } else {
        self.manualResetButton.hidden = YES;
    }
}

- (void)layoutBillView
{
    if(self.isTicketBase){
        self.tableInfoView.backgroundColor = self.order.statusColor;
    } else {
        self.tableInfoView.backgroundColor = self.tableInfo.statusColor;
    }

    // Bill ////////////////////////////////////////////////////////////////////
    UIColor *alertColor = [UIColor darkGrayColor];
    
    if(self.tableInfo.order.status == OrderStatusDraft){
        alertColor = [UIColor redColor];
    }
    
    self.subTotalTextField.textColor = alertColor;
    self.taxesTextField.textColor = alertColor;
    
    NSLayoutConstraint *constraint = nil;
    
    if(self.processButton.hidden){
        if(self.manualResetButton.hidden){
            
            if(self.declineGuideTextView.hidden){
                // total - close
                
                constraint = [self.billContainerView constraintForAttribute:NSLayoutAttributeTop
                                                                   withItem:self.cancelButton1];
                [self.billContainerView removeConstraint:constraint];
                
                constraint = [NSLayoutConstraint constraintWithItem:self.cancelButton1
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.totalPaidTitleLabel
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0f
                                                           constant:8.0f];
                [self.billContainerView addConstraint:constraint];
            } else {
                
                // total - close
                
                constraint = [self.billContainerView constraintForAttribute:NSLayoutAttributeTop
                                                                   withItem:self.declineGuideTextView];
                [self.billContainerView removeConstraint:constraint];
                
                constraint = [NSLayoutConstraint constraintWithItem:self.declineGuideTextView
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.totalPaidTitleLabel
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0f
                                                           constant:8.0f];
                [self.billContainerView addConstraint:constraint];
                
                // total - close
                
                constraint = [self.billContainerView constraintForAttribute:NSLayoutAttributeTop
                                                                   withItem:self.cancelButton1];
                [self.billContainerView removeConstraint:constraint];
                
                constraint = [NSLayoutConstraint constraintWithItem:self.cancelButton1
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.declineGuideTextView
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0f
                                                           constant:8.0f];
                [self.billContainerView addConstraint:constraint];
                
            }
            
        } else {
            // total - checkout
            
            constraint = [self.billContainerView constraintForAttribute:NSLayoutAttributeTop
                                                               withItem:self.manualResetButton];
            [self.billContainerView removeConstraint:constraint];
            
            constraint = [NSLayoutConstraint constraintWithItem:self.manualResetButton
                                                      attribute:NSLayoutAttributeTop
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:self.totalPaidTitleLabel
                                                      attribute:NSLayoutAttributeBottom
                                                     multiplier:1.0f
                                                       constant:8.0f];
            [self.billContainerView addConstraint:constraint];
            
            if(!self.declineGuideTextView.hidden){
                
                // total - close
                
                constraint = [self.billContainerView constraintForAttribute:NSLayoutAttributeTop
                                                                   withItem:self.declineGuideTextView];
                [self.billContainerView removeConstraint:constraint];
                
                constraint = [NSLayoutConstraint constraintWithItem:self.declineGuideTextView
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.manualResetButton
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0f
                                                           constant:8.0f];
                [self.billContainerView addConstraint:constraint];
                
                // total - close
                
                constraint = [self.billContainerView constraintForAttribute:NSLayoutAttributeTop
                                                                   withItem:self.cancelButton1];
                [self.billContainerView removeConstraint:constraint];
                
                constraint = [NSLayoutConstraint constraintWithItem:self.cancelButton1
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.declineGuideTextView
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0f
                                                           constant:8.0f];
                [self.billContainerView addConstraint:constraint];
                
            }
            
        }
    } else {
        if(self.manualResetButton.hidden){
            
            
            if(self.declineGuideTextView.hidden){
                // total - close
                
                constraint = [self.billContainerView constraintForAttribute:NSLayoutAttributeTop
                                                                   withItem:self.cancelButton1];
                [self.billContainerView removeConstraint:constraint];
                
                constraint = [NSLayoutConstraint constraintWithItem:self.cancelButton1
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.processButton
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0f
                                                           constant:8.0f];
                [self.billContainerView addConstraint:constraint];
            } else {
                
                // total - close
                
                constraint = [self.billContainerView constraintForAttribute:NSLayoutAttributeTop
                                                                   withItem:self.declineGuideTextView];
                [self.billContainerView removeConstraint:constraint];
                
                constraint = [NSLayoutConstraint constraintWithItem:self.declineGuideTextView
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.processButton
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0f
                                                           constant:8.0f];
                [self.billContainerView addConstraint:constraint];
                
                // total - close
                
                constraint = [self.billContainerView constraintForAttribute:NSLayoutAttributeTop
                                                                   withItem:self.cancelButton1];
                [self.billContainerView removeConstraint:constraint];
                
                constraint = [NSLayoutConstraint constraintWithItem:self.cancelButton1
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.declineGuideTextView
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1.0f
                                                           constant:8.0f];
                [self.billContainerView addConstraint:constraint];
                
            }
            
            
        } else {
            // current
        }
    }
    
    if(self.manualResetButton.hidden){
        if(!self.processButton.hidden){
            // processbutton vertical space to total textField
            NSLayoutConstraint *constraint = [self.billContainerView constraintForAttribute:NSLayoutAttributeTop
                                                                                   withItem:self.processButton];
            [self.billContainerView removeConstraint:constraint];
            
            constraint = [NSLayoutConstraint constraintWithItem:self.processButton
                                                      attribute:NSLayoutAttributeTop
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:self.totalPaidTitleLabel
                                                      attribute:NSLayoutAttributeBottom
                                                     multiplier:1.0f
                                                       constant:8.0f];
            [self.billContainerView addConstraint:constraint];
        }
    }
    
    [self.billContainerView setNeedsUpdateConstraints];
    
    constraint = [self.paymentSummaryView constraintForAttribute:NSLayoutAttributeTop
                                                                            withItem:self.sumContainer];
    [self.paymentSummaryView removeConstraint:constraint];
    
    // Order ///////////////////////////////////////////////////////////////////
    if(self.pointSumLabel.hidden){
        if(self.rewardedPointLabel.hidden){
            constraint = [NSLayoutConstraint constraintWithItem:self.sumContainer
                                                      attribute:NSLayoutAttributeTop
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:self.payAmountSumLabel
                                                      attribute:NSLayoutAttributeBottom
                                                     multiplier:1.0f
                                                       constant:3.0f];
            [self.paymentSummaryView addConstraint:constraint];
        } else {
            constraint = [self.paymentSummaryView constraintForAttribute:NSLayoutAttributeTop
                                                                withItem:self.rewardedPointLabel];
            [self.paymentSummaryView removeConstraint:constraint];
            
        
            constraint = [NSLayoutConstraint constraintWithItem:self.rewardedPointLabel
                                                      attribute:NSLayoutAttributeTop
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:self.payAmountSumLabel
                                                      attribute:NSLayoutAttributeBottom
                                                     multiplier:1.0f
                                                       constant:0.0f];
            [self.paymentSummaryView addConstraint:constraint];
            
            constraint = [NSLayoutConstraint constraintWithItem:self.sumContainer
                                                      attribute:NSLayoutAttributeTop
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:self.rewardedPointLabel
                                                      attribute:NSLayoutAttributeBottom
                                                     multiplier:1.0f
                                                       constant:3.0f];
            [self.paymentSummaryView addConstraint:constraint];
        }
    } else {
        if(self.rewardedPointLabel.hidden){
            // move sum container View to pointSumLabel
            constraint = [NSLayoutConstraint constraintWithItem:self.sumContainer
                                                      attribute:NSLayoutAttributeTop
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:self.pointSumLabel
                                                      attribute:NSLayoutAttributeBottom
                                                     multiplier:1.0f
                                                       constant:3.0f];
            [self.paymentSummaryView addConstraint:constraint];
        } else {
            // move sum container view to rewardedPointLabel
            constraint = [NSLayoutConstraint constraintWithItem:self.sumContainer
                                                      attribute:NSLayoutAttributeTop
                                                      relatedBy:NSLayoutRelationEqual
                                                         toItem:self.rewardedPointLabel
                                                      attribute:NSLayoutAttributeBottom
                                                     multiplier:1.0f
                                                       constant:3.0f];
            [self.paymentSummaryView addConstraint:constraint];
        }
    }
    
    constraint = [self.cartOperationView constraintForAttribute:NSLayoutAttributeTop
                                                       withItem:self.cancelButton2];
    [self.cartOperationView removeConstraint:constraint];
    
    if(self.requestChangeButton.hidden){
        constraint = [NSLayoutConstraint constraintWithItem:self.cancelButton2
                                                                      attribute:NSLayoutAttributeTop
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.rejectButton
                                                                      attribute:NSLayoutAttributeBottom
                                                                     multiplier:1.0f
                                                                       constant:8.0f];
        [self.cartOperationView addConstraint:constraint];
        
    } else {
        constraint = [NSLayoutConstraint constraintWithItem:self.cancelButton2
                                                  attribute:NSLayoutAttributeTop
                                                  relatedBy:NSLayoutRelationEqual
                                                     toItem:self.requestChangeButton
                                                  attribute:NSLayoutAttributeBottom
                                                 multiplier:1.0f
                                                   constant:8.0f];
        [self.cartOperationView addConstraint:constraint];
    }
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)resizeTableView
{
    if(self.inPopover){
        self.tableView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
        CGRect tableViewFrame = self.tableView.frame;
        CGFloat maxHeight = 650.0f;
        
        CGFloat totalHeight = maxHeight;
        
        if([self.order.paymentList count] == 0){
            
            totalHeight = 0;
            
            if(self.tableInfo.cart.isValid){
                totalHeight += self.cartHeaderView.height + ([self.tableInfo.cart.lineItems count] * CART_ORDER_LINEITEM_HEIGHT) + self.cartOperationView.height;
            }
            
            if(!self.tableInfo.cart.isValid || self.tableInfo.order != nil || self.order != nil){
                NSInteger lineItemCount = [self.tableInfo.order.lineItems count];
                if(self.isTicketBase){
                    lineItemCount = [self.order.lineItems count];
                }
                totalHeight += self.orderHeaderView.height + (lineItemCount * CART_ORDER_LINEITEM_HEIGHT) + self.billContainerView.height;
            }
            
            totalHeight = MIN(maxHeight, totalHeight);

        } else {

        }
        
        tableViewFrame.size.height = totalHeight;
        
        self.tableView.frame = tableViewFrame;
        
        self.preferredContentSize = CGSizeMake(self.billContainerView.frame.size.width,
                                                      CGRectGetMaxY(self.tableView.frame));
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)requestOrder
{
    PCSerial orderNo = self.tableInfo.order.orderNo;
    
    if(self.isTicketBase){
        orderNo = self.order.orderNo;
    }
    
    RequestResult result = RRFail;
    result = [MERCHANT requestOrder:orderNo
                    removeCompleted:!self.tableInfo.isWrappingMode
                    completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              self.order.paymentList = [response objectForKey:@"payments"];
//                              self.order.lineItems = [response objectForKey:@"line_items"];
                              
                              NSDictionary *orderDict = response.data;
                              [self.order updateWithDictionary:orderDict
                                                      merchant:nil
                                                       menuPan:MENUPAN.wholeSerialMenuList];
                              
                              [self.order updateSumWithPayments:self.order.paymentList];
                              [self.tableInfo configureTableStatus];
                              
//                              if(self.order.subTotal == 0){
//                                  self.shouldFocus = YES;
//                              }
                              
                              NSDictionary *userInfo = nil;
                              
                              if(self.isTicketBase){
                                  userInfo = [NSDictionary dictionaryWithObject:self.order forKey:@"order"];
                              } else {
                                  userInfo = [NSDictionary dictionaryWithObject:self.tableInfo forKey:@"tableInfo"];
                              }
                              
                              [self orderUpdated:nil];
                              [[NSNotificationCenter defaultCenter] postNotificationName:OrderUpdatedNotification
                                                                                  object:self
                                                                                userInfo:userInfo];
                              break;
                          }
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                              [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting table information", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sectionList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    TableSection *tableSection = [self.sectionList objectAtIndex:section];
    if(tableSection.tag == SectionTagAdditionalInfo){
        return 1;
    } else {
        return [tableSection.menus count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
    
    if(tableSection.tag == SectionTagAdditionalInfo){
//        [self.additionalInfoCell drawBorder];
        return self.additionalInfoCell;
    }
    
    id cellItem = [tableSection.menus objectAtIndex:indexPath.row];
    
    if([cellItem isKindOfClass:[Payment class]]){
        return [self tableView:tableView cellForRowAtIndexPath:indexPath payment:cellItem];
    } else if([cellItem isKindOfClass:[LineItem class]]){
        return [self tableView:tableView cellForRowAtIndexPath:indexPath lineItem:cellItem];
    } else if([cellItem isKindOfClass:[NSDictionary class]]){
        return [self tableView:tableView cellForRowAtIndexPath:indexPath optionDict:cellItem];
    } else {
        // Prevent crash!
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
        return cell;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath payment:(Payment *)payment
{
    static NSString *CellIdentifier = @"CardPaymentCell";
    
    CardPaymentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil){
        cell = [CardPaymentCell cell];
//        cell.backgroundView = [[UIView alloc] initWithFrame:cell.bounds];
    }
    cell.delegate = self;
    cell.payment = payment;
    [cell fillPayment];
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath lineItem:(LineItem *)lineItem
{
    NSString *cellIdentifier = @"DetailOrderMenuCell";
    
    DetailOrderMenuCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        cell = [DetailOrderMenuCell cell];
    }
    
    cell.lineItem = lineItem;

    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath optionDict:(NSDictionary *)optionDict
{
    NSString *cellIdentifier = @"LineItemOptionCell";
    
    LineItemOptionCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        cell = [LineItemOptionCell cell];
    }
    //{"option_name":"Side Menu 2","option_price":200}
//    cell.menuLabel.text = [optionDict objectForKey:@"option_name"];
//    cell.lineItem = lineItem;
    
    cell.optionDict = optionDict;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if([cell isKindOfClass:[DetailOrderMenuCell class]]){
        cell.backgroundColor = [UIColor clearColor];
//    }
}

- (void)tableView:(UITableView *)tableView didTouchPrintButtonAtIndexPath:(NSIndexPath *)indexPath
{
    Order *tempOrder = nil;
    if(self.tableInfo == nil){
        //Pickup
        tempOrder = self.order;
    } else {
        tempOrder = self.tableInfo.order;
    }
    
    
    long bixRtn = 0;
    
    switch(APP.selectedMakerCode){
        case PrinterMakerEpson:
            bixRtn = [EPSON printReceipt:[[ReceiptWrapper alloc] initWithOrder:tempOrder
                                                              withPaymentIndex:indexPath.row]
                                 atIndex:indexPath.row];
            break;
        case PrinterMakerStar:
            bixRtn = [STAR printReceipt:[[ReceiptWrapper alloc] initWithOrder:tempOrder
                                                             withPaymentIndex:indexPath.row]
                                atIndex:indexPath.row];
            break;
        case PrinterMakerBixolon:
            bixRtn = [BIX printReceipt:[[ReceiptWrapper alloc] initWithOrder:tempOrder
                                                            withPaymentIndex:indexPath.row]
                               atIndex:indexPath.row];
            break;
        default:
            bixRtn = RUSHORDER_PRINTER_NOT_SET;
            break;
    }
    [self treatPrintError:bixRtn];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableSection *section = [self.sectionList objectAtIndex:indexPath.section];
    
    switch(section.tag){
        case SectionTagCart:
        case SectionTagOrder:
        {
            id cellItem = [section.menus objectAtIndex:indexPath.row];
            
            if([cellItem isKindOfClass:[LineItem class]]){
                self.offScreenDetailOrderMenuCell.lineItem = cellItem;
                [self.offScreenDetailOrderMenuCell setNeedsUpdateConstraints];
                [self.offScreenDetailOrderMenuCell setNeedsLayout];
                [self.offScreenDetailOrderMenuCell updateConstraints];
                self.offScreenDetailOrderMenuCell.menuLabel.preferredMaxLayoutWidth = self.offScreenDetailOrderMenuCell.menuLabel.width;
                CGSize cellSize = [self.offScreenDetailOrderMenuCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
                return cellSize.height;
            } else {
                self.offScreenLineItemOptionCell.optionDict = cellItem;
                [self.offScreenDetailOrderMenuCell setNeedsUpdateConstraints];
                [self.offScreenDetailOrderMenuCell updateConstraints];
                [self.offScreenDetailOrderMenuCell setNeedsLayout];
                self.offScreenDetailOrderMenuCell.menuLabel.preferredMaxLayoutWidth = self.offScreenDetailOrderMenuCell.menuLabel.width;
                CGSize cellSize = [self.offScreenLineItemOptionCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
                return cellSize.height;
            }
        }
            break;
        case SectionTagAdditionalInfo:
        {
            if([self.order.customerRequest length] > 0){
                self.customerRequestLabel.text = self.order.customerRequest;
            } else {
                self.customerRequestLabel.text = NSLocalizedString(@"No Additional Request", nil);
            }
            
            if(self.order.isDelivery){
                self.addressLabel.text = self.order.address;
                self.phoneNumberLabel.text = self.order.phoneNumber;
            } else {
                self.phoneNumberTitleLabel.text = nil;
                self.phoneNumberLabel.text = nil;
                self.addressLabel.text = nil;
                self.addressTitleLabel.text = nil;
            }
            
            CGFloat cellHeight = [self.additionalInfoCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
            PCLog(@"cellHeight %f", cellHeight);
            return cellHeight;
        }
            break;
        case SectionTagPayment:{
            Payment *payment = [section.menus objectAtIndex:indexPath.row];
            if(payment.creditAmount > 0 || payment.rewardedAmount > 0){
                return 105.0f;
            } else {
                return 85.0f;
            }
        }
            break;
    }
    return 10.0f; // For detecting errors
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    TableSection *tableSection = [self.sectionList objectAtIndex:section];
    
    switch(tableSection.tag){
        case SectionTagPayment:
        {
            TableSection *tableSection = [self.sectionList objectAtIndex:section];
            if([tableSection.menus count] > 0){
#if SHOW_PAYMENT_SUMMARY_VIEW
                return self.paymentSummaryView;
#else
                return self.sectionHeaderView;
#endif
            } else {
                return nil;
            }
        }
            break;
        case SectionTagOrder:
            if(self.tableInfo.order != nil || self.order != nil || !self.tableInfo.cart.isValid){
                
                if(self.order.orderType == OrderTypeTakeout){
                    self.takeoutIcon.image = [UIImage imageNamed:@"icon_takeout_black"];
                    self.orderTypeLabel.text = NSLocalizedString(@"Take-out", nil);
                } else if(self.order.orderType == OrderTypeDelivery){
                    self.takeoutIcon.image = [UIImage imageNamed:@"icon_delivery_black"];
                    self.orderTypeLabel.text = NSLocalizedString(@"Delivery", nil);
                } else {
                    self.takeoutIcon.image = [UIImage imageNamed:@"icon_table_black"];
                    self.orderTypeLabel.text = NSLocalizedString(@"Dine-in", nil);
                }
//                self.statusLabel.text = self.order.hrStatusPickupString;
                
                return self.orderHeaderView;
            } else
                return nil;
            break;
        case SectionTagCart:
            return self.cartHeaderView;
            break;
        default:
            return nil;
            break;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    TableSection *tableSection = [self.sectionList objectAtIndex:section];
    
    switch(tableSection.tag){
        case SectionTagCart:
            return self.cartOperationView;
            break;
        case SectionTagOrder:
            if(self.tableInfo.order != nil || self.order != nil || !self.tableInfo.cart.isValid)
                return self.billContainerView;                
            else
                return nil;
            break;
        case SectionTagPayment:
        {
            TableSection *tableSection = [self.sectionList objectAtIndex:section];
            if([tableSection.menus count] > 0){
                return self.paymentFooterView;
            } else {
                return nil;
            }
        }
            break;
        default:
            return nil;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    TableSection *tableSection = [self.sectionList objectAtIndex:section];
    
    switch(tableSection.tag){
        case SectionTagPayment:
        {
            TableSection *tableSection = [self.sectionList objectAtIndex:section];
            if([tableSection.menus count] > 0){
#if SHOW_PAYMENT_SUMMARY_VIEW
                CGSize headerSize = [self.paymentSummaryView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
                return headerSize.height;
#else
                return self.sectionHeaderView.height;
#endif
            } else {
                return 0.0f;
            }
        }
            break;
        case SectionTagOrder:
            if(self.tableInfo.order != nil || self.order != nil || !self.tableInfo.cart.isValid){
                self.orderHeaderView.height = [self.orderHeaderView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
                return self.orderHeaderView.height;
            } else
                return 0.0f;
            break;
        case SectionTagCart:
            return self.cartHeaderView.height;
            break;
        default:
            return 0.0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    TableSection *tableSection = [self.sectionList objectAtIndex:section];
    
    switch(tableSection.tag){
        case SectionTagCart:{
            CGSize footerSize = [self.cartOperationView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
            return footerSize.height;
        }
            break;
        case SectionTagOrder:
            if(self.tableInfo.order != nil || self.order != nil || !self.tableInfo.cart.isValid){
                [self.billContainerView updateConstraints];
                return [self.billContainerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
            } else {
                return 0.0f;
            }
            break;
        case SectionTagPayment:
        {
            TableSection *tableSection = [self.sectionList objectAtIndex:section];
            if([tableSection.menus count] > 0){
                return self.paymentFooterView.height;
            } else {
                return 0.0f;
            }
        }
            break;
        default:
            return 0.0f;
            break;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Custom initialization
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tableInfoChanged:)
                                                 name:TableInfoChangedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tableStatusUpdated:)
                                                 name:TableListUpdatedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orderUpdated:)
                                                 name:OrderUpdatedNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NSLocalizedString(@"Refund", nil);
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
    
    id cellItem = [tableSection.menus objectAtIndex:indexPath.row];
    
    if([cellItem isKindOfClass:[Payment class]]){
        
        Payment *castedPayment = (Payment *)cellItem;
        
        if(castedPayment.status == PaymentStatusSuccess){
            return YES;
        } else {
            return NO;
        }
    }
    
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
    
    id cellItem = [tableSection.menus objectAtIndex:indexPath.row];
    
    if([cellItem isKindOfClass:[Payment class]]){
        
        Payment *castedPayment = (Payment *)cellItem;
        
        if(castedPayment.status == PaymentStatusSuccess){
            // Do refund
            [self requestRefund:castedPayment];
        }
    }
}

- (void)requestRefund:(Payment *)payment
{
    RequestResult result = RRFail;
    result = [ORDER requestRefund:payment.paymentNo
                  completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              [payment updatePayment:response];
                              
                              [self.order updateSumWithPayments:self.order.paymentList];
                              [self.tableInfo configureTableStatus];
                              
                              [self cancelButtonTouched:self];
                              
                              NSDictionary *userInfo;
                              
                              if(self.isTicketBase){
                                  userInfo = [NSDictionary dictionaryWithObject:self.order forKey:@"order"];
                              } else {
                                  userInfo = [NSDictionary dictionaryWithObject:self.tableInfo forKey:@"tableInfo"];
                              }
                              
                              [self orderUpdated:nil];
                              [[NSNotificationCenter defaultCenter] postNotificationName:OrderUpdatedNotification
                                                                                  object:self
                                                                                userInfo:userInfo];
                          }
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while refunding", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}



- (IBAction)progressButtonTouched:(id)sender
{    
    [self.view endEditing:YES];
    
    if(self.isTicketBase){
        switch(self.order.status){
            case OrderStatusDraft:{
                UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Do you want remove this order? Removed order cannot recover", nil)
                                                                         delegate:self
                                                                cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                           destructiveButtonTitle:NSLocalizedString(@"Remove", nil)
                                                                otherButtonTitles:nil];
                actionSheet.tag = 101;
                [actionSheet showInView:self.view];
            }
                break;
            case OrderStatusPaid:
                if((self.order.orderType == OrderTypeDelivery) || (self.order.orderType == OrderTypeTakeout)){
                    if(self.expectedTime == nil){
                        self.wantToConfirm = YES;
                        [self.pickupTimeTextField becomeFirstResponder];
                        [self.tableView setContentOffset:CGPointMake(0.0f, 140.0f)
                                                animated:NO];
                    } else {
                        if(self.order.hasAlcohol){
                            [UIAlertView alertWithTitle:NSLocalizedString(@"Please Check ID", nil)
                                                message:NSLocalizedString(@"This order includes alcoholic beverages", nil)];
                        }
                        [self requestUpdateOrder]; //Confirm
                    }
                } else {
                    if(self.order.hasAlcohol){
                        [UIAlertView alertWithTitle:NSLocalizedString(@"Please Check ID", nil)
                                            message:NSLocalizedString(@"This order includes alcoholic beverages", nil)];
                    }
                    [self requestUpdateOrder]; //Confirm
                }
                break;
            case OrderStatusConfirmed:
            case OrderStatusAccepted:
//                if (self.order.isRoService && self.order.isDelivery){
//                    [self updateOrderStatus:OrderStatusPrepared];
//                } else {
//                    [self updateOrderStatus:OrderStatusReady];
//                }
                if(self.order.isRoService && self.order.isDelivery){
                    [UIAlertView alertWithTitle:NSLocalizedString(@"Driver is On The Way!",nil)
                                        message:NSLocalizedString(@"Please call us if you need assistance.", nil)];
                } else {
                    [self updateOrderStatus:OrderStatusReady];
                }
                break;
            case OrderStatusPrepared:
                [UIAlertView alertWithTitle:NSLocalizedString(@"Delivery in Progress",nil)
                                    message:NSLocalizedString(@"Order status will automatically change once it's been delivered.", nil)];
                break;
            case OrderStatusReady:
                if (self.order.isRoService && self.order.isDelivery){
                    [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot Make This Order Completed",nil)
                                        message:NSLocalizedString(@"Sorry! You should wait until the driver marks this order Delivered(Completed)", nil)];
                } else {
                    [self updateOrderStatus:OrderStatusFixed];
                }
                break;
            default:
                [self updateOrderStatus:OrderStatusFixed];
                break;
        }
    } else {
        
        
        switch(self.tableInfo.status){
            case TableStatusEmpty:
                if(self.subTotalTextField.amount == 0){
                    [UIAlertView alertWithTitle:NSLocalizedString(@"Subtotal Amount is $0.00", nil)
                                        message:NSLocalizedString(@"Enter the subtotal amount.", nil)
                                       delegate:self
                                            tag:2];
                } else {
                    [self requestNewOrder];
                }
                break;
            case TableStatusBillReqeusted:
                if(self.subTotalTextField.amount == 0){
                    [UIAlertView alertWithTitle:NSLocalizedString(@"Subtotal Amount is $0.00", nil)
                                        message:NSLocalizedString(@"Enter the subtotal amount.", nil)
                                       delegate:self
                                            tag:2];
                } else {
                    [self requestNewOrder];
                }
                break;
            case TableStatusPartialPaid:
                [self manuallyPaidNResetButtonTocuehd:sender];
                break;
            case TableStatusBillIssued:
            case TableStatusCompleted:
                if(self.isAmountChanged){
                    [self requestUpdateOrder];
                } else {
                    switch(self.tableInfo.status){
                        case TableStatusBillIssued:{
                            [UIAlertView askWithTitle:NSLocalizedString(@"This Bill Will Be Removed", nil)
                                              message:NSLocalizedString(@"Removed bill cannot recover. Do you want to proceed?", nil)
                                             delegate:self
                                                  tag:301];
                        }
                            break;
                        case TableStatusPartialPaid:
                            [self pushCompleteOptionViewController];
                            break;
                        case TableStatusCompleted:
                            [self requestUpdateOrder:YES];
                            break;
                        default:
                            // Do nothing
                            break;
                    }
                }
                break;
            case TableStatusUnknown:
            default:
                // Do nothing
                [self updateOrderStatus:OrderStatusFixed];
                break;
        }
    }
}

- (void)requestNewOrder
{
    if(self.order == nil){
        self.order = [[Order alloc] init];
    }
    
    self.order.subTotal = self.subTotalTextField.amount;
    self.order.taxes = self.taxesTextField.amount;
    self.order.merchantNo = self.tableInfo.merchantNo;
    self.order.tableNumber = self.tableInfo.tableNumber;
    self.order.bfStatus = self.order.status;
    self.order.status = OrderStatusConfirmed;
    
    RequestResult result = RRFail;
    result = [MERCHANT requestNewOrder:self.order
                       completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              [self.order updateWithDictionary:response.data
                                                      merchant:nil
                                                       menuPan:MENUPAN.wholeSerialMenuList
                                                  withBfStatus:NO];
                              self.tableInfo.order = self.order;
                              
                          {
                              NSDictionary *userInfo = [NSDictionary dictionaryWithObject:self.tableInfo forKey:@"tableInfo"];
                              [[NSNotificationCenter defaultCenter] postNotificationName:TableInfoChangedNotification
                                                                                  object:self
                                                                                userInfo:userInfo];
                          }
                              
                              [self reloadData];
                              
                              [self performSelector:@selector(dismissPopover:)
                                         withObject:self
                                         afterDelay:1.2f];
                                                            
                              break;
                          case ResponseErrorExists:
                              // TODO: check the money amount. if same go on, or the status is draft update or back and show order
                              [UIAlertView alert:NSLocalizedString(@"The order already exists", nil)];
                              
                              [self requestOrder];
                              
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                              [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while creating new order", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)pushCompleteOptionViewController
{
    CompleteOptionViewController *viewController = [CompleteOptionViewController viewControllerFromNib];
    viewController.delegate = self;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)requestUpdateOrder
{
    [self requestUpdateOrder:NO];
}

- (void)requestUpdateOrder:(BOOL)isReset
{
    [self requestUpdateOrder:isReset reason:nil];
}

- (void)requestUpdateOrder:(BOOL)isReset reason:(NSString *)reason
{
    if(self.order == nil){
        PCError(@"Update order need order");
        return;
    }
    
//    if(self.subTotalTextField.amount == 0){
//        
//    }
    
    Order *updateOrder = [self.order copy];
    
    updateOrder.subTotal = self.subTotalTextField.amount;
    updateOrder.taxes = self.taxesTextField.amount;
    updateOrder.bfStatus = updateOrder.status;
    updateOrder.status = isReset ? OrderStatusFixed : OrderStatusConfirmed;
    
//    if(isReset){
//        updateOrder.status = OrderStatusFixed;
//    } else if (self.order.isRoService && updateOrder.isDelivery){
//        updateOrder.status = OrderStatusAccepted;
//    } else {
//        updateOrder.status = OrderStatusConfirmed;
//    }
    
    if(isReset){
        updateOrder.status = OrderStatusFixed;
    } else {
        updateOrder.status = OrderStatusConfirmed;
    }
    
    updateOrder.completeReasonJson = reason;
    
//    if(updateOrder.status == OrderStatusConfirmed){
//        if(self.expectedTime != nil){
//            updateOrder.expectedDate = self.expectedTime;
//        }
//    } else if(updateOrder.status == OrderStatusAccepted){
//        if(self.expectedTime != nil){
//            updateOrder.expectedPreparingDate = self.expectedTime;
//        }
//    }
    
    if(self.expectedTime != nil){
        if (self.order.isRoService && updateOrder.isDelivery){
            updateOrder.expectedPreparingDate = self.expectedTime;
            
//            NSDate *now = [NSDate date];
//            NSTimeInterval afterInterval = CRED.merchant.averageDeliveryTime * 60;
//            NSDate *afterNow = [now rount5dateByAddingTimeInterval:afterInterval];
//            updateOrder.expectedDate = afterNow;
        } else {
            updateOrder.expectedDate = self.expectedTime;
        }
    }
    
    [self requestUpdateWithOrder:updateOrder];
    
}

- (void)updateOrderStatus:(OrderStatus)newStatus
{
    [self updateOrderStatus:newStatus withReason:nil];
}

- (void)updateOrderStatus:(OrderStatus)newStatus withReason:(NSString *)reason
{
    if(self.order == nil){
        PCError(@"Update order should have order already");
        return;
    }
    
    if(self.subTotalTextField.amount == 0){
        
    }
    
    Order *updateOrder = [self.order copy];
    
    updateOrder.bfStatus = updateOrder.status;
    updateOrder.status = newStatus;
    if(reason != nil)
        updateOrder.completeReasonJson = reason;
    
    [self requestUpdateWithOrder:updateOrder];
}


- (void)requestUpdateWithOrder:(Order *)updateOrder
{
    RequestResult result = RRFail;
    result = [MERCHANT requestUpdateOrder:updateOrder
                          completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              [self.order updateWithDictionary:response.data
                                                      merchant:nil
                                                       menuPan:MENUPAN.wholeSerialMenuList];
                              
                              if(self.order.status == OrderStatusFixed ||
                                 self.order.status == OrderStatusCanceled ||
                                 self.order.status == OrderStatusDeclined){
                                  
                                  TableStatus tempStatus = self.tableInfo.status;
                                  self.tableInfo.order = nil;
                                  self.tableInfo.billRequest = nil;
                                  self.tableInfo.bfStatus = tempStatus;
                                  
                                  if(!self.isTicketBase){ // Table based order case
                                      // self.order means thant empty table
                                      // In Pickup status "Empty table" is needless
                                      // Even more self.order is very important to configure that this order is pickup order
                                      self.order = nil;
                                  }

                                  if(self.inPopover){
                                      [self dismissPopover:self];
                                  } else {
                                      [self.navigationController popViewControllerAnimated:YES];
                                  }

                              } else {
                                  
                                  self.tableInfo.order = self.order;

                                  if(self.order.isTicketBase){
                                      if(self.order.status == OrderStatusConfirmed){
                                          if(![NSUserDefaults standardUserDefaults].printWhenNewOrder){
                                              if([NSUserDefaults standardUserDefaults].autoKitchenPrint){
                                                  long bixRtn = 0;
                                                  
                                                  switch(APP.selectedMakerCode){
                                                      case PrinterMakerEpson:
                                                          bixRtn = [EPSON printOrder:self.order];
                                                          break;
                                                      case PrinterMakerStar:
                                                          bixRtn = [STAR printOrder:self.order];
                                                          break;
                                                      case PrinterMakerBixolon:
                                                          bixRtn = [BIX printOrder:self.order];
                                                          break;
                                                      default:
                                                          bixRtn = RUSHORDER_PRINTER_NOT_SET;
                                                          break;
                                                  }
                                                  
                                                  [self treatPrintError:bixRtn];
                                              }
                                              
                                              if([NSUserDefaults standardUserDefaults].autoReceiptPrint){
                                                  long bixRtn = 0;
                                                  
                                                  switch(APP.selectedMakerCode){
                                                      case PrinterMakerEpson:
                                                          bixRtn = [EPSON printReceipt:[[ReceiptWrapper alloc] initWithOrder:self.order withPaymentIndex:0] atIndex:0];
                                                          break;
                                                      case PrinterMakerStar:
                                                          bixRtn = [STAR printReceipt:[[ReceiptWrapper alloc] initWithOrder:self.order withPaymentIndex:0] atIndex:0];
                                                          break;
                                                      case PrinterMakerBixolon:
                                                          bixRtn = [BIX printReceipt:[[ReceiptWrapper alloc] initWithOrder:self.order withPaymentIndex:0] atIndex:0];
                                                          break;
                                                      default:
                                                          bixRtn = RUSHORDER_PRINTER_NOT_SET;
                                                          break;
                                                  }
    //                                              
    //                                              switch(APP.selectedMakerCode){
    //                                                  case PrinterMakerEpson:
    //                                                      bixRtn = [EPSON printBill:[[BillWrapper alloc] initWithOrder:self.order]];
    //                                                      break;
    //                                                  case PrinterMakerStar:
    //                                                      bixRtn = [STAR printBill:[[BillWrapper alloc] initWithOrder:self.order]];
    //                                                      break;
    //                                                  case PrinterMakerBixolon:
    //                                                      bixRtn = [BIX printBill:[[BillWrapper alloc] initWithOrder:self.order]];
    //                                                      break;
    //                                                  default:
    //                                                      bixRtn = RUSHORDER_PRINTER_NOT_SET;
    //                                                      break;
    //                                              }
                                                  
                                                  [self treatPrintError:bixRtn];
                                              }
                                          }
                                      }
                                      
                                      if(self.order.status == OrderStatusReady){
                                          // Prohibit to lead to press "complete" button.
                                          // and Fix state will be later after ready. (Time span)
                                          if(self.inPopover){
                                              [self dismissPopover:self];
                                          } else {
                                              [self.navigationController popViewControllerAnimated:YES];
                                          }
                                      }
                                  }
                              }

                              NSDictionary *userInfo = nil;
                              
                              if(self.isTicketBase){
                                  userInfo = [NSDictionary dictionaryWithObject:self.order forKey:@"order"];
                              } else {
                                  userInfo = [NSDictionary dictionaryWithObject:self.tableInfo forKey:@"tableInfo"];
                              }
                              
                              [[NSNotificationCenter defaultCenter] postNotificationName:TableInfoChangedNotification
                                                                                  object:self
                                                                                userInfo:userInfo];
                              
                              if(self.inPopover){
                                  [self dismissPopover:self];
                              } else {
                                  [self reloadData];
                              }
                          }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while updating this order", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)dismissPopover:(id)sender
{
    [self.popover dismissPopoverAnimated:YES];
}

- (IBAction)moneyFieldValueChanged:(UITextField *)textField
{
    if(textField == self.subTotalTextField || textField == self.taxesTextField){
        
        if(textField == self.subTotalTextField && self.subTotalTextField.editing){
            PCCurrency taxAmount = roundf(self.subTotalTextField.amount * (CRED.merchant.taxRate / 100.0f));
            if(self.taxesTextField.amount == 0 || self.taxesTextField.amount == self.justBeforeTaxAmount){
                self.taxesTextField.amount = taxAmount;
            }
            self.justBeforeTaxAmount = taxAmount;
        }

        //+ self.paidTipTextField.amount - self.discountTextField.amount
        self.totalTextField.amount = (self.subTotalTextField.amount + self.taxesTextField.amount + self.deliveryFeeTextField.amount);
        
        [self updateButtonTitle];
    }
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [self setSubTotalTextField:nil];
    [self setTaxesTextField:nil];
    [self setTotalTextField:nil];
    [self setTableInfoView:nil];
    [self setTableInfoLabel:nil];
    [self setSumContainer:nil];
    [self setSectionHeaderView:nil];
    [self setPayProgressView:nil];
    [self setGrandTotalSumLabel:nil];
    [self setPayAmountSumLabel:nil];
    [self setRemaingAmountSumLabel:nil];
    [self setProcessButton:nil];
    [self setBillContainerView:nil];
    [self setTotalTipLabel:nil];
    [self setTotalAmountLabel:nil];
    [self setPointSumLabel:nil];
    [self setPointSumTitme:nil];
    [self setRewardedPointTitleLabel:nil];
    [self setRewardedPointLabel:nil];
    [self setPaymentSummaryView:nil];
    [self setCartOperationView:nil];
    [self setOrderHeaderView:nil];
    [self setTableBackgroundImageView:nil];
    [self setConfirmButton:nil];
    [self setRejectButton:nil];
    [self setCartHeaderView:nil];
    [self setOrderListTitleLabel:nil];
    [self setCartSubtotalAmount:nil];
    [self setCartTaxesAmount:nil];
    [self setTotalAmount:nil];
    [self setForFooterView:nil];
    [self setRequestChangeButton:nil];
    [self setManualResetButton:nil];
    [self setCancelButton1:nil];
    [self setCancelButton2:nil];
    [super viewDidUnload];
}

- (BOOL)isAmountChanged
{
    if(self.order != nil){
        if(self.order.subTotal == self.subTotalTextField.amount &&
           self.order.taxes == self.taxesTextField.amount){
            return NO;
        } else {
            return YES;
        }
    }
    return NO;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 1:
            if(buttonIndex == AlertButtonIndexYES){
                [self requestNewOrder];
            }
            break;
        case 2:
            [self.subTotalTextField becomeFirstResponder];
            break;
        case 103:
            [self.tableView scrollToBottom:YES];
            break;
        case 301:
        {
            if(buttonIndex == AlertButtonIndexYES){
//                NSDictionary *optionDict = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES]
//                                                                       forKey:@"cancel"];
//                [self requestUpdateOrder:YES reason:[optionDict JSONString]];
                [self updateOrderStatus:OrderStatusCanceled];
            }
        }
            break;
        case 302:
        {
            //Remove this cart
            if(buttonIndex == AlertButtonIndexYES){
                [self rejectThisCart];
            }
            
        }
            break;
        default:
            break;
    }
}

#pragma mark - CompleteOptionViewControllerDelegate
- (void)completeOptionViewController:(CompleteOptionViewController *)viewController
                     didSelectOption:(NSString *)option
{
    [self requestUpdateOrder:YES reason:option];
}

- (BOOL)inPopover
{
    return self.navigationController.parentViewController == nil;
}

#pragma mark - Cart related

- (IBAction)confirmButtonTouched:(id)sender
{
    if(self.tableInfo.order == nil){
        [self confirm];
    } else {
        [self confirmAdd];
    }
}

- (void)confirm
{
    if(self.tableInfo.cart.hasAlcohol){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Please Check ID", nil)
                            message:NSLocalizedString(@"This order includes alcoholic beverages", nil)];
    }
    
    RequestResult result = RRFail;
    result = [ORDER requestConfirmCart:self.tableInfo.cart
                            merchantId:CRED.merchant.merchantNo
                       completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {

                              self.tableInfo.cart.status = CartStatusFixed;
                              
                              self.tableInfo.order = [[Order alloc] initWithDictionary:response.data
                                                                              merchant:CRED.merchant
                                                                               menuPan:MENUPAN.wholeSerialMenuList];
                              
                              self.tableInfo.order.customers = self.tableInfo.cart.customers;
                              
                              [self fillOrder];
                              
                              [self requestCart:NO];
                              
                              if(![NSUserDefaults standardUserDefaults].printWhenNewOrder){
                                  if([NSUserDefaults standardUserDefaults].autoKitchenPrint){
                                      long bixRtn = 0;
                                      
                                      if(self.tableInfo == nil){
                                          // Pickup Order
                                          switch(APP.selectedMakerCode){
                                              case PrinterMakerEpson:
                                                  bixRtn = [EPSON printOrder:self.order];
                                                  break;
                                              case PrinterMakerStar:
                                                  bixRtn = [STAR printOrder:self.order];
                                                  break;
                                              case PrinterMakerBixolon:
                                                  bixRtn = [BIX printOrder:self.order];
                                                  break;
                                              default:
                                                  bixRtn = RUSHORDER_PRINTER_NOT_SET;
                                                  break;
                                          }
                                      } else {
                                          // Table Order
                                          self.tableInfo.cart.tableLabel = self.tableInfo.tableNumber;
                                          self.tableInfo.cart.orderNo = self.tableInfo.order.orderNo;
                                          
                                          
                                          switch(APP.selectedMakerCode){
                                              case PrinterMakerEpson:
                                                  bixRtn = [EPSON printCart:self.tableInfo.cart];
                                                  break;
                                              case PrinterMakerStar:
                                                  bixRtn = [STAR printCart:self.tableInfo.cart];
                                                  break;
                                              case PrinterMakerBixolon:
                                                  bixRtn = [BIX printCart:self.tableInfo.cart];
                                                  break;
                                              default:
                                                  bixRtn = RUSHORDER_PRINTER_NOT_SET;
                                                  break;
                                          }
                                      }
                                      [self treatPrintError:bixRtn];
                                  }
                                  
                                  if([NSUserDefaults standardUserDefaults].autoBillPrint){
                                      long bixRtn = 0;
                                      
                                      switch(APP.selectedMakerCode){
                                          case PrinterMakerEpson:
                                              bixRtn = [EPSON printBill:[[BillWrapper alloc] initWithOrder:self.tableInfo.order]];
                                              break;
                                          case PrinterMakerStar:
                                              bixRtn = [STAR printBill:[[BillWrapper alloc] initWithOrder:self.tableInfo.order]];
                                              break;
                                          case PrinterMakerBixolon:
                                              bixRtn = [BIX printBill:[[BillWrapper alloc] initWithOrder:self.tableInfo.order]];
                                              break;
                                          default:
                                              bixRtn = RUSHORDER_PRINTER_NOT_SET;
                                              break;
                                      }
                                      [self treatPrintError:bixRtn];
                                  }
                              }
                              
                              NSDictionary *userInfo = nil;
                              
                              if(self.isTicketBase){
                                  userInfo = [NSDictionary dictionaryWithObject:self.order forKey:@"order"];
                              } else {
                                  userInfo = [NSDictionary dictionaryWithObject:self.tableInfo forKey:@"tableInfo"];
                              }
                              
                              [[NSNotificationCenter defaultCenter] postNotificationName:TableInfoChangedNotification
                                                                                  object:self
                                                                                userInfo:userInfo];
                              
                              if(self.inPopover){
                                  [self dismissPopover:self];
                              } else {
                                  [self.navigationController popViewControllerAnimated:YES];
                              }
                              

                              break;
                      }
                          case ResponseErrorCartIsNotSubmitted:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Customer is editing Order",nil)
                                                  message:NSLocalizedString(@"Wait for customer to submit order.",nil)];
                              
                              [self requestCart];
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while confirming orders.", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)confirmAdd
{
    if(self.tableInfo.cart.hasAlcohol){
        [UIAlertView alertWithTitle:NSLocalizedString(@"Please Check ID", nil)
                            message:NSLocalizedString(@"This order includes alcoholic beverages", nil)];
    }
    
    RequestResult result = RRFail;
    result = [ORDER requestConfirmAddCart:self.tableInfo.cart
                                  toOrder:self.tableInfo.order
                               merchantId:CRED.merchant.merchantNo
                          completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              PCLog(@"Success Confirm add");
                              
                              self.tableInfo.cart.status = CartStatusFixed;
                              
                              self.tableInfo.order = [[Order alloc] initWithDictionary:response.data
                                                                              merchant:CRED.merchant
                                                                               menuPan:MENUPAN.wholeSerialMenuList];
                              
                              [self fillOrder];
                              [self requestCart:NO];
    
                              
                              if(![NSUserDefaults standardUserDefaults].printWhenNewOrder){
                                  if([NSUserDefaults standardUserDefaults].autoKitchenPrint){
                                      long bixRtn = 0;
                                      
                                      // Confirm Add do not include Pickup order case - Only available in Table Order
                                      self.tableInfo.cart.tableLabel = self.tableInfo.tableNumber;
                                      self.tableInfo.cart.orderNo = self.tableInfo.order.orderNo;
                                      
                                      switch(APP.selectedMakerCode){
                                          case PrinterMakerEpson:
                                              bixRtn = [EPSON printCart:self.tableInfo.cart];
                                              break;
                                          case PrinterMakerStar:
                                              bixRtn = [STAR printCart:self.tableInfo.cart];
                                              break;
                                          case PrinterMakerBixolon:
                                              bixRtn = [BIX printCart:self.tableInfo.cart];
                                              break;
                                          default:
                                              bixRtn = RUSHORDER_PRINTER_NOT_SET;
                                              break;
                                      }

                                      [self treatPrintError:bixRtn];
                                  }
                                  
                                  if([NSUserDefaults standardUserDefaults].autoBillPrint){
                                      long bixRtn = 0;
                                      
                                      switch(APP.selectedMakerCode){
                                          case PrinterMakerEpson:
                                              bixRtn = [EPSON printBill:[[BillWrapper alloc] initWithOrder:self.tableInfo.order]];
                                              break;
                                          case PrinterMakerStar:
                                              bixRtn = [STAR printBill:[[BillWrapper alloc] initWithOrder:self.tableInfo.order]];
                                              break;
                                          case PrinterMakerBixolon:
                                              bixRtn = [BIX printBill:[[BillWrapper alloc] initWithOrder:self.tableInfo.order]];
                                              break;
                                          default:
                                              bixRtn = RUSHORDER_PRINTER_NOT_SET;
                                              break;
                                      }
                                      
                                      [self treatPrintError:bixRtn];
                                  }
                              }
                              
                              NSDictionary *userInfo = nil;
                              
                              if(self.isTicketBase){
                                  userInfo = [NSDictionary dictionaryWithObject:self.order forKey:@"order"];
                              } else {
                                  userInfo = [NSDictionary dictionaryWithObject:self.tableInfo forKey:@"tableInfo"];
                              }
                              
                              [[NSNotificationCenter defaultCenter] postNotificationName:TableInfoChangedNotification
                                                                                  object:self
                                                                                userInfo:userInfo];
                              
                              if(self.inPopover){
                                  [self dismissPopover:self];
                              } else {
                                  [self.navigationController popViewControllerAnimated:YES];
                              }
                          }
                              break;
                          case ResponseErrorCartIsNotSubmitted:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Customer is editing Order",nil)
                                                  message:NSLocalizedString(@"Wait for customer to submit order.",nil)];
                              
                              [self requestCart];
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while confirming additional orders.", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }    
}

- (IBAction)rejectButtonTouched:(id)sender
{
    [UIAlertView askWithTitle:NSLocalizedString(@"Confirm To Proceed", nil)
                      message:NSLocalizedString(@"Removed order cannot be back. Do you want to proceed?", nil)
                     delegate:self
                          tag:302];
}

- (void)rejectThisCart
{
    RequestResult result = RRFail;
    result = [MERCHANT requestDeleteCart:self.tableInfo.cart
                     completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              PCLog(@"Success delete cart");
                              
                              self.tableInfo.cart = nil;
                              [self.tableInfo configureTableStatus];
                              
                              [self reloadData];
                              
                              if(self.inPopover){
                                  [self dismissPopover:self];
                              } else {
                                  [self.navigationController popViewControllerAnimated:YES];
                              }
                              
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while declining this order.", nil)];
                              }
                          }
                              break;
                      }
                      
                      NSDictionary *userInfo = [NSDictionary dictionaryWithObject:self.tableInfo forKey:@"tableInfo"];
                      [[NSNotificationCenter defaultCenter] postNotificationName:TableInfoChangedNotification
                                                                          object:self
                                                                        userInfo:userInfo];
                      
                      
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)requestCart
{
    [self requestCart:YES];
}

- (void)requestCart:(BOOL)withOrderRequest
{
    RequestResult result = RRFail;
    result = [MENU requestGettingCart:self.tableInfo.cart
                      completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              [self.tableInfo.cart updateWithDictionary:response.data
                                                                menuPan:MENUPAN.wholeSerialMenuList];
                              
                              if(self.tableInfo.cart.status == CartStatusFixed){
                                  self.tableInfo.cart = nil;
                              }
                              
                              if(self.tableInfo.cart.isMine && self.tableInfo.cart.status == CartStatusOrdering){
                                  self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Change Order", nil)
                                                                                                            target:self
                                                                                                            action:@selector(editButtonTouched:)
                                                                                                          backType:NPBarButtonBackTypeAppDefault];
                              }
                              
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          case ResponseErrorNoCart:{
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Cart does not exist", nil)
                                                  message:NSLocalizedString(@"Customer might cancel their or remove all items from the list", nli)];
                              [TABLE reloadTableStatus];
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting order list information", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  
                  if(self.tableInfo.order != nil){
                      self.order = self.tableInfo.order;
                      // TODO: Don't need to call again order if the case of confirming cart. Because the response of confirming cart is the Order itself. This routine called when confirmed.
                      [self requestOrder];
                  } else {
                      [self reloadData];
                  }
                  
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            if(self.tableInfo.order != nil){
                self.order = self.tableInfo.order;
                [self requestOrder];
            } else {
                [self reloadData];
            }
            break;
        default:
            break;
    }
}

- (IBAction)requestChangeButtonTouched:(id)sender
{
    [self.tableInfo.cart updateOrderingWithSuccess:^(BOOL isOnlyLocal){
                                  if(!isOnlyLocal){
                                      
                                      [self fillOrder];

                                      [self requestCart:NO];
                                      
                                      NSDictionary *userInfo = [NSDictionary dictionaryWithObject:self.tableInfo forKey:@"tableInfo"];
                                      
                                      [[NSNotificationCenter defaultCenter] postNotificationName:TableInfoChangedNotification
                                                                                          object:self
                                                                                        userInfo:userInfo];
                                  }
                              } failure:^(){
                                  
                              }];
}

- (IBAction)manuallyPaidNResetButtonTocuehd:(UIButton *)sender
{
    switch(sender.tag){
        case ResetFunctionCheckout:
        {
            CheckOutViewController *viewController = [CheckOutViewController viewControllerFromNib];
            viewController.tableInfo = self.tableInfo;
            viewController.order = self.order;
            [self.navigationController pushViewController:viewController
                                                 animated:YES];
        }
            break;
        case ResetFunctionDecline:
        {
            RejectViewController *viewController = [RejectViewController viewControllerFromNib];
            viewController.delegate = self;
            [self.navigationController pushViewController:viewController
                                                 animated:YES];
        }
            break;
    }
}

- (void)rejectViewController:(RejectViewController *)viewController
     allowCustomerAdjustment:(BOOL)allowCustomerAdjustment
           rejectWithMessage:(NSString *)message
{
    PCSerial orderNo = self.tableInfo.order.orderNo;
    
    if(self.isTicketBase){
        orderNo = self.order.orderNo;
    }
    
    RequestResult result = RRFail;
    result = [ORDER requestReject:orderNo
                           reason:message
          allowCustomerAdjustment:allowCustomerAdjustment
                  completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              
                              for(Payment *payment in self.order.paymentList){
                                  payment.status = PaymentStatusRefunded;
                              }
                              
                              self.order.bfStatus = self.order.status;
                              self.order.status = OrderStatusDeclined;
                              [self.tableInfo configureTableStatus];
                              
                              NSDictionary *userInfo = nil;
                              if(self.isTicketBase){
                                  userInfo = [NSDictionary dictionaryWithObject:self.order forKey:@"order"];
                              } else {
                                  userInfo = [NSDictionary dictionaryWithObject:self.tableInfo forKey:@"tableInfo"];
                              }
                              
                              [[NSNotificationCenter defaultCenter] postNotificationName:TableInfoChangedNotification
                                                                                  object:self
                                                                                userInfo:userInfo];
                              
                              if(self.inPopover){
                                  [self dismissPopover:self];
                              } else {
                                  [self.navigationController popToRootViewControllerAnimated:YES];
                              }
                              
                          }
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while declining order", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}


- (void)reloadData
{
    [self configureSection];
    [self fillOrder];
    [self layoutBillView];
    [self.tableView reloadData];
    [self resizeTableView];
}

- (BOOL)isTicketBase
{
    if(self.tableInfo == nil){
        return self.order.isTicketBase;
    } else {
        return NO;
    }
}

- (IBAction)printButtonTouched:(id)sender
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Type To Print", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                         destructiveButtonTitle:NSLocalizedString(@"Receipt", nil)
                                              otherButtonTitles:NSLocalizedString(@"Kitchen", nil),
                            NSLocalizedString(@"Bill", nil), nil];
    sheet.tag = 100;
    
    [sheet showInView:self.view];
}


- (void)treatPrintError:(long)errorCode
{
    switch(APP.selectedMakerCode){
        case PrinterMakerEpson:
            [EPSON treatPrintError:errorCode];
            break;
        case PrinterMakerStar:
            [STAR treatPrintError:errorCode];
            break;
        case PrinterMakerBixolon:
            [BIX treatPrintError:errorCode];
            break;
        default:
            if(errorCode == RUSHORDER_PRINTER_NOT_SET){
                [UIAlertView alertWithTitle:NSLocalizedString(@"Printer is Not Set", nil)
                                    message:NSLocalizedString(@"Printer maker and model are not set. Please go to Settings > Print Settings to set.", nil)
                                   delegate:self
                                        tag:104];
            }
            break;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(actionSheet.tag){
        case 100:
        {
////////////////////////////// test code
//    switch(APP.selectedMakerCode){
//        case PrinterMakerEpson:
//            [EPSON printTest];
//            break;
//        case PrinterMakerStar:
//            [STAR printTest];
//            break;
//        case PrinterMakerBixolon:
//            [BIX printTest];
//            break;
//    }
//    return;
//////////////////////////////
            
            if(buttonIndex == actionSheet.cancelButtonIndex){
                
            } else {
                [SVProgressHUD showWithStatus:NSLocalizedString(@"Printing...", nil) maskType:SVProgressHUDMaskTypeClear];
                self.mainPrintButton.enabled = NO;
                
                [self performSelector:@selector(startPrintingWithIndex:)
                           withObject:[NSNumber numberWithInteger:buttonIndex]
                           afterDelay:0.01f];
                
            }
        }
            break;
        case 101:
        {
            if(actionSheet.destructiveButtonIndex == buttonIndex){
                [self updateOrderStatus:OrderStatusCanceled];
                
            } else {
                
            }
        }
            break;
        default:
            break;
    }
    
}

- (void)startPrintingWithIndex:(NSNumber *)indexNumber
{
    NSInteger buttonIndex = [indexNumber integerValue];
    
    long bixRtn = 0;
    
    if(buttonIndex == 1){
        
        // Kitchen
        if(self.tableInfo == nil){
            
            switch(APP.selectedMakerCode){
                case PrinterMakerEpson:
                    bixRtn = [EPSON printOrder:self.order];
                    break;
                case PrinterMakerStar:
                    bixRtn = [STAR printOrder:self.order];
                    break;
                case PrinterMakerBixolon:
                    bixRtn = [BIX printOrder:self.order];
                    break;
                default:
                    bixRtn = RUSHORDER_PRINTER_NOT_SET;
                    break;
            }
            
        } else {
            if(self.tableInfo.order == nil || (self.tableInfo.order.orderType == OrderTypeBill)){
                self.tableInfo.cart.tableLabel = self.tableInfo.tableNumber;
                self.tableInfo.cart.orderNo = self.tableInfo.order.orderNo;
                
                switch(APP.selectedMakerCode){
                    case PrinterMakerEpson:
                        bixRtn = [EPSON printCart:self.tableInfo.cart];
                        break;
                    case PrinterMakerStar:
                        bixRtn = [STAR printCart:self.tableInfo.cart];
                        break;
                    case PrinterMakerBixolon:
                        bixRtn = [BIX printCart:self.tableInfo.cart];
                        break;
                    default:
                        bixRtn = RUSHORDER_PRINTER_NOT_SET;
                        break;
                        
                }
            } else {
                
                switch(APP.selectedMakerCode){
                    case PrinterMakerEpson:
                        bixRtn = [EPSON printOrder:self.tableInfo.order];
                        break;
                    case PrinterMakerStar:
                        bixRtn = [STAR printOrder:self.tableInfo.order];
                        break;
                    case PrinterMakerBixolon:
                        bixRtn = [BIX printOrder:self.tableInfo.order];
                        break;
                    default:
                        bixRtn = RUSHORDER_PRINTER_NOT_SET;
                        break;
                }
            }
        }
        
    } else if(buttonIndex == 2){
        // Bill
        if(self.tableInfo == nil){
            switch(APP.selectedMakerCode){
                case PrinterMakerEpson:
                    bixRtn = [EPSON printBill:[[BillWrapper alloc] initWithOrder:self.order]];
                    break;
                case PrinterMakerStar:
                    bixRtn = [STAR printBill:[[BillWrapper alloc] initWithOrder:self.order]];
                    break;
                case PrinterMakerBixolon:
                    bixRtn = [BIX printBill:[[BillWrapper alloc] initWithOrder:self.order]];
                    break;
                default:
                    bixRtn = RUSHORDER_PRINTER_NOT_SET;
                    break;
            }
        } else {
            if(self.tableInfo.order == nil || (self.tableInfo.order.orderType == OrderTypeBill)){
                
                [UIAlertView alertWithTitle:NSLocalizedString(@"Order is not confirmed yet", nil)
                                    message:NSLocalizedString(@"Printing the bill is available only after confirming order.", nil)
                                   delegate:self
                                        tag:105];
                
            } else {
                switch(APP.selectedMakerCode){
                    case PrinterMakerEpson:
                        bixRtn = [EPSON printBill:[[BillWrapper alloc] initWithOrder:self.tableInfo.order]];
                        break;
                    case PrinterMakerStar:
                        bixRtn = [STAR printBill:[[BillWrapper alloc] initWithOrder:self.tableInfo.order]];
                        break;
                    case PrinterMakerBixolon:
                        bixRtn = [BIX printBill:[[BillWrapper alloc] initWithOrder:self.tableInfo.order]];
                        break;
                    default:
                        bixRtn = RUSHORDER_PRINTER_NOT_SET;
                        break;
                }
            }
        }
    } else if(buttonIndex == 0){
        Order *tempOrder = nil;
        if(self.tableInfo == nil){
            //Pickup
            tempOrder = self.order;
        } else {
            tempOrder = self.tableInfo.order;
        }
        
        NSUInteger paymentCount = [tempOrder.paymentList count];
        if(paymentCount == 0){
           
            [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot Print Receipt", nil)
                                message:NSLocalizedString(@"This order has not been paid for yet. You can only print receipts for paid orders.", nil)
                               delegate:self
                                    tag:104];
            
        } else if(paymentCount == 1){
            // Receipt
            switch(APP.selectedMakerCode){
                case PrinterMakerEpson:
                    bixRtn = [EPSON printReceipt:[[ReceiptWrapper alloc] initWithOrder:tempOrder withPaymentIndex:0] atIndex:0];
                    break;
                case PrinterMakerStar:
                    bixRtn = [STAR printReceipt:[[ReceiptWrapper alloc] initWithOrder:tempOrder withPaymentIndex:0] atIndex:0];
                    break;
                case PrinterMakerBixolon:
                    bixRtn = [BIX printReceipt:[[ReceiptWrapper alloc] initWithOrder:tempOrder withPaymentIndex:0] atIndex:0];
                    break;
                default:
                    bixRtn = RUSHORDER_PRINTER_NOT_SET;
                    break;
            }
            
        } else if(paymentCount > 1){
            
            [UIAlertView alertWithTitle:NSLocalizedString(@"Select a Payment to Print Receipt", nil)
                                message:NSLocalizedString(@"There are payments more than one. Select a payment to print receipt below", nil)
                               delegate:self
                                    tag:103];
            
        } else {
            // Do nothing
        }
    }
    
    [SVProgressHUD dismiss];
    self.mainPrintButton.enabled = YES;
    
    [self treatPrintError:bixRtn];
    
}


- (IBAction)cancelButtonTouched:(id)sender
{
    if(self.popover != nil){
        [self dismissPopover:sender];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (DetailOrderMenuCell *)offScreenDetailOrderMenuCell
{
    if(_offScreenDetailOrderMenuCell == nil){
        _offScreenDetailOrderMenuCell = [DetailOrderMenuCell cell];
    }
    
    return _offScreenDetailOrderMenuCell;
}

- (LineItemOptionCell *)offScreenLineItemOptionCell
{
    if(_offScreenLineItemOptionCell == nil){
        _offScreenLineItemOptionCell = [LineItemOptionCell cell];
    }
    
    return _offScreenLineItemOptionCell;
}

- (IBAction)minuteButtonTouched:(UIButton *)sender
{
    if(sender.tag == 0){
        self.pickupTimeTextField.inputView = self.timePickerView;
//        [self.pickupTimeTextField resignFirstResponder];
//        [self.pickupTimeTextField performSelector:@selector(becomeFirstResponder)
//                                       withObject:nil
//                                       afterDelay:0.1f];
        [self.pickupTimeTextField reloadInputViews];
//        [self.pickupTimeTextField becomeFirstResponder];
        return;
    }
    
    NSDate *now = [NSDate date];
    NSInteger afterMinute = sender.tag;
    NSTimeInterval afterInterval = afterMinute * 60;
    NSDate *afterNow = [now dateByAddingTimeInterval:afterInterval];
    
    self.expectedTime = afterNow;
    self.pickupTimeTextField.text = afterNow.hourMinuteString;
    self.selectedInterval = sender.tag;
    
    [self inputDoneButtonTouched:sender];
}



@end
