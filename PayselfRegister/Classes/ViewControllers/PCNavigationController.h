//
//  PCNavigationController.h
//  RushOrder
//
//  Created by Conan on 3/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const NetworkProblemReportNotification;
extern NSString * const NetworkGoodReportNotification;
extern NSString * const NetworkErrorCloseNotification;

@interface PCNavigationController : UINavigationController

@end
