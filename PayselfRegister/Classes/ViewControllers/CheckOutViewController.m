//
//  CheckOutViewController.m
//  RushOrder
//
//  Created by Conan on 10/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "CheckOutViewController.h"
#import "PCOrderService.h"
#import "TableStatusManager.h"

@interface CheckOutViewController ()

@property (weak, nonatomic) IBOutlet UILabel *taxAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTotalAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;
@property (weak, nonatomic) IBOutlet PCCurrencyTextField *payAmountTextField;
@property (weak, nonatomic) IBOutlet PCCurrencyTextField *tipAmountTextField;
@property (weak, nonatomic) IBOutlet NPStretchableButton *doneButton;
@property (weak, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *payMethodSegmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *totalPaymentLabel;
@end

@implementation CheckOutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Checkout", nil);
    
    [self fillOrder];
    
    [self.scrollView setContentSizeWithBottomView:self.doneButton];
}

- (IBAction)cancelButtonTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fillOrder
{
    Order *tempOrder = self.order;
    if(self.tableInfo != nil){
        tempOrder = self.tableInfo.order;
    }
    
    self.taxAmountLabel.text = [[NSNumber numberWithCurrency:tempOrder.taxes] currencyString];
    self.subTotalAmountLabel.text = [[NSNumber numberWithCurrency:tempOrder.subTotal] currencyString];
    self.totalAmountLabel.text = [[NSNumber numberWithCurrency:tempOrder.total] currencyString];
    
    self.payAmountTextField.amount = tempOrder.remainingToPay;
}

- (IBAction)doneButtonTouched:(id)sender
{
    Order *tempOrder = self.order;
    if(self.tableInfo != nil){
        tempOrder = self.tableInfo.order;
    }
    
    Payment *payment = [[Payment alloc] init];
    payment.merchantNo = CRED.merchant.merchantNo;
    payment.orderAmount = self.payAmountTextField.amount + self.tipAmountTextField.amount;
    payment.payAmount = payment.orderAmount;
    payment.tipAmount = self.tipAmountTextField.amount;
    payment.currency = self.payAmountTextField.currencyCode;
    payment.payDate = [NSDate date];
    payment.orderNo = tempOrder.orderNo;
    
    switch(self.payMethodSegmentedControl.selectedSegmentIndex){
        case 0:
            payment.method = PaymentMethodPlasticCard;
            break;
        case 1:
            payment.method = PaymentMethodCash;
            break;
    }
    
    RequestResult result = RRFail;
    result = [ORDER requestNewPayment:tempOrder.orderNo
                              payment:payment
                            lineItems:tempOrder.lineItems
                      completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              
                              [self.navigationController popViewControllerAnimated:YES];
                              
                              [TABLE reloadTableStatus];
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while making payment", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (IBAction)textFieldValueChanged:(id)sender
{
    PCCurrency total = self.payAmountTextField.amount + self.tipAmountTextField.amount;
    self.totalPaymentLabel.text = [[NSNumber numberWithCurrency:total] currencyString];
}

@end
