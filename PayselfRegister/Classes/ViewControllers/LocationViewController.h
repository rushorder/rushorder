//
//  LocationViewController.h
//  RushOrder
//
//  Created by Conan on 8/13/14.
//  Copyright (c) 2014 Payself Mobile Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddressAnnotation.h"

@protocol LocationViewControllerDelegate;

@interface LocationViewController : UITableViewController
<
UITextFieldDelegate,
UITableViewDelegate,
UITableViewDataSource
>

@property (weak, nonatomic) id<LocationViewControllerDelegate> delegate;
@property (strong, nonatomic) AddressAnnotation *selectedAddress;
@property (nonatomic) BOOL shouldBeBackToCurrent;
@end

@protocol LocationViewControllerDelegate <NSObject>
- (void)locationViewController:(LocationViewController *)viewController
            didApplyCoordinate:(CLLocationCoordinate2D)location
                          name:(NSString *)name
            placemarkOrAddress:(id)obj;
- (void)didCancelLocationViewController:(LocationViewController *)viewController;
@end

