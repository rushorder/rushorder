//
//  MerchantAboutViewController.h
//  RushOrder
//
//  Created by Conan on 2/22/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MerchantAboutViewController : PCViewController

@property (copy, nonatomic) NSString *versionString;
@end
