//
//  MerchantLocationViewController.m
//  RushOrder
//
//  Created by Conan on 3/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MerchantLocationViewController.h"

@interface MerchantLocationViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) PinAnnotation *pinAnnotation;
@property (strong, nonatomic) NSArray *placemarks;

@property (strong, nonatomic) NSArray *searchResults;
@property (strong, nonatomic) CLGeocoder *geocoder;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@end

@implementation MerchantLocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    self.mapView.delegate = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Merchant Location", nil);
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc ]initWithTitle:NSLocalizedString(@"Apply", nil)
                                                                              style:UIBarButtonItemStyleDone
                                                                             target:self
                                                                             action:@selector(selectPosition:)];
    
    if(self.isRootViewController){
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Cancel", nil)
                                                                                 target:self
                                                                                 action:@selector(cancelButtonTouched:)];
    }
    
    self.navigationItem.rightBarButtonItem.button.enabled = NO;
    
    if(self.initialLocation != nil){
        self.pinAnnotation.coordinate = self.initialLocation.coordinate;
        MKCoordinateSpan span = {0.02, 0.02};
        self.mapView.region = MKCoordinateRegionMake( self.initialLocation.coordinate,
                                                     span);
        [self reverseGeocoding:self.pinAnnotation.coordinate];
    }
    
//
    if(OVER_IOS8){
        [APP.locationManager requestWhenInUseAuthorization];
    }
    self.mapView.showsUserLocation = YES;
}

- (void)cancelButtonTouched:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 
                             }];
}

- (void)selectPosition:(id)sender
{
    if([self.delegate respondsToSelector:@selector(mapViewController:didSelectPlacemarks:pinAnnotation:)]){
        [self.delegate mapViewController:self
                     didSelectPlacemarks:self.placemarks
                           pinAnnotation:self.pinAnnotation];
    }
    if(self.isRootViewController){
        [self dismissViewControllerAnimated:YES
                                 completion:^{
                                     
                                 }];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)mapTouched:(UITapGestureRecognizer *)sender
{
    PCLog(@"Map touched %@", sender);

    self.navigationItem.rightBarButtonItem.button.enabled = NO;
    
    CGPoint touchPoint = [sender locationInView:self.mapView];
    CLLocationCoordinate2D coordinate = [self.mapView convertPoint:touchPoint
                                              toCoordinateFromView:self.mapView];
    PCLog(@"la %f, lo %f", coordinate.latitude, coordinate.longitude);
    
    if(sender.state == UIGestureRecognizerStateEnded){
        
        self.pinAnnotation.coordinate = coordinate;
        [self reverseGeocoding:coordinate];
    }
}

- (void)reverseGeocoding:(CLLocationCoordinate2D)coordinate
{

        [self.geocoder cancelGeocode];
        [self.geocoder reverseGeocodeLocation:[[CLLocation alloc] initWithLatitude:coordinate.latitude
                                                                         longitude:coordinate.longitude]
                            completionHandler:^(NSArray *placemark, NSError *error){
                                if(error != nil){
                                    if(error.code == 10){
                                        // User cancel case - don't show the error popup
                                    } else {
                                        NSString *errorMessage = [error localizedDescription];
                                        PCError(@"reverseGeocoding : %@ (code : %d)", errorMessage, error.code);
                                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cannot retrieve address", nil)
                                                                                            message:errorMessage
                                                                                           delegate:nil
                                                                                  cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                                                  otherButtonTitles:nil];
                                        [alertView show];
                                    }
                                } else {
                                    self.placemarks = placemark;

                                    self.navigationItem.rightBarButtonItem.button.enabled = (self.placemarks != nil);

                                    if([self.placemarks count] > 0){
//                                        CLPlacemark *mark = [self.placemarks objectAtIndex:0];
                                        
//                                        PCLog(@"name:%@\n thoroughfare:%@\n subThoroughfare:%@\n locality:%@\n subLocality:%@\n administrativeArea:%@\n subAdministrativeArea:%@\n postalCode:%@\n ISOcountryCode:%@\n country:%@\n inlandWater:%@\n ocean:%@\n areasOfInterest:%@\n",
//                                              mark.name,
//                                              mark.thoroughfare,
//                                              mark.subThoroughfare,
//                                              mark.locality,
//                                              mark.subLocality,
//                                              mark.administrativeArea,
//                                              mark.subAdministrativeArea,
//                                              mark.postalCode,
//                                              mark.ISOcountryCode,
//                                              mark.country,
//                                              mark.inlandWater,
//                                              mark.ocean,
//                                              mark.areasOfInterest);
//                                        
//                                        
//                                        PCLog(@"Address:%@", mark.address);
                                    }
                                }
                            }];
}




#pragma mark - UISearchDisplayDelegate
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    return NO;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    PCLog(@"searchBarSearchButtonClicked");
    
    if([searchBar.text length] == 0){
        return;
    }
    
    [self.geocoder cancelGeocode];
    [self.geocoder geocodeAddressString:searchBar.text
                      completionHandler:^(NSArray *rtnPlacemarks, NSError *error) {
                          if(error != nil){
                              if(error.code != kCLErrorGeocodeFoundNoResult
                                 && error.code != kCLErrorGeocodeCanceled){
                                  PCError(@"geocodeAddressString:completionHandler: got error %@", error);
                              } else {
                                  
                              }
                          }
                          
                          self.searchResults = rtnPlacemarks;
                          [self.searchDisplayController.searchResultsTableView reloadData];
                      }];
}


- (void)viewDidUnload {
    [self setMapView:nil];
    [self setSearchBar:nil];
    [self setAddressLabel:nil];
    [super viewDidUnload];
}

#pragma mark - MKMapViewDelegate
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    self.navigationItem.rightBarButtonItem.button.enabled = NO;
    
    if(self.initialLocation == nil){
        self.pinAnnotation.coordinate = userLocation.coordinate;
        [self reverseGeocoding:userLocation.coordinate];

        [self mapBoundForContents:YES
                     userLocation:userLocation];
    }
}

- (void)mapBoundForContents:(BOOL)animated userLocation:(MKUserLocation *)userLocation
{
    CLLocationDegrees maxLat = -90;
    CLLocationDegrees maxLon = -180;
    CLLocationDegrees minLat = 90;
    CLLocationDegrees minLon = 180;
    
    id<MKAnnotation> lastAnnotation = nil;
    
    if([self.mapView.annotations count] > 0){
        
        for(id<MKAnnotation> annotation in self.mapView.annotations){

            CLLocationCoordinate2D coordinate = annotation.coordinate;

            if(coordinate.latitude == 0 || coordinate.longitude == 0) continue;

            if(coordinate.latitude > maxLat)
                maxLat = coordinate.latitude;
            if(coordinate.latitude < minLat)
                minLat = coordinate.latitude;
            if(coordinate.longitude > maxLon)
                maxLon = coordinate.longitude;
            if(coordinate.longitude < minLon)
                minLon = coordinate.longitude;
            
            lastAnnotation = annotation;
        }
    }
    
    if(lastAnnotation != nil){
        [self.mapView selectAnnotation:lastAnnotation animated:YES];
    }
    
    if(maxLat == -90 && maxLon == -180 && minLat == 90 && minLon == 180){
        if(APP.location != nil){
            CLLocationCoordinate2D coord = APP.location.coordinate;
            MKCoordinateSpan span = {0.01, 0.01};
            MKCoordinateRegion region = {coord, span};
            
            [self.mapView setRegion:region animated:animated];
        } else {
            if(userLocation != nil){
                CLLocationCoordinate2D coord = userLocation.coordinate;
                MKCoordinateSpan span = {0.01, 0.01};
                MKCoordinateRegion region = {coord, span};
                
                [self.mapView setRegion:region animated:animated];
            }
        }
    } else {
        MKCoordinateRegion region;
        region.center.latitude     = (maxLat + minLat) / 2;
        region.center.longitude    = (maxLon + minLon) / 2;
        region.span.latitudeDelta  = maxLat - minLat + 0.01;
        region.span.longitudeDelta = maxLon - minLon + 0.01;
        
        [self.mapView setRegion:region animated:animated];
    }
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
	static NSString *identifier = @"annotation";
	
	MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
	
	if(annotationView == nil) {
		annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                         reuseIdentifier:identifier];
        annotationView.animatesDrop = YES;
        
	}
	annotationView.annotation=annotation;
	annotationView.canShowCallout = YES;
    
    return annotationView;
    
}


#pragma mark - Accessors
- (void)setPlacemarks:(NSArray *)placemarks
{
    if([placemarks count] > 0){
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
//        self.searchBar.text = @"";
//        self.searchBar.placeholder = placemark.address;
        self.addressLabel.text = placemark.address;
    }
    _placemarks = placemarks;
}

- (PinAnnotation *)pinAnnotation
{
    if(_pinAnnotation == nil){
        _pinAnnotation = [[PinAnnotation alloc] init];
        [self.mapView addAnnotation:self.pinAnnotation];
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
    return _pinAnnotation;
}

- (CLGeocoder *)geocoder
{
    if(_geocoder == nil){
        _geocoder = [[CLGeocoder alloc] init];
    }
    return _geocoder;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.searchResults count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:cellIdentifier];
    }
    
    CLPlacemark *mark = [self.searchResults objectAtIndex:indexPath.row];
    
//    TSLog(@"name:%@\n thoroughfare:%@\n subThoroughfare:%@\n locality:%@\n subLocality:%@\n administrativeArea:%@\n subAdministrativeArea:%@\n postalCode:%@\n ISOcountryCode:%@\n country:%@\n inlandWater:%@\n ocean:%@\n areasOfInterest:%@ addressDictionary:%@\n",
//          mark.name,
//          mark.thoroughfare,
//          mark.subThoroughfare,
//          mark.locality,
//          mark.subLocality,
//          mark.administrativeArea,
//          mark.subAdministrativeArea,
//          mark.postalCode,
//          mark.ISOcountryCode,
//          mark.country,
//          mark.inlandWater,
//          mark.ocean,
//          mark.areasOfInterest,
//          mark.addressDictionary);
    
    if(mark.areasOfInterest != nil){
        NSString *interestPlace = nil;
        if([mark.areasOfInterest count] > 0){
            interestPlace = [mark.areasOfInterest objectAtIndex:0];
        }
        if(interestPlace != nil){
            cell.textLabel.text = [NSString stringWithFormat:@"[%@]%@",interestPlace,
                                   mark.address];
        } else {
            cell.textLabel.text = mark.address;
        }
    } else {
        cell.textLabel.text = mark.address;
    }
    
    return cell;

}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.searchDisplayController setActive:NO
                                   animated:YES];
    
    CLPlacemark *selectedPlacemark = [self.searchResults objectAtIndex:indexPath.row];
    
    MKCoordinateSpan KWCoordinateSpan= {0.04f, 0.04f};
    self.pinAnnotation.coordinate = selectedPlacemark.location.coordinate;
    [self.mapView setRegion:MKCoordinateRegionMake(self.pinAnnotation.coordinate,
                                                   KWCoordinateSpan)
                   animated:YES];
    self.searchBar.placeholder = selectedPlacemark.address;
    
    self.placemarks = [NSArray arrayWithObject:selectedPlacemark];
}

- (IBAction)currentLocationButtonTouched:(id)sender
{
//    self.initialLocation = nil;
    if(OVER_IOS8){
        [APP.locationManager requestWhenInUseAuthorization];
    }
    self.mapView.showsUserLocation = YES;
    
#if DEBUG

    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(34.0304970, -118.4687750);
    [self reverseGeocoding:coordinate];
    
#else
    [self reverseGeocoding:self.mapView.userLocation.coordinate];
#endif
    
    CLLocationCoordinate2D coord = self.mapView.userLocation.coordinate;
    MKCoordinateSpan span = {0.01, 0.01};
    MKCoordinateRegion region = {coord, span};
    
    [self.mapView setRegion:region animated:YES];
    
    self.pinAnnotation.coordinate = coord;
    [self reverseGeocoding:coord];
}


@end



@implementation PinAnnotation
- (CLLocationCoordinate2D)coordinate
{
    return _coordinate;
}

- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate
{
    _coordinate = newCoordinate;
}
@end
