//
//  CuisineTableViewController.m
//  RushOrder
//
//  Created by Conan on 10/27/14.
//  Copyright (c) 2014 Paycorn. All rights reserved.
//

#import "CuisineTableViewController.h"
#import "CuisineTableViewCell.h"
#import "Cuisine.h"

@interface CuisineTableViewController ()


@end

@implementation CuisineTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [APP.cuisines count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CuisineTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CuisineTableViewCell"
                                                                 forIndexPath:indexPath];
    cell.cuisine = [APP.cuisines objectAtIndex:indexPath.row];
    [cell drawData];
    
    return cell;
}

- (IBAction)applyButtontouched:(id)sender
{
    NSMutableArray *cuisines = [NSMutableArray array];
    for(Cuisine *cuisine in APP.cuisines){
        if(cuisine.isSelected){
            [cuisines addObject:cuisine];
        }
    }
    
    if([cuisines count] == 0){
        [UIAlertView alertWithTitle:NSLocalizedString(@"You Should Select At Least One Cuisine", nil)
                            message:nil];
        return;
    }
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 if([self.delegate respondsToSelector:@selector(cuisineTableViewController:didSelectCuisines:)]){
                                     
                                     
                                     [self.delegate cuisineTableViewController:self
                                                             didSelectCuisines:cuisines];
                                 }
                             }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Cuisine *cuisine = [APP.cuisines objectAtIndex:indexPath.row];
    
    if(indexPath.row == 0){
        cuisine.selected = YES;
        
        for(Cuisine *aCuisine in APP.cuisines){
            if(aCuisine.no == 0) continue;
            aCuisine.selected = NO;
        }
        
        [tableView reloadData];
        
    } else {
        
        cuisine.selected = !cuisine.isSelected;
        
        BOOL hasSelectedCuisine = NO;
        
        for(Cuisine *aCuisine in APP.cuisines){
            
            if(aCuisine.no == 0) continue;
            
            if(aCuisine.isSelected){
                hasSelectedCuisine = YES;
                break;
            }
        }
        
        Cuisine *allCuisine = [APP.cuisines objectAtIndex:0];
        if(hasSelectedCuisine){
            allCuisine.selected = NO;
        } else {
            allCuisine.selected = YES;
        }
        
        [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0
                                                               inSection:0],
                                            indexPath]
                         withRowAnimation:UITableViewRowAnimationFade];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
