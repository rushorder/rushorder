//
//  CheckOutViewController.h
//  RushOrder
//
//  Created by Conan on 10/4/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"

@interface CheckOutViewController : PCViewController
<
UITextFieldDelegate
>

@property (strong, nonatomic) TableInfo *tableInfo; //for Table service base
@property (strong, nonatomic) Order *order; //for Self service base

@end
