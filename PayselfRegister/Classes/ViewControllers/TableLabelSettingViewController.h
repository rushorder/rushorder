//
//  TableLabelSettingViewController.h
//  RushOrder
//
//  Created by Conan on 3/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCiPadViewController.h"
#import "TableLabelEditCell.h"

@interface TableLabelSettingViewController : PCiPadViewController
<
TableLabelEditCellDelegate
>
@end
