//
//  FastFoodViewController.m
//  RushOrder
//
//  Created by Conan on 7/1/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "FastFoodViewController.h"
#import "MerchantSettingViewController.h"
#import "PostitButton.h"
#import "TableStatusManager.h"
#import "OrderDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "PCMerchantService.h"
#import "TaxSettingViewController.h"
#import "MerchantAccountViewController.h"
#import "DCRoundSwitch.h"
#import "TableLabelSettingViewController.h"
#import "OrderPopoverBackgroundView.h"
#import "MenuManager.h"
#import "BixPrintManager.h"
#import "EpsonPrintManager.h"
#import "StarPrintManager.h"
#import "SwitchMenuViewController.h"

#define MINUTE_SPAN     5
#define LIMIT_HOUR      3

@interface FastFoodViewController ()

@property (strong, nonatomic) UIPopoverController *popover;
@property (strong, nonatomic) UIPopoverController *settingPopoverController;
@property (weak, nonatomic) IBOutlet PCDynamicRowGridView *neoGridView;
@property (weak, nonatomic) IBOutlet PCDynamicRowGridView *preparingGridView;
@property (weak, nonatomic) IBOutlet PCDynamicRowGridView *readyGridView;
@property (strong, nonatomic) IBOutlet UIButton *locationUpdateButton;
@property (strong, nonatomic) IBOutlet UIButton *menuSwitchButton;

@property (weak, nonatomic) IBOutlet DCRoundSwitch *openSwitch;

@property (strong, nonatomic) IBOutlet UIView *noItemView;
@property (weak, nonatomic) IBOutlet UIButton *settingButton;
@property (weak, nonatomic) IBOutlet UILabel *dashboardTitleLabel;
@end

@implementation FastFoodViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self != nil){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(signedIn:)
                                                 name:SignedInNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(openSettingView:)
                                                 name:OpenSettingPopoverViewNotification
                                               object:nil];
}

- (void)reloadGridData
{
    [self.neoGridView reloadData];
    [self.preparingGridView reloadData];
    [self.readyGridView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self reloadGridData];
    [self sumUpdated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(togoListUpdated:)
                                                 name:TableListUpdatedNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sumCountUpdated:)
                                                 name:SumCountUpdatedNotification
                                               object:nil];
    
    [self layoutDirectUpdateButtons];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:TableListUpdatedNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:SumCountUpdatedNotification
                                                  object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)sumUpdated
{
    // Do nothing yet
}

- (void)togoListUpdated:(NSNotification *)aNoti
{
    NSIndexPath *fromIndexPath = [aNoti.userInfo objectForKey:@"shiftFromIndexPath"];
    NSIndexPath *toIndexPath = [aNoti.userInfo objectForKey:@"shiftToIndexPath"];
    
    PCLog(@"fromIndexPath : %@ / toIndexPath : %@", fromIndexPath, toIndexPath);
    
    if(fromIndexPath != nil || toIndexPath != nil){
        [self shiftSectsionFrom:fromIndexPath to:toIndexPath];
    } else {
        [self reloadGridData];
    }
    
    [self sumUpdated];
}

- (void)sumCountUpdated:(NSNotification *)aNoti
{
    if(aNoti.object != nil){
        [self sumUpdated];
    }
}

- (void)signedIn:(NSNotification *)aNoti
{
//    self.merchantTitleLabel.text = CRED.merchant.name;
    self.dashboardTitleLabel.text = CRED.merchant.name;
    self.openSwitch.on = CRED.merchant.isOpenHour;
    
}

- (void)orderUpdate:(NSNotification *)aNoti
{
    if([aNoti.object isKindOfClass:[Order class]]){
        [self reloadGridData];
        [self sumUpdated];
    }
}

#define MARGIN_FOR_ITEM_AND_BOX     6.0f
#define TICKET_DEFAULT_WIDTH        194.0f
- (void)initGridView
{
    self.neoGridView.dataSrouce = self;
    self.neoGridView.delegate = self;
    self.neoGridView.tileWidth = TICKET_DEFAULT_WIDTH;
    
    self.neoGridView.outerMargin = UIEdgeInsetsMake(MARGIN_FOR_ITEM_AND_BOX,
                                                 MARGIN_FOR_ITEM_AND_BOX,
                                                 MARGIN_FOR_ITEM_AND_BOX,
                                                 MARGIN_FOR_ITEM_AND_BOX);
    self.neoGridView.itemSpan = MARGIN_FOR_ITEM_AND_BOX;

    
    self.preparingGridView.dataSrouce = self;
    self.preparingGridView.delegate = self;
    self.preparingGridView.tileWidth = TICKET_DEFAULT_WIDTH;
    
    self.preparingGridView.outerMargin = UIEdgeInsetsMake(MARGIN_FOR_ITEM_AND_BOX,
                                                 MARGIN_FOR_ITEM_AND_BOX,
                                                 MARGIN_FOR_ITEM_AND_BOX,
                                                 MARGIN_FOR_ITEM_AND_BOX);
    self.preparingGridView.itemSpan = MARGIN_FOR_ITEM_AND_BOX;

    
    self.readyGridView.dataSrouce = self;
    self.readyGridView.delegate = self;
    self.readyGridView.tileWidth = TICKET_DEFAULT_WIDTH;
    
    self.readyGridView.outerMargin = UIEdgeInsetsMake(MARGIN_FOR_ITEM_AND_BOX,
                                                 MARGIN_FOR_ITEM_AND_BOX,
                                                 MARGIN_FOR_ITEM_AND_BOX,
                                                 MARGIN_FOR_ITEM_AND_BOX);
    self.readyGridView.itemSpan = MARGIN_FOR_ITEM_AND_BOX;

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initGridView];
    
    self.openSwitch.on = CRED.merchant.isOpenHour;
    self.openSwitch.onText = @"OPEN";
    self.openSwitch.offText= @"CLOSED";
    self.openSwitch.onTintColor = [UIColor colorWithR:251.0f G:168.0f B:8.0f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)uiLogoutProcess
{
    [APP transitLogin];
    [CRED.userAccountItem resetKeychainItem];
    [NSUserDefaults standardUserDefaults].autoLogin = NO;
}

- (IBAction)signOutButtonTouched:(id)sender
{
    [self uiLogoutProcess];

    RequestResult result = RRFail;
    result = [CRED requestSignOutCompletionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              PCLog(@"Successfully signed out");
                              
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while signing out", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (IBAction)settingButtonTouched:(id)sender
{
    //    [self.actionSheet showFromBarButtonItem:sender
    //                                   animated:YES];
    
    [self.settingPopoverController dismissPopoverAnimated:NO];
    
    MerchantSettingMenuViewController *viewController = [MerchantSettingMenuViewController viewControllerFromNib];
    viewController.delegate = self;
    
    PCNavigationController *navi = [[PCNavigationController alloc] initWithRootViewController:viewController];
    
//    if(self.settingPopoverController == nil){
        self.settingPopoverController = [[UIPopoverController alloc]
                                         initWithContentViewController:navi];
        self.settingPopoverController.delegate = self;
        self.settingPopoverController.popoverBackgroundViewClass = [OrderPopoverBackgroundView class];
//    } else {
//        self.settingPopoverController.contentViewController = navi;
//    }
    
    
    if([sender isKindOfClass:[UIBarButtonItem class]]){
        [self.settingPopoverController presentPopoverFromBarButtonItem:(UIBarButtonItem *)sender
                                              permittedArrowDirections:UIPopoverArrowDirectionAny
                                                              animated:YES];
    } else {
        [self.settingPopoverController presentPopoverFromRect:((UIButton *)sender).frame
                                                       inView:self.view
                                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                                     animated:YES];
    }
    viewController.popover = self.settingPopoverController;
}

- (void)optionViewController:(OptionViewController *)viewController
didChangeOneTouchLocationUpdate:(UISwitch *)aSwitch
{
    [self layoutDirectUpdateButtons];
}

- (void)optionViewController:(OptionViewController *)viewController
   didChangeDirectMenuSwitch:(UISwitch *)aSwitch
{
    [self layoutDirectUpdateButtons];
}

- (void)layoutDirectUpdateButtons
{
    [self layoutDirectUpdateButtonsWithAnimation:NO];
}

- (void)layoutDirectUpdateButtonsWithAnimation:(BOOL)animation
{
    self.locationUpdateButton.hidden = ![NSUserDefaults standardUserDefaults].oneTouchLocationUpdate;
    self.menuSwitchButton.hidden = ![NSUserDefaults standardUserDefaults].directSwitchMenu;
//    self.menuSwitchButton.hidden = YES;
    
    CGFloat startX = self.settingButton.x - 8.0f;
    
    if(!self.locationUpdateButton.hidden){
        self.locationUpdateButton.x = startX - self.locationUpdateButton.width;
        
        startX = self.locationUpdateButton.x - 8.0f;
    }
    
    if(!self.menuSwitchButton.hidden){
        self.menuSwitchButton.x = startX - self.menuSwitchButton.width;
    }
}

- (void)openSettingView:(NSNotification *)aNoti
{
    if(self.parentViewController != nil){
        [self settingButtonTouched:self.settingButton];
    }
}

- (IBAction)reportButtonTouched:(id)sender
{
    [self.settingPopoverController dismissPopoverAnimated:NO];
    [self.popover dismissPopoverAnimated:NO];
    [APP transitReport];
}

#define MARGIN 50.0f
#pragma mark - PCDynamicRowGridViewDataSource
- (NSInteger)numberOfItemsInGridView:(PCDynamicRowGridView *)gridView
{
    NSInteger ctr = [TABLE.activeTableList count];
    
//    if(ctr == 0){
//        CGRect viewFrame = self.view.bounds;
//        viewFrame.origin.x += MARGIN;
//        viewFrame.origin.y += (MARGIN * 2);
//        viewFrame.size.width -= (MARGIN * 2);
//        viewFrame.size.height -= (MARGIN * 3);
//        
//        self.noItemView.frame = viewFrame;
//        [self.view addSubview:self.noItemView];
//    } else {
//        [self.noItemView removeFromSuperview];
//    }
    
    switch(gridView.tag){
        case 1: // New
            ctr = [TABLE.newTableList count];
            break;
        case 2: // Preparing
            ctr = [TABLE.preparingTableList count];
            break;
        case 3: // Ready
            ctr = [TABLE.readyTableList count];
            break;
    }
    
    return ctr;
}

- (UIView *)gridView:(PCDynamicRowGridView *)gridView cellForItemIndex:(NSInteger)index
{
    PostitButton *postitButton = [[PostitButton alloc] initWithFrame:CGRectZero];
    postitButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    postitButton.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    postitButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 5.0f, 7.0f);
    postitButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    postitButton.titleLabel.numberOfLines = 3;
    [postitButton setTitleColor:[UIColor whiteColor]
                      forState:UIControlStateNormal];
    [postitButton setTitleColor:[UIColor colorWithWhite:0.9f alpha:1.0f]
                      forState:UIControlStateHighlighted];
    
    [postitButton addTarget:self
                    action:@selector(postitButtonTouched:)
          forControlEvents:UIControlEventTouchUpInside];
    postitButton.swipeTarget = self;
    postitButton.swipeSelector = @selector(postitButtonSwiped:);
    
    return postitButton;
}

- (void)gridView:(PCDynamicRowGridView *)gridView updateCell:(UIView *)cell atIndex:(NSInteger)index;
{
    Order *order = nil;
    
    switch(gridView.tag){
        case 1: // New
            order = [TABLE.newTableList objectAtIndex:index];
            break;
        case 2: // Preparing
            order = [TABLE.preparingTableList objectAtIndex:index];
            break;
        case 3: // Ready
            order = [TABLE.readyTableList objectAtIndex:index];
            break;
    }
    
    if([order isKindOfClass:[Order class]]){
        
        PostitButton *postitButton = (PostitButton *)cell;
        
//        postitButton.backgroundColor = order.statusColor;
        postitButton.shapeType = gridView.tag;
        if(order.orderType == OrderTypeTakeout){
            postitButton.iconType = 2;
        } else if(order.orderType == OrderTypeDelivery){
            postitButton.iconType = 3;
        } else {
            postitButton.iconType = 1;
//            if([order.tableNumber length] > 0){
//                
//            } else {
//            }
        }

//        if(order.isDelivery){
//            postitButton.takeoutIcon.hidden = YES;
//        } else if(order.isTakeout){
//            postitButton.takeoutIcon.hidden = NO;
//        } else {
//            postitButton.takeoutIcon.hidden = YES;
//        }
        
        if([order.tableNumber length] > 0){
            postitButton.tableNumberLabel.text = order.tableNumber;
        } else {
            postitButton.tableNumberLabel.text = [NSString stringWithFormat:@"%lld", order.orderNo];
        }
        postitButton.orderNumber = [NSString stringWithFormat:@"%lld", order.orderNo];
        
        postitButton.memoLabel.text = order.lineItemNames;
        
        [postitButton setImage:nil
                      forState:UIControlStateNormal];
        
        NSString *totalAmountString = [[NSNumber numberWithCurrency:order.total] currencyString];
        NSString *payAmountString = [[NSNumber numberWithCurrency:order.netPayAmounts] currencyString];
        NSString *tipAmountString = [[NSNumber numberWithCurrency:order.tips] currencyString];
        
        if(order.isTicketBase){
            postitButton.buttonTitle = [NSString stringWithFormat:@"%@\n %@\n%@",
                                        payAmountString,
                                        tipAmountString,
                                        order.grandTotalString];
        } else {
            postitButton.buttonTitle = [NSString stringWithFormat:@"%@\n(%@)\n%@",
                                        payAmountString,
                                        tipAmountString,
                                        totalAmountString];
        }
    } else {
        //
    }
}

- (void)postitButtonSwiped:(PostitButton *)sender
{
    NSInteger index = sender.index;
    
    Order *order = nil;
    
    switch(sender.section){
        case 1: // New
            order = [TABLE.newTableList objectAtIndex:index];
            break;
        case 2: // Preparing
            order = [TABLE.preparingTableList objectAtIndex:index];
            break;
        case 3: // Ready
            order = [TABLE.readyTableList objectAtIndex:index];
            break;
    }
    
    OrderStatus newOrderStatus = OrderStatusUnknown;
    switch(order.status){
        case OrderStatusPaid:
//            if(CRED.merchant.isRoService && order.isDelivery){
//                newOrderStatus = OrderStatusAccepted;
//            } else {
//                newOrderStatus = OrderStatusConfirmed;
//            }
            newOrderStatus = OrderStatusConfirmed;
            break;
        case OrderStatusConfirmed:
        case OrderStatusAccepted:
//            if(CRED.merchant.isRoService && order.isDelivery){
//                newOrderStatus = OrderStatusPrepared;
//            } else {
//                newOrderStatus = OrderStatusReady;
//            }
            newOrderStatus = OrderStatusReady;
            break;
        case OrderStatusReady:
            newOrderStatus = OrderStatusFixed;
            break;
        default:
            break;
    }
    
    if(newOrderStatus != OrderStatusUnknown){
        Order *updateOrder = [order copy];
        updateOrder.status = newOrderStatus;
        
        if(order.isRoService && updateOrder.isDelivery){
            if(newOrderStatus == OrderStatusConfirmed){
                
                NSInteger lastUsedInterval = [NSUserDefaults standardUserDefaults].lastUsedInterval;
                lastUsedInterval = MAX(lastUsedInterval, MINUTE_SPAN);
                
                if(lastUsedInterval == MINUTE_SPAN){
                    NSDate *now = [NSDate date];
                    NSTimeInterval afterInterval = 20 * 60; //20 mins for default
                    NSDate *afterNow = [now rount5dateByAddingTimeInterval:afterInterval];
                    updateOrder.expectedPreparingDate = afterNow;
                } else {
                    NSDate *lastUsedIntervalDate = [[NSDate date] dateByAddingTimeInterval:(lastUsedInterval * 60)];
                    updateOrder.expectedPreparingDate = lastUsedIntervalDate;
                }
            }
            
            if(order.status == OrderStatusConfirmed){
                [UIAlertView alertWithTitle:NSLocalizedString(@"Driver is On The Way!",nil)
                                    message:NSLocalizedString(@"Please call us if you need assistance.", nil)];
                return;
            } else if (order.status == OrderStatusReady){
                [UIAlertView alertWithTitle:NSLocalizedString(@"Delivery in Progress",nil)
                                    message:NSLocalizedString(@"Order status will automatically change once it's been delivered.", nil)];
                return;
            }
        } else if (updateOrder.orderType == OrderTypeDelivery){
            if(newOrderStatus == OrderStatusConfirmed){
                NSInteger lastUsedDeliveryInterval = [NSUserDefaults standardUserDefaults].lastUsedDeliveryInterval;
                lastUsedDeliveryInterval = MAX(lastUsedDeliveryInterval, MINUTE_SPAN);
                
                if(lastUsedDeliveryInterval == MINUTE_SPAN){
                    NSDate *now = [NSDate date];
                    NSTimeInterval afterInterval = CRED.merchant.averageDeliveryTime * 60;
                    NSDate *afterNow = [now rount5dateByAddingTimeInterval:afterInterval];
                    updateOrder.expectedDate = afterNow;
                } else {
                    NSDate *lastUsedDeliveryIntervalDate = [[NSDate date] dateByAddingTimeInterval:(lastUsedDeliveryInterval * 60)];
                    updateOrder.expectedDate = lastUsedDeliveryIntervalDate;
                }
            }
        } else if (updateOrder.orderType == OrderTypeTakeout ||
                   updateOrder.orderType == OrderTypePickup ){
            if(newOrderStatus == OrderStatusConfirmed){
                NSInteger lastUsedInterval = [NSUserDefaults standardUserDefaults].lastUsedInterval;
                lastUsedInterval = MAX(lastUsedInterval, MINUTE_SPAN);
                
                if(lastUsedInterval == MINUTE_SPAN){
                    NSDate *now = [NSDate date];
                    NSTimeInterval afterInterval = 20 * 60; //20 mins for default
                    NSDate *afterNow = [now rount5dateByAddingTimeInterval:afterInterval];
                    updateOrder.expectedDate = afterNow;
                } else {
                    NSDate *lastUsedIntervalDate = [[NSDate date] dateByAddingTimeInterval:(lastUsedInterval * 60)];
                    updateOrder.expectedDate = lastUsedIntervalDate;
                }
            }
        }
        
        
        
//        if(newOrderStatus == OrderStatusReady && updateOrder.isDelivery){
//            NSDate *now = [NSDate date];
//            NSTimeInterval afterInterval = CRED.merchant.averageDeliveryTime * 60;
//            NSDate *afterNow = [now rount5dateByAddingTimeInterval:afterInterval];
//            updateOrder.expectedDate = afterNow;
//        }
        
        [self requestUpdateWithOrder:updateOrder withOrder:order];
    } else {
//        if(order.isDelivery){
//            if(order.status == OrderStatusPrepared){
//                [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot Make This Order Completed",nil)
//                                    message:NSLocalizedString(@"Sorry! You should wait until the driver marks this order Picked Up", nil)];
//            } else if(order.status == OrderStatusReady){
//                [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot Make This Order Completed",nil)
//                                    message:NSLocalizedString(@"Sorry! You should wait until the driver marks this order Delivered(Completed)", nil)];
//            }
//        }
    }
}

- (void)requestUpdateWithOrder:(Order *)updateOrder withOrder:(Order *)order
{
    RequestResult result = RRFail;
    result = [MERCHANT requestUpdateOrder:updateOrder
                          completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                          {
                              NSMutableArray *paymentArray = [NSMutableArray array];
                              NSArray *payments = [response objectForKey:@"payments"];
                              for(NSDictionary *aPaymentDict in payments){
                                  Payment *payment = [[Payment alloc] initWithDictionary:aPaymentDict];
                                  [paymentArray addObject:payment];
                              }
                              order.paymentList = paymentArray;
                              
                              [order updateWithDictionary:response.data
                                                 merchant:nil
                                                  menuPan:MENUPAN.wholeSerialMenuList];
                              
                              [order updateSumWithPayments:order.paymentList];
                              
                              if(order.status == OrderStatusFixed ||
                                 order.status == OrderStatusCanceled){
                                  
                                  // Do nothing
                              }
                              
                              NSDictionary *userInfo = nil;
                              
                              userInfo = [NSDictionary dictionaryWithObject:order forKey:@"order"];
                              
                              [[NSNotificationCenter defaultCenter] postNotificationName:TableInfoChangedNotification
                                                                                  object:self
                                                                                userInfo:userInfo];
                              
                              if(order.status == OrderStatusConfirmed){
                                  if(![NSUserDefaults standardUserDefaults].printWhenNewOrder){
                                      if([NSUserDefaults standardUserDefaults].autoKitchenPrint){
                                          long bixRtn = 0;
                                          
                                          switch(APP.selectedMakerCode){
                                              case PrinterMakerEpson:
                                                  bixRtn = [EPSON printOrder:order];
                                                  break;
                                              case PrinterMakerStar:
                                                  bixRtn = [STAR printOrder:order];
                                                  break;
                                              case PrinterMakerBixolon:
                                                  bixRtn = [BIX printOrder:order];
                                                  break;
                                              default:
                                                  bixRtn = RUSHORDER_PRINTER_NOT_SET;
                                                  break;
                                          }
                                          
                                          [self treatPrintError:bixRtn];
                                      }
                                      
                                      if([NSUserDefaults standardUserDefaults].autoReceiptPrint){
                                          long bixRtn = 0;
                                          
                                          switch(APP.selectedMakerCode){
                                              case PrinterMakerEpson:
                                                  bixRtn = [EPSON printReceipt:[[ReceiptWrapper alloc] initWithOrder:order withPaymentIndex:0] atIndex:0];
                                                  break;
                                              case PrinterMakerStar:
                                                  bixRtn = [STAR printReceipt:[[ReceiptWrapper alloc] initWithOrder:order withPaymentIndex:0] atIndex:0];
                                                  break;
                                              case PrinterMakerBixolon:
                                                  bixRtn = [BIX printReceipt:[[ReceiptWrapper alloc] initWithOrder:order withPaymentIndex:0] atIndex:0];
                                                  break;
                                              default:
                                                  bixRtn = RUSHORDER_PRINTER_NOT_SET;
                                                  break;
                                          }
                                          
                                          [self treatPrintError:bixRtn];
                                      }
                                  }
                              }
                              
                          }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while updating this order", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}


- (void)postitButtonTouched:(PostitButton *)sender
{
    NSInteger index = sender.index;
    
    Order *order = nil;
    
    switch(sender.section){
        case 1: // New
            order = [TABLE.newTableList objectAtIndex:index];
            break;
        case 2: // Preparing
            order = [TABLE.preparingTableList objectAtIndex:index];
            break;
        case 3: // Ready
            order = [TABLE.readyTableList objectAtIndex:index];
            break;
    }
    
    OrderDetailViewController *viewController = [OrderDetailViewController viewControllerFromNib];
    viewController.preferredContentSize = CGSizeMake(320.f, 900.f);
    viewController.order = order;
    
    PCNavigationController *navi = [[PCNavigationController alloc] initWithRootViewController:viewController];
    navi.navigationBarHidden = YES;
    
//    if(self.popover == nil){
        UIPopoverController *popoverController = [[UIPopoverController alloc]
                                                  initWithContentViewController:navi];
        popoverController.popoverBackgroundViewClass = [OrderPopoverBackgroundView class];
    
        self.popover = popoverController;
        self.popover.delegate = self;
//    } else {
//        self.popover.contentViewController = navi;
//    }
    
    viewController.popover = self.popover;
    [self.popover presentPopoverFromRect:[self.view convertRect:sender.frame
                                                       fromView:sender.superview]
                                  inView:self.view
                permittedArrowDirections:UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionRight
                                animated:YES];    
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex){
        case 0: //Merchant information
            [self showMerchantInformationSetting];
            break;
        case 1: //Table labels
//            [self showTableLabelSettings];
            break;
    }
}

- (void)showMerchantInformationSetting
{
    MerchantSettingViewController *viewController = [MerchantSettingViewController viewControllerFromNib];
    viewController.modalPresentationStyle = UIModalPresentationFormSheet;
    viewController.merchantNo = CRED.merchant.merchantNo;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^(){
                                     
                                 }];
}


#pragma mark - MerchantSettingMenuViewControllerDelegate
- (BOOL)merchantSettingMenuViewController:(MerchantSettingMenuViewController *)viewController
                            didSelectMenu:(TableItem *)tableItem
{
    if([self respondsToSelector:tableItem.action] ){
        [self performSelector:tableItem.action
                   withObject:viewController];
        return YES;
    } else {
        return NO;
    }
}

- (void)merchantSettingMenuViewControllerWillChangeToLive:(MerchantSettingMenuViewController *)viewController
{
    [self accountSetting:viewController];
}

- (void)merchantInformation:(id)sender
{
    [self.settingPopoverController dismissPopoverAnimated:YES];
    self.settingPopoverController = nil;
    
    MerchantSettingViewController *viewController = [MerchantSettingViewController viewControllerFromNib];
    viewController.modalPresentationStyle = UIModalPresentationFormSheet;
    viewController.merchantNo = CRED.merchant.merchantNo;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^(){
                                     
                                 }];
}

- (void)taxSetting:(id)sender
{
    [self.settingPopoverController dismissPopoverAnimated:YES];
    self.settingPopoverController = nil;
    
    TaxSettingViewController *viewController = [TaxSettingViewController viewControllerFromNib];
    viewController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^(){
                                 }];
}

- (void)accountSetting:(id)sender
{
    [self.settingPopoverController dismissPopoverAnimated:YES];
    self.settingPopoverController = nil;
    
    MerchantAccountViewController *viewController = [MerchantAccountViewController viewControllerFromNib];
    viewController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^(){
                                 }];
}

- (IBAction)onSwitchValueChanged:(UISwitch *)sender
{
    if(!sender.on){
        [UIAlertView askWithTitle:NSLocalizedString(@"Confirm Close?", nil)
                          message:NSLocalizedString(@"Are you sure to close the restaurant?", nil)
                         delegate:self
                              tag:303];
    } else {
        [self requestMerchantModeChangeWithType:MerchantSwitchTypeOpen value:sender.on];
    }
}


- (void)requestMerchantModeChangeWithType:(MerchantSwitchType)type
                                    value:(BOOL)value
{
    RequestResult result = RRFail;
    result = [MERCHANT requestMerchant:CRED.merchant
                                  type:type
                                 value:value
                       completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              switch(type){
                                  case MerchantSwitchTypeTest:
                                      CRED.merchant.testMode = value;
                                      break;
                                  case MerchantSwitchTypeOpen:
                                      CRED.merchant.open = value;
                                      CRED.merchant.lastOperationDate = [response dateForKey:@"last_modified_open_close_at"];
                                      break;
                                  case MerchantSwitchTypePublish:
                                      CRED.merchant.published = value;
                                      break;
                              }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while changing open status", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  
                  self.openSwitch.on = CRED.merchant.isOpenHour;
                  [SVProgressHUD dismiss];
              }];
    
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)manageTablelabels:(id)sender
{
    [self.settingPopoverController dismissPopoverAnimated:YES];
    self.settingPopoverController = nil;
    
    TableLabelSettingViewController *viewController = [TableLabelSettingViewController viewControllerFromNib];
    viewController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^(){
                                     
                                 }];
}

- (IBAction)shiftTest:(id)sender
{
    [self shiftSectsionFrom:[NSIndexPath indexPathForRow:1 inSection:0]
                         to:[NSIndexPath indexPathForRow:1 inSection:2]];
    
}

- (void)shiftSectsionFrom:(NSIndexPath *)from to:(NSIndexPath *)to
{
    PCDynamicRowGridView *fromGridView = nil;
    if(from != nil){
        switch(from.section){
            case 0:
                fromGridView = self.neoGridView;
                break;
            case 1:
                fromGridView = self.preparingGridView;
                break;
            case 2:
                fromGridView = self.readyGridView;
                break;
        }
    }
    
    PCDynamicRowGridView *toGridView = nil;
    if(to != nil){
        switch(to.section){ // !!!:If to is nil, section return 0.
            case 0:
                toGridView = self.neoGridView;
                break;
            case 1:
                toGridView = self.preparingGridView;
                break;
            case 2:
                toGridView = self.readyGridView;
                break;
        }
    }
    
    PostitButton *button = [fromGridView buttonAtIndex:from.row];
    
    if(button == nil){
        PCError(@"Cannot find button at index : %d", from.row);
        return;
    }
    
    CGRect rectForSuperview = [self.view convertRect:button.frame
                                            fromView:fromGridView];
    [self.view addSubview:button];
    button.frame = rectForSuperview;
    
    CGRect toFrame = CGRectZero;
    CGRect toFrameForSuperview = rectForSuperview;
    if(to != nil){
        button.index = to.row;
        button.section = (to.section + 1); //tag of scrollview
        
        toFrame = [toGridView frameAtIndex:to.row];
        toFrameForSuperview = [self.view convertRect:toFrame
                                            fromView:toGridView];
    } else {
        toFrameForSuperview.origin.y += 700.0f; // Throw away!
    }
    
    [UIView animateWithDuration:0.2f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         button.frame = toFrameForSuperview;
                         
                         [self gridView:toGridView updateCell:button atIndex:button.index];
                         [fromGridView shiftLeftFromIndex:from.row];
                         if(to != nil){
                             [toGridView shiftRightFromIndex:to.row];
                         }
                         
                     }
                     completion:^(BOOL finished) {
                         if(finished){
                             if(to != nil){
                                 [toGridView insertSubview:button atIndex:to.row];
                                 button.frame = toFrame;
                             } else {
                                 [button removeFromSuperview];
                             }
                         }
                     }];
}

- (void)treatPrintError:(long)errorCode
{
    switch(APP.selectedMakerCode){
        case PrinterMakerEpson:
            [EPSON treatPrintError:errorCode];
            break;
        case PrinterMakerStar:
            [STAR treatPrintError:errorCode];
            break;
        case PrinterMakerBixolon:
            [BIX treatPrintError:errorCode];
            break;
        default:
            if(errorCode == RUSHORDER_PRINTER_NOT_SET){
                [UIAlertView alertWithTitle:NSLocalizedString(@"Printer is Not Set", nil)
                                    message:NSLocalizedString(@"Printer maker and model are not set. Please go to Settings > Print Settings to set.", nil)
                                   delegate:self
                                        tag:104];
            }
            break;
    }
}

- (IBAction)locationUpdateButtonTouched:(id)sender
{
    MerchantLocationViewController *viewController = [MerchantLocationViewController viewControllerFromNib];
    viewController.modalPresentationStyle = UIModalPresentationFormSheet;
    viewController.initialLocation = CRED.merchant.location;
    viewController.delegate = self;
    
    [self presentViewControllerInNavigation:viewController
                                   animated:YES
                                 completion:^(){
                                 }];
}

- (IBAction)switchMenuButtonTouched:(id)sender
{
    [self.settingPopoverController dismissPopoverAnimated:NO];
    
    SwitchMenuViewController *viewController = [SwitchMenuViewController viewControllerFromNib];
    viewController.title = NSLocalizedString(@"Switch Menu", nil);
    PCNavigationController *navi = [[PCNavigationController alloc] initWithRootViewController:viewController];
    
//    if(self.settingPopoverController == nil){
        self.settingPopoverController = [[UIPopoverController alloc]
                                         initWithContentViewController:navi];
        self.settingPopoverController.delegate = self;
        self.settingPopoverController.popoverBackgroundViewClass = [OrderPopoverBackgroundView class];
//    } else {
//        self.settingPopoverController.contentViewController = navi;
//    }
    
    if([sender isKindOfClass:[UIBarButtonItem class]]){
        [self.settingPopoverController presentPopoverFromBarButtonItem:(UIBarButtonItem *)sender
                                              permittedArrowDirections:UIPopoverArrowDirectionAny
                                                              animated:YES];
    } else {
        [self.settingPopoverController presentPopoverFromRect:((UIButton *)sender).frame
                                                       inView:self.view
                                     permittedArrowDirections:UIPopoverArrowDirectionAny
                                                     animated:YES];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 303:
            if(buttonIndex == AlertButtonIndexYES){
                [self requestMerchantModeChangeWithType:MerchantSwitchTypeOpen value:NO];
            } else {
                self.openSwitch.on = YES;
            }
            break;
        default:
            break;
    }
}

- (void)mapViewController:(MerchantLocationViewController *)mapViewController
      didSelectPlacemarks:(NSArray *)placemarks
            pinAnnotation:(PinAnnotation *)pinAnnotation
{
    if([placemarks count] > 0){
        CLPlacemark *placemark = [placemarks objectAtIndex:0];

        CLLocation *location = [[CLLocation alloc] initWithLatitude:pinAnnotation.coordinate.latitude
                                              longitude:pinAnnotation.coordinate.longitude];
        
        RequestResult result = RRFail;
        result = [MERCHANT requestMerchant:CRED.merchant
                                  location:location
                                geoAddress:placemark.address
                           completionBlock:
                  ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                      
                      if(isSuccess && 200 <= statusCode && statusCode < 300){
                          switch(response.errorCode){
                              case ResponseSuccess:
                                  
                                  CRED.merchant.location = location;
                                  
                                  break;
                              default:{
                                  NSString *message = [response objectForKey:@"message"];
                                  if([message isKindOfClass:[NSString class]]){
                                      [UIAlertView alert:message];
                                  } else {
                                      [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while setting location", nil)];
                                  }
                              }
                                  break;
                          }
                      }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                          [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                          //Signed out in requestMyCreditWithCompletionBlock
                      } else {
                          if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                              [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                          } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                              [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                                  ,statusCode]];
                          }
                      }
                      [SVProgressHUD dismiss];
                  }];
        switch(result){
            case RRSuccess:
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                break;
            case RRParameterError:
                [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                break;
            default:
                break;
        }
        
    } else {
    }
}


@end
