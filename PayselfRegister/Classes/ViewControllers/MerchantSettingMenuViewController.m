//
//  MerchantSettingMenuViewController.m
//  RushOrder
//
//  Created by Conan on 4/2/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "MerchantSettingMenuViewController.h"
#import "MerchantSettingViewController.h"
#import "TableLabelSettingViewController.h"
#import "EULAViewController.h"
#import "MerchantAboutViewController.h"
#import "PCMerchantService.h"
#import "TableStatusManager.h"
#import "DCRoundSwitch.h"
#import "PrintSettingViewController.h"
#import "TaxSettingViewController.h"
#import "MerchantAccountViewController.h"
#import "JoinedMerchantViewController.h"
#import "AdditionalViewController.h"
#import "ChangePasswordMerchantViewController.h"
#import "OptionViewController.h"
#import "SwitchMenuViewController.h"

enum _tableItemTag{
    NoMoreTag = 98,
    MoreTag = 99,
    DemoModeTag = 101,
    PublishTag = 102,
    OpenCloseTag = 103,
    LocationUpdateTag = 104
};

@interface MerchantSettingMenuViewController ()

@property (weak, nonatomic) IBOutlet UILabel *mailLabel;
@property (strong, nonatomic) NSMutableArray *sectionList;
@property (strong, nonatomic) DCRoundSwitch *demoSwitch;
@property (strong, nonatomic) DCRoundSwitch *openCloseSwitch;
@property (strong, nonatomic) UISwitch *publishSwitch;
@end

@implementation MerchantSettingMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self != nil){
        [self commonInit];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.openCloseSwitch.on = CRED.merchant.isOpenHour;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        BOOL didFound = NO;
        NSInteger aSection = 0;
        NSInteger aRow = 0;
        for(TableSection *section in self.sectionList){
            for(TableItem *item in section.menus){
                if(item.tag == LocationUpdateTag){
                    didFound = YES;
                    break;
                }
                aRow++;
            }
            if(didFound) break;
            aSection++;
        }
        
        if([NSUserDefaults standardUserDefaults].oneTouchLocationUpdate){
            if(!didFound){
                //Add
                if([self.sectionList count] > 0){
                    TableSection *section = [self.sectionList objectAtIndex:0];
                    TableItem *tableItem = [[TableItem alloc] init];
                    tableItem.title = NSLocalizedString(@"Direct Location Update", nil);
                    tableItem.action = @selector(directLocationUpdate:);
                    tableItem.tag = LocationUpdateTag;
                    [section.menus addObject:tableItem];
                    
                    [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:[section.menus count] - 1
                                                                                inSection:0]]
                                                             withRowAnimation:UITableViewRowAnimationRight];
                }
            }
        } else {
            if(didFound){
                if([self.sectionList count] > aSection){
                    TableSection *section = [self.sectionList objectAtIndex:aSection];
                    [section.menus removeObjectAtIndex:aRow];
                    
                    [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:aRow
                                                                                inSection:aSection]]
                                          withRowAnimation:UITableViewRowAnimationRight];
                }
            }
        }
    }
}

- (void)commonInit
{
    self.sectionList = [NSMutableArray array];
    
    ///////////////////////// Sections /////////////////////////////////////////
    NSMutableArray *sectionItems = [NSMutableArray array];
    
    //Menu
    TableItem *tableItem = nil;
    TableSection *tableSection = nil;
    
    if(CRED.userInfo.role == UserRoleOwner){
        tableItem = [[TableItem alloc] init];
        tableItem.title = NSLocalizedString(@"Merchant Information", nil);
        tableItem.action = @selector(merchantInformation:);
        [sectionItems addObject:tableItem];
        
//        tableItem = [[TableItem alloc] init];
//        tableItem.title = NSLocalizedString(@"Setting", nil);
//        tableItem.action = @selector(merchantSetting:);
//        [sectionItems addObject:tableItem];
        
        tableItem = [[TableItem alloc] init];
        tableItem.title = NSLocalizedString(@"Additional Information", nil);
        tableItem.action = @selector(additionalSetting:);
        tableItem.tag = MoreTag;
        [sectionItems addObject:tableItem];
        
        tableItem = [[TableItem alloc] init];
        tableItem.title = NSLocalizedString(@"Tax, Delivery, Tip", nil);
        tableItem.action = @selector(taxSetting:);
        [sectionItems addObject:tableItem];
        
        tableItem = [[TableItem alloc] init];
        tableItem.title = NSLocalizedString(@"Options", nil);
        tableItem.action = @selector(optionSetting:);
        tableItem.tag = MoreTag;
        [sectionItems addObject:tableItem];
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone
           && [NSUserDefaults standardUserDefaults].oneTouchLocationUpdate){
            tableItem = [[TableItem alloc] init];
            tableItem.title = NSLocalizedString(@"Direct Location Update", nil);
            tableItem.action = @selector(directLocationUpdate:);
            tableItem.tag = LocationUpdateTag;
            [sectionItems addObject:tableItem];
        }
        
        tableItem = [[TableItem alloc] init];
        tableItem.title = NSLocalizedString(@"Switch Menu", nil);
        tableItem.action = @selector(switchMenu:);
        tableItem.tag = MoreTag;
        [sectionItems addObject:tableItem];
        
    } else if(CRED.userInfo.role == UserRoleStaff){
        if([CRED.merchants count] > 1){
            tableItem = [[TableItem alloc] init];
            tableItem.title = NSLocalizedString(@"Switch Merchant", nil);
            tableItem.action = @selector(myMerchants:);
            [sectionItems addObject:tableItem];
        }
    }
    
    
    //////
    
    //Section
    if([sectionItems count] > 0){
        tableSection = [[TableSection alloc] init];
        tableSection.title = nil;
        tableSection.menus = sectionItems;
        [self.sectionList addObject:tableSection];
    }
    //////////
    
    ///////////////////////// Sections /////////////////////////////////////////
    sectionItems = [NSMutableArray array];
    
    if(CRED.userInfo.role == UserRoleOwner){
        if(CRED.merchant.servicedBy == ServicedByStaff || CRED.merchant.isTableBase){
            //Menu
            tableItem = [[TableItem alloc] init];
            tableItem.title = NSLocalizedString(@"Manage Table Labels", nil);
            tableItem.action = @selector(manageTablelabels:);        
            [sectionItems addObject:tableItem];
            //////
        }
    }
    
    //Menu
    tableItem = [[TableItem alloc] init];
    tableItem.title = NSLocalizedString(@"Print Setting", nil);
    tableItem.action = @selector(printSetting:);
    tableItem.tag = MoreTag;
    [sectionItems addObject:tableItem];
    //////
    
    //Section
    tableSection = [[TableSection alloc] init];
    tableSection.title = nil;
    tableSection.menus = sectionItems;
    [self.sectionList addObject:tableSection];
    /////////
    
    
    ///////////////////////// Sections /////////////////////////////////////////
    sectionItems = [NSMutableArray array];

    //Publish
//    tableItem = [[TableItem alloc] init];
//    tableItem.title = NSLocalizedString(@"Publish", nil);
//    tableItem.action = nil;
//    tableItem.tag = PublishTag;
//    [sectionItems addObject:tableItem];
    ///////
    
    if(CRED.userInfo.role == UserRoleOwner){
        //Menu
        tableItem = [[TableItem alloc] init];
        tableItem.title = NSLocalizedString(@"Real/Test Mode", nil);
        tableItem.action = nil;
        tableItem.tag = DemoModeTag;
        [sectionItems addObject:tableItem];
        ///////
    }
    
//    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        tableItem = [[TableItem alloc] init];
        tableItem.title = NSLocalizedString(@"Open/Close", nil);
        tableItem.action = nil;
        tableItem.tag = OpenCloseTag;
        [sectionItems addObject:tableItem];
//    }
    
    tableItem = [[TableItem alloc] init];
    tableItem.title = NSLocalizedString(@"Change Password", nil);
    tableItem.action = @selector(changePassword:);
    tableItem.tag = MoreTag;
    [sectionItems addObject:tableItem];
    
    
    if([sectionItems count] > 0){
        //Section
        tableSection = [[TableSection alloc] init];
        tableSection.title = nil;
        tableSection.menus = sectionItems;
        [self.sectionList addObject:tableSection];
        /////////
    }
    
    
    
    ////////////////////////////////////////////////////////////////////////////
    
    ///////////////////////// Sections /////////////////////////////////////////
    sectionItems = [NSMutableArray array];
    
    //Menu
    //CFBundleShortVersionString
    NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString *appShortVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
    tableItem = [[TableItem alloc] init];
    tableItem.title = NSLocalizedString(@"About", nil);
    tableItem.subTitle = [NSString stringWithFormat:@"v%@ (%@)", appShortVersion, appVersion];
    tableItem.action = @selector(about:);
    
    [sectionItems addObject:tableItem];
    //////
    
    //Menu
    tableItem = [[TableItem alloc] init];
    tableItem.title = NSLocalizedString(@"User Agreement", nil);
    tableItem.action = @selector(termsNCondition:);
    [sectionItems addObject:tableItem];
    //////
    //Menu
    tableItem = [[TableItem alloc] init];
    tableItem.title = NSLocalizedString(@"Privacy Policy", nil);
    tableItem.action = @selector(privacy:);
    [sectionItems addObject:tableItem];
    //////
    
    //Download wallet app
    tableItem = [[TableItem alloc] init];
    tableItem.title = NSLocalizedString(@"Download App for Customer", nil);
    tableItem.action = @selector(downloadWalletApp:);
    tableItem.tag = NoMoreTag;
    [sectionItems addObject:tableItem];
    ///////
    
    //Section
    tableSection = [[TableSection alloc] init];
    tableSection.title = nil;
    tableSection.menus = sectionItems;
    [self.sectionList addObject:tableSection];
    /////////
    
    ////////////////////////////////////////////////////////////////////////////
    
}



- (void)changePassword:(id)sender
{
    ChangePasswordMerchantViewController *viewController = [ChangePasswordMerchantViewController viewControllerFromNib];
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}


- (void)myMerchants:(TableItem *)tableItem
{
    JoinedMerchantViewController *viewController = [JoinedMerchantViewController viewControllerFromNib];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)merchantInformation:(TableItem *)tableItem
{
    MerchantSettingViewController *viewController = [MerchantSettingViewController viewControllerFromNib];
    viewController.merchantNo = CRED.merchant.merchantNo;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)merchantSetting:(TableItem *)tableItem
{
    
}

- (void)taxSetting:(TableItem *)tableItem
{
    TaxSettingViewController *viewController = [TaxSettingViewController viewControllerFromNib];
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)optionSetting:(TableItem *)tableItem
{
    OptionViewController *viewController = [OptionViewController viewControllerFromNib];
    viewController.delegate = self.delegate;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)directLocationUpdate:(TableItem *)tableItem
{
    MerchantLocationViewController *viewController = [MerchantLocationViewController viewControllerFromNib];
    viewController.modalPresentationStyle = UIModalPresentationFormSheet;
    viewController.initialLocation = CRED.merchant.location;
    viewController.delegate = self;
    
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)mapViewController:(MerchantLocationViewController *)mapViewController
      didSelectPlacemarks:(NSArray *)placemarks
            pinAnnotation:(PinAnnotation *)pinAnnotation
{
    if([placemarks count] > 0){
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:pinAnnotation.coordinate.latitude
                                                          longitude:pinAnnotation.coordinate.longitude];
        
        RequestResult result = RRFail;
        result = [MERCHANT requestMerchant:CRED.merchant
                                  location:location
                                geoAddress:placemark.address
                           completionBlock:
                  ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                      
                      if(isSuccess && 200 <= statusCode && statusCode < 300){
                          switch(response.errorCode){
                              case ResponseSuccess:
                                  
                                  CRED.merchant.location = location;
                                  
                                  break;
                              default:{
                                  NSString *message = [response objectForKey:@"message"];
                                  if([message isKindOfClass:[NSString class]]){
                                      [UIAlertView alert:message];
                                  } else {
                                      [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while setting location", nil)];
                                  }
                              }
                                  break;
                          }
                      }  else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                          [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                          //Signed out in requestMyCreditWithCompletionBlock
                      } else {
                          if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                              [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                                  object:nil];
                          } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                              [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                                  ,statusCode]];
                          }
                      }
                      [SVProgressHUD dismiss];
                  }];
        switch(result){
            case RRSuccess:
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
                break;
            case RRParameterError:
                [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
                break;
            default:
                break;
        }
        
    } else {
    }
}

- (void)switchMenu:(TableItem *)tableItem
{
    SwitchMenuViewController *viewController = [SwitchMenuViewController viewControllerFromNib];
    viewController.title = tableItem.title;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)additionalSetting:(TableItem *)tableItem
{
    AdditionalViewController *viewController = [AdditionalViewController viewControllerFromNib];
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)manageTablelabels:(TableItem *)tableItem
{
    TableLabelSettingViewController *viewController = [TableLabelSettingViewController viewControllerFromNib];
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)printSetting:(TableItem *)tableItem
{
    PrintSettingViewController *viewController = [PrintSettingViewController viewControllerFromNib];
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)termsNCondition:(TableItem *)tableItem
{
    EULAViewController *viewController = [EULAViewController viewControllerFromNib];
    viewController.title = tableItem.title;
    ((EULAViewController *) viewController).urlString = @"http://rushorderapp.com/legal/merchant_ua/";
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)privacy:(TableItem *)tableItem
{
    EULAViewController *viewController = [EULAViewController viewControllerFromNib];
    viewController.title = tableItem.title;
    ((EULAViewController *) viewController).urlString = @"http://rushorderapp.com/legal/privacy";
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)about:(TableItem *)tableItem
{
    MerchantAboutViewController *viewController = [MerchantAboutViewController viewControllerFromNib];
    viewController.versionString = tableItem.subTitle;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
    
}

- (void)downloadWalletApp:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:CUSTOMER_ITUNES_URL]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = NSLocalizedString(@"Settings", nil);
    
    CGFloat height = 484.0f;
    
    if(!CRED.merchant.isTableBase){
        height = 424.0f;
    }
    
//    self.contentSizeForViewInPopover = CGSizeMake(self.view.frame.size.width
//                                                  , height);
    self.preferredContentSize = CGSizeMake(self.view.frame.size.width
                                           , height);
    
    self.mailLabel.text = CRED.userInfo.email;
    self.openCloseSwitch.on = CRED.merchant.isOpenHour;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.sectionList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    TableSection *tableSection = [self.sectionList objectAtIndex:section];
    return [tableSection.menus count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                      reuseIdentifier:CellIdentifier];
    }
    
    TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
    TableItem *tableItem = [tableSection.menus objectAtIndex:indexPath.row];
    
    cell.textLabel.text = tableItem.title;
    
    if(tableItem.action != NULL){
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    } else {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.detailTextLabel.text = nil;
    
    switch(tableItem.tag){
        case MoreTag:
        case LocationUpdateTag:
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            break;
        case PublishTag:
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.accessoryView = self.publishSwitch;
            self.publishSwitch.on = CRED.merchant.isPublished;
            cell.detailTextLabel.text = CRED.merchant.isPublished ? NSLocalizedString(@"PUBLISHED", nil) : NSLocalizedString(@"HIDDEN", nil);
            break;
        case DemoModeTag:
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.accessoryView = self.demoSwitch;
            self.demoSwitch.on = !CRED.merchant.testMode;
            break;
        case OpenCloseTag:
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.accessoryView = self.openCloseSwitch;
            self.openCloseSwitch.on = CRED.merchant.isOpenHour;
            break;
        default:
            cell.detailTextLabel.text = tableItem.subTitle;
            cell.accessoryView = nil;
            break;
    }
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        if(tableItem.tag != DemoModeTag && tableItem.tag != PublishTag && tableItem.tag != NoMoreTag){
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableSection *tableSection = [self.sectionList objectAtIndex:indexPath.section];
    TableItem *tableItem = [tableSection.menus objectAtIndex:indexPath.row];
    
    if(tableItem.action != NULL){
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
            [self performSelector:tableItem.action
                       withObject:tableItem];
        } else {
            BOOL performed = NO;
            
            if([self.delegate respondsToSelector:@selector(merchantSettingMenuViewController:didSelectMenu:)]){
                performed = [self.delegate merchantSettingMenuViewController:self
                                                               didSelectMenu:tableItem];
            }
            
            if(!performed){
                if([self respondsToSelector:tableItem.action] ){
                    [self performSelector:tableItem.action
                               withObject:tableItem];
                }
            }
        }
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

- (DCRoundSwitch *)demoSwitch
{
    if(_demoSwitch == nil){
//        _demoSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
//        [_demoSwitch addTarget:self
//                        action:@selector(demoSwitchChanged:)
//              forControlEvents:UIControlEventValueChanged];
        _demoSwitch = [[DCRoundSwitch alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 88.0f, 29.0f)];
        _demoSwitch.onText = NSLocalizedString(@"Real", nil);
        _demoSwitch.offText = NSLocalizedString(@"Test", nil);
        _demoSwitch.onTintColor = [UIColor colorWithR:231.0f G:158.0f B:58.0f];
        [_demoSwitch addTarget:self
                        action:@selector(demoSwitchChanged:)
              forControlEvents:UIControlEventValueChanged];

        
    }
    return _demoSwitch;
}

- (DCRoundSwitch *)openCloseSwitch
{
    if(_openCloseSwitch == nil){
        //        _openCloseSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
        //        [_openCloseSwitch addTarget:self
        //                        action:@selector(openCloseSwitchChanged:)
        //              forControlEvents:UIControlEventValueChanged];
        _openCloseSwitch = [[DCRoundSwitch alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 106.0f, 29.0f)];
        _openCloseSwitch.onText = NSLocalizedString(@"Open", nil);
        _openCloseSwitch.offText = NSLocalizedString(@"Closed", nil);
        _openCloseSwitch.onTintColor = [UIColor colorWithR:231.0f G:158.0f B:58.0f];
        [_openCloseSwitch addTarget:self
                        action:@selector(openCloseSwitchChanged:)
              forControlEvents:UIControlEventValueChanged];
        
        
    }
    return _openCloseSwitch;
}

- (UISwitch *)publishSwitch
{
    if(_publishSwitch == nil){
        _publishSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
        [_publishSwitch addTarget:self
                        action:@selector(publishSwitchChanged:)
              forControlEvents:UIControlEventValueChanged];
    }
    return _publishSwitch;
}

- (void)demoSwitchChanged:(DCRoundSwitch *)sender
{
    if([TABLE.activeTableList count] > 0){
        [UIAlertView alertWithTitle:NSLocalizedString(@"There are some active orders", nil)
                            message:NSLocalizedString(@"You should complete or reset all active orders before changing test mode", nil)];
        sender.on = !CRED.merchant.isTestMode;
        [TABLE reloadTableStatus];
        return;
    }
    
    if(!CRED.merchant.isAuthenticated && sender.on){
//        if([self.delegate respondsToSelector:@selector(merchantSettingMenuViewControllerWillChangeToLive:)]){
//            [self.delegate merchantSettingMenuViewControllerWillChangeToLive:self];
//        } else {
//            MerchantAccountViewController *viewController = [MerchantAccountViewController viewControllerFromNib];
//            [self.navigationController pushViewController:viewController
//                                                 animated:YES];
//        }
        [UIAlertView alertWithTitle:NSLocalizedString(@"Please contact RushOrder for further steps", nil)
                            message:NSLocalizedString(@"Tel:310-473-8092\nrequest@rushorderapp.com​", nil)];
        sender.on = NO;
    } else {
        [self requestMerchantModeChangeWithType:MerchantSwitchTypeTest value:!sender.on];
    }
}

- (void)openCloseSwitchChanged:(DCRoundSwitch *)sender
{
    if(!sender.on){
        [UIAlertView askWithTitle:NSLocalizedString(@"Confirm Close?", nil)
                          message:NSLocalizedString(@"Are you sure to close the restaurant?", nil)
                         delegate:self
                              tag:303];
    } else {
        [self requestMerchantModeChangeWithType:MerchantSwitchTypeOpen value:sender.on];
    }
}


- (void)publishSwitchChanged:(UISwitch *)sender
{
    [self requestMerchantModeChangeWithType:MerchantSwitchTypePublish value:sender.on];
}

- (void)requestMerchantModeChangeWithType:(MerchantSwitchType)type
                                    value:(BOOL)value
{
    RequestResult result = RRFail;
    result = [MERCHANT requestMerchant:CRED.merchant
                                  type:type
                                 value:value
                       completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              switch(type){
                                  case MerchantSwitchTypeTest:
                                      CRED.merchant.testMode = value;
                                      break;
                                  case MerchantSwitchTypeOpen:
                                      CRED.merchant.open = value;
                                      CRED.merchant.lastOperationDate = [response dateForKey:@"last_modified_open_close_at"];
                                      break;
                                  case MerchantSwitchTypePublish:
                                      CRED.merchant.published = value;
                                      break;
                              }

                              [self.tableView reloadData];
                              break;
                          case ResponseErrorUnauthorized:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"You're not activated", nil)
                                                  message:NSLocalizedString(@"You have to activate your account before turning on real mode.", nil)
                                                 delegate:self
                                                      tag:302];
                              
                              break;
                          case ResponseErrorIncompleteOrderExists:
                          case ResponseErrorActiveOrderExists:
                              [UIAlertView alertWithTitle:NSLocalizedString(@"There are some active orders", nil)
                                                  message:NSLocalizedString(@"You should complete or reset all active orders before changing test mode", nil)];
                              [TABLE reloadTableStatus];
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while changing running mode", nil)];
                                  switch(type){
                                      case MerchantSwitchTypeTest:
                                          [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while changing running mode", nil)];
                                          break;
                                      case MerchantSwitchTypeOpen:
                                          // Shoud not occur
                                          break;
                                      case MerchantSwitchTypePublish:
                                          [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while changing publish status", nil)];
                                          break;
                                  }
                              }
                              
                              switch(type){
                                  case MerchantSwitchTypeTest:
                                      self.demoSwitch.on = !CRED.merchant.testMode;
                                      break;
                                  case MerchantSwitchTypeOpen:
                                      self.openCloseSwitch.on = CRED.merchant.isOpenHour;
                                      break;
                                  case MerchantSwitchTypePublish:
                                      self.publishSwitch.on = CRED.merchant.isPublished;
                                      break;
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(alertView.tag){
        case 302:
        if([self.delegate respondsToSelector:@selector(merchantSettingMenuViewControllerWillChangeToLive:)]){
            [self.delegate merchantSettingMenuViewControllerWillChangeToLive:self];
        }
            break;
        case 303:
            // Open/Close
            if(buttonIndex == AlertButtonIndexYES){
                [self requestMerchantModeChangeWithType:MerchantSwitchTypeOpen value:NO];
            } else {
                self.openCloseSwitch.on = YES;
            }
            break;
        default:
            break;
    }
}


- (void)uiLogoutProcess
{
    [APP transitLogin];
    [CRED.userAccountItem resetKeychainItem];
    [NSUserDefaults standardUserDefaults].autoLogin = NO;
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)signOutButtonTouched:(id)sender
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [self dismissViewControllerAnimated:YES
                                 completion:^{
                                     [self signOutProcess];
                                 }];
    } else {
        [self signOutProcess];
    }
}

- (void)signOutProcess
{   
    RequestResult result = RRFail;
    result = [CRED requestSignOutCompletionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              PCLog(@"Successfully signed out");
                              [self uiLogoutProcess];
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while signing out", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        case RRAuthenticateError:
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Not Signed In",nil)];
            break;
        default:
            break;
    }
}
@end
