//
//  NoticeViewControllerIPad.h
//  RushOrder
//
//  Created by Conan on 6/13/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCiPadViewController.h"

@protocol NoticeViewControllerIPadDelegate;

@interface NoticeViewControllerIPad : PCiPadViewController

@property (weak, nonatomic) id<NoticeViewControllerIPadDelegate> delegate;
@property (nonatomic) PCSerial noticeNo;
@property (copy, nonatomic) NSString *format;
@property (copy, nonatomic) NSString *content;
@property (weak, nonatomic) IBOutlet UIWebView *noticeWebView;

@end

@protocol NoticeViewControllerIPadDelegate <NSObject>
- (void)noticeViewController:(NoticeViewControllerIPad *)viewController didTouchCloseButton:(id)sender;
@end
