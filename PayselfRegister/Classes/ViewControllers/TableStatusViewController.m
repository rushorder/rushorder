//
//  TableStatusViewController.m
//  RushOrder
//
//  Created by Conan on 2/28/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "TableStatusViewController.h"
#import "TableStatusCell.h"
#import "PickupOrderCell.h"
#import "OrderStatusCell.h"
#import "TableStatusManager.h"
#import "OrderDetailViewController.h"
#import "PCMerchantService.h"
#import "AggregateGroup.h"
#import "MerchantSettingMenuViewController.h"
#import "MenuManager.h"
#import "MenuOrderViewController.h"
#import "MenuOrderManager.h"
#import "AlertManager.h"

@interface TableStatusViewController ()

@property (strong, atomic) Order *totalOrder;

@property (strong, nonatomic) NSMutableArray *aggregateList;
@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *endDate;

@property (weak, nonatomic) IBOutlet UIView *logoView;
@property (strong, nonatomic) IBOutlet UIView *hotlineView;
@property (weak, nonatomic) IBOutlet UILabel *logoSubLabel;
@property (weak, nonatomic) IBOutlet NPBatchTableView *tableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *filterSegmentedControl;
@property (weak, nonatomic) NSArray *tableStatusList; //Just reference to list in the TableStatusManager (Do not set strong)

@property (strong, nonatomic) NSMutableArray *fixedList;
@property (strong, nonatomic) IBOutlet UIView *noItemView;
@property (strong, nonatomic) IBOutlet UIView *downloadView;
@property (strong, nonatomic) IBOutlet UITableViewCell *addItemCell;
@property (strong, nonatomic) NSDate *lastUpdateDate;

@property (nonatomic, getter = isLoading) BOOL loading;
@end

@implementation TableStatusViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if(self != nil){
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
//    self.startDate = [[NSDate date] dateWithZero];    
//    self.endDate = [[[NSDate date] dateByAddingTimeInterval:(60 * 60 * 24)] dateWithZero];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tableStatusUpdated:)
                                                 name:TableListUpdatedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tableStatusError:)
                                                 name:TableListErrorNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(togoListUpdated:)
                                                 name:TogoListUpdatedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orderUpdate:)
                                                 name:OrderUpdatedNotification
                                               object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)tableStatusUpdated:(NSNotification *)aNoti
{
    NSDictionary *userInfo = aNoti.userInfo;
    BOOL isSignOut = [userInfo boolForKey:@"signedOutKey"];
    
    if(isSignOut){
        //Signed out!!!!
        self.aggregateList = nil;
        [self reloadData];
        return;
    }
    
    if(aNoti.userInfo == nil){
        [self segmentedControlValuChanged:self.filterSegmentedControl];
    } else {
        
        NSArray *removedIndexPaths = [aNoti.userInfo objectForKey:@"removedIndexPaths"];
        NSArray *addedIndexPaths = [aNoti.userInfo objectForKey:@"addedIndexPaths"];
        NSArray *updatedIndexPaths = [aNoti.userInfo objectForKey:@"updatedIndexPaths"];
        
        switch(self.filterSegmentedControl.selectedSegmentIndex){
            case 0:{
                [self.tableView beginUpdates];
                if(addedIndexPaths != nil){
                    [self.tableView insertRowsAtIndexPaths:addedIndexPaths
                                          withRowAnimation:UITableViewRowAnimationRight];
                }
                
                if(updatedIndexPaths != nil){
                    [self.tableView reloadRowsAtIndexPaths:updatedIndexPaths
                                          withRowAnimation:UITableViewRowAnimationFade];
                }
                
                if(removedIndexPaths != nil){
                    [self.tableView deleteRowsAtIndexPaths:removedIndexPaths
                                          withRowAnimation:UITableViewRowAnimationLeft];
                }
                [self.tableView endUpdates];
                
                if([self.tableStatusList count] == 0){
                    self.tableView.tableFooterView = self.noItemView;
                } else {
                    self.tableView.tableFooterView = nil;
                }
            }
                break;
            case 1:
                if([removedIndexPaths count] > 0){
                    [self requestFixedList];
                } else {
                    // If ActiveTableList already purge the fixed state, there is no indexPaths. But refresh completed payment list
                    if([removedIndexPaths count] == 0 &&
                       [addedIndexPaths count] == 0 &&
                       [updatedIndexPaths count] == 0
                       ){
                        [self requestFixedList];
                    }
                }
                break;
        }
    }
    self.lastUpdateDate = [NSDate date];
    self.loading = NO;
    [self.tableView.refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
}

- (void)tableStatusError:(NSNotification *)aNoti
{
    self.loading = NO;
    [self.tableView.refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
}

- (void)togoListUpdated:(NSNotification *)aNoti
{
    if(aNoti.userInfo == nil){
        [self segmentedControlValuChanged:self.filterSegmentedControl];
    } else {
        
        NSArray *removedIndexPaths = [aNoti.userInfo objectForKey:@"removedIndexPaths"];
        NSArray *addedIndexPaths = [aNoti.userInfo objectForKey:@"addedIndexPaths"];
        NSArray *updatedIndexPaths = [aNoti.userInfo objectForKey:@"updatedIndexPaths"];
        
        switch(self.filterSegmentedControl.selectedSegmentIndex){
            case 0:{
                [self.tableView beginUpdates];
                if(addedIndexPaths != nil){
                    [self.tableView insertRowsAtIndexPaths:addedIndexPaths
                                          withRowAnimation:UITableViewRowAnimationRight];
                }
                
                if(updatedIndexPaths != nil){
                    [self.tableView reloadRowsAtIndexPaths:updatedIndexPaths
                                          withRowAnimation:UITableViewRowAnimationFade];
                }
                
                if(removedIndexPaths != nil){
                    [self.tableView deleteRowsAtIndexPaths:removedIndexPaths
                                          withRowAnimation:UITableViewRowAnimationLeft];
                }
                [self.tableView endUpdates];
                
                if([self.tableStatusList count] == 0){
                    self.tableView.tableFooterView = self.noItemView;
                } else {
                    self.tableView.tableFooterView = nil;
                }
            }
                break;
            case 1:
                if([removedIndexPaths count] > 0){
                    [self requestFixedList];
                } else {
                    // If ActiveTableList already purge the fixed state, there is no indexPaths. But refresh completed payment list
                    if([removedIndexPaths count] == 0 &&
                       [addedIndexPaths count] == 0 &&
                       [updatedIndexPaths count] == 0
                       ){
                        [self requestFixedList];
                    }
                }
                break;
        }
    }
    self.lastUpdateDate = [NSDate date];
    self.loading = NO;
    [self.tableView.refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
}

- (void)orderUpdate:(NSNotification *)aNoti
{
   [self segmentedControlValuChanged:self.filterSegmentedControl];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = NSLocalizedString(@"Back", nil);
    
//    self.navigationItem.titleView = self.logoView;
    self.navigationItem.titleView = self.hotlineView;
    
//    [self changeSubLogo:[NSUserDefaults standardUserDefaults].isTestDrive];
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
//        self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonSystemItem:UIBarButtonSystemItemRefresh
//                                                                               target:self
//                                                                               action:@selector(refreshButtonTouched:)];
    } else {
//        self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemSetting
//                                                                             target:self
//                                                                             action:@selector(settingButtonTouched:)];
//        
//        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonNovaItem:NPBarButtonItemLeftMenu
//                                                                            target:self
//                                                                            action:@selector(functionButtonTouched:)];
//        
//        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonSystemItem:UIBarButtonSystemItemAction
//                                                                               target:self
//                                                                               action:@selector(functionButtonTouched:)];
        
        self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithImage:[UIImage imageNamed:@"button_settings"]
                                                                                                      target:self
                                                                                                      action:@selector(settingButtonTouched:)
                                                                                                    backType:NPBarButtonBackTypeClear];
    
    }
    
    self.tableView.rowHeight = 78.0f;
    
    self.tableStatusList = TABLE.activeTableList;
    
    self.downloadView.hidden = [NSUserDefaults standardUserDefaults].walletCommercialClosed;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    if(self.tableView.contentOffset.y == 0){
//        [self.tableView setContentOffset:CGPointMake(0, self.searchDisplayController.searchBar.height)
//                                animated:NO];
//    }
    
    [TABLE reloadTableStatusIfNeeded];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self reloadData];
}

- (void)settingButtonTouched:(id)sender
{
    MerchantSettingMenuViewController *viewController = [MerchantSettingMenuViewController viewControllerFromNib];
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (void)functionButtonTouched:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:NSLocalizedString(@"Sign Out", nil)
                                                    otherButtonTitles:nil];
    actionSheet.tag = 1;
    [actionSheet showFromBarButtonItem:sender
                              animated:YES];
}

- (void)refreshButtonTouched:(id)sender
{
    [TABLE reloadTableStatus];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger ctr = 0;
    
    switch(self.filterSegmentedControl.selectedSegmentIndex){
        case 0:
            return 1;
            break;
        case 1:
            ctr = [self.aggregateList count];
            return ctr;
            break;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger ctr = 0;
    
    switch(self.filterSegmentedControl.selectedSegmentIndex){
        case 0:
            ctr = [self.tableStatusList count];
#if POS_ENABLED
            ctr++;
#endif
            return ctr;
            break;
        case 1:
        {
            AggregateGroup *aggregateGroup = [self.aggregateList objectAtIndex:section];
            return [aggregateGroup.list count];
        }
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch(self.filterSegmentedControl.selectedSegmentIndex){
        case 0:
#if POS_ENABLED
            if(indexPath.row == 0){
                return self.addItemCell;
            } else {
#endif
                return [self tableView:tableView tableCellForRowAtIndexPath:indexPath];
#if POS_ENABLED
            }
#endif
            break;
        case 1:
            return [self tableView:tableView orderCellForRowAtIndexPath:indexPath];
            break;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch(self.filterSegmentedControl.selectedSegmentIndex){
        case 0:
#if POS_ENABLED
            if(indexPath.row == 0){
                return self.addItemCell.height;
            } else {
#endif
                return self.tableView.rowHeight;
#if POS_ENABLED
            }
#endif
            break;
        default:
            return self.tableView.rowHeight;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView tableCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
#if POS_ENABLED
    id obj = [self.tableStatusList objectAtIndex:(indexPath.row - 1)];
#else
    id obj = [self.tableStatusList objectAtIndex:indexPath.row];
#endif
    
    if([obj isKindOfClass:[TableInfo class]]){
        return [self tableView:tableView tableInfo:obj cellForRowAtIndexPath:indexPath];
    } else if([obj isKindOfClass:[Order class]]){
        return [self tableView:tableView order:obj cellForRowAtIndexPath:indexPath];
    }
    
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView tableInfo:(TableInfo *)tableInfo cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TableStatusCell";
    
    TableStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil){
        cell = [TableStatusCell cell];
    }
    
    cell.tableInfo = tableInfo;
    
    [cell fillTableInfo];
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView order:(Order *)order cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PickupOrderCell";
    
    PickupOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil){
        cell = [PickupOrderCell cell];
    }
    
    cell.order = order;
    
    [cell fillOrderInfo];
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView orderCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"OrderStatusCell";
    
    OrderStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil){
        cell = [OrderStatusCell cell];
    }
    
    AggregateGroup *aggregateGroup = [self.aggregateList objectAtIndex:indexPath.section];
    Order *anOrder = [aggregateGroup.list objectAtIndex:indexPath.row];
    cell.order = anOrder;
    
    [cell fillOrderInfo];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderDetailViewController *viewController = nil;
    
    switch(self.filterSegmentedControl.selectedSegmentIndex){
        case 0:
        {
#if POS_ENABLED
            if(indexPath.row == 0){
                //Menu selection
                [self pushMenuViewController:CRED.merchant
                               withTableInfo:nil];
                
            } else {
                id obj = [self.tableStatusList objectAtIndex:indexPath.row - 1];
                NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:(indexPath.row - 1)
                                                               inSection:indexPath.section];
#else
                id obj = [self.tableStatusList objectAtIndex:indexPath.row];
                NSIndexPath *newIndexPath = indexPath;
#endif
                
                if([obj isKindOfClass:[TableInfo class]]){
                    [self tableView:tableView didSelectTableInfo:obj rowAtIndexPath:newIndexPath];
                } else if([obj isKindOfClass:[Order class]]){
                    [self tableView:tableView didSelectOrder:obj rowAtIndexPath:newIndexPath];
                }
#if POS_ENABLED
            }
#endif
        }
            break;
        case 1:
        {
            viewController = [OrderDetailViewController viewControllerFromNib];
            
            AggregateGroup *aggregateGroup = [self.aggregateList objectAtIndex:indexPath.section];
            Order *anOrder = [aggregateGroup.list objectAtIndex:indexPath.row];
            
            if(anOrder.isTicketBase){
                viewController.order = anOrder;
            } else {
                TableInfo *wrapTableInfo = [[TableInfo alloc] initWithTableLabel:anOrder.tableNumber
                                                                      merchantNo:anOrder.merchantNo];
                wrapTableInfo.wrappingMode = YES;
                wrapTableInfo.order = anOrder;
                viewController.tableInfo = wrapTableInfo;
            }
            
            [self.navigationController pushViewController:viewController
                                                 animated:YES];
        }
            break;
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath
                                  animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}

- (void)tableView:(UITableView *)tableView didSelectTableInfo:(TableInfo *)tableInfo rowAtIndexPath:(NSIndexPath *)indexPath
{
//    if((selectedTableInfo.cart.status == CartStatusSubmitted) ||
//       (selectedTableInfo.cart.status == CartStatusOrdering && selectedTableInfo.order == nil)){
//        [self tableView:tableView didSelectCartRowAtIndexPath:indexPath];
//    } else {
    OrderDetailViewController *viewController = [OrderDetailViewController viewControllerFromNib];
    viewController.tableInfo = tableInfo;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
//     }

}

- (void)tableView:(UITableView *)tableView didSelectOrder:(Order *)order rowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderDetailViewController *viewController = [OrderDetailViewController viewControllerFromNib];
    viewController.order = order;
    [self.navigationController pushViewController:viewController
                                         animated:YES];
}

- (IBAction)segmentedControlValuChanged:(UISegmentedControl *)sender
{
    switch(sender.selectedSegmentIndex){
        case 0:
            self.tableStatusList = TABLE.activeTableList;
            break;
        case 1:
            self.tableStatusList = self.aggregateList;
            [self requestFixedList];
            break;
        case 2:
            self.tableStatusList = TABLE.notEmptyTableList;
            break;
    }
    [self reloadData];
}



- (void)reloadData
{
    switch(self.filterSegmentedControl.selectedSegmentIndex){
        case 0:
            if([self.tableStatusList count] == 0){
                self.tableView.tableFooterView = self.noItemView;
            } else {
                self.tableView.tableFooterView = nil;
            }
            break;
        case 1:
            if([self.aggregateList count] == 0){
                self.tableView.tableFooterView = self.noItemView;
            } else {
                self.tableView.tableFooterView = nil;
            }
            break;
    }
    
    [self.tableView reloadData];
}

- (NSMutableArray *)fixedList
{
    if(_fixedList == nil){
        _fixedList = [NSMutableArray array];
    }
    
    return _fixedList;
}

- (void)requestFixedList
{
    if(CRED.userInfo.authToken == nil){
        self.aggregateList = nil;
        [self reloadData];
        return;
    }
    
    RequestResult result = RRFail;
    
    self.startDate = [[NSDate date] dateWithZero];
    self.endDate = [[[NSDate date] dateByAddingTimeInterval:(60 * 60 * 24)] dateWithZero];
    
    result = [MERCHANT requestOrder:CRED.merchant.merchantNo
                          startDate:self.startDate
                            endDate:self.endDate
                    completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:{
                              
                              self.totalOrder = [[Order alloc] init];
                              
                              dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                              dispatch_async(aQueue, ^{
                                  
                                  NSArray *list = (NSArray *)response.data;
                                  NSMutableArray *groupList = [NSMutableArray array];
                                  
                                  NSMutableArray *groupedList = [NSMutableArray array];
                                  
                                  NSDate *prevDate = nil;
                                  NSString *sectionTitle = nil;
                                  Order *sumOrder = nil;
                                  
                                  NSArray *sortedList = [list sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
                                      NSDate *date1 = [[obj1 objectForKey:@"updated_at"] date];
                                      NSDate *date2 = [[obj2 objectForKey:@"updated_at"] date];
                                      
                                      return [date2 compare:date1];
                                  }];

                                  for(NSDictionary *dict in sortedList){
                                      Order *anOrder = [[Order alloc] initWithDictionary:dict
                                                                                merchant:CRED.merchant
                                                                                 menuPan:MENUPAN.wholeSerialMenuList];
                                      
                                      if(anOrder.status != OrderStatusFixed && anOrder.status != OrderStatusDeclined){
                                          continue;
                                      }
                                      
                                      [self.totalOrder sum:anOrder];
                                      
                                      BOOL groupChanged = NO;
                                      if(prevDate != nil){
                                          groupChanged = [anOrder dateChanged:prevDate];
                                          sectionTitle = [prevDate dateStringForHumanReadableFormat];
                                      }
                                      
                                      if(groupChanged){
                                          AggregateGroup *group = [[AggregateGroup alloc] init];
                                          
                                          group.sectionTitle = sectionTitle;
                                          group.list = groupedList;
                                          group.sumOrder = sumOrder;
                                          sumOrder = nil;
                                          [groupList addObject:group];
                                          
                                          groupedList = [NSMutableArray array];
                                      }
                                      
                                      if(sumOrder == nil){
                                          sumOrder = [[Order alloc] init];
                                      }
                                      [sumOrder sum:anOrder];
                                      [groupedList addObject:anOrder];
                                      prevDate = anOrder.orderDate;
                                  }
                                  
                                  if([groupedList count] > 0){
                                      Order *anOrder = [groupedList objectAtIndex:0];
                                      AggregateGroup *group = [[AggregateGroup alloc] init];
                                      
                                      
                                      sectionTitle = [anOrder.orderDate dateStringForHumanReadableFormat];
                                      
                                      
                                      group.sectionTitle = sectionTitle;
                                      group.list = groupedList;
                                      group.sumOrder = sumOrder;
                                      sumOrder = nil;
                                      [groupList addObject:group];
                                  }
                                  
                                  
                                  dispatch_async(dispatch_get_main_queue(),^{
                                      self.aggregateList = groupList;
//                                      self.footerView.order = self.totalOrder;
//                                      [self.footerView fillCell];
//                                      
//                                      if([self.aggregateList count] > 0){
//                                          self.footerView.payDateLabel.text = [NSString stringWithFormat:@"%@ - %@",
//                                                                               self.startDateTextField.text,
//                                                                               self.endDateTextField.text];
//                                          self.tableView.tableFooterView = self.footerView;
//                                      } else {
//                                          self.tableView.tableFooterView = nil;
//                                      }
                                      
                                      [self reloadData];
                                  });
                              });
                              break;
                          }
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while getting order list", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (IBAction)bottomCloseButtonTouched:(id)sender
{
    self.downloadView.hidden = YES;
    
    [NSUserDefaults standardUserDefaults].walletCommercialClosed = YES;
}

- (IBAction)downloadButtonTouched:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:CUSTOMER_ITUNES_URL]];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(actionSheet.tag){
        case 1:
            PCLog(@"Button Index %d", buttonIndex);
            
            if(actionSheet.destructiveButtonIndex == buttonIndex){ //Sign out
                [self signOut];
            }
            
            break;
        default:
            break;
    }
}

- (void)uiLogoutProcess
{
    [APP transitLogin];
    [CRED.userAccountItem resetKeychainItem];
    [NSUserDefaults standardUserDefaults].autoLogin = NO;
}

- (void)signOut
{
    [self uiLogoutProcess];
    
    RequestResult result = RRFail;
    result = [CRED requestSignOutCompletionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              PCLog(@"Successfully signed out");
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while signing out", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)pushMenuViewController:(Merchant *)merchant withTableInfo:(TableInfo *)tableInfo
{
    BOOL pushMenuView = YES;
    if([MENUPAN resetGraphWithMerchant:merchant]){
        
    } else {
        // When same merchant
        if(MENUPAN.isMenuFetched && merchant.isAblePayment){
            if([MENUPAN.wholeMenuList count] == 0){
                pushMenuView = NO;
            } else {
                
            }
        } else {
            
        }
    }
    
    if(pushMenuView){
        MenuOrderViewController *viewController = [MenuOrderViewController viewControllerFromNib];
        [MENUORDER resetGraphWithMerchant:merchant];
        viewController.tableInfo = tableInfo;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (BOOL)refresh
{
    if([TABLE reloadTableStatus]){
        self.loading = YES;
        return YES;
    } else {
        [self.tableView.refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
        return NO;
    }
}

- (IBAction)hotlineButtonTouched:(id)sender
{
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://424-488-3170"]]){
        if(![[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://424-488-3170"]]){
            //            User's cancel
        }
    } else {
        [UIAlertView alertWithTitle:NSLocalizedString(@"Failed to Call",nil)
                            message:NSLocalizedString(@"Try to call manually", nil)];
    }
}

- (BOOL)inNewLoading
{
    return self.isLoading;
}

- (NSDate *)lastUpdatedDate
{
    return self.lastUpdateDate;
}
- (BOOL)isRedType
{
    return YES;
}
@end
