//
//  CompleteOptionViewController.h
//  RushOrder
//
//  Created by Conan on 3/13/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCiPadViewController.h"

@protocol CompleteOptionViewControllerDelegate;

@interface CompleteOptionViewController : PCiPadViewController
@property (weak, nonatomic) id<CompleteOptionViewControllerDelegate> delegate;
@end

@protocol CompleteOptionViewControllerDelegate<NSObject>
- (void)completeOptionViewController:(CompleteOptionViewController *)viewController
                     didSelectOption:(NSString *)option;
@end