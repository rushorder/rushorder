//
//  AdditionalViewController.m
//  RushOrder
//
//  Created by Conan on 12/2/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "AdditionalViewController.h"
#import "PCMerchantService.h"

@interface AdditionalViewController ()

@property (weak, nonatomic) IBOutlet UITextField *homepageTextField;
@property (weak, nonatomic) IBOutlet UITextView *descTextView;
//@property (weak, nonatomic) IBOutlet UITextView *hoursTextView;
@property (weak, nonatomic) IBOutlet UITextView *takeoutRemarkTextView;
@property (strong, nonatomic) IBOutlet NPKeyboardAwareScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NPStretchableButton *cancelButton;
@end

@implementation AdditionalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Additional Information", nil);
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonSystemItem:UIBarButtonSystemItemCancel
                                                                          target:self
                                                                          action:@selector(cancelButtonTouched:)];
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonSystemItem:UIBarButtonSystemItemSave
                                                                          target:self
                                                                          action:@selector(saveButtonTouched:)];

    self.homepageTextField.text = CRED.merchant.homepage;
    self.descTextView.text = CRED.merchant.desc;
//    self.hoursTextView.text = CRED.merchant.hours;
    self.takeoutRemarkTextView.text = CRED.merchant.takeoutRemark;
    
    [self.scrollView setContentSizeWithBottomView:self.cancelButton];
}

- (IBAction)cancelButtonTouched:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveButtonTouched:(id)sender
{
    [self requestMerchantAdditionalUpdate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)requestMerchantAdditionalUpdate
{
    
    Merchant *tempMerchant = [CRED.merchant copy];
    
    tempMerchant.homepage = self.homepageTextField.text;
    tempMerchant.desc = self.descTextView.text;
//    tempMerchant.hours = self.hoursTextView.text;
    tempMerchant.takeoutRemark = self.takeoutRemarkTextView.text;
    
    RequestResult result = RRFail;
    result = [MERCHANT requestMerchantAdditional:tempMerchant
                                 completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && 200 <= statusCode && statusCode < 300){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              [self cancelButtonTouched:nil];
                              
                              [CRED.merchant updateWithDictionary:response];
                              
                              break;
                          default:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while <#Default error message#>", nil)];
                              }
                          }
                              break;
                      }
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

@end
