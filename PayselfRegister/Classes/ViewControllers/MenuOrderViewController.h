//
//  MenuOrderViewController.h
//  RushOrder
//
//  Created by Conan on 5/14/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"
#import "MenuItemCell.h"
#import "MenuOrderDetailViewController.h"
#import "OrderListViewController.h"
#import "Merchant.h"

@interface MenuOrderViewController : PCViewController
<
UITableViewDelegate,
UITableViewDataSource,
UIPickerViewDataSource,
UIPickerViewDelegate,
MenuItemCellDelegate,
MenuOrderDetailViewControllerDelegate,
OrderListViewControllerDelegate,
UITextFieldDelegate,
UIAlertViewDelegate
>

@property (strong, nonatomic) TableInfo *tableInfo;

- (void)cartUnsolicitChanged:(NSNotification *)notification;

@end