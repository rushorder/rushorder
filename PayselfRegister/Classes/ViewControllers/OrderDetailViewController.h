//
//  OrderDetailViewController.h
//  RushOrder
//
//  Created by Conan on 3/5/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCiPadViewController.h"
#import "TableInfo.h"
#import "Order.h"
#import "CompleteOptionViewController.h"
#import "CardPaymentCell.h"
#import "RejectViewController.h"



@interface OrderDetailViewController : PCiPadViewController
<
UITableViewDataSource,
UITableViewDelegate,
UIAlertViewDelegate,
CompleteOptionViewControllerDelegate,
UIActionSheetDelegate,
CardPaymentCellDelegate,
RejectViewControllerDelegate,
UITextFieldDelegate
>

@property (strong, nonatomic) TableInfo *tableInfo; //for Table service base
@property (strong, nonatomic) Order *order; //for Self service base

@property (weak, nonatomic) UIPopoverController *popover;

@property (readonly) BOOL isTicketBase;

- (IBAction)moneyFieldValueChanged:(id)sender;

@end