//
//  TableLabelSettingViewController.m
//  RushOrder
//
//  Created by Conan on 3/19/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "TableLabelSettingViewController.h"
#import "TableStatusManager.h"
#import "JSONKit.h"
#import "PCMerchantService.h"

@interface TableLabelSettingViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableList;
@end

@implementation TableLabelSettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        for(TableInfo *tableInfo in TABLE.tableList){
            if([tableInfo isKindOfClass:[TableInfo class]]){
                NSString *tableLabel = [tableInfo.tableNumber copy];
                if(tableLabel != nil){
                    [self.tableList addObject:tableLabel];
                } else {
                    PCError(@"table number should not be nil");
                }
            }
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Table Labels", nil);
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithTitle:NSLocalizedString(@"Cancel", nil)
                                                                                 target:self
                                                                                 action:@selector(cancelButtonTouched:)];
    }
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc ]initWithTitle:NSLocalizedString(@"Apply", nil)
                                                                              style:UIBarButtonItemStyleDone
                                                                             target:self
                                                                             action:@selector(applyButtonTouched:)];
    self.tableView.rowHeight = 53.0f;
//    [self.tableView addDefaultBackgroundImageFill];
}

- (void)applyButtonTouched:(id)sender
{
    [self.tableView endEditing:YES];
    
    NSDictionary *dictForJson = [NSDictionary dictionaryWithObject:self.tableList
                                                            forKey:@"labels"];
    NSString *labelJsonString = [dictForJson JSONString];
    
    PCLog(@"jsonString : %@", labelJsonString);
    
    RequestResult result = RRFail;
    result = [MERCHANT requestSetTableLabels:CRED.merchant.merchantNo
                                labelSetJson:labelJsonString
                             completionBlock:
              ^(BOOL isSuccess, NSDictionary *response, NSInteger statusCode){
                  
                  if(isSuccess && HTTP_STATUS_OK <= statusCode && statusCode < HTTP_STATUS_MULTIPLE_CHOICES){
                      switch(response.errorCode){
                          case ResponseSuccess:
                              [self dismissViewControllerAnimated:YES
                                                       completion:^{
                                                           [TABLE reloadTableStatus];
                                                       }];
                              break;
                          case ResponseErrorActiveOrderExists:{
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Cannot change table labels", nil)
                                                  message:NSLocalizedString(@"You can change table labels only without active orders. Plaese try to change lables after completing or resetting active orders.", nil)];
                          }
                              break;
                          case ResponseErrorGeneral:{
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              }
                              break;
                          }
                          case ResponseErrorTableLabelDuplicated:{
                              [UIAlertView alertWithTitle:NSLocalizedString(@"Table number is duplicated", nil)
                                                  message:NSLocalizedString(@"Table number has to be unique. Find duplicated table labels and fix them first.", nil)];
                          }
                              break;
                          default:
                          {
                              NSString *message = [response objectForKey:@"message"];
                              if([message isKindOfClass:[NSString class]]){
                                  [UIAlertView alert:message];
                              } else {
                                  [UIAlertView alert:NSLocalizedString(@"Unknown error has occurred while setting table labels", nil)];
                              }
                          }
                              break;
                      }
                  } else if(statusCode == HTTP_STATUS_UNAUTHORIZED){
                      [UIAlertView alertWithTitle:UNAUTHORIZED_ERROR_TITLE message:UNAUTHORIZED_ERROR_MESSAGE];
                  } else {
                      if(statusCode == HTTP_STATUS_CONNECTION_ERROR){
                          [[NSNotificationCenter defaultCenter] postNotificationName:NetworkProblemReportNotification
                                                                              object:nil];
                      } else if(statusCode != HTTP_STATUS_CONNECTION_CANCELED){
                          [UIAlertView alert:[NSString stringWithFormat:NSLocalizedString(@"Unknown error has occurred.\n Error Code:%d", nil)
                                              ,statusCode]];
                      }
                  }
                  [SVProgressHUD dismiss];
              }];
    switch(result){
        case RRSuccess:
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
            break;
        case RRParameterError:
            [SVProgressHUD showErrorWithStatus:PARAMETER_ERROR_MESSAGE];
            break;
        default:
            break;
    }
}

- (void)cancelButtonTouched:(id)sender
{
    [self dismissViewControllerAnimated:YES
                             completion:^{

                             }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tableList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TableLabelEditCell";
    
    TableLabelEditCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil){
        cell = [TableLabelEditCell cell];
        cell.delegate = self;
    }
    
    NSString *tableLabel = [self.tableList objectAtIndex:indexPath.row];
    
    cell.tableLabelTextField.text = tableLabel;
    cell.serialLabel.text = [NSString stringWithInteger:(indexPath.row + 1)];
    
    return cell;
}

- (NSMutableArray *)tableList
{
    if(_tableList == nil){
        _tableList = [NSMutableArray array];
    }
    return _tableList;
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [super viewDidUnload];
}

#pragma mark - TableLabelEditCellDelegate

- (BOOL)tableLabelEditCell:(TableLabelEditCell *)cell textFieldShouldReturn:(UITextField *)textField indexPath:(NSIndexPath *)indexPath
{
    if((indexPath.row + 1)< [self.tableList count]){
        TableLabelEditCell *nextCell = (TableLabelEditCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:(indexPath.row + 1)
                                                                                                inSection:0]];
        [nextCell.tableLabelTextField becomeFirstResponder];
    }
    return YES;
}
- (BOOL)tableLabelEditCell:(TableLabelEditCell *)cell textFieldShouldClear:(UITextField *)textField indexPath:(NSIndexPath *)indexPath
{ 
    return YES;
}
- (void)tableLabelEditCell:(TableLabelEditCell *)cell textFieldDidEndEditing:(UITextField *)textField indexPath:(NSIndexPath *)indexPath
{
    [self.tableList replaceObjectAtIndex:indexPath.row
                              withObject:[textField.text uppercaseString]];
}
@end
