//
//  RejectViewController.h
//  RushOrder
//
//  Created by Conan on 11/15/13.
//  Copyright (c) 2013 RushOrder. All rights reserved.
//

#import "PCViewController.h"

@protocol RejectViewControllerDelegate;

@interface RejectViewController : PCViewController
@property(weak, nonatomic) id<RejectViewControllerDelegate> delegate;
@end

@protocol RejectViewControllerDelegate <NSObject>
- (void)rejectViewController:(RejectViewController *)viewController
     allowCustomerAdjustment:(BOOL)allowCustomerAdjustment
           rejectWithMessage:(NSString *)message;
@end